package com.navdy.obd;

import java.util.Iterator;
import java.util.Collections;
import java.util.ArrayList;
import android.os.Parcel;
import java.util.List;
import android.os.Parcelable.Creator;
import android.os.Parcelable;

public class VehicleInfo implements Parcelable
{
    public static final Parcelable.Creator<VehicleInfo> CREATOR;
    public final List<ECU> ecus;
    public final boolean isCheckEngineLightOn;
    private ECU primaryEcu;
    public final String protocol;
    public final List<String> troubleCodes;
    public final String vin;
    
    static {
        CREATOR = (Parcelable.Creator)new Parcelable.Creator<VehicleInfo>() {
            public VehicleInfo createFromParcel(final Parcel parcel) {
                return new VehicleInfo(parcel);
            }
            
            public VehicleInfo[] newArray(final int n) {
                return new VehicleInfo[n];
            }
        };
    }

    public VehicleInfo(Parcel parcel) {
        ArrayList arrayList = new ArrayList();
        this.protocol = parcel.readString();
        this.vin = parcel.readString();
        parcel.readList(arrayList, null);
        this.ecus = Collections.unmodifiableList(arrayList);
        boolean bl = parcel.readByte() != 0;
        this.isCheckEngineLightOn = bl;
        arrayList = new ArrayList();
        parcel.readList(arrayList, null);
        this.troubleCodes = Collections.unmodifiableList(arrayList);
    }

    
    public VehicleInfo(final String s) {
        this(null, null, s, false, null);
    }
    
    public VehicleInfo(final String protocol, final List<ECU> list, final String vin, final boolean isCheckEngineLightOn, final List<String> troubleCodes) {
        this.protocol = protocol;
        List<ECU> unmodifiableList;
        if (list != null) {
            unmodifiableList = Collections.<ECU>unmodifiableList((List<? extends ECU>)list);
        }
        else {
            unmodifiableList = null;
        }
        this.ecus = unmodifiableList;
        this.vin = vin;
        this.isCheckEngineLightOn = isCheckEngineLightOn;
        this.troubleCodes = troubleCodes;
    }
    
    public int describeContents() {
        return 0;
    }
    
    public List<ECU> getEcus() {
        return this.ecus;
    }
    
    public ECU getPrimaryEcu() {
        if (this.primaryEcu == null) {
            int n = 0;
            ECU primaryEcu = null;
            for (final ECU ecu : this.ecus) {
                final int size = ecu.supportedPids.size();
                if (size > n && ecu.supportedPids.contains(13)) {
                    primaryEcu = ecu;
                    n = size;
                }
            }
            this.primaryEcu = primaryEcu;
        }
        return this.primaryEcu;
    }
    
    public void writeToParcel(final Parcel parcel, int n) {
        parcel.writeString(this.protocol);
        parcel.writeString(this.vin);
        parcel.writeList((List)this.ecus);
        if (this.isCheckEngineLightOn) {
            n = 1;
        }
        else {
            n = 0;
        }
        parcel.writeByte((byte)n);
        parcel.writeList((List)this.troubleCodes);
    }
}
