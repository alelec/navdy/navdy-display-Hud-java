package com.navdy.hud.app.event;

import com.navdy.hud.app.analytics.AnalyticsSupport$WakeupReason;

public class Wakeup {
    public AnalyticsSupport$WakeupReason reason;

    public Wakeup(AnalyticsSupport$WakeupReason reason) {
        this.reason = reason;
    }
}
