package com.navdy.hud.app.maps.here;

class HereSafetySpotListener$2 implements Runnable {
    final com.navdy.hud.app.maps.here.HereSafetySpotListener this$0;
    final com.here.android.mpa.guidance.SafetySpotNotification val$safetySpotNotification;
    
    HereSafetySpotListener$2(com.navdy.hud.app.maps.here.HereSafetySpotListener a, com.here.android.mpa.guidance.SafetySpotNotification a0) {

        super();
        this.this$0 = a;
        this.val$safetySpotNotification = a0;
    }
    
    public void run() {
        java.util.List a = this.val$safetySpotNotification.getSafetySpotNotificationInfos();
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (a.size() != 0) {
                        break label0;
                    }
                }
                com.navdy.hud.app.maps.here.HereSafetySpotListener.access$300().i("no safety spots");
                break label2;
            }
            com.here.android.mpa.common.GeoCoordinate a0 = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
            if (a0 != null) {
                java.util.ArrayList a1 = new java.util.ArrayList();
                Object a2 = a.iterator();
                while(((java.util.Iterator)a2).hasNext()) {
                    com.here.android.mpa.guidance.SafetySpotNotificationInfo a3 = (com.here.android.mpa.guidance.SafetySpotNotificationInfo)((java.util.Iterator)a2).next();
                    com.here.android.mpa.mapping.SafetySpotInfo a4 = a3.getSafetySpot();
                    if (a4 != null && a4.getType() != com.here.android.mpa.mapping.SafetySpotInfo.Type.UNDEFINED) {
                        ((java.util.List)a1).add(a3);
                    }
                }
                if (((java.util.List)a1).size() != 0) {
                    com.navdy.hud.app.maps.here.HereSafetySpotListener.access$500(this.this$0).removeCallbacks(com.navdy.hud.app.maps.here.HereSafetySpotListener.access$700(this.this$0));
                    com.navdy.hud.app.maps.here.HereSafetySpotListener.access$800(this.this$0, (java.util.List)a1, a0);
                    com.navdy.hud.app.maps.here.HereSafetySpotListener.access$500(this.this$0).postDelayed(com.navdy.hud.app.maps.here.HereSafetySpotListener.access$700(this.this$0), (long)com.navdy.hud.app.maps.here.HereSafetySpotListener.access$400());
                } else {
                    com.navdy.hud.app.maps.here.HereSafetySpotListener.access$300().i("no valid safety spots");
                }
            } else {
                com.navdy.hud.app.maps.here.HereSafetySpotListener.access$300().e("no current location");
            }
        }
    }
}
