package com.navdy.hud.app.maps.here;

class HereMapCameraManager$9 {
    final static int[] $SwitchMap$com$navdy$hud$app$maps$MapEvents$DialMapZoom$Type;
    final static int[] $SwitchMap$com$navdy$hud$app$maps$here$HereMapController$State;
    
    static {
        $SwitchMap$com$navdy$hud$app$maps$MapEvents$DialMapZoom$Type = new int[com.navdy.hud.app.maps.MapEvents$DialMapZoom$Type.values().length];
        com.navdy.hud.app.maps.MapEvents$DialMapZoom$Type a0 = com.navdy.hud.app.maps.MapEvents$DialMapZoom$Type.IN;
        try {
            $SwitchMap$com$navdy$hud$app$maps$MapEvents$DialMapZoom$Type[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignored) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$maps$MapEvents$DialMapZoom$Type[com.navdy.hud.app.maps.MapEvents$DialMapZoom$Type.OUT.ordinal()] = 2;
        } catch(NoSuchFieldError ignored) {
        }
        $SwitchMap$com$navdy$hud$app$maps$here$HereMapController$State = new int[com.navdy.hud.app.maps.here.HereMapController$State.values().length];
        com.navdy.hud.app.maps.here.HereMapController$State a2 = com.navdy.hud.app.maps.here.HereMapController$State.OVERVIEW;
        try {
            $SwitchMap$com$navdy$hud$app$maps$here$HereMapController$State[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignored) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$maps$here$HereMapController$State[com.navdy.hud.app.maps.here.HereMapController$State.AR_MODE.ordinal()] = 2;
        } catch(NoSuchFieldError ignored) {
        }
    }
}
