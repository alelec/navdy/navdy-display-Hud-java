package com.navdy.hud.app.maps.here;
import com.navdy.hud.app.R;

final class HereRouteManager$2 implements com.navdy.hud.app.maps.here.HerePlacesManager$GeoCodeCallback {
    final boolean val$factoringInTraffic;
    final com.navdy.service.library.events.navigation.NavigationRouteRequest val$request;
    final com.here.android.mpa.common.GeoCoordinate val$startPoint;
    final java.util.List val$waypoints;
    
    HereRouteManager$2(com.navdy.service.library.events.navigation.NavigationRouteRequest a, com.here.android.mpa.common.GeoCoordinate a0, java.util.List a1, boolean b) {

        super();
        this.val$request = a;
        this.val$startPoint = a0;
        this.val$waypoints = a1;
        this.val$factoringInTraffic = b;
    }
    
    public void result(com.here.android.mpa.search.ErrorCode a, java.util.List a0) {
        label0: try {
            com.here.android.mpa.search.ErrorCode a3 = com.here.android.mpa.search.ErrorCode.CANCELLED;
            {
                if (a != a3) {
                    boolean b;
                    synchronized (HereRouteManager.access$400()) {
                        b = android.text.TextUtils.equals(this.val$request.requestId, HereRouteManager.access$800());
                        /*monexit(a4)*/
                        ;
                    }
                    if (b) {
                        com.here.android.mpa.search.ErrorCode a7;
                        synchronized (HereRouteManager.access$400()) {
                            HereRouteManager.access$902(null);
                            HereRouteManager.access$802(null);
                            HereRouteManager.access$1002(null);
                            /*monexit(a1)*/
                            ;
                            a7 = com.here.android.mpa.search.ErrorCode.NONE;
                        }
                        label1:
                        {
                            label2:
                            {
                                if (a != a7) {
                                    break label2;
                                }
                                if (a0 == null) {
                                    break label2;
                                }
                                if (a0.size() > 0) {
                                    break label1;
                                }
                            }
                            HereRouteManager.access$100().v("could not geocode, Street Address placesSearch error:" + a.name());
                            HereRouteManager.access$300(com.navdy.service.library.events.RequestStatus.REQUEST_SERVICE_ERROR, this.val$request, HereRouteManager.access$200().getString(R.string.geocoding_failed));
                            break label0;
                        }
                        HereRouteManager.access$100().v("Street Address results size=" + a0.size());
                        com.here.android.mpa.common.GeoCoordinate a9 = ((com.here.android.mpa.search.Location) a0.get(0)).getCoordinate();
                        HereRouteManager.access$100().v("Street Address placesSearch success: new-end-geo:" + a9);
                        HereRouteManager.access$1100(this.val$request, this.val$startPoint, this.val$waypoints, a9, this.val$factoringInTraffic);
                    } else {
                        HereRouteManager.access$100().v("geocode request is not valid active:" + HereRouteManager.access$800() + " request:" + this.val$request.requestId);
                        com.navdy.service.library.events.navigation.NavigationRouteResponse a10 = new com.navdy.service.library.events.navigation.NavigationRouteResponse(com.navdy.service.library.events.RequestStatus.REQUEST_CANCELLED, null, this.val$request.destination, null, null, Boolean.FALSE, this.val$request.requestId);
                        com.navdy.hud.app.maps.MapsEventHandler.getInstance().sendEventToClient(a10);
                    }
                } else {
                    HereRouteManager.access$100().v("geocode request has been cancelled [" + this.val$request.requestId + "]");
                }

            }

        } catch(Throwable a15) {
            com.navdy.hud.app.maps.here.HereRouteManager.access$100().v("Street Address placesSearch exception", a15);
            com.navdy.hud.app.maps.here.HereRouteManager.access$300(com.navdy.service.library.events.RequestStatus.REQUEST_UNKNOWN_ERROR, this.val$request, com.navdy.hud.app.maps.here.HereRouteManager.access$200().getString(R.string.geocoding_failed));
        }
    }
}
