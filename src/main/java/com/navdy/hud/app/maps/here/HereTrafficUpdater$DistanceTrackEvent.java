package com.navdy.hud.app.maps.here;

class HereTrafficUpdater$DistanceTrackEvent {
    final public com.here.android.mpa.routing.Route route;
    final com.navdy.hud.app.maps.here.HereTrafficUpdater this$0;
    final public com.here.android.mpa.mapping.TrafficEvent trafficEvent;
    
    public HereTrafficUpdater$DistanceTrackEvent(com.navdy.hud.app.maps.here.HereTrafficUpdater a, com.here.android.mpa.routing.Route a0, com.here.android.mpa.mapping.TrafficEvent a1) {

        super();
        this.this$0 = a;
        this.route = a0;
        this.trafficEvent = a1;
    }
}
