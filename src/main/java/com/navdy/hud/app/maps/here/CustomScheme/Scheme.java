package com.navdy.hud.app.maps.here.CustomScheme;

import java.util.Map;

public class Scheme {
    public String SchemeName;
    public String BaseScheme;
    public Map<String, String> Colours;
    public double ZoomMin = 0;
    public double ZoomMax = 20;
}
