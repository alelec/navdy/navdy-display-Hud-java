package com.navdy.hud.app.maps.here;

public class HereNavController {
    final private static int ANIMATE_STATE_CLEAR_DELAY = 2000;
    final private static com.navdy.hud.app.framework.trips.TripManager.FinishedTripRouteEvent TRIP_END;
    final private static com.navdy.hud.app.framework.trips.TripManager.NewTripEvent TRIP_START;
    final private static com.navdy.service.library.log.Logger sLogger;
    final private com.squareup.otto.Bus bus;
    private Runnable clearAnimatorState;
    private Runnable clearAnimatorStateBk;
    private boolean firstTrip;
    private android.os.Handler handler;
    private boolean initialized;
    final private com.here.android.mpa.guidance.NavigationManager navigationManager;
    final private com.navdy.hud.app.analytics.NavigationQualityTracker navigationQualityTracker;
    private com.here.android.mpa.routing.Route route;
    private volatile com.navdy.hud.app.maps.here.HereNavController$State state;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereNavController.class);
        TRIP_START = new com.navdy.hud.app.framework.trips.TripManager.NewTripEvent();
        TRIP_END = new com.navdy.hud.app.framework.trips.TripManager.FinishedTripRouteEvent();
    }
    
    HereNavController(com.here.android.mpa.guidance.NavigationManager a, com.squareup.otto.Bus a0) {
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.firstTrip = true;
        this.clearAnimatorState = (Runnable)new com.navdy.hud.app.maps.here.HereNavController$1(this);
        this.clearAnimatorStateBk = (Runnable)new com.navdy.hud.app.maps.here.HereNavController$2(this);
        this.navigationManager = a;
        this.bus = a0;
        sLogger.v(new StringBuilder().append(":ctor: state:").append(this.state).toString());
        this.navigationQualityTracker = com.navdy.hud.app.analytics.NavigationQualityTracker.getInstance();
    }
    
    static Runnable access$000(com.navdy.hud.app.maps.here.HereNavController a) {
        return a.clearAnimatorStateBk;
    }
    
    private void startTrip() {
        if (this.firstTrip) {
            this.firstTrip = false;
            sLogger.v("first trip started");
        } else {
            sLogger.v("trip started");
        }
        this.bus.post(TRIP_START);
    }
    
    public void addLaneInfoListener(java.lang.ref.WeakReference a) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        this.navigationManager.addLaneInformationListener(a);
    }
    
    public void addRealisticViewAspectRatio(com.here.android.mpa.guidance.NavigationManager.AspectRatio a) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        this.navigationManager.addRealisticViewAspectRatio(a);
    }
    
    public void addRealisticViewListener(java.lang.ref.WeakReference a) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        this.navigationManager.addRealisticViewListener(a);
    }
    
    public void addTrafficRerouteListener(java.lang.ref.WeakReference a) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        this.navigationManager.addTrafficRerouteListener(a);
    }
    
    public void arrivedAtDestination() {
        synchronized(this) {
            com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
            sLogger.v(new StringBuilder().append("arrivedAtDestination state[").append(this.state).append("]").toString());
            if (this.state != com.navdy.hud.app.maps.here.HereNavController$State.NAVIGATING) {
                sLogger.v(new StringBuilder().append("arrivedAtDestination: not navigating:").append(this.state).toString());
            } else {
                this.navigationQualityTracker.trackTripEnded(this.getElapsedDistance());
                this.expireGlympseTickets();
                this.navigationManager.stop();
                com.here.android.mpa.guidance.NavigationManager.Error a = this.navigationManager.startTracking();
                sLogger.v(new StringBuilder().append("arrivedAtDestination: tracking error state =").append(a).toString());
            }
        }
        /*monexit(this)*/;
    }
    
    public void endTrip() {
        sLogger.v("trip ended");
        this.bus.post(TRIP_END);
    }
    
    void expireGlympseTickets() {
        this.handler.post((Runnable)new com.navdy.hud.app.maps.here.HereNavController$3(this));
    }
    
    public com.here.android.mpa.routing.Maneuver getAfterNextManeuver() {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        return this.navigationManager.getAfterNextManeuver();
    }
    
    public long getDestinationDistance() {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        return (this.state != com.navdy.hud.app.maps.here.HereNavController$State.TRACKING) ? this.navigationManager.getDestinationDistance() : 0L;
    }
    
    public long getElapsedDistance() {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        return this.navigationManager.getElapsedDistance();
    }
    
    public java.util.Date getEta(boolean b, com.here.android.mpa.routing.Route.TrafficPenaltyMode a) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        return this.navigationManager.getEta(b, a);
    }
    
    public com.here.android.mpa.routing.Maneuver getNextManeuver() {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        return this.navigationManager.getNextManeuver();
    }
    
    public long getNextManeuverDistance() {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        return this.navigationManager.getNextManeuverDistance();
    }
    
    public com.navdy.hud.app.maps.here.HereNavController$State getState() {
        return this.state;
    }
    
    public com.here.android.mpa.routing.RouteTta getTta(com.here.android.mpa.routing.Route.TrafficPenaltyMode a, boolean b) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        return this.navigationManager.getTta(a, b);
    }
    
    public java.util.Date getTtaDate(boolean b, com.here.android.mpa.routing.Route.TrafficPenaltyMode a) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        com.here.android.mpa.routing.RouteTta a0 = this.navigationManager.getTta(a, b);
        java.util.Date a1 = null;
        if (a0 != null) {
            int i = a0.getDuration();
            a1 = null;
            if (i > 0) {
                a1 = new java.util.Date(System.currentTimeMillis() + java.util.concurrent.TimeUnit.SECONDS.toMillis((long)i));
            }
        }
        return a1;
    }
    
    public void initialize() {
        synchronized(this) {
            if (!this.initialized) {
                this.initialized = true;
                this.state = com.navdy.hud.app.maps.here.HereNavController$State.TRACKING;
                com.here.android.mpa.guidance.NavigationManager.Error a = this.navigationManager.startTracking();
                sLogger.v(new StringBuilder().append("initialize state:").append(this.state).append(" error = ").append(a).toString());
                this.startTrip();
            }
        }
        /*monexit(this)*/;
    }
    
    public boolean isInitialized() {
        boolean b = false;
        synchronized(this) {
            b = this.initialized;
        }
        /*monexit(this)*/;
        return b;
    }
    
    public void pause() {
        synchronized(this) {
            com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
            this.navigationManager.pause();
        }
        /*monexit(this)*/;
    }
    
    public void removeLaneInfoListener(com.here.android.mpa.guidance.NavigationManager.LaneInformationListener a) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        this.navigationManager.removeLaneInformationListener(a);
    }
    
    public void removeRealisticViewListener(com.here.android.mpa.guidance.NavigationManager.RealisticViewListener a) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        this.navigationManager.removeRealisticViewListener(a);
    }
    
    public void removeTrafficRerouteListener(com.here.android.mpa.guidance.NavigationManager.TrafficRerouteListener a) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        this.navigationManager.removeTrafficRerouteListener(a);
    }
    
    public com.here.android.mpa.guidance.NavigationManager.Error resume() {
        com.here.android.mpa.guidance.NavigationManager.Error a = null;
        synchronized(this) {
            com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
            a = this.navigationManager.resume();
        }
        /*monexit(this)*/;
        return a;
    }
    
    public void setDistanceUnit(com.here.android.mpa.guidance.NavigationManager.UnitSystem a) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        this.navigationManager.setDistanceUnit(a);
    }
    
    public void setRealisticViewMode(com.here.android.mpa.guidance.NavigationManager.RealisticViewMode a) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        this.navigationManager.setRealisticViewMode(a);
    }
    
    public com.here.android.mpa.guidance.NavigationManager.Error setRoute(com.here.android.mpa.routing.Route a) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        return this.navigationManager.setRoute(a);
    }
    
    public void setTrafficAvoidanceMode(com.here.android.mpa.guidance.NavigationManager.TrafficAvoidanceMode a) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        this.navigationManager.setTrafficAvoidanceMode(a);
    }
    
    public com.here.android.mpa.guidance.NavigationManager.Error startNavigation(com.here.android.mpa.routing.Route a, int i) {
        com.here.android.mpa.guidance.NavigationManager.Error a0 = null;
        synchronized(this) {
            com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
            sLogger.v(new StringBuilder().append("startNavigation state[").append(this.state).append("]").toString());
            sLogger.v(new StringBuilder().append("startNavigation simulationSpeed:").append(i).append(" route-id=").append(System.identityHashCode(a)).toString());
            if (this.state == com.navdy.hud.app.maps.here.HereNavController$State.NAVIGATING) {
                sLogger.v("startNavigation stop existing nav");
                this.stopNavigation(false);
            }
            long j = android.os.SystemClock.elapsedRealtime();
            if (i == 0) {
                i = com.navdy.hud.app.maps.MapSettings.getSimulationSpeed();
            }
            a0 = (i <= 0) ? this.navigationManager.startNavigation(a) : this.navigationManager.simulate(a, (long)i);
            sLogger.v(new StringBuilder().append("startNavigation took [").append(android.os.SystemClock.elapsedRealtime() - j).append("]").toString());
            if (a0 != com.here.android.mpa.guidance.NavigationManager.Error.NONE) {
                sLogger.e(new StringBuilder().append("startNavigation error [").append(this.state).append("] ").append(a0.name()).toString());
            } else {
                this.state = com.navdy.hud.app.maps.here.HereNavController$State.NAVIGATING;
                this.route = a;
                sLogger.v(new StringBuilder().append("startNavigation: success [").append(this.state).append("] route-id=").append(System.identityHashCode(a)).toString());
                this.endTrip();
                this.startTrip();
                long j0 = (long)a.getTta(com.here.android.mpa.routing.Route.TrafficPenaltyMode.OPTIMAL, 268435455).getDuration();
                long j1 = (long)a.getTta(com.here.android.mpa.routing.Route.TrafficPenaltyMode.DISABLED, 268435455).getDuration();
                long j2 = (long)a.getLength();
                this.navigationQualityTracker.trackTripStarted(j0, j1, j2);
                com.navdy.hud.app.analytics.AnalyticsSupport.recordNavigation(true);
            }
        }
        /*monexit(this)*/;
        return a0;
    }
    
    public void stopNavigation(boolean b) {
        synchronized(this) {
            com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
            sLogger.v(new StringBuilder().append("stopNavigation state[").append(this.state).append("]").toString());
            if (this.state != com.navdy.hud.app.maps.here.HereNavController$State.NAVIGATING) {
                sLogger.v(new StringBuilder().append("stopNavigation: already ").append(this.state).toString());
            } else {
                long j = android.os.SystemClock.elapsedRealtime();
                sLogger.v(new StringBuilder().append("stopNavigation:stopping nav state[").append(this.state).append(" ] route-id=").append(System.identityHashCode(this.route)).toString());
                boolean b0 = this.navigationManager.getNavigationMode() == com.here.android.mpa.guidance.NavigationManager.NavigationMode.SIMULATION;
                this.expireGlympseTickets();
                this.navigationManager.stop();
                if (b0) {
                    this.handler.postDelayed(this.clearAnimatorState, 2000L);
                }
                sLogger.v(new StringBuilder().append("stopNavigation:stopped nav state[").append(this.state).append(" ] took [").append(android.os.SystemClock.elapsedRealtime() - j).append("]").toString());
                com.here.android.mpa.guidance.NavigationManager.Error a = this.navigationManager.startTracking();
                sLogger.v(new StringBuilder().append("stopNavigation: tracking error state =").append(a).toString());
                this.state = com.navdy.hud.app.maps.here.HereNavController$State.TRACKING;
                com.navdy.hud.app.analytics.AnalyticsSupport.recordNavigation(false);
                if (b) {
                    this.endTrip();
                    this.startTrip();
                }
                this.route = null;
                this.navigationQualityTracker.cancelTrip();
            }
        }
        /*monexit(this)*/;
    }
}
