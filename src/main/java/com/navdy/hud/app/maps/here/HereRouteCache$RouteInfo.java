package com.navdy.hud.app.maps.here;

public class HereRouteCache$RouteInfo {
    public com.here.android.mpa.routing.Route route;
    public com.navdy.service.library.events.navigation.NavigationRouteRequest routeRequest;
    public com.navdy.service.library.events.navigation.NavigationRouteResult routeResult;
    public com.here.android.mpa.common.GeoCoordinate routeStartPoint;
    public boolean traffic;
    
    public HereRouteCache$RouteInfo(com.here.android.mpa.routing.Route a, com.navdy.service.library.events.navigation.NavigationRouteRequest a0, com.navdy.service.library.events.navigation.NavigationRouteResult a1, boolean b, com.here.android.mpa.common.GeoCoordinate a2) {
        this.route = a;
        this.routeRequest = a0;
        this.routeResult = a1;
        this.traffic = b;
        this.routeStartPoint = a2;
    }
}
