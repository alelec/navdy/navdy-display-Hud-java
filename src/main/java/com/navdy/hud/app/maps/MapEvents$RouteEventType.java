package com.navdy.hud.app.maps;

public enum MapEvents$RouteEventType {
    STARTED(0),
    FINISHED(1),
    FAILED(2);

    private int value;
    MapEvents$RouteEventType(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }

}

//final public class MapEvents$RouteEventType extends Enum {
//    final private static com.navdy.hud.app.maps.MapEvents$RouteEventType[] $VALUES;
//    final public static com.navdy.hud.app.maps.MapEvents$RouteEventType FAILED;
//    final public static com.navdy.hud.app.maps.MapEvents$RouteEventType FINISHED;
//    final public static com.navdy.hud.app.maps.MapEvents$RouteEventType STARTED;
//
//    static {
//        STARTED = new com.navdy.hud.app.maps.MapEvents$RouteEventType("STARTED", 0);
//        FINISHED = new com.navdy.hud.app.maps.MapEvents$RouteEventType("FINISHED", 1);
//        FAILED = new com.navdy.hud.app.maps.MapEvents$RouteEventType("FAILED", 2);
//        com.navdy.hud.app.maps.MapEvents$RouteEventType[] a = new com.navdy.hud.app.maps.MapEvents$RouteEventType[3];
//        a[0] = STARTED;
//        a[1] = FINISHED;
//        a[2] = FAILED;
//        $VALUES = a;
//    }
//
//    private MapEvents$RouteEventType(String s, int i) {
//        super(s, i);
//    }
//
//    public static com.navdy.hud.app.maps.MapEvents$RouteEventType valueOf(String s) {
//        return (com.navdy.hud.app.maps.MapEvents$RouteEventType)Enum.valueOf(com.navdy.hud.app.maps.MapEvents$RouteEventType.class, s);
//    }
//
//    public static com.navdy.hud.app.maps.MapEvents$RouteEventType[] values() {
//        return $VALUES.clone();
//    }
//}
