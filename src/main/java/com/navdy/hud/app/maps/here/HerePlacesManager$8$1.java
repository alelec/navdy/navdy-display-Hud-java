package com.navdy.hud.app.maps.here;

class HerePlacesManager$8$1 implements Runnable {
    final com.navdy.hud.app.maps.here.HerePlacesManager$8 this$0;
    final com.here.android.mpa.search.ErrorCode val$error;
    final com.here.android.mpa.search.Place val$place;
    
    HerePlacesManager$8$1(com.navdy.hud.app.maps.here.HerePlacesManager$8 a, com.here.android.mpa.search.ErrorCode a0, com.here.android.mpa.search.Place a1) {

        super();
        this.this$0 = a;
        this.val$error = a0;
        this.val$place = a1;
    }
    
    public void run() {
        try {
            int i = this.this$0.val$counter.decrementAndGet();
            com.here.android.mpa.search.ErrorCode a = this.val$error;
            com.here.android.mpa.search.ErrorCode a0 = com.here.android.mpa.search.ErrorCode.NONE;
            label2: {
                label0: {
                    label1: {
                        if (a != a0) {
                            break label1;
                        }
                        if (this.val$place != null) {
                            break label0;
                        }
                    }
                    com.navdy.hud.app.maps.here.HerePlacesManager.access$000().e(new StringBuilder().append("Error in place detail response: ").append(this.val$error.name()).toString());
                    break label2;
                }
                com.navdy.hud.app.maps.here.HerePlacesManager.access$000().v(new StringBuilder().append("Found a Place: ").append(this.val$place.getName()).toString());
                Object a1 = this.val$place.getCategories().iterator();
                while(((java.util.Iterator)a1).hasNext()) {
                    com.here.android.mpa.search.Category a2 = (com.here.android.mpa.search.Category)((java.util.Iterator)a1).next();
                    com.navdy.hud.app.maps.here.HerePlacesManager.access$000().v(new StringBuilder().append("place category: ").append(a2.getName()).toString());
                }
                this.this$0.val$places.add(this.val$place);
            }
            if (i == 0) {
                com.navdy.hud.app.maps.here.HerePlacesManager.access$1200(this.this$0.val$places, this.this$0.val$listener);
            }
        } catch(Throwable a3) {
            com.navdy.hud.app.maps.here.HerePlacesManager.access$000().e("HERE internal PlaceLink.getDetailsRequest callback exception: ", a3);
            if (this.this$0.val$counter.get() == 0) {
                com.navdy.hud.app.maps.here.HerePlacesManager.access$1200(this.this$0.val$places, this.this$0.val$listener);
            }
        }
    }
}
