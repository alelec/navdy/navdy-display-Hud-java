package com.navdy.hud.app.device.gps;

import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.HandlerThread;
import android.os.Message;
import android.os.SystemClock;
import android.text.TextUtils;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.event.DrivingStateChange;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.manager.SpeedManager.SpeedUnit;
import com.navdy.hud.app.obd.ObdManager;
import com.navdy.hud.app.obd.ObdManager.ObdConnectionStatusEvent;
import com.navdy.hud.app.obd.ObdManager.ObdSupportedPidsChangedEvent;
import com.navdy.hud.app.storage.db.helper.VinInformationHelper;
import com.navdy.hud.app.util.GForceRawSensorDataProcessor;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.concurrent.atomic.AtomicInteger;

import org.json.JSONException;
import org.json.JSONObject;

public class GpsDeadReckoningManager implements Runnable {
    public static final Logger sLogger = new Logger(GpsDeadReckoningManager.class);
    private static final byte TIME_ONLY = 5;
    private static final byte DEAD_RECKONING_ONLY = 1;
    private static final byte NO_FIX = 0;
    private static final byte FIX_2D = 2;
    private static final byte FIX_3D = 3;
    private static final int FUSION_DONE = 1;
    private static final byte GPS_DEAD_RECKONING_COMBINED = 4;
    private static final int ALIGNMENT_DONE = 3;
    private static final int ALIGNMENT_PACKET_LENGTH = 24;
    private static final int ALIGNMENT_PACKET_PAYLOAD_LENGTH = 16;
    private static final byte[] ALIGNMENT_PATTERN = {-75, 98, 16, 20};
    private static final long ALIGNMENT_POLL_FREQUENCY = 30000;
    private static final String ALIGNMENT_TIME = "alignment_time";
    private static final byte[] AUTO_ALIGNMENT = {-75, 98, 6, 86, 12, NO_FIX, NO_FIX, DEAD_RECKONING_ONLY, NO_FIX, NO_FIX, NO_FIX, NO_FIX, NO_FIX, NO_FIX, NO_FIX, NO_FIX, NO_FIX, NO_FIX, 105, 29};
    /* access modifiers changed from: private */
    public static final byte[] CFG_ESF_RAW = {-75, 98, 6, DEAD_RECKONING_ONLY, FIX_3D, NO_FIX, 16, FIX_3D, DEAD_RECKONING_ONLY, NO_FIX, NO_FIX};
    private static final byte[] CFG_ESF_RAW_OFF = {-75, 98, 6, DEAD_RECKONING_ONLY, FIX_3D, NO_FIX, 16, FIX_3D, NO_FIX, NO_FIX, NO_FIX};
    private static final byte[] CFG_RST_COLD = {-75, 98, 6, GPS_DEAD_RECKONING_COMBINED, GPS_DEAD_RECKONING_COMBINED, NO_FIX, -1, -1, FIX_2D, NO_FIX, 14, 97};
    /* access modifiers changed from: private */
    public static final byte[] CFG_RST_WARM = {-75, 98, 6, GPS_DEAD_RECKONING_COMBINED, GPS_DEAD_RECKONING_COMBINED, NO_FIX, DEAD_RECKONING_ONLY, NO_FIX, FIX_2D, NO_FIX, 17, 108};
    private static final int ESF_PACKET_LENGTH = 19;
    private static final byte[] ESF_RAW_PATTERN = {-75, 98, 16, FIX_3D};
    private static final byte[] ESF_STATUS_PATTERN = {-75, 98, 16, 16};
    private static final int FAILURE_RETRY_INTERVAL = 10000;
    /* access modifiers changed from: private */
    public static final byte[] GET_ALIGNMENT = {-75, 98, 16, 20, NO_FIX, NO_FIX, 36, 124};
    /* access modifiers changed from: private */
    public static final byte[] GET_ESF_STATUS = {-75, 98, 16, 16, NO_FIX, NO_FIX, 32, 112};
    private static final String GPS = "gps";
    private static final String GPS_LOG = "gps.log";
    private static final char[] HEX = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    private static final byte[] INJECT_OBD_SPEED = {-75, 98, 16, FIX_2D, 12, NO_FIX, NO_FIX, NO_FIX, NO_FIX, NO_FIX, NO_FIX, NO_FIX, NO_FIX, NO_FIX, NO_FIX, NO_FIX, NO_FIX, 11, NO_FIX, NO_FIX};
    private static final int MSG_ALIGNMENT_VALUE = 1;
    private static final byte[] NAV_STATUS_PATTERN = {-75, 98, DEAD_RECKONING_ONLY, FIX_3D};
    private static final byte[] PASSPHRASE = {110, 97, 118, 100, 121};
    private static final String PITCH = "pitch";
    private static final long POLL_FREQUENCY = 10000;
    private static final byte[] READ_BUF = new byte[8192];
    private static final String ROLL = "roll";
    private static final int SPEED_INJECTION_FREQUENCY = 100;
    private static final int SPEED_TIME_TAG_COUNTER = 100;
    private static final String TCP_SERVER_HOST = "127.0.0.1";
    private static final int TCP_SERVER_PORT = 42434;
    private static final byte[] TEMP_BUF = new byte[128];
    private static final AtomicInteger THREAD_COUNTER = new AtomicInteger(1);
    private static final String YAW = "yaw";
    private static final GpsDeadReckoningManager sInstance = new GpsDeadReckoningManager();
    /* access modifiers changed from: private */
    private volatile boolean alignmentChecked;
    private Alignment alignmentInfo;
    /* access modifiers changed from: private */
    public final Bus bus;
    /* access modifiers changed from: private */
    public volatile boolean deadReckoningInjectionStarted;
    private boolean deadReckoningOn;
    private byte deadReckoningType = ((byte) -1);
    private Runnable enableEsfRunnable = new Runnable() {
        public void run() {
            GpsDeadReckoningManager.this.invokeUblox(GpsDeadReckoningManager.CFG_ESF_RAW, "ESF-RAW");
            GpsDeadReckoningManager.this.sensorDataProcessor.setCalibrated(false);
            GpsDeadReckoningManager.this.bus.post(new CalibratedGForceData(0.0f, 0.0f, 0.0f));
        }
    };
    /* access modifiers changed from: private */
    public final Runnable getAlignmentRunnable = new Runnable() {
        public void run() {
            if (GpsDeadReckoningManager.this.invokeUblox(GpsDeadReckoningManager.GET_ALIGNMENT, "get alignment")) {
                GpsDeadReckoningManager.this.handler.removeCallbacks(GpsDeadReckoningManager.this.getAlignmentRunnableRetry);
                GpsDeadReckoningManager.this.handler.postDelayed(GpsDeadReckoningManager.this.getAlignmentRunnableRetry, GpsDeadReckoningManager.ALIGNMENT_POLL_FREQUENCY);
                return;
            }
            GpsDeadReckoningManager.this.postAlignmentRunnable(true);
        }
    };
    /* access modifiers changed from: private */
    public final Runnable getAlignmentRunnableRetry = new Runnable() {
        public void run() {
            GpsDeadReckoningManager.sLogger.v("getAlignmentRunnableRetry");
            GpsDeadReckoningManager.this.getAlignmentRunnable.run();
        }
    };
    /* access modifiers changed from: private */
    public final Runnable getFusionStatusRunnable = new Runnable() {
        public void run() {
            if (GpsDeadReckoningManager.this.invokeUblox(GpsDeadReckoningManager.GET_ESF_STATUS, "get fusion status")) {
                GpsDeadReckoningManager.this.handler.removeCallbacks(GpsDeadReckoningManager.this.getFusionStatusRunnableRetry);
                GpsDeadReckoningManager.this.handler.postDelayed(GpsDeadReckoningManager.this.getFusionStatusRunnableRetry, GpsDeadReckoningManager.POLL_FREQUENCY);
                return;
            }
            GpsDeadReckoningManager.this.postFusionRunnable(true);
        }
    };
    /* access modifiers changed from: private */
    public final Runnable getFusionStatusRunnableRetry = new Runnable() {
        public void run() {
            GpsDeadReckoningManager.sLogger.v("getFusionStatusRunnableRetry");
            GpsDeadReckoningManager.this.getFusionStatusRunnable.run();
        }
    };
    /* access modifiers changed from: private */
    public Handler handler;
    private HandlerThread handlerThread;
    private final Runnable injectRunnable = new Runnable() {
        public void run() {
            GpsDeadReckoningManager.this.invokeUblox(GpsDeadReckoningManager.this.injectSpeed);
            if (!GpsDeadReckoningManager.this.deadReckoningInjectionStarted || GpsDeadReckoningManager.this.lastConnectionFailure != 0) {
                GpsDeadReckoningManager.this.handler.removeCallbacks(this);
            } else {
                GpsDeadReckoningManager.this.handler.postDelayed(this, 100);
            }
        }
    };
    /* access modifiers changed from: private */
    public final CommandWriter injectSpeed = new CommandWriter() {
        public void run() throws IOException {
            GpsDeadReckoningManager.this.sendObdSpeed();
        }
    };
    private InputStream inputStream;
    /* access modifiers changed from: private */
    public long lastConnectionFailure;
    /* access modifiers changed from: private */
    public OutputStream outputStream;
    private Runnable resetRunnable = new Runnable() {
        public void run() {
            GpsDeadReckoningManager.this.invokeUblox(GpsDeadReckoningManager.CFG_RST_WARM, "RST-WARM (warm reset)");
        }
    };
    private JSONObject rootInfo;
    private volatile boolean runThread;
    /* access modifiers changed from: private */
    public GForceRawSensorDataProcessor sensorDataProcessor;
    private Socket socket;
    private final SpeedManager speedManager = SpeedManager.getInstance();
    private Thread thread;
    private int timeStampCounter = 0;
    /* access modifiers changed from: private */
    public boolean waitForAutoAlignment;
    /* access modifiers changed from: private */
    public boolean waitForFusionStatus;

    private static class Alignment {
        boolean done;
        float pitch;
        float roll;
        float yaw;

        Alignment(float f, float f2, float f3, boolean z) {
            this.yaw = f;
            this.pitch = f2;
            this.roll = f3;
            this.done = z;
        }

        public String toString() {
            String sb = "Alignment{" + "yaw=" + this.yaw +
                    ", pitch=" + this.pitch +
                    ", roll=" + this.roll +
                    ", done=" + this.done +
                    '}';
            return sb;
        }
    }

    interface CommandWriter {
        void run() throws IOException;
    }

    static {
        calculateChecksum(CFG_ESF_RAW);
        calculateChecksum(CFG_ESF_RAW_OFF);
    }

    private GpsDeadReckoningManager() {
        sLogger.v("ctor()");
        this.sensorDataProcessor = new GForceRawSensorDataProcessor();
        this.handlerThread = new HandlerThread("gpsDeadReckoningHandler");
        this.handlerThread.start();
        this.handler = new Handler(this.handlerThread.getLooper(), new Callback() {
            public boolean handleMessage(Message message) {
                /*1*/
                if (message.what == GpsNmeaParser.FIX_TYPE_2D_3D) {
                    try {
                        GpsDeadReckoningManager.this.handleAutoAlignmentResult((Alignment) message.obj);
                        return false;
                    } catch (Throwable th) {
                        GpsDeadReckoningManager.sLogger.e(th);
                        if (GpsDeadReckoningManager.this.waitForAutoAlignment) {
                            if (GpsDeadReckoningManager.this.waitForFusionStatus) {
                                GpsDeadReckoningManager.this.postFusionRunnable(true);
                                return false;
                            }
                        } else {
                            GpsDeadReckoningManager.this.postAlignmentRunnable(true);
                            return false;
                        }
                    }
                }
                return false;
            }
        });
        ObdManager instance = ObdManager.getInstance();
        if (instance.isConnected()) {
            sLogger.v("ctor() obd is connected");
            if (instance.isSpeedPidAvailable()) {
                sLogger.v("ctor() speed pid is available");
                checkAlignment();
                startDeadReckoning();
            } else {
                sLogger.v("ctor() speed pid is not available");
            }
        } else {
            sLogger.v("ctor() obd is not connected, waiting for obd to connect");
        }
        this.bus = RemoteDeviceManager.getInstance().getBus();
        this.bus.register(this);
        sLogger.v("ctor() initialized");
        this.handler.post(this.enableEsfRunnable);
    }

    /* access modifiers changed from: private */
    public static String bytesToHex(byte[] bArr, int i, int i2) {
        char[] cArr = new char[(i2 * 2)];
        for (int i3 = 0; i3 < i2; i3++) {
            int b = bArr[i3 + i] & 0xFF;
            cArr[i3 * 2] = HEX[b >>> GPS_DEAD_RECKONING_COMBINED];
            cArr[(i3 * 2) + 1] = HEX[b & 15];
        }
        return new String(cArr);
    }

    private static void calculateChecksum(byte[] bArr) {
        byte b = NO_FIX;
        int length = bArr.length;
        int i = 2;
        byte b2 = 0;
        while (true) {
            byte b3 = b;
            if (i < length - 2) {
                b = (byte) (bArr[i] + b3);
                b2 = (byte) (b2 + b);
                i++;
            } else {
                bArr[length - 2] = (byte) b3;
                bArr[length - 1] = (byte) b2;
                return;
            }
        }
    }

    private void checkAlignment() {
        if (!this.alignmentChecked) {
            sLogger.v("checking alignment");
            postAlignmentRunnable(false);
            return;
        }
        sLogger.v("alignment already checked");
    }

    private void closeSocket() {
        this.runThread = false;
        IOUtils.closeStream(this.inputStream);
        IOUtils.closeStream(this.outputStream);
        IOUtils.closeStream(this.socket);
        this.inputStream = null;
        this.outputStream = null;
        this.socket = null;
        if (this.thread != null) {
            try {
                if (this.thread.isAlive()) {
                    this.thread.interrupt();
                    sLogger.v("waiting for thread");
                    this.thread.join();
                    sLogger.v("waited");
                } else {
                    sLogger.v("thread is not alive");
                }
            } catch (Throwable th) {
                sLogger.e(th);
            } finally {
                this.thread = null;
            }
        }
        this.deadReckoningOn = false;
        this.deadReckoningType = (byte) -1;
    }

    private boolean connectSocket() {
        try {
            if (this.socket != null) {
                return true;
            }
            this.socket = new Socket(TCP_SERVER_HOST, TCP_SERVER_PORT);
            this.inputStream = this.socket.getInputStream();
            this.outputStream = this.socket.getOutputStream();
            sLogger.v("connected to 42434");
            this.outputStream.write(PASSPHRASE);
            sLogger.v("sent passphrase");
            this.thread = new Thread(this);
            this.thread.setName("GpsDeadReckoningManager-" + THREAD_COUNTER.getAndIncrement());
            this.runThread = true;
            this.thread.start();
            GenericUtil.sleep(2000);
            return true;
        } catch (Throwable th) {
            sLogger.e(th);
            closeSocket();
            return false;
        }
    }

    private static String getDRType(byte b) {
        switch (b) {
            case 0:
                return "NO_FIX";
            case GpsNmeaParser.FIX_TYPE_2D_3D /*1*/:
                return GpsConstants.DEAD_RECKONING_ONLY;
            case GpsNmeaParser.FIX_TYPE_DIFFERENTIAL /*2*/:
                return "FIX_2D";
            case 3:
                return "FIX_3D";
            case GpsNmeaParser.FIX_TYPE_RTK /*4*/:
                return GpsConstants.GPS_DEAD_RECKONING_COMBINED;
            case 5:
                return "TIME_ONLY";
            default:
                return "UNKNOWN";
        }
    }

    public static GpsDeadReckoningManager getInstance() {
        return sInstance;
    }

    private void handleAlignment(int i, int i2) throws Throwable {
        boolean z = false;
        if (i + ALIGNMENT_PACKET_LENGTH <= i2) {
            sLogger.v("alignment raw data[" + bytesToHex(READ_BUF, i, ALIGNMENT_PACKET_LENGTH) + "]");
            ByteBuffer wrap = ByteBuffer.wrap(READ_BUF, i, ALIGNMENT_PACKET_LENGTH);
            wrap.order(ByteOrder.LITTLE_ENDIAN);
            wrap.get(TEMP_BUF, 0, ALIGNMENT_PATTERN.length);
            short s = wrap.getShort();
            if (s != 16) {
                throw new RuntimeException("len[" + s + "] expected [" + 16 + "]");
            }
            wrap.get(TEMP_BUF, 0, 4);
            byte b = wrap.get();
            byte b2 = wrap.get();
            int i3 = (b2 & 14) >> DEAD_RECKONING_ONLY;
            boolean z2 = (b2 & DEAD_RECKONING_ONLY) == 1;
            if (i3 == 3) {
                z = true;
            }
            wrap.get();
            wrap.get();
            float f = ((float) wrap.getInt()) / 100.0f;
            float f2 = ((float) wrap.getShort()) / 100.0f;
            float f3 = ((float) wrap.getShort()) / 100.0f;
            Message obtain = Message.obtain();
            obtain.what = 1;
            Alignment alignment = new Alignment(f, f2, f3, z);
            obtain.obj = alignment;
            sLogger.v("Alignment bitField[" + b2 + "] alignment[" + i3 + "] autoAlignment[" + z2 + "] version[" + b + "] " + alignment);
            this.handler.removeCallbacks(this.getAlignmentRunnableRetry);
            this.handler.sendMessage(obtain);
            return;
        }
        postAlignmentRunnable(true);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0147  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void handleAutoAlignmentResult(Alignment alignment) {
        boolean alignmentRequired;
        String string;
        if (!this.waitForAutoAlignment) {
            sLogger.v("waiting for align:false");
            String vin = ObdManager.getInstance().getVin();
            if (vin == null) {
                vin = "UNKNOWN_VIN";
            }
            sLogger.v("Vin is " + vin);
            String vinInfo = VinInformationHelper.getVinInfo(vin);
            if (TextUtils.isEmpty(vinInfo)) {
                sLogger.v("no info found for vin,  need auto alignment");
                alignmentRequired = true;
            } else {
//                try {
                    sLogger.v("VinInfo is " + vinInfo);
                    if (alignment.yaw == 0.0f && alignment.pitch == 0.0f && alignment.roll == 0.0f) {
                        sLogger.w("VinInfo exists but u-blox alignment is lost, trigger alignment");
                        AnalyticsSupport.localyticsSendEvent("GPS_Calibration_Lost", new String[0]);
//                        try {
                            alignmentRequired = true;
                            VinInformationHelper.deleteVinInfo(vin);
//                        } catch (Throwable th) {
//                            th = th;
//                            alignmentRequired = true;
//                            sLogger.e(th);
//                            string = VinInformationHelper.getVinPreference().getString("vin", null);
//                            sLogger.v("last vin:" + string + " current vin:" + vin);
//                            if (!TextUtils.isEmpty(string)) {
//                            }
//                            sLogger.v("vin switch not detected");
//                            if (alignmentRequired) {
//                            }
//                        }
                    } else {
                        try {
                            this.rootInfo = new JSONObject(vinInfo);
                            long parseLong = Long.parseLong(this.rootInfo.getJSONObject(GPS).getString(ALIGNMENT_TIME));
                            sLogger.v("elapsed=" + (System.currentTimeMillis() - parseLong));
                            alignmentRequired = false;
                        } catch (JSONException e) {
                            sLogger.e(e);
                            alignmentRequired = true;
                        }
                    }
//                } catch (Throwable th2) {
////                    th = th2;
//                    alignmentRequired = false;
//                    sLogger.e(th);
//                    string = VinInformationHelper.getVinPreference().getString("vin", null);
//                    sLogger.v("last vin:" + string + " current vin:" + vin);
//                    if (!TextUtils.isEmpty(string)) {
//                    }
//                    sLogger.v("vin switch not detected");
//                    if (alignmentRequired) {
//                    }
//                }
            }
            string = VinInformationHelper.getVinPreference().getString("vin", null);
            sLogger.v("last vin:" + string + " current vin:" + vin);
            if (!TextUtils.isEmpty(string) || TextUtils.equals(string, vin)) {
                sLogger.v("vin switch not detected");
            } else {
                sLogger.v("vin has switched: trigger auto alignment");
                AnalyticsSupport.localyticsSendEvent("GPS_Calibration_VinSwitch", new String[0]);
                alignmentRequired = true;
            }
            if (alignmentRequired) {
                sLogger.v("alignment required");
                VinInformationHelper.getVinPreference().edit().remove("vin").commit();
                sLogger.v("last pref removed");
                if (!invokeUblox(new CommandWriter() {
                    public void run() throws IOException {
                        GpsDeadReckoningManager.this.sendAutoAlignment();
                    }
                })) {
                    postAlignmentRunnable(true);
                    return;
                }
                return;
            }
            this.alignmentChecked = true;
            sLogger.v("alignment not reqd");
            TTSUtils.debugShowGpsSensorCalibrationNotNeeded();
        } else if (!alignment.done) {
            sLogger.v("alignment not done yet, try again");
            postAlignmentRunnable(true);
        } else {
            this.alignmentInfo = alignment;
            AnalyticsSupport.localyticsSendEvent("GPS_Calibration_IMU_Done", new String[0]);
            TTSUtils.debugShowGpsImuCalibrationDone();
            this.waitForAutoAlignment = false;
            this.waitForFusionStatus = true;
            postFusionRunnable(false);
            sLogger.v("got alignment, wait for fusion status");
        }
    }

    private void handleEsfRaw(int i, int i2) {
        this.sensorDataProcessor.onRawData(new RawSensorData(READ_BUF, i));
        if (this.sensorDataProcessor.isCalibrated()) {
            this.bus.post(new CalibratedGForceData(this.sensorDataProcessor.xAccel, this.sensorDataProcessor.yAccel, this.sensorDataProcessor.zAccel));
        }
    }

    private void handleFusion(int i, int i2) throws Throwable {
        if (i + ESF_PACKET_LENGTH <= i2) {
            sLogger.v("esf raw data[" + bytesToHex(READ_BUF, i, ESF_PACKET_LENGTH) + "]");
            ByteBuffer wrap = ByteBuffer.wrap(READ_BUF, i, ESF_PACKET_LENGTH);
            wrap.order(ByteOrder.LITTLE_ENDIAN);
            wrap.get(TEMP_BUF, 0, ALIGNMENT_PATTERN.length);
            short s = wrap.getShort();
            wrap.get(TEMP_BUF, 0, 4);
            byte b = wrap.get();
            byte b2 = wrap.get();
            byte b3 = wrap.get();
            wrap.get(TEMP_BUF, 0, 5);
            byte b4 = wrap.get();
            this.handler.removeCallbacks(this.getFusionStatusRunnableRetry);
            if (b4 != 1) {
                sLogger.v("Fusion not done retry len[" + s + "] fusionMode[" + b4 + "] initStatus1[" + b2 + "] initStatus2[" + b3 + "] version[" + b + "]");
                postFusionRunnable(true);
                return;
            }
            sLogger.v("Fusion Done len[" + s + "] fusionMode[" + b4 + "] initStatus1[" + b2 + "] initStatus2[" + b3 + "] version[" + b + "]");
            storeVinInfoInDb();
            return;
        }
        postFusionRunnable(true);
    }

    private void handleNavStatus(int i, int i2) throws Throwable {
        int i3 = i + 10;
        if (i3 <= i2 - 1) {
            byte b = READ_BUF[i3];
            switch (b) {
                case GpsNmeaParser.FIX_TYPE_2D_3D /*1*/:
                case GpsNmeaParser.FIX_TYPE_RTK /*4*/:
                    if (!this.deadReckoningOn) {
                        this.deadReckoningOn = true;
                        this.deadReckoningType = b;
                        String dRType = getDRType(b);
                        TTSUtils.debugShowDRStarted(dRType);
                        sLogger.v("dead reckoning on[" + b + "] " + dRType);
                        Bundle bundle = new Bundle();
                        bundle.putString(GpsConstants.GPS_EXTRA_DR_TYPE, b == 1 ? GpsConstants.DEAD_RECKONING_ONLY : GpsConstants.GPS_DEAD_RECKONING_COMBINED);
                        GpsUtils.sendEventBroadcast(GpsConstants.GPS_EVENT_DEAD_RECKONING_STARTED, bundle);
                        return;
                    } else if (this.deadReckoningType != b) {
                        String dRType2 = getDRType(b);
                        sLogger.v("dead reckoning type changed from [" + this.deadReckoningType + "] to [" + b + "] " + dRType2);
                        this.deadReckoningType = b;
                        TTSUtils.debugShowDRStarted(dRType2);
                        Bundle bundle2 = new Bundle();
                        bundle2.putString(GpsConstants.GPS_EXTRA_DR_TYPE, b == 1 ? GpsConstants.DEAD_RECKONING_ONLY : GpsConstants.GPS_DEAD_RECKONING_COMBINED);
                        GpsUtils.sendEventBroadcast(GpsConstants.GPS_EVENT_DEAD_RECKONING_STARTED, bundle2);
                        return;
                    } else {
                        return;
                    }
                default:
                    if (this.deadReckoningOn) {
                        this.deadReckoningOn = false;
                        this.deadReckoningType = (byte) -1;
                        TTSUtils.debugShowDREnded();
                        sLogger.v("dead reckoning stopped:" + b);
                        GpsUtils.sendEventBroadcast(GpsConstants.GPS_EVENT_DEAD_RECKONING_STOPPED, null);
                        return;
                    }
                    return;
            }
        }
    }

    private void initDeadReckoning() {
        if (ObdManager.getInstance().isSpeedPidAvailable()) {
            sLogger.v("speed pid is available");
            checkAlignment();
            startDeadReckoning();
            return;
        }
        sLogger.v("speed pid is not available");
    }

    /* access modifiers changed from: private */
    public boolean invokeUblox(CommandWriter commandWriter) {
        try {
            if (this.lastConnectionFailure > 0) {
                long elapsedRealtime = SystemClock.elapsedRealtime() - this.lastConnectionFailure;
                if (elapsedRealtime < POLL_FREQUENCY) {
                    if (!sLogger.isLoggable(2)) {
                        return false;
                    }
                    sLogger.v("would retry time[" + elapsedRealtime + "]");
                    return false;
                }
            }
            if (connectSocket()) {
                this.lastConnectionFailure = 0;
                commandWriter.run();
                return true;
            }
            this.lastConnectionFailure = SystemClock.elapsedRealtime();
            return false;
        } catch (IOException e) {
            sLogger.e("Failed to ");
            closeSocket();
            this.lastConnectionFailure = SystemClock.elapsedRealtime();
            return false;
        }
    }

    /* access modifiers changed from: private */
    public boolean invokeUblox(final byte[] bArr, final String str) {
        return invokeUblox(new CommandWriter() {
            public void run() throws IOException {
                if (GpsDeadReckoningManager.this.outputStream != null) {
                    if (str != null) {
                        GpsDeadReckoningManager.sLogger.v(str + " [" + GpsDeadReckoningManager.bytesToHex(bArr, 0, bArr.length) + "]");
                    }
                    GpsDeadReckoningManager.this.outputStream.write(bArr);
                    return;
                }
                throw new IOException("Disconnected socket - failed to " + str);
            }
        });
    }

    /* access modifiers changed from: private */
    public void postAlignmentRunnable(boolean z) {
        this.handler.removeCallbacks(this.getAlignmentRunnable);
        this.handler.removeCallbacks(this.getAlignmentRunnableRetry);
        if (z) {
            this.handler.postDelayed(this.getAlignmentRunnable, ALIGNMENT_POLL_FREQUENCY);
        } else {
            this.handler.post(this.getAlignmentRunnable);
        }
    }

    /* access modifiers changed from: private */
    public void postFusionRunnable(boolean z) {
        this.handler.removeCallbacks(this.getFusionStatusRunnable);
        this.handler.removeCallbacks(this.getFusionStatusRunnableRetry);
        if (z) {
            this.handler.postDelayed(this.getFusionStatusRunnable, POLL_FREQUENCY);
        } else {
            this.handler.post(this.getFusionStatusRunnable);
        }
    }

    /* access modifiers changed from: private */
    public void sendAutoAlignment() throws IOException {
        if (!ObdManager.getInstance().isConnected()) {
            sLogger.v("not connected to obd manager any more");
            return;
        }
        sLogger.v("send auto alignment [" + bytesToHex(AUTO_ALIGNMENT, 0, AUTO_ALIGNMENT.length) + "]");
        this.waitForAutoAlignment = true;
        this.outputStream.write(AUTO_ALIGNMENT);
        postAlignmentRunnable(true);
        AnalyticsSupport.localyticsSendEvent("GPS_Calibration_Start", new String[0]);
        TTSUtils.debugShowGpsCalibrationStarted();
    }

    /* access modifiers changed from: private */
    public void sendObdSpeed() throws IOException {
        boolean isLoggable = sLogger.isLoggable(2);
        long rawObdSpeed = (long) this.speedManager.getRawObdSpeed();
        if (rawObdSpeed >= 0) {
            this.timeStampCounter += 100;
            if (this.timeStampCounter < 0) {
                this.timeStampCounter = 100;
            }
            INJECT_OBD_SPEED[6] = (byte) ((byte) (this.timeStampCounter >> 0));
            INJECT_OBD_SPEED[7] = (byte) ((byte) (this.timeStampCounter >> 8));
            INJECT_OBD_SPEED[8] = (byte) ((byte) (this.timeStampCounter >> 16));
            INJECT_OBD_SPEED[9] = (byte) ((byte) (this.timeStampCounter >> ALIGNMENT_PACKET_LENGTH));
            long convertWithPrecision = (long) ((int) (SpeedManager.convertWithPrecision((double) rawObdSpeed, SpeedUnit.KILOMETERS_PER_HOUR, SpeedUnit.METERS_PER_SECOND) * 1000.0f));
            INJECT_OBD_SPEED[14] = (byte) ((byte) ((int) (convertWithPrecision >> 0)));
            INJECT_OBD_SPEED[15] = (byte) ((byte) ((int) (convertWithPrecision >> 8)));
            INJECT_OBD_SPEED[16] = (byte) ((byte) ((int) (convertWithPrecision >> 16)));
            calculateChecksum(INJECT_OBD_SPEED);
            if (isLoggable) {
                sLogger.v("inject obd speed [" + bytesToHex(INJECT_OBD_SPEED, 0, INJECT_OBD_SPEED.length) + "]");
            }
            this.outputStream.write(INJECT_OBD_SPEED);
        } else if (isLoggable) {
            sLogger.i("invalid obd speed:" + rawObdSpeed);
        }
    }

    private void startDeadReckoning() {
        if (!this.deadReckoningInjectionStarted) {
            sLogger.v("starting dead reckoning injection");
            this.deadReckoningInjectionStarted = true;
            this.handler.removeCallbacks(this.injectRunnable);
            this.handler.postDelayed(this.injectRunnable, 100);
        }
    }

    private void stopDeadReckoning() {
        if (this.deadReckoningInjectionStarted) {
            this.waitForAutoAlignment = false;
            this.waitForFusionStatus = false;
            this.alignmentInfo = null;
            this.rootInfo = null;
            this.alignmentChecked = false;
            sLogger.v("stopping dead rekoning injection");
            this.deadReckoningInjectionStarted = false;
            this.handler.removeCallbacks(this.injectRunnable);
            this.handler.removeCallbacks(this.getAlignmentRunnable);
            this.handler.removeCallbacks(this.getAlignmentRunnableRetry);
            this.handler.removeCallbacks(this.getFusionStatusRunnable);
            this.handler.removeCallbacks(this.getFusionStatusRunnableRetry);
        }
    }

    private void storeVinInfoInDb() {
        JSONObject jSONObject;
        try {
            String vin = ObdManager.getInstance().getVin();
            String str = vin == null ? "UNKNOWN_VIN" : vin;
            sLogger.v("store alignment info in db for vin[" + str + "]");
            if (this.rootInfo == null) {
                this.rootInfo = new JSONObject();
                jSONObject = new JSONObject();
                this.rootInfo.put(GPS, jSONObject);
            } else {
                jSONObject = this.rootInfo.getJSONObject(GPS);
            }
            jSONObject.put(YAW, String.valueOf(this.alignmentInfo.yaw));
            jSONObject.put(PITCH, String.valueOf(this.alignmentInfo.pitch));
            jSONObject.put(ROLL, String.valueOf(this.alignmentInfo.roll));
            jSONObject.put(ALIGNMENT_TIME, String.valueOf(System.currentTimeMillis()));
            VinInformationHelper.storeVinInfo(str, this.rootInfo.toString());
            VinInformationHelper.getVinPreference().edit().putString("vin", str).commit();
            sLogger.v("store last vin pref [" + str + "]");
            this.waitForAutoAlignment = false;
            this.waitForFusionStatus = false;
            this.alignmentChecked = true;
            AnalyticsSupport.localyticsSendEvent("GPS_Calibration_Sensor_Done", new String[0]);
            TTSUtils.debugShowGpsSensorCalibrationDone();
        } catch (Throwable th) {
            sLogger.e(th);
        }
    }

    @Subscribe
    public void ObdConnectionStatusEvent(ObdConnectionStatusEvent obdConnectionStatusEvent) {
        if (obdConnectionStatusEvent.connected) {
            sLogger.v("got obd connected");
            initDeadReckoning();
            return;
        }
        sLogger.v("got obd dis-connected");
        stopDeadReckoning();
    }

    public void dumpGpsInfo(String str) {
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(str + File.separator + GPS_LOG);
            PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(fileOutputStream));
            ObdManager instance = ObdManager.getInstance();
            printWriter.write("obd_connected=" + instance.isConnected() + "\n");
            if (this.deadReckoningInjectionStarted) {
                String vin = instance.getVin();
                if (vin == null) {
                    vin = "UNKNOWN_VIN";
                }
                printWriter.write("vin=" + vin + "\n");
                printWriter.write("calibrated=" + (this.rootInfo == null ? "no" : this.rootInfo.toString()));
                printWriter.write("\n");
            }
            printWriter.flush();
            sLogger.i("gps log written:" + str);
        } catch (FileNotFoundException e) {
            sLogger.e("Could not open log file");
        } finally {
            IOUtils.fileSync(fileOutputStream);
            IOUtils.closeStream(fileOutputStream);
        }
    }

    public void enableEsfRaw() {
        this.handler.post(this.enableEsfRunnable);
    }

    public String getDeadReckoningStatus() {
        Resources resources = HudApplication.getAppContext().getResources();
        ObdManager instance = ObdManager.getInstance();
        return !instance.isConnected() ? resources.getString(R.string.obd_not_connected) :
                !instance.isSpeedPidAvailable() ? resources.getString(R.string.obd_no_speed_pid) :
                this.deadReckoningInjectionStarted ? this.rootInfo != null ? resources.getString(R.string.gps_calibrated) + GpsConstants.SPACE + this.rootInfo.toString() :
                this.waitForAutoAlignment ? resources.getString(R.string.gps_calibrating_sensor) :
                this.waitForFusionStatus ? resources.getString(R.string.gps_calibrating_fusion) :
                resources.getString(R.string.gps_calibration_unknown) :
                resources.getString(R.string.gps_calibration_unknown);
    }

    @Subscribe
    public void onDrivingStateChange(DrivingStateChange drivingStateChange) {
        if (!drivingStateChange.driving) {
            this.sensorDataProcessor.setCalibrated(false);
        }
    }

    @Subscribe
    public void onSupportedPidEventsChange(ObdSupportedPidsChangedEvent obdSupportedPidsChangedEvent) {
        initDeadReckoning();
    }

    public void run() {
        sLogger.v("start thread");
        while (this.runThread) {
            try {
                int read = this.inputStream.read(READ_BUF);
                if (read == -1) {
                    sLogger.v("eof");
                    this.runThread = false;
                } else if (read > 0) {
                    int indexOf = GenericUtil.indexOf(READ_BUF, ESF_RAW_PATTERN, 0, read - 1);
                    if (indexOf != -1) {
                        handleEsfRaw(indexOf, read);
                    } else {
                        int indexOf2 = GenericUtil.indexOf(READ_BUF, NAV_STATUS_PATTERN, 0, read - 1);
                        if (indexOf2 != -1) {
                            handleNavStatus(indexOf2, read);
                        } else {
                            if (!this.alignmentChecked) {
                                int indexOf3 = GenericUtil.indexOf(READ_BUF, ALIGNMENT_PATTERN, 0, read - 1);
                                if (indexOf3 != -1) {
                                    try {
                                        handleAlignment(indexOf3, read);
                                    } catch (Throwable th) {
                                        sLogger.e(th);
                                        postAlignmentRunnable(true);
                                    }
                                }
                            }
                            if (this.waitForFusionStatus) {
                                int indexOf4 = GenericUtil.indexOf(READ_BUF, ESF_STATUS_PATTERN, 0, read - 1);
                                if (indexOf4 != -1) {
                                    try {
                                        handleFusion(indexOf4, read);
                                    } catch (Throwable th2) {
                                        sLogger.e(th2);
                                        postFusionRunnable(true);
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (Throwable th3) {
                if (!(th3 instanceof InterruptedException)) {
                    sLogger.e(th3);
                }
                this.runThread = false;
            }
        }
        sLogger.v("end thread");
    }

    public void sendWarmReset() {
        this.handler.postAtFrontOfQueue(this.resetRunnable);
        this.handler.postDelayed(this.enableEsfRunnable, 2000);
    }
}
