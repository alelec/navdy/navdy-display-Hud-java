package com.navdy.hud.app.device.gps;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.text.TextUtils;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

public class GpsNmeaParser {
    private static final String COLON = ":";
    private static final String COMMA = ",";
    private static final char COMMA_CHAR = ',';
    private static final String DB = "db";
    private static final String EQUAL = "=";
    public static final int FIX_TYPE_2D_3D = 1;
    public static final int FIX_TYPE_DIFFERENTIAL = 2;
    public static final int FIX_TYPE_DR = 6;
    public static final int FIX_TYPE_FRTK = 5;
    public static final int FIX_TYPE_NONE = 0;
    public static final int FIX_TYPE_PPS = 3;
    public static final int FIX_TYPE_RTK = 4;
    private static final String GGA = "GGA";
    private static final String GLL = "GLL";
    private static final String GNS = "GNS";
    private static final String GSV = "GSV";
    private static final String RMC = "RMC";
    private static final String SPACE = " ";
    private static final String SVID = "SV-";
    private static final String VTG = "VTG";
    private static final int SATELLITE_REPORT_INTERVAL = 5000;
    private int fixType;
    private Handler handler;
    private long lastSatelliteReportTime;
    private int messageCount;
    private ArrayList<String> messages = new ArrayList<>();
    private String[] nmeaResult = new String[200];
    private Logger sLogger;
    private HashMap<String, HashMap<Integer, Integer>> satellitesSeen = new HashMap<>();
    private HashMap<String, Integer> satellitesUsed = new HashMap<>();

    /* renamed from: com.navdy.hud.app.device.gps.GpsNmeaParser$1 reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$navdy$hud$app$device$gps$GpsNmeaParser$NmeaMessage = new int[NmeaMessage.values().length];

        static {
            try {
                $SwitchMap$com$navdy$hud$app$device$gps$GpsNmeaParser$NmeaMessage[NmeaMessage.GGA.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$device$gps$GpsNmeaParser$NmeaMessage[NmeaMessage.GLL.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$device$gps$GpsNmeaParser$NmeaMessage[NmeaMessage.GNS.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$device$gps$GpsNmeaParser$NmeaMessage[NmeaMessage.RMC.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$device$gps$GpsNmeaParser$NmeaMessage[NmeaMessage.VTG.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$device$gps$GpsNmeaParser$NmeaMessage[NmeaMessage.GSV.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
        }
    }

    enum NmeaMessage {
        NOT_SUPPORTED,
        GGA,
        GLL,
        GNS,
        RMC,
        VTG,
        GSV
    }

    GpsNmeaParser(Logger logger, Handler handler2) {
        this.sLogger = logger;
        this.handler = handler2;
    }

    private String getGnssProviderName(String str) {
        char c = 65535;
        switch (str.hashCode()) {
            case 2266:
                if (str.equals("GA")) {
                    c = 2;
                    break;
                }
                break;
            case 2267:
                if (str.equals("GB")) {
                    c = 3;
                    break;
                }
                break;
            case 2277:
                if (str.equals("GL")) {
                    c = 1;
                    break;
                }
                break;
            case 2279:
                if (str.equals("GN")) {
                    c = 4;
                    break;
                }
                break;
            case 2281:
                if (str.equals("GP")) {
                    c = 0;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                return "GPS";
            case FIX_TYPE_2D_3D /*1*/:
                return "GLONASS";
            case FIX_TYPE_DIFFERENTIAL /*2*/:
                return "Galileo";
            case FIX_TYPE_PPS /*3*/:
                return "BeiDou";
            case FIX_TYPE_RTK /*4*/:
                return "MultiGNSS";
            default:
                return "UNK";
        }
    }

    static NmeaMessage getMessageType(char c, char c2, char c3) {
        switch (c) {
            case 'G':
                return (c2 == 'S' && c3 == 'V') ? NmeaMessage.GSV : (c2 == 'G' && c3 == 'A') ? NmeaMessage.GGA : (c2 == 'L' && c3 == 'L') ? NmeaMessage.GLL : (c2 == 'N' && c3 == 'S') ? NmeaMessage.GNS : NmeaMessage.NOT_SUPPORTED;
            case 'R':
                return (c2 == 'M' && c3 == 'C') ? NmeaMessage.RMC : NmeaMessage.NOT_SUPPORTED;
            case 'V':
                return (c2 == 'T' && c3 == 'G') ? NmeaMessage.VTG : NmeaMessage.NOT_SUPPORTED;
            default:
                return NmeaMessage.NOT_SUPPORTED;
        }
    }

    static int parseData(String str, String[] strArr) {
        int i;
        char[] charArray = str.toCharArray();
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        while (i2 < charArray.length) {
            if (charArray[i2] == ',') {
                strArr[i4] = new String(charArray, i3, i2 - i3);
                i = i2 + 1;
                i4++;
            } else {
                i = i3;
            }
            i2++;
            i3 = i;
        }
        strArr[i4] = new String(charArray, i3, (i2 - i3) - 2);
        return i4 + 1;
    }

    private void processGGA(String str) {
        parseData(str, this.nmeaResult);
        try {
            this.fixType = Integer.parseInt(this.nmeaResult[6]);
        } catch (Throwable th) {
            this.fixType = 0;
        }
    }

    private void processGLL(String str) {
    }

    private void processGNS(String str) {
        parseData(str, this.nmeaResult);
        String substring = this.nmeaResult[0].substring(1, 3);
        String str2 = this.nmeaResult[2];
        String str3 = this.nmeaResult[4];
        String str4 = this.nmeaResult[7];
        if (TextUtils.isEmpty(str2) || TextUtils.isEmpty(str3) || TextUtils.isEmpty(str4)) {
            this.satellitesUsed.put(substring, 0);
        } else {
            this.satellitesUsed.put(substring, Integer.parseInt(str4));
        }
    }

    private void processGSV(String str) {
        HashMap hashMap;
        if (parseData(str, this.nmeaResult) >= 8) {
            String substring = this.nmeaResult[0].substring(1, 3);
            int parseInt = Integer.parseInt(this.nmeaResult[1]);
            int parseInt2 = Integer.parseInt(this.nmeaResult[2]);
            if (parseInt2 == 1) {
                this.messages.clear();
                this.messageCount = parseInt;
            } else if (this.messages.size() == 0) {
                return;
            }
            this.messages.add(str);
            if (parseInt2 == this.messageCount) {
                HashMap hashMap2 = (HashMap) this.satellitesSeen.get(substring);
                if (hashMap2 == null) {
                    HashMap hashMap3 = new HashMap();
                    this.satellitesSeen.put(substring, hashMap3);
                    hashMap = hashMap3;
                } else {
                    hashMap2.clear();
                    hashMap = hashMap2;
                }
                int size = this.messages.size();
                for (int i = 0; i < size; i++) {
                    int parseData = parseData((String) this.messages.get(i), this.nmeaResult);
                    for (int i2 = 4; i2 + 4 < parseData; i2 += 4) {
                        String str2 = this.nmeaResult[i2];
                        String str3 = this.nmeaResult[i2 + 1];
                        String str4 = this.nmeaResult[i2 + 2];
                        String str5 = this.nmeaResult[i2 + 3];
                        if (!TextUtils.isEmpty(str2)) {
                            if (TextUtils.isEmpty(str3) || TextUtils.isEmpty(str4) || TextUtils.isEmpty(str5)) {
                                hashMap.remove(str2);
                            } else {
                                try {
                                    try {
                                        hashMap.put(Integer.parseInt(str2), Integer.parseInt(str5));
                                    } catch (NumberFormatException e) {
                                    }
                                } catch (NumberFormatException e2) {
                                }
                            }
                        }
                    }
                }
                this.messageCount = 0;
                this.messages.clear();
                long elapsedRealtime = SystemClock.elapsedRealtime();
                if (elapsedRealtime - this.lastSatelliteReportTime >= SATELLITE_REPORT_INTERVAL) {
                    sendGpsSatelliteStatus();
                    this.lastSatelliteReportTime = elapsedRealtime;
                }
            }
        }
    }

    private void processRMC(String str) {
    }

    private void processVTG(String str) {
    }

    private void sendGpsSatelliteStatus() {
        int i = 0;
        Bundle bundle = new Bundle();
        int i2 = 0;
        int i3 = 0;
        for (Entry<String, HashMap<Integer, Integer>> entry : this.satellitesSeen.entrySet()) {
            String str = entry.getKey();
            int i4 = i2;
            for (Entry<Integer, Integer> entry2 : (entry.getValue()).entrySet()) {
                int intValue = entry2.getKey();
                int intValue2 = entry2.getValue();
                i3++;
                bundle.putString(GpsConstants.GPS_SATELLITE_PROVIDER + i3, str);
                bundle.putInt(GpsConstants.GPS_SATELLITE_ID + i3, intValue);
                bundle.putInt(GpsConstants.GPS_SATELLITE_DB + i3, intValue2);
                if (intValue2 > i4) {
                    i4 = intValue2;
                }
            }
            i2 = i4;
        }
        bundle.putInt(GpsConstants.GPS_SATELLITE_SEEN, i3);
        bundle.putInt(GpsConstants.GPS_SATELLITE_MAX_DB, i2);
        for (Entry value : this.satellitesUsed.entrySet()) {
            i += (Integer) value.getValue();
        }
        bundle.putInt(GpsConstants.GPS_SATELLITE_USED, i);
        bundle.putInt(GpsConstants.GPS_FIX_TYPE, this.fixType);
        Message obtainMessage = this.handler.obtainMessage(2);
        obtainMessage.obj = bundle;
        this.handler.sendMessage(obtainMessage);
    }

    public void parseNmeaMessage(String str) {
        if (str != null) {
            char[] charArray = str.toCharArray();
            if (charArray.length >= 7 && charArray[0] == '$') {
                NmeaMessage messageType = getMessageType(charArray[3], charArray[4], charArray[5]);
                if (messageType != NmeaMessage.NOT_SUPPORTED) {
                    for (int i = 0; i < this.nmeaResult.length; i++) {
                        this.nmeaResult[i] = null;
                    }
                    switch (AnonymousClass1.$SwitchMap$com$navdy$hud$app$device$gps$GpsNmeaParser$NmeaMessage[messageType.ordinal()]) {
                        case FIX_TYPE_2D_3D /*1*/:
                            processGGA(str);
                            return;
                        case FIX_TYPE_PPS /*3*/:
                            processGNS(str);
                            return;
                        case FIX_TYPE_DR /*6*/:
                            processGSV(str);
                            return;
                        default:
                            return;
                    }
                }
            }
        }
    }
}
