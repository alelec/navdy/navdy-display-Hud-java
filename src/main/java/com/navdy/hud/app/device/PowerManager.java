package com.navdy.hud.app.device;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.SystemClock;
import android.view.KeyEvent;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.analytics.AnalyticsSupport$WakeupReason;
import com.navdy.hud.app.device.dial.DialManager;
import com.navdy.hud.app.device.gps.GpsConstants;
import com.navdy.hud.app.device.light.HUDLightUtils;
import com.navdy.hud.app.device.light.LED;
import com.navdy.hud.app.device.light.LightManager;
import com.navdy.hud.app.event.Shutdown;
import com.navdy.hud.app.event.Shutdown.Reason;
import com.navdy.hud.app.event.Wakeup;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

public class PowerManager {
    private static final String COOLING_DEVICE = "/sys/devices/virtual/thermal/cooling_device0/cur_state";
    /* access modifiers changed from: private */
    public static final long COOLING_MONITOR_INTERVAL = TimeUnit.SECONDS.toMillis(15);
    private static final long DEFAULT_INSTANT_ON_TIMEOUT = TimeUnit.HOURS.toMillis(4);
    private static final String INSTANT_ON = "persist.sys.instanton";
    private static final String INSTANT_ON_TIMEOUT_PROPERTRY = "persist.sys.instanton.timeout";
    public static final String LAST_LOW_VOLTAGE_EVENT = "last_low_voltage_event";
    public static final String NORMAL_MODE = "normal";
    public static final String POWER_MODE_PROPERTY = "sys.power.mode";
    public static final String QUIET_MODE = "quiet";
    public static final String BOOT_POWER_MODE = SystemProperties.get(POWER_MODE_PROPERTY);
    private static final long INSTANT_ON_TIMEOUT = SystemProperties.getLong(INSTANT_ON_TIMEOUT_PROPERTRY, DEFAULT_INSTANT_ON_TIMEOUT);
    public static final long RECHARGE_TIME = TimeUnit.DAYS.toMillis(7);
    /* access modifiers changed from: private */
    public static final Logger sLogger = new Logger(PowerManager.class);
    private boolean awake;
    /* access modifiers changed from: private */
    public Bus bus;
    /* access modifiers changed from: private */
    public Runnable checkCoolingState;
    /* access modifiers changed from: private */
    public Handler handler;
    private long lastLowVoltage;
    private Method mIPowerManagerShutdownMethod;
    private Object mPowerManager;
    private String oldCoolingState;
    private android.os.PowerManager powerManager;
    private SharedPreferences preferences;
    private Runnable quietModeTimeout;
    private RunState runState;

    private enum RunState {
        Unknown,
        Booting,
        Waking,
        Running
    }

    public PowerManager(Bus bus2, Context context, SharedPreferences sharedPreferences) {
        this.awake = !QUIET_MODE.equals(BOOT_POWER_MODE);
        this.handler = new Handler(Looper.getMainLooper());
        this.checkCoolingState = new Runnable() {
            public void run() {
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        if (PowerManager.this.updateCoolingState()) {
                            PowerManager.this.handler.postDelayed(PowerManager.this.checkCoolingState, PowerManager.COOLING_MONITOR_INTERVAL);
                        }
                    }
                }, 10);
            }
        };
        this.quietModeTimeout = new Runnable() {
            public void run() {
                PowerManager.sLogger.i("Quiet mode has timed out - forcing full shutdown");
                PowerManager.this.bus.post(new Shutdown(Reason.TIMEOUT));
            }
        };
        this.mIPowerManagerShutdownMethod = null;
        this.mPowerManager = null;
        this.bus = bus2;
        bus2.register(this);
        this.powerManager = (android.os.PowerManager) context.getSystemService(Context.POWER_SERVICE);
        this.preferences = sharedPreferences;
        this.lastLowVoltage = sharedPreferences.getLong(LAST_LOW_VOLTAGE_EVENT, -1);
        setupShutdown();
        SystemProperties.set(POWER_MODE_PROPERTY, inQuietMode() ? QUIET_MODE : NORMAL_MODE);
        if (!inQuietMode()) {
            startOverheatMonitoring(RunState.Booting);
        }
        sLogger.i("quietMode:" + inQuietMode());
    }

    private void androidSleep() {
        sLogger.d("androidSleep()");
        ToastManager.getInstance().disableToasts(true);
        try {
            android.os.PowerManager.class.getMethod("goToSleep", new Class[]{Long.TYPE}).invoke(this.powerManager, new Object[]{Long.valueOf(SystemClock.uptimeMillis())});
        } catch (Exception e) {
            sLogger.e("error invoking PowerManager.goToSleep(): " + e);
        }
    }

    private void androidWakeup() {
        ToastManager.getInstance().disableToasts(false);
        try {
            android.os.PowerManager.class.getMethod("wakeUp", new Class[]{Long.TYPE}).invoke(this.powerManager, new Object[]{Long.valueOf(SystemClock.uptimeMillis())});
        } catch (Exception e) {
            sLogger.e("error invoking PowerManager.wakeUp(): " + e);
        }
    }

    public static boolean isAwake() {
        return !SystemProperties.get(POWER_MODE_PROPERTY, GpsConstants.EMPTY).equals(QUIET_MODE);
    }

    private boolean isCoolingActive(String str) {
        return str != null && !str.equals("0");
    }

    private void setupShutdown() {
        try {
            IBinder iBinder = (IBinder) Class.forName("android.os.ServiceManager").getDeclaredMethod("getService", new Class[]{String.class}).invoke(null, new Object[]{"power"});
            this.mPowerManager = Class.forName("android.os.IPowerManager$Stub").getDeclaredMethod("asInterface", new Class[]{IBinder.class}).invoke(null, new Object[]{iBinder});
            try {
                this.mIPowerManagerShutdownMethod = Class.forName("android.os.IPowerManager").getDeclaredMethod("shutdown", new Class[]{Boolean.TYPE, Boolean.TYPE});
            } catch (Exception e) {
                sLogger.e("exception getting IPowerManager.shutdown() method", e);
            }
        } catch (Exception e2) {
            sLogger.e("exception invoking IPowerManager.Stub.asInterface()", e2);
        }
    }

    private void startOverheatMonitoring(RunState runState2) {
        this.runState = runState2;
        this.handler.removeCallbacks(this.checkCoolingState);
        this.handler.post(this.checkCoolingState);
    }

    /* access modifiers changed from: private */
    public boolean updateCoolingState() {
        try {
            String trim = IOUtils.convertFileToString(COOLING_DEVICE).trim();
            if (!trim.equals(this.oldCoolingState)) {
                sLogger.i("Cooling state changed from:" + this.oldCoolingState + " to:" + trim);
                if (!isCoolingActive(this.oldCoolingState) && isCoolingActive(trim)) {
                    AnalyticsSupport.recordCpuOverheat(this.runState.name());
                }
                this.oldCoolingState = trim;
            }
            this.runState = RunState.Running;
            return true;
        } catch (IOException e) {
            sLogger.e("Failed to read cooling device state - stopping monitoring", e);
            return false;
        }
    }

    @SuppressLint({"ApplySharedPref"})
    public void androidShutdown(Reason reason, boolean z) {
        boolean z2 = false;
        sLogger.d("androidShutdown: " + reason + " forceFullShutdown:" + z);
        DialManager.getInstance().requestDialReboot(false);
        if (reason == Reason.LOW_VOLTAGE || reason == Reason.CRITICAL_VOLTAGE) {
            this.preferences.edit().putLong(LAST_LOW_VOLTAGE_EVENT, System.currentTimeMillis()).commit();
        }
        if (!z) {
            z2 = true;
        }
        AnalyticsSupport.recordShutdown(reason, z2);
        LED.writeToSysfs("0", "/sys/dlpc/led_enable");
        if (this.mPowerManager == null || this.mIPowerManagerShutdownMethod == null) {
            sLogger.e("shutdown was not properly initialized");
        } else if (z) {
            try {
                this.mIPowerManagerShutdownMethod.invoke(this.mPowerManager, Boolean.FALSE, Boolean.FALSE);
            } catch (Exception e) {
                sLogger.e("exception invoking IPowerManager.shutdown()", e);
            }
        } else {
            this.powerManager.reboot(QUIET_MODE);
        }
    }

    public void enterSleepMode() {
        this.handler.postDelayed(this.quietModeTimeout, INSTANT_ON_TIMEOUT);
        HUDLightUtils.turnOffFrontLED(LightManager.getInstance());
        androidSleep();
    }

    public boolean inQuietMode() {
        return !this.awake;
    }

    @Subscribe
    public void onKey(KeyEvent keyEvent) {
        wakeUp(AnalyticsSupport$WakeupReason.DIAL);
    }

    public boolean quietModeEnabled() {
        long currentTimeMillis = System.currentTimeMillis() - this.lastLowVoltage;
        boolean z = this.lastLowVoltage != -1 && currentTimeMillis < RECHARGE_TIME;
        if (z) {
            sLogger.d("disabling quiet mode since we had a low voltage event " + TimeUnit.MILLISECONDS.toHours(currentTimeMillis) + " hours ago");
        }
        return SystemProperties.getBoolean(INSTANT_ON, true) && !z;
    }

    public void wakeUp(AnalyticsSupport$WakeupReason wakeupReason) {
        if (!this.awake) {
            sLogger.d("waking up");
            this.awake = true;
            SystemProperties.set(POWER_MODE_PROPERTY, NORMAL_MODE);
            this.handler.removeCallbacks(this.quietModeTimeout);
            HUDLightUtils.resetFrontLED(LightManager.getInstance());
            LED.writeToSysfs("1", "/sys/dlpc/led_enable");
            androidWakeup();
            startOverheatMonitoring(RunState.Waking);
            this.bus.post(new Wakeup(wakeupReason));
        }
    }
}
