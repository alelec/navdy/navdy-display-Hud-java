package com.navdy.hud.app.device.gps;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class RawSensorData {
    private static final float ACCEL_SCALE_FACTOR = ((float) Math.pow(2.0d, -10.0d));
    public static final int ACCEL_X = 16;
    public static final int ACCEL_Y = 17;
    public static final int ACCEL_Z = 18;
    private static final float GYRO_SCALE_FACTOR = ((float) Math.pow(2.0d, -12.0d));
    public static final int GYRO_TEMP = 12;
    public static final int GYRO_X = 13;
    public static final int GYRO_Y = 14;
    public static final int GYRO_Z = 5;
    public static final float MAX_ACCEL = 2.0f;
    public final float x;
    public final float y;
    public final float z;

    public RawSensorData(byte[] bArr, int i) {
        ByteBuffer wrap = ByteBuffer.wrap(bArr, i + 4, (bArr.length - i) - 4);
        wrap.order(ByteOrder.LITTLE_ENDIAN);
        int i2 = (wrap.getShort() - 4) / 8;
        wrap.mark();
        this.x = clamp(average(wrap, i2, 16, ACCEL_SCALE_FACTOR) / 9.8f, -2.0f, 2.0f);
        this.y = clamp(average(wrap, i2, 17, ACCEL_SCALE_FACTOR) / 9.8f, -2.0f, 2.0f);
        this.z = clamp(average(wrap, i2, 18, ACCEL_SCALE_FACTOR) / 9.8f, -2.0f, 2.0f);
    }

    private float average(ByteBuffer byteBuffer, int i, int i2, float f) {
        float f2;
        byteBuffer.reset();
        byteBuffer.getInt();
        int i3 = 0;
        float f3 = 0.0f;
        int i4 = 0;
        while (i3 < i && byteBuffer.position() < byteBuffer.limit()) {
            int i5 = byteBuffer.getInt();
            byteBuffer.getInt();
            if ((i5 >> 24) == i2) {
                int i6 = i5 & 16777215;
                if ((8388608 & i6) != 0) {
                    i6 |= -16777216;
                }
                f2 = (((float) i6) * f) + f3;
                i4++;
            } else {
                f2 = f3;
            }
            i3++;
            f3 = f2;
        }
        if (i4 > 0) {
            return f3 / ((float) i4);
        }
        return 0.0f;
    }

    private float clamp(float f, float f2, float f3) {
        return f < f2 ? f2 : f <= f3 ? f : f3;
    }
}
