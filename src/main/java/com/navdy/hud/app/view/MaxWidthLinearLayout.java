package com.navdy.hud.app.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View.MeasureSpec;
import android.widget.LinearLayout;
import com.navdy.hud.app.R;

public class MaxWidthLinearLayout extends LinearLayout {
    private int maxWidth;

    public MaxWidthLinearLayout(Context context) {
        super(context);
        this.maxWidth = 0;
    }

    public MaxWidthLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.MaxWidthLinearLayout);
        this.maxWidth = a.getDimensionPixelSize(R.styleable.MaxWidthLinearLayout_maxWidth, Integer.MAX_VALUE);
        a.recycle();
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int measuredWidth = MeasureSpec.getSize(widthMeasureSpec);
        if (this.maxWidth > 0 && this.maxWidth < measuredWidth) {
            widthMeasureSpec = MeasureSpec.makeMeasureSpec(this.maxWidth, MeasureSpec.getMode(widthMeasureSpec));
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    public void setMaxWidth(int n) {
        this.maxWidth = n;
        invalidate();
    }

    public int getMaxWidth() {
        return this.maxWidth;
    }
}
