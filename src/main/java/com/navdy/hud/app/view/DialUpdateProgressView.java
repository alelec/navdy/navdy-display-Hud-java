package com.navdy.hud.app.view;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import butterknife.ButterKnife;
import butterknife.InjectView;

import com.navdy.hud.app.R;
import com.navdy.hud.app.device.dial.DialFirmwareUpdater.Error;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.screen.DialUpdateProgressScreen.Presenter;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.log.Logger;
import javax.inject.Inject;
import mortar.Mortar;

public class DialUpdateProgressView extends RelativeLayout implements IInputHandler {
    private static final long UPDATE_TIMEOUT = 480000;
    private static final Logger sLogger = new Logger(DialUpdateProgressView.class);
    private Handler handler = new Handler();
    @Inject
    Presenter mPresenter;
    @InjectView(R.id.progress)
    ProgressBar mProgress;
    private Runnable timeout = new Runnable() {
        public void run() {
            DialUpdateProgressView.sLogger.v("timedout");
            DialUpdateProgressView.this.mPresenter.finish(Error.TIMEDOUT);
        }
    };

    public DialUpdateProgressView(Context context) {
        super(context, null);
        if (!isInEditMode()) {
            Mortar.inject(context, this);
        }
    }

    public DialUpdateProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            Mortar.inject(context, this);
        }
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View) this);
        if (this.mPresenter != null) {
            this.mPresenter.takeView(this);
            NotificationManager.getInstance().enableNotifications(false);
            ToastManager.getInstance().disableToasts(true);
            this.mPresenter.startUpdate();
            this.handler.removeCallbacks(this.timeout);
            this.handler.postDelayed(this.timeout, UPDATE_TIMEOUT);
        }
        setProgress(0);
    }

    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        ToastManager.getInstance().disableToasts(false);
        NotificationManager.getInstance().enableNotifications(true);
        if (this.mPresenter != null) {
            this.handler.removeCallbacks(this.timeout);
            this.mPresenter.dropView(this);
        }
    }

    public void setProgress(int percentage) {
        this.mProgress.setProgress(percentage);
    }

    public boolean onGesture(GestureEvent event) {
        return false;
    }

    public boolean onKey(CustomKeyEvent event) {
        return true;
    }

    public IInputHandler nextHandler() {
        return null;
    }
}
