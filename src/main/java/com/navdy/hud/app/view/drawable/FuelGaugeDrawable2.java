package com.navdy.hud.app.view.drawable;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.RectF;
import com.navdy.hud.app.R;

public class FuelGaugeDrawable2 extends GaugeDrawable {
    private static final int TICKS_COUNT = 4;
    private static final int TICK_ANGLE_WIDTH = 2;
    private int mBackgroundColor;
    private int mFuelGaugeWidth;
    private boolean mLeftOriented = true;

    public FuelGaugeDrawable2(Context context, int stateColorTable) {
        super(context, 0, stateColorTable);
        this.mFuelGaugeWidth = context.getResources().getDimensionPixelSize(R.dimen.fuel_gauge_width);
        this.mBackgroundColor = this.mColorTable[3];
    }

    public void setLeftOriented(boolean leftOriented) {
        this.mLeftOriented = leftOriented;
    }

    public void draw(Canvas canvas) {
        RectF fuelGaugeArcBounds;
        super.draw(canvas);
        Rect bounds = getBounds();
        if (this.mLeftOriented) {
            fuelGaugeArcBounds = new RectF((float) bounds.left, (float) bounds.top, (float) (bounds.right + bounds.width()), (float) bounds.bottom);
        } else {
            fuelGaugeArcBounds = new RectF((float) (bounds.left - bounds.width()), (float) bounds.top, (float) bounds.right, (float) bounds.bottom);
        }
        fuelGaugeArcBounds.inset((float) ((this.mFuelGaugeWidth / 2) + 1), (float) ((this.mFuelGaugeWidth / 2) + 1));
        this.mPaint.setStyle(Style.STROKE);
        this.mPaint.setStrokeWidth((float) this.mFuelGaugeWidth);
        this.mPaint.setColor(this.mBackgroundColor);
        int startAngle = this.mLeftOriented ? 90 : 270;
        int endAngle = startAngle + 180;
        canvas.drawArc(fuelGaugeArcBounds, (float) startAngle, (float) 180, false, this.mPaint);
        this.mPaint.setColor(this.mDefaultColor);
        int valueSweepAngle = (int) ((this.mValue / this.mMaxValue) * ((float) 180));
        canvas.drawArc(fuelGaugeArcBounds, (float) (this.mLeftOriented ? 90 : 450 - valueSweepAngle), (float) valueSweepAngle, false, this.mPaint);
        int slice = 180 / 4;
        this.mPaint.setColor(-16777216);
        for (int i = 1; i < 4; i++) {
            canvas.drawArc(fuelGaugeArcBounds, (float) (((i * 45) + startAngle) % 360), 2.0f, false, this.mPaint);
        }
    }
}
