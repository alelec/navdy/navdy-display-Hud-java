package com.navdy.hud.app.view.drawable;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import com.navdy.hud.app.R;
import com.navdy.hud.app.util.DateUtil;

public class AnalogClockDrawable extends CustomDrawable {
    private int centerPointWidth;
    private int dateTextMargin;
    private int dateTextSize;
    private int dayOfMonth;
    private int frameColor;
    private int hour;
    private int hourHandColor;
    private float hourHandLengthFraction = 0.75f;
    private int hourHandStrokeWidth;
    private int minute;
    private int minuteHandColor;
    private float minuteHandLengthFraction = 0.8f;
    private int minuteHandStrokeWidth;
    private int seconds;

    public AnalogClockDrawable(Context context) {
        this.frameColor = context.getResources().getColor(R.color.analog_clock_frame_color);
        this.hourHandColor = context.getResources().getColor(R.color.cyan);
        this.hourHandStrokeWidth = context.getResources().getDimensionPixelSize(R.dimen.analog_clock_hour_hand_width);
        this.minuteHandStrokeWidth = context.getResources().getDimensionPixelSize(R.dimen.analog_clock_minute_hand_width);
        this.centerPointWidth = context.getResources().getDimensionPixelSize(R.dimen.analog_clock_center_point_width);
        this.minuteHandColor = -1;
        this.dateTextSize = context.getResources().getDimensionPixelSize(R.dimen.analog_clock_date_text_size);
        this.dateTextMargin = context.getResources().getDimensionPixelSize(R.dimen.analog_clock_date_text_margin);
    }

    public void setTime(int day, int hour, int minute, int seconds) {
        this.dayOfMonth = day;
        this.hour = hour;
        this.minute = minute;
        this.seconds = seconds;
    }

    @Override
    public void draw(final Canvas canvas) {
        super.draw(canvas);
        final Rect bounds = this.getBounds();
        final RectF rectF = new RectF(bounds);
        this.mPaint.setAntiAlias(true);
        this.mPaint.setStyle(Style.FILL);
        this.mPaint.setColor(this.frameColor);
        canvas.drawArc(rectF, 0.0f, 360.0f, true, this.mPaint);
        this.mPaint.setStyle(Style.STROKE);
        this.mPaint.setColor(this.hourHandColor);
        this.mPaint.setStrokeCap(Cap.ROUND);
        this.mPaint.setStrokeWidth((float)this.hourHandStrokeWidth);
        this.mPaint.setShadowLayer(10.0f, 10.0f, 10.0f, -16777216);
        final float clockAngleForHour = DateUtil.getClockAngleForHour(this.hour, this.minute);
        final float n = bounds.width() / 2 - 15;
        canvas.drawLine((float)bounds.centerX(), (float)bounds.centerY(), (float)(int)(bounds.centerX() + n * Math.cos(Math.toRadians(clockAngleForHour))), (float)(int)(bounds.centerY() + n * Math.sin(Math.toRadians(clockAngleForHour))), this.mPaint);
        this.mPaint.setShadowLayer(0.0f, 0.0f, 0.0f, this.mPaint.getColor());
        this.mPaint.setStyle(Style.STROKE);
        this.mPaint.setColor(this.minuteHandColor);
        this.mPaint.setStrokeCap(Cap.ROUND);
        this.mPaint.setStrokeWidth((float)this.minuteHandStrokeWidth);
        final float clockAngleForMinutes = DateUtil.getClockAngleForMinutes(this.minute);
        final float n2 = bounds.width() / 2 - 10;
        canvas.drawLine((float)bounds.centerX(), (float)bounds.centerY(), (float)(int)(bounds.centerX() + n2 * Math.cos(Math.toRadians(clockAngleForMinutes))), (float)(int)(bounds.centerY() + n2 * Math.sin(Math.toRadians(clockAngleForMinutes))), this.mPaint);
        this.mPaint.setStyle(Style.FILL);
        this.mPaint.setStrokeWidth(1.0f);
        this.mPaint.setColor(this.minuteHandColor);
        canvas.drawArc((float)(bounds.centerX() - this.centerPointWidth), (float)(bounds.centerY() - this.centerPointWidth), (float)(bounds.centerX() + this.centerPointWidth), (float)(bounds.centerY() + this.centerPointWidth), 0.0f, 360.0f, true, this.mPaint);
        final float n3 = bounds.width() / 2 - this.dateTextMargin;
        this.mPaint.setStyle(Style.FILL);
        this.mPaint.setTypeface(Typeface.create(Typeface.DEFAULT, 1));
        this.mPaint.setTextSize((float)this.dateTextSize);
        final String string = Integer.toString(this.dayOfMonth);
        final Rect rect = new Rect();
        this.mPaint.getTextBounds(string, 0, string.length(), rect);
        final int n4 = this.hour % 12;
        if ((n4 < 7 || n4 >= 11) && (this.minute < 35 || this.minute >= 55)) {
            canvas.drawText(string, (float)(int)(bounds.centerX() + n3 * Math.cos(Math.toRadians(180.0f))), (float)(rect.height() / 2 + (int)(bounds.centerY() + n3 * Math.sin(Math.toRadians(180.0f)))), this.mPaint);
        }
        else if ((n4 < 1 || n4 >= 4) && (this.minute < 5 || this.minute >= 20)) {
            canvas.drawText(string, (float)((int)(bounds.centerX() + n3 * Math.cos(Math.toRadians(0.0f))) - rect.width()), (float)(rect.height() / 2 + (int)(bounds.centerY() + n3 * Math.sin(Math.toRadians(0.0f)))), this.mPaint);
        }
        else if ((n4 < 4 || n4 >= 7) && (this.minute < 20 || this.minute >= 35)) {
            canvas.drawText(string, (float)((int)(bounds.centerX() + n3 * Math.cos(Math.toRadians(90.0f))) - rect.width() / 2), (float)(int)(bounds.centerY() + n3 * Math.sin(Math.toRadians(90.0f))), this.mPaint);
        }
    }
}
