package com.navdy.hud.app.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.FillType;
import android.graphics.PointF;
import android.graphics.RadialGradient;
import android.graphics.RectF;
import android.graphics.Shader.TileMode;
import android.util.AttributeSet;
import android.view.View;
import com.navdy.hud.app.R;
import com.navdy.hud.app.util.CustomDimension;

public class MapMask extends View {
    private static final String TAG = MapMask.class.getSimpleName();
    private int bottom;
    private Paint fadePaint;
    private Path fadePath;
    private int left;
    private int mHorizontalRadius;
    private CustomDimension mHorizontalRadiusAttribute;
    private IndicatorView mIndicator;
    private int mMaskColor;
    private int mVerticalRadius;
    private CustomDimension mVerticalRadiusAttribute;
    private Paint maskPaint;
    private Path maskPath;
    private int right;
    private int top;

    public MapMask(Context context) {
        this(context, null);
    }

    public MapMask(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MapMask(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initFromAttributes(context, attrs);
        this.mIndicator = new IndicatorView(context, attrs);
        initDrawingTools();
    }

    private void initFromAttributes(Context context, AttributeSet attrs) {
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.MapMask, 0, 0);
        try {
            this.mHorizontalRadiusAttribute = CustomDimension.getDimension(this, a, 0, 0.0f);
            this.mVerticalRadiusAttribute = CustomDimension.getDimension(this, a, 1, 0.0f);
            this.mMaskColor = a.getColor(R.styleable.MapMask_maskColor, -16777216);
        } finally {
            a.recycle();
        }
    }

    private void evaluateDimensions(int w, int h) {
        this.mHorizontalRadius = (int) this.mHorizontalRadiusAttribute.getSize(this, (float) w, 0.0f);
        this.mVerticalRadius = (int) this.mVerticalRadiusAttribute.getSize(this, (float) h, 0.0f);
        this.mIndicator.evaluateDimensions(this.mHorizontalRadius * 2, h);
    }

    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        evaluateDimensions(w, h);
        initDrawingTools();
    }

    public void layout(int l, int t, int r, int b) {
        super.layout(l, t, r, b);
        this.mIndicator.layout(this.left, 0, this.right, this.mIndicator.getIndicatorHeight());
    }

    private void initDrawingTools() {
        this.maskPath = new Path();
        this.maskPaint = new Paint();
        this.maskPaint.setColor(this.mMaskColor);
        this.maskPaint.setAntiAlias(true);
        this.maskPaint.setStyle(Style.FILL_AND_STROKE);
        int width = getWidth();
        int height = getHeight();
        this.maskPath.moveTo(0.0f, 0.0f);
        this.maskPath.lineTo(0.0f, (float) height);
        this.maskPath.lineTo((float) width, (float) height);
        this.maskPath.lineTo((float) width, 0.0f);
        this.maskPath.close();
        this.top = (height - this.mVerticalRadius) / 2;
        this.bottom = this.top + this.mVerticalRadius;
        int ovalBottom = this.bottom + this.mVerticalRadius;
        this.left = (width - (this.mHorizontalRadius * 2)) / 2;
        this.right = this.left + (this.mHorizontalRadius * 2);
        this.maskPath.moveTo((float) this.left, (float) this.bottom);
        RectF oval = new RectF((float) this.left, (float) this.top, (float) this.right, (float) ovalBottom);
        this.maskPath.arcTo(oval, 180.0f, 180.0f, false);
        IndicatorView.drawIndicatorPath(this.maskPath, (float) this.left, (float) this.bottom, this.mIndicator.getCurveRadius(), (float) this.mIndicator.getIndicatorWidth(), (float) this.mIndicator.getIndicatorHeight(), (float) (this.mHorizontalRadius * 2));
        this.maskPath.setFillType(FillType.EVEN_ODD);
        float strokeWidth = this.mIndicator.getStrokeWidth();
        float radius = (float) this.mHorizontalRadius;
        if (radius < 60.0f) {
            radius = 60.0f;
        }
        this.fadePaint = new Paint();
        this.fadePaint.setStrokeWidth(60.0f);
        this.fadePaint.setAntiAlias(true);
        this.fadePaint.setStrokeCap(Cap.BUTT);
        this.fadePaint.setStyle(Style.STROKE);
        this.fadePaint.setShader(new RadialGradient(((float) this.left) + radius, ((float) this.bottom) + strokeWidth, radius, new int[]{0, 0, -16777216}, new float[]{0.0f, (radius - 60.0f) / radius, 1.0f}, TileMode.CLAMP));
        this.fadePath = new Path();
        this.fadePath.moveTo((float) this.left, (float) this.bottom);
        oval.inset((60.0f / 2.0f) - strokeWidth, (60.0f / 2.0f) - strokeWidth);
        oval.top += strokeWidth;
        oval.bottom += strokeWidth;
        this.fadePath.arcTo(oval, 180.0f, 180.0f, false);
    }

    public PointF getIndicatorPoint() {
        return new PointF((float) (this.left + this.mHorizontalRadius), (float) (this.bottom - this.mIndicator.getIndicatorHeight()));
    }

    protected void onDraw(Canvas canvas) {
        canvas.drawPath(this.maskPath, this.maskPaint);
        int yOffset = this.bottom - this.mIndicator.getIndicatorHeight();
        canvas.translate((float) this.left, (float) yOffset);
        this.mIndicator.draw(canvas);
        canvas.translate((float) (-this.left), (float) (-yOffset));
        canvas.drawPath(this.fadePath, this.fadePaint);
    }
}
