package com.navdy.hud.app.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.util.AttributeSet;
import com.navdy.hud.app.R;

public class BrightnessControlGauge extends Gauge {
    private static final int PIVOT_ANGLE = 5;
    private static final int THUMB_ANGLE = 10;
    private int mPivotIndicatorColor;
    private Paint mPivotIndicatorPaint;
    private int mPivotValue;
    private int mThumbColor;
    private Paint mThumbPaint;

    public BrightnessControlGauge(Context context) {
        this(context, null);
    }

    public BrightnessControlGauge(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BrightnessControlGauge(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Resources resources = getResources();
        this.mPivotValue = context.getTheme().obtainStyledAttributes(attrs, R.styleable.BrightnessControlGauge, 0, 0).getInteger(R.styleable.BrightnessControlGauge_gaugePivotValue, 0);
        this.mThumbColor = resources.getColor(R.color.thumb_color);
        this.mPivotIndicatorColor = resources.getColor(R.color.pivot_indicator_color);
    }

    protected void initDrawingTools() {
        super.initDrawingTools();
        this.mPivotIndicatorPaint = new Paint();
        this.mPivotIndicatorPaint.setStrokeWidth((float) this.mBackgroundThickness);
        this.mPivotIndicatorPaint.setAntiAlias(true);
        this.mPivotIndicatorPaint.setStrokeCap(Cap.BUTT);
        this.mPivotIndicatorPaint.setStyle(Style.STROKE);
        this.mThumbPaint = new Paint();
        this.mThumbPaint.setColor(this.mThumbColor);
        this.mThumbPaint.setStrokeWidth((float) this.mThickness);
        this.mThumbPaint.setAntiAlias(true);
        this.mThumbPaint.setStrokeCap(Cap.BUTT);
        this.mThumbPaint.setStyle(Style.STROKE);
    }

    protected void drawGauge(Canvas canvas) {
        float width = (float) getWidth();
        float height = (float) getHeight();
        float radius = getRadius();
        float startAngle = deltaToAngle(this.mPivotValue - this.mMinValue) - (5.0f / 2.0f);
        float rectLeft = (width / 2.0f) - radius;
        float rectTop = (height / 2.0f) - radius;
        float rectRight = (width / 2.0f) + radius;
        float rectBottom = (height / 2.0f) + radius;
        RectF rect = new RectF();
        rect.set(rectLeft, rectTop, rectRight, rectBottom);
        canvas.drawArc(rect, ((float) this.mStartAngle) + startAngle, 5.0f, false, this.mPivotIndicatorPaint);
        if (this.mValue > this.mPivotValue) {
            drawIndicator(canvas, this.mPivotValue, this.mValue);
        } else if (this.mValue < this.mPivotValue) {
            drawIndicator(canvas, this.mValue, this.mPivotValue);
        }
        Canvas canvas2 = canvas;
        canvas2.drawArc(rect, ((float) this.mStartAngle) + (deltaToAngle(this.mValue - this.mMinValue) - (10.0f / 2.0f)), 10.0f, false, this.mThumbPaint);
    }
}
