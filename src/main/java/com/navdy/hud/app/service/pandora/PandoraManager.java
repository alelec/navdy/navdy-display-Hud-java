package com.navdy.hud.app.service.pandora;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.res.Resources;
import android.os.Handler;
import android.text.TextUtils;
import com.navdy.hud.app.R;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException;
import com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException;
import com.navdy.hud.app.service.pandora.exceptions.UnsupportedMessageReceivedException;
import com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage;
import com.navdy.hud.app.service.pandora.messages.BaseOutgoingMessage;
import com.navdy.hud.app.service.pandora.messages.EventTrackPlay;
import com.navdy.hud.app.service.pandora.messages.GetTrackAlbumArt;
import com.navdy.hud.app.service.pandora.messages.GetTrackInfoExtended;
import com.navdy.hud.app.service.pandora.messages.ReturnTrackAlbumArtSegment;
import com.navdy.hud.app.service.pandora.messages.ReturnTrackInfoExtended;
import com.navdy.hud.app.service.pandora.messages.SessionStart;
import com.navdy.hud.app.service.pandora.messages.SessionTerminate;
import com.navdy.hud.app.service.pandora.messages.SetTrackElapsedPolling;
import com.navdy.hud.app.service.pandora.messages.UpdateStatus;
import com.navdy.hud.app.service.pandora.messages.UpdateTrack;
import com.navdy.hud.app.service.pandora.messages.UpdateTrackAlbumArt;
import com.navdy.hud.app.service.pandora.messages.UpdateTrackCompleted;
import com.navdy.hud.app.service.pandora.messages.UpdateTrackElapsed;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.events.DeviceInfo.Platform;
import com.navdy.service.library.events.audio.MusicDataSource;
import com.navdy.service.library.events.audio.MusicPlaybackState;
import com.navdy.service.library.events.audio.MusicTrackInfo;
import com.navdy.service.library.events.audio.MusicTrackInfo.Builder;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.service.library.events.notification.NotificationsState;
import com.navdy.service.library.events.notification.NotificationsStatusUpdate;
import com.navdy.service.library.events.notification.ServiceType;
import com.navdy.service.library.events.photo.PhotoType;
import com.navdy.service.library.events.photo.PhotoUpdate;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import okio.ByteString;

public class PandoraManager implements Closeable {
    private static final int ACK_WAITING_TIME = 750;
    private static final AtomicInteger CONNECT_THREAD_COUNTER = new AtomicInteger(1);
    private static final Builder INITIAL_TRACK_INFO_BUILDER = new Builder().playbackState(MusicPlaybackState.PLAYBACK_NONE).dataSource(MusicDataSource.MUSIC_SOURCE_PANDORA_API);
    private static final int MAX_SEND_MESSAGE_RETRIES = 10;
    private static final UUID PANDORA_UUID = UUID.fromString("453994D5-D58B-96F9-6616-B37F586BA2EC");
    private static final AtomicInteger READ_THREAD_COUNTER = new AtomicInteger(1);
    private static final Handler handler = new Handler();
    static final Logger sLogger = new Logger(PandoraManager.class);
    private Runnable activeAckMessageTimeoutCounter = null;
    private String adTitle = "";
    private Bus bus;
    private ByteArrayOutputStream currentArtwork = null;
    private MusicTrackInfo currentTrack = INITIAL_TRACK_INFO_BUILDER.build();
    private byte expectedArtworkSegmentIndex = (byte) 0;
    private AtomicBoolean isConnectingInProgress = new AtomicBoolean(false);
    private boolean isCurrentSequence0 = true;
    private ConcurrentLinkedQueue<BaseOutgoingMessage> msgQueue = new ConcurrentLinkedQueue();
    private String remoteDeviceId = null;
    private BluetoothSocket socket = null;

    class ConnectRunnable implements Runnable {
        private static final int MAX_CONNECTING_RETRIES = 10;
        private static final int PAUSE_BETWEEN_CONNECTING_RETRIES = 2000;
        private boolean isStartMusicOn = false;

        public ConnectRunnable(boolean isStartMusicOn) {
            this.isStartMusicOn = isStartMusicOn;
        }

        private void safeSleepPause() {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                PandoraManager.sLogger.e("Interrupted while pausing between Pandora connection retries");
            }
        }

        public void run() {
            BluetoothSocket tempSocket = null;
            for (int retry = 0; retry < 10; retry++) {
                try {
                    if (PandoraManager.this.isConnected()) {
                        PandoraManager.this.terminateAndClose();
                    }
                    BluetoothDevice device = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(new NavdyDeviceId(PandoraManager.this.remoteDeviceId).getBluetoothAddress());
                    tempSocket = device.createRfcommSocketToServiceRecord(PandoraManager.PANDORA_UUID);
                    tempSocket.connect();
                    PandoraManager.this.setSocket(tempSocket);
                    if (PandoraManager.this.isConnected()) {
                        PandoraManager.this.sendOrQueueMessage(SessionStart.INSTANCE);
                        PandoraManager.this.sendOrQueueMessage(GetTrackInfoExtended.INSTANCE);
                        PandoraManager.this.sendOrQueueMessage(SetTrackElapsedPolling.ENABLED);
                        if (this.isStartMusicOn) {
                            PandoraManager.this.sendOrQueueMessage(EventTrackPlay.INSTANCE);
                        }
                        Thread thread = new Thread(new ReadMessagesThread(PandoraManager.this));
                        thread.setName("Pandora-Read-" + PandoraManager.READ_THREAD_COUNTER.getAndIncrement());
                        thread.start();
                        PandoraManager.this.isConnectingInProgress.set(false);
                        break;
                    }
                } catch (IOException e) {
                    PandoraManager.sLogger.e("Cannot connect to Pandora on remote device: " + PandoraManager.this.remoteDeviceId);
                    IOUtils.closeObject(tempSocket);
                    safeSleepPause();
                } catch (Throwable t) {
                    PandoraManager.sLogger.e(Thread.currentThread().getName(), t);
                }
                PandoraManager.sLogger.v("exiting thread:" + Thread.currentThread().getName());
            }
        }
    }

    public PandoraManager(Bus bus, Resources resources) {
        this.bus = bus;
        this.adTitle = resources.getString(R.string.music_pandora_ad_title);
    }

    private void sendByteArray(byte[] frame) throws IOException {
        OutputStream os = getSocket().getOutputStream();
        os.write(frame);
        os.flush();
    }

    private void sendDataMessage(final byte[] frame, int leftAttempts) {
        if (leftAttempts <= 0) {
            sLogger.e("Max number of retries for the message achieved - closing connection");
            close();
            return;
        }
        final int newValueLeftAttempts = leftAttempts - 1;
        try {
            sendByteArray(frame);
            this.activeAckMessageTimeoutCounter = new Runnable() {
                public void run() {
                    PandoraManager.this.sendDataMessage(frame, newValueLeftAttempts);
                }
            };
            handler.postDelayed(this.activeAckMessageTimeoutCounter, 750);
        } catch (IOException e) {
            sLogger.e("Cannot send message frame through the socket - disconnecting", e);
            close();
        }
    }

    public void sendOrQueueMessage(BaseOutgoingMessage msg) {
        this.msgQueue.add(msg);
        if (this.activeAckMessageTimeoutCounter == null) {
            sendNextMessage();
        }
    }

    private void sendNextMessage() {
        try {
            BaseOutgoingMessage msg = (BaseOutgoingMessage) this.msgQueue.poll();
            if (msg == null) {
                sLogger.v("Queue empty: no new message waiting to be sent");
                return;
            }
            sLogger.i("Sending message to Pandora: " + msg.toString());
            sendDataMessage(new DataFrameMessage(this.isCurrentSequence0, msg.buildPayload()).buildFrame(), 10);
        } catch (Exception e) {
            sLogger.e("Cannot send data message - disconnecting", e);
            close();
        }
    }

    private void sendAckMessage(boolean isAckSequence0) {
        byte[] ackMsgFrame;
        if (isAckSequence0) {
            ackMsgFrame = AckFrameMessage.ACK_WITH_SEQUENCE_0.buildFrame();
        } else {
            ackMsgFrame = AckFrameMessage.ACK_WITH_SEQUENCE_1.buildFrame();
        }
        try {
            sendByteArray(ackMsgFrame);
            if (sLogger.isLoggable(2)) {
                sLogger.i("Ack message sent to Pandora - is sequence 0: " + isAckSequence0);
            }
        } catch (IOException e) {
            sLogger.e("Cannot send ACK message frame through the socket - disconnecting", e);
            close();
        }
    }

    protected void onFrameReceived(byte[] frame) {
        Exception e;
        try {
            FrameMessage msg = FrameMessage.parseFrame(frame);
            if (msg instanceof AckFrameMessage) {
                if (sLogger.isLoggable(2)) {
                    sLogger.i("Ack message sent to Pandora - is sequence 0: " + msg.isSequence0);
                }
                if (this.activeAckMessageTimeoutCounter == null || this.isCurrentSequence0 == msg.isSequence0) {
                    sLogger.e("Invalid ACK message received - ignoring it");
                    return;
                }
                handler.removeCallbacks(this.activeAckMessageTimeoutCounter);
                this.isCurrentSequence0 = msg.isSequence0;
                this.activeAckMessageTimeoutCounter = null;
                sendNextMessage();
            } else if (msg instanceof DataFrameMessage) {
                sendAckMessage(!msg.isSequence0);
                BaseIncomingMessage parsedMsg = null;
                try {
                    parsedMsg = BaseIncomingMessage.buildFromPayload(msg.payload);
                    if (sLogger.isLoggable(2)) {
                        sLogger.i("Received (and ACKed) message from Pandora: " + parsedMsg.toString());
                    }
                } catch (MessageWrongWayException e2) {
                    e = e2;
                    sLogger.w(e.getMessage() + " - ignoring the message (already ACKed)");
                    if (parsedMsg == null) {
                        onMessageReceived(parsedMsg);
                    }
                } catch (CorruptedPayloadException e3) {
                    e = e3;
                    sLogger.w(e.getMessage() + " - ignoring the message (already ACKed)");
                    if (parsedMsg == null) {
                        onMessageReceived(parsedMsg);
                    }
                } catch (UnsupportedMessageReceivedException e4) {
                    e = e4;
                    sLogger.w(e.getMessage() + " - ignoring the message (already ACKed)");
                    if (parsedMsg == null) {
                        onMessageReceived(parsedMsg);
                    }
                }
                if (parsedMsg == null) {
                    onMessageReceived(parsedMsg);
                }
            } else {
                sLogger.e("Unsupported message type");
            }
        } catch (IllegalArgumentException e5) {
            sLogger.e("Cannot parse received message frame", e5);
        }
    }

    private boolean isFlagInField(byte flag, byte flagsField) {
        return (flagsField & flag) != 0;
    }

    private void onMessageReceived(BaseIncomingMessage msg) {
        if (msg instanceof UpdateStatus) {
            onUpdateStatus((UpdateStatus) msg);
        } else if (msg instanceof UpdateTrack) {
            onUpdateTrack((UpdateTrack) msg);
        } else if (msg instanceof UpdateTrackCompleted) {
            onUpdateTrackCompleted((UpdateTrackCompleted) msg);
        } else if (msg instanceof ReturnTrackInfoExtended) {
            onReturnTrackInfoExtended((ReturnTrackInfoExtended) msg);
        } else if (msg instanceof UpdateTrackAlbumArt) {
            onUpdateTrackAlbumArt((UpdateTrackAlbumArt) msg);
        } else if (msg instanceof ReturnTrackAlbumArtSegment) {
            onReturnTrackAlbumArtSegment((ReturnTrackAlbumArtSegment) msg);
        } else if (msg instanceof UpdateTrackElapsed) {
            onUpdateTrackElapsed((UpdateTrackElapsed) msg);
        } else {
            sLogger.w("Received unsupported message from Pandora - ignoring: " + msg.toString());
        }
    }

    private void onUpdateStatus(UpdateStatus msg) {
        Builder resultBuilder = new Builder(this.currentTrack);
        switch (msg.value) {
            case PLAYING:
                resultBuilder.playbackState(MusicPlaybackState.PLAYBACK_PLAYING);
                break;
            case PAUSED:
                resultBuilder.playbackState(MusicPlaybackState.PLAYBACK_PAUSED);
                break;
            default:
                resultBuilder.playbackState(MusicPlaybackState.PLAYBACK_NONE);
                break;
        }
        setCurrentTrack(resultBuilder.build());
        if (resultBuilder.playbackState == MusicPlaybackState.PLAYBACK_NONE) {
            sLogger.w("Receive bad status from Pandora: " + msg.value);
            terminateAndClose();
        }
    }

    private void onUpdateTrackCompleted(UpdateTrackCompleted msg) {
        resetCurrentArtwork();
    }

    private void onUpdateTrack(UpdateTrack msg) {
        if (msg.value != 0) {
            try {
                sendOrQueueMessage(GetTrackInfoExtended.INSTANCE);
            } catch (Exception e) {
                sLogger.e("Cannot send request message for track's info - disconnecting", e);
                terminateAndClose();
            }
        }
    }

    private void onReturnTrackInfoExtended(ReturnTrackInfoExtended msg) {
        String title = msg.title;
        if (TextUtils.isEmpty(title) && TextUtils.isEmpty(msg.album) && TextUtils.isEmpty(msg.artist) && msg.duration > (short) 0) {
            title = this.adTitle;
            resetCurrentArtwork();
        } else if (msg.albumArtLength > 0) {
            requestArtwork();
        }
        setCurrentTrack(new Builder(this.currentTrack).index(Long.valueOf((long) msg.trackToken)).name(title).album(msg.album).author(msg.artist).duration(Integer.valueOf(msg.duration * 1000)).currentPosition(Integer.valueOf(secondsToMilliseconds(msg.elapsed))).isPreviousAllowed(Boolean.valueOf(false)).isNextAllowed(Boolean.valueOf(isFlagInField((byte) 2, msg.permissionFlags))).isOnlineStream(Boolean.valueOf(true)).build());
    }

    private void onUpdateTrackAlbumArt(UpdateTrackAlbumArt msg) {
        if (this.currentTrack != null && this.currentTrack.index != null) {
            if (this.currentTrack.index.longValue() != ((long) msg.trackToken)) {
                sLogger.w("Received update for artwork on the wrong song - ignoring");
            } else {
                requestArtwork();
            }
        }
    }

    private void onReturnTrackAlbumArtSegment(ReturnTrackAlbumArtSegment msg) {
        if (this.currentTrack != null && this.currentTrack.index != null) {
            if (((long) msg.trackToken) == this.currentTrack.index.longValue() && msg.segmentIndex == this.expectedArtworkSegmentIndex) {
                if (this.currentArtwork == null) {
                    if (msg.segmentIndex != (byte) 0) {
                        sLogger.e("Wrong initial artwork's segment sent - ignoring it");
                        return;
                    }
                    this.currentArtwork = new ByteArrayOutputStream();
                }
                try {
                    this.currentArtwork.write(msg.data);
                    this.currentArtwork.flush();
                    this.expectedArtworkSegmentIndex = (byte) (msg.segmentIndex + 1);
                    if (this.expectedArtworkSegmentIndex == msg.totalSegments) {
                        this.bus.post(new PhotoUpdate.Builder().identifier(String.valueOf(msg.trackToken)).photo(ByteString.of(this.currentArtwork.toByteArray())).photoType(PhotoType.PHOTO_ALBUM_ART).build());
                        resetCurrentArtwork();
                        return;
                    }
                    return;
                } catch (IOException e) {
                    sLogger.e("Exception while processing artwork chunk: " + msg.segmentIndex);
                    resetCurrentArtwork();
                    return;
                }
            }
            sLogger.e("Wrong artwork's segment sent - ignoring it");
        }
    }

    private void onUpdateTrackElapsed(UpdateTrackElapsed msg) {
        if (this.currentTrack != null && this.currentTrack.index != null) {
            if (((long) msg.trackToken) != this.currentTrack.index.longValue()) {
                sLogger.e("Elapsed time received for wrong song - ignoring");
                return;
            }
            int elapsed = secondsToMilliseconds(msg.elapsed);
            if (elapsed != this.currentTrack.currentPosition.intValue()) {
                setCurrentTrack(new Builder(this.currentTrack).currentPosition(Integer.valueOf(elapsed)).build());
            }
        }
    }

    private int secondsToMilliseconds(short seconds) {
        return seconds * 1000;
    }

    private void setCurrentTrack(MusicTrackInfo currentTrack) {
        if (!this.currentTrack.equals(currentTrack)) {
            this.currentTrack = currentTrack;
            this.bus.post(this.currentTrack);
        }
    }

    private void resetCurrentArtwork() {
        IOUtils.closeStream(this.currentArtwork);
        this.currentArtwork = null;
        this.expectedArtworkSegmentIndex = (byte) 0;
    }

    private void requestArtwork() {
        resetCurrentArtwork();
        try {
            sendOrQueueMessage(GetTrackAlbumArt.INSTANCE);
        } catch (Exception e) {
            sLogger.e("Exception while requesting pre-loaded artwork - ignoring", e);
        }
    }

    protected void terminateAndClose() {
        sendOrQueueMessage(SessionTerminate.INSTANCE);
        close();
    }

    public void close() {
        if (this.activeAckMessageTimeoutCounter != null) {
            handler.removeCallbacks(this.activeAckMessageTimeoutCounter);
            this.activeAckMessageTimeoutCounter = null;
        }
        resetCurrentArtwork();
        setCurrentTrack(INITIAL_TRACK_INFO_BUILDER.build());
        IOUtils.closeStream(this.socket);
        setSocket(null);
        this.msgQueue.clear();
    }

    @Subscribe
    public void onDeviceConnectionStateChange(ConnectionStateChange connectionStateChange) {
        switch (connectionStateChange.state) {
            case CONNECTION_VERIFIED:
                this.remoteDeviceId = connectionStateChange.remoteDeviceId;
                return;
            case CONNECTION_DISCONNECTED:
                this.remoteDeviceId = null;
                close();
                return;
            default:
                return;
        }
    }

    @Subscribe
    public void onNotificationsStatusUpdate(NotificationsStatusUpdate event) {
        if (!ServiceType.SERVICE_PANDORA.equals(event.service)) {
            return;
        }
        if (this.remoteDeviceId == null) {
            sLogger.e("Received Pandora app state message while remote device ID is unknown");
            return;
        }
        if (Platform.PLATFORM_Android.equals(RemoteDeviceManager.getInstance().getRemoteDevicePlatform())) {
            boolean isClientsPandoraRunning = NotificationsState.NOTIFICATIONS_ENABLED.equals(event.state);
            sLogger.d("Received Pandora's running status: " + isClientsPandoraRunning);
            if (isClientsPandoraRunning) {
                if (isConnected()) {
                    sLogger.i("Pandora is already connected - restarting connection");
                    terminateAndClose();
                }
                startConnecting(false);
                return;
            }
            terminateAndClose();
            return;
        }
        sLogger.e("Pandora is supported only for Android clients for now");
    }

    public void startAndPlay() {
        if (isConnected()) {
            sendOrQueueMessage(EventTrackPlay.INSTANCE);
        } else {
            startConnecting(true);
        }
    }

    private void startConnecting(boolean isStartMusicOn) {
        if (this.isConnectingInProgress.compareAndSet(false, true)) {
            Thread thread = new Thread(new ConnectRunnable(isStartMusicOn));
            thread.setName("Pandora-Connect-" + CONNECT_THREAD_COUNTER.getAndIncrement());
            thread.start();
            return;
        }
        sLogger.w("Connecting thread is already running - ignoring the request");
    }

    public synchronized BluetoothSocket getSocket() {
        return this.socket;
    }

    private synchronized void setSocket(BluetoothSocket newSocket) {
        if (this.socket != newSocket) {
            if (newSocket == null) {
                IOUtils.closeStream(this.socket);
            }
            this.socket = newSocket;
        }
    }

    private boolean isConnected() {
        BluetoothSocket tempSocket = getSocket();
        return tempSocket != null && tempSocket.isConnected();
    }
}
