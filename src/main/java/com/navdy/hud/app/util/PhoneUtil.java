package com.navdy.hud.app.util;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.navdy.hud.app.framework.DriverProfileHelper;

public class PhoneUtil {
    final private static java.util.Map sISOCountryCodeMap;
    final private static com.navdy.service.library.log.Logger sLogger;
    final private static PhoneNumberUtil sPhoneNumberUtil;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(PhoneUtil.class);
        sPhoneNumberUtil = PhoneNumberUtil.getInstance();
        sISOCountryCodeMap = new java.util.HashMap(310);
        sISOCountryCodeMap.put("US", 1);
        sISOCountryCodeMap.put("RU", 7);
        sISOCountryCodeMap.put("KZ", 7);
        sISOCountryCodeMap.put("EG", 20);
        sISOCountryCodeMap.put("ZA", 27);
        sISOCountryCodeMap.put("GR", 30);
        sISOCountryCodeMap.put("NL", 31);
        sISOCountryCodeMap.put("BE", 32);
        sISOCountryCodeMap.put("FR", 33);
        sISOCountryCodeMap.put("ES", 34);
        sISOCountryCodeMap.put("HU", 36);
        sISOCountryCodeMap.put("IT", 39);
        sISOCountryCodeMap.put("VA", 39);
        sISOCountryCodeMap.put("RO", 40);
        sISOCountryCodeMap.put("CH", 41);
        sISOCountryCodeMap.put("AT", 43);
        sISOCountryCodeMap.put("GB", 44);
        sISOCountryCodeMap.put("GG", 44);
        sISOCountryCodeMap.put("IM", 44);
        sISOCountryCodeMap.put("JE", 44);
        sISOCountryCodeMap.put("DK", 45);
        sISOCountryCodeMap.put("SE", 46);
        sISOCountryCodeMap.put("NO", 47);
        sISOCountryCodeMap.put("SJ", 47);
        sISOCountryCodeMap.put("PL", 48);
        sISOCountryCodeMap.put("DE", 49);
        sISOCountryCodeMap.put("PE", 51);
        sISOCountryCodeMap.put("MX", 52);
        sISOCountryCodeMap.put("CU", 53);
        sISOCountryCodeMap.put("AR", 54);
        sISOCountryCodeMap.put("BR", 55);
        sISOCountryCodeMap.put("CL", 56);
        sISOCountryCodeMap.put("CO", 57);
        sISOCountryCodeMap.put("VE", 58);
        sISOCountryCodeMap.put("MY", 60);
        sISOCountryCodeMap.put("AU", 61);
        sISOCountryCodeMap.put("CC", 61);
        sISOCountryCodeMap.put("CX", 61);
        sISOCountryCodeMap.put("ID", 62);
        sISOCountryCodeMap.put("PH", 63);
        sISOCountryCodeMap.put("NZ", 64);
        sISOCountryCodeMap.put("SG", 65);
        sISOCountryCodeMap.put("TH", 66);
        sISOCountryCodeMap.put("JP", 81);
        sISOCountryCodeMap.put("KR", 82);
        sISOCountryCodeMap.put("VN", 84);
        sISOCountryCodeMap.put("CN", 86);
        sISOCountryCodeMap.put("TR", 90);
        sISOCountryCodeMap.put("IN", 91);
        sISOCountryCodeMap.put("PK", 92);
        sISOCountryCodeMap.put("AF", 93);
        sISOCountryCodeMap.put("LK", 94);
        sISOCountryCodeMap.put("MM", 95);
        sISOCountryCodeMap.put("IR", 98);
        sISOCountryCodeMap.put("SS", 211);
        sISOCountryCodeMap.put("MA", 212);
        sISOCountryCodeMap.put("EH", 212);
        sISOCountryCodeMap.put("DZ", 213);
        sISOCountryCodeMap.put("TN", 216);
        sISOCountryCodeMap.put("LY", 218);
        sISOCountryCodeMap.put("GM", 220);
        sISOCountryCodeMap.put("SN", 221);
        sISOCountryCodeMap.put("MR", 222);
        sISOCountryCodeMap.put("ML", 223);
        sISOCountryCodeMap.put("GN", 224);
        sISOCountryCodeMap.put("CI", 225);
        sISOCountryCodeMap.put("BF", 226);
        sISOCountryCodeMap.put("NE", 227);
        sISOCountryCodeMap.put("TG", 228);
        sISOCountryCodeMap.put("BJ", 229);
        sISOCountryCodeMap.put("MU", 230);
        sISOCountryCodeMap.put("LR", 231);
        sISOCountryCodeMap.put("SL", 232);
        sISOCountryCodeMap.put("GH", 233);
        sISOCountryCodeMap.put("NG", 234);
        sISOCountryCodeMap.put("TD", 235);
        sISOCountryCodeMap.put("CF", 236);
        sISOCountryCodeMap.put("CM", 237);
        sISOCountryCodeMap.put("CV", 238);
        sISOCountryCodeMap.put("ST", 239);
        sISOCountryCodeMap.put("GQ", 240);
        sISOCountryCodeMap.put("GA", 241);
        sISOCountryCodeMap.put("CG", 242);
        sISOCountryCodeMap.put("CD", 243);
        sISOCountryCodeMap.put("AO", 244);
        sISOCountryCodeMap.put("GW", 245);
        sISOCountryCodeMap.put("IO", 246);
        sISOCountryCodeMap.put("AC", 247);
        sISOCountryCodeMap.put("SC", 248);
        sISOCountryCodeMap.put("SD", 249);
        sISOCountryCodeMap.put("RW", 250);
        sISOCountryCodeMap.put("ET", 251);
        sISOCountryCodeMap.put("SO", 252);
        sISOCountryCodeMap.put("DJ", 253);
        sISOCountryCodeMap.put("KE", 254);
        sISOCountryCodeMap.put("TZ", 255);
        sISOCountryCodeMap.put("UG", 256);
        sISOCountryCodeMap.put("BI", 257);
        sISOCountryCodeMap.put("MZ", 258);
        sISOCountryCodeMap.put("ZM", 260);
        sISOCountryCodeMap.put("MG", 261);
        sISOCountryCodeMap.put("YT", 262);
        sISOCountryCodeMap.put("KE", 262);
        sISOCountryCodeMap.put("ZW", 263);
        sISOCountryCodeMap.put("NA", 264);
        sISOCountryCodeMap.put("MW", 265);
        sISOCountryCodeMap.put("LS", 266);
        sISOCountryCodeMap.put("BW", 267);
        sISOCountryCodeMap.put("SZ", 268);
        sISOCountryCodeMap.put("KM", 269);
        sISOCountryCodeMap.put("SH", 290);
        sISOCountryCodeMap.put("TA", 290);
        sISOCountryCodeMap.put("ER", 291);
        sISOCountryCodeMap.put("AW", 297);
        sISOCountryCodeMap.put("FO", 298);
        sISOCountryCodeMap.put("GL", 299);
        sISOCountryCodeMap.put("GI", 350);
        sISOCountryCodeMap.put("PT", 351);
        sISOCountryCodeMap.put("LU", 352);
        sISOCountryCodeMap.put("IE", 353);
        sISOCountryCodeMap.put("IS", 354);
        sISOCountryCodeMap.put("AL", 355);
        sISOCountryCodeMap.put("MT", 356);
        sISOCountryCodeMap.put("CY", 357);
        sISOCountryCodeMap.put("FI", 358);
        sISOCountryCodeMap.put("AX", 358);
        sISOCountryCodeMap.put("BG", 359);
        sISOCountryCodeMap.put("LT", 370);
        sISOCountryCodeMap.put("LV", 371);
        sISOCountryCodeMap.put("EE", 372);
        sISOCountryCodeMap.put("MD", 373);
        sISOCountryCodeMap.put("AM", 374);
        sISOCountryCodeMap.put("BY", 375);
        sISOCountryCodeMap.put("AD", 376);
        sISOCountryCodeMap.put("MC", 377);
        sISOCountryCodeMap.put("SM", 378);
        sISOCountryCodeMap.put("UA", 380);
        sISOCountryCodeMap.put("RS", 381);
        sISOCountryCodeMap.put("ME", 382);
        sISOCountryCodeMap.put("HR", 385);
        sISOCountryCodeMap.put("SI", 386);
        sISOCountryCodeMap.put("BA", 387);
        sISOCountryCodeMap.put("MK", 389);
        sISOCountryCodeMap.put("CZ", 420);
        sISOCountryCodeMap.put("SK", 421);
        sISOCountryCodeMap.put("LI", 423);
        sISOCountryCodeMap.put("FK", 500);
        sISOCountryCodeMap.put("BZ", 501);
        sISOCountryCodeMap.put("GT", 502);
        sISOCountryCodeMap.put("SV", 503);
        sISOCountryCodeMap.put("HN", 504);
        sISOCountryCodeMap.put("NI", 505);
        sISOCountryCodeMap.put("CR", 506);
        sISOCountryCodeMap.put("PA", 507);
        sISOCountryCodeMap.put("PM", 508);
        sISOCountryCodeMap.put("HT", 509);
        sISOCountryCodeMap.put("GP", 590);
        sISOCountryCodeMap.put("BL", 590);
        sISOCountryCodeMap.put("MF", 590);
        sISOCountryCodeMap.put("BO", 591);
        sISOCountryCodeMap.put("GY", 592);
        sISOCountryCodeMap.put("EC", 593);
        sISOCountryCodeMap.put("GF", 594);
        sISOCountryCodeMap.put("PY", 595);
        sISOCountryCodeMap.put("MQ", 596);
        sISOCountryCodeMap.put("SR", 597);
        sISOCountryCodeMap.put("UY", 598);
        sISOCountryCodeMap.put("CW", 599);
        sISOCountryCodeMap.put("BQ", 599);
        sISOCountryCodeMap.put("TL", 670);
        sISOCountryCodeMap.put("NF", 672);
        sISOCountryCodeMap.put("BN", 673);
        sISOCountryCodeMap.put("NR", 674);
        sISOCountryCodeMap.put("PG", 675);
        sISOCountryCodeMap.put("TO", 676);
        sISOCountryCodeMap.put("SB", 677);
        sISOCountryCodeMap.put("VU", 678);
        sISOCountryCodeMap.put("FJ", 679);
        sISOCountryCodeMap.put("PW", 680);
        sISOCountryCodeMap.put("WF", 681);
        sISOCountryCodeMap.put("CK", 682);
        sISOCountryCodeMap.put("NU", 683);
        sISOCountryCodeMap.put("WS", 685);
        sISOCountryCodeMap.put("KI", 686);
        sISOCountryCodeMap.put("NC", 687);
        sISOCountryCodeMap.put("TV", 688);
        sISOCountryCodeMap.put("PF", 689);
        sISOCountryCodeMap.put("TK", 690);
        sISOCountryCodeMap.put("FM", 691);
        sISOCountryCodeMap.put("MH", 692);
        sISOCountryCodeMap.put("KP", 850);
        sISOCountryCodeMap.put("HK", 852);
        sISOCountryCodeMap.put("MO", 853);
        sISOCountryCodeMap.put("KH", 855);
        sISOCountryCodeMap.put("LA", 856);
        sISOCountryCodeMap.put("BD", 880);
        sISOCountryCodeMap.put("TW", 886);
        sISOCountryCodeMap.put("MV", 960);
        sISOCountryCodeMap.put("LB", 961);
        sISOCountryCodeMap.put("JO", 962);
        sISOCountryCodeMap.put("SY", 963);
        sISOCountryCodeMap.put("IQ", 964);
        sISOCountryCodeMap.put("KW", 965);
        sISOCountryCodeMap.put("SA", 966);
        sISOCountryCodeMap.put("YE", 967);
        sISOCountryCodeMap.put("OM", 968);
        sISOCountryCodeMap.put("PS", 970);
        sISOCountryCodeMap.put("AE", 971);
        sISOCountryCodeMap.put("IL", 972);
        sISOCountryCodeMap.put("BH", 973);
        sISOCountryCodeMap.put("QA", 974);
        sISOCountryCodeMap.put("BT", 975);
        sISOCountryCodeMap.put("MN", 976);
        sISOCountryCodeMap.put("NP", 977);
        sISOCountryCodeMap.put("TJ", 992);
        sISOCountryCodeMap.put("TM", 993);
        sISOCountryCodeMap.put("AZ", 994);
        sISOCountryCodeMap.put("GE", 995);
        sISOCountryCodeMap.put("KG", 996);
        sISOCountryCodeMap.put("UZ", 998);
    }
    
    public PhoneUtil() {
    }
    
    public static String convertToE164Format(String s) {
        try {
            java.util.Locale a = DriverProfileHelper.getInstance().getCurrentLocale();
            Phonenumber.PhoneNumber a0 = sPhoneNumberUtil.parse(s, a.getCountry());
            s = sPhoneNumberUtil.format(a0, PhoneNumberUtil.PhoneNumberFormat.E164);
        } catch(Throwable a1) {
            sLogger.e(a1);
        }
        return s;
    }
    
    public static String formatPhoneNumber(String s) {
        s = "";
        try {
            if (android.text.TextUtils.isEmpty(s)) {
                return s;
            } else {
                boolean b;
                boolean b0;
                java.util.Locale a0 = DriverProfileHelper.getInstance().getCurrentLocale();
                int i = PhoneUtil.getCountryPhoneCode(a0.getCountry());
                Phonenumber.PhoneNumber a1 = sPhoneNumberUtil.parse(s, a0.getCountry());
                label3: {
                    if (i <= 0) {
                        b0 = true;
                        break label3;
                    }
                    if (!a1.hasCountryCode()) {
                        b0 = true;
                        break label3;
                    }
                    int i0 = a1.getCountryCode();
                    label2: {
                        if (i0 != i) {
                            break label2;
                        }
                        b0 = true;
                        break label3;
                    }
                    boolean b1 = s.startsWith(String.valueOf(i0));
                    label1: {
                        if (b1) {
                            break label1;
                        }
                        return s;
                    }
                    b0 = false;
                }
                s = b0 ? sPhoneNumberUtil.format(a1, PhoneNumberUtil.PhoneNumberFormat.NATIONAL) : sPhoneNumberUtil.format(a1, PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL);
                return s;
            }
        } catch(Throwable a2) {
            if (!sLogger.isLoggable(2)) {
                return s;
            }
            sLogger.e(a2);
            return s;

        }
    }
    
    private static int getCountryPhoneCode(String s) {
        int i;
        if (android.text.TextUtils.isEmpty(s)) {
            i = -1;
        } else {
            Integer a = (Integer)sISOCountryCodeMap.get(s);
            i = (a == null) ? -1 : a;
        }
        return i;
    }
    
    public static String normalizeNumber(String s) {
        if (!android.text.TextUtils.isEmpty(s)) {
            s = s.replaceAll("[^0-9]+", "");
        }
        return s;
    }
}
