package com.navdy.hud.app.util.picasso;

abstract public interface PicassoItemCacheListener {
    abstract public void itemAdded(Object arg);
    
    
    abstract public void itemRemoved(Object arg);
}
