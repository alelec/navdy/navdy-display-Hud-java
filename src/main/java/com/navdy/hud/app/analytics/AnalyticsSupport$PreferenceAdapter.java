package com.navdy.hud.app.analytics;

abstract interface AnalyticsSupport$PreferenceAdapter {
    abstract public com.navdy.hud.app.analytics.Event calculateState(com.navdy.hud.app.profile.DriverProfile arg);
}
