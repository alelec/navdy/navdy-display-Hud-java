package com.navdy.hud.app.screen;

import android.content.SharedPreferences;
import android.os.Bundle;

import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.gesture.GestureServiceConnector;
import com.navdy.hud.app.ui.framework.BasePresenter;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.view.GestureLearningView;
import com.navdy.hud.app.view.GestureVideoCaptureView;
import com.navdy.hud.app.view.LearnGestureScreenLayout;
import com.navdy.hud.app.view.ScrollableTextPresenterLayout;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.events.ui.ShowScreen.Builder;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import flow.Layout;
import javax.inject.Inject;
import javax.inject.Singleton;

@Layout(R.layout.screen_learn_gesture_layout)
public class GestureLearningScreen extends BaseScreen {
    private static final Logger sLogger = new Logger(GestureLearningScreen.class);

    @dagger.Module(addsTo = com.navdy.hud.app.ui.activity.Main.Module.class, injects = {LearnGestureScreenLayout.class, GestureLearningView.class, ScrollableTextPresenterLayout.class, GestureVideoCaptureView.class})
    public class Module {
    }

    @Singleton
    public static class Presenter extends BasePresenter<LearnGestureScreenLayout> {
        @Inject
        GestureServiceConnector gestureServiceConnector;
        @Inject
        Bus mBus;
        @Inject
        SharedPreferences mPreferences;
        Bundle tipsBundle;
        @Inject
        UIStateManager uiStateManager;

        public void onLoad(Bundle savedInstanceState) {
            GestureLearningScreen.sLogger.v("onLoad");
            super.onLoad(savedInstanceState);
            this.uiStateManager.enableSystemTray(false);
            NotificationManager.getInstance().enableNotifications(false);
            ToastManager.getInstance().disableToasts(true);
        }

        protected void onUnload() {
            GestureLearningScreen.sLogger.v("onUnload");
            this.uiStateManager.enableSystemTray(true);
            NotificationManager.getInstance().enableNotifications(true);
            ToastManager.getInstance().disableToasts(false);
            super.onUnload();
        }

        public void showTips() {
            LearnGestureScreenLayout view = (LearnGestureScreenLayout) getView();
            if (view != null) {
                view.showTips();
            }
        }

        public void hideTips() {
            LearnGestureScreenLayout view = (LearnGestureScreenLayout) getView();
            if (view != null) {
                view.hideTips();
            }
        }

        public void showCameraSensorBlocked() {
            LearnGestureScreenLayout view = (LearnGestureScreenLayout) getView();
            if (view != null) {
                view.showSensorBlocked();
            }
        }

        public void hideCameraSensorBlocked() {
            LearnGestureScreenLayout view = (LearnGestureScreenLayout) getView();
            if (view != null) {
                view.hideSensorBlocked();
            }
        }

        public void showCaptureView() {
            LearnGestureScreenLayout view = (LearnGestureScreenLayout) getView();
            if (view != null) {
                view.showCaptureGestureVideosView();
            }
        }

        public void hideCaptureView() {
            LearnGestureScreenLayout view = (LearnGestureScreenLayout) getView();
            if (view != null) {
                view.hideCaptureGestureVideosView();
            }
        }

        public void finish() {
            this.mBus.post(new Builder().screen(Screen.SCREEN_BACK).build());
        }
    }

    public Screen getScreen() {
        return Screen.SCREEN_GESTURE_LEARNING;
    }

    public String getMortarScopeName() {
        return GestureLearningScreen.class.getSimpleName();
    }

    public Object getDaggerModule() {
        return new Module();
    }
}
