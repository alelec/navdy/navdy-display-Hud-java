package com.navdy.hud.app.bluetooth.vcard;

import java.io.ObjectInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import android.provider.ContactsContract;
import android.content.ContentProviderOperation;
import android.provider.ContactsContract;
import android.content.ContentProviderOperation;
import com.navdy.hud.app.util.DateUtil;
import android.text.TextUtils;
import java.util.Iterator;
import android.util.Log;
import java.util.Arrays;
import java.util.Collection;
import android.net.Uri;
import android.provider.ContactsContract;
import android.content.ContentResolver;
import java.util.Collections;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Date;
import android.accounts.Account;
import java.util.Map;
import java.util.List;

public class VCardEntry
{
    private static final int DEFAULT_ORGANIZATION_TYPE = 1;
    private static final String LOG_TAG = "vCard";
    private static final List<String> sEmptyList;
    private static final Map<String, Integer> sImMap;
    private final Account mAccount;
    private List<AndroidCustomData> mAndroidCustomDataList;
    private AnniversaryData mAnniversary;
    private BirthdayData mBirthday;
    private Date mCallTime;
    private int mCallType;
    private List<VCardEntry> mChildren;
    private List<EmailData> mEmailList;
    private List<ImData> mImList;
    private final NameData mNameData;
    private List<NicknameData> mNicknameList;
    private List<NoteData> mNoteList;
    private List<OrganizationData> mOrganizationList;
    private List<PhoneData> mPhoneList;
    private List<PhotoData> mPhotoList;
    private List<PostalData> mPostalList;
    private List<SipData> mSipList;
    private final int mVCardType;
    private List<WebsiteData> mWebsiteList;
    
    static {
        (sImMap = new HashMap<String, Integer>()).put("X-AIM", 0);
        VCardEntry.sImMap.put("X-MSN", 1);
        VCardEntry.sImMap.put("X-YAHOO", 2);
        VCardEntry.sImMap.put("X-ICQ", 6);
        VCardEntry.sImMap.put("X-JABBER", 7);
        VCardEntry.sImMap.put("X-SKYPE-USERNAME", 3);
        VCardEntry.sImMap.put("X-GOOGLE-TALK", 5);
        VCardEntry.sImMap.put("X-GOOGLE TALK", 5);
        sEmptyList = Collections.<String>unmodifiableList((List<? extends String>)new ArrayList<String>(0));
    }
    
    public VCardEntry() {
        this(-1073741824);
    }
    
    public VCardEntry(final int n) {
        this(n, null);
    }
    
    public VCardEntry(final int mvCardType, final Account mAccount) {
        this.mNameData = new NameData();
        this.mVCardType = mvCardType;
        this.mAccount = mAccount;
    }
    
    private void addCallTime(final int mCallType, final Date mCallTime) {
        this.mCallType = mCallType;
        this.mCallTime = mCallTime;
    }
    
    private void addEmail(final int n, final String s, final String s2, final boolean b) {
        if (this.mEmailList == null) {
            this.mEmailList = new ArrayList<EmailData>();
        }
        this.mEmailList.add(new EmailData(s, n, s2, b));
    }
    
    private void addIm(final int n, final String s, final String s2, final int n2, final boolean b) {
        if (this.mImList == null) {
            this.mImList = new ArrayList<ImData>();
        }
        this.mImList.add(new ImData(n, s, s2, n2, b));
    }
    
    private void addNewOrganization(final String s, final String s2, final String s3, final String s4, final int n, final boolean b) {
        if (this.mOrganizationList == null) {
            this.mOrganizationList = new ArrayList<OrganizationData>();
        }
        this.mOrganizationList.add(new OrganizationData(s, s2, s3, s4, n, b));
    }
    
    private void addNickName(final String s) {
        if (this.mNicknameList == null) {
            this.mNicknameList = new ArrayList<NicknameData>();
        }
        this.mNicknameList.add(new NicknameData(s));
    }
    
    private void addNote(final String s) {
        if (this.mNoteList == null) {
            this.mNoteList = new ArrayList<NoteData>(1);
        }
        this.mNoteList.add(new NoteData(s));
    }
    
    private void addPhone(final int n, String s, final String s2, final boolean b) {
        if (this.mPhoneList == null) {
            this.mPhoneList = new ArrayList<PhoneData>();
        }
        final StringBuilder sb = new StringBuilder();
        s = s.trim();
        if (n != 6 && !VCardConfig.refrainPhoneNumberFormatting(this.mVCardType)) {
            int n2 = 0;
            int n3;
            for (int length = s.length(), i = 0; i < length; ++i, n2 = n3) {
                final char char1 = s.charAt(i);
                if (char1 == 'p' || char1 == 'P') {
                    sb.append(',');
                    n3 = 1;
                }
                else if (char1 == 'w' || char1 == 'W') {
                    sb.append(';');
                    n3 = 1;
                }
                else {
                    if ('0' > char1 || char1 > '9') {
                        n3 = n2;
                        if (i != 0) {
                            continue;
                        }
                        n3 = n2;
                        if (char1 != '+') {
                            continue;
                        }
                    }
                    sb.append(char1);
                    n3 = n2;
                }
            }
            if (n2 == 0) {
                s = VCardUtils.PhoneNumberUtilsPort.formatNumber(sb.toString(), VCardUtils.getPhoneNumberFormat(this.mVCardType));
            }
            else {
                s = sb.toString();
            }
        }
        this.mPhoneList.add(new PhoneData(s, n, s2, b));
    }
    
    private void addPhotoBytes(final String s, final byte[] array, final boolean b) {
        if (this.mPhotoList == null) {
            this.mPhotoList = new ArrayList<PhotoData>(1);
        }
        this.mPhotoList.add(new PhotoData(s, array, b));
    }
    
    private void addPostal(final int n, final List<String> list, final String s, final boolean b) {
        if (this.mPostalList == null) {
            this.mPostalList = new ArrayList<PostalData>(0);
        }
        this.mPostalList.add(PostalData.constructPostalData(list, n, s, b, this.mVCardType));
    }
    
    private void addSip(final String s, final int n, final String s2, final boolean b) {
        if (this.mSipList == null) {
            this.mSipList = new ArrayList<SipData>();
        }
        this.mSipList.add(new SipData(s, n, s2, b));
    }
    
    public static VCardEntry buildFromResolver(final ContentResolver contentResolver) {
        return buildFromResolver(contentResolver, ContactsContract.Contacts.CONTENT_URI);
    }
    
    public static VCardEntry buildFromResolver(final ContentResolver contentResolver, final Uri uri) {
        return null;
    }
    
    private String buildSinglePhoneticNameFromSortAsParam(final Map<String, Collection<String>> map) {
        final Collection<String> collection = map.get("SORT-AS");
        String string;
        if (collection != null && collection.size() != 0) {
            if (collection.size() > 1) {
                Log.w("vCard", "Incorrect multiple SORT_AS parameters detected: " + Arrays.toString(collection.toArray()));
            }
            final List<String> constructListFromValue = VCardUtils.constructListFromValue(collection.iterator().next(), this.mVCardType);
            final StringBuilder sb = new StringBuilder();
            final Iterator<String> iterator = constructListFromValue.iterator();
            while (iterator.hasNext()) {
                sb.append(iterator.next());
            }
            string = sb.toString();
        }
        else {
            string = null;
        }
        return string;
    }
    
    private String constructDisplayName() {
        final String s = null;
        String s2;
        if (!TextUtils.isEmpty((CharSequence)this.mNameData.mFormatted)) {
            s2 = this.mNameData.mFormatted;
        }
        else if (!this.mNameData.emptyStructuredName()) {
            s2 = VCardUtils.constructNameFromElements(this.mVCardType, this.mNameData.mFamily, this.mNameData.mMiddle, this.mNameData.mGiven, this.mNameData.mPrefix, this.mNameData.mSuffix);
        }
        else if (!this.mNameData.emptyPhoneticStructuredName()) {
            s2 = VCardUtils.constructNameFromElements(this.mVCardType, this.mNameData.mPhoneticFamily, this.mNameData.mPhoneticMiddle, this.mNameData.mPhoneticGiven);
        }
        else if (this.mEmailList != null && this.mEmailList.size() > 0) {
            s2 = this.mEmailList.get(0).mAddress;
        }
        else if (this.mPhoneList != null && this.mPhoneList.size() > 0) {
            s2 = this.mPhoneList.get(0).mNumber;
        }
        else if (this.mPostalList != null && this.mPostalList.size() > 0) {
            s2 = this.mPostalList.get(0).getFormattedAddress(this.mVCardType);
        }
        else {
            s2 = s;
            if (this.mOrganizationList != null) {
                s2 = s;
                if (this.mOrganizationList.size() > 0) {
                    s2 = this.mOrganizationList.get(0).getFormattedString();
                }
            }
        }
        String s3;
        if ((s3 = s2) == null) {
            s3 = "";
        }
        return s3;
    }
    
    private void handleAndroidCustomProperty(final List<String> list) {
        if (this.mAndroidCustomDataList == null) {
            this.mAndroidCustomDataList = new ArrayList<AndroidCustomData>();
        }
        this.mAndroidCustomDataList.add(AndroidCustomData.constructAndroidCustomData(list));
    }
    
    private void handleNProperty(final List<String> list, final Map<String, Collection<String>> map) {
        this.tryHandleSortAsName(map);
        if (list != null) {
            final int size = list.size();
            if (size >= 1) {
                int n;
                if ((n = size) > 5) {
                    n = 5;
                }
                switch (n) {
                    case 5:
                        this.mNameData.mSuffix = list.get(4);
                    case 4:
                        this.mNameData.mPrefix = list.get(3);
                    case 3:
                        this.mNameData.mMiddle = list.get(2);
                    case 2:
                        this.mNameData.mGiven = list.get(1);
                        break;
                }
                this.mNameData.mFamily = list.get(0);
            }
        }
    }
    
    private void handleOrgValue(final int n, final List<String> list, final Map<String, Collection<String>> map, final boolean b) {
        final String buildSinglePhoneticNameFromSortAsParam = this.buildSinglePhoneticNameFromSortAsParam(map);
        List<String> sEmptyList = list;
        if (list == null) {
            sEmptyList = VCardEntry.sEmptyList;
        }
        final int size = sEmptyList.size();
        String s = null;
        String string = null;
        switch (size) {
            default: {
                s = sEmptyList.get(0);
                final StringBuilder sb = new StringBuilder();
                for (int i = 1; i < size; ++i) {
                    if (i > 1) {
                        sb.append(' ');
                    }
                    sb.append(sEmptyList.get(i));
                }
                string = sb.toString();
                break;
            }
            case 0:
                s = "";
                string = null;
                break;
            case 1:
                s = sEmptyList.get(0);
                string = null;
                break;
        }
        if (this.mOrganizationList == null) {
            this.addNewOrganization(s, string, null, buildSinglePhoneticNameFromSortAsParam, n, b);
        }
        else {
            for (final OrganizationData organizationData : this.mOrganizationList) {
                if (organizationData.mOrganizationName == null && organizationData.mDepartmentName == null) {
                    organizationData.mOrganizationName = s;
                    organizationData.mDepartmentName = string;
                    organizationData.mIsPrimary = b;
                    return;
                }
            }
            this.addNewOrganization(s, string, null, buildSinglePhoneticNameFromSortAsParam, n, b);
        }
    }

    private void handlePhoneticNameFromSound(List<String> elems) {
        if (TextUtils.isEmpty(this.mNameData.mPhoneticFamily) && TextUtils.isEmpty(this.mNameData.mPhoneticMiddle) && TextUtils.isEmpty(this.mNameData.mPhoneticGiven) && elems != null) {
            int size = elems.size();
            if (size >= 1) {
                if (size > 3) {
                    size = 3;
                }
                if (((String) elems.get(0)).length() > 0) {
                    boolean onlyFirstElemIsNonEmpty = true;
                    for (int i = 1; i < size; i++) {
                        if (((String) elems.get(i)).length() > 0) {
                            onlyFirstElemIsNonEmpty = false;
                            break;
                        }
                    }
                    if (onlyFirstElemIsNonEmpty) {
                        String[] namesArray = ((String) elems.get(0)).split(" ");
                        int nameArrayLength = namesArray.length;
                        if (nameArrayLength == 3) {
                            this.mNameData.mPhoneticFamily = namesArray[0];
                            this.mNameData.mPhoneticMiddle = namesArray[1];
                            this.mNameData.mPhoneticGiven = namesArray[2];
                            return;
                        } else if (nameArrayLength == 2) {
                            this.mNameData.mPhoneticFamily = namesArray[0];
                            this.mNameData.mPhoneticGiven = namesArray[1];
                            return;
                        } else {
                            this.mNameData.mPhoneticGiven = (String) elems.get(0);
                            return;
                        }
                    }
                }
                switch (size) {
                    case 2:
                        break;
                    case 3:
                        this.mNameData.mPhoneticMiddle = (String) elems.get(2);
                        break;
                }
                this.mNameData.mPhoneticGiven = (String) elems.get(1);
                this.mNameData.mPhoneticFamily = (String) elems.get(0);
            }
        }
    }

    private void handleSipCase(String propValue, Collection<String> typeCollection) {
        if (!TextUtils.isEmpty(propValue)) {
            if (propValue.startsWith("sip:")) {
                propValue = propValue.substring(4);
                if (propValue.length() == 0) {
                    return;
                }
            }
            int type = -1;
            String label = null;
            boolean isPrimary = false;
            if (typeCollection != null) {
                for (String typeStringOrg : typeCollection) {
                    String typeStringUpperCase = typeStringOrg.toUpperCase();
                    if (typeStringUpperCase.equals(VCardConstants.PARAM_TYPE_PREF)) {
                        isPrimary = true;
                    } else if (typeStringUpperCase.equals(VCardConstants.PARAM_TYPE_HOME)) {
                        type = 1;
                    } else if (typeStringUpperCase.equals(VCardConstants.PARAM_TYPE_WORK)) {
                        type = 2;
                    } else if (type < 0) {
                        if (typeStringUpperCase.startsWith("X-")) {
                            label = typeStringOrg.substring(2);
                        } else {
                            label = typeStringOrg;
                        }
                        type = 0;
                    }
                }
            }
            if (type < 0) {
                type = 3;
            }
            addSip(propValue, type, label, isPrimary);
        }
    }
    
    private void handleTitleValue(final String s) {
        if (this.mOrganizationList == null) {
            this.addNewOrganization(null, null, s, null, 1, false);
        }
        else {
            for (final OrganizationData organizationData : this.mOrganizationList) {
                if (organizationData.mTitle == null) {
                    organizationData.mTitle = s;
                    return;
                }
            }
            this.addNewOrganization(null, null, s, null, 1, false);
        }
    }
    
    private void iterateOneList(final List<? extends EntryElement> list, final EntryElementIterator entryElementIterator) {
        if (list != null && list.size() > 0) {
            entryElementIterator.onElementGroupStarted(((EntryElement)list.get(0)).getEntryLabel());
            final Iterator<? extends EntryElement> iterator = list.iterator();
            while (iterator.hasNext()) {
                entryElementIterator.onElement((EntryElement)iterator.next());
            }
            entryElementIterator.onElementGroupEnded();
        }
    }
    
    private String listToString(final List<String> list) {
        final int size = list.size();
        String string;
        if (size > 1) {
            final StringBuilder sb = new StringBuilder();
            final Iterator<String> iterator = list.iterator();
            while (iterator.hasNext()) {
                sb.append(iterator.next());
                if (size - 1 < 0) {
                    sb.append(";");
                }
            }
            string = sb.toString();
        }
        else if (size == 1) {
            string = list.get(0);
        }
        else {
            string = "";
        }
        return string;
    }
    
    private void tryHandleSortAsName(final Map<String, Collection<String>> map) {
        if (!VCardConfig.isVersion30(this.mVCardType) || (TextUtils.isEmpty((CharSequence)this.mNameData.mPhoneticFamily) && TextUtils.isEmpty((CharSequence)this.mNameData.mPhoneticMiddle) && TextUtils.isEmpty((CharSequence)this.mNameData.mPhoneticGiven))) {
            final Collection<String> collection = map.get("SORT-AS");
            if (collection != null && collection.size() != 0) {
                if (collection.size() > 1) {
                    Log.w("vCard", "Incorrect multiple SORT_AS parameters detected: " + Arrays.toString(collection.toArray()));
                }
                final List<String> constructListFromValue = VCardUtils.constructListFromValue(collection.iterator().next(), this.mVCardType);
                int size;
                if ((size = constructListFromValue.size()) > 3) {
                    size = 3;
                }
                switch (size) {
                    case 3:
                        this.mNameData.mPhoneticMiddle = constructListFromValue.get(2);
                    case 2:
                        this.mNameData.mPhoneticGiven = constructListFromValue.get(1);
                        break;
                }
                this.mNameData.mPhoneticFamily = constructListFromValue.get(0);
            }
        }
    }
    
    public void addChild(final VCardEntry vCardEntry) {
        if (this.mChildren == null) {
            this.mChildren = new ArrayList<VCardEntry>();
        }
        this.mChildren.add(vCardEntry);
    }

    public void addProperty(VCardProperty property) {
        String propertyName = property.getName();
        Map<String, Collection<String>> paramMap = property.getParameterMap();
        List<String> propertyValueList = property.getValueList();
        byte[] propertyBytes = property.getByteValue();
        if ((propertyValueList != null && propertyValueList.size() != 0) || propertyBytes != null) {
            String propValue = propertyValueList != null ? listToString(propertyValueList).trim() : null;
            if (!propertyName.equals(VCardConstants.PROPERTY_VERSION)) {
                if (propertyName.equals(VCardConstants.PROPERTY_FN)) {
                    this.mNameData.mFormatted = propValue;
                    return;
                }
                if (!propertyName.equals(VCardConstants.PROPERTY_NAME)) {
                    if (propertyName.equals(VCardConstants.PROPERTY_N)) {
                        handleNProperty(propertyValueList, paramMap);
                        return;
                    }
                    if (propertyName.equals(VCardConstants.PROPERTY_SORT_STRING)) {
                        this.mNameData.mSortString = propValue;
                        return;
                    }
                    if (!propertyName.equals(VCardConstants.PROPERTY_NICKNAME)) {
                        if (!propertyName.equals(VCardConstants.ImportOnly.PROPERTY_X_NICKNAME)) {
                            Collection<String> typeCollection;
                            if (propertyName.equals(VCardConstants.PROPERTY_SOUND)) {
                                typeCollection = paramMap.get(VCardConstants.PARAM_TYPE);
                                if (typeCollection != null) {
                                    if (typeCollection.contains("X-IRMC-N")) {
                                        handlePhoneticNameFromSound(VCardUtils.constructListFromValue(propValue, this.mVCardType));
                                        return;
                                    }
                                    return;
                                }
                                return;
                            }
                            int type;
                            String label;
                            boolean isPrimary;
                            String typeStringUpperCase;
                            if (propertyName.equals(VCardConstants.PROPERTY_ADR)) {
                                boolean valuesAreAllEmpty = true;
                                for (String value : propertyValueList) {
                                    if (!TextUtils.isEmpty(value)) {
                                        valuesAreAllEmpty = false;
                                        break;
                                    }
                                }
                                if (!valuesAreAllEmpty) {
                                    type = -1;
                                    label = null;
                                    isPrimary = false;
                                    typeCollection = (Collection) paramMap.get(VCardConstants.PARAM_TYPE);
                                    if (typeCollection != null) {
                                        for (String typeStringOrg : typeCollection) {
                                            typeStringUpperCase = typeStringOrg.toUpperCase();
                                            if (typeStringUpperCase.equals(VCardConstants.PARAM_TYPE_PREF)) {
                                                isPrimary = true;
                                            } else {
                                                if (typeStringUpperCase.equals(VCardConstants.PARAM_TYPE_HOME)) {
                                                    type = 1;
                                                    label = null;
                                                } else {
                                                    if (!typeStringUpperCase.equals(VCardConstants.PARAM_TYPE_WORK)) {
                                                        if (!typeStringUpperCase.equalsIgnoreCase(VCardConstants.PARAM_EXTRA_TYPE_COMPANY)) {
                                                            if (!typeStringUpperCase.equals(VCardConstants.PARAM_ADR_TYPE_PARCEL)) {
                                                                if (!typeStringUpperCase.equals(VCardConstants.PARAM_ADR_TYPE_DOM)) {
                                                                    if (!typeStringUpperCase.equals(VCardConstants.PARAM_ADR_TYPE_INTL) && type < 0) {
                                                                        type = 0;
                                                                        if (typeStringUpperCase.startsWith("X-")) {
                                                                            label = typeStringOrg.substring(2);
                                                                        } else {
                                                                            label = typeStringOrg;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    type = 2;
                                                    label = null;
                                                }
                                            }
                                        }
                                    }
                                    if (type < 0) {
                                        type = 1;
                                    }
                                    addPostal(type, propertyValueList, label, isPrimary);
                                    return;
                                }
                                return;
                            }
                            if (propertyName.equals(VCardConstants.PROPERTY_EMAIL)) {
                                type = -1;
                                label = null;
                                isPrimary = false;
                                typeCollection = (Collection) paramMap.get(VCardConstants.PARAM_TYPE);
                                if (typeCollection != null) {
                                    for (String typeStringOrg2 : typeCollection) {
                                        typeStringUpperCase = typeStringOrg2.toUpperCase();
                                        if (typeStringUpperCase.equals(VCardConstants.PARAM_TYPE_PREF)) {
                                            isPrimary = true;
                                        } else {
                                            if (typeStringUpperCase.equals(VCardConstants.PARAM_TYPE_HOME)) {
                                                type = 1;
                                            } else {
                                                if (typeStringUpperCase.equals(VCardConstants.PARAM_TYPE_WORK)) {
                                                    type = 2;
                                                } else {
                                                    if (typeStringUpperCase.equals(VCardConstants.PARAM_TYPE_CELL)) {
                                                        type = 4;
                                                    } else if (type < 0) {
                                                        if (typeStringUpperCase.startsWith("X-")) {
                                                            label = typeStringOrg2.substring(2);
                                                        } else {
                                                            label = typeStringOrg2;
                                                        }
                                                        type = 0;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                if (type < 0) {
                                    type = 3;
                                }
                                addEmail(type, propValue, label, isPrimary);
                                return;
                            }
                            if (propertyName.equals(VCardConstants.PROPERTY_ORG)) {
                                isPrimary = false;
                                typeCollection = (Collection) paramMap.get(VCardConstants.PARAM_TYPE);
                                if (typeCollection != null) {
                                    for (String equals : typeCollection) {
                                        if (equals.equals(VCardConstants.PARAM_TYPE_PREF)) {
                                            isPrimary = true;
                                        }
                                    }
                                }
                                handleOrgValue(1, propertyValueList, paramMap, isPrimary);
                                return;
                            }
                            if (propertyName.equals(VCardConstants.PROPERTY_TITLE)) {
                                handleTitleValue(propValue);
                                return;
                            }
                            if (!propertyName.equals(VCardConstants.PROPERTY_ROLE)) {
                                if (!propertyName.equals(VCardConstants.PROPERTY_PHOTO)) {
                                    if (!propertyName.equals(VCardConstants.PROPERTY_LOGO)) {
                                        if (propertyName.equals(VCardConstants.PROPERTY_TEL)) {
                                            String phoneNumber = null;
                                            boolean isSip = false;
                                            if (!VCardConfig.isVersion40(this.mVCardType)) {
                                                phoneNumber = propValue;
                                            } else if (propValue.startsWith("sip:")) {
                                                isSip = true;
                                            } else if (propValue.startsWith("tel:")) {
                                                phoneNumber = propValue.substring(4);
                                            } else {
                                                phoneNumber = propValue;
                                            }
                                            if (isSip) {
                                                handleSipCase(propValue, (Collection) paramMap.get(VCardConstants.PARAM_TYPE));
                                                return;
                                            } else if (propValue.length() != 0) {
                                                typeCollection = (Collection) paramMap.get(VCardConstants.PARAM_TYPE);
                                                Object typeObject = VCardUtils.getPhoneTypeFromStrings(typeCollection, phoneNumber);
                                                if (typeObject instanceof Integer) {
                                                    type = (Integer) typeObject;
                                                    label = null;
                                                } else {
                                                    type = 0;
                                                    label = typeObject.toString();
                                                }
                                                if (typeCollection != null) {
                                                    if (typeCollection.contains(VCardConstants.PARAM_TYPE_PREF)) {
                                                        isPrimary = true;
                                                        addPhone(type, phoneNumber, label, isPrimary);
                                                        return;
                                                    }
                                                }
                                                isPrimary = false;
                                                addPhone(type, phoneNumber, label, isPrimary);
                                                return;
                                            } else {
                                                return;
                                            }
                                        }
                                        if (propertyName.equals(VCardConstants.PROPERTY_DATETIME)) {
                                            String dateTimeStr = propValue;
                                            if (!TextUtils.isEmpty(dateTimeStr)) {
                                                typeCollection = (Collection) paramMap.get(VCardConstants.PARAM_TYPE);
                                                int callType = 0;
                                                if (typeCollection == null || typeCollection.size() > 0) {
                                                    String o = typeCollection.iterator().next();
                                                    if (o instanceof String) {
                                                        String str = o;
                                                        if (VCardConstants.DIALED.equalsIgnoreCase(str)) {
                                                            callType = 11;
                                                        } else if (VCardConstants.MISSED.equalsIgnoreCase(str)) {
                                                            callType = 12;
                                                        } else if (VCardConstants.RECEIVED.equalsIgnoreCase(str)) {
                                                            callType = 10;
                                                        }
                                                    }
                                                }
                                                Date date = DateUtil.parseIrmcDateStr(dateTimeStr);
                                                if (date != null) {
                                                    addCallTime(callType, date);
                                                    return;
                                                }
                                                return;
                                            }
                                            return;
                                        }
                                        if (propertyName.equals(VCardConstants.PROPERTY_X_SKYPE_PSTNNUMBER)) {
                                            typeCollection = (Collection) paramMap.get(VCardConstants.PARAM_TYPE);
                                            if (typeCollection != null) {
                                                if (typeCollection.contains(VCardConstants.PARAM_TYPE_PREF)) {
                                                    isPrimary = true;
                                                    addPhone(7, propValue, null, isPrimary);
                                                    return;
                                                }
                                            }
                                            isPrimary = false;
                                            addPhone(7, propValue, null, isPrimary);
                                            return;
                                        } else if (sImMap.containsKey(propertyName)) {
                                            int protocol = ((Integer) sImMap.get(propertyName)).intValue();
                                            isPrimary = false;
                                            type = -1;
                                            typeCollection = (Collection) paramMap.get(VCardConstants.PARAM_TYPE);
                                            if (typeCollection != null) {
                                                for (String typeString : typeCollection) {
                                                    if (typeString.equals(VCardConstants.PARAM_TYPE_PREF)) {
                                                        isPrimary = true;
                                                    } else if (type < 0) {
                                                        if (typeString.equalsIgnoreCase(VCardConstants.PARAM_TYPE_HOME)) {
                                                            type = 1;
                                                        } else {
                                                            if (typeString.equalsIgnoreCase(VCardConstants.PARAM_TYPE_WORK)) {
                                                                type = 2;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            if (type < 0) {
                                                type = 1;
                                            }
                                            addIm(protocol, null, propValue, type, isPrimary);
                                            return;
                                        } else {
                                            if (propertyName.equals(VCardConstants.PROPERTY_NOTE)) {
                                                addNote(propValue);
                                                return;
                                            }
                                            if (propertyName.equals("URL")) {
                                                if (this.mWebsiteList == null) {
                                                    this.mWebsiteList = new ArrayList(1);
                                                }
                                                this.mWebsiteList.add(new WebsiteData(propValue));
                                                return;
                                            }
                                            if (propertyName.equals(VCardConstants.PROPERTY_BDAY)) {
                                                this.mBirthday = new BirthdayData(propValue);
                                                return;
                                            }
                                            if (propertyName.equals(VCardConstants.PROPERTY_ANNIVERSARY)) {
                                                this.mAnniversary = new AnniversaryData(propValue);
                                                return;
                                            }
                                            if (propertyName.equals(VCardConstants.PROPERTY_X_PHONETIC_FIRST_NAME)) {
                                                this.mNameData.mPhoneticGiven = propValue;
                                                return;
                                            }
                                            if (propertyName.equals(VCardConstants.PROPERTY_X_PHONETIC_MIDDLE_NAME)) {
                                                this.mNameData.mPhoneticMiddle = propValue;
                                                return;
                                            }
                                            if (propertyName.equals(VCardConstants.PROPERTY_X_PHONETIC_LAST_NAME)) {
                                                this.mNameData.mPhoneticFamily = propValue;
                                                return;
                                            }
                                            if (!propertyName.equals(VCardConstants.PROPERTY_IMPP)) {
                                                if (!propertyName.equals(VCardConstants.PROPERTY_X_SIP)) {
                                                    if (propertyName.equals(VCardConstants.PROPERTY_X_ANDROID_CUSTOM)) {
                                                        handleAndroidCustomProperty(VCardUtils.constructListFromValue(propValue, this.mVCardType));
                                                        return;
                                                    }
                                                    return;
                                                } else if (!TextUtils.isEmpty(propValue)) {
                                                    handleSipCase(propValue, (Collection) paramMap.get(VCardConstants.PARAM_TYPE));
                                                    return;
                                                } else {
                                                    return;
                                                }
                                            } else if (propValue.startsWith("sip:")) {
                                                handleSipCase(propValue, (Collection) paramMap.get(VCardConstants.PARAM_TYPE));
                                                return;
                                            } else {
                                                return;
                                            }
                                        }
                                    }
                                }
                                Collection<String> paramMapValue = (Collection) paramMap.get(VCardConstants.PARAM_VALUE);
                                if (paramMapValue != null) {
                                    if (paramMapValue.contains("URL")) {
                                        return;
                                    }
                                }
                                typeCollection = (Collection) paramMap.get(VCardConstants.PARAM_TYPE);
                                String formatName = null;
                                isPrimary = false;
                                if (typeCollection != null) {
                                    for (String typeValue : typeCollection) {
                                        if (VCardConstants.PARAM_TYPE_PREF.equals(typeValue)) {
                                            isPrimary = true;
                                        } else if (formatName == null) {
                                            formatName = typeValue;
                                        }
                                    }
                                }
                                addPhotoBytes(formatName, propertyBytes, isPrimary);
                                return;
                            }
                            return;
                        }
                    }
                    addNickName(propValue);
                } else if (TextUtils.isEmpty(this.mNameData.mFormatted)) {
                    this.mNameData.mFormatted = propValue;
                }
            }
        }
    }
    
    public void consolidateFields() {
        this.mNameData.displayName = this.constructDisplayName();
    }
    
    public ArrayList<ContentProviderOperation> constructInsertOperations(final ContentResolver contentResolver, final ArrayList<ContentProviderOperation> list) {
        ArrayList<ContentProviderOperation> list2 = list;
        if (list == null) {
            list2 = new ArrayList<ContentProviderOperation>();
        }
        if (!this.isIgnorable()) {
            final int size = list2.size();
            final ContentProviderOperation.Builder insert = ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI);
            if (this.mAccount != null) {
                insert.withValue("account_name", this.mAccount.name);
                insert.withValue("account_type", this.mAccount.type);
            }
            else {
                insert.withValue("account_name", null);
                insert.withValue("account_type", null);
            }
            list2.add(insert.build());
            list2.size();
            this.iterateAllData((EntryElementIterator)new InsertOperationConstrutor(list2, size));
            list2.size();
        }
        return list2;
    }
    
    public final String getBirthday() {
        String access$1800;
        if (this.mBirthday != null) {
            access$1800 = this.mBirthday.mBirthday;
        }
        else {
            access$1800 = null;
        }
        return access$1800;
    }
    
    public Date getCallTime() {
        return this.mCallTime;
    }
    
    public int getCallType() {
        return this.mCallType;
    }
    
    public final List<VCardEntry> getChildlen() {
        return this.mChildren;
    }
    
    public String getDisplayName() {
        if (this.mNameData.displayName == null) {
            this.mNameData.displayName = this.constructDisplayName();
        }
        return this.mNameData.displayName;
    }
    
    public final List<EmailData> getEmailList() {
        return this.mEmailList;
    }
    
    public final List<ImData> getImList() {
        return this.mImList;
    }
    
    public final NameData getNameData() {
        return this.mNameData;
    }
    
    public final List<NicknameData> getNickNameList() {
        return this.mNicknameList;
    }
    
    public final List<NoteData> getNotes() {
        return this.mNoteList;
    }
    
    public final List<OrganizationData> getOrganizationList() {
        return this.mOrganizationList;
    }
    
    public final List<PhoneData> getPhoneList() {
        return this.mPhoneList;
    }
    
    public final List<PhotoData> getPhotoList() {
        return this.mPhotoList;
    }
    
    public final List<PostalData> getPostalList() {
        return this.mPostalList;
    }
    
    public final List<WebsiteData> getWebsiteList() {
        return this.mWebsiteList;
    }
    
    public boolean isIgnorable() {
        final IsIgnorableIterator isIgnorableIterator = new IsIgnorableIterator();
        this.iterateAllData((EntryElementIterator)isIgnorableIterator);
        return isIgnorableIterator.getResult();
    }
    
    public final void iterateAllData(final EntryElementIterator entryElementIterator) {
        entryElementIterator.onIterationStarted();
        entryElementIterator.onElementGroupStarted(this.mNameData.getEntryLabel());
        entryElementIterator.onElement(this.mNameData);
        entryElementIterator.onElementGroupEnded();
        this.iterateOneList((List<? extends EntryElement>)this.mPhoneList, entryElementIterator);
        this.iterateOneList((List<? extends EntryElement>)this.mEmailList, entryElementIterator);
        this.iterateOneList((List<? extends EntryElement>)this.mPostalList, entryElementIterator);
        this.iterateOneList((List<? extends EntryElement>)this.mOrganizationList, entryElementIterator);
        this.iterateOneList((List<? extends EntryElement>)this.mImList, entryElementIterator);
        this.iterateOneList((List<? extends EntryElement>)this.mPhotoList, entryElementIterator);
        this.iterateOneList((List<? extends EntryElement>)this.mWebsiteList, entryElementIterator);
        this.iterateOneList((List<? extends EntryElement>)this.mSipList, entryElementIterator);
        this.iterateOneList((List<? extends EntryElement>)this.mNicknameList, entryElementIterator);
        this.iterateOneList((List<? extends EntryElement>)this.mNoteList, entryElementIterator);
        this.iterateOneList((List<? extends EntryElement>)this.mAndroidCustomDataList, entryElementIterator);
        if (this.mBirthday != null) {
            entryElementIterator.onElementGroupStarted(this.mBirthday.getEntryLabel());
            entryElementIterator.onElement(this.mBirthday);
            entryElementIterator.onElementGroupEnded();
        }
        if (this.mAnniversary != null) {
            entryElementIterator.onElementGroupStarted(this.mAnniversary.getEntryLabel());
            entryElementIterator.onElement(this.mAnniversary);
            entryElementIterator.onElementGroupEnded();
        }
        entryElementIterator.onIterationEnded();
    }
    
    @Override
    public String toString() {
        final ToStringIterator toStringIterator = new ToStringIterator();
        this.iterateAllData((EntryElementIterator)toStringIterator);
        return toStringIterator.toString();
    }
    
    public static class AndroidCustomData implements EntryElement
    {
        private final List<String> mDataList;
        private final String mMimeType;
        
        public AndroidCustomData(final String mMimeType, final List<String> mDataList) {
            this.mMimeType = mMimeType;
            this.mDataList = mDataList;
        }
        
        public static AndroidCustomData constructAndroidCustomData(final List<String> list) {
            int size = 16;
            String s;
            List<String> subList;
            if (list == null) {
                s = null;
                subList = null;
            }
            else if (list.size() < 2) {
                s = list.get(0);
                subList = null;
            }
            else {
                if (list.size() < 16) {
                    size = list.size();
                }
                s = list.get(0);
                subList = list.subList(1, size);
            }
            return new AndroidCustomData(s, subList);
        }
        
        @Override
        public void constructInsertOperation(final List<ContentProviderOperation> list, int i) {
            final ContentProviderOperation.Builder insert = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
            insert.withValueBackReference("raw_contact_id", i);
            insert.withValue("mimetype", this.mMimeType);
            String s;
            for (i = 0; i < this.mDataList.size(); ++i) {
                s = this.mDataList.get(i);
                if (!TextUtils.isEmpty((CharSequence)s)) {
                    insert.withValue("data" + (i + 1), s);
                }
            }
            list.add(insert.build());
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean b = false;
            boolean b2;
            if (this == o) {
                b2 = true;
            }
            else {
                b2 = b;
                if (o instanceof AndroidCustomData) {
                    final AndroidCustomData androidCustomData = (AndroidCustomData)o;
                    b2 = b;
                    if (TextUtils.equals((CharSequence)this.mMimeType, (CharSequence)androidCustomData.mMimeType)) {
                        if (this.mDataList == null) {
                            b2 = (androidCustomData.mDataList == null);
                        }
                        else {
                            final int size = this.mDataList.size();
                            b2 = b;
                            if (size == androidCustomData.mDataList.size()) {
                                for (int i = 0; i < size; ++i) {
                                    b2 = b;
                                    if (!TextUtils.equals((CharSequence)this.mDataList.get(i), (CharSequence)androidCustomData.mDataList.get(i))) {
                                        return b2;
                                    }
                                }
                                b2 = true;
                            }
                        }
                    }
                }
            }
            return b2;
        }
        
        public List<String> getDataList() {
            return this.mDataList;
        }
        
        @Override
        public EntryLabel getEntryLabel() {
            return EntryLabel.ANDROID_CUSTOM;
        }
        
        public String getMimeType() {
            return this.mMimeType;
        }
        
        @Override
        public int hashCode() {
            int hash;
            if (this.mMimeType != null) {
                hash = this.mMimeType.hashCode();
            } else {
                hash = 0;
            }
            if (this.mDataList != null) {
                for (String data : this.mDataList) {
                    int hashCode;
                    int i = hash * 31;
                    if (data != null) {
                        hashCode = data.hashCode();
                    } else {
                        hashCode = 0;
                    }
                    hash = i + hashCode;
                }
            }
            return hash;
        }
        
        @Override
        public boolean isEmpty() {
            return TextUtils.isEmpty((CharSequence)this.mMimeType) || this.mDataList == null || this.mDataList.size() == 0;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("android-custom: " + this.mMimeType + ", data: ");
            String string;
            if (this.mDataList == null) {
                string = "null";
            }
            else {
                string = Arrays.toString(this.mDataList.toArray());
            }
            sb.append(string);
            return sb.toString();
        }
    }
    
    public static class AnniversaryData implements EntryElement
    {
        private final String mAnniversary;
        
        public AnniversaryData(final String mAnniversary) {
            this.mAnniversary = mAnniversary;
        }
        
        @Override
        public void constructInsertOperation(final List<ContentProviderOperation> list, final int n) {
            final ContentProviderOperation.Builder insert = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
            insert.withValueBackReference("raw_contact_id", n);
            insert.withValue("mimetype", "vnd.android.cursor.item/contact_event");
            insert.withValue("data1", this.mAnniversary);
            insert.withValue("data2", 1);
            list.add(insert.build());
        }
        
        @Override
        public boolean equals(final Object o) {
            return this == o || (o instanceof AnniversaryData && TextUtils.equals((CharSequence)this.mAnniversary, (CharSequence)((AnniversaryData)o).mAnniversary));
        }
        
        public String getAnniversary() {
            return this.mAnniversary;
        }
        
        @Override
        public EntryLabel getEntryLabel() {
            return EntryLabel.ANNIVERSARY;
        }
        
        @Override
        public int hashCode() {
            int hashCode;
            if (this.mAnniversary != null) {
                hashCode = this.mAnniversary.hashCode();
            }
            else {
                hashCode = 0;
            }
            return hashCode;
        }
        
        @Override
        public boolean isEmpty() {
            return TextUtils.isEmpty((CharSequence)this.mAnniversary);
        }
        
        @Override
        public String toString() {
            return "anniversary: " + this.mAnniversary;
        }
    }
    
    public static class BirthdayData implements EntryElement
    {
        private final String mBirthday;
        
        public BirthdayData(final String mBirthday) {
            this.mBirthday = mBirthday;
        }
        
        @Override
        public void constructInsertOperation(final List<ContentProviderOperation> list, final int n) {
            final ContentProviderOperation.Builder insert = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
            insert.withValueBackReference("raw_contact_id", n);
            insert.withValue("mimetype", "vnd.android.cursor.item/contact_event");
            insert.withValue("data1", this.mBirthday);
            insert.withValue("data2", 3);
            list.add(insert.build());
        }
        
        @Override
        public boolean equals(final Object o) {
            return this == o || (o instanceof BirthdayData && TextUtils.equals((CharSequence)this.mBirthday, (CharSequence)((BirthdayData)o).mBirthday));
        }
        
        public String getBirthday() {
            return this.mBirthday;
        }
        
        @Override
        public EntryLabel getEntryLabel() {
            return EntryLabel.BIRTHDAY;
        }
        
        @Override
        public int hashCode() {
            int hashCode;
            if (this.mBirthday != null) {
                hashCode = this.mBirthday.hashCode();
            }
            else {
                hashCode = 0;
            }
            return hashCode;
        }
        
        @Override
        public boolean isEmpty() {
            return TextUtils.isEmpty((CharSequence)this.mBirthday);
        }
        
        @Override
        public String toString() {
            return "birthday: " + this.mBirthday;
        }
    }
    
    public static class EmailData implements EntryElement
    {
        private final String mAddress;
        private final boolean mIsPrimary;
        private final String mLabel;
        private final int mType;
        
        public EmailData(final String mAddress, final int mType, final String mLabel, final boolean mIsPrimary) {
            this.mType = mType;
            this.mAddress = mAddress;
            this.mLabel = mLabel;
            this.mIsPrimary = mIsPrimary;
        }
        
        @Override
        public void constructInsertOperation(final List<ContentProviderOperation> list, final int n) {
            final ContentProviderOperation.Builder insert = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
            insert.withValueBackReference("raw_contact_id", n);
            insert.withValue("mimetype", "vnd.android.cursor.item/email_v2");
            insert.withValue("data2", this.mType);
            if (this.mType == 0) {
                insert.withValue("data3", this.mLabel);
            }
            insert.withValue("data1", this.mAddress);
            if (this.mIsPrimary) {
                insert.withValue("is_primary", 1);
            }
            list.add(insert.build());
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b = true;
            if (this != o) {
                if (!(o instanceof EmailData)) {
                    b = false;
                }
                else {
                    final EmailData emailData = (EmailData)o;
                    if (this.mType != emailData.mType || !TextUtils.equals((CharSequence)this.mAddress, (CharSequence)emailData.mAddress) || !TextUtils.equals((CharSequence)this.mLabel, (CharSequence)emailData.mLabel) || this.mIsPrimary != emailData.mIsPrimary) {
                        b = false;
                    }
                }
            }
            return b;
        }
        
        public String getAddress() {
            return this.mAddress;
        }
        
        @Override
        public final EntryLabel getEntryLabel() {
            return EntryLabel.EMAIL;
        }
        
        public String getLabel() {
            return this.mLabel;
        }
        
        public int getType() {
            return this.mType;
        }
        
        @Override
        public int hashCode() {
            int hashCode = 0;
            final int mType = this.mType;
            int hashCode2;
            if (this.mAddress != null) {
                hashCode2 = this.mAddress.hashCode();
            }
            else {
                hashCode2 = 0;
            }
            if (this.mLabel != null) {
                hashCode = this.mLabel.hashCode();
            }
            int n;
            if (this.mIsPrimary) {
                n = 1231;
            }
            else {
                n = 1237;
            }
            return ((mType * 31 + hashCode2) * 31 + hashCode) * 31 + n;
        }
        
        @Override
        public boolean isEmpty() {
            return TextUtils.isEmpty((CharSequence)this.mAddress);
        }
        
        public boolean isPrimary() {
            return this.mIsPrimary;
        }
        
        @Override
        public String toString() {
            return String.format("type: %d, data: %s, label: %s, isPrimary: %s", this.mType, this.mAddress, this.mLabel, this.mIsPrimary);
        }
    }
    
    public interface EntryElement
    {
        void constructInsertOperation(final List<ContentProviderOperation> p0, final int p1);
        
        EntryLabel getEntryLabel();
        
        boolean isEmpty();
    }
    
    public interface EntryElementIterator
    {
        boolean onElement(final EntryElement p0);
        
        void onElementGroupEnded();
        
        void onElementGroupStarted(final EntryLabel p0);
        
        void onIterationEnded();
        
        void onIterationStarted();
    }

    public enum EntryLabel {
        NAME(0),
        PHONE(1),
        EMAIL(2),
        POSTAL_ADDRESS(3),
        ORGANIZATION(4),
        IM(5),
        PHOTO(6),
        WEBSITE(7),
        SIP(8),
        NICKNAME(9),
        NOTE(10),
        BIRTHDAY(11),
        ANNIVERSARY(12),
        ANDROID_CUSTOM(13);

        private int value;
        EntryLabel(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
    public static class ImData implements EntryElement
    {
        private final String mAddress;
        private final String mCustomProtocol;
        private final boolean mIsPrimary;
        private final int mProtocol;
        private final int mType;
        
        public ImData(final int mProtocol, final String mCustomProtocol, final String mAddress, final int mType, final boolean mIsPrimary) {
            this.mProtocol = mProtocol;
            this.mCustomProtocol = mCustomProtocol;
            this.mType = mType;
            this.mAddress = mAddress;
            this.mIsPrimary = mIsPrimary;
        }
        
        @Override
        public void constructInsertOperation(final List<ContentProviderOperation> list, final int n) {
            final ContentProviderOperation.Builder insert = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
            insert.withValueBackReference("raw_contact_id", n);
            insert.withValue("mimetype", "vnd.android.cursor.item/im");
            insert.withValue("data2", this.mType);
            insert.withValue("data5", this.mProtocol);
            insert.withValue("data1", this.mAddress);
            if (this.mProtocol == -1) {
                insert.withValue("data6", this.mCustomProtocol);
            }
            if (this.mIsPrimary) {
                insert.withValue("is_primary", 1);
            }
            list.add(insert.build());
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b = true;
            if (this != o) {
                if (!(o instanceof ImData)) {
                    b = false;
                }
                else {
                    final ImData imData = (ImData)o;
                    if (this.mType != imData.mType || this.mProtocol != imData.mProtocol || !TextUtils.equals((CharSequence)this.mCustomProtocol, (CharSequence)imData.mCustomProtocol) || !TextUtils.equals((CharSequence)this.mAddress, (CharSequence)imData.mAddress) || this.mIsPrimary != imData.mIsPrimary) {
                        b = false;
                    }
                }
            }
            return b;
        }
        
        public String getAddress() {
            return this.mAddress;
        }
        
        public String getCustomProtocol() {
            return this.mCustomProtocol;
        }
        
        @Override
        public final EntryLabel getEntryLabel() {
            return EntryLabel.IM;
        }
        
        public int getProtocol() {
            return this.mProtocol;
        }
        
        public int getType() {
            return this.mType;
        }
        
        @Override
        public int hashCode() {
            int hashCode = 0;
            final int mType = this.mType;
            final int mProtocol = this.mProtocol;
            int hashCode2;
            if (this.mCustomProtocol != null) {
                hashCode2 = this.mCustomProtocol.hashCode();
            }
            else {
                hashCode2 = 0;
            }
            if (this.mAddress != null) {
                hashCode = this.mAddress.hashCode();
            }
            int n;
            if (this.mIsPrimary) {
                n = 1231;
            }
            else {
                n = 1237;
            }
            return (((mType * 31 + mProtocol) * 31 + hashCode2) * 31 + hashCode) * 31 + n;
        }
        
        @Override
        public boolean isEmpty() {
            return TextUtils.isEmpty((CharSequence)this.mAddress);
        }
        
        public boolean isPrimary() {
            return this.mIsPrimary;
        }
        
        @Override
        public String toString() {
            return String.format("type: %d, protocol: %d, custom_protcol: %s, data: %s, isPrimary: %s", this.mType, this.mProtocol, this.mCustomProtocol, this.mAddress, this.mIsPrimary);
        }
    }
    
    private class InsertOperationConstrutor implements EntryElementIterator
    {
        private final int mBackReferenceIndex;
        private final List<ContentProviderOperation> mOperationList;
        
        public InsertOperationConstrutor(final List<ContentProviderOperation> mOperationList, final int mBackReferenceIndex) {
            this.mOperationList = mOperationList;
            this.mBackReferenceIndex = mBackReferenceIndex;
        }
        
        @Override
        public boolean onElement(final EntryElement entryElement) {
            if (!entryElement.isEmpty()) {
                entryElement.constructInsertOperation(this.mOperationList, this.mBackReferenceIndex);
            }
            return true;
        }
        
        @Override
        public void onElementGroupEnded() {
        }
        
        @Override
        public void onElementGroupStarted(final EntryLabel entryLabel) {
        }
        
        @Override
        public void onIterationEnded() {
        }
        
        @Override
        public void onIterationStarted() {
        }
    }
    
    private class IsIgnorableIterator implements EntryElementIterator
    {
        private boolean mEmpty;
        
        private IsIgnorableIterator() {
            this.mEmpty = true;
        }
        
        public boolean getResult() {
            return this.mEmpty;
        }
        
        @Override
        public boolean onElement(final EntryElement entryElement) {
            boolean b = false;
            if (!entryElement.isEmpty()) {
                this.mEmpty = false;
            }
            else {
                b = true;
            }
            return b;
        }
        
        @Override
        public void onElementGroupEnded() {
        }
        
        @Override
        public void onElementGroupStarted(final EntryLabel entryLabel) {
        }
        
        @Override
        public void onIterationEnded() {
        }
        
        @Override
        public void onIterationStarted() {
        }
    }
    
    public static class NameData implements EntryElement
    {
        public String displayName;
        private String mFamily;
        private String mFormatted;
        private String mGiven;
        private String mMiddle;
        private String mPhoneticFamily;
        private String mPhoneticGiven;
        private String mPhoneticMiddle;
        private String mPrefix;
        private String mSortString;
        private String mSuffix;
        
        @Override
        public void constructInsertOperation(final List<ContentProviderOperation> list, int n) {
            final ContentProviderOperation.Builder insert = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
            insert.withValueBackReference("raw_contact_id", n);
            insert.withValue("mimetype", "vnd.android.cursor.item/name");
            if (!TextUtils.isEmpty((CharSequence)this.mGiven)) {
                insert.withValue("data2", this.mGiven);
            }
            if (!TextUtils.isEmpty((CharSequence)this.mFamily)) {
                insert.withValue("data3", this.mFamily);
            }
            if (!TextUtils.isEmpty((CharSequence)this.mMiddle)) {
                insert.withValue("data5", this.mMiddle);
            }
            if (!TextUtils.isEmpty((CharSequence)this.mPrefix)) {
                insert.withValue("data4", this.mPrefix);
            }
            if (!TextUtils.isEmpty((CharSequence)this.mSuffix)) {
                insert.withValue("data6", this.mSuffix);
            }
            n = 0;
            if (!TextUtils.isEmpty((CharSequence)this.mPhoneticGiven)) {
                insert.withValue("data7", this.mPhoneticGiven);
                n = 1;
            }
            if (!TextUtils.isEmpty((CharSequence)this.mPhoneticFamily)) {
                insert.withValue("data9", this.mPhoneticFamily);
                n = 1;
            }
            if (!TextUtils.isEmpty((CharSequence)this.mPhoneticMiddle)) {
                insert.withValue("data8", this.mPhoneticMiddle);
                n = 1;
            }
            if (n == 0) {
                insert.withValue("data7", this.mSortString);
            }
            insert.withValue("data1", this.displayName);
            list.add(insert.build());
        }
        
        public boolean emptyPhoneticStructuredName() {
            return TextUtils.isEmpty((CharSequence)this.mPhoneticFamily) && TextUtils.isEmpty((CharSequence)this.mPhoneticGiven) && TextUtils.isEmpty((CharSequence)this.mPhoneticMiddle);
        }
        
        public boolean emptyStructuredName() {
            return TextUtils.isEmpty((CharSequence)this.mFamily) && TextUtils.isEmpty((CharSequence)this.mGiven) && TextUtils.isEmpty((CharSequence)this.mMiddle) && TextUtils.isEmpty((CharSequence)this.mPrefix) && TextUtils.isEmpty((CharSequence)this.mSuffix);
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b = true;
            if (this != o) {
                if (!(o instanceof NameData)) {
                    b = false;
                }
                else {
                    final NameData nameData = (NameData)o;
                    if (!TextUtils.equals((CharSequence)this.mFamily, (CharSequence)nameData.mFamily) || !TextUtils.equals((CharSequence)this.mMiddle, (CharSequence)nameData.mMiddle) || !TextUtils.equals((CharSequence)this.mGiven, (CharSequence)nameData.mGiven) || !TextUtils.equals((CharSequence)this.mPrefix, (CharSequence)nameData.mPrefix) || !TextUtils.equals((CharSequence)this.mSuffix, (CharSequence)nameData.mSuffix) || !TextUtils.equals((CharSequence)this.mFormatted, (CharSequence)nameData.mFormatted) || !TextUtils.equals((CharSequence)this.mPhoneticFamily, (CharSequence)nameData.mPhoneticFamily) || !TextUtils.equals((CharSequence)this.mPhoneticMiddle, (CharSequence)nameData.mPhoneticMiddle) || !TextUtils.equals((CharSequence)this.mPhoneticGiven, (CharSequence)nameData.mPhoneticGiven) || !TextUtils.equals((CharSequence)this.mSortString, (CharSequence)nameData.mSortString)) {
                        b = false;
                    }
                }
            }
            return b;
        }
        
        @Override
        public final EntryLabel getEntryLabel() {
            return EntryLabel.NAME;
        }
        
        public String getFamily() {
            return this.mFamily;
        }
        
        public String getFormatted() {
            return this.mFormatted;
        }
        
        public String getGiven() {
            return this.mGiven;
        }
        
        public String getMiddle() {
            return this.mMiddle;
        }
        
        public String getPrefix() {
            return this.mPrefix;
        }
        
        public String getSortString() {
            return this.mSortString;
        }
        
        public String getSuffix() {
            return this.mSuffix;
        }
        
        @Override
        public int hashCode() {
            final String[] array = { this.mFamily, this.mMiddle, this.mGiven, this.mPrefix, this.mSuffix, this.mFormatted, this.mPhoneticFamily, this.mPhoneticMiddle, this.mPhoneticGiven, this.mSortString };
            int n = 0;
            for (final String s : array) {
                int hashCode;
                if (s != null) {
                    hashCode = s.hashCode();
                }
                else {
                    hashCode = 0;
                }
                n = n * 31 + hashCode;
            }
            return n;
        }
        
        @Override
        public boolean isEmpty() {
            return TextUtils.isEmpty((CharSequence)this.mFamily) && TextUtils.isEmpty((CharSequence)this.mMiddle) && TextUtils.isEmpty((CharSequence)this.mGiven) && TextUtils.isEmpty((CharSequence)this.mPrefix) && TextUtils.isEmpty((CharSequence)this.mSuffix) && TextUtils.isEmpty((CharSequence)this.mFormatted) && TextUtils.isEmpty((CharSequence)this.mPhoneticFamily) && TextUtils.isEmpty((CharSequence)this.mPhoneticMiddle) && TextUtils.isEmpty((CharSequence)this.mPhoneticGiven) && TextUtils.isEmpty((CharSequence)this.mSortString);
        }
        
        public void setFamily(final String mFamily) {
            this.mFamily = mFamily;
        }
        
        public void setGiven(final String mGiven) {
            this.mGiven = mGiven;
        }
        
        public void setMiddle(final String mMiddle) {
            this.mMiddle = mMiddle;
        }
        
        public void setPrefix(final String mPrefix) {
            this.mPrefix = mPrefix;
        }
        
        public void setSuffix(final String mSuffix) {
            this.mSuffix = mSuffix;
        }
        
        @Override
        public String toString() {
            return String.format("family: %s, given: %s, middle: %s, prefix: %s, suffix: %s", this.mFamily, this.mGiven, this.mMiddle, this.mPrefix, this.mSuffix);
        }
    }
    
    public static class NicknameData implements EntryElement
    {
        private final String mNickname;
        
        public NicknameData(final String mNickname) {
            this.mNickname = mNickname;
        }
        
        @Override
        public void constructInsertOperation(final List<ContentProviderOperation> list, final int n) {
            final ContentProviderOperation.Builder insert = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
            insert.withValueBackReference("raw_contact_id", n);
            insert.withValue("mimetype", "vnd.android.cursor.item/nickname");
            insert.withValue("data2", 1);
            insert.withValue("data1", this.mNickname);
            list.add(insert.build());
        }
        
        @Override
        public boolean equals(final Object o) {
            return o instanceof NicknameData && TextUtils.equals((CharSequence)this.mNickname, (CharSequence)((NicknameData)o).mNickname);
        }
        
        @Override
        public EntryLabel getEntryLabel() {
            return EntryLabel.NICKNAME;
        }
        
        public String getNickname() {
            return this.mNickname;
        }
        
        @Override
        public int hashCode() {
            int hashCode;
            if (this.mNickname != null) {
                hashCode = this.mNickname.hashCode();
            }
            else {
                hashCode = 0;
            }
            return hashCode;
        }
        
        @Override
        public boolean isEmpty() {
            return TextUtils.isEmpty((CharSequence)this.mNickname);
        }
        
        @Override
        public String toString() {
            return "nickname: " + this.mNickname;
        }
    }
    
    public static class NoteData implements EntryElement
    {
        public final String mNote;
        
        public NoteData(final String mNote) {
            this.mNote = mNote;
        }
        
        @Override
        public void constructInsertOperation(final List<ContentProviderOperation> list, final int n) {
            final ContentProviderOperation.Builder insert = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
            insert.withValueBackReference("raw_contact_id", n);
            insert.withValue("mimetype", "vnd.android.cursor.item/note");
            insert.withValue("data1", this.mNote);
            list.add(insert.build());
        }
        
        @Override
        public boolean equals(final Object o) {
            return this == o || (o instanceof NoteData && TextUtils.equals((CharSequence)this.mNote, (CharSequence)((NoteData)o).mNote));
        }
        
        @Override
        public EntryLabel getEntryLabel() {
            return EntryLabel.NOTE;
        }
        
        public String getNote() {
            return this.mNote;
        }
        
        @Override
        public int hashCode() {
            int hashCode;
            if (this.mNote != null) {
                hashCode = this.mNote.hashCode();
            }
            else {
                hashCode = 0;
            }
            return hashCode;
        }
        
        @Override
        public boolean isEmpty() {
            return TextUtils.isEmpty((CharSequence)this.mNote);
        }
        
        @Override
        public String toString() {
            return "note: " + this.mNote;
        }
    }
    
    public static class OrganizationData implements EntryElement
    {
        private String mDepartmentName;
        private boolean mIsPrimary;
        private String mOrganizationName;
        private final String mPhoneticName;
        private String mTitle;
        private final int mType;
        
        public OrganizationData(final String mOrganizationName, final String mDepartmentName, final String mTitle, final String mPhoneticName, final int mType, final boolean mIsPrimary) {
            this.mType = mType;
            this.mOrganizationName = mOrganizationName;
            this.mDepartmentName = mDepartmentName;
            this.mTitle = mTitle;
            this.mPhoneticName = mPhoneticName;
            this.mIsPrimary = mIsPrimary;
        }
        
        @Override
        public void constructInsertOperation(final List<ContentProviderOperation> list, final int n) {
            final ContentProviderOperation.Builder insert = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
            insert.withValueBackReference("raw_contact_id", n);
            insert.withValue("mimetype", "vnd.android.cursor.item/organization");
            insert.withValue("data2", this.mType);
            if (this.mOrganizationName != null) {
                insert.withValue("data1", this.mOrganizationName);
            }
            if (this.mDepartmentName != null) {
                insert.withValue("data5", this.mDepartmentName);
            }
            if (this.mTitle != null) {
                insert.withValue("data4", this.mTitle);
            }
            if (this.mPhoneticName != null) {
                insert.withValue("data8", this.mPhoneticName);
            }
            if (this.mIsPrimary) {
                insert.withValue("is_primary", 1);
            }
            list.add(insert.build());
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b = true;
            if (this != o) {
                if (!(o instanceof OrganizationData)) {
                    b = false;
                }
                else {
                    final OrganizationData organizationData = (OrganizationData)o;
                    if (this.mType != organizationData.mType || !TextUtils.equals((CharSequence)this.mOrganizationName, (CharSequence)organizationData.mOrganizationName) || !TextUtils.equals((CharSequence)this.mDepartmentName, (CharSequence)organizationData.mDepartmentName) || !TextUtils.equals((CharSequence)this.mTitle, (CharSequence)organizationData.mTitle) || this.mIsPrimary != organizationData.mIsPrimary) {
                        b = false;
                    }
                }
            }
            return b;
        }
        
        public String getDepartmentName() {
            return this.mDepartmentName;
        }
        
        @Override
        public final EntryLabel getEntryLabel() {
            return EntryLabel.ORGANIZATION;
        }
        
        public String getFormattedString() {
            final StringBuilder sb = new StringBuilder();
            if (!TextUtils.isEmpty((CharSequence)this.mOrganizationName)) {
                sb.append(this.mOrganizationName);
            }
            if (!TextUtils.isEmpty((CharSequence)this.mDepartmentName)) {
                if (sb.length() > 0) {
                    sb.append(", ");
                }
                sb.append(this.mDepartmentName);
            }
            if (!TextUtils.isEmpty((CharSequence)this.mTitle)) {
                if (sb.length() > 0) {
                    sb.append(", ");
                }
                sb.append(this.mTitle);
            }
            return sb.toString();
        }
        
        public String getOrganizationName() {
            return this.mOrganizationName;
        }
        
        public String getPhoneticName() {
            return this.mPhoneticName;
        }
        
        public String getTitle() {
            return this.mTitle;
        }
        
        public int getType() {
            return this.mType;
        }
        
        @Override
        public int hashCode() {
            int hashCode = 0;
            final int mType = this.mType;
            int hashCode2;
            if (this.mOrganizationName != null) {
                hashCode2 = this.mOrganizationName.hashCode();
            }
            else {
                hashCode2 = 0;
            }
            int hashCode3;
            if (this.mDepartmentName != null) {
                hashCode3 = this.mDepartmentName.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            if (this.mTitle != null) {
                hashCode = this.mTitle.hashCode();
            }
            int n;
            if (this.mIsPrimary) {
                n = 1231;
            }
            else {
                n = 1237;
            }
            return (((mType * 31 + hashCode2) * 31 + hashCode3) * 31 + hashCode) * 31 + n;
        }
        
        @Override
        public boolean isEmpty() {
            return TextUtils.isEmpty((CharSequence)this.mOrganizationName) && TextUtils.isEmpty((CharSequence)this.mDepartmentName) && TextUtils.isEmpty((CharSequence)this.mTitle) && TextUtils.isEmpty((CharSequence)this.mPhoneticName);
        }
        
        public boolean isPrimary() {
            return this.mIsPrimary;
        }
        
        @Override
        public String toString() {
            return String.format("type: %d, organization: %s, department: %s, title: %s, isPrimary: %s", this.mType, this.mOrganizationName, this.mDepartmentName, this.mTitle, this.mIsPrimary);
        }
    }
    
    public static class PhoneData implements EntryElement
    {
        private boolean mIsPrimary;
        private final String mLabel;
        private final String mNumber;
        private final int mType;
        
        public PhoneData(final String mNumber, final int mType, final String mLabel, final boolean mIsPrimary) {
            this.mNumber = mNumber;
            this.mType = mType;
            this.mLabel = mLabel;
            this.mIsPrimary = mIsPrimary;
        }
        
        @Override
        public void constructInsertOperation(final List<ContentProviderOperation> list, final int n) {
            final ContentProviderOperation.Builder insert = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
            insert.withValueBackReference("raw_contact_id", n);
            insert.withValue("mimetype", "vnd.android.cursor.item/phone_v2");
            insert.withValue("data2", this.mType);
            if (this.mType == 0) {
                insert.withValue("data3", this.mLabel);
            }
            insert.withValue("data1", this.mNumber);
            if (this.mIsPrimary) {
                insert.withValue("is_primary", 1);
            }
            list.add(insert.build());
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b = true;
            if (this != o) {
                if (!(o instanceof PhoneData)) {
                    b = false;
                }
                else {
                    final PhoneData phoneData = (PhoneData)o;
                    if (this.mType != phoneData.mType || !TextUtils.equals((CharSequence)this.mNumber, (CharSequence)phoneData.mNumber) || !TextUtils.equals((CharSequence)this.mLabel, (CharSequence)phoneData.mLabel) || this.mIsPrimary != phoneData.mIsPrimary) {
                        b = false;
                    }
                }
            }
            return b;
        }
        
        @Override
        public final EntryLabel getEntryLabel() {
            return EntryLabel.PHONE;
        }
        
        public String getLabel() {
            return this.mLabel;
        }
        
        public String getNumber() {
            return this.mNumber;
        }
        
        public int getType() {
            return this.mType;
        }
        
        @Override
        public int hashCode() {
            int hashCode = 0;
            final int mType = this.mType;
            int hashCode2;
            if (this.mNumber != null) {
                hashCode2 = this.mNumber.hashCode();
            }
            else {
                hashCode2 = 0;
            }
            if (this.mLabel != null) {
                hashCode = this.mLabel.hashCode();
            }
            int n;
            if (this.mIsPrimary) {
                n = 1231;
            }
            else {
                n = 1237;
            }
            return ((mType * 31 + hashCode2) * 31 + hashCode) * 31 + n;
        }
        
        @Override
        public boolean isEmpty() {
            return TextUtils.isEmpty((CharSequence)this.mNumber);
        }
        
        public boolean isPrimary() {
            return this.mIsPrimary;
        }
        
        @Override
        public String toString() {
            return String.format("type: %d, data: %s, label: %s, isPrimary: %s", this.mType, this.mNumber, this.mLabel, this.mIsPrimary);
        }
    }
    
    public static class PhotoData implements EntryElement
    {
        private final byte[] mBytes;
        private final String mFormat;
        private Integer mHashCode;
        private final boolean mIsPrimary;
        
        public PhotoData(final String mFormat, final byte[] mBytes, final boolean mIsPrimary) {
            this.mHashCode = null;
            this.mFormat = mFormat;
            this.mBytes = mBytes;
            this.mIsPrimary = mIsPrimary;
        }
        
        @Override
        public void constructInsertOperation(final List<ContentProviderOperation> list, final int n) {
            final ContentProviderOperation.Builder insert = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
            insert.withValueBackReference("raw_contact_id", n);
            insert.withValue("mimetype", "vnd.android.cursor.item/photo");
            insert.withValue("data15", this.mBytes);
            if (this.mIsPrimary) {
                insert.withValue("is_primary", 1);
            }
            list.add(insert.build());
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b = true;
            if (this != o) {
                if (!(o instanceof PhotoData)) {
                    b = false;
                }
                else {
                    final PhotoData photoData = (PhotoData)o;
                    if (!TextUtils.equals((CharSequence)this.mFormat, (CharSequence)photoData.mFormat) || !Arrays.equals(this.mBytes, photoData.mBytes) || this.mIsPrimary != photoData.mIsPrimary) {
                        b = false;
                    }
                }
            }
            return b;
        }
        
        public byte[] getBytes() {
            return this.mBytes;
        }
        
        @Override
        public final EntryLabel getEntryLabel() {
            return EntryLabel.PHOTO;
        }
        
        public String getFormat() {
            return this.mFormat;
        }
        
        @Override
        public int hashCode() {
            int i = 0;
            if (this.mHashCode != null) {
                return this.mHashCode.intValue();
            }
            int hash;
            if (this.mFormat != null) {
                hash = this.mFormat.hashCode();
            } else {
                hash = 0;
            }
            hash *= 31;
            if (this.mBytes != null) {
                byte[] bArr = this.mBytes;
                while (i < bArr.length) {
                    hash += bArr[i];
                    i++;
                }
            }
            hash = (hash * 31) + (this.mIsPrimary ? 1231 : 1237);
            this.mHashCode = Integer.valueOf(hash);
            return hash;
        }
        
        @Override
        public boolean isEmpty() {
            return this.mBytes == null || this.mBytes.length == 0;
        }
        
        public boolean isPrimary() {
            return this.mIsPrimary;
        }
        
        @Override
        public String toString() {
            return String.format("format: %s: size: %d, isPrimary: %s", this.mFormat, this.mBytes.length, this.mIsPrimary);
        }
    }
    
    public static class PostalData implements EntryElement
    {
        private static final int ADDR_MAX_DATA_SIZE = 7;
        private final String mCountry;
        private final String mExtendedAddress;
        private boolean mIsPrimary;
        private final String mLabel;
        private final String mLocalty;
        private final String mPobox;
        private final String mPostalCode;
        private final String mRegion;
        private final String mStreet;
        private final int mType;
        private int mVCardType;
        
        public PostalData(final String mPobox, final String mExtendedAddress, final String mStreet, final String mLocalty, final String mRegion, final String mPostalCode, final String mCountry, final int mType, final String mLabel, final boolean mIsPrimary, final int mvCardType) {
            this.mType = mType;
            this.mPobox = mPobox;
            this.mExtendedAddress = mExtendedAddress;
            this.mStreet = mStreet;
            this.mLocalty = mLocalty;
            this.mRegion = mRegion;
            this.mPostalCode = mPostalCode;
            this.mCountry = mCountry;
            this.mLabel = mLabel;
            this.mIsPrimary = mIsPrimary;
            this.mVCardType = mvCardType;
        }

        public static PostalData constructPostalData(List<String> propValueList, int type, String label, boolean isPrimary, int vcardType) {
            int i;
            String[] dataArray = new String[7];
            int size = propValueList.size();
            if (size > 7) {
                size = 7;
            }
            int i2 = 0;
            for (String addressElement : propValueList) {
                dataArray[i2] = addressElement;
                i2++;
                if (i2 >= size) {
                    i = i2;
                    break;
                }
            }
            i = i2;
            while (i < 7) {
                i2 = i + 1;
                dataArray[i] = null;
                i = i2;
            }
            return new PostalData(dataArray[0], dataArray[1], dataArray[2], dataArray[3], dataArray[4], dataArray[5], dataArray[6], type, label, isPrimary, vcardType);
        }
        
        @Override
        public void constructInsertOperation(final List<ContentProviderOperation> list, final int n) {
            final ContentProviderOperation.Builder insert = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
            insert.withValueBackReference("raw_contact_id", n);
            insert.withValue("mimetype", "vnd.android.cursor.item/postal-address_v2");
            insert.withValue("data2", this.mType);
            if (this.mType == 0) {
                insert.withValue("data3", this.mLabel);
            }
            Object o;
            if (TextUtils.isEmpty((CharSequence)this.mStreet)) {
                if (TextUtils.isEmpty((CharSequence)this.mExtendedAddress)) {
                    o = null;
                }
                else {
                    o = this.mExtendedAddress;
                }
            }
            else if (TextUtils.isEmpty((CharSequence)this.mExtendedAddress)) {
                o = this.mStreet;
            }
            else {
                o = this.mStreet + " " + this.mExtendedAddress;
            }
            insert.withValue("data5", this.mPobox);
            insert.withValue("data4", o);
            insert.withValue("data7", this.mLocalty);
            insert.withValue("data8", this.mRegion);
            insert.withValue("data9", this.mPostalCode);
            insert.withValue("data10", this.mCountry);
            insert.withValue("data1", this.getFormattedAddress(this.mVCardType));
            if (this.mIsPrimary) {
                insert.withValue("is_primary", 1);
            }
            list.add(insert.build());
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b = true;
            if (this != o) {
                if (!(o instanceof PostalData)) {
                    b = false;
                }
                else {
                    final PostalData postalData = (PostalData)o;
                    if (this.mType != postalData.mType || (this.mType == 0 && !TextUtils.equals((CharSequence)this.mLabel, (CharSequence)postalData.mLabel)) || this.mIsPrimary != postalData.mIsPrimary || !TextUtils.equals((CharSequence)this.mPobox, (CharSequence)postalData.mPobox) || !TextUtils.equals((CharSequence)this.mExtendedAddress, (CharSequence)postalData.mExtendedAddress) || !TextUtils.equals((CharSequence)this.mStreet, (CharSequence)postalData.mStreet) || !TextUtils.equals((CharSequence)this.mLocalty, (CharSequence)postalData.mLocalty) || !TextUtils.equals((CharSequence)this.mRegion, (CharSequence)postalData.mRegion) || !TextUtils.equals((CharSequence)this.mPostalCode, (CharSequence)postalData.mPostalCode) || !TextUtils.equals((CharSequence)this.mCountry, (CharSequence)postalData.mCountry)) {
                        b = false;
                    }
                }
            }
            return b;
        }
        
        public String getCountry() {
            return this.mCountry;
        }
        
        @Override
        public final EntryLabel getEntryLabel() {
            return EntryLabel.POSTAL_ADDRESS;
        }
        
        public String getExtendedAddress() {
            return this.mExtendedAddress;
        }
        
        public String getFormattedAddress(int n) {
            final StringBuilder sb = new StringBuilder();
            final int n2 = 1;
            final int n3 = 1;
            final String[] array = { this.mPobox, this.mExtendedAddress, this.mStreet, this.mLocalty, this.mRegion, this.mPostalCode, this.mCountry };
            if (VCardConfig.isJapaneseDevice(n)) {
                int i = 6;
                n = n3;
                while (i >= 0) {
                    final String s = array[i];
                    int n4 = n;
                    if (!TextUtils.isEmpty((CharSequence)s)) {
                        if (n == 0) {
                            sb.append(' ');
                        }
                        else {
                            n = 0;
                        }
                        sb.append(s);
                        n4 = n;
                    }
                    --i;
                    n = n4;
                }
            }
            else {
                int j = 0;
                n = n2;
                while (j < 7) {
                    final String s2 = array[j];
                    int n5 = n;
                    if (!TextUtils.isEmpty((CharSequence)s2)) {
                        if (n == 0) {
                            sb.append(' ');
                        }
                        else {
                            n = 0;
                        }
                        sb.append(s2);
                        n5 = n;
                    }
                    ++j;
                    n = n5;
                }
            }
            return sb.toString().trim();
        }
        
        public String getLabel() {
            return this.mLabel;
        }
        
        public String getLocalty() {
            return this.mLocalty;
        }
        
        public String getPobox() {
            return this.mPobox;
        }
        
        public String getPostalCode() {
            return this.mPostalCode;
        }
        
        public String getRegion() {
            return this.mRegion;
        }
        
        public String getStreet() {
            return this.mStreet;
        }
        
        public int getType() {
            return this.mType;
        }
        
        @Override
        public int hashCode() {
            final int mType = this.mType;
            int hashCode;
            if (this.mLabel != null) {
                hashCode = this.mLabel.hashCode();
            }
            else {
                hashCode = 0;
            }
            int n;
            if (this.mIsPrimary) {
                n = 1231;
            }
            else {
                n = 1237;
            }
            int n2 = (mType * 31 + hashCode) * 31 + n;
            for (final String s : new String[] { this.mPobox, this.mExtendedAddress, this.mStreet, this.mLocalty, this.mRegion, this.mPostalCode, this.mCountry }) {
                int hashCode2;
                if (s != null) {
                    hashCode2 = s.hashCode();
                }
                else {
                    hashCode2 = 0;
                }
                n2 = n2 * 31 + hashCode2;
            }
            return n2;
        }
        
        @Override
        public boolean isEmpty() {
            return TextUtils.isEmpty((CharSequence)this.mPobox) && TextUtils.isEmpty((CharSequence)this.mExtendedAddress) && TextUtils.isEmpty((CharSequence)this.mStreet) && TextUtils.isEmpty((CharSequence)this.mLocalty) && TextUtils.isEmpty((CharSequence)this.mRegion) && TextUtils.isEmpty((CharSequence)this.mPostalCode) && TextUtils.isEmpty((CharSequence)this.mCountry);
        }
        
        public boolean isPrimary() {
            return this.mIsPrimary;
        }
        
        @Override
        public String toString() {
            return String.format("type: %d, label: %s, isPrimary: %s, pobox: %s, extendedAddress: %s, street: %s, localty: %s, region: %s, postalCode %s, country: %s", this.mType, this.mLabel, this.mIsPrimary, this.mPobox, this.mExtendedAddress, this.mStreet, this.mLocalty, this.mRegion, this.mPostalCode, this.mCountry);
        }
    }
    
    public static class SipData implements EntryElement
    {
        private final String mAddress;
        private final boolean mIsPrimary;
        private final String mLabel;
        private final int mType;
        
        public SipData(final String mAddress, final int mType, final String mLabel, final boolean mIsPrimary) {
            if (mAddress.startsWith("sip:")) {
                this.mAddress = mAddress.substring(4);
            }
            else {
                this.mAddress = mAddress;
            }
            this.mType = mType;
            this.mLabel = mLabel;
            this.mIsPrimary = mIsPrimary;
        }
        
        @Override
        public void constructInsertOperation(final List<ContentProviderOperation> list, final int n) {
            final ContentProviderOperation.Builder insert = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
            insert.withValueBackReference("raw_contact_id", n);
            insert.withValue("mimetype", "vnd.android.cursor.item/sip_address");
            insert.withValue("data1", this.mAddress);
            insert.withValue("data2", this.mType);
            if (this.mType == 0) {
                insert.withValue("data3", this.mLabel);
            }
            if (this.mIsPrimary) {
                insert.withValue("is_primary", this.mIsPrimary);
            }
            list.add(insert.build());
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b = true;
            if (this != o) {
                if (!(o instanceof SipData)) {
                    b = false;
                }
                else {
                    final SipData sipData = (SipData)o;
                    if (this.mType != sipData.mType || !TextUtils.equals((CharSequence)this.mLabel, (CharSequence)sipData.mLabel) || !TextUtils.equals((CharSequence)this.mAddress, (CharSequence)sipData.mAddress) || this.mIsPrimary != sipData.mIsPrimary) {
                        b = false;
                    }
                }
            }
            return b;
        }
        
        public String getAddress() {
            return this.mAddress;
        }
        
        @Override
        public EntryLabel getEntryLabel() {
            return EntryLabel.SIP;
        }
        
        public String getLabel() {
            return this.mLabel;
        }
        
        public int getType() {
            return this.mType;
        }
        
        @Override
        public int hashCode() {
            int hashCode = 0;
            final int mType = this.mType;
            int hashCode2;
            if (this.mLabel != null) {
                hashCode2 = this.mLabel.hashCode();
            }
            else {
                hashCode2 = 0;
            }
            if (this.mAddress != null) {
                hashCode = this.mAddress.hashCode();
            }
            int n;
            if (this.mIsPrimary) {
                n = 1231;
            }
            else {
                n = 1237;
            }
            return ((mType * 31 + hashCode2) * 31 + hashCode) * 31 + n;
        }
        
        @Override
        public boolean isEmpty() {
            return TextUtils.isEmpty((CharSequence)this.mAddress);
        }
        
        @Override
        public String toString() {
            return "sip: " + this.mAddress;
        }
    }
    
    private class ToStringIterator implements EntryElementIterator
    {
        private StringBuilder mBuilder;
        private boolean mFirstElement;
        
        @Override
        public boolean onElement(final EntryElement entryElement) {
            if (!this.mFirstElement) {
                this.mBuilder.append(", ");
                this.mFirstElement = false;
            }
            this.mBuilder.append("[").append(entryElement.toString()).append("]");
            return true;
        }
        
        @Override
        public void onElementGroupEnded() {
            this.mBuilder.append("\n");
        }
        
        @Override
        public void onElementGroupStarted(final EntryLabel entryLabel) {
            this.mBuilder.append(entryLabel.toString() + ": ");
            this.mFirstElement = true;
        }
        
        @Override
        public void onIterationEnded() {
            this.mBuilder.append("]]\n");
        }
        
        @Override
        public void onIterationStarted() {
            (this.mBuilder = new StringBuilder()).append("[[hash: " + VCardEntry.this.hashCode() + "\n");
        }
        
        @Override
        public String toString() {
            return this.mBuilder.toString();
        }
    }
    
    public static class WebsiteData implements EntryElement
    {
        private final String mWebsite;
        
        public WebsiteData(final String mWebsite) {
            this.mWebsite = mWebsite;
        }
        
        @Override
        public void constructInsertOperation(final List<ContentProviderOperation> list, final int n) {
            final ContentProviderOperation.Builder insert = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
            insert.withValueBackReference("raw_contact_id", n);
            insert.withValue("mimetype", "vnd.android.cursor.item/website");
            insert.withValue("data1", this.mWebsite);
            insert.withValue("data2", 1);
            list.add(insert.build());
        }
        
        @Override
        public boolean equals(final Object o) {
            return this == o || (o instanceof WebsiteData && TextUtils.equals((CharSequence)this.mWebsite, (CharSequence)((WebsiteData)o).mWebsite));
        }
        
        @Override
        public EntryLabel getEntryLabel() {
            return EntryLabel.WEBSITE;
        }
        
        public String getWebsite() {
            return this.mWebsite;
        }
        
        @Override
        public int hashCode() {
            int hashCode;
            if (this.mWebsite != null) {
                hashCode = this.mWebsite.hashCode();
            }
            else {
                hashCode = 0;
            }
            return hashCode;
        }
        
        @Override
        public boolean isEmpty() {
            return TextUtils.isEmpty((CharSequence)this.mWebsite);
        }
        
        @Override
        public String toString() {
            return "website: " + this.mWebsite;
        }
    }
}
