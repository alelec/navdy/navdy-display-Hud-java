package com.navdy.hud.app.ui.component.mainmenu;

import com.navdy.hud.app.R;
import com.navdy.hud.app.maps.MapEvents$MapEngineReady;
import com.navdy.hud.app.maps.MapEvents$NavigationModeChange;
import com.navdy.hud.app.maps.here.HereRouteCache$RouteInfo;
import com.navdy.hud.app.ui.component.homescreen.HomeScreenView;
import mortar.Presenter;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;
import com.here.android.mpa.common.GeoCoordinate;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.here.android.mpa.routing.Route;
import com.navdy.hud.app.maps.MapEvents;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.event.DeviceInfoAvailable;
import com.squareup.otto.Subscribe;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import java.util.Iterator;
import android.os.Parcelable;
import com.navdy.hud.app.framework.contacts.Contact;
import java.util.ArrayList;
import android.text.TextUtils;
import android.os.Bundle;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder;
import com.navdy.hud.app.maps.here.HereRouteCache;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.here.android.mpa.common.GeoBoundingBox;
import com.navdy.hud.app.ui.component.ConfirmationLayout;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.animation.Animator.AnimatorListener;
import com.navdy.service.library.events.ui.ShowScreen;
import android.animation.Animator;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.ui.component.homescreen.NavigationView;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import java.util.List;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.framework.trips.TripManager;
import com.navdy.hud.app.ui.component.homescreen.HomeScreen;
import android.content.SharedPreferences;
import android.view.View;
import javax.inject.Inject;
import com.squareup.otto.Bus;
import javax.inject.Singleton;
import com.navdy.hud.app.ui.framework.BasePresenter;
import com.navdy.hud.app.ui.activity.Main;
import dagger.Module;
import com.navdy.service.library.events.ui.Screen;
import flow.Flow;
import com.navdy.service.library.log.Logger;
import flow.Layout;
import com.navdy.hud.app.screen.BaseScreen;

import static android.view.View.GONE;

@Layout(R.layout.screen_main_menu_2)
public class MainMenuScreen2 extends BaseScreen
{
    static final String ARG_CONTACTS_LIST = "CONTACTS";
    static final String ARG_MENU_MODE = "MENU_MODE";
    static final String ARG_MENU_PATH = "MENU_PATH";
    static final String ARG_NOTIFICATION_ID = "NOTIF_ID";
    static final String SLASH = "/";
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(MainMenuScreen2.class);
    }

    @Override
    public int getAnimationIn(final Flow.Direction direction) {
        return -1;
    }
    
    @Override
    public int getAnimationOut(final Flow.Direction direction) {
        return -1;
    }
    
    @Override
    public Object getDaggerModule() {
        return new Module();
    }
    
    @Override
    public String getMortarScopeName() {
        return this.getClass().getName();
    }
    
    @Override
    public Screen getScreen() {
        return Screen.SCREEN_MAIN_MENU;
    }

    public enum MenuMode {
        MAIN_MENU(0),
        REPLY_PICKER(1),
        SNAPSHOT_TITLE_PICKER(2);

        private int value;
        MenuMode(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
    @dagger.Module(addsTo = Main.Module.class, injects = { MainMenuView2.class })
    public class Module
    {
    }
    
    @Singleton
    public static class Presenter extends BasePresenter<MainMenuView2>
    {
        private boolean animateIn;
        @Inject
        Bus bus;
        private boolean closed;
        private IMenu currentMenu;
        private boolean firstLoad;
        private boolean handledSelection;
        private View[] leftView;
        private MainMenu mainMenu;
        private boolean mapShown;
        private MenuMode menuMode;
        private String menuPath;
        private boolean navEnded;
        private boolean registered;
        @Inject
        SharedPreferences sharedPreferences;
        private HomeScreen.DisplayMode switchBackMode;
        @Inject
        TripManager tripManager;
        private UIStateManager uiStateManager;
        
        private int findEntry(final List<VerticalList.Model> list, final int n) {
            for (int size = list.size(), i = 0; i < size; ++i) {
                if (list.get(i).id == n) {
                    return i;
                }
            }
            return -1;
        }
        
        IMenu buildMenuPath(MainMenu mainMenu, String substring) {
            try {
                if (substring.indexOf("/") != 0) {
                    MainMenuScreen2.sLogger.w("menu_path does not start with slash");
                    return mainMenu;
                }

                    String s = substring.substring(1);
                    final int index = s.indexOf("/");
                    if (index >= 0) {
                        substring = substring.substring(index + 1);
                        s = s.substring(0, index);
                    }
                    else {
                        substring = null;
                    }
                    final IMenu childMenu = mainMenu.getChildMenu(null, s, substring);
                    if (childMenu != null) {
                       // mainMenu = (MainMenu)childMenu;
                        return  childMenu;
                    }

                return mainMenu;
            }
            catch (Throwable t) {
                MainMenuScreen2.sLogger.e(t);
                return mainMenu;
            }
        }
        
        void cancelLoadingAnimation(final int n) {
            final MainMenuView2 mainMenuView2 = this.getView();
            if (mainMenuView2 != null) {
                mainMenuView2.vmenuComponent.verticalList.cancelLoadingAnimation(n);
            }
        }
        
        public void cleanMapFluctuator() {
            MainMenuScreen2.sLogger.v("cleanMapFluctuator");
            final NavigationView navigationView = this.uiStateManager.getNavigationView();
            if (navigationView != null) {
                navigationView.cleanupFluctuator();
            }
        }
        
        void close() {
            if (this.currentMenu != null && this.currentMenu.getType() == IMenu.Menu.SEARCH) {
                AnalyticsSupport.recordNearbySearchClosed();
            }
            this.close(null);
        }
        
        void close(final Runnable runnable) {
            if (this.closed) {
                MainMenuScreen2.sLogger.v("already closed");
            }
            else {
                this.closed = true;
                MainMenuScreen2.sLogger.v("close");
                AnalyticsSupport.recordMenuSelection("exit");
                final MainMenuView2 mainMenuView2 = this.getView();
                if (mainMenuView2 != null) {
                    mainMenuView2.vmenuComponent.animateOut(new DefaultAnimationListener() {
                        @Override
                        public void onAnimationEnd(final Animator animator) {
                            MainMenuScreen2.sLogger.v("post back");
                            Presenter.this.bus.post(new ShowScreen.Builder().screen(Screen.SCREEN_BACK).build());
                            if (runnable != null) {
                                runnable.run();
                            }
                        }
                    });
                }
            }
        }
        
        public void disableMapViews() {
            final MainMenuView2 mainMenuView2 = this.getView();
            if (mainMenuView2 != null) {
                MainMenuScreen2.sLogger.v("disableMapViews");
                mainMenuView2.setBackgroundColor(-16777216);
                mainMenuView2.vmenuComponent.leftContainer.removeAllViews();
                mainMenuView2.vmenuComponent.leftContainer.setBackgroundColor(-16777216);
                if (this.leftView != null) {
                    for (View aLeftView : this.leftView) {
                        mainMenuView2.vmenuComponent.leftContainer.addView(aLeftView);
                    }
                    this.leftView = null;
                }
                mainMenuView2.vmenuComponent.rightContainer.setBackgroundColor(0);
                mainMenuView2.vmenuComponent.closeContainer.setBackgroundColor(0);
                mainMenuView2.rightBackground.setVisibility(GONE);
            }
        }
        
        public void enableMapViews() {
            final MainMenuView2 mainMenuView2 = this.getView();
            if (mainMenuView2 != null) {
                final int childCount = mainMenuView2.vmenuComponent.leftContainer.getChildCount();
                MainMenuScreen2.sLogger.v("enableMapViews:" + childCount);
                this.leftView = new View[childCount];
                for (int i = 0; i < childCount; ++i) {
                    this.leftView[i] = mainMenuView2.vmenuComponent.leftContainer.getChildAt(i);
                }
                mainMenuView2.vmenuComponent.leftContainer.removeAllViews();
                mainMenuView2.vmenuComponent.leftContainer.setBackgroundColor(0);
                mainMenuView2.vmenuComponent.leftContainer.addView(LayoutInflater.from(mainMenuView2.getContext()).inflate(R.layout.active_trip_menu_mask_lyt, mainMenuView2, false));
                mainMenuView2.vmenuComponent.rightContainer.setBackgroundColor(-16777216);
                mainMenuView2.vmenuComponent.closeContainer.setBackgroundColor(-16777216);
                mainMenuView2.rightBackground.setVisibility(View.VISIBLE);
            }
        }
        
        ConfirmationLayout getConfirmationLayout() {
            final MainMenuView2 mainMenuView2 = this.getView();
            ConfirmationLayout confirmationLayout;
            if (mainMenuView2 != null) {
                confirmationLayout = mainMenuView2.confirmationLayout;
            }
            else {
                confirmationLayout = null;
            }
            return confirmationLayout;
        }
        
        IMenu getCurrentMenu() {
            return this.currentMenu;
        }

        public GeoBoundingBox getCurrentRouteBoundingBox() {
            String routeId = HereNavigationManager.getInstance().getCurrentRouteId();
            if (routeId != null) {
                HereRouteCache$RouteInfo routeInfo = HereRouteCache.getInstance().getRoute(routeId);
                if (routeInfo != null) {
                    return routeInfo.route.getBoundingBox();
                }
            }
            return null;
        }
        
        int getCurrentSelection() {
            final MainMenuView2 mainMenuView2 = this.getView();
            int currentPosition;
            if (mainMenuView2 != null) {
                currentPosition = mainMenuView2.vmenuComponent.verticalList.getCurrentPosition();
            }
            else {
                currentPosition = 0;
            }
            return currentPosition;
        }

        public int getCurrentSubSelectionId() {
            MainMenuView2 view = getView();
            if (view != null) {
                VerticalViewHolder vh = view.vmenuComponent.verticalList.getCurrentViewHolder();
                if (vh instanceof IconOptionsViewHolder) {
                    return ((IconOptionsViewHolder) vh).getCurrentSelectionId();
                }
            }
            return -1;
        }

        public int getCurrentSubSelectionPos() {
            MainMenuView2 view = getView();
            if (view != null) {
                VerticalViewHolder vh = view.vmenuComponent.verticalList.getCurrentViewHolder();
                if (vh instanceof IconOptionsViewHolder) {
                    return ((IconOptionsViewHolder) vh).getCurrentSelection();
                }
            }
            return -1;
        }
        
        public void hideMap() {
            boolean b = true;
            if (this.mapShown) {
                this.mapShown = false;
                if (this.getView() != null) {
                    MainMenuScreen2.sLogger.v("hideMap");
                    this.disableMapViews();
                    if (this.switchBackMode != null) {
                        MainMenuScreen2.sLogger.v("hideMap switchbackmode: " + this.switchBackMode);
                        this.uiStateManager.getHomescreenView().setDisplayMode(this.switchBackMode);
                    }
                    final NavigationView navigationView = this.uiStateManager.getNavigationView();
                    if (!this.navEnded) {
                        final HereNavigationManager instance = HereNavigationManager.getInstance();
                        if (instance.isNavigationModeOn()) {
                            if (instance.hasArrived()) {
                                this.navEnded = true;
                            }
                        }
                        else {
                            this.navEnded = true;
                        }
                    }
                    if (this.navEnded) {
                        b = false;
                    }
                    navigationView.switchBackfromRouteSearchMode(b);
                }
            }
        }
        
        public void hideScrimCover() {
            final MainMenuView2 mainMenuView2 = this.getView();
            if (mainMenuView2 != null) {
                MainMenuScreen2.sLogger.v("scrim-hide");
                mainMenuView2.scrimCover.setVisibility(GONE);
            }
        }
        
        public void hideToolTip() {
            final MainMenuView2 mainMenuView2 = this.getView();
            if (mainMenuView2 != null) {
                mainMenuView2.vmenuComponent.hideToolTip();
            }
        }
        
        void init(final MainMenuView2 mainMenuView2, final Bundle bundle) {
            switch (this.menuMode) {
                case MAIN_MENU:
                    this.mainMenu = new MainMenu(this.bus, mainMenuView2.vmenuComponent, this);
                    if (TextUtils.isEmpty(this.menuPath)) {
                        this.loadMenu(this.mainMenu, null, 0, 0);
                        break;
                    }
                    this.loadMenu(this.buildMenuPath(this.mainMenu, this.menuPath), null, 0, 0);
                    break;
                case REPLY_PICKER: {
                    final ArrayList parcelableArrayList = bundle.getParcelableArrayList(ARG_CONTACTS_LIST);
                    final String string = bundle.getString(ARG_NOTIFICATION_ID);
                    final ArrayList<Contact> list = new ArrayList<>();
                    for (Parcelable aParcelableArrayList : (Iterable<Parcelable>) parcelableArrayList) {
                        list.add((Contact) aParcelableArrayList);
                    }
                    this.loadMenu(this.currentMenu = new ContactOptionsMenu(list, string, mainMenuView2.vmenuComponent, this, null, this.bus), null, 0, 0);
                    break;
                }
                case SNAPSHOT_TITLE_PICKER:
                    this.loadMenu(this.currentMenu = new ReportIssueMenu(mainMenuView2.vmenuComponent, this, null, ReportIssueMenu.ReportIssueMenuType.SNAP_SHOT), null, 0, 0);
                    break;
            }
        }
        
        boolean isClosed() {
            return this.closed;
        }
        
        boolean isItemClickable(final VerticalList.ItemSelectionState itemSelectionState) {
            return this.currentMenu != null && this.currentMenu.isItemClickable(itemSelectionState.id, itemSelectionState.pos);
        }
        
        public boolean isMapShown() {
            return this.mapShown;
        }
        
        void loadMenu(final IMenu menu, final IMenu.MenuLevel menuLevel, final int n, final int n2) {
            this.loadMenu(menu, menuLevel, n, n2, false);
        }
        
        void loadMenu(final IMenu menu, final IMenu.MenuLevel menuLevel, final int n, final int n2, final boolean b) {
            this.loadMenu(menu, menuLevel, n, n2, b, 0);
        }
        
        void loadMenu(final IMenu currentMenu, final IMenu.MenuLevel menuLevel, int backSelectionPos, final int n, final boolean b, final int n2) {
            this.hideToolTip();
            if (menuLevel == IMenu.MenuLevel.ROOT && this.isMapShown()) {
                this.disableMapViews();
            }
            final MainMenuView2 mainMenuView2 = this.getView();
            if (mainMenuView2 != null) {
                if (this.firstLoad) {
                    MainMenuScreen2.sLogger.v("Loading menu:" + currentMenu.getType());
                    (this.currentMenu = currentMenu).setSelectedIcon();
                    this.firstLoad = false;
                    mainMenuView2.vmenuComponent.verticalList.setBindCallbacks(this.currentMenu.isBindCallsEnabled());
                    mainMenuView2.vmenuComponent.updateView(this.currentMenu.getItems(), this.currentMenu.getInitialSelection(), this.currentMenu.isFirstItemEmpty());
                }
                else {
                    switch (menuLevel) {
                        case SUB_LEVEL: {
                            MainMenuScreen2.sLogger.v("Loading menu:" + currentMenu.getType());
                            final VerticalList.Model modelfromPos = this.currentMenu.getModelfromPos(backSelectionPos);
                            if (this.currentMenu != null) {
                                this.currentMenu.onUnload(IMenu.MenuLevel.SUB_LEVEL);
                            }
                            (this.currentMenu = currentMenu).setBackSelectionPos(backSelectionPos);
                            this.currentMenu.setSelectedIcon();
                            mainMenuView2.vmenuComponent.performEnterAnimation(new Runnable() {
                                public void run() {
                                    MainMenuView2 view = Presenter.this.getView();
                                    if (view != null) {
                                        MainMenuScreen2.sLogger.v("performEnterAnimation:" + Presenter.this.currentMenu.getType());
                                        view.vmenuComponent.unlock(false);
                                        view.vmenuComponent.verticalList.setBindCallbacks(Presenter.this.currentMenu.isBindCallsEnabled());
                                        view.vmenuComponent.updateView(Presenter.this.currentMenu.getItems(), Presenter.this.currentMenu.getInitialSelection(), true, b, Presenter.this.currentMenu.getScrollIndex());
                                    }
                                }
                            }, null, modelfromPos);
                            break;
                        }
                        case BACK_TO_PARENT: {
                            MainMenuScreen2.sLogger.v("backSelectionId:" + n);
                            final List<VerticalList.Model> items = currentMenu.getItems();
                            int n3 = backSelectionPos;
                            if (n != 0) {
                                final int entry = this.findEntry(items, n);
                                if (entry != -1) {
                                    backSelectionPos = entry;
                                }
                                MainMenuScreen2.sLogger.v("backSelectionId:" + n + " index=" + entry);
                                n3 = backSelectionPos;
                            }
                            VerticalList.Model model = null;
                            if (n3 < items.size()) {
                                model = items.get(n3);
                            }
                            final int finalN = n3;
                            mainMenuView2.vmenuComponent.performBackAnimation(new Runnable() {
                                @Override
                                public void run() {
                                    final MainMenuView2 mainMenuView2 = Presenter.this.getView();
                                    if (mainMenuView2 != null) {
                                        MainMenuScreen2.sLogger.v("Loading menu:" + currentMenu.getType());
                                        if (Presenter.this.currentMenu != null) {
                                            Presenter.this.currentMenu.onUnload(IMenu.MenuLevel.BACK_TO_PARENT);
                                        }
                                        Presenter.this.currentMenu = currentMenu;
                                        Presenter.this.currentMenu.setSelectedIcon();
                                        MainMenuScreen2.sLogger.v("performBackAnimation:" + Presenter.this.currentMenu.getType());
                                        mainMenuView2.vmenuComponent.unlock(false);
                                        mainMenuView2.vmenuComponent.verticalList.setBindCallbacks(Presenter.this.currentMenu.isBindCallsEnabled());
                                        mainMenuView2.vmenuComponent.updateView(items, finalN, Presenter.this.currentMenu.isFirstItemEmpty(), b, Presenter.this.currentMenu.getScrollIndex());
                                    }
                                }
                            }, null, model, n2);
                            break;
                        }
                        case REFRESH_CURRENT:
                            if (this.currentMenu == currentMenu) {
                                MainMenuScreen2.sLogger.v("refresh current menu:" + this.currentMenu.getType());
                                mainMenuView2.vmenuComponent.updateView(this.currentMenu.getItems(), this.currentMenu.getInitialSelection(), true, b, mainMenuView2.vmenuComponent.getFastScrollIndex());
                                break;
                            }
                            MainMenuScreen2.sLogger.w("refresh current menu different cur=" + this.currentMenu.getType() + " passed:" + currentMenu.getType());
                            break;
                        case ROOT:
                            MainMenuScreen2.sLogger.v("refresh root menu:" + currentMenu.getType());
                            (this.currentMenu = currentMenu).setSelectedIcon();
                            mainMenuView2.vmenuComponent.updateView(currentMenu.getItems(), currentMenu.getInitialSelection(), this.currentMenu.isFirstItemEmpty());
                            break;
                    }
                }
            }
        }
        
        @Subscribe
        public void onConnectionStateChange(final ConnectionStateChange connectionStateChange) {
            Label_0007: {
                if (this.currentMenu != null) {
                    switch (connectionStateChange.state) {
                        case CONNECTION_DISCONNECTED:
                            switch (this.menuMode) {
                                default:
                                    break Label_0007;
                                case MAIN_MENU:
                                    MainMenuScreen2.sLogger.v("disconnected:" + this.currentMenu.getType());
                                    if (this.mainMenu != null) {
                                        this.mainMenu.clearState();
                                    }
                                    if (this.currentMenu.getType() == IMenu.Menu.MAIN) {
                                        MainMenuScreen2.sLogger.v("disconnected: refresh main menu");
                                        this.loadMenu(this.currentMenu, IMenu.MenuLevel.REFRESH_CURRENT, 0, 0);
                                        break Label_0007;
                                    }
                                    MainMenuScreen2.sLogger.v("disconnected: refresh menu,back to root");
                                    this.loadMenu(this.mainMenu, IMenu.MenuLevel.ROOT, 0, 0);
                                    break Label_0007;
                                case REPLY_PICKER:
                                    this.close();
                            }
//                            break;
                    }
                }
            }
        }
        
        @Subscribe
        public void onDeviceInfoAvailable(final DeviceInfoAvailable deviceInfoAvailable) {
            if (this.currentMenu != null && this.menuMode == MenuMode.MAIN_MENU) {
                MainMenuScreen2.sLogger.v("onDeviceInfoAvailable:" + this.currentMenu.getType());
                if (this.currentMenu.getType() == IMenu.Menu.MAIN && this.currentMenu.getModelfromPos(this.currentMenu.getInitialSelection()).id == R.id.main_menu_settings_connect_phone) {
                    MainMenuScreen2.sLogger.v("connected: refresh main menu");
                    this.loadMenu(this.currentMenu, IMenu.MenuLevel.REFRESH_CURRENT, 0, 0);
                }
            }
        }
        
        @Override
        public void onLoad(final Bundle bundle) {
            this.reset();
            this.uiStateManager = RemoteDeviceManager.getInstance().getUiStateManager();
            this.bus.register(this);
            this.registered = true;
            this.firstLoad = true;
            this.menuPath = null;
            this.menuMode = MenuMode.MAIN_MENU;
            if (bundle != null) {
                this.menuPath = bundle.getString(ARG_MENU_PATH);
                MainMenuScreen2.sLogger.v("menu_path:" + this.menuPath);
                final int int1 = bundle.getInt(ARG_MENU_MODE, -1);
                if (int1 != -1) {
                    this.menuMode = MenuMode.values()[int1];
                    MainMenuScreen2.sLogger.v("menu_mode:" + this.menuMode);
                }
            }
            this.updateView(bundle);
            super.onLoad(bundle);
        }
        
        @Subscribe
        public void onMapEngineReady(final MapEvents$MapEngineReady mapEngineReady) {
            if (this.currentMenu != null && this.menuMode == MenuMode.MAIN_MENU) {
                MainMenuScreen2.sLogger.v("onMapEngineReady");
                switch (this.currentMenu.getType()) {
                    case MAIN:
                        this.loadMenu(this.currentMenu, IMenu.MenuLevel.REFRESH_CURRENT, 0, 0);
                        break;
                }
            }
        }
        
        @Subscribe
        public void onNavigationModeChanged(final MapEvents$NavigationModeChange navigationModeChange) {
            if (this.currentMenu != null && this.menuMode == MenuMode.MAIN_MENU) {
                MainMenuScreen2.sLogger.v("onNavigationModeChanged:" + navigationModeChange.navigationMode);
                switch (this.currentMenu.getType()) {
                    case MAIN:
                        this.loadMenu(this.currentMenu, IMenu.MenuLevel.REFRESH_CURRENT, 0, 0);
                        break;
                    case ACTIVE_TRIP:
                        this.loadMenu(this.mainMenu, IMenu.MenuLevel.ROOT, 0, 0);
                        break;
                }
            }
        }
        
        public void onUnload() {
            if (this.registered) {
                this.registered = false;
                this.bus.unregister(this);
            }
            this.reset();
            this.mainMenu = null;
            this.currentMenu = null;
            super.onUnload();
        }
        
        void performSelectionAnimation(final Runnable runnable) {
            this.performSelectionAnimation(runnable, 0);
        }
        
        void performSelectionAnimation(final Runnable runnable, final int n) {
            final MainMenuView2 mainMenuView2 = this.getView();
            if (mainMenuView2 != null) {
                mainMenuView2.performSelectionAnimation(runnable, n);
            }
        }
        
        void refreshData(final int n, final VerticalList.Model[] array, final IMenu menu) {
            final MainMenuView2 mainMenuView2 = this.getView();
            if (mainMenuView2 != null) {
                if (this.closed) {
                    MainMenuScreen2.sLogger.v("refreshData(s) already closed");
                }
                else if (this.currentMenu == menu) {
                    MainMenuScreen2.sLogger.v("refreshData(s):" + menu.getType());
                    mainMenuView2.vmenuComponent.verticalList.refreshData(n, array);
                }
                else {
                    MainMenuScreen2.sLogger.v("refreshData(s) update menu:" + menu.getType() + " different from current:" + this.currentMenu.getType());
                }
            }
        }
        
        void refreshDataforPos(final int n) {
            this.refreshDataforPos(n, true);
        }
        
        void refreshDataforPos(final int n, final boolean b) {
            final MainMenuView2 mainMenuView2 = this.getView();
            if (mainMenuView2 != null) {
                mainMenuView2.vmenuComponent.verticalList.refreshData(n, null, !b);
            }
        }
        
        void reset() {
            this.closed = false;
            this.handledSelection = false;
            this.animateIn = false;
            this.firstLoad = false;
            this.mapShown = false;
            this.navEnded = false;
        }
        
        void resetSelectedItem() {
            MainMenuScreen2.sLogger.v("resetSelectedItem");
            this.handledSelection = false;
            if (!this.animateIn) {
                this.animateIn = true;
                final MainMenuView2 mainMenuView2 = this.getView();
                if (mainMenuView2 != null) {
                    mainMenuView2.vmenuComponent.animateIn(null);
                }
            }
        }
        
        boolean selectItem(final VerticalList.ItemSelectionState itemSelectionState) {
            boolean handledSelection = true;
            if (this.currentMenu != null) {
                if (this.handledSelection) {
                    MainMenuScreen2.sLogger.v("already handled [" + itemSelectionState.id + "], " + itemSelectionState.pos);
                }
                else {
                    this.handledSelection = this.currentMenu.selectItem(itemSelectionState);
                    handledSelection = this.handledSelection;
                }
            }
            return handledSelection;
        }
        
        void sendCloseEvent() {
            this.hideMap();
            if (this.currentMenu != this.mainMenu) {
                this.currentMenu.onUnload(IMenu.MenuLevel.CLOSE);
            }
            if (this.mainMenu != null) {
                this.mainMenu.onUnload(IMenu.MenuLevel.CLOSE);
            }
        }
        
        void setInitialItemState(final boolean initialState) {
            final MainMenuView2 mainMenuView2 = this.getView();
            if (mainMenuView2 != null) {
                mainMenuView2.vmenuComponent.verticalList.adapter.setInitialState(initialState);
            }
        }
        
        public void setNavEnded() {
            this.navEnded = true;
        }
        
        void setScrollIdleEvents(final boolean scrollIdleEvent) {
            final MainMenuView2 mainMenuView2 = this.getView();
            if (mainMenuView2 != null) {
                mainMenuView2.vmenuComponent.verticalList.setScrollIdleEvent(scrollIdleEvent);
            }
        }
        
        public void setViewBackgroundColor(final int backgroundColor) {
            final MainMenuView2 mainMenuView2 = this.getView();
            if (mainMenuView2 != null) {
                mainMenuView2.setBackgroundColor(backgroundColor);
            }
        }
        
        public void showBoundingBox(final GeoBoundingBox geoBoundingBox) {
            if (this.getView() != null && this.mapShown && geoBoundingBox != null) {
                this.uiStateManager.getNavigationView().zoomToBoundBox(geoBoundingBox, null, false, false);
            }
        }

        public void showMap() {
            if (this.mapShown) {
                MainMenuScreen2.sLogger.v("showMap: already shown");
                return;
            }
            this.mapShown = true;
            if (getView() != null) {
                GeoBoundingBox boundingBox;
                GeoBoundingBox boundingBox2;
                MainMenuScreen2.sLogger.v("showMap");
                HomeScreenView homeScreenView = this.uiStateManager.getHomescreenView();
                NavigationView navigationView = this.uiStateManager.getNavigationView();
                this.switchBackMode = homeScreenView.getDisplayMode();
                if (this.switchBackMode == HomeScreen.DisplayMode.MAP) {
                    MainMenuScreen2.sLogger.v("showMap: switchbackmode null");
                    this.switchBackMode = null;
                } else {
                    MainMenuScreen2.sLogger.v(" showMap switchbackmode: " + this.switchBackMode);
                    homeScreenView.setDisplayMode(HomeScreen.DisplayMode.MAP);
                }
                HereNavigationManager hereNavigationManager = HereNavigationManager.getInstance();
                GeoCoordinate start = HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
                GeoCoordinate end = null;
                NavigationRouteRequest current = hereNavigationManager.getCurrentNavigationRouteRequest();
                if (current != null) {
                    if (current.destination.latitude != 0.0d && current.destination.longitude != 0.0d) {
                        end = new GeoCoordinate(current.destination.latitude, current.destination.longitude);
                    } else if (!(current.destinationDisplay == null || current.destinationDisplay.latitude == 0.0d || current.destinationDisplay.longitude == 0.0d))
                        end = new GeoCoordinate(current.destinationDisplay.latitude, current.destinationDisplay.longitude);
                }
                String routeId = hereNavigationManager.getCurrentRouteId();
                Route route = null;
                if (routeId != null) {
                    HereRouteCache$RouteInfo routeInfo = HereRouteCache.getInstance().getRoute(routeId);
                    if (routeInfo != null) {
                        route = routeInfo.route;
                        boundingBox = routeInfo.route.getBoundingBox();
                        if (boundingBox == null && start != null) {
                            boundingBox2 = new GeoBoundingBox(start, 5000.0f, 5000.0f);
                            navigationView.switchToRouteSearchMode(start, end, boundingBox2, route, false, null, -1);
                            enableMapViews();
                        }
                        boundingBox2 = boundingBox;
                        navigationView.switchToRouteSearchMode(start, end, boundingBox2, route, false, null, -1);
                        enableMapViews();
                    }
                }
                boundingBox = null;
                try {
                    boundingBox2 = new GeoBoundingBox(start, 5000.0f, 5000.0f);
                    navigationView.switchToRouteSearchMode(start, end, boundingBox2, route, false, null, -1);
                } catch (Throwable t) {
                    MainMenuScreen2.sLogger.e(t);
                }
                enableMapViews();
            }
        }

        public void showScrimCover() {
            final MainMenuView2 mainMenuView2 = this.getView();
            if (mainMenuView2 != null) {
                MainMenuScreen2.sLogger.v("scrim-show");
                mainMenuView2.scrimCover.setVisibility(View.VISIBLE);
            }
        }
        
        public void showToolTip(final int n, final String s) {
            final MainMenuView2 mainMenuView2 = this.getView();
            if (mainMenuView2 != null) {
                mainMenuView2.vmenuComponent.showToolTip(n, s);
            }
        }
        
        void updateCurrentMenu(final IMenu menu) {
            final MainMenuView2 mainMenuView2 = this.getView();
            if (mainMenuView2 != null) {
                if (this.closed) {
                    MainMenuScreen2.sLogger.v("updateCurrentMenu already closed");
                }
                else if (this.currentMenu == menu) {
                    MainMenuScreen2.sLogger.v("updateCurrentMenu:" + menu.getType());
                    mainMenuView2.vmenuComponent.unlock(false);
                    mainMenuView2.vmenuComponent.verticalList.setBindCallbacks(this.currentMenu.isBindCallsEnabled());
                    mainMenuView2.vmenuComponent.updateView(this.currentMenu.getItems(), this.currentMenu.getInitialSelection(), true, false, this.currentMenu.getScrollIndex());
                }
                else {
                    MainMenuScreen2.sLogger.v("updateCurrentMenu update menu:" + menu.getType() + " different from current:" + this.currentMenu.getType());
                }
            }
        }
        
        protected void updateView(final Bundle bundle) {
            final MainMenuView2 mainMenuView2 = this.getView();
            if (mainMenuView2 != null) {
                this.init(mainMenuView2, bundle);
            }
        }
    }
}
