package com.navdy.hud.app.ui.component.mainmenu;

import android.view.View;
import com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import java.util.List;

public interface IMenu
{
    IMenu getChildMenu(final IMenu p0, final String p1, final String p2);
    
    int getInitialSelection();
    
    List<VerticalList.Model> getItems();
    
    VerticalList.Model getModelfromPos(final int p0);
    
    VerticalFastScrollIndex getScrollIndex();
    
    Menu getType();
    
    boolean isBindCallsEnabled();
    
    boolean isFirstItemEmpty();
    
    boolean isItemClickable(final int p0, final int p1);
    
    void onBindToView(final VerticalList.Model p0, final View p1, final int p2, final VerticalList.ModelState p3);
    
    void onFastScrollEnd();
    
    void onFastScrollStart();
    
    void onItemSelected(final VerticalList.ItemSelectionState p0);
    
    void onScrollIdle();
    
    void onUnload(final MenuLevel p0);
    
    boolean selectItem(final VerticalList.ItemSelectionState p0);
    
    void setBackSelectionId(final int p0);
    
    void setBackSelectionPos(final int p0);
    
    void setSelectedIcon();
    
    void showToolTip();

    public enum Menu {
        MAIN(0),
        SETTINGS(1),
        PLACES(2),
        RECENT_PLACES(3),
        CONTACTS(4),
        RECENT_CONTACTS(5),
        CONTACT_OPTIONS(6),
        MAIN_OPTIONS(7),
        MUSIC(8),
        SEARCH(9),
        REPORT_ISSUE(10),
        ACTIVE_TRIP(11),
        SYSTEM_INFO(12),
        TEST_FAST_SCROLL_MENU(13),
        NUMBER_PICKER(14),
        MESSAGE_PICKER(15);

        private int value;
        Menu(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }

    public enum MenuLevel {
        BACK_TO_PARENT(0),
        SUB_LEVEL(1),
        CLOSE(2),
        REFRESH_CURRENT(3),
        ROOT(4);

        private int value;
        MenuLevel(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
}
