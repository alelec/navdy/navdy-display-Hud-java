package com.navdy.hud.app.ui.component.mainmenu;

import android.view.View;

class PlacesMenu$3 implements com.navdy.hud.app.ui.component.ChoiceLayout.IListener {
    private final com.navdy.hud.app.ui.component.mainmenu.PlacesMenu this$0;
    final com.navdy.hud.app.framework.destinations.Destination val$destination;
    
    PlacesMenu$3(com.navdy.hud.app.ui.component.mainmenu.PlacesMenu a, com.navdy.hud.app.framework.destinations.Destination a0) {
        super();
        this.this$0 = a;
        this.val$destination = a0;
    }
    
    public void executeItem(int i, int i0) {
        com.navdy.hud.app.ui.component.ConfirmationLayout a = com.navdy.hud.app.ui.component.mainmenu.PlacesMenu.access$000(this.this$0).getConfirmationLayout();
        if (a != null) {
            switch(i) {
                case 1: {
                    a.setVisibility(View.GONE);
                    com.navdy.hud.app.ui.component.mainmenu.PlacesMenu.access$000(this.this$0).reset();
                    com.navdy.hud.app.ui.component.mainmenu.PlacesMenu.access$200(this.this$0).verticalList.unlock();
                    break;
                }
                case 0: {
                    com.navdy.hud.app.ui.component.mainmenu.PlacesMenu.access$100().v("called requestNavigation");
                    com.navdy.hud.app.ui.component.mainmenu.PlacesMenu.access$000(this.this$0).close(new PlacesMenu$3$1(this));
                    break;
                }
            }
        } else {
            com.navdy.hud.app.ui.component.mainmenu.PlacesMenu.access$100().v("confirmation layout not found");
        }
    }
    
    public void itemSelected(int i, int i0) {
    }
}
