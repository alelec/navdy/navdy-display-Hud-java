package com.navdy.hud.app.ui.component;

import android.view.ViewGroup;
import android.animation.TimeInterpolator;
import android.view.ViewGroup;
import android.animation.Animator;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.util.AttributeSet;
import android.content.Context;
import android.view.animation.Interpolator;
import android.animation.ValueAnimator;
import android.animation.Animator;
import android.animation.ValueAnimator;
import android.view.View;

public class ShrinkingBorderView extends View
{
    private static final int FORCE_COLLAPSE_INTERVAL = 300;
    private static final int RESET_TIMEOUT_INITIAL_INTERVAL = 300;
    private static final int RESET_TIMEOUT_INTERVAL = 100;
    private ValueAnimator animator;
    private Animator.AnimatorListener animatorListener;
    private Animator.AnimatorListener animatorResetListener;
    private ValueAnimator.AnimatorUpdateListener animatorResetUpdateListener;
    private ValueAnimator.AnimatorUpdateListener animatorUpdateListener;
    private Runnable forceCompleteCallback;
    private Animator.AnimatorListener forceCompleteListener;
    private Interpolator interpolator;
    private IListener listener;
    private Interpolator restoreInterpolator;
    private int timeoutVal;
    
    public ShrinkingBorderView(final Context context) {
        this(context, null);
    }
    
    public ShrinkingBorderView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public ShrinkingBorderView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.restoreInterpolator = (Interpolator)new LinearInterpolator();
        this.interpolator = (Interpolator)new AccelerateInterpolator();
        this.forceCompleteListener = (Animator.AnimatorListener)new DefaultAnimationListener() {
            @Override
            public void onAnimationEnd(final Animator animator) {
                ShrinkingBorderView.this.animator = null;
                if (ShrinkingBorderView.this.forceCompleteCallback != null) {
                    ShrinkingBorderView.this.forceCompleteCallback.run();
                    ShrinkingBorderView.this.forceCompleteCallback = null;
                }
            }
        };
        this.animatorListener = (Animator.AnimatorListener)new DefaultAnimationListener() {
            @Override
            public void onAnimationEnd(final Animator animator) {
                ShrinkingBorderView.this.animator = null;
                if (ShrinkingBorderView.this.listener != null) {
                    ShrinkingBorderView.this.listener.timeout();
                }
            }
        };
        this.animatorUpdateListener = (ValueAnimator.AnimatorUpdateListener)new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                final ViewGroup.MarginLayoutParams viewGroup$MarginLayoutParams = (ViewGroup.MarginLayoutParams)ShrinkingBorderView.this.getLayoutParams();
                final int intValue = (int)valueAnimator.getAnimatedValue();
                if (viewGroup$MarginLayoutParams.height != intValue) {
                    viewGroup$MarginLayoutParams.height = intValue;
                    ShrinkingBorderView.this.requestLayout();
                }
            }
        };
        this.animatorResetListener = (Animator.AnimatorListener)new DefaultAnimationListener() {
            @Override
            public void onAnimationEnd(final Animator animator) {
                ShrinkingBorderView.this.animator = null;
                ShrinkingBorderView.this.startTimeout(ShrinkingBorderView.this.timeoutVal, true);
            }
        };
        this.animatorResetUpdateListener = (ValueAnimator.AnimatorUpdateListener)new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                ((ViewGroup.MarginLayoutParams)ShrinkingBorderView.this.getLayoutParams()).height = (int)valueAnimator.getAnimatedValue();
                ShrinkingBorderView.this.requestLayout();
            }
        };
    }
    
    private void clearAnimator() {
        if (this.animator != null) {
            this.animator.removeAllListeners();
            this.animator.cancel();
            this.animator = null;
            this.forceCompleteCallback = null;
        }
    }
    
    private void startTimeout(final int timeoutVal, final boolean b) {
        if (timeoutVal != 0 && this.animator == null) {
            this.timeoutVal = timeoutVal;
            if (b) {
                (this.animator = ValueAnimator.ofInt(new int[] { ((ViewGroup.MarginLayoutParams)this.getLayoutParams()).height, 0 })).addUpdateListener(this.animatorUpdateListener);
                this.animator.addListener(this.animatorListener);
                this.animator.setDuration((long)timeoutVal);
                this.animator.setInterpolator((TimeInterpolator)this.interpolator);
                this.animator.start();
            }
            else {
                ((ViewGroup.MarginLayoutParams)this.getLayoutParams()).height = 0;
                this.requestLayout();
                this.resetTimeout(true);
            }
        }
    }
    
    public boolean isVisible() {
        return ((ViewGroup.MarginLayoutParams)this.getLayoutParams()).height != 0;
    }
    
    public void resetTimeout() {
        this.resetTimeout(false);
    }
    
    public void resetTimeout(final boolean b) {
        this.clearAnimator();
        int n;
        if (b) {
            n = 300;
        }
        else {
            n = 100;
        }
        (this.animator = ValueAnimator.ofInt(new int[] { ((ViewGroup.MarginLayoutParams)this.getLayoutParams()).height, ((ViewGroup)this.getParent()).getHeight() })).addUpdateListener(this.animatorResetUpdateListener);
        this.animator.addListener(this.animatorResetListener);
        this.animator.setDuration((long)n);
        this.animator.setInterpolator((TimeInterpolator)this.restoreInterpolator);
        if (b) {
            this.animator.setStartDelay(300L);
        }
        this.animator.start();
    }
    
    public void setListener(final IListener listener) {
        this.listener = listener;
    }
    
    public void startTimeout(final int n) {
        this.startTimeout(n, false);
    }
    
    public void stopTimeout(final boolean b, final Runnable forceCompleteCallback) {
        if (b) {
            this.clearAnimator();
            if (forceCompleteCallback != null) {
                this.forceCompleteCallback = forceCompleteCallback;
                (this.animator = ValueAnimator.ofInt(new int[] { ((ViewGroup.MarginLayoutParams)this.getLayoutParams()).height, 0 })).addUpdateListener(this.animatorResetUpdateListener);
                this.animator.addListener(this.forceCompleteListener);
                this.animator.setDuration(300L);
                this.animator.setInterpolator((TimeInterpolator)this.interpolator);
                this.animator.start();
            }
            else {
                ((ViewGroup.MarginLayoutParams)this.getLayoutParams()).height = 0;
                this.requestLayout();
            }
        }
        else if (this.animator == null) {
            ((ViewGroup.MarginLayoutParams)this.getLayoutParams()).height = 0;
            this.requestLayout();
        }
    }
    
    public interface IListener
    {
        void timeout();
    }
}
