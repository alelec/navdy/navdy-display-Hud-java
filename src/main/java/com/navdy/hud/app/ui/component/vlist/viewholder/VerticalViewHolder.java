package com.navdy.hud.app.ui.component.vlist.viewholder;

import android.animation.TimeInterpolator;

import com.navdy.hud.app.R;
import com.navdy.hud.app.manager.InputManager;
import android.widget.TextView;
import android.widget.ImageView;
import android.view.View;
import android.content.res.Resources;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import android.view.ViewGroup;
import android.animation.AnimatorSet;
import android.view.animation.Interpolator;
import android.os.Handler;
import java.util.HashMap;
import com.navdy.service.library.log.Logger;
import android.support.v7.widget.RecyclerView;

public abstract class VerticalViewHolder extends RecyclerView.ViewHolder
{
    public static final String EMPTY = "";
    public static final int NO_COLOR = 0;
    public static final int iconMargin;
    public static final int listItemHeight;
    public static final int mainIconSize;
    public static final int rootTopOffset;
    static final Logger sLogger;
    public static final int scrollDistance;
    public static final int selectedIconSize;
    public static final int selectedImageX;
    public static final int selectedImageY;
    public static final int selectedTextX;
    public static final int subTitle2Color;
    public static final int subTitleColor;
    public static final float subTitleHeight;
    public static final int textLeftMargin;
    public static final float titleHeight;
    public static final int unselectedIconSize;
    protected State currentState;
    protected HashMap<String, String> extras;
    protected Handler handler;
    protected Interpolator interpolator;
    protected AnimatorSet itemAnimatorSet;
    public ViewGroup layout;
    protected int pos;
    protected VerticalList vlist;
    
    static {
        sLogger = new Logger(VerticalViewHolder.class);
        final Resources resources = HudApplication.getAppContext().getResources();
        selectedIconSize = resources.getDimensionPixelSize(R.dimen.vlist_image);
        unselectedIconSize = resources.getDimensionPixelSize(R.dimen.vlist_small_image);
        selectedImageX = resources.getDimensionPixelSize(R.dimen.vmenu_sel_image_x);
        selectedImageY = resources.getDimensionPixelSize(R.dimen.vmenu_sel_image_y);
        mainIconSize = resources.getDimensionPixelSize(R.dimen.vlist_image_frame);
        textLeftMargin = resources.getDimensionPixelSize(R.dimen.vlist_text_left_margin);
        listItemHeight = resources.getDimensionPixelSize(R.dimen.vlist_item_height);
        titleHeight = resources.getDimension(R.dimen.vlist_title);
        subTitleHeight = resources.getDimension(R.dimen.vlist_subtitle);
        selectedTextX = resources.getDimensionPixelSize(R.dimen.vmenu_sel_text_x);
        rootTopOffset = resources.getDimensionPixelSize(R.dimen.vmenu_root_top_offset);
        iconMargin = resources.getDimensionPixelOffset(R.dimen.vlist_icon_list_margin);
        subTitleColor = resources.getColor(R.color.vlist_subtitle);
        subTitle2Color = resources.getColor(R.color.vlist_subtitle);
        scrollDistance = VerticalViewHolder.listItemHeight;
    }
    
    public VerticalViewHolder(final ViewGroup layout, final VerticalList vlist, final Handler handler) {
        super((View)layout);
        this.pos = -1;
        this.interpolator = VerticalList.getInterpolator();
        this.layout = layout;
        this.vlist = vlist;
        this.handler = handler;
    }
    
    public abstract void bind(final VerticalList.Model p0, final VerticalList.ModelState p1);
    
    public abstract void clearAnimation();
    
    public abstract void copyAndPosition(final ImageView p0, final TextView p1, final TextView p2, final TextView p3, final boolean p4);
    
    public abstract VerticalList.ModelType getModelType();
    
    public int getPos() {
        return this.pos;
    }
    
    public State getState() {
        return this.currentState;
    }
    
    public boolean handleKey(final InputManager.CustomKeyEvent customKeyEvent) {
        return false;
    }
    
    public boolean hasToolTip() {
        return false;
    }
    
    public void preBind(final VerticalList.Model model, final VerticalList.ModelState modelState) {
    }
    
    public abstract void select(final VerticalList.Model p0, final int p1, final int p2);
    
    public void setExtras(final HashMap<String, String> extras) {
        this.extras = extras;
    }
    
    public abstract void setItemState(final State p0, final AnimationType p1, final int p2, final boolean p3);
    
    public void setPos(final int pos) {
        this.pos = pos;
        this.currentState = null;
    }
    
    public final void setState(final State state, final AnimationType animationType, final int n) {
        this.setState(state, animationType, n, true);
    }
    
    public final void setState(final State currentState, final AnimationType animationType, final int n, final boolean b) {
        switch (this.getModelType()) {
            default:
                if (VerticalViewHolder.sLogger.isLoggable(2)) {
                    VerticalViewHolder.sLogger.v("setState {" + this.pos + ", " + currentState + "} animate=" + animationType + " dur=" + n + " current-raw {" + this.vlist.getRawPosition() + "} vh-id{" + System.identityHashCode(this) + "}");
                }
                if (this.currentState == currentState) {
                    if (currentState == State.SELECTED) {
                        this.vlist.selectedList.add(this.pos);
                        return;
                    }
                    this.vlist.selectedList.remove(this.pos);
                    return;
                }
                else {
                    this.clearAnimation();
                    if ((this.currentState = currentState) == State.SELECTED) {
                        this.vlist.selectedList.add(this.pos);
                    }
                    else {
                        this.vlist.selectedList.remove(this.pos);
                    }
                    this.setItemState(currentState, animationType, n, b);
                    if (this.itemAnimatorSet != null) {
                        this.itemAnimatorSet.setDuration((long)n);
                        this.itemAnimatorSet.setInterpolator((TimeInterpolator)this.interpolator);
                        this.itemAnimatorSet.start();
                    }
                    return;
                }
//                break;
            case BLANK:
        }
    }
    
    public void startFluctuator() {
    }
    
    public void stopAnimation() {
        if (this.itemAnimatorSet != null && this.itemAnimatorSet.isRunning()) {
            this.itemAnimatorSet.removeAllListeners();
            this.itemAnimatorSet.cancel();
        }
        this.itemAnimatorSet = null;
    }

    public enum AnimationType {
        NONE(0),
        MOVE(1),
        INIT(2);

        private int value;
        AnimationType(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }

    public enum State {
        SELECTED(0),
        UNSELECTED(1);

        private int value;
        State(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
}
