package com.navdy.hud.app.framework.contacts;

public enum NumberType {
    HOME(1),
    MOBILE(2),
    WORK(3),
    WORK_MOBILE(4),
    OTHER(5);

    int value;

    private NumberType(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }

    public static NumberType buildFromValue(int n) {
        switch (n) {
            case 1:
                return HOME;
            case 2:
                return MOBILE;
            case 3:
                return WORK;
            case 4:
                return WORK_MOBILE;
            default:
                return OTHER;
        }
    }
}