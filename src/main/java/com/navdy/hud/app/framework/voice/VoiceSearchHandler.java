package com.navdy.hud.app.framework.voice;

import com.navdy.hud.app.R;
import com.navdy.hud.app.manager.MusicManager;
import com.squareup.wire.Message;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.service.library.events.audio.VoiceSearchRequest;
import com.navdy.hud.app.util.RemoteCapabilitiesUtil;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import java.util.List;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.framework.notifications.INotification;
import com.navdy.hud.app.ui.component.destination.IDestinationPicker;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import android.os.Parcelable;
import com.navdy.hud.app.maps.util.DestinationUtil;
import com.navdy.service.library.events.places.PlaceType;
import com.navdy.hud.app.ui.component.destination.DestinationParcelable;
import android.text.TextUtils;
import android.os.Bundle;
import com.squareup.otto.Subscribe;
import com.navdy.hud.app.event.DriverProfileChanged;
import android.support.v4.content.ContextCompat;
import com.navdy.hud.app.ui.component.destination.DestinationPickerScreen;
import com.navdy.hud.app.framework.destinations.DestinationsManager;
import com.navdy.service.library.events.destination.Destination;
import android.os.Looper;
import com.navdy.hud.app.util.FeatureUtil;
import com.navdy.hud.app.HudApplication;
import com.navdy.service.library.events.audio.VoiceSearchResponse;
import android.os.Handler;
import android.content.Context;
import com.squareup.otto.Bus;

public class VoiceSearchHandler
{
    private static final String searchAgainSubTitle;
    private Bus bus;
    private Context context;
    private Handler handler;
    private VoiceSearchResponse response;
    private boolean showVoiceSearchTipsToUser;
    private VoiceSearchUserInterface voiceSearchUserInterface;
    
    static {
        searchAgainSubTitle = HudApplication.getAppContext().getResources().getString(R.string.retry_search);
    }
    
    public VoiceSearchHandler(final Context context, final Bus bus, final FeatureUtil featureUtil) {
        this.showVoiceSearchTipsToUser = true;
        this.handler = new Handler(Looper.getMainLooper());
        this.bus = bus;
        this.context = context;
        this.bus.register(this);
    }
    
    public void go() {
        if (this.response != null && this.response.state == VoiceSearchResponse.VoiceSearchState.VOICE_SEARCH_SUCCESS && this.response.results != null && this.response.results.size() > 0) {
            final Destination destination = this.response.results.get(0);
            final com.navdy.hud.app.framework.destinations.Destination transformToInternalDestination = DestinationsManager.getInstance().transformToInternalDestination(destination);
            int iconRes = 0;
            final DestinationPickerScreen.PlaceTypeResourceHolder placeTypeHolder = DestinationPickerScreen.getPlaceTypeHolder(destination.place_type);
            if (placeTypeHolder != null) {
                iconRes = placeTypeHolder.iconRes;
            }
            com.navdy.hud.app.framework.destinations.Destination build = transformToInternalDestination;
            if (transformToInternalDestination.destinationIcon == 0) {
                build = transformToInternalDestination;
                if (iconRes != 0) {
                    build = new com.navdy.hud.app.framework.destinations.Destination$Builder(transformToInternalDestination).destinationIcon(iconRes).destinationIconBkColor(ContextCompat.getColor(this.context, R.color.icon_bk_color_unselected)).build();
                }
            }
            DestinationsManager.getInstance().requestNavigationWithNavLookup(build);
        }
    }
    
    @Subscribe
    public void onDriverProfileChanged(final DriverProfileChanged driverProfileChanged) {
        this.showVoiceSearchTipsToUser = true;
    }
    
    @Subscribe
    public void onVoiceSearchResponse(final VoiceSearchResponse response) {
        if (this.voiceSearchUserInterface != null) {
            this.response = response;
            this.voiceSearchUserInterface.onVoiceSearchResponse(response);
        }
    }
    
    public void setVoiceSearchUserInterface(final VoiceSearchUserInterface voiceSearchUserInterface) {
        this.voiceSearchUserInterface = voiceSearchUserInterface;
    }
    
    public boolean shouldShowVoiceSearchTipsToUser() {
        return this.showVoiceSearchTipsToUser;
    }
    
    public void showResults() {
        if (this.response != null && this.response.results != null) {
            final List<Destination> results = this.response.results;
            final Bundle bundle = new Bundle();
            bundle.putBoolean("PICKER_SHOW_DESTINATION_MAP", true);
            bundle.putInt("PICKER_DESTINATION_ICON", -1);
            bundle.putInt("PICKER_INITIAL_SELECTION", 1);
            final int color = ContextCompat.getColor(this.context, R.color.route_sel);
            final int color2 = ContextCompat.getColor(this.context, R.color.icon_bk_color_unselected);
            String searchAgainSubTitle = null;
            String s;
            if (!TextUtils.isEmpty((CharSequence)this.response.recognizedWords)) {
                s = "\"" + this.response.recognizedWords + "\"";
                searchAgainSubTitle = VoiceSearchHandler.searchAgainSubTitle;
            }
            else {
                s = VoiceSearchHandler.searchAgainSubTitle;
            }
            final DestinationParcelable destinationParcelable = new DestinationParcelable(R.id.search_again, s, searchAgainSubTitle, false, null, true, null, 0.0, 0.0, 0.0, 0.0, R.drawable.icon_mm_voice_search_2, 0, color, color2, DestinationParcelable.DestinationType.NONE, null);
            final List<DestinationParcelable> convert = DestinationUtil.convert(this.context, results, color, color2, true);
            convert.add(0, destinationParcelable);
            convert.add(new DestinationParcelable(R.id.retry, VoiceSearchHandler.searchAgainSubTitle, null, false, null, false, null, 0.0, 0.0, 0.0, 0.0, R.drawable.icon_mm_voice_search_2, 0, color, color2, DestinationParcelable.DestinationType.NONE, PlaceType.PLACE_TYPE_UNKNOWN));
            final DestinationParcelable[] array = new DestinationParcelable[convert.size()];
            convert.<DestinationParcelable>toArray(array);
            bundle.putParcelableArray("PICKER_DESTINATIONS", (Parcelable[])array);
            NotificationManager.getInstance().removeNotification("navdy#voicesearch#notif", true, Screen.SCREEN_DESTINATION_PICKER, bundle, new IDestinationPicker() {
                boolean retrySelected = false;
                
                @Override
                public void onDestinationPickerClosed() {
                    if (this.retrySelected) {
                        final NotificationManager instance = NotificationManager.getInstance();
                        VoiceSearchNotification voiceSearchNotification;
                        if ((voiceSearchNotification = (VoiceSearchNotification)instance.getNotification("navdy#voicesearch#notif")) == null) {
                            voiceSearchNotification = new VoiceSearchNotification();
                        }
                        instance.addNotification(voiceSearchNotification);
                    }
                }
                
                @Override
                public boolean onItemClicked(final int n, final int n2, final DestinationPickerScreen.DestinationPickerState destinationPickerState) {
                    boolean b = true;
                    switch (n) {
                        default:
                            AnalyticsSupport.recordVoiceSearchListItemSelection(n2);
                            b = false;
                            break;
                        case R.id.retry:
                        case R.id.search_again:
                            this.retrySelected = true;
                            if (n == R.id.retry) {
                                AnalyticsSupport.recordVoiceSearchListItemSelection(-1);
                                break;
                            }
                            AnalyticsSupport.recordVoiceSearchListItemSelection(-2);
                            break;
                    }
                    return b;
                }
                
                @Override
                public boolean onItemSelected(final int n, final int n2, final DestinationPickerScreen.DestinationPickerState destinationPickerState) {
                    return false;
                }
            });
        }
    }
    
    public void showedVoiceSearchTipsToUser() {
        this.showVoiceSearchTipsToUser = false;
    }
    
    public void startVoiceSearch() {
        final DeviceInfo.Platform remoteDevicePlatform = RemoteDeviceManager.getInstance().getRemoteDevicePlatform();
        if (remoteDevicePlatform != null) {
            if (remoteDevicePlatform == DeviceInfo.Platform.PLATFORM_iOS && RemoteCapabilitiesUtil.supportsVoiceSearchNewIOSPauseBehaviors()) {
                final MusicManager musicManager = RemoteDeviceManager.getInstance().getMusicManager();
                if (musicManager != null) {
                    musicManager.softPause();
                }
                this.handler.postDelayed((Runnable)new Runnable() {
                    @Override
                    public void run() {
                        VoiceSearchHandler.this.bus.post(new RemoteEvent(new VoiceSearchRequest(Boolean.valueOf(false))));
                    }
                }, 250L);
            }
            else {
                this.bus.post(new RemoteEvent(new VoiceSearchRequest(Boolean.valueOf(false))));
            }
        }
    }
    
    public void stopVoiceSearch() {
        if (this.response != null && this.response.state != VoiceSearchResponse.VoiceSearchState.VOICE_SEARCH_SUCCESS) {
            this.bus.post(new RemoteEvent(new VoiceSearchRequest(Boolean.valueOf(true))));
            final MusicManager musicManager = RemoteDeviceManager.getInstance().getMusicManager();
            if (musicManager != null) {
                musicManager.acceptResumes();
            }
        }
    }
    
    public interface VoiceSearchUserInterface
    {
        void close();
        
        void onVoiceSearchResponse(final VoiceSearchResponse p0);
    }
}
