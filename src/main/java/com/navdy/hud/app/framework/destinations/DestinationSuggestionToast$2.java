package com.navdy.hud.app.framework.destinations;

class DestinationSuggestionToast$2 {
    final static int[] $SwitchMap$com$navdy$service$library$events$destination$Destination$FavoriteType;
    final static int[] $SwitchMap$com$navdy$service$library$events$places$SuggestedDestination$SuggestionType;
    
    static {
        $SwitchMap$com$navdy$service$library$events$destination$Destination$FavoriteType = new int[com.navdy.service.library.events.destination.Destination.FavoriteType.values().length];
        int[] a = $SwitchMap$com$navdy$service$library$events$destination$Destination$FavoriteType;
        com.navdy.service.library.events.destination.Destination.FavoriteType a0 = com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_HOME;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$destination$Destination$FavoriteType[com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_WORK.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$destination$Destination$FavoriteType[com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_CUSTOM.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$destination$Destination$FavoriteType[com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_NONE.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException2) {
        }
        $SwitchMap$com$navdy$service$library$events$places$SuggestedDestination$SuggestionType = new int[com.navdy.service.library.events.places.SuggestedDestination.SuggestionType.values().length];
        int[] a1 = $SwitchMap$com$navdy$service$library$events$places$SuggestedDestination$SuggestionType;
        com.navdy.service.library.events.places.SuggestedDestination.SuggestionType a2 = com.navdy.service.library.events.places.SuggestedDestination.SuggestionType.SUGGESTION_CALENDAR;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException3) {
        }
    }
}
