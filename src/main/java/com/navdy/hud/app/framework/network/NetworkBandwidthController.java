package com.navdy.hud.app.framework.network;

import com.squareup.otto.Subscribe;

public class NetworkBandwidthController {
    final private static int ACTIVE;
    final private static com.navdy.hud.app.framework.network.NetworkBandwidthController$UserBandwidthSettingChanged BW_SETTING_CHANGED;

    final private static String PERM_DISABLE_NETWORK = "persist.sys.perm_disable_nw";

    final private static int DATA_COLLECTION_INITIAL_INTERVAL;
    final private static int DATA_COLLECTION_INTERVAL;
    final private static int DATA_COLLECTION_INTERVAL_ENG;
    final private static boolean networkDisabled;
    final private static com.navdy.service.library.log.Logger sLogger;
    final private static com.navdy.hud.app.framework.network.NetworkBandwidthController singleton;
    private com.squareup.otto.Bus bus;
    final private java.util.HashMap componentInfoMap;
    private Runnable dataCollectionRunnable;
    private Runnable dataCollectionRunnableBk;
    private com.navdy.hud.app.framework.network.DnsCache dnsCache;
    private android.os.Handler handler;
    private boolean limitBandwidthModeOn;
    private Runnable networkStatRunnable;
    private Thread networkStatThread;
    private com.navdy.hud.app.framework.network.NetworkStatCache statCache;
    private boolean trafficDataDownloadedOnce;

   final private java.util.HashMap urlToComponentMap;

    static {
        sLogger = new com.navdy.service.library.log.Logger("NetworkBandwidthControl");
        ACTIVE = (int)java.util.concurrent.TimeUnit.SECONDS.toMillis(15L);
        BW_SETTING_CHANGED = new com.navdy.hud.app.framework.network.NetworkBandwidthController$UserBandwidthSettingChanged();
        networkDisabled = com.navdy.hud.app.util.os.SystemProperties.getBoolean(PERM_DISABLE_NETWORK, false);
        if (networkDisabled) {
            sLogger.v("n/w disabled permanently");
        }
        DATA_COLLECTION_INITIAL_INTERVAL = (int)java.util.concurrent.TimeUnit.SECONDS.toMillis(25L);
        DATA_COLLECTION_INTERVAL_ENG = (int)java.util.concurrent.TimeUnit.MINUTES.toMillis(1L);
        DATA_COLLECTION_INTERVAL = (int)java.util.concurrent.TimeUnit.MINUTES.toMillis(5L);
        singleton = new com.navdy.hud.app.framework.network.NetworkBandwidthController();
    }

    private NetworkBandwidthController() {
        this.componentInfoMap = new java.util.HashMap<>();
       this.urlToComponentMap = new java.util.HashMap<>();
        this.dnsCache = new com.navdy.hud.app.framework.network.DnsCache();
        this.statCache = new com.navdy.hud.app.framework.network.NetworkStatCache(this.dnsCache);
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());

        this.networkStatRunnable = new NetworkBandwidthController$1(this);
        this.dataCollectionRunnable = new NetworkBandwidthController$2(this);
        this.dataCollectionRunnableBk = new NetworkBandwidthController$3(this);

        this.bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();

        this.urlToComponentMap.put("v102-62-30-8.route.hybrid.api.here.com", com.navdy.hud.app.framework.network.NetworkBandwidthController$Component.HERE_ROUTE);
        this.urlToComponentMap.put("tpeg.traffic.api.here.com", com.navdy.hud.app.framework.network.NetworkBandwidthController$Component.HERE_TRAFFIC);
        this.urlToComponentMap.put("tpeg.hybrid.api.here.com", com.navdy.hud.app.framework.network.NetworkBandwidthController$Component.HERE_TRAFFIC);
        this.urlToComponentMap.put("download.hybrid.api.here.com", com.navdy.hud.app.framework.network.NetworkBandwidthController$Component.HERE_MAP_DOWNLOAD);
        this.urlToComponentMap.put("static.bundles.hybrid.api.here.com", com.navdy.hud.app.framework.network.NetworkBandwidthController$Component.HERE_MAP_DOWNLOAD);
        this.urlToComponentMap.put("reverse.geocoder.api.here.com", com.navdy.hud.app.framework.network.NetworkBandwidthController$Component.HERE_REVERSE_GEO);
//        this.urlToComponentMap.put("analytics.localytics.com", com.navdy.hud.app.framework.network.NetworkBandwidthController$Component.LOCALYTICS);
//        this.urlToComponentMap.put("profile.localytics.com", com.navdy.hud.app.framework.network.NetworkBandwidthController$Component.LOCALYTICS);
        this.urlToComponentMap.put("sdk.hockeyapp.net", com.navdy.hud.app.framework.network.NetworkBandwidthController$Component.HOCKEY);
//        this.urlToComponentMap.put("navdyhud.atlassian.net", com.navdy.hud.app.framework.network.NetworkBandwidthController$Component.JIRA);
//        this.componentInfoMap.put(com.navdy.hud.app.framework.network.NetworkBandwidthController$Component.LOCALYTICS, new com.navdy.hud.app.framework.network.NetworkBandwidthController$ComponentInfo(null));
        this.componentInfoMap.put(com.navdy.hud.app.framework.network.NetworkBandwidthController$Component.HERE_ROUTE, new com.navdy.hud.app.framework.network.NetworkBandwidthController$ComponentInfo(null));
        this.componentInfoMap.put(com.navdy.hud.app.framework.network.NetworkBandwidthController$Component.HERE_TRAFFIC, new com.navdy.hud.app.framework.network.NetworkBandwidthController$ComponentInfo(null));
        this.componentInfoMap.put(com.navdy.hud.app.framework.network.NetworkBandwidthController$Component.HERE_MAP_DOWNLOAD, new com.navdy.hud.app.framework.network.NetworkBandwidthController$ComponentInfo(null));
        this.componentInfoMap.put(com.navdy.hud.app.framework.network.NetworkBandwidthController$Component.HERE_REVERSE_GEO, new com.navdy.hud.app.framework.network.NetworkBandwidthController$ComponentInfo(null));
        this.componentInfoMap.put(com.navdy.hud.app.framework.network.NetworkBandwidthController$Component.HOCKEY, new com.navdy.hud.app.framework.network.NetworkBandwidthController$ComponentInfo(null));
//        this.componentInfoMap.put(com.navdy.hud.app.framework.network.NetworkBandwidthController$Component.JIRA, new com.navdy.hud.app.framework.network.NetworkBandwidthController$ComponentInfo(null));
        this.networkStatThread = new Thread(this.networkStatRunnable);
        this.networkStatThread.setName("hudNetStatThread");
        this.networkStatThread.start();
        sLogger.v("networkStat thread started");
        this.handler.postDelayed(this.dataCollectionRunnable, (long)DATA_COLLECTION_INITIAL_INTERVAL);
        this.limitBandwidthModeOn = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile().isLimitBandwidthModeOn();
        this.bus.register(this);
        sLogger.v("registered bus");
    }

    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }

    static com.navdy.hud.app.framework.network.DnsCache access$100(com.navdy.hud.app.framework.network.NetworkBandwidthController a) {
        return a.dnsCache;
    }

    static com.navdy.hud.app.framework.network.NetworkStatCache access$200(com.navdy.hud.app.framework.network.NetworkBandwidthController a) {
        return a.statCache;
    }

    static java.util.HashMap access$300(com.navdy.hud.app.framework.network.NetworkBandwidthController a) {
        return a.urlToComponentMap;
    }

    static boolean access$400(com.navdy.hud.app.framework.network.NetworkBandwidthController a) {
        return a.trafficDataDownloadedOnce;
    }

    static void access$402(NetworkBandwidthController a) {
        a.trafficDataDownloadedOnce = true;
    }

    static java.util.HashMap access$500(com.navdy.hud.app.framework.network.NetworkBandwidthController a) {
        return a.componentInfoMap;
    }

    static Runnable access$600(com.navdy.hud.app.framework.network.NetworkBandwidthController a) {
        return a.dataCollectionRunnableBk;
    }

    static int access$700() {
        return DATA_COLLECTION_INTERVAL;
    }

    static int access$800() {
        return DATA_COLLECTION_INTERVAL_ENG;
    }

    static android.os.Handler access$900(com.navdy.hud.app.framework.network.NetworkBandwidthController a) {
        return a.handler;
    }

    public static com.navdy.hud.app.framework.network.NetworkBandwidthController getInstance() {
        return singleton;
    }

    private void handleBandwidthPreferenceChange() {
        boolean b = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile().isLimitBandwidthModeOn();
        if (b != this.limitBandwidthModeOn) {
            sLogger.v("limitbandwidth: changed current[" + this.limitBandwidthModeOn + "] new [" + b + "]");
            this.limitBandwidthModeOn = b;
            if (b) {
                sLogger.v("limitbandwidth: on");
            } else {
                sLogger.v("limitbandwidth: off");
            }
            com.navdy.hud.app.maps.here.HereMapsManager.getInstance().handleBandwidthPreferenceChange();
            this.bus.post(BW_SETTING_CHANGED);
        } else {
            sLogger.v("limitbandwidth: no-op current[" + this.limitBandwidthModeOn + "] new [" + b + "]");
        }
    }

    private boolean isComponentActive(com.navdy.hud.app.framework.network.NetworkBandwidthController$Component a) {
        boolean b;
        long j = this.getLastActivityTime(a);
        int i = Long.compare(j, 0L);
        label2: {
            long j0;
            label0: {
                label1: {
                    if (i <= 0) {
                        break label1;
                    }
                    j0 = android.os.SystemClock.elapsedRealtime() - j;
                    if (j0 <= (long)ACTIVE) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            sLogger.v("component is ACTIVE:" + a + " diff:" + j0);
            b = true;
        }
        return b;
    }

    public java.util.List getBootStat() {
        return this.statCache.getBootStat();
    }

    private long getLastActivityTime(com.navdy.hud.app.framework.network.NetworkBandwidthController$Component a) {
        {
            long j;
            if (this.componentInfoMap.get(a) != null) {
                synchronized ((com.navdy.hud.app.framework.network.NetworkBandwidthController$ComponentInfo)this.componentInfoMap.get(a)) {
                    com.navdy.hud.app.framework.network.NetworkBandwidthController$ComponentInfo a1 = (com.navdy.hud.app.framework.network.NetworkBandwidthController$ComponentInfo)this.componentInfoMap.get(a);
                    j = a1.lastActivity;
                    /*monexit(a1)*/
                }
            } else {
                j = 0L;
            }
            return j;
        }
//        while(true) {
//            Throwable a3 = null;
//            try {
//                /*monexit(a1)*/;
//                a3 = a0;
//            } catch(IllegalMonitorStateException | NullPointerException a4) {
//                a0 = a4;
//                continue;
//            }
//            throw a3;
//        }
    }

    public java.util.List getSessionStat() {
        return this.statCache.getSessionStat();
    }

    public boolean isLimitBandwidthModeOn() {
        return this.limitBandwidthModeOn;
    }

    public boolean isNetworkAccessAllowed(com.navdy.hud.app.framework.network.NetworkBandwidthController$Component a) {
        boolean b;
        if (com.navdy.hud.app.framework.network.NetworkStateManager.isConnectedToNetwork(com.navdy.hud.app.HudApplication.getAppContext())) {
            switch(com.navdy.hud.app.framework.network.NetworkBandwidthController$4.$SwitchMap$com$navdy$hud$app$framework$network$NetworkBandwidthController$Component[a.ordinal()]) {
                case 1: case 2: case 3: {
                    if (this.isComponentActive(com.navdy.hud.app.framework.network.NetworkBandwidthController$Component.HERE_ROUTE)) {
                        sLogger.v("n/w access not allowed:" + a);
                        b = false;
                        break;
                    } else if (this.isComponentActive(com.navdy.hud.app.framework.network.NetworkBandwidthController$Component.HERE_TRAFFIC)) {
                        sLogger.v("n/w access not allowed:" + a);
                        b = false;
                        break;
                    } else {
                        sLogger.v("n/w access allowed:" + a);
                        b = true;
                        break;
                    }
                }
                default: {
                    sLogger.v("n/w access allowed:" + a);
                    b = true;
                }
            }
        } else {
            sLogger.v("n/w access: not connected to network:" + a);
            b = false;
        }
        return b;
    }

    public boolean isNetworkDisabled() {
        return networkDisabled;
    }

    public boolean isTrafficDataDownloadedOnce() {
        return this.trafficDataDownloadedOnce;
    }

    public void netStat() {
        this.handler.removeCallbacks(this.dataCollectionRunnable);
        this.dataCollectionRunnable.run();
    }

    @Subscribe
    public void onDriverProfileChanged(com.navdy.hud.app.event.DriverProfileChanged a) {
        sLogger.v("driver profile changed");
        this.handleBandwidthPreferenceChange();
    }

    @Subscribe
    public void onDriverProfileUpdated(com.navdy.hud.app.event.DriverProfileUpdated a) {
        sLogger.v("driver profile updated");
        if (a.state == com.navdy.hud.app.event.DriverProfileUpdated.State.UPDATED) {
            this.handleBandwidthPreferenceChange();
        }
    }
}


