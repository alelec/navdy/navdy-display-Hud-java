package com.navdy.service.library.util;

public class ScalingUtilities {
    public ScalingUtilities() {
    }
    
    public static android.graphics.Rect calculateDstRect(int i, int i0, int i1, int i2, com.navdy.service.library.util.ScalingUtilities$ScalingLogic a) {
        android.graphics.Rect a0 = null;
        if (a != com.navdy.service.library.util.ScalingUtilities$ScalingLogic.FIT) {
            a0 = new android.graphics.Rect(0, 0, i1, i2);
        } else {
            float f = (float)i / (float)i0;
            a0 = (f > (float)i1 / (float)i2) ? new android.graphics.Rect(0, 0, i1, (int)((float)i1 / f)) : new android.graphics.Rect(0, 0, (int)((float)i2 * f), i2);
        }
        return a0;
    }
    
    public static int calculateSampleSize(int i, int i0, int i1, int i2, com.navdy.service.library.util.ScalingUtilities$ScalingLogic a) {
        int i3 = 0;
        if (a != com.navdy.service.library.util.ScalingUtilities$ScalingLogic.FIT) {
            i3 = ((float)i / (float)i0 > (float)i1 / (float)i2) ? i0 / i2 : i / i1;
        } else {
            i3 = ((float)i / (float)i0 > (float)i1 / (float)i2) ? i / i1 : i0 / i2;
        }
        return i3;
    }
    
    public static android.graphics.Rect calculateSrcRect(int i, int i0, int i1, int i2, com.navdy.service.library.util.ScalingUtilities$ScalingLogic a) {
        android.graphics.Rect a0 = null;
        if (a != com.navdy.service.library.util.ScalingUtilities$ScalingLogic.CROP) {
            a0 = new android.graphics.Rect(0, 0, i, i0);
        } else {
            float f = (float)i / (float)i0;
            float f0 = (float)i1 / (float)i2;
            if (f > f0) {
                int i3 = (int)((float)i0 * f0);
                int i4 = (i - i3) / 2;
                a0 = new android.graphics.Rect(i4, 0, i4 + i3, i0);
            } else {
                int i5 = (int)((float)i / f0);
                int i6 = (i0 - i5) / 2;
                a0 = new android.graphics.Rect(0, i6, i, i6 + i5);
            }
        }
        return a0;
    }
    
    public static android.graphics.Bitmap createScaledBitmap(android.graphics.Bitmap a, int i, int i0, com.navdy.service.library.util.ScalingUtilities$ScalingLogic a0) {
        int i1 = a.getHeight();
        label0: {
            label1: {
                if (i1 != i0) {
                    break label1;
                }
                if (a.getWidth() == i) {
                    break label0;
                }
            }
            android.graphics.Rect a1 = com.navdy.service.library.util.ScalingUtilities.calculateSrcRect(a.getWidth(), a.getHeight(), i, i0, a0);
            android.graphics.Rect a2 = com.navdy.service.library.util.ScalingUtilities.calculateDstRect(a.getWidth(), a.getHeight(), i, i0, a0);
            android.graphics.Bitmap a3 = android.graphics.Bitmap.createBitmap(a2.width(), a2.height(), android.graphics.Bitmap.Config.ARGB_8888);
            new android.graphics.Canvas(a3).drawBitmap(a, a1, a2, new android.graphics.Paint(2));
            a = a3;
        }
        return a;
    }
    
    public static android.graphics.Bitmap createScaledBitmapAndRecycleOriginalIfScaled(android.graphics.Bitmap a, int i, int i0, com.navdy.service.library.util.ScalingUtilities$ScalingLogic a0) {
        int i1 = a.getHeight();
        label0: {
            label1: {
                if (i1 != i0) {
                    break label1;
                }
                if (a.getWidth() == i) {
                    break label0;
                }
            }
            android.graphics.Rect a1 = com.navdy.service.library.util.ScalingUtilities.calculateSrcRect(a.getWidth(), a.getHeight(), i, i0, a0);
            android.graphics.Rect a2 = com.navdy.service.library.util.ScalingUtilities.calculateDstRect(a.getWidth(), a.getHeight(), i, i0, a0);
            android.graphics.Bitmap a3 = android.graphics.Bitmap.createBitmap(a2.width(), a2.height(), android.graphics.Bitmap.Config.ARGB_8888);
            new android.graphics.Canvas(a3).drawBitmap(a, a1, a2, new android.graphics.Paint(2));
            a.recycle();
            a = a3;
        }
        return a;
    }
    
    public static android.graphics.Bitmap decodeByteArray(byte[] a, int i, int i0, com.navdy.service.library.util.ScalingUtilities$ScalingLogic a0) {
        android.graphics.BitmapFactory.Options a1 = new android.graphics.BitmapFactory.Options();
        a1.inJustDecodeBounds = true;
        android.graphics.BitmapFactory.decodeByteArray(a, 0, a.length, a1);
        int i1 = a1.outWidth;
        label1: {
            label0: {
                if (i1 != i) {
                    break label0;
                }
                if (a1.outHeight != i0) {
                    break label0;
                }
                a1 = null;
                break label1;
            }
            a1.inJustDecodeBounds = false;
            a1.inSampleSize = com.navdy.service.library.util.ScalingUtilities.calculateSampleSize(a1.outWidth, a1.outHeight, i, i0, a0);
        }
        return android.graphics.BitmapFactory.decodeByteArray(a, 0, a.length, a1);
    }
    
    public static android.graphics.Bitmap decodeResource(android.content.res.Resources a, int i, int i0, int i1, com.navdy.service.library.util.ScalingUtilities$ScalingLogic a0) {
        android.graphics.BitmapFactory.Options a1 = new android.graphics.BitmapFactory.Options();
        a1.inJustDecodeBounds = true;
        android.graphics.BitmapFactory.decodeResource(a, i, a1);
        a1.inJustDecodeBounds = false;
        a1.inSampleSize = com.navdy.service.library.util.ScalingUtilities.calculateSampleSize(a1.outWidth, a1.outHeight, i0, i1, a0);
        return android.graphics.BitmapFactory.decodeResource(a, i, a1);
    }
    
    public static byte[] encodeByteArray(android.graphics.Bitmap a) {
        java.io.ByteArrayOutputStream a0 = new java.io.ByteArrayOutputStream();
        a.compress(android.graphics.Bitmap.CompressFormat.PNG, 100, (java.io.OutputStream)a0);
        return a0.toByteArray();
    }
}
