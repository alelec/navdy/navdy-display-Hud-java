package com.navdy.service.library.util;

import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.Listenable.Listener;
import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.Iterator;

public abstract class Listenable<T extends Listener> {
    private final Object listenerLock = new Object();
    private final Logger logger = new Logger(getClass());
    protected HashSet<WeakReference<T>> mListeners = new HashSet<>();

    public interface EventDispatcher<U extends Listenable, T extends Listener> {
        void dispatchEvent(U u, T t);
    }

    public interface Listener {
    }

    public boolean addListener(T t) {
        if (t == null) {
            this.logger.e("attempted to add null listener");
            return false;
        }
        synchronized (this.listenerLock) {
            Iterator it = this.mListeners.iterator();
            while (it.hasNext()) {
                Listener listener = (Listener) ((WeakReference) it.next()).get();
                if (listener == null) {
                    it.remove();
                } else if (listener.equals(t)) {
                    return false;
                }
            }
            this.mListeners.add(new WeakReference<>(t));
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public void dispatchToListeners(EventDispatcher eventDispatcher) {
        HashSet hashSet;
        synchronized (this.listenerLock) {
            hashSet = (HashSet) this.mListeners.clone();
        }
        Iterator it = hashSet.iterator();
        while (it.hasNext()) {
            Listener listener = (Listener) ((WeakReference) it.next()).get();
            if (listener == null) {
                it.remove();
            } else {
                eventDispatcher.dispatchEvent(this, listener);
            }
        }
    }

    public boolean removeListener(T t) {
        if (t == null) {
            this.logger.e("attempted to remove null listener");
            return false;
        }
        synchronized (this.listenerLock) {
            Iterator it = this.mListeners.iterator();
            while (it.hasNext()) {
                Listener listener = (Listener) ((WeakReference) it.next()).get();
                if (listener == null) {
                    it.remove();
                } else if (listener.equals(t)) {
                    it.remove();
                    return true;
                }
            }
            return false;
        }
    }
}