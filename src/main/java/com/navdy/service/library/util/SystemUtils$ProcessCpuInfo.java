package com.navdy.service.library.util;

public class SystemUtils$ProcessCpuInfo {
    private int cpu;
    private String name;
    private int pid;
    private String thread;
    private int tid;
    
    public SystemUtils$ProcessCpuInfo(int i, int i0, String s, String s0, int i1) {
        this.pid = i;
        this.tid = i0;
        this.name = s;
        this.thread = s0;
        this.cpu = i1;
    }
    
    public int getCpu() {
        return this.cpu;
    }
    
    public int getPid() {
        return this.pid;
    }
    
    public String getProcessName() {
        return this.name;
    }
    
    public String getThreadName() {
        return this.thread;
    }
    
    public int getTid() {
        return this.tid;
    }
}
