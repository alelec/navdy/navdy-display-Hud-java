package com.navdy.service.library.log;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class FastPrintWriter extends java.io.PrintWriter {
    private static java.io.Writer sDummyWriter;
    final private boolean mAutoFlush;
    final private int mBufferLen;
    final private java.nio.ByteBuffer mBytes;
    private java.nio.charset.CharsetEncoder mCharset;
    private boolean mIoError;
    final private java.io.OutputStream mOutputStream;
    private int mPos;
    final private android.util.Printer mPrinter;
    final private String mSeparator;
    final private char[] mText;
    final private java.io.Writer mWriter;
    
    static {
        sDummyWriter = new com.navdy.service.library.log.FastPrintWriter$1();
    }
    
    public FastPrintWriter(android.util.Printer a) {
        this(a, 512);
    }
    
    public FastPrintWriter(android.util.Printer a, int i) {
        super(sDummyWriter, true);
        if (a == null) {
            throw new NullPointerException("pr is null");
        }
        this.mBufferLen = i;
        this.mText = new char[i];
        this.mBytes = null;
        this.mOutputStream = null;
        this.mWriter = null;
        this.mPrinter = a;
        this.mAutoFlush = true;
        if (android.os.Build.VERSION.SDK_INT < 19) {
            this.mSeparator = "\n";
        } else {
            this.mSeparator = System.lineSeparator();
        }
        this.initDefaultEncoder();
    }
    
    public FastPrintWriter(java.io.OutputStream a) {
        this(a, false, 8192);
    }
    
    public FastPrintWriter(java.io.OutputStream a, boolean b) {
        this(a, b, 8192);
    }
    
    public FastPrintWriter(java.io.OutputStream a, boolean b, int i) {
        super(sDummyWriter, b);
        if (a == null) {
            throw new NullPointerException("out is null");
        }
        this.mBufferLen = i;
        this.mText = new char[i];
        this.mBytes = java.nio.ByteBuffer.allocate(this.mBufferLen);
        this.mOutputStream = a;
        this.mWriter = null;
        this.mPrinter = null;
        this.mAutoFlush = b;
        if (android.os.Build.VERSION.SDK_INT < 19) {
            this.mSeparator = "\n";
        } else {
            this.mSeparator = System.lineSeparator();
        }
        this.initDefaultEncoder();
    }
    
    public FastPrintWriter(java.io.Writer a) {
        this(a, false, 8192);
    }
    
    public FastPrintWriter(java.io.Writer a, boolean b) {
        this(a, b, 8192);
    }
    
    public FastPrintWriter(java.io.Writer a, boolean b, int i) {
        super(sDummyWriter, b);
        if (a == null) {
            throw new NullPointerException("wr is null");
        }
        this.mBufferLen = i;
        this.mText = new char[i];
        this.mBytes = null;
        this.mOutputStream = null;
        this.mWriter = a;
        this.mPrinter = null;
        this.mAutoFlush = b;
        if (android.os.Build.VERSION.SDK_INT < 19) {
            this.mSeparator = "\n";
        } else {
            this.mSeparator = System.lineSeparator();
        }
        this.initDefaultEncoder();
    }
    
    private void appendLocked(char a) throws IOException {
        int i = this.mPos;
        int i0 = this.mBufferLen - 1;
        int i1 = a;
        if (i >= i0) {
            this.flushLocked();
            i = this.mPos;
        }
        char[] a0 = this.mText;
        int i2 = (char)i1;
        a0[i] = (char)i2;
        this.mPos = i + 1;
    }
    
    private void appendLocked(String s, int i, int i0) throws IOException {
        int i1 = this.mBufferLen;
        if (i0 <= i1) {
            int i2 = this.mPos;
            if (i2 + i0 > i1) {
                this.flushLocked();
                i2 = this.mPos;
            }
            s.getChars(i, i + i0, this.mText, i2);
            this.mPos = i2 + i0;
        } else {
            int i3 = i + i0;
            while(i < i3) {
                int i4 = i + i1;
                this.appendLocked(s, i, (i4 >= i3) ? i3 - i : i1);
                i = i4;
            }
        }
    }
    
    private void appendLocked(char[] a, int i, int i0) throws IOException {
        int i1 = this.mBufferLen;
        if (i0 <= i1) {
            int i2 = this.mPos;
            if (i2 + i0 > i1) {
                this.flushLocked();
                i2 = this.mPos;
            }
            System.arraycopy(a, i, this.mText, i2, i0);
            this.mPos = i2 + i0;
        } else {
            int i3 = i + i0;
            while(i < i3) {
                int i4 = i + i1;
                this.appendLocked(a, i, (i4 >= i3) ? i3 - i : i1);
                i = i4;
            }
        }
    }
    
    private void flushBytesLocked() throws IOException {
        int i = this.mBytes.position();
        if (i > 0) {
            this.mBytes.flip();
            this.mOutputStream.write(this.mBytes.array(), 0, i);
            this.mBytes.clear();
        }
    }
    
    private void flushLocked() throws IOException {
        if (this.mPos > 0) {
            if (this.mOutputStream == null) {
                if (this.mWriter == null) {
                    int i = 0;
                    int i0 = this.mSeparator.length();
                    if (i0 >= this.mPos) {
                        i0 = this.mPos;
                        i = 0;
                    } else {
                        i = 0;
                    }
                    while(i < i0) {
                        int i1 = this.mText[this.mPos - 1 - i];
                        int i2 = this.mSeparator.charAt(this.mSeparator.length() - 1 - i);
                        if (i1 != i2) {
                            break;
                        }
                        i = i + 1;
                    }
                    if (i < this.mPos) {
                        this.mPrinter.println(new String(this.mText, 0, this.mPos - i));
                    } else {
                        this.mPrinter.println("");
                    }
                } else {
                    this.mWriter.write(this.mText, 0, this.mPos);
                    this.mWriter.flush();
                }
            } else {
                java.nio.CharBuffer a = java.nio.CharBuffer.wrap(this.mText, 0, this.mPos);
                java.nio.charset.CoderResult a0 = this.mCharset.encode(a, this.mBytes, true);
                while(true) {
                    if (a0.isError()) {
                        throw new java.io.IOException(a0.toString());
                    }
                    if (!a0.isOverflow()) {
                        break;
                    }
                    this.flushBytesLocked();
                    a0 = this.mCharset.encode(a, this.mBytes, true);
                }
                this.flushBytesLocked();
                this.mOutputStream.flush();
            }
            this.mPos = 0;
        }
    }
    
    final private void initDefaultEncoder() {
        this.mCharset = java.nio.charset.Charset.defaultCharset().newEncoder();
        this.mCharset.onMalformedInput(java.nio.charset.CodingErrorAction.REPLACE);
        this.mCharset.onUnmappableCharacter(java.nio.charset.CodingErrorAction.REPLACE);
    }
    
    final private void initEncoder(String s) throws UnsupportedEncodingException {
        try {
            this.mCharset = java.nio.charset.Charset.forName(s).newEncoder();
        } catch(Exception ignoredException) {
            throw new java.io.UnsupportedEncodingException(s);
        }
        this.mCharset.onMalformedInput(java.nio.charset.CodingErrorAction.REPLACE);
        this.mCharset.onUnmappableCharacter(java.nio.charset.CodingErrorAction.REPLACE);
    }
    
    public java.io.PrintWriter append(CharSequence a, int i, int i0) {
        Object a0 = (a != null) ? a : "null";
        String s = ((CharSequence)a0).subSequence(i, i0).toString();
        this.write(s, 0, s.length());
        return this;
    }

//    public java.io.Writer append(CharSequence a, int i, int i0) {
//        return this.append(a, i, i0);
//    }
//
//    public Appendable append(CharSequence a, int i, int i0) {
//        return (Appendable)this.append(a, i, i0);
//    }
    
    public boolean checkError() {
        boolean b = false;
        this.flush();
        synchronized(this.lock) {
            b = this.mIoError;
            /*monexit(a)*/;
        }
        return b;
    }
    
    protected void clearError() {
        synchronized(this.lock) {
            this.mIoError = false;
            /*monexit(a)*/;
        }
    }
    
    public void close() {
        synchronized(this.lock) {
            try {
                this.flushLocked();
                if (this.mOutputStream == null) {
                    if (this.mWriter != null) {
                        this.mWriter.close();
                    }
                } else {
                    this.mOutputStream.close();
                }
            } catch(java.io.IOException ignoredException) {
                this.setError();
            }
            /*monexit(a)*/;
        }
    }
    
    public void flush() {
        synchronized(this.lock) {
            try {
                this.flushLocked();
                if (this.mOutputStream == null) {
                    if (this.mWriter != null) {
                        this.mWriter.flush();
                    }
                } else {
                    this.mOutputStream.flush();
                }
            } catch(java.io.IOException ignoredException) {
                this.setError();
            }
            /*monexit(a)*/;
        }
    }
    
    public void print(char a) {
        synchronized(this.lock) {
            int i = a;
            try {
                this.appendLocked((char)i);
            } catch(java.io.IOException ignoredException) {
            }
            /*monexit(a0)*/;
        }
    }
    
    public void print(int i) {
        if (i != 0) {
            super.print(i);
        } else {
            this.print("0");
        }
    }
    
    public void print(long j) {
        if (j != 0L) {
            super.print(j);
        } else {
            this.print("0");
        }
    }
    
    public void print(String s) {
        if (s == null) {
            s = String.valueOf(null);
        }
        synchronized(this.lock) {
            try {
                this.appendLocked(s, 0, s.length());
            } catch(java.io.IOException ignoredException) {
                this.setError();
            }
            /*monexit(a)*/;
        }
    }
    
    public void print(char[] a) {
        synchronized(this.lock) {
            try {
                this.appendLocked(a, 0, a.length);
            } catch(java.io.IOException ignoredException) {
            }
            /*monexit(a0)*/;
        }
    }
    
    public void println() {
        synchronized(this.lock) {
            try {
                this.appendLocked(this.mSeparator, 0, this.mSeparator.length());
                if (this.mAutoFlush) {
                    this.flushLocked();
                }
            } catch(java.io.IOException ignoredException) {
                this.setError();
            }
            /*monexit(a)*/;
        }
    }
    
    public void println(char a) {
        this.print(a);
        this.println();
    }
    
    public void println(int i) {
        if (i != 0) {
            super.println(i);
        } else {
            this.println("0");
        }
    }
    
    public void println(long j) {
        if (j != 0L) {
            super.println(j);
        } else {
            this.println("0");
        }
    }
    
    public void println(char[] a) {
        this.print(a);
        this.println();
    }
    
    protected void setError() {
        synchronized(this.lock) {
            this.mIoError = true;
            /*monexit(a)*/;
        }
    }
    
    public void write(int i) {
        synchronized(this.lock) {
            int i0 = (char)i;
            try {
                this.appendLocked((char)i0);
            } catch(java.io.IOException ignoredException) {
            }
            /*monexit(a)*/;
        }
    }
    
    public void write(String s) {
        synchronized(this.lock) {
            try {
                this.appendLocked(s, 0, s.length());
            } catch(java.io.IOException ignoredException) {
            }
            /*monexit(a)*/;
        }
    }
    
    public void write(String s, int i, int i0) {
        synchronized(this.lock) {
            try {
                this.appendLocked(s, i, i0);
            } catch(java.io.IOException ignoredException) {
            }
            /*monexit(a)*/;
        }
    }
    
    public void write(char[] a, int i, int i0) {
        synchronized(this.lock) {
            try {
                this.appendLocked(a, i, i0);
            } catch(java.io.IOException ignoredException) {
            }
            /*monexit(a0)*/;
        }
    }
}
