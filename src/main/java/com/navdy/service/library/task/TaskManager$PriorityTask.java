package com.navdy.service.library.task;

final class TaskManager$PriorityTask extends java.util.concurrent.FutureTask implements Comparable {
    final private com.navdy.service.library.task.TaskManager$PriorityRunnable priorityRunnable;
    
    public TaskManager$PriorityTask(com.navdy.service.library.task.TaskManager$PriorityRunnable a, Object a0) {
        super((Runnable)a, a0);
        this.priorityRunnable = a;
    }
    
    public TaskManager$PriorityTask(com.navdy.service.library.task.TaskManager$PriorityRunnable a, java.util.concurrent.Callable a0) {
        super(a0);
        this.priorityRunnable = a;
    }
    
    public int compareTo(com.navdy.service.library.task.TaskManager$PriorityTask a) {
        int i = this.priorityRunnable.priority - a.priorityRunnable.priority;
        if (i == 0) {
            i = (int)(this.priorityRunnable.order - a.priorityRunnable.order);
        }
        return i;
    }
    
    public int compareTo(Object a) {
        return this.compareTo((com.navdy.service.library.task.TaskManager$PriorityTask)a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        if (a instanceof com.navdy.service.library.task.TaskManager$PriorityTask) {
            com.navdy.service.library.task.TaskManager$PriorityTask a0 = (com.navdy.service.library.task.TaskManager$PriorityTask)a;
            if (this.priorityRunnable.priority != a0.priorityRunnable.priority) {
                b = false;
            } else {
                b = this.priorityRunnable.order == a0.priorityRunnable.order;
            }
        } else {
            b = false;
        }
        return b;
    }
    
    public int hashCode() {
        return (int)this.priorityRunnable.order;
    }
}
