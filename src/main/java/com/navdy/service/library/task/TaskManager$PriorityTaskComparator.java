package com.navdy.service.library.task;

class TaskManager$PriorityTaskComparator implements java.util.Comparator, java.io.Serializable {
    private TaskManager$PriorityTaskComparator() {
    }
    
    TaskManager$PriorityTaskComparator(com.navdy.service.library.task.TaskManager$1 a) {
        this();
    }
    
    public int compare(Object a, Object a0) {
        return this.compare((Runnable)a, (Runnable)a0);
    }
    
    public int compare(Runnable a, Runnable a0) {
        return ((com.navdy.service.library.task.TaskManager$PriorityTask)a).compareTo((com.navdy.service.library.task.TaskManager$PriorityTask)a0);
    }
}
