package com.navdy.service.library.file;

abstract public interface IFileTransferAuthority {
    abstract public String getDirectoryForFileType(com.navdy.service.library.events.file.FileType arg);
    
    
    abstract public String getFileToSend(com.navdy.service.library.events.file.FileType arg);
    
    
    abstract public boolean isFileTypeAllowed(com.navdy.service.library.events.file.FileType arg);
    
    
    abstract public void onFileSent(com.navdy.service.library.events.file.FileType arg);
}
