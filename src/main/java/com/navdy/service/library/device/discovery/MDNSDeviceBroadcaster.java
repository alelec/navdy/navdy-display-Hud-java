package com.navdy.service.library.device.discovery;

abstract public class MDNSDeviceBroadcaster implements com.navdy.service.library.device.discovery.RemoteDeviceBroadcaster {
    final public static com.navdy.service.library.log.Logger sLogger;
    protected android.net.nsd.NsdManager mNsdManager;
    protected android.net.nsd.NsdManager.RegistrationListener mRegistrationListener;
    protected android.net.nsd.NsdServiceInfo mServiceInfo;
    protected boolean registered;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.device.discovery.MDNSDeviceBroadcaster.class);
    }
    
    protected MDNSDeviceBroadcaster() {
    }
    
    public MDNSDeviceBroadcaster(android.content.Context a, android.net.nsd.NsdServiceInfo a0) {
        this.mNsdManager = (android.net.nsd.NsdManager)a.getSystemService("servicediscovery");
        if (a0 == null) {
            throw new IllegalArgumentException("Service info required.");
        }
        this.mServiceInfo = a0;
    }
    
    protected android.net.nsd.NsdManager.RegistrationListener getRegistrationListener() {
        return (android.net.nsd.NsdManager.RegistrationListener)new com.navdy.service.library.device.discovery.MDNSDeviceBroadcaster$1(this);
    }
    
    protected boolean registerRecord(android.net.nsd.NsdServiceInfo a, android.net.nsd.NsdManager.RegistrationListener a0) {
        sLogger.e(new StringBuilder().append("Registering: ").append(a.getServiceType()).toString());
        new java.util.Timer().schedule((java.util.TimerTask)new com.navdy.service.library.device.discovery.MDNSDeviceBroadcaster$2(this, a, a0), 2000L);
        return true;
    }
    
    public boolean start() {
        boolean b = false;
        if (this.mRegistrationListener == null) {
            this.mRegistrationListener = this.getRegistrationListener();
            b = this.registerRecord(this.mServiceInfo, this.mRegistrationListener);
        } else {
            sLogger.e(new StringBuilder().append("Already started: ").append(this.mServiceInfo.getServiceType()).toString());
            b = false;
        }
        return b;
    }
    
    public boolean stop() {
        boolean b = false;
        if (this.mRegistrationListener != null) {
            sLogger.e(new StringBuilder().append("Unregistering: ").append(this.mServiceInfo.getServiceType()).toString());
            b = this.unregisterRecord(this.mRegistrationListener);
        } else {
            sLogger.e(new StringBuilder().append("Already stopped: ").append(this.mServiceInfo.getServiceType()).toString());
            b = false;
        }
        return b;
    }
    
    protected boolean unregisterRecord(android.net.nsd.NsdManager.RegistrationListener a) {
        if (this.registered) {
            this.registered = false;
            this.mNsdManager.unregisterService(a);
        }
        return true;
    }
}
