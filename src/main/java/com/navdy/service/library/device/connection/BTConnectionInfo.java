package com.navdy.service.library.device.connection;

public class BTConnectionInfo extends com.navdy.service.library.device.connection.ConnectionInfo {
    com.navdy.service.library.device.connection.ServiceAddress mAddress;
    
    public BTConnectionInfo(com.navdy.service.library.device.NavdyDeviceId a, com.navdy.service.library.device.connection.ServiceAddress a0, com.navdy.service.library.device.connection.ConnectionType a1) {
        super(a, a1);
        this.mAddress = a0;
    }
    
    public BTConnectionInfo(com.navdy.service.library.device.NavdyDeviceId a, java.util.UUID a0, com.navdy.service.library.device.connection.ConnectionType a1) {
        super(a, a1);
        if (a.getBluetoothAddress() == null) {
            throw new IllegalArgumentException("BT address missing");
        }
        this.mAddress = new com.navdy.service.library.device.connection.ServiceAddress(a.getBluetoothAddress(), a0.toString());
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        if (this != a) {
            if (a == null) {
                b = false;
            } else if ((this).getClass() != a.getClass()) {
                b = false;
            } else if (super.equals(a)) {
                com.navdy.service.library.device.connection.BTConnectionInfo a0 = (com.navdy.service.library.device.connection.BTConnectionInfo)a;
                b = this.mAddress.equals(a0.mAddress);
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public com.navdy.service.library.device.connection.ServiceAddress getAddress() {
        return this.mAddress;
    }
    
    public int hashCode() {
        return super.hashCode() * 31 + this.mAddress.hashCode();
    }
}
