package com.navdy.service.library.device.connection;

import com.navdy.service.library.device.connection.Connection.ConnectionFailureCause;
import com.navdy.service.library.device.connection.Connection.DisconnectCause;
import com.navdy.service.library.device.connection.Connection.Status;
import com.navdy.service.library.network.SocketAdapter;
import com.navdy.service.library.network.SocketFactory;
import java.io.IOException;
import java.net.SocketException;

public class SocketConnection extends Connection {
    /* access modifiers changed from: private */
    public SocketFactory connector;
    protected boolean mBlockReconnect;
    protected ConnectThread mConnectThread;
    private SocketAdapter socket;

    private class ConnectThread extends Thread {
        private SocketAdapter mmSocket;

        public ConnectThread() {
            setName("ConnectThread");
        }

        public void cancel() {
            SocketConnection.this.logger.d("[SOCK]Cancelling connect, closing socket");
            SocketConnection.this.closeQuietly(this.mmSocket);
            this.mmSocket = null;
        }

        public void run() {
            SocketConnection.this.logger.i("BEGIN mConnectThread");
            ConnectionFailureCause connectionFailureCause = ConnectionFailureCause.UNKNOWN;
            try {
                this.mmSocket = SocketConnection.this.connector.build();
                this.mmSocket.connect();
                synchronized (SocketConnection.this) {
                    SocketConnection.this.mConnectThread = null;
                }
                SocketConnection.this.connected(this.mmSocket);
            } catch (Exception e) {
                SocketConnection.this.logger.e("Unable to connect - " + e.getMessage());
                if (e instanceof SocketException) {
                    String message = e.getMessage();
                    if (message.contains("ETIMEDOUT")) {
                        connectionFailureCause = ConnectionFailureCause.CONNECTION_TIMED_OUT;
                    } else if (message.contains("ECONNREFUSED")) {
                        connectionFailureCause = ConnectionFailureCause.CONNECTION_REFUSED;
                    }
                }
                synchronized (SocketConnection.this) {
                    SocketConnection.this.logger.d("Connect failed, closing socket");
                    SocketConnection.this.closeQuietly(this.mmSocket);
                    this.mmSocket = null;
                    Status status = SocketConnection.this.mStatus;
                    SocketConnection.this.mStatus = Status.DISCONNECTED;
                    switch (status) {
                        case CONNECTING:
                            SocketConnection.this.connectionFailed(connectionFailureCause);
                            return;
                        case DISCONNECTING:
                            SocketConnection.this.dispatchDisconnectEvent(DisconnectCause.NORMAL);
                            return;
                        default:
                            SocketConnection.this.logger.w("unexpected state during connection failure: " + status);
                            return;
                    }
                }
            }
        }
    }

    public SocketConnection(ConnectionInfo connectionInfo, SocketAdapter socketAdapter) {
        super(connectionInfo);
        setExistingSocketConnection(socketAdapter);
    }

    public SocketConnection(ConnectionInfo connectionInfo, SocketFactory socketFactory) {
        super(connectionInfo);
        this.connector = socketFactory;
    }

    /* access modifiers changed from: private */
    public void closeQuietly(SocketAdapter socketAdapter) {
        if (socketAdapter != null) {
            try {
                socketAdapter.close();
            } catch (Throwable th) {
                this.logger.e("[SOCK] Exception while closing", th);
            }
        }
    }

    /* access modifiers changed from: private */
    public void connectionFailed(ConnectionFailureCause connectionFailureCause) {
        dispatchConnectionFailedEvent(connectionFailureCause);
    }

    public boolean connect() {
        boolean z = false;
        synchronized (this) {
            if (!this.mBlockReconnect) {
                this.logger.d("connect to: " + this.mConnectionInfo);
                if (this.mStatus != Status.DISCONNECTED) {
                    this.logger.d("can't connect: state: " + this.mStatus);
                } else {
                    this.mStatus = Status.CONNECTING;
                    this.mConnectThread = new ConnectThread();
                    this.mConnectThread.start();
                    z = true;
                }
            }
        }
        return z;
    }

    public void connected(SocketAdapter socketAdapter) {
        synchronized (this) {
            this.logger.i("connected");
            if (this.mConnectThread != null) {
                this.mConnectThread.cancel();
                this.mConnectThread = null;
            }
            if (this.mStatus != Status.CONNECTING || socketAdapter == null) {
                this.logger.e("No longer connecting - disconnecting.");
                closeQuietly(socketAdapter);
                this.mStatus = Status.DISCONNECTED;
                dispatchDisconnectEvent(DisconnectCause.NORMAL);
            } else {
                this.socket = socketAdapter;
                this.mStatus = Status.CONNECTED;
                dispatchConnectEvent();
            }
        }
    }

    public boolean disconnect() {
        synchronized (this) {
            if (this.mStatus == Status.DISCONNECTED || this.mStatus == Status.DISCONNECTING) {
                this.logger.d("can't disconnect: state: " + this.mStatus);
            } else {
                this.logger.d("disconnect - connectThread:" + this.mConnectThread + " socket:" + this.socket);
                this.mStatus = Status.DISCONNECTING;
                if (this.mConnectThread != null) {
                    this.mConnectThread.cancel();
                    this.mConnectThread = null;
                } else if (this.socket != null) {
                    try {
                        this.socket.close();
                    } catch (IOException e) {
                        this.logger.e("Exception closing socket" + e.getMessage());
                    }
                    this.socket = null;
                } else {
                    this.mStatus = Status.DISCONNECTED;
                    dispatchDisconnectEvent(DisconnectCause.NORMAL);
                }
            }
        }
        return true;
    }

    public SocketAdapter getSocket() {
        return this.socket;
    }

    /* access modifiers changed from: protected */
    public void setExistingSocketConnection(SocketAdapter socketAdapter) {
        synchronized (this) {
            this.mBlockReconnect = true;
            this.mStatus = Status.CONNECTING;
            connected(socketAdapter);
        }
    }
}