package com.navdy.service.library.device.connection;

import android.content.Context;

import com.navdy.service.library.log.Logger;
import com.navdy.service.library.network.BTSocketFactory;
import com.navdy.service.library.network.SocketAdapter;
import com.navdy.service.library.network.TCPSocketFactory;
import com.navdy.service.library.util.Listenable;

import java.util.HashMap;
import java.util.Map;

public abstract class Connection extends Listenable<Connection.Listener> {
    private static Map<ConnectionType, Connection.ConnectionFactory> factoryMap = new HashMap<>();
    private static Connection.ConnectionFactory sDefaultFactory = new Connection.ConnectionFactory() {
        public Connection build(Context context, ConnectionInfo connectionInfo) {
            switch (connectionInfo.getType()) {
                case BT_PROTOBUF:
                case BT_TUNNEL:
                case EA_PROTOBUF:
                case BT_IAP2_LINK:
                    ServiceAddress address = connectionInfo.getAddress();
                    return new SocketConnection(connectionInfo, new BTSocketFactory(
                            address.getService().equals(ConnectionService.ACCESSORY_IAP2.toString()) ?
                                    new ServiceAddress(address.getAddress(), ConnectionService.DEVICE_IAP2.toString(), address.getProtocol()) : address)
                    );
                case TCP_PROTOBUF:
                    return new SocketConnection(connectionInfo, new TCPSocketFactory(connectionInfo.getAddress()));
                default:
                    throw new IllegalArgumentException("Unknown connection class for type: " + connectionInfo.getType());
            }
        }
    };
    protected Logger logger;
    protected ConnectionInfo mConnectionInfo;
    protected Connection.Status mStatus;

    public interface ConnectionFactory {
        Connection build(Context context, ConnectionInfo connectionInfo);
    }

    public enum ConnectionFailureCause {
        UNKNOWN,
        CONNECTION_REFUSED,
        CONNECTION_TIMED_OUT
    }

    public enum DisconnectCause {
        UNKNOWN,
        NORMAL,
        ABORTED
    }

    protected interface EventDispatcher extends Listenable.EventDispatcher<Connection, Connection.Listener> {
    }

    public interface Listener extends Listenable.Listener {
        void onConnected(Connection connection);

        void onConnectionFailed(Connection connection, Connection.ConnectionFailureCause connectionFailureCause);

        void onDisconnected(Connection connection, Connection.DisconnectCause disconnectCause);
    }

    public enum Status {
        DISCONNECTED,
        CONNECTING,
        CONNECTED,
        DISCONNECTING
    }

    static {
        registerConnectionType(ConnectionType.BT_PROTOBUF, sDefaultFactory);
        registerConnectionType(ConnectionType.BT_TUNNEL, sDefaultFactory);
        registerConnectionType(ConnectionType.TCP_PROTOBUF, sDefaultFactory);
        registerConnectionType(ConnectionType.BT_IAP2_LINK, sDefaultFactory);
        registerConnectionType(ConnectionType.EA_PROTOBUF, sDefaultFactory);
    }

    public Connection(ConnectionInfo connectionInfo) {
        this.mConnectionInfo = connectionInfo;
        this.mStatus = Connection.Status.DISCONNECTED;
        logger = new Logger(getClass());
    }

    public static Connection instantiateFromConnectionInfo(Context context, ConnectionInfo connectionInfo) {
        Connection.ConnectionFactory connectionFactory = (Connection.ConnectionFactory) factoryMap.get(connectionInfo.getType());
        if (connectionFactory != null) {
            return connectionFactory.build(context, connectionInfo);
        }
        throw new IllegalArgumentException("Unknown connection class for type: " + connectionInfo.getType());
    }

    public static void registerConnectionType(ConnectionType connectionType, Connection.ConnectionFactory connectionFactory) {
        factoryMap.put(connectionType, connectionFactory);
    }

    public abstract boolean connect();

    public abstract boolean disconnect();

    /* access modifiers changed from: protected */
    public void dispatchConnectEvent() {
        dispatchToListeners(new Connection.EventDispatcher() {
            public void dispatchEvent(Connection connection, Connection.Listener listener) {
                listener.onConnected(connection);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void dispatchConnectionFailedEvent(final Connection.ConnectionFailureCause connectionFailureCause) {
        dispatchToListeners(new Connection.EventDispatcher() {
            public void dispatchEvent(Connection connection, Connection.Listener listener) {
                listener.onConnectionFailed(connection, connectionFailureCause);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void dispatchDisconnectEvent(final Connection.DisconnectCause disconnectCause) {
        dispatchToListeners(new Connection.EventDispatcher() {
            public void dispatchEvent(Connection connection, Connection.Listener listener) {
                listener.onDisconnected(connection, disconnectCause);
            }
        });
    }

    public ConnectionInfo getConnectionInfo() {
        return this.mConnectionInfo;
    }

    public abstract SocketAdapter getSocket();

    public Connection.Status getStatus() {
        return this.mStatus;
    }

    public ConnectionType getType() {
        return this.mConnectionInfo.getType();
    }
}