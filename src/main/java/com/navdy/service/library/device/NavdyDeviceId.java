package com.navdy.service.library.device;

public class NavdyDeviceId {
    final protected static String ATTRIBUTE_NAME = "N";
    final protected static String ATTRIBUTE_SEPARATOR = ";";
    final protected static String RESERVED_CHARACTER_REGEX = ";|/";
    final public static com.navdy.service.library.device.NavdyDeviceId UNKNOWN_ID;
    final protected static String VALUE_SEPARATOR = "/";
    static com.navdy.service.library.device.NavdyDeviceId sThisDeviceId;
    protected String mDeviceId;
    protected com.navdy.service.library.device.NavdyDeviceId$Type mDeviceIdType;
    protected String mDeviceName;
    protected String mSerializedDeviceId;
    
    static {
        UNKNOWN_ID = new com.navdy.service.library.device.NavdyDeviceId("UNK/FF:FF:FF:FF:FF:FF;N/Unknown");
    }
    
    public NavdyDeviceId(android.bluetooth.BluetoothDevice a) {
        this(com.navdy.service.library.device.NavdyDeviceId$Type.BT, a.getAddress(), (android.text.TextUtils.isEmpty(a.getName())) ? "unknown" : a.getName());
    }
    
    public NavdyDeviceId(com.navdy.service.library.device.NavdyDeviceId$Type a, String s, String s0) {
        this.mDeviceIdType = a;
        this.mDeviceId = this.normalizeDeviceIdString(s);
        this.mDeviceName = this.normalizeDeviceName(s0);
        this.mSerializedDeviceId = this.createSerializedDeviceId();
    }
    
    public NavdyDeviceId(String s) {
        if (s != null && !s.equals("")) {
            if (!this.parseDeviceIdString(s)) {
                throw new IllegalArgumentException("Invalid device ID string.");
            }
            this.mSerializedDeviceId = this.createSerializedDeviceId();
            return;
        }
        throw new IllegalArgumentException("Cannot create empty NavdyDeviceId");
    }
    
    private String createSerializedDeviceId() {
        String s = this.mDeviceIdType.toString() + "/" + this.mDeviceId;
        if (!android.text.TextUtils.isEmpty(this.mDeviceName)) {
            s = s + ";N/" + this.mDeviceName;
        }
        return s;
    }
    
    public static com.navdy.service.library.device.NavdyDeviceId getThisDevice(android.content.Context a) {
        com.navdy.service.library.device.NavdyDeviceId a0 = sThisDeviceId;
        label0: {
            label1: {
                if (a0 == null) {
                    break label1;
                }
                if (!sThisDeviceId.equals(UNKNOWN_ID)) {
                    break label0;
                }
            }
            android.bluetooth.BluetoothAdapter a1 = android.bluetooth.BluetoothAdapter.getDefaultAdapter();
            if (a1 != null) {
                String s = a1.getAddress();
                if (android.text.TextUtils.isEmpty(s)) {
                    sThisDeviceId = UNKNOWN_ID;
                } else {
                    String s0 = a1.getName();
                    sThisDeviceId = new com.navdy.service.library.device.NavdyDeviceId(com.navdy.service.library.device.NavdyDeviceId$Type.BT, s, s0);
                }
            } else if (a != null) {
                String s1 = android.provider.Settings.Secure.getString(a.getContentResolver(), "android_id");
                if (android.text.TextUtils.isEmpty(s1)) {
                    sThisDeviceId = UNKNOWN_ID;
                } else {
                    sThisDeviceId = new com.navdy.service.library.device.NavdyDeviceId(com.navdy.service.library.device.NavdyDeviceId$Type.EMU, s1, "emulator");
                }
            }
        }
        return sThisDeviceId;
    }
    
    public boolean equals(Object a) {
        boolean b;
        label0: if (this != a) {
            label2: {
                label3: {
                    if (a == null) {
                        break label3;
                    }
                    if ((this).getClass() == a.getClass()) {
                        break label2;
                    }
                }
                b = false;
                break label0;
            }
            com.navdy.service.library.device.NavdyDeviceId a0 = (com.navdy.service.library.device.NavdyDeviceId)a;
            boolean b0 = this.mDeviceId.equals(a0.mDeviceId);
            label1: {
                if (!b0) {
                    break label1;
                }
                if (this.mDeviceIdType == a0.mDeviceIdType) {
                    b = true;
                    break label0;
                }
            }
            b = false;
        } else {
            b = true;
        }
        return b;
    }
    
    public String getBluetoothAddress() {
        String s;
        com.navdy.service.library.device.NavdyDeviceId$Type a = this.mDeviceIdType;
        com.navdy.service.library.device.NavdyDeviceId$Type a0 = com.navdy.service.library.device.NavdyDeviceId$Type.BT;
        label2: {
            label0: {
                label1: {
                    if (a == a0) {
                        break label1;
                    }
                    if (this.mDeviceIdType != com.navdy.service.library.device.NavdyDeviceId$Type.EA) {
                        break label0;
                    }
                }
                s = this.mDeviceId;
                break label2;
            }
            s = null;
        }
        return s;
    }
    
    public String getDeviceName() {
        return this.mDeviceName;
    }
    
    public String getDisplayName() {
        return (android.text.TextUtils.isEmpty(this.mDeviceName)) ? this.toString() : this.mDeviceName;
    }
    
    public int hashCode() {
        return this.mDeviceId.hashCode() * 31 + this.mDeviceIdType.hashCode();
    }
    
    protected String normalizeDeviceIdString(String s) {
        return s.toUpperCase(java.util.Locale.US);
    }
    
    protected String normalizeDeviceName(String s) {
        String s0;
        if (s != null) {
            s0 = s.replaceAll("[;/]", "");
            if (s0.equals("")) {
                s0 = null;
            }
        } else {
            s0 = null;
        }
        return s0;
    }
    
    protected boolean parseDeviceIdString(String s) {
        boolean b;
        String[] a = s.split(";");
        int i = a.length;
        label0: {
            if (i != 0) {
                int i0 = a.length;
                int i1 = 0;
                while(true) {
                    if (i1 >= i0) {
                        b = true;
                        break label0;
                    } else {
                        String[] a0 = a[i1].split("/");
                        if (a0.length < 2) {
                            b = false;
                            break label0;
                        } else {
                            String s0 = a0[0];
                            String s1 = a0[1];
                            if (s0.equals("N")) {
                                this.mDeviceName = this.normalizeDeviceName(s1);
                            } else {
                                try {
                                    this.mDeviceIdType = com.navdy.service.library.device.NavdyDeviceId$Type.valueOf(a0[0]);
                                } catch(IllegalArgumentException ignoredException) {
                                    break;
                                }
                                this.mDeviceId = this.normalizeDeviceIdString(a0[1]);
                            }
                            i1 = i1 + 1;
                        }
                    }
                }
            } else {
                b = false;
                break label0;
            }
            throw new IllegalArgumentException("Invalid device ID type");
        }
        return b;
    }
    
    public String toString() {
        return this.mSerializedDeviceId;
    }
}
