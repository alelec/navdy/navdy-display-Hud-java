package com.navdy.service.library.device.link;

public class ProtobufLink implements com.navdy.service.library.device.link.Link {
    private int bandwidthLevel;
    private com.navdy.service.library.device.connection.ConnectionInfo connectionInfo;
    private com.navdy.service.library.device.link.ReaderThread readerThread;
    private com.navdy.service.library.device.link.WriterThread writerThread;
    
    public ProtobufLink(com.navdy.service.library.device.connection.ConnectionInfo a) {
        this.bandwidthLevel = 1;
        this.connectionInfo = a;
    }
    
    public void close() {
        if (this.writerThread != null) {
            this.writerThread.cancel();
            this.writerThread = null;
        }
        if (this.readerThread != null) {
            this.readerThread.cancel();
            this.readerThread = null;
        }
    }
    
    public int getBandWidthLevel() {
        return this.bandwidthLevel;
    }
    
    public void setBandwidthLevel(int i) {
        this.bandwidthLevel = i;
    }
    
    public boolean start(com.navdy.service.library.network.SocketAdapter a, java.util.concurrent.LinkedBlockingDeque a0, com.navdy.service.library.device.link.LinkListener a1) {
        boolean b = false;
        com.navdy.service.library.device.link.WriterThread a2 = this.writerThread;
        label2: {
            label3: {
                label4: {
                    if (a2 == null) {
                        break label4;
                    }
                    if (!this.writerThread.isClosing()) {
                        break label3;
                    }
                }
                if (this.readerThread == null) {
                    break label2;
                }
                if (this.readerThread.isClosing()) {
                    break label2;
                }
            }
            throw new IllegalStateException("Must stop threads before calling startLink");
        }
        label1: {
            com.navdy.service.library.device.link.WriterThread a3 = null;
            label0: {
                java.io.InputStream a4 = null;
                try {
                    a4 = null;
                    a4 = a.getInputStream();
                    java.io.OutputStream a5 = a.getOutputStream();
                    this.readerThread = new com.navdy.service.library.device.link.ReaderThread(this.connectionInfo.getType(), a4, a1, true);
                    a3 = new com.navdy.service.library.device.link.WriterThread(a0, a5, (com.navdy.service.library.device.link.WriterThread$EventProcessor)null);
                    break label0;
                } catch(java.io.IOException ignoredException) {
                }
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a4);
                this.readerThread = null;
                this.writerThread = null;
                b = false;
                break label1;
            }
            this.writerThread = a3;
            this.readerThread.start();
            this.writerThread.start();
            b = true;
        }
        return b;
    }
}
