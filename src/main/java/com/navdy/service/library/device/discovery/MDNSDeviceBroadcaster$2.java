package com.navdy.service.library.device.discovery;

class MDNSDeviceBroadcaster$2 extends java.util.TimerTask {
    final com.navdy.service.library.device.discovery.MDNSDeviceBroadcaster this$0;
    final android.net.nsd.NsdManager.RegistrationListener val$registrationListener;
    final android.net.nsd.NsdServiceInfo val$serviceInfo;
    
    MDNSDeviceBroadcaster$2(com.navdy.service.library.device.discovery.MDNSDeviceBroadcaster a, android.net.nsd.NsdServiceInfo a0, android.net.nsd.NsdManager.RegistrationListener a1) {
        super();
        this.this$0 = a;
        this.val$serviceInfo = a0;
        this.val$registrationListener = a1;
    }
    
    public void run() {
        com.navdy.service.library.device.discovery.MDNSDeviceBroadcaster.sLogger.e(new StringBuilder().append("Register timer fired: ").append(this.val$serviceInfo.getServiceType()).toString());
        this.this$0.mNsdManager.registerService(this.val$serviceInfo, 1, this.val$registrationListener);
        this.this$0.registered = true;
        com.navdy.service.library.device.discovery.MDNSDeviceBroadcaster.sLogger.e(new StringBuilder().append("Registered: ").append(this.val$serviceInfo.getServiceType()).toString());
    }
}
