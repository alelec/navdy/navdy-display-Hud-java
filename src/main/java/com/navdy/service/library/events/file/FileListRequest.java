package com.navdy.service.library.events.file;

final public class FileListRequest extends com.squareup.wire.Message {
    final public static com.navdy.service.library.events.file.FileType DEFAULT_FILE_TYPE;
    final private static long serialVersionUID = 0L;
    final public com.navdy.service.library.events.file.FileType file_type;
    
    static {
        DEFAULT_FILE_TYPE = com.navdy.service.library.events.file.FileType.FILE_TYPE_OTA;
    }
    
    private FileListRequest(com.navdy.service.library.events.file.FileListRequest$Builder a) {
        this(a.file_type);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    FileListRequest(com.navdy.service.library.events.file.FileListRequest$Builder a, com.navdy.service.library.events.file.FileListRequest$1 a0) {
        this(a);
    }
    
    public FileListRequest(com.navdy.service.library.events.file.FileType a) {
        this.file_type = a;
    }
    
    public boolean equals(Object a) {
        return a == this || a instanceof com.navdy.service.library.events.file.FileListRequest && this.equals(this.file_type, ((com.navdy.service.library.events.file.FileListRequest)a).file_type);
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (this.file_type == null) ? 0 : this.file_type.hashCode();
            this.hashCode = i;
        }
        return i;
    }
}
