package com.navdy.service.library.events.file;

final public class PreviewFileResponse$Builder extends com.squareup.wire.Message.Builder {
    public String filename;
    public com.navdy.service.library.events.RequestStatus status;
    public String status_detail;
    
    public PreviewFileResponse$Builder() {
    }
    
    public PreviewFileResponse$Builder(com.navdy.service.library.events.file.PreviewFileResponse a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.status = a.status;
            this.status_detail = a.status_detail;
            this.filename = a.filename;
        }
    }
    
    public com.navdy.service.library.events.file.PreviewFileResponse build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.file.PreviewFileResponse(this, (com.navdy.service.library.events.file.PreviewFileResponse$1)null);
    }
    
//    public com.squareup.wire.Message build() {
//        return this.build();
//    }
    
    public com.navdy.service.library.events.file.PreviewFileResponse$Builder filename(String s) {
        this.filename = s;
        return this;
    }
    
    public com.navdy.service.library.events.file.PreviewFileResponse$Builder status(com.navdy.service.library.events.RequestStatus a) {
        this.status = a;
        return this;
    }
    
    public com.navdy.service.library.events.file.PreviewFileResponse$Builder status_detail(String s) {
        this.status_detail = s;
        return this;
    }
}
