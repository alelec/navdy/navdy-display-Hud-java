package com.navdy.hud.mfi;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public abstract class IAPControlMessageProcessor {
    protected ExecutorService mSerialExecutor = Executors.newSingleThreadExecutor();
    protected iAPProcessor miAPProcessor;

    public abstract void bProcessControlMessage(iAPMessage iapmessage, int i, byte[] bArr);

    public IAPControlMessageProcessor(iAPMessage[] messages, iAPProcessor processor) {
        this.miAPProcessor = processor;
        if (messages != null) {
            for (iAPMessage message : messages) {
                this.miAPProcessor.registerControlMessageProcessor(message, this);
            }
        }
    }

    public void processControlMessage(final iAPMessage message, final int session, final byte[] data) {
        this.mSerialExecutor.submit(new Runnable() {
            public void run() {
                IAPControlMessageProcessor.this.bProcessControlMessage(message, session, data);
            }
        });
    }
}
