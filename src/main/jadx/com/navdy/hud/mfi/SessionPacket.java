package com.navdy.hud.mfi;

public class SessionPacket extends Packet {
    int session;

    public SessionPacket(int session, byte[] data) {
        super(data);
        this.session = session;
    }
}
