package com.navdy.hud.mfi;

public class EASessionPacket extends Packet {
    int session;

    public EASessionPacket(int session, byte[] data) {
        super(data);
        this.session = session;
    }
}
