package com.navdy.hud.mfi;

import android.text.TextUtils;
import android.util.Log;
import com.navdy.hud.mfi.NowPlayingUpdate.PlaybackRepeat;
import com.navdy.hud.mfi.NowPlayingUpdate.PlaybackShuffle;
import com.navdy.hud.mfi.NowPlayingUpdate.PlaybackStatus;

public class IAPMusicManager extends IAPControlMessageProcessor {
    private static final String TAG = IAPMusicManager.class.getSimpleName();
    public static iAPMessage[] mMessages = new iAPMessage[]{iAPMessage.NowPlayingUpdate};
    private NowPlayingUpdate aggregatedNowPlayingUpdate = new NowPlayingUpdate();
    private IAPNowPlayingUpdateListener mNowPlayingUpdateListener;

    public enum MediaItemAttributes {
        MediaItemPersistentIdentifier(0),
        MediaItemTitle(1),
        MediaItemPlaybackDurationInMilliSeconds(4),
        MediaItemAlbumTitle(6),
        MediaItemAlbumTrackNumber(7),
        MediaItemAlbumTrackCount(8),
        MediaItemArtist(12),
        MediaItemGenre(16),
        MediaItemArtworkFileTransferIdentifier(26);
        
        int paramIndex;

        private MediaItemAttributes(int index) {
            this.paramIndex = index;
        }

        public int getParam() {
            return this.paramIndex;
        }
    }

    public enum MediaKey {
        Siri,
        PlayPause,
        Next,
        Previous
    }

    public enum NowPlayingUpdatesParameters {
        MediaItemAttributes,
        PlaybackAttributes
    }

    public enum PlaybackAttributes {
        PlaybackStatus(0),
        PlaybackElapsedTimeInMilliseconds(1),
        PlaybackShuffleMode(5),
        PlaybackRepeatMode(6),
        PlaybackAppName(7),
        PlaybackAppBundleId(16);
        
        int paramIndex;

        private PlaybackAttributes(int index) {
            this.paramIndex = index;
        }

        public int getParam() {
            return this.paramIndex;
        }
    }

    public IAPMusicManager(iAPProcessor processor) {
        super(mMessages, processor);
    }

    public void setNowPlayingUpdateListener(IAPNowPlayingUpdateListener nowPlayingUpdateListener) {
        this.mNowPlayingUpdateListener = nowPlayingUpdateListener;
    }

    public void startNowPlayingUpdates() {
        int i = 0;
        IAP2SessionMessage message = new IAP2SessionMessage(iAPMessage.StartNowPlayingUpdates);
        IAP2ParamsCreator mediaItemProperties = new IAP2ParamsCreator();
        for (MediaItemAttributes attribute : MediaItemAttributes.values()) {
            mediaItemProperties.addNone(attribute.getParam());
        }
        message.addBlob(NowPlayingUpdatesParameters.MediaItemAttributes, mediaItemProperties.toBytes());
        IAP2ParamsCreator playbackProperties = new IAP2ParamsCreator();
        PlaybackAttributes[] values = PlaybackAttributes.values();
        int length = values.length;
        while (i < length) {
            playbackProperties.addNone(values[i].getParam());
            i++;
        }
        message.addBlob(NowPlayingUpdatesParameters.PlaybackAttributes, playbackProperties.toBytes());
        this.miAPProcessor.sendControlMessage(message);
    }

    public void stopNowPlayingUpdates() {
        this.miAPProcessor.sendControlMessage(new IAP2SessionMessage(iAPMessage.StopNowPlayingUpdates));
    }

    public void bProcessControlMessage(iAPMessage message, int session, byte[] data) {
        IAP2Params params = iAPProcessor.parse(data);
        switch (message) {
            case NowPlayingUpdate:
                NowPlayingUpdate update = parseNowPlayingUpdate(params);
                Log.d(TAG, "Parsed " + update);
                mergeNowPlayingUpdate(update);
                if (this.mNowPlayingUpdateListener != null) {
                    this.mNowPlayingUpdateListener.onNowPlayingUpdate(this.aggregatedNowPlayingUpdate);
                    return;
                }
                return;
            default:
                return;
        }
    }

    private static NowPlayingUpdate parseNowPlayingUpdate(IAP2Params params) {
        NowPlayingUpdate nowPlayingUpdate = new NowPlayingUpdate();
        if (params.hasParam(NowPlayingUpdatesParameters.MediaItemAttributes)) {
            IAP2Params mediaItemAttributesParams = new IAP2Params(params.getBlob(NowPlayingUpdatesParameters.MediaItemAttributes), 0);
            if (mediaItemAttributesParams.hasParam(MediaItemAttributes.MediaItemPersistentIdentifier.getParam())) {
                nowPlayingUpdate.mediaItemPersistentIdentifier = mediaItemAttributesParams.getUInt64(MediaItemAttributes.MediaItemPersistentIdentifier.getParam());
            }
            if (mediaItemAttributesParams.hasParam(MediaItemAttributes.MediaItemTitle.getParam())) {
                nowPlayingUpdate.mediaItemTitle = mediaItemAttributesParams.getUTF8(MediaItemAttributes.MediaItemTitle.getParam());
            }
            if (mediaItemAttributesParams.hasParam(MediaItemAttributes.MediaItemAlbumTrackCount.getParam())) {
                nowPlayingUpdate.mediaItemAlbumTrackCount = mediaItemAttributesParams.getUInt16(MediaItemAttributes.MediaItemAlbumTrackCount.getParam());
            }
            if (mediaItemAttributesParams.hasParam(MediaItemAttributes.MediaItemAlbumTrackNumber.getParam())) {
                nowPlayingUpdate.mediaItemAlbumTrackNumber = mediaItemAttributesParams.getUInt16(MediaItemAttributes.MediaItemAlbumTrackNumber.getParam());
            }
            if (mediaItemAttributesParams.hasParam(MediaItemAttributes.MediaItemArtist.getParam())) {
                nowPlayingUpdate.mediaItemArtist = mediaItemAttributesParams.getUTF8(MediaItemAttributes.MediaItemArtist.getParam());
            }
            if (mediaItemAttributesParams.hasParam(MediaItemAttributes.MediaItemAlbumTitle.getParam())) {
                nowPlayingUpdate.mediaItemAlbumTitle = mediaItemAttributesParams.getUTF8(MediaItemAttributes.MediaItemAlbumTitle.getParam());
            }
            if (mediaItemAttributesParams.hasParam(MediaItemAttributes.MediaItemArtworkFileTransferIdentifier.getParam())) {
                nowPlayingUpdate.mediaItemArtworkFileTransferIdentifier = mediaItemAttributesParams.getUInt8(MediaItemAttributes.MediaItemArtworkFileTransferIdentifier.getParam());
            }
            if (mediaItemAttributesParams.hasParam(MediaItemAttributes.MediaItemGenre.getParam())) {
                nowPlayingUpdate.mediaItemGenre = mediaItemAttributesParams.getUTF8(MediaItemAttributes.MediaItemGenre.getParam());
            }
            if (mediaItemAttributesParams.hasParam(MediaItemAttributes.MediaItemPlaybackDurationInMilliSeconds.getParam())) {
                nowPlayingUpdate.mediaItemPlaybackDurationInMilliseconds = mediaItemAttributesParams.getUInt32(MediaItemAttributes.MediaItemPlaybackDurationInMilliSeconds.getParam());
            }
        }
        if (params.hasParam(NowPlayingUpdatesParameters.PlaybackAttributes)) {
            IAP2Params playbackAttributesParams = new IAP2Params(params.getBlob(NowPlayingUpdatesParameters.PlaybackAttributes), 0);
            if (playbackAttributesParams.hasParam(PlaybackAttributes.PlaybackStatus.getParam())) {
                nowPlayingUpdate.mPlaybackStatus = (PlaybackStatus) playbackAttributesParams.getEnum(PlaybackStatus.class, PlaybackAttributes.PlaybackStatus.getParam());
            }
            if (playbackAttributesParams.hasParam(PlaybackAttributes.PlaybackElapsedTimeInMilliseconds.getParam())) {
                nowPlayingUpdate.mPlaybackElapsedTimeMilliseconds = playbackAttributesParams.getUInt32(PlaybackAttributes.PlaybackElapsedTimeInMilliseconds.getParam());
            }
            if (playbackAttributesParams.hasParam(PlaybackAttributes.PlaybackAppName.getParam())) {
                nowPlayingUpdate.mAppName = playbackAttributesParams.getUTF8(PlaybackAttributes.PlaybackAppName.getParam());
            }
            if (playbackAttributesParams.hasParam(PlaybackAttributes.PlaybackAppBundleId.getParam())) {
                nowPlayingUpdate.mAppBundleId = playbackAttributesParams.getUTF8(PlaybackAttributes.PlaybackAppBundleId.getParam());
            }
            if (playbackAttributesParams.hasParam(PlaybackAttributes.PlaybackShuffleMode.getParam())) {
                nowPlayingUpdate.playbackShuffle = (PlaybackShuffle) playbackAttributesParams.getEnum(PlaybackShuffle.class, PlaybackAttributes.PlaybackShuffleMode.getParam());
            }
            if (playbackAttributesParams.hasParam(PlaybackAttributes.PlaybackRepeatMode.getParam())) {
                nowPlayingUpdate.playbackRepeat = (PlaybackRepeat) playbackAttributesParams.getEnum(PlaybackRepeat.class, PlaybackAttributes.PlaybackRepeatMode.getParam());
            }
        }
        return nowPlayingUpdate;
    }

    private void mergeNowPlayingUpdate(NowPlayingUpdate incomingUpdate) {
        if (incomingUpdate.mediaItemTitle != null) {
            this.aggregatedNowPlayingUpdate.mediaItemTitle = incomingUpdate.mediaItemTitle;
        }
        if (incomingUpdate.mediaItemAlbumTitle != null) {
            this.aggregatedNowPlayingUpdate.mediaItemAlbumTitle = incomingUpdate.mediaItemAlbumTitle;
        }
        if (incomingUpdate.mediaItemArtist != null) {
            this.aggregatedNowPlayingUpdate.mediaItemArtist = incomingUpdate.mediaItemArtist;
        }
        if (incomingUpdate.mediaItemGenre != null) {
            this.aggregatedNowPlayingUpdate.mediaItemGenre = incomingUpdate.mediaItemGenre;
        }
        if (incomingUpdate.mPlaybackStatus != null) {
            this.aggregatedNowPlayingUpdate.mPlaybackStatus = incomingUpdate.mPlaybackStatus;
        }
        if (incomingUpdate.playbackShuffle != null) {
            this.aggregatedNowPlayingUpdate.playbackShuffle = incomingUpdate.playbackShuffle;
        }
        if (incomingUpdate.playbackRepeat != null) {
            this.aggregatedNowPlayingUpdate.playbackRepeat = incomingUpdate.playbackRepeat;
        }
        if (incomingUpdate.mediaItemPersistentIdentifier != null) {
            this.aggregatedNowPlayingUpdate.mediaItemPersistentIdentifier = incomingUpdate.mediaItemPersistentIdentifier;
        }
        if (incomingUpdate.mediaItemPlaybackDurationInMilliseconds > 0) {
            this.aggregatedNowPlayingUpdate.mediaItemPlaybackDurationInMilliseconds = incomingUpdate.mediaItemPlaybackDurationInMilliseconds;
        }
        if (incomingUpdate.mediaItemAlbumTrackNumber >= 0) {
            this.aggregatedNowPlayingUpdate.mediaItemAlbumTrackNumber = incomingUpdate.mediaItemAlbumTrackNumber;
        }
        if (incomingUpdate.mediaItemAlbumTrackCount >= 0) {
            this.aggregatedNowPlayingUpdate.mediaItemAlbumTrackCount = incomingUpdate.mediaItemAlbumTrackCount;
        }
        if (incomingUpdate.mPlaybackElapsedTimeMilliseconds >= 0) {
            this.aggregatedNowPlayingUpdate.mPlaybackElapsedTimeMilliseconds = incomingUpdate.mPlaybackElapsedTimeMilliseconds;
        }
        if (!TextUtils.isEmpty(incomingUpdate.mAppName)) {
            this.aggregatedNowPlayingUpdate.mAppName = incomingUpdate.mAppName;
        }
        if (!TextUtils.isEmpty(incomingUpdate.mAppBundleId)) {
            this.aggregatedNowPlayingUpdate.mAppBundleId = incomingUpdate.mAppBundleId;
        }
        if (incomingUpdate.mediaItemArtworkFileTransferIdentifier >= 128 && incomingUpdate.mediaItemArtworkFileTransferIdentifier <= 255) {
            this.aggregatedNowPlayingUpdate.mediaItemArtworkFileTransferIdentifier = incomingUpdate.mediaItemArtworkFileTransferIdentifier;
        }
    }

    public void onKeyDown(MediaKey key) {
        this.miAPProcessor.onKeyDown((Enum) key);
    }

    public void onKeyUp(MediaKey key) {
        this.miAPProcessor.onKeyUp((Enum) key);
    }

    public void onKeyDown(int key) {
        this.miAPProcessor.onKeyDown(key);
    }

    public void onKeyUp(int key) {
        this.miAPProcessor.onKeyUp(key);
    }
}
