package com.navdy.hud.mfi;

import android.util.Log;
import com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class IAPFileTransferManager implements IIAPFileTransferManager {
    public static final int DEFAULT_FILE_TRANSFER_LIMIT = 1048576;
    private static final int FILE_TRANSFER_IDENTIFIER_INDEX = 0;
    private static final int MAX_SESSIONS = 3;
    private static final String TAG = IAPFileTransferManager.class.getSimpleName();
    private volatile int expectedFileTransferIdentifier = -1;
    private long mFileTransferLimit = BluetoothPbapClient.VCARD_ATTR_URL;
    private IAPFileTransferListener mFileTransferListener;
    private iAPProcessor mIapProcessor;
    private ExecutorService mSerialExecutor = Executors.newSingleThreadExecutor();
    private HashMap<Integer, FileTransferSession> mSessionCache;

    public IAPFileTransferManager(iAPProcessor processor) {
        this.mIapProcessor = processor;
        this.mSessionCache = new HashMap();
    }

    public void setFileTransferListener(IAPFileTransferListener mFileTransferListener) {
        this.mFileTransferListener = mFileTransferListener;
    }

    public void queue(final byte[] data) {
        this.mSerialExecutor.submit(new Runnable() {
            public void run() {
                if (data != null) {
                    int fileTransferIdentifier = data[0] & 255;
                    FileTransferSession transferSession = (FileTransferSession) IAPFileTransferManager.this.mSessionCache.get(Integer.valueOf(fileTransferIdentifier));
                    if (transferSession == null) {
                        Log.d(IAPFileTransferManager.TAG, "Creating a new file transfer session " + fileTransferIdentifier);
                        transferSession = new FileTransferSession(fileTransferIdentifier, IAPFileTransferManager.this);
                        IAPFileTransferManager.this.mSessionCache.put(Integer.valueOf(fileTransferIdentifier), transferSession);
                    }
                    transferSession.bProcessFileTransferMessage(data);
                }
            }
        });
    }

    public void onSuccess(int fileTransferIdentifier, byte[] data) {
        if (this.mFileTransferListener != null) {
            Log.d(TAG, "File successfully transferred " + fileTransferIdentifier);
            this.mFileTransferListener.onFileReceived(fileTransferIdentifier, data);
        }
        this.mSessionCache.remove(Integer.valueOf(fileTransferIdentifier));
    }

    public void onPaused(int fileTransferIdentifier) {
        Log.d(TAG, "File transfer paused");
    }

    public void onCanceled(int fileTransferIdentifier) {
        Log.d(TAG, "File transfer was cancelled " + fileTransferIdentifier);
        this.mSessionCache.remove(Integer.valueOf(fileTransferIdentifier));
        if (this.mFileTransferListener != null) {
            this.mFileTransferListener.onFileTransferCancel(fileTransferIdentifier);
        }
    }

    public void sendMessage(int fileTransferIdentifier, byte[] message) {
        this.mIapProcessor.sendToDevice(2, message);
    }

    public void onError(int fileTransferIdentifier, Throwable t) {
        Log.d(TAG, "Error in file transfer " + t);
        this.mSessionCache.remove(Integer.valueOf(fileTransferIdentifier));
    }

    public void clear() {
        this.mSessionCache.clear();
    }

    public void setFileTransferLimit(int limit) {
        this.mFileTransferLimit = (long) limit;
    }

    public long getFileTransferLimit() {
        return this.mFileTransferLimit;
    }

    public void onFileTransferSetupRequest(int identifier, long size) {
        if (this.mFileTransferListener != null) {
            this.mFileTransferListener.onFileTransferSetup(identifier, size);
        }
    }

    public void onFileTransferSetupResponse(int identifier, boolean proceed) {
        FileTransferSession transferSession = (FileTransferSession) this.mSessionCache.get(Integer.valueOf(identifier));
        if (transferSession == null) {
            return;
        }
        if (proceed) {
            transferSession.proceed();
        } else {
            transferSession.cancel();
        }
    }

    public void cancel() {
        this.mSerialExecutor.submit(new Runnable() {
            public void run() {
                for (FileTransferSession session : IAPFileTransferManager.this.mSessionCache.values()) {
                    if (session != null) {
                        if (session.isActive()) {
                            Log.d(IAPFileTransferManager.TAG, "Canceling, File transfer session , ID : " + session.mFileTransferIdentifier);
                            session.cancel();
                            if (IAPFileTransferManager.this.mFileTransferListener != null) {
                                IAPFileTransferManager.this.mFileTransferListener.onFileTransferCancel(session.mFileTransferIdentifier);
                            }
                        } else {
                            Log.d(IAPFileTransferManager.TAG, "File transfer session not canceled , ID : " + session.mFileTransferIdentifier + ", Not active");
                        }
                    }
                }
            }
        });
    }
}
