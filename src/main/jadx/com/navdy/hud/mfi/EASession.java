package com.navdy.hud.mfi;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import okio.Buffer;

public class EASession {
    private static final String TAG = EASession.class.getSimpleName();
    private Buffer buffer;
    private boolean closed;
    private final Object dataAvailable = new Object();
    private iAPProcessor iAPProcessor;
    private String macAddress;
    private EASession mySelf = this;
    private String name;
    private String protocol;
    private int sessionIdentifier;

    class BlockingInputStream extends InputStream {
        private InputStream source;

        public BlockingInputStream(InputStream source) {
            this.source = source;
        }

        public int read() throws IOException {
            int read;
            synchronized (EASession.this.dataAvailable) {
                waitForAvailable();
                read = this.source.read();
            }
            return read;
        }

        public int read(byte[] buffer) throws IOException {
            int read;
            synchronized (EASession.this.dataAvailable) {
                waitForAvailable();
                read = this.source.read(buffer);
            }
            return read;
        }

        public int read(byte[] buffer, int byteOffset, int byteCount) throws IOException {
            int read;
            synchronized (EASession.this.dataAvailable) {
                waitForAvailable();
                read = this.source.read(buffer, byteOffset, byteCount);
            }
            return read;
        }

        private void waitForAvailable() throws IOException {
            if (this.source.available() == 0) {
                try {
                    EASession.this.dataAvailable.wait();
                } catch (InterruptedException e) {
                }
            }
        }
    }

    class OutStream extends OutputStream {
        private int FLUSH_DELAY = 250;
        private Runnable autoFlush = new Runnable() {
            public void run() {
                try {
                    OutStream.this.flush();
                } catch (IOException e) {
                }
            }
        };
        private List<byte[]> frames = new ArrayList();
        private Handler handler = new Handler(Looper.getMainLooper());

        OutStream() {
        }

        public void close() throws IOException {
            flush();
        }

        public synchronized void flush() throws IOException {
            this.handler.removeCallbacks(this.autoFlush);
            byte[] packet = assembleFrames();
            if (!(EASession.this.closed || packet == null)) {
                EASession.this.iAPProcessor.queue(new EASessionPacket(EASession.this.getSessionIdentifier(), packet));
            }
        }

        private byte[] assembleFrames() {
            int i;
            byte[] buffer = null;
            int totalLength = 0;
            for (i = 0; i < this.frames.size(); i++) {
                totalLength += ((byte[]) this.frames.get(i)).length;
            }
            if (totalLength > 0) {
                if (this.frames.size() == 1) {
                    buffer = (byte[]) this.frames.get(0);
                } else {
                    int offset = 0;
                    buffer = new byte[totalLength];
                    for (i = 0; i < this.frames.size(); i++) {
                        byte[] src = (byte[]) this.frames.get(i);
                        System.arraycopy(src, 0, buffer, offset, src.length);
                        offset += src.length;
                    }
                }
                this.frames.clear();
            }
            return buffer;
        }

        public synchronized void write(byte[] buffer) throws IOException {
            if (Log.isLoggable(EASession.TAG, 3)) {
                Log.d(EASession.TAG, String.format("%s: OutStream.write: %d bytes: %s, '%s'", new Object[]{EASession.this.mySelf, Integer.valueOf(buffer.length), Utils.bytesToHex(buffer, true), Utils.toASCII(buffer)}));
            }
            this.frames.add(buffer);
            this.handler.removeCallbacks(this.autoFlush);
            this.handler.postDelayed(this.autoFlush, (long) this.FLUSH_DELAY);
        }

        public void write(byte[] buffer, int offset, int count) throws IOException {
            write(Arrays.copyOfRange(buffer, offset, count));
        }

        public void write(int oneByte) throws IOException {
            write(new byte[]{(byte) oneByte});
        }
    }

    public String toString() {
        return String.format("%s{%s/%d}", new Object[]{super.toString(), this.protocol, Integer.valueOf(this.sessionIdentifier)});
    }

    public EASession(iAPProcessor iAPProcessor, String macAddress, String name, int sessionIdentifier, String protocol) {
        this.iAPProcessor = iAPProcessor;
        this.macAddress = macAddress;
        this.name = name;
        this.sessionIdentifier = sessionIdentifier;
        this.protocol = protocol;
        if (Log.isLoggable(TAG, 3)) {
            Log.d(TAG, String.format("%s: created", new Object[]{this.mySelf}));
        }
        this.buffer = new Buffer();
    }

    public String getMacAddress() {
        return this.macAddress;
    }

    public String getName() {
        return this.name;
    }

    public int getSessionIdentifier() {
        return this.sessionIdentifier;
    }

    public String getProtocol() {
        return this.protocol;
    }

    public void close() {
        Log.d(TAG, "Closing EASession streams");
        synchronized (this.dataAvailable) {
            this.closed = true;
            this.buffer.close();
            this.dataAvailable.notify();
        }
    }

    public void queue(byte[] data) {
        if (Log.isLoggable(TAG, 3)) {
            Log.d(TAG, String.format("%s: buffer.write: %d bytes: %s, '%s'", new Object[]{this.mySelf, Integer.valueOf(data.length), Utils.bytesToHex(data, true), Utils.toASCII(data)}));
        }
        synchronized (this.dataAvailable) {
            this.buffer.write(data);
            this.dataAvailable.notify();
        }
    }

    public InputStream getInputStream() {
        return new BlockingInputStream(this.buffer.inputStream());
    }

    public OutputStream getOutputStream() {
        return new OutStream();
    }
}
