package com.navdy.hud.mfi;

import android.util.Log;
import java.io.IOException;

public class FileTransferSession {
    public static final int DATA_OFFSET = 2;
    private static final int INDEX_OF_COMMAND = 1;
    private static final int INDEX_OF_ID = 0;
    public static final int MESSAGE_SIZE = 2;
    private static final String TAG = FileTransferSession.class.getSimpleName();
    private boolean mCanceled = false;
    private byte[] mFileData;
    private int mFileReceivedSoFar;
    private long mFileSize;
    public int mFileTransferIdentifier;
    IIAPFileTransferManager mFileTransferManager;
    private boolean mFinished = false;
    private boolean mStarted = false;

    enum FileTransferCommand {
        COMMAND_DATA(0),
        COMMAND_START(1),
        COMMAND_CANCEL(2),
        COMMAND_PAUSE(3),
        COMMAND_SETUP(4),
        COMMAND_SUCCESS(5),
        COMMAND_FAILURE(6),
        COMMAND_LAST_DATA(64),
        COMMAND_FIRST_DATA(128),
        COMMAND_FIRST_AND_ONLY_DATA(192);
        
        public final int value;

        private FileTransferCommand(int value) {
            this.value = value;
        }

        public static FileTransferCommand fromInt(int val) {
            if (val >= COMMAND_DATA.value && val <= COMMAND_FAILURE.value) {
                return values()[val];
            }
            if (val == COMMAND_LAST_DATA.value) {
                return COMMAND_LAST_DATA;
            }
            if (val == COMMAND_FIRST_DATA.value) {
                return COMMAND_FIRST_DATA;
            }
            if (val == COMMAND_FIRST_AND_ONLY_DATA.value) {
                return COMMAND_FIRST_AND_ONLY_DATA;
            }
            return null;
        }
    }

    public FileTransferSession(int mFileTransferIdentifier, IIAPFileTransferManager fileTransferManager) {
        this.mFileTransferManager = fileTransferManager;
        this.mFileTransferIdentifier = mFileTransferIdentifier;
    }

    public void bProcessFileTransferMessage(byte[] data) {
        FileTransferCommand command = FileTransferCommand.fromInt(data[1] & 255);
        if (command == FileTransferCommand.COMMAND_SETUP || this.mStarted) {
            switch (command) {
                case COMMAND_SETUP:
                    this.mStarted = true;
                    this.mFileSize = Utils.unpackInt64(data, 2);
                    Log.d(Utils.MFI_TAG, "Apple Device(F): " + this.mFileTransferIdentifier + " Command : " + command.name() + ", File_Size : " + this.mFileSize);
                    if (this.mFileSize > this.mFileTransferManager.getFileTransferLimit()) {
                        Log.e(TAG, "File too large, not starting the transfer");
                        sendCommand(FileTransferCommand.COMMAND_CANCEL);
                        return;
                    }
                    this.mFileTransferManager.onFileTransferSetupRequest(this.mFileTransferIdentifier, this.mFileSize);
                    if (this.mFileSize == 0) {
                        sendCommand(FileTransferCommand.COMMAND_CANCEL);
                        this.mFileTransferManager.onSuccess(this.mFileTransferIdentifier, null);
                        return;
                    }
                    return;
                case COMMAND_FIRST_DATA:
                    try {
                        System.arraycopy(data, 2, this.mFileData, this.mFileReceivedSoFar, data.length - 2);
                        this.mFileReceivedSoFar += data.length - 2;
                        Log.d(Utils.MFI_TAG, "Apple Device(F): " + this.mFileTransferIdentifier + " Command : " + command.name() + ", Received : " + this.mFileReceivedSoFar);
                        return;
                    } catch (Throwable t) {
                        error(t);
                        return;
                    }
                case COMMAND_FIRST_AND_ONLY_DATA:
                    try {
                        System.arraycopy(data, 2, this.mFileData, this.mFileReceivedSoFar, data.length - 2);
                        this.mFileReceivedSoFar += data.length - 2;
                        Log.d(Utils.MFI_TAG, "Apple Device(F): " + this.mFileTransferIdentifier + " Command : " + command.name() + ", Received : " + this.mFileReceivedSoFar);
                        sendCommand(FileTransferCommand.COMMAND_SUCCESS);
                        this.mFinished = true;
                        this.mFileTransferManager.onSuccess(this.mFileTransferIdentifier, this.mFileData);
                        return;
                    } catch (Throwable t2) {
                        error(t2);
                        return;
                    }
                case COMMAND_DATA:
                    try {
                        System.arraycopy(data, 2, this.mFileData, this.mFileReceivedSoFar, data.length - 2);
                        this.mFileReceivedSoFar += data.length - 2;
                        Log.d(Utils.MFI_TAG, "Apple Device(F): " + this.mFileTransferIdentifier + " Command : " + command.name() + ", Received : " + this.mFileReceivedSoFar);
                        return;
                    } catch (Throwable t22) {
                        error(t22);
                        return;
                    }
                case COMMAND_LAST_DATA:
                    try {
                        System.arraycopy(data, 2, this.mFileData, this.mFileReceivedSoFar, data.length - 2);
                        this.mFileReceivedSoFar += data.length - 2;
                        Log.d(Utils.MFI_TAG, "Apple Device(F): " + this.mFileTransferIdentifier + " Command : " + command.name() + ", Received : " + this.mFileReceivedSoFar);
                        if (((long) this.mFileReceivedSoFar) != this.mFileSize) {
                            sendCommand(FileTransferCommand.COMMAND_FAILURE);
                            this.mFinished = true;
                            this.mFileTransferManager.onError(this.mFileTransferIdentifier, new IOException("Missed packets"));
                            return;
                        }
                        sendCommand(FileTransferCommand.COMMAND_SUCCESS);
                        this.mFinished = true;
                        this.mFileTransferManager.onSuccess(this.mFileTransferIdentifier, this.mFileData);
                        return;
                    } catch (Throwable t222) {
                        error(t222);
                        return;
                    }
                case COMMAND_CANCEL:
                    this.mFinished = true;
                    Log.d(Utils.MFI_TAG, "Apple Device(F): " + this.mFileTransferIdentifier + " Command : " + command.name());
                    this.mFileTransferManager.onCanceled(this.mFileTransferIdentifier);
                    return;
                case COMMAND_PAUSE:
                    Log.d(Utils.MFI_TAG, "Apple Device(F): " + this.mFileTransferIdentifier + " Command : " + command.name());
                    this.mFileTransferManager.onPaused(this.mFileTransferIdentifier);
                    return;
                default:
                    return;
            }
        }
        Log.e(TAG, "File transfer is not initiated, bad state " + command);
        this.mFileTransferManager.onError(this.mFileTransferIdentifier, new IllegalStateException("File transfer not started"));
    }

    private void error(Throwable t) {
        this.mFinished = true;
        sendCommand(FileTransferCommand.COMMAND_FAILURE);
        this.mFileTransferManager.onError(this.mFileTransferIdentifier, t);
    }

    private void sendCommand(FileTransferCommand command) {
        this.mFileTransferManager.sendMessage(this.mFileTransferIdentifier, new byte[]{(byte) (this.mFileTransferIdentifier & 255), (byte) command.value});
        Log.d(Utils.MFI_TAG, "Accessory(F): " + this.mFileTransferIdentifier + " Command : " + command.name());
    }

    public void release() {
    }

    public synchronized void cancel() {
        this.mCanceled = true;
        if (this.mFileTransferManager != null) {
            sendCommand(FileTransferCommand.COMMAND_CANCEL);
            this.mFinished = true;
        }
    }

    public synchronized void proceed() {
        if (!this.mCanceled) {
            this.mFileData = new byte[((int) this.mFileSize)];
            if (this.mFileTransferManager != null) {
                sendCommand(FileTransferCommand.COMMAND_START);
            }
        }
    }

    public boolean isActive() {
        return this.mStarted && !this.mFinished;
    }
}
