package com.navdy.hud.app.obd;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import com.navdy.hud.app.common.ServiceReconnector;
import com.navdy.obd.ICarService;
import com.navdy.obd.ICarService.Stub;
import com.navdy.obd.ObdServiceInterface;

public class CarServiceConnector extends ServiceReconnector {
    private ICarService carApi;

    public CarServiceConnector(Context context) {
        super(context, new Intent(ObdServiceInterface.ACTION_START_AUTO_CONNECT), ICarService.class.getName());
    }

    protected void onConnected(ComponentName name, IBinder service) {
        this.carApi = Stub.asInterface(service);
        ObdManager.getInstance().serviceConnected();
    }

    protected void onDisconnected(ComponentName name) {
        this.carApi = null;
        ObdManager.getInstance().serviceDisconnected();
    }

    public ICarService getCarApi() {
        return this.carApi;
    }
}
