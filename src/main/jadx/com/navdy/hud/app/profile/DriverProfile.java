package com.navdy.hud.app.profile;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.hud.app.ui.component.UISettings;
import com.navdy.service.library.events.MessageStore;
import com.navdy.service.library.events.NavdyEventUtil;
import com.navdy.service.library.events.NavdyEventUtil.Initializer;
import com.navdy.service.library.events.glances.CannedMessagesUpdate;
import com.navdy.service.library.events.preferences.DisplaySpeakerPreferences;
import com.navdy.service.library.events.preferences.DriverProfilePreferences;
import com.navdy.service.library.events.preferences.DriverProfilePreferences.DialLongPressAction;
import com.navdy.service.library.events.preferences.DriverProfilePreferences.DisplayFormat;
import com.navdy.service.library.events.preferences.DriverProfilePreferences.FeatureMode;
import com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting;
import com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem;
import com.navdy.service.library.events.preferences.InputPreferences;
import com.navdy.service.library.events.preferences.LocalPreferences;
import com.navdy.service.library.events.preferences.NavigationPreferences;
import com.navdy.service.library.events.preferences.NotificationPreferences;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.squareup.wire.Message;
import com.squareup.wire.Message.Builder;
import com.squareup.wire.Wire;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

public class DriverProfile {
    private static final String CANNED_MESSAGES = "CannedMessages";
    private static final String CONTACTS_IMAGE_CACHE_DIR = "contacts";
    public static final String DRIVER_PROFILE_IMAGE = "DriverImage";
    private static final String DRIVER_PROFILE_PREFERENCES = "DriverProfilePreferences";
    private static final String INPUT_PREFERENCES = "InputPreferences";
    private static final String LOCALE_SEPARATOR = "_";
    private static final String LOCAL_PREFERENCES = "LocalPreferences";
    private static final String MUSIC_IMAGE_CACHE_DIR = "music";
    private static final String NAVIGATION_PREFERENCES = "NavigationPreferences";
    private static final String NOTIFICATION_PREFERENCES = "NotificationPreferences";
    private static final String PLACES_IMAGE_CACHE_DIR = "places";
    private static final String PREFERENCES_DIRECTORY = "Preferences";
    private static final String SPEAKER_PREFERENCES = "SpeakerPreferences";
    private static final Logger sLogger = new Logger(DriverProfile.class);
    CannedMessagesUpdate mCannedMessages;
    private File mContactsImageDirectory;
    private Bitmap mDriverImage;
    DriverProfilePreferences mDriverProfilePreferences;
    InputPreferences mInputPreferences;
    private LocalPreferences mLocalPreferences;
    private Locale mLocale;
    private MessageStore mMessageStore = new MessageStore(this.mPreferencesDirectory);
    private File mMusicImageDirectory;
    NavigationPreferences mNavigationPreferences;
    NotificationPreferences mNotificationPreferences;
    private NotificationSettings mNotificationSettings;
    private File mPlacesImageDirectory;
    private File mPreferencesDirectory;
    private File mProfileDirectory;
    private String mProfileName;
    private DisplaySpeakerPreferences mSpeakerPreferences;
    private boolean mTrafficEnabled;

    static DriverProfile createProfileForId(String profileName, String profileDirectoryName) throws IOException {
        if (profileName.contains(File.separator) || profileName.startsWith(GlanceConstants.PERIOD)) {
            throw new IllegalArgumentException("Profile names can't refer to directories");
        }
        String profilePath = profileDirectoryName + File.separator + profileName;
        File profileDirectory = new File(profilePath);
        if (profileDirectory.mkdir() || profileDirectory.isDirectory()) {
            File preferencesDirectory = new File(profilePath + File.separator + PREFERENCES_DIRECTORY);
            if (preferencesDirectory.mkdir() || preferencesDirectory.isDirectory()) {
                return new DriverProfile(profileDirectory);
            }
            throw new IOException("could not create preferences directory");
        }
        throw new IOException("could not create profile");
    }

    protected DriverProfile(File profileDirectory) throws IOException {
        Throwable th;
        this.mProfileName = profileDirectory.getName();
        this.mProfileDirectory = profileDirectory;
        this.mPreferencesDirectory = new File(profileDirectory, PREFERENCES_DIRECTORY);
        this.mPlacesImageDirectory = new File(profileDirectory, PLACES_IMAGE_CACHE_DIR);
        IOUtils.createDirectory(this.mPlacesImageDirectory);
        this.mContactsImageDirectory = new File(profileDirectory, CONTACTS_IMAGE_CACHE_DIR);
        IOUtils.createDirectory(this.mContactsImageDirectory);
        this.mMusicImageDirectory = new File(profileDirectory, "music");
        IOUtils.createDirectory(this.mMusicImageDirectory);
        this.mDriverProfilePreferences = (DriverProfilePreferences) readPreference(DRIVER_PROFILE_PREFERENCES, DriverProfilePreferences.class, new Initializer<DriverProfilePreferences>() {
            public DriverProfilePreferences build(Builder<DriverProfilePreferences> builder) {
                return ((DriverProfilePreferences.Builder) builder).device_name("Phone " + DriverProfile.this.mProfileDirectory.getName()).build();
            }
        });
        this.mNavigationPreferences = (NavigationPreferences) readPreference(NAVIGATION_PREFERENCES, NavigationPreferences.class);
        setTraffic();
        this.mInputPreferences = (InputPreferences) readPreference(INPUT_PREFERENCES, InputPreferences.class);
        this.mSpeakerPreferences = (DisplaySpeakerPreferences) readPreference(SPEAKER_PREFERENCES, DisplaySpeakerPreferences.class);
        this.mNotificationPreferences = (NotificationPreferences) readPreference(NOTIFICATION_PREFERENCES, NotificationPreferences.class);
        this.mNotificationSettings = new NotificationSettings(this.mNotificationPreferences);
        this.mLocalPreferences = (LocalPreferences) readPreference(LOCAL_PREFERENCES, LocalPreferences.class);
        this.mCannedMessages = (CannedMessagesUpdate) readPreference(CANNED_MESSAGES, CannedMessagesUpdate.class);
        InputStream driverImageStream = null;
        try {
            InputStream driverImageStream2 = new FileInputStream(new File(this.mPreferencesDirectory, DRIVER_PROFILE_IMAGE));
            try {
                this.mDriverImage = BitmapFactory.decodeStream(driverImageStream2);
                IOUtils.closeStream(driverImageStream2);
                driverImageStream = driverImageStream2;
            } catch (FileNotFoundException e) {
                driverImageStream = driverImageStream2;
                try {
                    this.mDriverImage = null;
                    IOUtils.closeStream(driverImageStream);
                    setLocale();
                } catch (Throwable th2) {
                    th = th2;
                    IOUtils.closeStream(driverImageStream);
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                driverImageStream = driverImageStream2;
                IOUtils.closeStream(driverImageStream);
                throw th;
            }
        } catch (FileNotFoundException e2) {
            this.mDriverImage = null;
            IOUtils.closeStream(driverImageStream);
            setLocale();
        }
        setLocale();
    }

    private <T extends Message> T readPreference(String filename, Class<T> type) throws IOException {
        return this.mMessageStore.readMessage(filename, type);
    }

    private <T extends Message> T readPreference(String filename, Class<T> type, Initializer<T> initializer) throws IOException {
        return this.mMessageStore.readMessage(filename, type, initializer);
    }

    void setDriverProfilePreferences(DriverProfilePreferences preferences) {
        this.mDriverProfilePreferences = (DriverProfilePreferences) removeNulls(preferences);
        setLocale();
        sLogger.i("[" + this.mProfileName + "] updating driver profile preferences to ver[" + preferences.serial_number + "] " + this.mDriverProfilePreferences);
        writeMessageToFile(this.mDriverProfilePreferences, DRIVER_PROFILE_PREFERENCES);
    }

    public NavigationPreferences getNavigationPreferences() {
        return this.mNavigationPreferences;
    }

    public InputPreferences getInputPreferences() {
        return this.mInputPreferences;
    }

    public NotificationPreferences getNotificationPreferences() {
        return this.mNotificationPreferences;
    }

    public NotificationSettings getNotificationSettings() {
        return this.mNotificationSettings;
    }

    public DisplaySpeakerPreferences getSpeakerPreferences() {
        return this.mSpeakerPreferences;
    }

    public LocalPreferences getLocalPreferences() {
        return this.mLocalPreferences;
    }

    public CannedMessagesUpdate getCannedMessages() {
        return this.mCannedMessages;
    }

    void setNavigationPreferences(NavigationPreferences preferences) {
        this.mNavigationPreferences = (NavigationPreferences) removeNulls(preferences);
        sLogger.i("[" + this.mProfileName + "] updating nav preferences to ver[" + preferences.serial_number + "] " + this.mNavigationPreferences);
        writeMessageToFile(this.mNavigationPreferences, NAVIGATION_PREFERENCES);
        setTraffic();
    }

    void setInputPreferences(InputPreferences preferences) {
        this.mInputPreferences = (InputPreferences) removeNulls(preferences);
        sLogger.i("[" + this.mProfileName + "] updating input preferences to ver[" + preferences.serial_number + "] " + this.mInputPreferences);
        writeMessageToFile(this.mInputPreferences, INPUT_PREFERENCES);
    }

    void setSpeakerPreferences(DisplaySpeakerPreferences preferences) {
        this.mSpeakerPreferences = (DisplaySpeakerPreferences) removeNulls(preferences);
        sLogger.i("[" + this.mProfileName + "] updating speaker preferences to ver[" + preferences.serial_number + "] " + this.mSpeakerPreferences);
        writeMessageToFile(this.mSpeakerPreferences, SPEAKER_PREFERENCES);
    }

    void setNotificationPreferences(NotificationPreferences preferences) {
        this.mNotificationPreferences = (NotificationPreferences) removeNulls(preferences);
        sLogger.i("[" + this.mProfileName + "] updating notification preferences to ver[" + preferences.serial_number + "] " + this.mNotificationPreferences);
        writeMessageToFile(this.mNotificationPreferences, NOTIFICATION_PREFERENCES);
        this.mNotificationSettings.update(this.mNotificationPreferences);
    }

    public void setLocalPreferences(LocalPreferences preferences) {
        this.mLocalPreferences = (LocalPreferences) removeNulls(preferences);
        sLogger.i("[" + this.mProfileName + "] updating local preferences to " + this.mLocalPreferences);
        writeMessageToFile(this.mLocalPreferences, LOCAL_PREFERENCES);
    }

    void setCannedMessages(CannedMessagesUpdate cannedMessages) {
        this.mCannedMessages = (CannedMessagesUpdate) removeNulls(cannedMessages);
        sLogger.i("[" + this.mProfileName + "] updating canned messages preferences to ver[" + cannedMessages.serial_number + "]");
        writeMessageToFile(this.mCannedMessages, CANNED_MESSAGES);
    }

    private void writeMessageToFile(Message message, String fileName) {
        this.mMessageStore.writeMessage(message, fileName);
    }

    private <T extends Message> T removeNulls(T message) {
        return NavdyEventUtil.applyDefaults(message);
    }

    public String getDriverName() {
        return this.mDriverProfilePreferences.driver_name;
    }

    public String getFirstName() {
        String driverName = this.mDriverProfilePreferences.driver_name;
        if (TextUtils.isEmpty(driverName)) {
            return driverName;
        }
        driverName = driverName.trim();
        int firstWordEnd = driverName.indexOf(" ");
        return firstWordEnd != -1 ? driverName.substring(0, firstWordEnd) : driverName;
    }

    public String getDriverEmail() {
        return this.mDriverProfilePreferences.driver_email;
    }

    public String getDeviceName() {
        return this.mDriverProfilePreferences.device_name;
    }

    public String getCarMake() {
        return this.mDriverProfilePreferences.car_make;
    }

    public String getCarModel() {
        return this.mDriverProfilePreferences.car_model;
    }

    public String getCarYear() {
        return this.mDriverProfilePreferences.car_year;
    }

    public DisplayFormat getDisplayFormat() {
        return this.mDriverProfilePreferences.display_format;
    }

    public DialLongPressAction getLongPressAction() {
        if (UISettings.isLongPressActionPlaceSearch()) {
            return DialLongPressAction.DIAL_LONG_PRESS_PLACE_SEARCH;
        }
        return this.mDriverProfilePreferences.dial_long_press_action;
    }

    public FeatureMode getFeatureMode() {
        return this.mDriverProfilePreferences.feature_mode;
    }

    public UnitSystem getUnitSystem() {
        return this.mDriverProfilePreferences.unit_system;
    }

    public Locale getLocale() {
        if (this.mLocale != null) {
            return this.mLocale;
        }
        return Locale.getDefault();
    }

    private void setLocale() {
        try {
            this.mLocale = null;
            String localeStr = this.mDriverProfilePreferences.locale;
            if (TextUtils.isEmpty(localeStr)) {
                sLogger.v("no locale string");
                return;
            }
            Locale locale = HudLocale.getLocaleForID(localeStr);
            sLogger.v("locale string:" + localeStr + " locale:" + locale + " profile:" + getProfileName() + " email:" + getDriverEmail());
            if (TextUtils.isEmpty(locale.getISO3Language())) {
                sLogger.v("locale no language");
            } else {
                this.mLocale = locale;
            }
        } catch (Throwable t) {
            sLogger.e("setLocale", t);
        }
    }

    public boolean isTrafficEnabled() {
        return this.mTrafficEnabled;
    }

    public void setTrafficEnabled(boolean b) {
        this.mTrafficEnabled = b;
    }

    private void setTraffic() {
        this.mTrafficEnabled = true;
    }

    public boolean isAutoOnEnabled() {
        return ((Boolean) Wire.get(this.mDriverProfilePreferences.auto_on_enabled, DriverProfilePreferences.DEFAULT_AUTO_ON_ENABLED)).booleanValue();
    }

    public ObdScanSetting getObdScanSetting() {
        return this.mDriverProfilePreferences.obdScanSetting;
    }

    public long getObdBlacklistModificationTime() {
        return this.mDriverProfilePreferences.obdBlacklistLastModified != null ? this.mDriverProfilePreferences.obdBlacklistLastModified.longValue() : 0;
    }

    public boolean isLimitBandwidthModeOn() {
        return Boolean.TRUE.equals(this.mDriverProfilePreferences.limit_bandwidth);
    }

    public boolean isProfilePublic() {
        return Boolean.TRUE.equals(this.mDriverProfilePreferences.profile_is_public);
    }

    public Bitmap getDriverImage() {
        return this.mDriverImage;
    }

    public File getDriverImageFile() {
        return new File(this.mPreferencesDirectory, DRIVER_PROFILE_IMAGE);
    }

    private void removeDriverImage() {
        this.mDriverImage = null;
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                IOUtils.deleteFile(HudApplication.getAppContext(), new File(DriverProfile.this.mPreferencesDirectory, DriverProfile.DRIVER_PROFILE_IMAGE).getAbsolutePath());
            }
        }, 1);
    }

    public String getProfileName() {
        return this.mProfileName;
    }

    public File getPlacesImageDir() {
        return this.mPlacesImageDirectory;
    }

    public File getContactsImageDir() {
        return this.mContactsImageDirectory;
    }

    public File getMusicImageDir() {
        return this.mMusicImageDirectory;
    }

    public File getPreferencesDirectory() {
        return this.mPreferencesDirectory;
    }

    public boolean isDefaultProfile() {
        return DriverProfileHelper.getInstance().getDriverProfileManager().isDefaultProfile(this);
    }

    public String toString() {
        return "DriverProfile{mProfileName='" + this.mProfileName + '\'' + '}';
    }

    void copy(DriverProfile profile) {
        this.mDriverProfilePreferences = new DriverProfilePreferences.Builder(profile.mDriverProfilePreferences).build();
        writeMessageToFile(this.mDriverProfilePreferences, DRIVER_PROFILE_PREFERENCES);
        this.mNavigationPreferences = new NavigationPreferences.Builder(profile.mNavigationPreferences).build();
        writeMessageToFile(this.mNavigationPreferences, NAVIGATION_PREFERENCES);
        this.mInputPreferences = new InputPreferences.Builder(profile.mInputPreferences).build();
        writeMessageToFile(this.mInputPreferences, INPUT_PREFERENCES);
        this.mSpeakerPreferences = new DisplaySpeakerPreferences.Builder(profile.mSpeakerPreferences).build();
        writeMessageToFile(this.mSpeakerPreferences, SPEAKER_PREFERENCES);
        this.mNotificationPreferences = new NotificationPreferences.Builder(profile.mNotificationPreferences).build();
        writeMessageToFile(this.mNotificationPreferences, NOTIFICATION_PREFERENCES);
        this.mLocalPreferences = new LocalPreferences.Builder(profile.mLocalPreferences).build();
        writeMessageToFile(this.mLocalPreferences, LOCAL_PREFERENCES);
        sLogger.v("copy done:" + this.mPreferencesDirectory);
    }
}
