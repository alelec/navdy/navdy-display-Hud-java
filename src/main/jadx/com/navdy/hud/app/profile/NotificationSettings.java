package com.navdy.hud.app.profile;

import com.navdy.ancs.AppleNotification;
import com.navdy.service.library.events.notification.NotificationSetting;
import com.navdy.service.library.events.preferences.NotificationPreferences;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class NotificationSettings {
    public static final String APP_ID_CALENDAR = "com.apple.mobilecal";
    public static final String APP_ID_MAIL = "com.apple.mobilemail";
    public static final String APP_ID_PHONE = "com.apple.mobilephone";
    public static final String APP_ID_REMINDERS = "com.apple.reminders";
    public static final String APP_ID_SMS = "com.apple.MobileSMS";
    private static Logger sLogger = new Logger(NotificationSettings.class);
    private Map<String, Boolean> enabledApps = new HashMap();
    private final Object lock = new Object();

    public NotificationSettings(NotificationPreferences preferences) {
        update(preferences);
    }

    public void update(NotificationPreferences preferences) {
        synchronized (this.lock) {
            this.enabledApps.clear();
            if (!(preferences == null || preferences.settings == null)) {
                for (NotificationSetting setting : preferences.settings) {
                    this.enabledApps.put(setting.app, setting.enabled);
                }
            }
        }
    }

    public Boolean enabled(String appId) {
        Boolean setting;
        synchronized (this.lock) {
            setting = (Boolean) this.enabledApps.get(appId);
        }
        return setting;
    }

    public Boolean enabled(AppleNotification notification) {
        return enabled(notification.getAppId());
    }

    public List<String> enabledApps() {
        List<String> result = new ArrayList();
        synchronized (this.lock) {
            for (Entry<String, Boolean> entry : this.enabledApps.entrySet()) {
                if (Boolean.TRUE.equals(entry.getValue())) {
                    result.add(entry.getKey());
                }
            }
        }
        return result;
    }
}
