package com.navdy.hud.app.manager;

import android.bluetooth.BluetoothAdapter;
import android.os.Handler;
import android.os.Looper;
import com.navdy.hud.app.event.InitEvents.BluetoothStateChanged;
import com.navdy.hud.app.event.InitEvents.ConnectionServiceStarted;
import com.navdy.hud.app.event.InitEvents.InitPhase;
import com.navdy.hud.app.event.InitEvents.Phase;
import com.navdy.hud.app.service.ConnectionHandler;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

public class InitManager {
    private static final Logger sLogger = new Logger(InitManager.class);
    private Bus bus;
    private Runnable checkConnectionServiceReady = new Runnable() {
        public boolean notified;

        public void run() {
            if (!this.notified && InitManager.this.connectionHandler.serviceConnected() && BluetoothAdapter.getDefaultAdapter().isEnabled()) {
                InitManager.sLogger.i("bt and connection service ready");
                this.notified = true;
                InitManager.this.bus.post(new InitPhase(Phase.CONNECTION_SERVICE_STARTED));
                InitManager.this.handler.post(new PhaseEmitter(Phase.POST_START));
                InitManager.this.handler.post(new PhaseEmitter(Phase.LATE));
            }
        }
    };
    private ConnectionHandler connectionHandler;
    private Handler handler;

    private class PhaseEmitter implements Runnable {
        private Phase phase;

        public PhaseEmitter(Phase phase) {
            this.phase = phase;
        }

        public void run() {
            InitManager.sLogger.i("Triggering init phase:" + this.phase);
            InitManager.this.bus.post(new InitPhase(this.phase));
        }
    }

    public InitManager(Bus bus, ConnectionHandler connectionHandler) {
        this.bus = bus;
        this.handler = new Handler(Looper.getMainLooper());
        this.bus.register(this);
        this.connectionHandler = connectionHandler;
    }

    public void start() {
        this.handler.post(new PhaseEmitter(Phase.PRE_USER_INTERACTION));
        this.handler.post(this.checkConnectionServiceReady);
    }

    @Subscribe
    public void onBluetoothChanged(BluetoothStateChanged event) {
        this.handler.post(this.checkConnectionServiceReady);
    }

    @Subscribe
    public void onConnectionService(ConnectionServiceStarted event) {
        this.handler.post(this.checkConnectionServiceReady);
    }
}
