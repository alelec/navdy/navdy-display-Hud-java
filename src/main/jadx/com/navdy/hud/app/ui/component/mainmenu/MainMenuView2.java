package com.navdy.hud.app.ui.component.mainmenu;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.RelativeLayout;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.hud.app.R;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.ui.component.ConfirmationLayout;
import com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.Callback;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.log.Logger;
import javax.inject.Inject;
import mortar.Mortar;

public class MainMenuView2 extends RelativeLayout implements IInputHandler {
    private static final Logger sLogger = new Logger(MainMenuView2.class);
    private Callback callback;
    @InjectView(R.id.confirmationLayout)
    public ConfirmationLayout confirmationLayout;
    @Inject
    public Presenter presenter;
    @InjectView(R.id.rightBackground)
    View rightBackground;
    public View scrimCover;
    VerticalMenuComponent vmenuComponent;

    public MainMenuView2(Context context) {
        this(context, null);
    }

    public MainMenuView2(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MainMenuView2(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.callback = new Callback() {
            public void select(ItemSelectionState selection) {
                MainMenuView2.this.presenter.selectItem(selection);
            }

            public void onLoad() {
                MainMenuView2.sLogger.v("onLoad");
                MainMenuView2.this.presenter.resetSelectedItem();
            }

            public boolean isItemClickable(ItemSelectionState selection) {
                return MainMenuView2.this.presenter.isItemClickable(selection);
            }

            public void onBindToView(Model model, View view, int pos, ModelState state) {
                IMenu menu = MainMenuView2.this.presenter.getCurrentMenu();
                if (menu != null) {
                    menu.onBindToView(model, view, pos, state);
                }
            }

            public void onItemSelected(ItemSelectionState selection) {
                IMenu menu = MainMenuView2.this.presenter.getCurrentMenu();
                if (menu != null) {
                    menu.onItemSelected(selection);
                }
            }

            public void onScrollIdle() {
                IMenu menu = MainMenuView2.this.presenter.getCurrentMenu();
                if (menu != null) {
                    menu.onScrollIdle();
                }
            }

            public void onFastScrollStart() {
                IMenu menu = MainMenuView2.this.presenter.getCurrentMenu();
                if (menu != null) {
                    menu.onFastScrollStart();
                }
            }

            public void onFastScrollEnd() {
                IMenu menu = MainMenuView2.this.presenter.getCurrentMenu();
                if (menu != null) {
                    menu.onFastScrollEnd();
                }
            }

            public void showToolTip() {
                IMenu menu = MainMenuView2.this.presenter.getCurrentMenu();
                if (menu != null) {
                    menu.showToolTip();
                }
            }

            public void close() {
                MainMenuView2.this.presenter.close();
            }

            public boolean isClosed() {
                return MainMenuView2.this.presenter.isClosed();
            }
        };
        if (!isInEditMode()) {
            Mortar.inject(context, this);
        }
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View) this);
        this.vmenuComponent = new VerticalMenuComponent(this, this.callback, true);
        this.vmenuComponent.leftContainer.setAlpha(0.0f);
        this.vmenuComponent.rightContainer.setAlpha(0.0f);
        this.scrimCover = LayoutInflater.from(getContext()).inflate(R.layout.screen_main_menu_2_scrim, null);
        this.scrimCover.setLayoutParams(new MarginLayoutParams(-1, -1));
        this.vmenuComponent.rightContainer.addView(this.scrimCover);
    }

    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.vmenuComponent.clear();
        this.presenter.sendCloseEvent();
        this.vmenuComponent.verticalList.cancelLoadingAnimation(1);
        if (this.presenter != null) {
            this.presenter.dropView((View) this);
        }
    }

    public boolean onGesture(GestureEvent event) {
        if (this.confirmationLayout.getVisibility() == 0) {
            return false;
        }
        return this.vmenuComponent.handleGesture(event);
    }

    public boolean onKey(CustomKeyEvent event) {
        if (this.confirmationLayout.getVisibility() == 0) {
            return this.confirmationLayout.handleKey(event);
        }
        return this.vmenuComponent.handleKey(event);
    }

    public IInputHandler nextHandler() {
        return InputManager.nextContainingHandler(this);
    }

    void performSelectionAnimation(Runnable endAction, int delay) {
        this.vmenuComponent.performSelectionAnimation(endAction, delay);
    }
}
