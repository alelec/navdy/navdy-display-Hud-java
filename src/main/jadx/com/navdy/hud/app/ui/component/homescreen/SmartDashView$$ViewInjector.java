package com.navdy.hud.app.ui.component.homescreen;

import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.dashboard.GaugeViewPager;
import com.navdy.hud.app.view.GaugeView;

public class SmartDashView$$ViewInjector {
    public static void inject(Finder finder, SmartDashView target, Object source) {
        target.mLeftGaugeViewPager = (GaugeViewPager) finder.findRequiredView(source, R.id.left_gauge, "field 'mLeftGaugeViewPager'");
        target.mRightGaugeViewPager = (GaugeViewPager) finder.findRequiredView(source, R.id.right_gauge, "field 'mRightGaugeViewPager'");
        target.mMiddleGaugeView = (GaugeView) finder.findRequiredView(source, R.id.middle_gauge, "field 'mMiddleGaugeView'");
        target.mEtaLayout = (ViewGroup) finder.findRequiredView(source, R.id.eta_layout, "field 'mEtaLayout'");
        target.etaText = (TextView) finder.findRequiredView(source, R.id.eta_time, "field 'etaText'");
        target.etaAmPm = (TextView) finder.findRequiredView(source, R.id.eta_time_amPm, "field 'etaAmPm'");
    }

    public static void reset(SmartDashView target) {
        target.mLeftGaugeViewPager = null;
        target.mRightGaugeViewPager = null;
        target.mMiddleGaugeView = null;
        target.mEtaLayout = null;
        target.etaText = null;
        target.etaAmPm = null;
    }
}
