package com.navdy.hud.app.ui.component.vlist.viewholder;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.AnimatorSet.Builder;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.ImageView;
import android.widget.TextView;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.ui.component.HaloView;
import com.navdy.hud.app.ui.component.image.CrossFadeImageView;
import com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode;
import com.navdy.hud.app.ui.component.image.IconColorImageView;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State;
import com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;

public class IconOptionsViewHolder extends VerticalViewHolder {
    private static final int iconMargin = HudApplication.getAppContext().getResources().getDimensionPixelOffset(R.dimen.vlist_icon_list_margin);
    private static final Logger sLogger = new Logger(IconOptionsViewHolder.class);
    protected Builder animatorSetBuilder;
    private int currentSelection = -1;
    private Runnable fluctuatorRunnable = new Runnable() {
        public void run() {
            IconOptionsViewHolder.this.startFluctuator();
        }
    };
    private DefaultAnimationListener fluctuatorStartListener = new DefaultAnimationListener() {
        public void onAnimationEnd(Animator animation) {
            IconOptionsViewHolder.this.itemAnimatorSet = null;
            IconOptionsViewHolder.this.handler.removeCallbacks(IconOptionsViewHolder.this.fluctuatorRunnable);
            IconOptionsViewHolder.this.handler.postDelayed(IconOptionsViewHolder.this.fluctuatorRunnable, 100);
        }
    };
    private boolean itemNotSelected;
    private State lastState;
    private Model model;
    private ArrayList<ViewContainer> viewContainers = new ArrayList();

    private static class ViewContainer {
        IconColorImageView big;
        CrossFadeImageView crossFadeImageView;
        HaloView haloView;
        ViewGroup iconContainer;
        IconColorImageView small;

        private ViewContainer() {
        }

        /* synthetic */ ViewContainer(AnonymousClass1 x0) {
            this();
        }
    }

    public static Model buildModel(int id, int[] icons, int[] ids, int[] iconSelectedColors, int[] iconDeselectedColors, int[] iconFluctuatorColors, int currentSelection, boolean supportsToolTip) {
        Model model = new Model();
        model.type = ModelType.ICON_OPTIONS;
        model.id = id;
        model.iconList = icons;
        model.iconIds = ids;
        model.iconSelectedColors = iconSelectedColors;
        model.iconDeselectedColors = iconDeselectedColors;
        model.iconFluctuatorColors = iconFluctuatorColors;
        model.currentIconSelection = currentSelection;
        model.supportsToolTip = supportsToolTip;
        return model;
    }

    public static IconOptionsViewHolder buildViewHolder(ViewGroup parent, VerticalList vlist, Handler handler) {
        return new IconOptionsViewHolder((ViewGroup) LayoutInflater.from(parent.getContext()).inflate(R.layout.vlist_icon_options, parent, false), vlist, handler);
    }

    public IconOptionsViewHolder(ViewGroup layout, VerticalList vlist, Handler handler) {
        super(layout, vlist, handler);
        layout.setPivotX((float) (VerticalViewHolder.listItemHeight / 2));
        layout.setPivotY((float) (VerticalViewHolder.listItemHeight / 2));
    }

    public ModelType getModelType() {
        return ModelType.ICON_OPTIONS;
    }

    public void setItemState(State state, AnimationType animation, int duration, boolean startFluctuator) {
        if (this.lastState != state) {
            boolean initialMove = false;
            if (this.itemNotSelected) {
                initialMove = true;
                this.itemNotSelected = false;
                animation = AnimationType.MOVE;
            }
            this.animatorSetBuilder = null;
            float iconScaleFactor = 0.0f;
            Mode mode = null;
            switch (state) {
                case SELECTED:
                    iconScaleFactor = 1.0f;
                    mode = Mode.BIG;
                    break;
                case UNSELECTED:
                    iconScaleFactor = 0.6f;
                    mode = Mode.SMALL;
                    break;
            }
            int i;
            ViewContainer viewContainer;
            switch (animation) {
                case NONE:
                case INIT:
                    for (i = 0; i < this.model.iconList.length; i++) {
                        viewContainer = (ViewContainer) this.viewContainers.get(i);
                        viewContainer.iconContainer.setScaleX(iconScaleFactor);
                        viewContainer.iconContainer.setScaleY(iconScaleFactor);
                        viewContainer.crossFadeImageView.setMode(mode);
                        if (i == this.currentSelection) {
                            viewContainer.big.setIcon(this.model.iconList[i], this.model.iconSelectedColors[i], null, 0.83f);
                        } else {
                            viewContainer.big.setIcon(this.model.iconList[i], this.model.iconDeselectedColors[i], null, 0.83f);
                        }
                    }
                    break;
                case MOVE:
                    if (!initialMove) {
                        this.itemAnimatorSet = new AnimatorSet();
                        boolean currentSelected = false;
                        if (this.currentSelection != -1) {
                            viewContainer = (ViewContainer) this.viewContainers.get(this.currentSelection);
                            if (viewContainer.crossFadeImageView.getMode() != mode) {
                                currentSelected = true;
                                viewContainer.crossFadeImageView.setMode(mode);
                            }
                        }
                        for (i = 0; i < this.model.iconList.length; i++) {
                            viewContainer = (ViewContainer) this.viewContainers.get(i);
                            if (!((i == this.currentSelection && currentSelected) || viewContainer.crossFadeImageView.getMode() == mode)) {
                                if (this.animatorSetBuilder == null) {
                                    this.animatorSetBuilder = this.itemAnimatorSet.play(viewContainer.crossFadeImageView.getCrossFadeAnimator());
                                } else {
                                    this.animatorSetBuilder.with(viewContainer.crossFadeImageView.getCrossFadeAnimator());
                                }
                            }
                        }
                        PropertyValuesHolder p1 = PropertyValuesHolder.ofFloat(View.SCALE_X, new float[]{iconScaleFactor});
                        PropertyValuesHolder p2 = PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[]{iconScaleFactor});
                        if (this.animatorSetBuilder != null) {
                            this.animatorSetBuilder.with(ObjectAnimator.ofPropertyValuesHolder(this.layout, new PropertyValuesHolder[]{p1, p2}));
                            break;
                        }
                        this.animatorSetBuilder = this.itemAnimatorSet.play(ObjectAnimator.ofPropertyValuesHolder(this.layout, new PropertyValuesHolder[]{p1, p2}));
                        break;
                    }
                    for (i = 0; i < this.model.iconList.length; i++) {
                        viewContainer = (ViewContainer) this.viewContainers.get(i);
                        viewContainer.big.setIcon(this.model.iconList[i], this.model.iconDeselectedColors[i], null, 0.83f);
                        if (viewContainer.crossFadeImageView.getMode() != mode) {
                            viewContainer.crossFadeImageView.setMode(mode);
                        }
                    }
                    this.layout.setScaleX(iconScaleFactor);
                    this.layout.setScaleY(iconScaleFactor);
                    break;
            }
            if (state == State.SELECTED) {
                if (this.itemAnimatorSet != null) {
                    this.itemAnimatorSet.addListener(this.fluctuatorStartListener);
                } else {
                    startFluctuator();
                }
            }
            this.lastState = state;
        }
    }

    public void preBind(Model model, ModelState modelState) {
        this.model = model;
        Context context = this.layout.getContext();
        if (this.layout.getChildCount() != (model.iconList.length + model.iconList.length) - 1) {
            this.currentSelection = model.currentIconSelection;
            sLogger.v("preBind: createIcons:" + model.iconList.length);
            this.layout.removeAllViews();
            this.viewContainers.clear();
            LayoutInflater inflater = LayoutInflater.from(this.layout.getContext());
            for (int i = 0; i < model.iconList.length; i++) {
                if (i != 0) {
                    View view = new View(context);
                    view.setLayoutParams(new MarginLayoutParams(VerticalViewHolder.iconMargin, VerticalViewHolder.mainIconSize));
                    this.layout.addView(view);
                }
                ViewContainer viewContainer = new ViewContainer();
                ViewGroup viewgroup = (ViewGroup) inflater.inflate(R.layout.vlist_halo_image, this.layout, false);
                viewContainer.iconContainer = (ViewGroup) viewgroup.findViewById(R.id.iconContainer);
                viewContainer.crossFadeImageView = (CrossFadeImageView) viewgroup.findViewById(R.id.crossFadeImageView);
                viewContainer.crossFadeImageView.inject(Mode.SMALL);
                viewContainer.big = (IconColorImageView) viewgroup.findViewById(R.id.big);
                viewContainer.small = (IconColorImageView) viewgroup.findViewById(R.id.small);
                viewContainer.haloView = (HaloView) viewgroup.findViewById(R.id.halo);
                this.viewContainers.add(viewContainer);
                viewgroup.setLayoutParams(new MarginLayoutParams(VerticalViewHolder.mainIconSize, VerticalViewHolder.mainIconSize));
                this.layout.addView(viewgroup);
            }
            return;
        }
        sLogger.v("preBind: createIcons: already created");
    }

    public void bind(Model model, ModelState modelState) {
        if (this.vlist.adapter.getModel(this.vlist.getRawPosition()) != model) {
            this.itemNotSelected = true;
        }
        for (int i = 0; i < model.iconList.length; i++) {
            ViewContainer viewContainer = (ViewContainer) this.viewContainers.get(i);
            viewContainer.big.setIcon(model.iconList[i], model.iconSelectedColors[i], null, 0.83f);
            viewContainer.small.setIcon(model.iconList[i], model.iconDeselectedColors[i], null, 0.83f);
            int fluctuatorColor = Color.argb(IconBaseViewHolder.FLUCTUATOR_OPACITY_ALPHA, Color.red(model.iconFluctuatorColors[i]), Color.green(model.iconFluctuatorColors[i]), Color.blue(model.iconFluctuatorColors[i]));
            viewContainer.haloView.setVisibility(4);
            viewContainer.haloView.setStrokeColor(fluctuatorColor);
        }
    }

    public void clearAnimation() {
        stopFluctuator(this.currentSelection, true);
        stopAnimation();
        this.currentState = null;
        this.layout.setVisibility(0);
        this.layout.setAlpha(1.0f);
    }

    public void select(Model model, final int pos, int duration) {
        if (isItemInBounds(this.currentSelection)) {
            stopFluctuator(this.currentSelection, false);
            VerticalAnimationUtils.performClick(((ViewContainer) this.viewContainers.get(this.currentSelection)).iconContainer, duration, new Runnable() {
                public void run() {
                    ItemSelectionState selectionState = IconOptionsViewHolder.this.vlist.getItemSelectionState();
                    selectionState.set(IconOptionsViewHolder.this.model, IconOptionsViewHolder.this.model.id, pos, IconOptionsViewHolder.this.model.iconIds[IconOptionsViewHolder.this.currentSelection], IconOptionsViewHolder.this.currentSelection);
                    IconOptionsViewHolder.this.vlist.performSelectAction(selectionState);
                }
            });
        }
    }

    public void copyAndPosition(ImageView imageC, TextView titleC, TextView subTitleC, TextView subTitle2C, boolean setImage) {
        if (this.currentSelection >= 0) {
            View view = ((ViewContainer) this.viewContainers.get(this.currentSelection)).crossFadeImageView.getBig();
            if (setImage) {
                VerticalAnimationUtils.copyImage((ImageView) view, imageC);
            }
            imageC.setX((float) (selectedImageX + getOffSetFromPos(this.currentSelection)));
            imageC.setY((float) selectedImageY);
        }
    }

    public void startFluctuator() {
        startFluctuator(this.currentSelection, true);
    }

    private void startFluctuator(int item, boolean changeColor) {
        if (isItemInBounds(item)) {
            ViewContainer viewContainer = (ViewContainer) this.viewContainers.get(item);
            if (changeColor) {
                viewContainer.big.setIcon(this.model.iconList[item], this.model.iconSelectedColors[item], null, 0.83f);
            }
            viewContainer.haloView.setVisibility(0);
            viewContainer.haloView.start();
        }
    }

    private void stopFluctuator(int item, boolean changeColor) {
        if (isItemInBounds(item)) {
            this.handler.removeCallbacks(this.fluctuatorRunnable);
            ViewContainer viewContainer = (ViewContainer) this.viewContainers.get(item);
            if (changeColor) {
                viewContainer.big.setIcon(this.model.iconList[item], this.model.iconDeselectedColors[item], null, 0.83f);
            }
            viewContainer.haloView.setVisibility(8);
            viewContainer.haloView.stop();
        }
    }

    private boolean isItemInBounds(int item) {
        if (this.model == null || item < 0 || item >= this.model.iconList.length) {
            return false;
        }
        return true;
    }

    public boolean handleKey(CustomKeyEvent event) {
        if (!isItemInBounds(this.currentSelection)) {
            return false;
        }
        ItemSelectionState itemSelectionState;
        switch (event) {
            case LEFT:
                if (this.currentSelection == 0) {
                    return false;
                }
                this.currentSelection--;
                stopFluctuator(this.currentSelection + 1, true);
                startFluctuator(this.currentSelection, true);
                itemSelectionState = this.vlist.getItemSelectionState();
                itemSelectionState.set(this.model, this.model.id, this.vlist.getCurrentPosition(), this.model.iconIds[this.currentSelection], this.currentSelection);
                this.vlist.callItemSelected(itemSelectionState);
                return true;
            case RIGHT:
                if (this.currentSelection == this.model.iconList.length - 1) {
                    return false;
                }
                this.currentSelection++;
                stopFluctuator(this.currentSelection - 1, true);
                startFluctuator(this.currentSelection, true);
                itemSelectionState = this.vlist.getItemSelectionState();
                itemSelectionState.set(this.model, this.model.id, this.vlist.getCurrentPosition(), this.model.iconIds[this.currentSelection], this.currentSelection);
                this.vlist.callItemSelected(itemSelectionState);
                return true;
            default:
                return false;
        }
    }

    public int getCurrentSelection() {
        return this.currentSelection;
    }

    public int getCurrentSelectionId() {
        if (this.currentSelection < 0 || this.currentSelection >= this.model.iconIds.length) {
            return -1;
        }
        return this.model.iconIds[this.currentSelection];
    }

    public void selectionDown() {
        sLogger.v("selectionDown");
        if (isItemInBounds(this.currentSelection)) {
            stopFluctuator(this.currentSelection, false);
            VerticalAnimationUtils.performClickDown(((ViewContainer) this.viewContainers.get(this.currentSelection)).iconContainer, 25, null, false);
            return;
        }
        sLogger.v("selectionDown:invalid pos:" + this.currentSelection);
    }

    public void selectionUp() {
        sLogger.v("selectionUp");
        if (isItemInBounds(this.currentSelection)) {
            startFluctuator(this.currentSelection, false);
            VerticalAnimationUtils.performClickUp(((ViewContainer) this.viewContainers.get(this.currentSelection)).iconContainer, 25, null);
            return;
        }
        sLogger.v("selectionUp:invalid pos:" + this.currentSelection);
    }

    public static int getOffSetFromPos(int currentSelection) {
        if (currentSelection <= 0) {
            return 0;
        }
        return (VerticalViewHolder.mainIconSize * currentSelection) + (iconMargin * currentSelection);
    }

    public boolean hasToolTip() {
        return this.model.supportsToolTip;
    }
}
