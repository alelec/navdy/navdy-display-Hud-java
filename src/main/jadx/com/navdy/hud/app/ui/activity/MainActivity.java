package com.navdy.hud.app.ui.activity;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.device.PowerManager;
import com.navdy.hud.app.device.dial.DialManager;
import com.navdy.hud.app.event.InitEvents.BluetoothStateChanged;
import com.navdy.hud.app.event.InitEvents.InitPhase;
import com.navdy.hud.app.event.InitEvents.Phase;
import com.navdy.hud.app.event.Wakeup;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.hud.app.manager.PairingManager;
import com.navdy.hud.app.ui.activity.Main.Presenter;
import com.navdy.hud.app.ui.component.bluetooth.BluetoothPairing;
import com.navdy.hud.app.ui.component.bluetooth.BluetoothPairing.BondStateChange;
import com.navdy.hud.app.ui.component.bluetooth.BluetoothPairing.PairingCancelled;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.hud.app.view.ContainerView;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import javax.inject.Inject;
import mortar.Blueprint;

public class MainActivity extends HudBaseActivity {
    private static final String ACTION_PAIRING_CANCEL = "android.bluetooth.device.action.PAIRING_CANCEL";
    private BroadcastReceiver bluetoothReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            BluetoothDevice device = (BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
            Bus bus = MainActivity.this.mainPresenter.bus;
            if (action.equals("android.bluetooth.device.action.BOND_STATE_CHANGED")) {
                int bondState = intent.getIntExtra("android.bluetooth.device.extra.BOND_STATE", Integer.MIN_VALUE);
                int oldState = intent.getIntExtra("android.bluetooth.device.extra.PREVIOUS_BOND_STATE", Integer.MIN_VALUE);
                MainActivity.this.logger.d("Bond state change - device:" + device + " state:" + bondState + " prevState:" + oldState);
                bus.post(new BondStateChange(device, oldState, bondState));
            } else if (action.equals("android.bluetooth.adapter.action.STATE_CHANGED")) {
                int btState = intent.getIntExtra("android.bluetooth.adapter.extra.STATE", Integer.MIN_VALUE);
                if (btState == 12 || btState == 10) {
                    bus.post(new BluetoothStateChanged(btState == 12));
                }
            } else if (action.equals("android.bluetooth.device.action.PAIRING_REQUEST")) {
                abortBroadcast();
                if (DialManager.getInstance().isDialDevice(device)) {
                    MainActivity.this.logger.w("Pairing request from dial device, ignore [" + device.getName() + "]");
                    return;
                }
                int type = intent.getIntExtra("android.bluetooth.device.extra.PAIRING_VARIANT", Integer.MIN_VALUE);
                int pairingKey = intent.getIntExtra("android.bluetooth.device.extra.PAIRING_KEY", Integer.MIN_VALUE);
                MainActivity.this.logger.d("Showing pairing request from " + device + " of type:" + type + " with pin:" + pairingKey);
                Bundle bundle = new Bundle();
                bundle.putParcelable(BluetoothPairing.ARG_DEVICE, device);
                bundle.putInt(BluetoothPairing.ARG_VARIANT, type);
                bundle.putInt(BluetoothPairing.ARG_PIN, pairingKey);
                boolean autoAccept = MainActivity.this.pairingManager.isAutoPairing();
                Bundle extra = getResultExtras(false);
                if (!(autoAccept || extra == null)) {
                    autoAccept = extra.getBoolean("auto", false);
                }
                bundle.putBoolean("auto", autoAccept);
                BluetoothPairing.showBluetoothPairingToast(bundle);
            } else if (action.equals(MainActivity.ACTION_PAIRING_CANCEL)) {
                abortBroadcast();
                bus.post(new PairingCancelled(device));
            }
        }
    };
    @InjectView(R.id.container)
    ContainerView container;
    TextView debugText;
    @Inject
    Presenter mainPresenter;
    @Inject
    PairingManager pairingManager;
    @Inject
    PowerManager powerManager;

    protected Blueprint getBlueprint() {
        return new Main();
    }

    protected void onCreate(Bundle savedInstanceState) {
        boolean z;
        super.onCreate(savedInstanceState);
        if (DeviceUtil.isNavdyDevice()) {
            setContentView(R.layout.main_view_lyt);
        } else {
            setContentView(R.layout.activity_fullscreen);
            this.debugText = (TextView) findViewById(R.id.debug_text);
            Resources resources = getResources();
            int hudWidth = (int) resources.getDimension(R.dimen.dashboard_width);
            this.debugText.setText("Device:  " + Build.DEVICE + GlanceConstants.COLON_SEPARATOR + Build.MODEL + " width = " + hudWidth + " height = " + ((int) resources.getDimension(R.dimen.dashboard_height)) + " scale factor = " + (hudWidth / 640));
        }
        ButterKnife.inject((Activity) this);
        PreferenceManager.setDefaultValues(this, "HUD", 0, R.xml.developer_preferences, false);
        if (this.powerManager.inQuietMode()) {
            z = false;
        } else {
            z = true;
        }
        makeImmersive(z);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.bluetooth.device.action.PAIRING_REQUEST");
        intentFilter.addAction("android.bluetooth.device.action.BOND_STATE_CHANGED");
        intentFilter.addAction("android.bluetooth.adapter.action.STATE_CHANGED");
        intentFilter.addAction(ACTION_PAIRING_CANCEL);
        registerReceiver(this.bluetoothReceiver, intentFilter);
        this.mainPresenter.bus.register(this);
    }

    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.bluetoothReceiver);
    }

    @Subscribe
    public void onInitEvent(InitPhase event) {
        if (event.phase == Phase.POST_START) {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    ((HudApplication) MainActivity.this.getApplication()).initHudApp();
                }
            }, 1);
        }
    }

    @Subscribe
    public void onWakeup(Wakeup event) {
        this.logger.v("enableNormalMode(): FLAG_KEEP_SCREEN_ON set");
        getWindow().addFlags(128);
    }

    public void onConfigurationChanged(Configuration newConfig) {
        this.logger.v("onConfigurationChanged:" + newConfig);
        super.onConfigurationChanged(newConfig);
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            makeImmersive(!this.powerManager.inQuietMode());
        }
    }
}
