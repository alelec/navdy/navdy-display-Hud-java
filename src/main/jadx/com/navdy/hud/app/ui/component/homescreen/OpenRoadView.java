package com.navdy.hud.app.ui.component.homescreen;

import android.animation.AnimatorSet.Builder;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.MapEvents.ManeuverDisplay;
import com.navdy.hud.app.maps.here.HereMapUtil;
import com.navdy.hud.app.view.MainView.CustomAnimationMode;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

public class OpenRoadView extends RelativeLayout {
    private Bus bus;
    private HomeScreenView homeScreenView;
    private Logger logger;
    @InjectView(R.id.openMapRoadInfo)
    TextView openMapRoadInfo;

    public OpenRoadView(Context context) {
        this(context, null);
    }

    public OpenRoadView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public OpenRoadView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    protected void onFinishInflate() {
        this.logger = HomeScreenView.sLogger;
        super.onFinishInflate();
        ButterKnife.inject((View) this);
        this.bus = RemoteDeviceManager.getInstance().getBus();
    }

    public void init(HomeScreenView homeScreenView) {
        this.homeScreenView = homeScreenView;
        this.bus.register(this);
    }

    @Subscribe
    public void onManeuverDisplay(ManeuverDisplay event) {
        setRoad(event.currentRoad);
    }

    public void setView(CustomAnimationMode mode) {
        MarginLayoutParams margins = (MarginLayoutParams) this.openMapRoadInfo.getLayoutParams();
        switch (mode) {
            case EXPAND:
                margins.leftMargin = HomeScreenResourceValues.openMapRoadInfoMargin;
                margins.rightMargin = HomeScreenResourceValues.openMapRoadInfoMargin;
                return;
            case SHRINK_LEFT:
                margins.leftMargin = HomeScreenResourceValues.openMapRoadInfoShrinkLeft_L_Margin;
                margins.rightMargin = HomeScreenResourceValues.openMapRoadInfoShrinkLeft_R_Margin;
                return;
            default:
                return;
        }
    }

    public void getCustomAnimator(CustomAnimationMode mode, Builder builder) {
        switch (mode) {
            case EXPAND:
                builder.with(HomeScreenUtils.getMarginAnimator(this.openMapRoadInfo, HomeScreenResourceValues.openMapRoadInfoMargin, HomeScreenResourceValues.openMapRoadInfoMargin));
                return;
            case SHRINK_LEFT:
                builder.with(HomeScreenUtils.getMarginAnimator(this.openMapRoadInfo, HomeScreenResourceValues.openMapRoadInfoShrinkLeft_L_Margin, HomeScreenResourceValues.openMapRoadInfoShrinkLeft_R_Margin));
                return;
            default:
                return;
        }
    }

    public void setRoad() {
        setRoad(HereMapUtil.getCurrentRoadName());
    }

    private void setRoad(String str) {
        CharSequence str2;
        if (str2 == null) {
            str2 = "";
        }
        this.openMapRoadInfo.setText(str2);
    }
}
