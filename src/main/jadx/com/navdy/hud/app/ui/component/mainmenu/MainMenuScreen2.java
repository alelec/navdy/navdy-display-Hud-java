package com.navdy.hud.app.ui.component.mainmenu;

import android.animation.Animator;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import com.here.android.mpa.common.GeoBoundingBox;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.routing.Route;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.event.DeviceInfoAvailable;
import com.navdy.hud.app.framework.contacts.Contact;
import com.navdy.hud.app.framework.trips.TripManager;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.MapEvents.MapEngineReady;
import com.navdy.hud.app.maps.MapEvents.NavigationModeChange;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.hud.app.maps.here.HereRouteCache;
import com.navdy.hud.app.maps.here.HereRouteCache.RouteInfo;
import com.navdy.hud.app.screen.BaseScreen;
import com.navdy.hud.app.ui.component.ConfirmationLayout;
import com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode;
import com.navdy.hud.app.ui.component.homescreen.HomeScreenView;
import com.navdy.hud.app.ui.component.homescreen.NavigationView;
import com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu;
import com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel;
import com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.ReportIssueMenuType;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder;
import com.navdy.hud.app.ui.framework.BasePresenter;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.events.ui.ShowScreen.Builder;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import flow.Flow.Direction;
import flow.Layout;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Singleton;

@Layout(R.layout.screen_main_menu_2)
public class MainMenuScreen2 extends BaseScreen {
    public static final String ARG_CONTACTS_LIST = "CONTACTS";
    public static final String ARG_MENU_MODE = "MENU_MODE";
    public static final String ARG_MENU_PATH = "MENU_PATH";
    public static final String ARG_NOTIFICATION_ID = "NOTIF_ID";
    static final String SLASH = "/";
    private static final Logger sLogger = new Logger(MainMenuScreen2.class);

    public enum MenuMode {
        MAIN_MENU,
        REPLY_PICKER,
        SNAPSHOT_TITLE_PICKER
    }

    @dagger.Module(addsTo = com.navdy.hud.app.ui.activity.Main.Module.class, injects = {MainMenuView2.class})
    public class Module {
    }

    @Singleton
    public static class Presenter extends BasePresenter<MainMenuView2> {
        private boolean animateIn;
        @Inject
        Bus bus;
        private boolean closed;
        private IMenu currentMenu;
        private boolean firstLoad;
        private boolean handledSelection;
        private View[] leftView;
        private MainMenu mainMenu;
        private boolean mapShown;
        private MenuMode menuMode;
        private String menuPath;
        private boolean navEnded;
        private boolean registered;
        @Inject
        SharedPreferences sharedPreferences;
        private DisplayMode switchBackMode;
        @Inject
        TripManager tripManager;
        private UIStateManager uiStateManager;

        public void onLoad(Bundle savedInstanceState) {
            reset();
            this.uiStateManager = RemoteDeviceManager.getInstance().getUiStateManager();
            this.bus.register(this);
            this.registered = true;
            this.firstLoad = true;
            this.menuPath = null;
            this.menuMode = MenuMode.MAIN_MENU;
            if (savedInstanceState != null) {
                this.menuPath = savedInstanceState.getString(MainMenuScreen2.ARG_MENU_PATH);
                MainMenuScreen2.sLogger.v("menu_path:" + this.menuPath);
                int modeOrdinal = savedInstanceState.getInt(MainMenuScreen2.ARG_MENU_MODE, -1);
                if (modeOrdinal != -1) {
                    this.menuMode = MenuMode.values()[modeOrdinal];
                    MainMenuScreen2.sLogger.v("menu_mode:" + this.menuMode);
                }
            }
            updateView(savedInstanceState);
            super.onLoad(savedInstanceState);
        }

        public void onUnload() {
            if (this.registered) {
                this.registered = false;
                this.bus.unregister(this);
            }
            reset();
            this.mainMenu = null;
            this.currentMenu = null;
            super.onUnload();
        }

        void reset() {
            this.closed = false;
            this.handledSelection = false;
            this.animateIn = false;
            this.firstLoad = false;
            this.mapShown = false;
            this.navEnded = false;
        }

        protected void updateView(Bundle savedInstanceState) {
            MainMenuView2 view = (MainMenuView2) getView();
            if (view != null) {
                init(view, savedInstanceState);
            }
        }

        void init(MainMenuView2 view, Bundle savedInstanceState) {
            switch (this.menuMode) {
                case MAIN_MENU:
                    this.mainMenu = new MainMenu(this.bus, view.vmenuComponent, this);
                    if (TextUtils.isEmpty(this.menuPath)) {
                        loadMenu(this.mainMenu, null, 0, 0);
                        return;
                    } else {
                        loadMenu(buildMenuPath(this.mainMenu, this.menuPath), null, 0, 0);
                        return;
                    }
                case REPLY_PICKER:
                    ArrayList<Parcelable> list = savedInstanceState.getParcelableArrayList(MainMenuScreen2.ARG_CONTACTS_LIST);
                    String notifId = savedInstanceState.getString(MainMenuScreen2.ARG_NOTIFICATION_ID);
                    List<Contact> contacts = new ArrayList();
                    Iterator it = list.iterator();
                    while (it.hasNext()) {
                        contacts.add((Contact) ((Parcelable) it.next()));
                    }
                    this.currentMenu = new ContactOptionsMenu(contacts, notifId, view.vmenuComponent, this, null, this.bus);
                    loadMenu(this.currentMenu, null, 0, 0);
                    return;
                case SNAPSHOT_TITLE_PICKER:
                    this.currentMenu = new ReportIssueMenu(view.vmenuComponent, this, null, ReportIssueMenuType.SNAP_SHOT);
                    loadMenu(this.currentMenu, null, 0, 0);
                    return;
                default:
                    return;
            }
        }

        boolean selectItem(ItemSelectionState selection) {
            if (this.currentMenu == null) {
                return true;
            }
            if (this.handledSelection) {
                MainMenuScreen2.sLogger.v("already handled [" + selection.id + "], " + selection.pos);
                return true;
            }
            this.handledSelection = this.currentMenu.selectItem(selection);
            return this.handledSelection;
        }

        void resetSelectedItem() {
            MainMenuScreen2.sLogger.v("resetSelectedItem");
            this.handledSelection = false;
            if (!this.animateIn) {
                this.animateIn = true;
                MainMenuView2 view = (MainMenuView2) getView();
                if (view != null) {
                    view.vmenuComponent.animateIn(null);
                }
            }
        }

        void close() {
            if (this.currentMenu != null && this.currentMenu.getType() == Menu.SEARCH) {
                AnalyticsSupport.recordNearbySearchClosed();
            }
            close(null);
        }

        void close(final Runnable endAction) {
            if (this.closed) {
                MainMenuScreen2.sLogger.v("already closed");
                return;
            }
            this.closed = true;
            MainMenuScreen2.sLogger.v("close");
            AnalyticsSupport.recordMenuSelection("exit");
            MainMenuView2 view = (MainMenuView2) getView();
            if (view != null) {
                view.vmenuComponent.animateOut(new DefaultAnimationListener() {
                    public void onAnimationEnd(Animator animation) {
                        MainMenuScreen2.sLogger.v("post back");
                        Presenter.this.bus.post(new Builder().screen(Screen.SCREEN_BACK).build());
                        if (endAction != null) {
                            endAction.run();
                        }
                    }
                });
            }
        }

        boolean isClosed() {
            return this.closed;
        }

        boolean isItemClickable(ItemSelectionState selection) {
            if (this.currentMenu != null) {
                return this.currentMenu.isItemClickable(selection.id, selection.pos);
            }
            return false;
        }

        void loadMenu(IMenu menu, MenuLevel level, int backSelection, int backSelectionId) {
            loadMenu(menu, level, backSelection, backSelectionId, false);
        }

        void loadMenu(IMenu menu, MenuLevel level, int backSelection, int backSelectionId, boolean hasScrollModel) {
            loadMenu(menu, level, backSelection, backSelectionId, hasScrollModel, 0);
        }

        void loadMenu(IMenu menu, MenuLevel level, int backSelection, int backSelectionId, boolean hasScrollModel, int xOffset) {
            hideToolTip();
            if (level == MenuLevel.ROOT && isMapShown()) {
                disableMapViews();
            }
            MainMenuView2 view = (MainMenuView2) getView();
            if (view == null) {
                return;
            }
            if (this.firstLoad) {
                MainMenuScreen2.sLogger.v("Loading menu:" + menu.getType());
                this.currentMenu = menu;
                this.currentMenu.setSelectedIcon();
                this.firstLoad = false;
                view.vmenuComponent.verticalList.setBindCallbacks(this.currentMenu.isBindCallsEnabled());
                view.vmenuComponent.updateView(this.currentMenu.getItems(), this.currentMenu.getInitialSelection(), this.currentMenu.isFirstItemEmpty());
                return;
            }
            Model item;
            switch (level) {
                case SUB_LEVEL:
                    MainMenuScreen2.sLogger.v("Loading menu:" + menu.getType());
                    item = this.currentMenu.getModelfromPos(backSelection);
                    if (this.currentMenu != null) {
                        this.currentMenu.onUnload(MenuLevel.SUB_LEVEL);
                    }
                    this.currentMenu = menu;
                    this.currentMenu.setBackSelectionPos(backSelection);
                    this.currentMenu.setSelectedIcon();
                    final boolean z = hasScrollModel;
                    view.vmenuComponent.performEnterAnimation(new Runnable() {
                        public void run() {
                            MainMenuView2 view = (MainMenuView2) Presenter.this.getView();
                            if (view != null) {
                                MainMenuScreen2.sLogger.v("performEnterAnimation:" + Presenter.this.currentMenu.getType());
                                view.vmenuComponent.unlock(false);
                                view.vmenuComponent.verticalList.setBindCallbacks(Presenter.this.currentMenu.isBindCallsEnabled());
                                view.vmenuComponent.updateView(Presenter.this.currentMenu.getItems(), Presenter.this.currentMenu.getInitialSelection(), true, z, Presenter.this.currentMenu.getScrollIndex());
                            }
                        }
                    }, null, item);
                    return;
                case BACK_TO_PARENT:
                    MainMenuScreen2.sLogger.v("backSelectionId:" + backSelectionId);
                    final List<Model> parentList = menu.getItems();
                    int selection = backSelection;
                    if (backSelectionId != 0) {
                        int index = findEntry(parentList, backSelectionId);
                        if (index != -1) {
                            selection = index;
                        }
                        MainMenuScreen2.sLogger.v("backSelectionId:" + backSelectionId + " index=" + index);
                    }
                    item = null;
                    if (selection < parentList.size()) {
                        item = (Model) parentList.get(selection);
                    }
                    final int itemSelection = selection;
                    final IMenu iMenu = menu;
                    final boolean z2 = hasScrollModel;
                    view.vmenuComponent.performBackAnimation(new Runnable() {
                        public void run() {
                            MainMenuView2 view = (MainMenuView2) Presenter.this.getView();
                            if (view != null) {
                                MainMenuScreen2.sLogger.v("Loading menu:" + iMenu.getType());
                                if (Presenter.this.currentMenu != null) {
                                    Presenter.this.currentMenu.onUnload(MenuLevel.BACK_TO_PARENT);
                                }
                                Presenter.this.currentMenu = iMenu;
                                Presenter.this.currentMenu.setSelectedIcon();
                                MainMenuScreen2.sLogger.v("performBackAnimation:" + Presenter.this.currentMenu.getType());
                                view.vmenuComponent.unlock(false);
                                view.vmenuComponent.verticalList.setBindCallbacks(Presenter.this.currentMenu.isBindCallsEnabled());
                                view.vmenuComponent.updateView(parentList, itemSelection, Presenter.this.currentMenu.isFirstItemEmpty(), z2, Presenter.this.currentMenu.getScrollIndex());
                            }
                        }
                    }, null, item, xOffset);
                    return;
                case REFRESH_CURRENT:
                    if (this.currentMenu == menu) {
                        MainMenuScreen2.sLogger.v("refresh current menu:" + this.currentMenu.getType());
                        view.vmenuComponent.updateView(this.currentMenu.getItems(), this.currentMenu.getInitialSelection(), true, hasScrollModel, view.vmenuComponent.getFastScrollIndex());
                        return;
                    }
                    MainMenuScreen2.sLogger.w("refresh current menu different cur=" + this.currentMenu.getType() + " passed:" + menu.getType());
                    return;
                case ROOT:
                    MainMenuScreen2.sLogger.v("refresh root menu:" + menu.getType());
                    this.currentMenu = menu;
                    this.currentMenu.setSelectedIcon();
                    view.vmenuComponent.updateView(menu.getItems(), menu.getInitialSelection(), this.currentMenu.isFirstItemEmpty());
                    return;
                default:
                    return;
            }
        }

        void updateCurrentMenu(IMenu menu) {
            MainMenuView2 view = (MainMenuView2) getView();
            if (view != null) {
                if (this.closed) {
                    MainMenuScreen2.sLogger.v("updateCurrentMenu already closed");
                } else if (this.currentMenu == menu) {
                    MainMenuScreen2.sLogger.v("updateCurrentMenu:" + menu.getType());
                    view.vmenuComponent.unlock(false);
                    view.vmenuComponent.verticalList.setBindCallbacks(this.currentMenu.isBindCallsEnabled());
                    view.vmenuComponent.updateView(this.currentMenu.getItems(), this.currentMenu.getInitialSelection(), true, false, this.currentMenu.getScrollIndex());
                } else {
                    MainMenuScreen2.sLogger.v("updateCurrentMenu update menu:" + menu.getType() + " different from current:" + this.currentMenu.getType());
                }
            }
        }

        void refreshData(int pos, Model[] models, IMenu menu) {
            MainMenuView2 view = (MainMenuView2) getView();
            if (view != null) {
                if (this.closed) {
                    MainMenuScreen2.sLogger.v("refreshData(s) already closed");
                } else if (this.currentMenu == menu) {
                    MainMenuScreen2.sLogger.v("refreshData(s):" + menu.getType());
                    view.vmenuComponent.verticalList.refreshData(pos, models);
                } else {
                    MainMenuScreen2.sLogger.v("refreshData(s) update menu:" + menu.getType() + " different from current:" + this.currentMenu.getType());
                }
            }
        }

        void performSelectionAnimation(Runnable endAction) {
            performSelectionAnimation(endAction, 0);
        }

        void performSelectionAnimation(Runnable endAction, int delay) {
            MainMenuView2 view = (MainMenuView2) getView();
            if (view != null) {
                view.performSelectionAnimation(endAction, delay);
            }
        }

        ConfirmationLayout getConfirmationLayout() {
            MainMenuView2 view = (MainMenuView2) getView();
            if (view != null) {
                return view.confirmationLayout;
            }
            return null;
        }

        IMenu getCurrentMenu() {
            return this.currentMenu;
        }

        void refreshDataforPos(int pos) {
            refreshDataforPos(pos, true);
        }

        void refreshDataforPos(int pos, boolean startFluctuator) {
            MainMenuView2 view = (MainMenuView2) getView();
            if (view != null) {
                view.vmenuComponent.verticalList.refreshData(pos, null, !startFluctuator);
            }
        }

        int getCurrentSelection() {
            MainMenuView2 view = (MainMenuView2) getView();
            if (view != null) {
                return view.vmenuComponent.verticalList.getCurrentPosition();
            }
            return 0;
        }

        void setInitialItemState(boolean b) {
            MainMenuView2 view = (MainMenuView2) getView();
            if (view != null) {
                view.vmenuComponent.verticalList.adapter.setInitialState(b);
            }
        }

        IMenu buildMenuPath(MainMenu menu, String menuPath) {
            try {
                if (menuPath.indexOf("/") != 0) {
                    MainMenuScreen2.sLogger.w("menu_path does not start with slash");
                    return menu;
                }
                String element = menuPath.substring(1);
                int index = element.indexOf("/");
                if (index >= 0) {
                    menuPath = menuPath.substring(index + 1);
                    element = element.substring(0, index);
                } else {
                    menuPath = null;
                }
                IMenu child = menu.getChildMenu(null, element, menuPath);
                if (child != null) {
                    return child;
                }
                return menu;
            } catch (Throwable t) {
                MainMenuScreen2.sLogger.e(t);
                return menu;
            }
        }

        private int findEntry(List<Model> list, int id) {
            int len = list.size();
            for (int i = 0; i < len; i++) {
                if (((Model) list.get(i)).id == id) {
                    return i;
                }
            }
            return -1;
        }

        void cancelLoadingAnimation(int pos) {
            MainMenuView2 view = (MainMenuView2) getView();
            if (view != null) {
                view.vmenuComponent.verticalList.cancelLoadingAnimation(pos);
            }
        }

        void setScrollIdleEvents(boolean send) {
            MainMenuView2 view = (MainMenuView2) getView();
            if (view != null) {
                view.vmenuComponent.verticalList.setScrollIdleEvent(send);
            }
        }

        @Subscribe
        public void onConnectionStateChange(ConnectionStateChange event) {
            if (this.currentMenu != null) {
                switch (event.state) {
                    case CONNECTION_DISCONNECTED:
                        switch (this.menuMode) {
                            case MAIN_MENU:
                                MainMenuScreen2.sLogger.v("disconnected:" + this.currentMenu.getType());
                                if (this.mainMenu != null) {
                                    this.mainMenu.clearState();
                                }
                                if (this.currentMenu.getType() == Menu.MAIN) {
                                    MainMenuScreen2.sLogger.v("disconnected: refresh main menu");
                                    loadMenu(this.currentMenu, MenuLevel.REFRESH_CURRENT, 0, 0);
                                    return;
                                }
                                MainMenuScreen2.sLogger.v("disconnected: refresh menu,back to root");
                                loadMenu(this.mainMenu, MenuLevel.ROOT, 0, 0);
                                return;
                            case REPLY_PICKER:
                                close();
                                return;
                            default:
                                return;
                        }
                    default:
                        return;
                }
            }
        }

        @Subscribe
        public void onDeviceInfoAvailable(DeviceInfoAvailable event) {
            if (this.currentMenu != null && this.menuMode == MenuMode.MAIN_MENU) {
                MainMenuScreen2.sLogger.v("onDeviceInfoAvailable:" + this.currentMenu.getType());
                if (this.currentMenu.getType() == Menu.MAIN && this.currentMenu.getModelfromPos(this.currentMenu.getInitialSelection()).id == R.id.main_menu_settings_connect_phone) {
                    MainMenuScreen2.sLogger.v("connected: refresh main menu");
                    loadMenu(this.currentMenu, MenuLevel.REFRESH_CURRENT, 0, 0);
                }
            }
        }

        @Subscribe
        public void onNavigationModeChanged(NavigationModeChange event) {
            if (this.currentMenu != null && this.menuMode == MenuMode.MAIN_MENU) {
                MainMenuScreen2.sLogger.v("onNavigationModeChanged:" + event.navigationMode);
                switch (this.currentMenu.getType()) {
                    case MAIN:
                        loadMenu(this.currentMenu, MenuLevel.REFRESH_CURRENT, 0, 0);
                        return;
                    case ACTIVE_TRIP:
                        loadMenu(this.mainMenu, MenuLevel.ROOT, 0, 0);
                        return;
                    default:
                        return;
                }
            }
        }

        @Subscribe
        public void onMapEngineReady(MapEngineReady event) {
            if (this.currentMenu != null && this.menuMode == MenuMode.MAIN_MENU) {
                MainMenuScreen2.sLogger.v("onMapEngineReady");
                switch (this.currentMenu.getType()) {
                    case MAIN:
                        loadMenu(this.currentMenu, MenuLevel.REFRESH_CURRENT, 0, 0);
                        return;
                    default:
                        return;
                }
            }
        }

        void sendCloseEvent() {
            hideMap();
            if (this.currentMenu != this.mainMenu) {
                this.currentMenu.onUnload(MenuLevel.CLOSE);
            }
            if (this.mainMenu != null) {
                this.mainMenu.onUnload(MenuLevel.CLOSE);
            }
        }

        public void showScrimCover() {
            MainMenuView2 view = (MainMenuView2) getView();
            if (view != null) {
                MainMenuScreen2.sLogger.v("scrim-show");
                view.scrimCover.setVisibility(0);
            }
        }

        public void hideScrimCover() {
            MainMenuView2 view = (MainMenuView2) getView();
            if (view != null) {
                MainMenuScreen2.sLogger.v("scrim-hide");
                view.scrimCover.setVisibility(8);
            }
        }

        public void showToolTip(int posIndex, String text) {
            MainMenuView2 view = (MainMenuView2) getView();
            if (view != null) {
                view.vmenuComponent.showToolTip(posIndex, text);
            }
        }

        public void hideToolTip() {
            MainMenuView2 view = (MainMenuView2) getView();
            if (view != null) {
                view.vmenuComponent.hideToolTip();
            }
        }

        public int getCurrentSubSelectionId() {
            MainMenuView2 view = (MainMenuView2) getView();
            if (view != null) {
                VerticalViewHolder vh = view.vmenuComponent.verticalList.getCurrentViewHolder();
                if (vh instanceof IconOptionsViewHolder) {
                    return ((IconOptionsViewHolder) vh).getCurrentSelectionId();
                }
            }
            return -1;
        }

        public int getCurrentSubSelectionPos() {
            MainMenuView2 view = (MainMenuView2) getView();
            if (view != null) {
                VerticalViewHolder vh = view.vmenuComponent.verticalList.getCurrentViewHolder();
                if (vh instanceof IconOptionsViewHolder) {
                    return ((IconOptionsViewHolder) vh).getCurrentSelection();
                }
            }
            return -1;
        }

        public boolean isMapShown() {
            return this.mapShown;
        }

        public void showMap() {
            if (this.mapShown) {
                MainMenuScreen2.sLogger.v("showMap: already shown");
                return;
            }
            this.mapShown = true;
            if (((MainMenuView2) getView()) != null) {
                GeoBoundingBox boundingBox;
                GeoBoundingBox boundingBox2;
                MainMenuScreen2.sLogger.v("showMap");
                HomeScreenView homeScreenView = this.uiStateManager.getHomescreenView();
                NavigationView navigationView = this.uiStateManager.getNavigationView();
                this.switchBackMode = homeScreenView.getDisplayMode();
                if (this.switchBackMode == DisplayMode.MAP) {
                    MainMenuScreen2.sLogger.v("showMap: switchbackmode null");
                    this.switchBackMode = null;
                } else {
                    MainMenuScreen2.sLogger.v(" showMap switchbackmode: " + this.switchBackMode);
                    homeScreenView.setDisplayMode(DisplayMode.MAP);
                }
                HereNavigationManager hereNavigationManager = HereNavigationManager.getInstance();
                GeoCoordinate start = HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
                GeoCoordinate end = null;
                NavigationRouteRequest current = hereNavigationManager.getCurrentNavigationRouteRequest();
                if (current != null) {
                    if (current.destination.latitude.doubleValue() != 0.0d && current.destination.longitude.doubleValue() != 0.0d) {
                        end = new GeoCoordinate(current.destination.latitude.doubleValue(), current.destination.longitude.doubleValue());
                    } else if (!(current.destinationDisplay == null || current.destinationDisplay.latitude.doubleValue() == 0.0d || current.destinationDisplay.longitude.doubleValue() == 0.0d)) {
                        end = new GeoCoordinate(current.destinationDisplay.latitude.doubleValue(), current.destinationDisplay.longitude.doubleValue());
                    }
                }
                String routeId = hereNavigationManager.getCurrentRouteId();
                Route route = null;
                if (routeId != null) {
                    RouteInfo routeInfo = HereRouteCache.getInstance().getRoute(routeId);
                    if (routeInfo != null) {
                        route = routeInfo.route;
                        boundingBox = routeInfo.route.getBoundingBox();
                        if (boundingBox == null && start != null) {
                            boundingBox2 = new GeoBoundingBox(start, 5000.0f, 5000.0f);
                            navigationView.switchToRouteSearchMode(start, end, boundingBox2, route, false, null, -1);
                            enableMapViews();
                        }
                        boundingBox2 = boundingBox;
                        navigationView.switchToRouteSearchMode(start, end, boundingBox2, route, false, null, -1);
                        enableMapViews();
                    }
                }
                boundingBox = null;
                try {
                    boundingBox2 = new GeoBoundingBox(start, 5000.0f, 5000.0f);
                } catch (Throwable t) {
                    MainMenuScreen2.sLogger.e(t);
                }
                navigationView.switchToRouteSearchMode(start, end, boundingBox2, route, false, null, -1);
                enableMapViews();
            }
        }

        public void showBoundingBox(GeoBoundingBox boundingBox) {
            if (((MainMenuView2) getView()) != null && this.mapShown && boundingBox != null) {
                this.uiStateManager.getNavigationView().zoomToBoundBox(boundingBox, null, false, false);
            }
        }

        public GeoBoundingBox getCurrentRouteBoundingBox() {
            String routeId = HereNavigationManager.getInstance().getCurrentRouteId();
            if (routeId != null) {
                RouteInfo routeInfo = HereRouteCache.getInstance().getRoute(routeId);
                if (routeInfo != null) {
                    return routeInfo.route.getBoundingBox();
                }
            }
            return null;
        }

        public void setNavEnded() {
            this.navEnded = true;
        }

        public void hideMap() {
            boolean z = true;
            if (this.mapShown) {
                this.mapShown = false;
                if (((MainMenuView2) getView()) != null) {
                    MainMenuScreen2.sLogger.v("hideMap");
                    disableMapViews();
                    if (this.switchBackMode != null) {
                        MainMenuScreen2.sLogger.v("hideMap switchbackmode: " + this.switchBackMode);
                        this.uiStateManager.getHomescreenView().setDisplayMode(this.switchBackMode);
                    }
                    NavigationView navigationView = this.uiStateManager.getNavigationView();
                    if (!this.navEnded) {
                        HereNavigationManager hereNavigationManager = HereNavigationManager.getInstance();
                        if (!hereNavigationManager.isNavigationModeOn()) {
                            this.navEnded = true;
                        } else if (hereNavigationManager.hasArrived()) {
                            this.navEnded = true;
                        }
                    }
                    if (this.navEnded) {
                        z = false;
                    }
                    navigationView.switchBackfromRouteSearchMode(z);
                }
            }
        }

        public void cleanMapFluctuator() {
            MainMenuScreen2.sLogger.v("cleanMapFluctuator");
            NavigationView navigationView = this.uiStateManager.getNavigationView();
            if (navigationView != null) {
                navigationView.cleanupFluctuator();
            }
        }

        public void setViewBackgroundColor(int color) {
            MainMenuView2 view = (MainMenuView2) getView();
            if (view != null) {
                view.setBackgroundColor(color);
            }
        }

        public void enableMapViews() {
            MainMenuView2 view = (MainMenuView2) getView();
            if (view != null) {
                int nChilds = view.vmenuComponent.leftContainer.getChildCount();
                MainMenuScreen2.sLogger.v("enableMapViews:" + nChilds);
                this.leftView = new View[nChilds];
                for (int i = 0; i < nChilds; i++) {
                    this.leftView[i] = view.vmenuComponent.leftContainer.getChildAt(i);
                }
                view.vmenuComponent.leftContainer.removeAllViews();
                view.vmenuComponent.leftContainer.setBackgroundColor(0);
                view.vmenuComponent.leftContainer.addView(LayoutInflater.from(view.getContext()).inflate(R.layout.active_trip_menu_mask_lyt, view, false));
                view.vmenuComponent.rightContainer.setBackgroundColor(-16777216);
                view.vmenuComponent.closeContainer.setBackgroundColor(-16777216);
                view.rightBackground.setVisibility(0);
            }
        }

        public void disableMapViews() {
            MainMenuView2 view = (MainMenuView2) getView();
            if (view != null) {
                MainMenuScreen2.sLogger.v("disableMapViews");
                view.setBackgroundColor(-16777216);
                view.vmenuComponent.leftContainer.removeAllViews();
                view.vmenuComponent.leftContainer.setBackgroundColor(-16777216);
                if (this.leftView != null) {
                    for (View addView : this.leftView) {
                        view.vmenuComponent.leftContainer.addView(addView);
                    }
                    this.leftView = null;
                }
                view.vmenuComponent.rightContainer.setBackgroundColor(0);
                view.vmenuComponent.closeContainer.setBackgroundColor(0);
                view.rightBackground.setVisibility(8);
            }
        }
    }

    public String getMortarScopeName() {
        return getClass().getName();
    }

    public Object getDaggerModule() {
        return new Module();
    }

    public Screen getScreen() {
        return Screen.SCREEN_MAIN_MENU;
    }

    public int getAnimationIn(Direction direction) {
        return -1;
    }

    public int getAnimationOut(Direction direction) {
        return -1;
    }
}
