package com.navdy.hud.app.ui.component.image;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.ImageView;

public class ColorImageView extends ImageView {
    private int color;
    private Paint paint;

    public ColorImageView(Context context) {
        this(context, null, 0);
    }

    public ColorImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ColorImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        this.paint = new Paint();
        this.paint.setStrokeWidth(0.0f);
        this.paint.setAntiAlias(true);
    }

    public void setColor(int color) {
        this.color = color;
        invalidate();
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int width = getWidth();
        int height = getHeight();
        canvas.drawColor(0);
        this.paint.setColor(this.color);
        canvas.drawCircle((float) (width / 2), (float) (height / 2), (float) (width / 2), this.paint);
    }
}
