package com.navdy.hud.app.ui.component.vlist.viewholder;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode;
import com.navdy.hud.app.ui.component.image.IconColorImageView;
import com.navdy.hud.app.ui.component.image.IconColorImageView.IconShape;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType;
import com.navdy.hud.app.ui.component.vlist.VerticalModelCache;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State;
import com.navdy.service.library.log.Logger;

public class IconBkColorViewHolder extends IconBaseViewHolder {
    private static final Logger sLogger = VerticalViewHolder.sLogger;

    public static Model buildModel(int id, int icon, int iconSelectedColor, int iconDeselectedColor, int iconFluctuatorColor, String title, String subTitle) {
        return buildModel(id, icon, iconSelectedColor, iconDeselectedColor, iconFluctuatorColor, title, subTitle, null, IconShape.CIRCLE);
    }

    public static Model buildModel(int id, int icon, int iconSelectedColor, int iconDeselectedColor, int iconFluctuatorColor, String title, String subTitle, String subTitle2) {
        return buildModel(id, icon, iconSelectedColor, iconDeselectedColor, iconFluctuatorColor, title, subTitle, subTitle2, IconShape.CIRCLE);
    }

    public static Model buildModel(int id, int icon, int iconSelectedColor, int iconDeselectedColor, int iconFluctuatorColor, String title, String subTitle, String subTitle2, IconShape iconShape) {
        Model model = VerticalModelCache.getFromCache(ModelType.ICON_BKCOLOR);
        if (model == null) {
            model = new Model();
        }
        model.type = ModelType.ICON_BKCOLOR;
        model.id = id;
        model.icon = icon;
        model.iconSelectedColor = iconSelectedColor;
        model.iconDeselectedColor = iconDeselectedColor;
        model.iconFluctuatorColor = iconFluctuatorColor;
        model.title = title;
        model.subTitle = subTitle;
        model.subTitle2 = subTitle2;
        model.iconShape = iconShape;
        return model;
    }

    public static IconBkColorViewHolder buildViewHolder(ViewGroup parent, VerticalList vlist, Handler handler) {
        return new IconBkColorViewHolder(IconBaseViewHolder.getLayout(parent, R.layout.vlist_item, R.layout.crossfade_image_bkcolor_lyt), vlist, handler);
    }

    private IconBkColorViewHolder(ViewGroup layout, VerticalList vlist, Handler handler) {
        super(layout, vlist, handler);
    }

    public ModelType getModelType() {
        return ModelType.ICON_BKCOLOR;
    }

    public void setItemState(State state, AnimationType animation, int duration, boolean startFluctuator) {
        super.setItemState(state, animation, duration, startFluctuator);
        float iconScaleFactor = 0.0f;
        Mode mode = null;
        switch (state) {
            case SELECTED:
                iconScaleFactor = 1.0f;
                mode = Mode.BIG;
                break;
            case UNSELECTED:
                iconScaleFactor = 0.6f;
                mode = Mode.SMALL;
                break;
        }
        switch (animation) {
            case NONE:
            case INIT:
                this.imageContainer.setScaleX(iconScaleFactor);
                this.imageContainer.setScaleY(iconScaleFactor);
                this.crossFadeImageView.setMode(mode);
                return;
            case MOVE:
                PropertyValuesHolder p1 = PropertyValuesHolder.ofFloat(View.SCALE_X, new float[]{iconScaleFactor});
                PropertyValuesHolder p2 = PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[]{iconScaleFactor});
                this.animatorSetBuilder.with(ObjectAnimator.ofPropertyValuesHolder(this.imageContainer, new PropertyValuesHolder[]{p1, p2}));
                if (this.crossFadeImageView.getMode() != mode) {
                    this.animatorSetBuilder.with(this.crossFadeImageView.getCrossFadeAnimator());
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void preBind(Model model, ModelState modelState) {
        super.preBind(model, modelState);
        ((IconColorImageView) this.crossFadeImageView.getBig()).setIconShape(model.iconShape);
        ((IconColorImageView) this.crossFadeImageView.getSmall()).setIconShape(model.iconShape);
    }

    public void bind(Model model, ModelState modelState) {
        super.bind(model, modelState);
        setIcon(model.icon, model.iconSelectedColor, model.iconDeselectedColor, modelState.updateImage, modelState.updateSmallImage);
    }

    private void setIcon(int icon, int iconSelectedColor, int iconDeselectedColor, boolean updateImage, boolean updateSmallImage) {
        IconColorImageView big = (IconColorImageView) this.crossFadeImageView.getBig();
        if (updateImage) {
            big.setDraw(true);
            big.setIcon(icon, iconSelectedColor, null, 0.83f);
        } else {
            big.setDraw(false);
        }
        IconColorImageView small = (IconColorImageView) this.crossFadeImageView.getSmall();
        if (updateSmallImage) {
            small.setDraw(true);
            small.setIcon(icon, iconDeselectedColor, null, 0.83f);
            return;
        }
        small.setDraw(false);
    }
}
