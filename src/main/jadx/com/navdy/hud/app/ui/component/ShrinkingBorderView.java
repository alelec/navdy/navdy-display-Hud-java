package com.navdy.hud.app.ui.component;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;

public class ShrinkingBorderView extends View {
    private static final int FORCE_COLLAPSE_INTERVAL = 300;
    private static final int RESET_TIMEOUT_INITIAL_INTERVAL = 300;
    private static final int RESET_TIMEOUT_INTERVAL = 100;
    private ValueAnimator animator;
    private AnimatorListener animatorListener;
    private AnimatorListener animatorResetListener;
    private AnimatorUpdateListener animatorResetUpdateListener;
    private AnimatorUpdateListener animatorUpdateListener;
    private Runnable forceCompleteCallback;
    private AnimatorListener forceCompleteListener;
    private Interpolator interpolator;
    private IListener listener;
    private Interpolator restoreInterpolator;
    private int timeoutVal;

    public interface IListener {
        void timeout();
    }

    public ShrinkingBorderView(Context context) {
        this(context, null);
    }

    public ShrinkingBorderView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ShrinkingBorderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.restoreInterpolator = new LinearInterpolator();
        this.interpolator = new AccelerateInterpolator();
        this.forceCompleteListener = new DefaultAnimationListener() {
            public void onAnimationEnd(Animator animation) {
                ShrinkingBorderView.this.animator = null;
                if (ShrinkingBorderView.this.forceCompleteCallback != null) {
                    ShrinkingBorderView.this.forceCompleteCallback.run();
                    ShrinkingBorderView.this.forceCompleteCallback = null;
                }
            }
        };
        this.animatorListener = new DefaultAnimationListener() {
            public void onAnimationEnd(Animator animation) {
                ShrinkingBorderView.this.animator = null;
                if (ShrinkingBorderView.this.listener != null) {
                    ShrinkingBorderView.this.listener.timeout();
                }
            }
        };
        this.animatorUpdateListener = new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                MarginLayoutParams lytParams = (MarginLayoutParams) ShrinkingBorderView.this.getLayoutParams();
                int val = ((Integer) animation.getAnimatedValue()).intValue();
                if (lytParams.height != val) {
                    lytParams.height = val;
                    ShrinkingBorderView.this.requestLayout();
                }
            }
        };
        this.animatorResetListener = new DefaultAnimationListener() {
            public void onAnimationEnd(Animator animation) {
                ShrinkingBorderView.this.animator = null;
                ShrinkingBorderView.this.startTimeout(ShrinkingBorderView.this.timeoutVal, true);
            }
        };
        this.animatorResetUpdateListener = new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                ((MarginLayoutParams) ShrinkingBorderView.this.getLayoutParams()).height = ((Integer) animation.getAnimatedValue()).intValue();
                ShrinkingBorderView.this.requestLayout();
            }
        };
    }

    public void setListener(IListener listener) {
        this.listener = listener;
    }

    public void startTimeout(int timeout) {
        startTimeout(timeout, false);
    }

    private void startTimeout(int timeout, boolean reset) {
        if (timeout != 0 && this.animator == null) {
            this.timeoutVal = timeout;
            if (reset) {
                this.animator = ValueAnimator.ofInt(new int[]{((MarginLayoutParams) getLayoutParams()).height, 0});
                this.animator.addUpdateListener(this.animatorUpdateListener);
                this.animator.addListener(this.animatorListener);
                this.animator.setDuration((long) timeout);
                this.animator.setInterpolator(this.interpolator);
                this.animator.start();
                return;
            }
            ((MarginLayoutParams) getLayoutParams()).height = 0;
            requestLayout();
            resetTimeout(true);
        }
    }

    public void resetTimeout() {
        resetTimeout(false);
    }

    public void resetTimeout(boolean initial) {
        int interval;
        clearAnimator();
        if (initial) {
            interval = 300;
        } else {
            interval = 100;
        }
        MarginLayoutParams lytParams = (MarginLayoutParams) getLayoutParams();
        int parentHeight = ((ViewGroup) getParent()).getHeight();
        this.animator = ValueAnimator.ofInt(new int[]{lytParams.height, parentHeight});
        this.animator.addUpdateListener(this.animatorResetUpdateListener);
        this.animator.addListener(this.animatorResetListener);
        this.animator.setDuration((long) interval);
        this.animator.setInterpolator(this.restoreInterpolator);
        if (initial) {
            this.animator.setStartDelay(300);
        }
        this.animator.start();
    }

    public void stopTimeout(boolean force, Runnable forceCompleteCallback) {
        if (force) {
            clearAnimator();
            if (forceCompleteCallback != null) {
                this.forceCompleteCallback = forceCompleteCallback;
                this.animator = ValueAnimator.ofInt(new int[]{((MarginLayoutParams) getLayoutParams()).height, 0});
                this.animator.addUpdateListener(this.animatorResetUpdateListener);
                this.animator.addListener(this.forceCompleteListener);
                this.animator.setDuration(300);
                this.animator.setInterpolator(this.interpolator);
                this.animator.start();
                return;
            }
            ((MarginLayoutParams) getLayoutParams()).height = 0;
            requestLayout();
        } else if (this.animator == null) {
            ((MarginLayoutParams) getLayoutParams()).height = 0;
            requestLayout();
        }
    }

    public boolean isVisible() {
        if (((MarginLayoutParams) getLayoutParams()).height == 0) {
            return false;
        }
        return true;
    }

    private void clearAnimator() {
        if (this.animator != null) {
            this.animator.removeAllListeners();
            this.animator.cancel();
            this.animator = null;
            this.forceCompleteCallback = null;
        }
    }
}
