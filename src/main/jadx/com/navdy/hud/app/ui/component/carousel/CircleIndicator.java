package com.navdy.hud.app.ui.component.carousel;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.RectF;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;

class CircleIndicator extends RelativeLayout implements IProgressIndicator {
    private int circleFocusSize;
    private int circleMargin;
    private int circleSize;
    private final Context context;
    private int currentItem = -1;
    private int currentItemColor = -1;
    private int defaultColor;
    private int id = 100;
    private int itemCount;
    private View moveAnimatorView;
    private Orientation orientation;

    public CircleIndicator(Context context) {
        super(context);
        this.context = context;
    }

    public CircleIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    public void setProperties(int circleSize, int circleFocusSize, int circleMargin, int defaultColor) {
        this.circleSize = circleSize;
        this.circleFocusSize = circleFocusSize;
        this.circleMargin = circleMargin;
        this.defaultColor = defaultColor;
    }

    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }

    public AnimatorSet getItemMoveAnimator(final int toPos, int color) {
        AnimatorSet set = null;
        if (this.currentItem != -1 && toPos >= 0 && toPos < this.itemCount) {
            View currentItemView = getChildAt(this.currentItem);
            View targetItemView = getChildAt(toPos);
            GradientDrawable background = (GradientDrawable) this.moveAnimatorView.getBackground();
            if (color == -1) {
                background.setColor(this.defaultColor);
            } else {
                background.setColor(color);
            }
            set = new AnimatorSet();
            set.addListener(new DefaultAnimationListener() {
                public void onAnimationEnd(Animator animation) {
                    CircleIndicator.this.currentItem = toPos;
                }
            });
            set.play(ObjectAnimator.ofFloat(this.moveAnimatorView, this.orientation == Orientation.HORIZONTAL ? View.X : View.Y, new float[]{(float) getTargetViewPos(targetItemView)}));
        }
        return set;
    }

    public RectF getItemPos(int n) {
        if (n < 0 || n >= this.itemCount) {
            return null;
        }
        View view = getChildAt(n);
        View parent = (View) view.getParent();
        return new RectF((float) (view.getLeft() + parent.getLeft()), (float) (view.getTop() + parent.getTop()), 0.0f, 0.0f);
    }

    public void setItemCount(int n) {
        if (n < 0) {
            throw new IllegalArgumentException();
        }
        this.currentItem = -1;
        if (getChildCount() == 0) {
            this.itemCount = n;
            buildItems(n);
        } else if (this.itemCount != n) {
            int diff = this.itemCount - n;
            if (diff > 0) {
                this.itemCount = n;
                removeView(this.moveAnimatorView);
                for (int i = 0; i < diff; i++) {
                    removeView(getChildAt(getChildCount() - 1));
                }
                int count = getChildCount();
                if (count > 0) {
                    MarginLayoutParams params = (MarginLayoutParams) getChildAt(count - 1).getLayoutParams();
                    if (this.orientation == Orientation.HORIZONTAL) {
                        params.rightMargin = this.circleMargin;
                    } else {
                        params.bottomMargin = this.circleMargin;
                    }
                }
                addView(this.moveAnimatorView);
                return;
            }
            this.itemCount = n;
            removeView(this.moveAnimatorView);
            buildItems(-diff);
            addView(this.moveAnimatorView);
        }
    }

    private void buildItems(int n) {
        if (n != 0) {
            View prev = null;
            int nItems = getChildCount();
            if (nItems > 0) {
                prev = getChildAt(nItems - 1);
                MarginLayoutParams params = (MarginLayoutParams) prev.getLayoutParams();
                if (this.orientation == Orientation.HORIZONTAL) {
                    params.rightMargin = 0;
                } else {
                    params.bottomMargin = 0;
                }
            }
            for (int i = nItems; i < n + nItems; i++) {
                View view = new View(this.context);
                int i2 = this.id;
                this.id = i2 + 1;
                view.setId(i2);
                view.setBackgroundResource(R.drawable.circle_page_indicator);
                LayoutParams lytParams = new LayoutParams(this.circleSize, this.circleSize);
                if (i == 0 && prev == null) {
                    if (this.orientation == Orientation.HORIZONTAL) {
                        lytParams.addRule(9);
                        lytParams.leftMargin = this.circleMargin;
                    } else {
                        lytParams.addRule(10);
                        lytParams.topMargin = this.circleMargin;
                    }
                }
                if (i == this.itemCount - 1) {
                    if (this.orientation == Orientation.HORIZONTAL) {
                        lytParams.rightMargin = this.circleMargin;
                    } else {
                        lytParams.bottomMargin = this.circleMargin;
                    }
                }
                if (this.orientation == Orientation.HORIZONTAL) {
                    lytParams.addRule(15);
                    if (i > 0) {
                        lytParams.leftMargin = this.circleMargin;
                    }
                    if (prev != null) {
                        lytParams.addRule(1, prev.getId());
                    }
                } else {
                    lytParams.addRule(14);
                    if (i > 0) {
                        lytParams.topMargin = this.circleMargin;
                    }
                    if (prev != null) {
                        lytParams.addRule(3, prev.getId());
                    }
                }
                view.setAlpha(0.5f);
                addView(view, lytParams);
                prev = view;
            }
            if (this.moveAnimatorView == null) {
                this.moveAnimatorView = new View(this.context);
                this.moveAnimatorView.setBackgroundResource(R.drawable.circle_page_indicator_focus);
                this.moveAnimatorView.setLayoutParams(new LayoutParams(this.circleFocusSize, this.circleFocusSize));
                this.moveAnimatorView.setVisibility(8);
                addView(this.moveAnimatorView);
            }
        }
    }

    public int getCurrentItem() {
        return this.currentItem;
    }

    public void setCurrentItem(int n, int color) {
        if (this.currentItem != n) {
            this.currentItemColor = color;
            if (n >= 0 && n < this.itemCount) {
                int pos;
                final View view = getChildAt(n);
                if (this.orientation == Orientation.HORIZONTAL) {
                    pos = view.getLeft();
                } else {
                    pos = view.getTop();
                }
                if (pos == 0) {
                    view.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
                        public void onGlobalLayout() {
                            int pos;
                            if (CircleIndicator.this.orientation == Orientation.HORIZONTAL) {
                                pos = view.getLeft();
                            } else {
                                pos = view.getTop();
                            }
                            if (pos != 0) {
                                view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                                CircleIndicator.this.setViewPos(view);
                            }
                        }
                    });
                } else {
                    setViewPos(view);
                }
            }
            this.currentItem = n;
        }
    }

    public void setCurrentItem(int n) {
        setCurrentItem(n, -1);
    }

    private int getTargetViewPos(View targetView) {
        if (this.orientation == Orientation.HORIZONTAL) {
            return (targetView.getLeft() + (targetView.getWidth() / 2)) - (this.circleFocusSize / 2);
        }
        return (targetView.getTop() + (targetView.getHeight() / 2)) - (this.circleFocusSize / 2);
    }

    private void setViewPos(View view) {
        float pos = (float) getTargetViewPos(view);
        int itemColor = this.currentItemColor;
        if (itemColor == -1) {
            itemColor = this.defaultColor;
        }
        if (this.orientation == Orientation.HORIZONTAL) {
            this.moveAnimatorView.setX(pos);
        } else {
            this.moveAnimatorView.setY(pos);
        }
        ((GradientDrawable) this.moveAnimatorView.getBackground()).setColor(itemColor);
        this.moveAnimatorView.setVisibility(0);
        this.moveAnimatorView.invalidate();
    }
}
