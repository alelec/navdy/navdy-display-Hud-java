package com.navdy.hud.app.ui.activity;

import android.content.SharedPreferences;
import com.navdy.hud.app.config.SettingsManager;
import com.navdy.hud.app.device.PowerManager;
import com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler;
import com.navdy.hud.app.framework.phonecall.CallManager;
import com.navdy.hud.app.framework.trips.TripManager;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.hud.app.manager.MusicManager;
import com.navdy.hud.app.profile.DriverProfileManager;
import com.navdy.hud.app.service.ConnectionHandler;
import com.navdy.hud.app.service.pandora.PandoraManager;
import com.navdy.hud.app.ui.activity.Main.Presenter;
import com.navdy.hud.app.ui.framework.BasePresenter;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.squareup.otto.Bus;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;
import javax.inject.Provider;

public final class Main$Presenter$$InjectAdapter extends Binding<Presenter> implements Provider<Presenter>, MembersInjector<Presenter> {
    private Binding<Bus> bus;
    private Binding<CallManager> callManager;
    private Binding<ConnectionHandler> connectionHandler;
    private Binding<DialSimulatorMessagesHandler> dialSimulatorMessagesHandler;
    private Binding<DriverProfileManager> driverProfileManager;
    private Binding<SharedPreferences> globalPreferences;
    private Binding<InputManager> inputManager;
    private Binding<MusicManager> musicManager;
    private Binding<PandoraManager> pandoraManager;
    private Binding<PowerManager> powerManager;
    private Binding<SharedPreferences> preferences;
    private Binding<SettingsManager> settingsManager;
    private Binding<BasePresenter> supertype;
    private Binding<TripManager> tripManager;
    private Binding<UIStateManager> uiStateManager;

    public Main$Presenter$$InjectAdapter() {
        super("com.navdy.hud.app.ui.activity.Main$Presenter", "members/com.navdy.hud.app.ui.activity.Main$Presenter", true, Presenter.class);
    }

    public void attach(Linker linker) {
        this.bus = linker.requestBinding("com.squareup.otto.Bus", Presenter.class, getClass().getClassLoader());
        this.inputManager = linker.requestBinding("com.navdy.hud.app.manager.InputManager", Presenter.class, getClass().getClassLoader());
        this.connectionHandler = linker.requestBinding("com.navdy.hud.app.service.ConnectionHandler", Presenter.class, getClass().getClassLoader());
        this.uiStateManager = linker.requestBinding("com.navdy.hud.app.ui.framework.UIStateManager", Presenter.class, getClass().getClassLoader());
        this.callManager = linker.requestBinding("com.navdy.hud.app.framework.phonecall.CallManager", Presenter.class, getClass().getClassLoader());
        this.musicManager = linker.requestBinding("com.navdy.hud.app.manager.MusicManager", Presenter.class, getClass().getClassLoader());
        this.pandoraManager = linker.requestBinding("com.navdy.hud.app.service.pandora.PandoraManager", Presenter.class, getClass().getClassLoader());
        this.dialSimulatorMessagesHandler = linker.requestBinding("com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler", Presenter.class, getClass().getClassLoader());
        this.globalPreferences = linker.requestBinding("android.content.SharedPreferences", Presenter.class, getClass().getClassLoader());
        this.settingsManager = linker.requestBinding("com.navdy.hud.app.config.SettingsManager", Presenter.class, getClass().getClassLoader());
        this.powerManager = linker.requestBinding("com.navdy.hud.app.device.PowerManager", Presenter.class, getClass().getClassLoader());
        this.driverProfileManager = linker.requestBinding("com.navdy.hud.app.profile.DriverProfileManager", Presenter.class, getClass().getClassLoader());
        this.tripManager = linker.requestBinding("com.navdy.hud.app.framework.trips.TripManager", Presenter.class, getClass().getClassLoader());
        this.preferences = linker.requestBinding("android.content.SharedPreferences", Presenter.class, getClass().getClassLoader());
        Linker linker2 = linker;
        this.supertype = linker2.requestBinding("members/com.navdy.hud.app.ui.framework.BasePresenter", Presenter.class, getClass().getClassLoader(), false, true);
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.bus);
        injectMembersBindings.add(this.inputManager);
        injectMembersBindings.add(this.connectionHandler);
        injectMembersBindings.add(this.uiStateManager);
        injectMembersBindings.add(this.callManager);
        injectMembersBindings.add(this.musicManager);
        injectMembersBindings.add(this.pandoraManager);
        injectMembersBindings.add(this.dialSimulatorMessagesHandler);
        injectMembersBindings.add(this.globalPreferences);
        injectMembersBindings.add(this.settingsManager);
        injectMembersBindings.add(this.powerManager);
        injectMembersBindings.add(this.driverProfileManager);
        injectMembersBindings.add(this.tripManager);
        injectMembersBindings.add(this.preferences);
        injectMembersBindings.add(this.supertype);
    }

    public Presenter get() {
        Presenter result = new Presenter();
        injectMembers(result);
        return result;
    }

    public void injectMembers(Presenter object) {
        object.bus = (Bus) this.bus.get();
        object.inputManager = (InputManager) this.inputManager.get();
        object.connectionHandler = (ConnectionHandler) this.connectionHandler.get();
        object.uiStateManager = (UIStateManager) this.uiStateManager.get();
        object.callManager = (CallManager) this.callManager.get();
        object.musicManager = (MusicManager) this.musicManager.get();
        object.pandoraManager = (PandoraManager) this.pandoraManager.get();
        object.dialSimulatorMessagesHandler = (DialSimulatorMessagesHandler) this.dialSimulatorMessagesHandler.get();
        object.globalPreferences = (SharedPreferences) this.globalPreferences.get();
        object.settingsManager = (SettingsManager) this.settingsManager.get();
        object.powerManager = (PowerManager) this.powerManager.get();
        object.driverProfileManager = (DriverProfileManager) this.driverProfileManager.get();
        object.tripManager = (TripManager) this.tripManager.get();
        object.preferences = (SharedPreferences) this.preferences.get();
        this.supertype.injectMembers(object);
    }
}
