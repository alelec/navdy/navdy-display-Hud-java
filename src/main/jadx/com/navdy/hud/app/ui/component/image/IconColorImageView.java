package com.navdy.hud.app.ui.component.image;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

public class IconColorImageView extends ImageView {
    private int bkColor;
    private Shader bkColorGradient;
    private Context context;
    private boolean draw;
    private int iconResource;
    private IconShape iconShape;
    private Paint paint;
    private float scaleF;

    public enum IconShape {
        CIRCLE,
        SQUARE
    }

    public IconColorImageView(Context context) {
        this(context, null, 0);
    }

    public IconColorImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public IconColorImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.iconShape = IconShape.CIRCLE;
        this.draw = true;
        this.context = context;
        init();
    }

    private void init() {
        this.paint = new Paint();
        this.paint.setStrokeWidth(0.0f);
        this.paint.setAntiAlias(true);
    }

    public void setIcon(int iconResource, int bkColor, Shader gradient, float scaleF) {
        this.iconResource = iconResource;
        this.bkColor = bkColor;
        this.bkColorGradient = gradient;
        this.scaleF = scaleF;
        invalidate();
    }

    public void setIconShape(IconShape iconShape) {
        this.iconShape = iconShape;
    }

    protected void onDraw(Canvas canvas) {
        int width = getWidth();
        int height = getHeight();
        canvas.drawColor(0);
        if (this.bkColorGradient != null) {
            this.paint.setShader(this.bkColorGradient);
        } else {
            this.paint.setShader(null);
            this.paint.setColor(this.bkColor);
        }
        if (this.iconShape == null || this.iconShape == IconShape.CIRCLE) {
            canvas.drawCircle((float) (width / 2), (float) (height / 2), (float) (width / 2), this.paint);
        } else {
            canvas.drawRect(0.0f, 0.0f, (float) width, (float) height, this.paint);
        }
        if (!this.draw) {
            super.onDraw(canvas);
        } else if (this.iconResource != 0) {
            Drawable drawable = this.context.getDrawable(this.iconResource);
            if (drawable != null) {
                Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                if (bitmap != null) {
                    canvas.scale(this.scaleF, this.scaleF);
                    canvas.drawBitmap(bitmap, 0.0f, 0.0f, null);
                }
            }
        }
    }

    public void setDraw(boolean b) {
        this.draw = b;
    }
}
