package com.navdy.hud.app.ui.component.carousel;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.view.View;
import com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction;

public class ShrinkAnimator extends CarouselAnimator {
    public Animator createViewOutAnimation(CarouselLayout carousel, Direction direction) {
        View target;
        float newX;
        if (direction == Direction.RIGHT) {
            target = carousel.rightView;
            newX = ((target.getX() + ((float) target.getMeasuredWidth())) + ((float) (carousel.viewPadding / 2))) - (20.0f / 2.0f);
        } else {
            target = carousel.leftView;
            newX = (target.getX() - ((float) (carousel.viewPadding / 2))) - (20.0f / 2.0f);
        }
        float newY = (target.getY() + ((float) (target.getMeasuredHeight() / 2))) - (20.0f / 2.0f);
        float scaleFactor = 20.0f / ((float) target.getMeasuredWidth());
        AnimatorSet animatorSet = new AnimatorSet();
        Animator[] animatorArr = new Animator[4];
        animatorArr[0] = ObjectAnimator.ofFloat(target, "x", new float[]{newX});
        animatorArr[1] = ObjectAnimator.ofFloat(target, "y", new float[]{newY});
        animatorArr[2] = ObjectAnimator.ofFloat(target, "scaleX", new float[]{scaleFactor});
        animatorArr[3] = ObjectAnimator.ofFloat(target, "scaleY", new float[]{scaleFactor});
        animatorSet.playTogether(animatorArr);
        target.setPivotX(0.5f);
        target.setPivotY(0.5f);
        return animatorSet;
    }

    public AnimatorSet createHiddenViewAnimation(CarouselLayout carousel, Direction direction) {
        View srcView;
        View targetPosView;
        int i;
        AnimatorSet animatorSet = new AnimatorSet();
        if (direction == Direction.RIGHT) {
            srcView = carousel.newLeftView;
            targetPosView = carousel.leftView;
        } else {
            srcView = carousel.newRightView;
            targetPosView = carousel.rightView;
        }
        float scaleFactor = 20.0f / ((float) targetPosView.getMeasuredWidth());
        srcView.setScaleX(scaleFactor);
        srcView.setScaleY(scaleFactor);
        float x = targetPosView.getX();
        if (direction == Direction.RIGHT) {
            i = -1;
        } else {
            i = 1;
        }
        x += (float) ((i * carousel.viewPadding) / 2);
        if (direction == Direction.RIGHT) {
            i = 0;
        } else {
            i = targetPosView.getMeasuredWidth();
        }
        srcView.setX(((float) i) + x);
        srcView.setY((targetPosView.getY() + ((float) (targetPosView.getMeasuredHeight() / 2))) - (20.0f / 2.0f));
        Animator[] animatorArr = new Animator[4];
        animatorArr[0] = ObjectAnimator.ofFloat(srcView, "x", new float[]{targetPosView.getX()});
        animatorArr[1] = ObjectAnimator.ofFloat(srcView, "y", new float[]{targetPosView.getY()});
        animatorArr[2] = ObjectAnimator.ofFloat(srcView, "scaleX", new float[]{1.0f});
        animatorArr[3] = ObjectAnimator.ofFloat(srcView, "scaleY", new float[]{1.0f});
        animatorSet.playTogether(animatorArr);
        srcView.setPivotX(0.5f);
        srcView.setPivotY(0.5f);
        return animatorSet;
    }
}
