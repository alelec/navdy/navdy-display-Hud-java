package com.navdy.hud.app.ui.component.carousel;

import android.animation.AnimatorSet;
import android.graphics.RectF;
import com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation;

public interface IProgressIndicator {
    int getCurrentItem();

    AnimatorSet getItemMoveAnimator(int i, int i2);

    RectF getItemPos(int i);

    void setCurrentItem(int i);

    void setCurrentItem(int i, int i2);

    void setItemCount(int i);

    void setOrientation(Orientation orientation);
}
