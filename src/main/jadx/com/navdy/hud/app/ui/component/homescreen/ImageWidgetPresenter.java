package com.navdy.hud.app.ui.component.homescreen;

import android.content.Context;
import android.graphics.drawable.Drawable;
import com.navdy.hud.app.R;
import com.navdy.hud.app.view.DashboardWidgetPresenter;
import com.navdy.hud.app.view.DashboardWidgetView;

public class ImageWidgetPresenter extends DashboardWidgetPresenter {
    private String id;
    private Drawable mDrawable;

    public ImageWidgetPresenter(Context context, int imageResource, String id) {
        if (imageResource > 0) {
            this.mDrawable = context.getResources().getDrawable(imageResource);
        }
        this.id = id;
    }

    public void setView(DashboardWidgetView dashboardWidgetView) {
        if (dashboardWidgetView != null) {
            dashboardWidgetView.setContentView((int) R.layout.smart_dash_widget_layout);
        }
        super.setView(dashboardWidgetView);
    }

    public Drawable getDrawable() {
        return this.mDrawable;
    }

    protected void updateGauge() {
    }

    public String getWidgetIdentifier() {
        return this.id;
    }

    public String getWidgetName() {
        return null;
    }
}
