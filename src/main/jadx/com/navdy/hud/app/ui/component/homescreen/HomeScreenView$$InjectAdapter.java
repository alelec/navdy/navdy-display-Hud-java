package com.navdy.hud.app.ui.component.homescreen;

import android.content.SharedPreferences;
import com.navdy.hud.app.ui.component.homescreen.HomeScreen.Presenter;
import com.squareup.otto.Bus;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class HomeScreenView$$InjectAdapter extends Binding<HomeScreenView> implements MembersInjector<HomeScreenView> {
    private Binding<Bus> bus;
    private Binding<SharedPreferences> globalPreferences;
    private Binding<Presenter> presenter;

    public HomeScreenView$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.ui.component.homescreen.HomeScreenView", false, HomeScreenView.class);
    }

    public void attach(Linker linker) {
        this.presenter = linker.requestBinding("com.navdy.hud.app.ui.component.homescreen.HomeScreen$Presenter", HomeScreenView.class, getClass().getClassLoader());
        this.bus = linker.requestBinding("com.squareup.otto.Bus", HomeScreenView.class, getClass().getClassLoader());
        this.globalPreferences = linker.requestBinding("android.content.SharedPreferences", HomeScreenView.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.presenter);
        injectMembersBindings.add(this.bus);
        injectMembersBindings.add(this.globalPreferences);
    }

    public void injectMembers(HomeScreenView object) {
        object.presenter = (Presenter) this.presenter.get();
        object.bus = (Bus) this.bus.get();
        object.globalPreferences = (SharedPreferences) this.globalPreferences.get();
    }
}
