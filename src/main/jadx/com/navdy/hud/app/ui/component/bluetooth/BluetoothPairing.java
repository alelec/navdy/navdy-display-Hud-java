package com.navdy.hud.app.ui.component.bluetooth;

import android.bluetooth.BluetoothDevice;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.device.dial.DialManager;
import com.navdy.hud.app.framework.toast.IToastCallback;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.framework.toast.ToastManager.ToastParams;
import com.navdy.hud.app.framework.toast.ToastPresenter;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.ui.component.ChoiceLayout.Choice;
import com.navdy.hud.app.util.BluetoothUtil;
import com.navdy.hud.app.view.ToastView;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.util.ArrayList;

public class BluetoothPairing implements IToastCallback {
    public static final String ARG_AUTO_ACCEPT = "auto";
    public static final String ARG_DEVICE = "device";
    public static final String ARG_PIN = "pin";
    public static final String ARG_VARIANT = "variant";
    private static final String BLUETOOTH_PAIRING_TOAST_ID = "bt-pairing";
    private static final Logger sLogger = new Logger(BluetoothPairing.class);
    private boolean acceptDone;
    private boolean autoAccept;
    private Bus bus;
    private ArrayList<Choice> choices = new ArrayList(2);
    private boolean confirmationRequired;
    private BluetoothDevice device;
    private boolean initialized;
    private int pin;
    private int variant;

    public static class BondStateChange {
        public final BluetoothDevice device;
        public final int newState;
        public final int oldState;

        public BondStateChange(BluetoothDevice device, int oldState, int newState) {
            this.device = device;
            this.oldState = oldState;
            this.newState = newState;
        }
    }

    public static class PairingCancelled {
        public final BluetoothDevice device;

        public PairingCancelled(BluetoothDevice device) {
            this.device = device;
        }
    }

    public static void showBluetoothPairingToast(Bundle bundle) {
        sLogger.v("showBluetoothPairingToast");
        BluetoothDevice device = (BluetoothDevice) bundle.getParcelable(ARG_DEVICE);
        int pin = bundle.getInt(ARG_PIN);
        int variant = bundle.getInt(ARG_VARIANT);
        boolean autoAccept = bundle.getBoolean("auto");
        if (!Boolean.TRUE.equals(Boolean.valueOf(DialManager.getInstance().isDialConnected()))) {
            autoAccept = true;
        }
        BluetoothPairing pairingToast = new BluetoothPairing(device, pin, variant, autoAccept, RemoteDeviceManager.getInstance().getBus());
        sLogger.v("show()");
        pairingToast.show();
    }

    BluetoothPairing(BluetoothDevice device, int pin, int variant, boolean autoAccept, Bus bus) {
        this.device = device;
        this.pin = pin;
        this.variant = variant;
        this.autoAccept = autoAccept;
        this.bus = bus;
    }

    void show() {
        boolean required;
        Resources resources = HudApplication.getAppContext().getResources();
        Bundle data = new Bundle();
        data.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, R.drawable.icon_bluetooth_connecting);
        data.putString(ToastPresenter.EXTRA_MAIN_TITLE, resources.getString(R.string.bluetooth_pairing_title));
        data.putString(ToastPresenter.EXTRA_MAIN_TITLE_1, resources.getString(R.string.bluetooth_pairing_code));
        String displayName = null;
        if (this.device != null) {
            displayName = this.device.getName();
            if (TextUtils.isEmpty(displayName)) {
                displayName = this.device.getAddress();
            }
        }
        if (displayName == null) {
            displayName = resources.getString(R.string.unknown_device);
        }
        data.putString(ToastPresenter.EXTRA_MAIN_TITLE_3, resources.getString(R.string.bluetooth_device, new Object[]{displayName}));
        data.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, formatPin(this.pin, this.variant));
        if (this.autoAccept) {
            required = false;
        } else {
            required = true;
        }
        if (!(required == this.confirmationRequired && this.initialized)) {
            this.initialized = true;
            this.confirmationRequired = required;
            if (required) {
                this.choices.add(new Choice(resources.getString(R.string.bluetooth_confirm), 0));
                this.choices.add(new Choice(resources.getString(R.string.cancel), 0));
            } else {
                this.choices.add(new Choice(resources.getString(R.string.cancel), 0));
            }
            data.putParcelableArrayList(ToastPresenter.EXTRA_CHOICE_LIST, this.choices);
        }
        ToastManager toastManager = ToastManager.getInstance();
        toastManager.dismissCurrentToast();
        toastManager.clearAllPendingToast();
        toastManager.addToast(new ToastParams(BLUETOOTH_PAIRING_TOAST_ID, data, this, false, true, true));
    }

    public void onStart(ToastView view) {
        sLogger.v("registerBus");
        this.bus.register(this);
        if (!this.acceptDone && this.autoAccept) {
            this.acceptDone = true;
            sLogger.v("auto-accept");
            BluetoothUtil.confirmPairing(this.device, this.variant, this.pin);
        }
    }

    public void onStop() {
        sLogger.v("unregisterBus");
        this.bus.unregister(this);
    }

    public boolean onKey(CustomKeyEvent event) {
        return false;
    }

    public void executeChoiceItem(int pos, int id) {
        sLogger.v("execute:" + pos);
        if (!this.confirmationRequired) {
            finish();
        } else if (pos == 0) {
            confirm();
        } else {
            finish();
        }
    }

    private String formatPin(int pin, int variant) {
        if (variant == 5) {
            return String.format("%04d", new Object[]{Integer.valueOf(pin)});
        }
        return String.format("%06d", new Object[]{Integer.valueOf(pin)});
    }

    @Subscribe
    public void onPairingCancelled(PairingCancelled event) {
        sLogger.v("onPairingCancelled");
        finish();
    }

    @Subscribe
    public void onBondStateChange(BondStateChange event) {
        sLogger.v("onBondStateChange");
        if (!event.device.equals(this.device)) {
            return;
        }
        if (event.newState == 10 || event.newState == 12) {
            sLogger.i("ending pairing dialog because of bond state change");
            finish();
            return;
        }
        sLogger.i("Ignoring bond state change for " + event.device.getAddress() + " state: " + event.newState + " dialog device:" + this.device.getAddress());
    }

    private void finish() {
        sLogger.v("finish()");
        ToastManager.getInstance().dismissCurrentToast();
    }

    private void confirm() {
        sLogger.v("confirm()");
        BluetoothUtil.confirmPairing(this.device, this.variant, this.pin);
        finish();
    }
}
