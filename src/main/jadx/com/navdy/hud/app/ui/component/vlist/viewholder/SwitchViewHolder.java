package com.navdy.hud.app.ui.component.vlist.viewholder;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.AnimatorSet.Builder;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.graphics.Color;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Handler;
import android.text.Html;
import android.text.TextUtils.TruncateAt;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.ImageView;
import android.widget.TextView;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.SwitchHaloView;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType;
import com.navdy.hud.app.ui.component.vlist.VerticalModelCache;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State;
import com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils;
import com.navdy.service.library.log.Logger;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u00a1\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0017\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006*\u0001\u0019\u0018\u0000 u2\u00020\u0001:\u0001uB\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\u0018\u0010P\u001a\u00020Q2\u0006\u0010R\u001a\u00020S2\u0006\u0010T\u001a\u00020UH\u0016J\b\u0010V\u001a\u00020QH\u0016J0\u0010W\u001a\u00020Q2\u0006\u0010X\u001a\u00020Y2\u0006\u0010Z\u001a\u00020-2\u0006\u0010[\u001a\u00020-2\u0006\u0010\\\u001a\u00020-2\u0006\u0010]\u001a\u00020\"H\u0016J\u0016\u0010^\u001a\u00020Q2\u0006\u0010+\u001a\u00020\"2\u0006\u0010*\u001a\u00020\"J\b\u0010_\u001a\u00020`H\u0016J\b\u0010a\u001a\u00020\nH\u0002J\b\u0010b\u001a\u00020\nH\u0002J\u0010\u0010c\u001a\u00020\n2\u0006\u0010d\u001a\u00020eH\u0002J\u0018\u0010f\u001a\u00020Q2\u0006\u0010R\u001a\u00020S2\u0006\u0010T\u001a\u00020UH\u0016J \u0010g\u001a\u00020Q2\u0006\u0010R\u001a\u00020S2\u0006\u0010h\u001a\u00020\n2\u0006\u0010i\u001a\u00020\nH\u0016J\u0010\u0010j\u001a\u00020Q2\u0006\u0010k\u001a\u00020\nH\u0002J(\u0010l\u001a\u00020Q2\u0006\u0010m\u001a\u00020n2\u0006\u0010o\u001a\u00020p2\u0006\u0010i\u001a\u00020\n2\u0006\u0010q\u001a\u00020\"H\u0016J\u001a\u00100\u001a\u00020Q2\b\u0010r\u001a\u0004\u0018\u00010e2\u0006\u0010s\u001a\u00020\"H\u0002J\u001a\u00104\u001a\u00020Q2\b\u0010r\u001a\u0004\u0018\u00010e2\u0006\u0010s\u001a\u00020\"H\u0002J\u0010\u0010L\u001a\u00020Q2\u0006\u0010r\u001a\u00020eH\u0002J\b\u0010q\u001a\u00020QH\u0016J\b\u0010t\u001a\u00020QH\u0002R\u0014\u0010\t\u001a\u00020\nX\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0014\u0010\r\u001a\u00020\nX\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\fR \u0010\u000f\u001a\b\u0018\u00010\u0010R\u00020\u0011X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\u001aR\u001a\u0010\u001b\u001a\u00020\u001cX\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001d\u0010\u001e\"\u0004\b\u001f\u0010 R\u000e\u0010!\u001a\u00020\"X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020\"X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020\"X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010%\u001a\u00020\"X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b&\u0010'\"\u0004\b(\u0010)R\u000e\u0010*\u001a\u00020\"X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010+\u001a\u00020\"X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010,\u001a\u00020-X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b.\u0010/\"\u0004\b0\u00101R\u001a\u00102\u001a\u00020-X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b3\u0010/\"\u0004\b4\u00101R\u001a\u00105\u001a\u000206X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b7\u00108\"\u0004\b9\u0010:R\u001a\u0010;\u001a\u00020\u0003X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b<\u0010=\"\u0004\b>\u0010?R\u001a\u0010@\u001a\u000206X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bA\u00108\"\u0004\bB\u0010:R\u001a\u0010C\u001a\u00020\"X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bD\u0010'\"\u0004\bE\u0010)R\u001a\u0010F\u001a\u00020\u0003X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bG\u0010=\"\u0004\bH\u0010?R\u000e\u0010I\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010J\u001a\u00020-X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bK\u0010/\"\u0004\bL\u00101R\u000e\u0010M\u001a\u00020NX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010O\u001a\u00020NX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006v"}, d2 = {"Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;", "Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;", "layout", "Landroid/view/ViewGroup;", "vlist", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList;", "handler", "Landroid/os/Handler;", "(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)V", "FLUCTUATOR_OPACITY_ALPHA", "", "getFLUCTUATOR_OPACITY_ALPHA", "()I", "HALO_DELAY_START_DURATION", "getHALO_DELAY_START_DURATION", "animatorSetBuilder", "Landroid/animation/AnimatorSet$Builder;", "Landroid/animation/AnimatorSet;", "getAnimatorSetBuilder", "()Landroid/animation/AnimatorSet$Builder;", "setAnimatorSetBuilder", "(Landroid/animation/AnimatorSet$Builder;)V", "fluctuatorRunnable", "Ljava/lang/Runnable;", "fluctuatorStartListener", "com/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$fluctuatorStartListener$1", "Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$fluctuatorStartListener$1;", "haloView", "Lcom/navdy/hud/app/ui/component/SwitchHaloView;", "getHaloView", "()Lcom/navdy/hud/app/ui/component/SwitchHaloView;", "setHaloView", "(Lcom/navdy/hud/app/ui/component/SwitchHaloView;)V", "hasIconFluctuatorColor", "", "hasSubTitle", "hasSubTitle2", "iconScaleAnimationDisabled", "getIconScaleAnimationDisabled", "()Z", "setIconScaleAnimationDisabled", "(Z)V", "isEnabled", "isOn", "subTitle", "Landroid/widget/TextView;", "getSubTitle", "()Landroid/widget/TextView;", "setSubTitle", "(Landroid/widget/TextView;)V", "subTitle2", "getSubTitle2", "setSubTitle2", "switchBackground", "Landroid/view/View;", "getSwitchBackground", "()Landroid/view/View;", "setSwitchBackground", "(Landroid/view/View;)V", "switchContainer", "getSwitchContainer", "()Landroid/view/ViewGroup;", "setSwitchContainer", "(Landroid/view/ViewGroup;)V", "switchThumb", "getSwitchThumb", "setSwitchThumb", "textAnimationDisabled", "getTextAnimationDisabled", "setTextAnimationDisabled", "textContainer", "getTextContainer", "setTextContainer", "thumbMargin", "title", "getTitle", "setTitle", "titleSelectedTopMargin", "", "titleUnselectedScale", "bind", "", "model", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;", "modelState", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;", "clearAnimation", "copyAndPosition", "imageC", "Landroid/widget/ImageView;", "titleC", "subTitleC", "subTitle2C", "setImage", "drawSwitch", "getModelType", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;", "getSubTitle2Color", "getSubTitleColor", "getTextColor", "property", "", "preBind", "select", "pos", "duration", "setIconFluctuatorColor", "color", "setItemState", "state", "Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;", "animation", "Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;", "startFluctuator", "str", "formatted", "stopFluctuator", "Companion", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
/* compiled from: SwitchViewHolder.kt */
public final class SwitchViewHolder extends VerticalViewHolder {
    public static final Companion Companion = new Companion();
    @NotNull
    private static final Logger logger = new Logger(Companion.getClass());
    private final int FLUCTUATOR_OPACITY_ALPHA = IconBaseViewHolder.FLUCTUATOR_OPACITY_ALPHA;
    private final int HALO_DELAY_START_DURATION = 100;
    @Nullable
    private Builder animatorSetBuilder;
    private final Runnable fluctuatorRunnable;
    private final SwitchViewHolder$fluctuatorStartListener$1 fluctuatorStartListener;
    @NotNull
    private SwitchHaloView haloView;
    private boolean hasIconFluctuatorColor;
    private boolean hasSubTitle;
    private boolean hasSubTitle2;
    private boolean iconScaleAnimationDisabled;
    private boolean isEnabled;
    private boolean isOn;
    @NotNull
    private TextView subTitle;
    @NotNull
    private TextView subTitle2;
    @NotNull
    private View switchBackground;
    @NotNull
    private ViewGroup switchContainer;
    @NotNull
    private View switchThumb;
    private boolean textAnimationDisabled;
    @NotNull
    private ViewGroup textContainer;
    private int thumbMargin;
    @NotNull
    private TextView title;
    private float titleSelectedTopMargin;
    private float titleUnselectedScale;

    @Metadata(bv = {1, 0, 1}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J:\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\r2\b\b\u0002\u0010\u000f\u001a\u00020\u00102\b\b\u0002\u0010\u0011\u001a\u00020\u0010J\u001e\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0019J\u0016\u0010\u001a\u001a\u00020\u00152\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u001b\u001a\u00020\nR\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u001c"}, d2 = {"Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$Companion;", "", "()V", "logger", "Lcom/navdy/service/library/log/Logger;", "getLogger", "()Lcom/navdy/service/library/log/Logger;", "buildModel", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;", "id", "", "iconFluctuatorColor", "title", "", "subTitle", "isOn", "", "isEnabled", "buildViewHolder", "Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;", "parent", "Landroid/view/ViewGroup;", "vlist", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList;", "handler", "Landroid/os/Handler;", "getLayout", "lytId", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
    /* compiled from: SwitchViewHolder.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker $constructor_marker) {
            this();
        }

        @NotNull
        public final Logger getLogger() {
            return SwitchViewHolder.logger;
        }

        @NotNull
        public static /* bridge */ /* synthetic */ Model buildModel$default(Companion companion, int i, int i2, String str, String str2, boolean z, boolean z2, int i3, Object obj) {
            boolean z3 = false;
            boolean z4 = (i3 & 16) != 0 ? false : z;
            if ((i3 & 32) == 0) {
                z3 = z2;
            }
            return companion.buildModel(i, i2, str, str2, z4, z3);
        }

        @NotNull
        public final Model buildModel(int id, int iconFluctuatorColor, @NotNull String title, @NotNull String subTitle, boolean isOn, boolean isEnabled) {
            Intrinsics.checkParameterIsNotNull(title, "title");
            Intrinsics.checkParameterIsNotNull(subTitle, "subTitle");
            Model model = VerticalModelCache.getFromCache(ModelType.SWITCH);
            if (model == null) {
                model = new Model();
            }
            model.type = ModelType.SWITCH;
            model.id = id;
            model.iconFluctuatorColor = iconFluctuatorColor;
            model.title = title;
            model.subTitle = subTitle;
            model.isOn = isOn;
            model.isEnabled = isEnabled;
            return model;
        }

        @NotNull
        public final SwitchViewHolder buildViewHolder(@NotNull ViewGroup parent, @NotNull VerticalList vlist, @NotNull Handler handler) {
            Intrinsics.checkParameterIsNotNull(parent, "parent");
            Intrinsics.checkParameterIsNotNull(vlist, "vlist");
            Intrinsics.checkParameterIsNotNull(handler, "handler");
            return new SwitchViewHolder(getLayout(parent, R.layout.vlist_toggle_switch), vlist, handler);
        }

        @NotNull
        public final ViewGroup getLayout(@NotNull ViewGroup parent, int lytId) {
            Intrinsics.checkParameterIsNotNull(parent, "parent");
            ViewGroup layout = LayoutInflater.from(parent.getContext()).inflate(lytId, parent, false);
            if (layout != null) {
                return layout;
            }
            throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup");
        }
    }

    public SwitchViewHolder(@NotNull ViewGroup layout, @NotNull VerticalList vlist, @NotNull Handler handler) {
        Intrinsics.checkParameterIsNotNull(layout, "layout");
        Intrinsics.checkParameterIsNotNull(vlist, "vlist");
        Intrinsics.checkParameterIsNotNull(handler, "handler");
        super(layout, vlist, handler);
        this.fluctuatorStartListener = new SwitchViewHolder$fluctuatorStartListener$1(this, handler);
        this.fluctuatorRunnable = new SwitchViewHolder$fluctuatorRunnable$1(this);
        View findViewById = layout.findViewById(R.id.textContainer);
        if (findViewById == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup");
        }
        this.textContainer = (ViewGroup) findViewById;
        findViewById = layout.findViewById(R.id.title);
        if (findViewById == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.widget.TextView");
        }
        this.title = (TextView) findViewById;
        findViewById = layout.findViewById(R.id.subTitle);
        if (findViewById == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.widget.TextView");
        }
        this.subTitle = (TextView) findViewById;
        findViewById = layout.findViewById(R.id.subTitle2);
        if (findViewById == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.widget.TextView");
        }
        this.subTitle2 = (TextView) findViewById;
        findViewById = layout.findViewById(R.id.toggleSwitchBackground);
        Intrinsics.checkExpressionValueIsNotNull(findViewById, "layout.findViewById(R.id.toggleSwitchBackground)");
        this.switchBackground = findViewById;
        findViewById = layout.findViewById(R.id.toggleSwitchThumb);
        Intrinsics.checkExpressionValueIsNotNull(findViewById, "layout.findViewById(R.id.toggleSwitchThumb)");
        this.switchThumb = findViewById;
        findViewById = layout.findViewById(R.id.imageContainer);
        if (findViewById == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup");
        }
        this.switchContainer = (ViewGroup) findViewById;
        this.thumbMargin = layout.getResources().getDimensionPixelSize(R.dimen.vlist_toggle_switch_thumb_margin);
        findViewById = layout.findViewById(R.id.halo);
        if (findViewById == null) {
            throw new TypeCastException("null cannot be cast to non-null type com.navdy.hud.app.ui.component.SwitchHaloView");
        }
        this.haloView = (SwitchHaloView) findViewById;
        this.haloView.setVisibility(8);
    }

    public final int getFLUCTUATOR_OPACITY_ALPHA() {
        return this.FLUCTUATOR_OPACITY_ALPHA;
    }

    public final int getHALO_DELAY_START_DURATION() {
        return this.HALO_DELAY_START_DURATION;
    }

    @NotNull
    protected final ViewGroup getTextContainer() {
        return this.textContainer;
    }

    protected final void setTextContainer(@NotNull ViewGroup <set-?>) {
        Intrinsics.checkParameterIsNotNull(<set-?>, "<set-?>");
        this.textContainer = <set-?>;
    }

    @NotNull
    protected final TextView getTitle() {
        return this.title;
    }

    protected final void setTitle(@NotNull TextView <set-?>) {
        Intrinsics.checkParameterIsNotNull(<set-?>, "<set-?>");
        this.title = <set-?>;
    }

    @NotNull
    protected final TextView getSubTitle() {
        return this.subTitle;
    }

    protected final void setSubTitle(@NotNull TextView <set-?>) {
        Intrinsics.checkParameterIsNotNull(<set-?>, "<set-?>");
        this.subTitle = <set-?>;
    }

    @NotNull
    protected final TextView getSubTitle2() {
        return this.subTitle2;
    }

    protected final void setSubTitle2(@NotNull TextView <set-?>) {
        Intrinsics.checkParameterIsNotNull(<set-?>, "<set-?>");
        this.subTitle2 = <set-?>;
    }

    @NotNull
    protected final View getSwitchBackground() {
        return this.switchBackground;
    }

    protected final void setSwitchBackground(@NotNull View <set-?>) {
        Intrinsics.checkParameterIsNotNull(<set-?>, "<set-?>");
        this.switchBackground = <set-?>;
    }

    @NotNull
    protected final View getSwitchThumb() {
        return this.switchThumb;
    }

    protected final void setSwitchThumb(@NotNull View <set-?>) {
        Intrinsics.checkParameterIsNotNull(<set-?>, "<set-?>");
        this.switchThumb = <set-?>;
    }

    @NotNull
    protected final ViewGroup getSwitchContainer() {
        return this.switchContainer;
    }

    protected final void setSwitchContainer(@NotNull ViewGroup <set-?>) {
        Intrinsics.checkParameterIsNotNull(<set-?>, "<set-?>");
        this.switchContainer = <set-?>;
    }

    @NotNull
    protected final SwitchHaloView getHaloView() {
        return this.haloView;
    }

    protected final void setHaloView(@NotNull SwitchHaloView <set-?>) {
        Intrinsics.checkParameterIsNotNull(<set-?>, "<set-?>");
        this.haloView = <set-?>;
    }

    protected final boolean getTextAnimationDisabled() {
        return this.textAnimationDisabled;
    }

    protected final void setTextAnimationDisabled(boolean <set-?>) {
        this.textAnimationDisabled = <set-?>;
    }

    protected final boolean getIconScaleAnimationDisabled() {
        return this.iconScaleAnimationDisabled;
    }

    protected final void setIconScaleAnimationDisabled(boolean <set-?>) {
        this.iconScaleAnimationDisabled = <set-?>;
    }

    @Nullable
    protected final Builder getAnimatorSetBuilder() {
        return this.animatorSetBuilder;
    }

    protected final void setAnimatorSetBuilder(@Nullable Builder <set-?>) {
        this.animatorSetBuilder = <set-?>;
    }

    public void setItemState(@NotNull State state, @NotNull AnimationType animation, int duration, boolean startFluctuator) {
        Builder builder;
        PropertyValuesHolder p1;
        PropertyValuesHolder p2;
        PropertyValuesHolder p3;
        Intrinsics.checkParameterIsNotNull(state, TransferTable.COLUMN_STATE);
        Intrinsics.checkParameterIsNotNull(animation, "animation");
        float switchScaleFactor = 0.0f;
        float titleScaleFactor = 0.0f;
        float titleTopMargin = 0.0f;
        float subTitleAlpha = 0.0f;
        float subTitleScaleFactor = 0.0f;
        float subTitle2Alpha = 0.0f;
        float subTitle2ScaleFactor = 0.0f;
        Object itemState = state;
        this.animatorSetBuilder = (Builder) null;
        if (this.textAnimationDisabled) {
            if (Intrinsics.areEqual( animation,  AnimationType.MOVE)) {
                this.itemAnimatorSet = new AnimatorSet();
                float[] fArr = new float[2];
                this.animatorSetBuilder = this.itemAnimatorSet.play(ValueAnimator.ofFloat(new float[]{0.0f, 1.0f}));
                return;
            }
            itemState = State.SELECTED;
        }
        switch (itemState) {
            case SELECTED:
                switchScaleFactor = 1.0f;
                titleScaleFactor = 1.0f;
                titleTopMargin = this.titleSelectedTopMargin;
                subTitleAlpha = 1.0f;
                subTitleScaleFactor = 1.0f;
                subTitle2Alpha = 1.0f;
                subTitle2ScaleFactor = 1.0f;
                break;
            case UNSELECTED:
                switchScaleFactor = 0.6f;
                titleScaleFactor = this.titleUnselectedScale;
                titleTopMargin = 0.0f;
                subTitleAlpha = 0.0f;
                subTitleScaleFactor = this.titleUnselectedScale;
                subTitle2Alpha = 0.0f;
                subTitle2ScaleFactor = this.titleUnselectedScale;
                break;
        }
        if (!this.hasSubTitle) {
            titleTopMargin = 0.0f;
            subTitleAlpha = 0.0f;
            subTitle2Alpha = 0.0f;
        }
        switch (animation) {
            case INIT:
            case NONE:
                this.switchContainer.setScaleX(switchScaleFactor);
                this.switchContainer.setScaleY(switchScaleFactor);
                break;
            case MOVE:
                this.itemAnimatorSet = new AnimatorSet();
                PropertyValuesHolder switchP1 = PropertyValuesHolder.ofFloat(View.SCALE_X, new float[]{switchScaleFactor});
                PropertyValuesHolder switchP2 = PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[]{switchScaleFactor});
                this.animatorSetBuilder = this.itemAnimatorSet.play(ObjectAnimator.ofPropertyValuesHolder(this.title, new PropertyValuesHolder[]{switchP1, switchP2}));
                builder = this.animatorSetBuilder;
                if (builder == null) {
                    Intrinsics.throwNpe();
                }
                builder.with(ObjectAnimator.ofPropertyValuesHolder(this.switchContainer, new PropertyValuesHolder[]{switchP1, switchP2}));
                break;
        }
        this.title.setPivotX(0.0f);
        this.title.setPivotY(VerticalViewHolder.titleHeight / ((float) 2));
        switch (animation) {
            case INIT:
            case NONE:
                this.title.setScaleX(titleScaleFactor);
                this.title.setScaleY(titleScaleFactor);
                MarginLayoutParams layoutParams = this.title.getLayoutParams();
                if (layoutParams != null) {
                    layoutParams.topMargin = (int) titleTopMargin;
                    this.title.requestLayout();
                    break;
                }
                throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            case MOVE:
                p1 = PropertyValuesHolder.ofFloat(View.SCALE_X, new float[]{titleScaleFactor});
                p2 = PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[]{titleScaleFactor});
                builder = this.animatorSetBuilder;
                if (builder == null) {
                    Intrinsics.throwNpe();
                }
                builder.with(ObjectAnimator.ofPropertyValuesHolder(this.title, new PropertyValuesHolder[]{p1, p2}));
                builder = this.animatorSetBuilder;
                if (builder == null) {
                    Intrinsics.throwNpe();
                }
                builder.with(VerticalAnimationUtils.animateMargin(this.title, (int) titleTopMargin));
                break;
        }
        this.subTitle.setPivotX(0.0f);
        this.subTitle.setPivotY(VerticalViewHolder.subTitleHeight / ((float) 2));
        AnimationType animation2 = animation;
        if (!this.hasSubTitle) {
            animation2 = AnimationType.NONE;
        }
        switch (animation2) {
            case INIT:
            case NONE:
                this.subTitle.setAlpha(subTitleAlpha);
                this.subTitle.setScaleX(subTitleScaleFactor);
                this.subTitle.setScaleY(subTitleScaleFactor);
                break;
            case MOVE:
                p1 = PropertyValuesHolder.ofFloat(View.SCALE_X, new float[]{subTitleScaleFactor});
                p2 = PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[]{subTitleScaleFactor});
                p3 = PropertyValuesHolder.ofFloat(View.ALPHA, new float[]{subTitleAlpha});
                builder = this.animatorSetBuilder;
                if (builder == null) {
                    Intrinsics.throwNpe();
                }
                builder.with(ObjectAnimator.ofPropertyValuesHolder(this.subTitle, new PropertyValuesHolder[]{p1, p2, p3}));
                break;
        }
        this.subTitle2.setPivotX(0.0f);
        this.subTitle2.setPivotY(VerticalViewHolder.subTitleHeight / ((float) 2));
        animation2 = animation;
        if (!this.hasSubTitle2) {
            animation2 = AnimationType.NONE;
        }
        switch (animation2) {
            case INIT:
            case NONE:
                this.subTitle2.setAlpha(subTitle2Alpha);
                this.subTitle2.setScaleX(subTitle2ScaleFactor);
                this.subTitle2.setScaleY(subTitle2ScaleFactor);
                break;
            case MOVE:
                p1 = PropertyValuesHolder.ofFloat(View.SCALE_X, new float[]{subTitle2ScaleFactor});
                p2 = PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[]{subTitle2ScaleFactor});
                p3 = PropertyValuesHolder.ofFloat(View.ALPHA, new float[]{subTitleAlpha});
                builder = this.animatorSetBuilder;
                if (builder == null) {
                    Intrinsics.throwNpe();
                }
                builder.with(ObjectAnimator.ofPropertyValuesHolder(this.subTitle2, new PropertyValuesHolder[]{p1, p2, p3}));
                break;
        }
        if (!Intrinsics.areEqual(itemState,  State.SELECTED) || !startFluctuator) {
            return;
        }
        if (this.itemAnimatorSet != null) {
            this.itemAnimatorSet.addListener(this.fluctuatorStartListener);
            return;
        }
        startFluctuator();
    }

    public void clearAnimation() {
        stopFluctuator();
        stopAnimation();
        this.currentState = (State) null;
        this.layout.setVisibility(0);
        this.layout.setAlpha(1.0f);
    }

    private final void setTitle(String str) {
        this.title.setText(str);
    }

    private final void setSubTitle(String str, boolean formatted) {
        if (str == null) {
            this.subTitle.setText("");
            this.hasSubTitle = false;
            return;
        }
        int c = getSubTitleColor();
        if (c == 0) {
            this.subTitle.setTextColor(VerticalViewHolder.subTitleColor);
        } else {
            this.subTitle.setTextColor(c);
        }
        if (formatted) {
            this.subTitle.setText(Html.fromHtml(str));
        } else {
            this.subTitle.setText(str);
        }
        this.hasSubTitle = true;
    }

    private final void setSubTitle2(String str, boolean formatted) {
        if (str == null) {
            this.subTitle2.setText("");
            this.subTitle2.setVisibility(8);
            this.hasSubTitle2 = false;
            return;
        }
        int c = getSubTitle2Color();
        if (c == 0) {
            this.subTitle2.setTextColor(VerticalViewHolder.subTitle2Color);
        } else {
            this.subTitle2.setTextColor(c);
        }
        if (formatted) {
            this.subTitle2.setText(Html.fromHtml(str));
        } else {
            this.subTitle2.setText(str);
        }
        this.subTitle2.setVisibility(0);
        this.hasSubTitle2 = true;
    }

    private final void setIconFluctuatorColor(int color) {
        if (color != 0) {
            int fluctuatorColor = Color.argb(this.FLUCTUATOR_OPACITY_ALPHA, Color.red(color), Color.green(color), Color.blue(color));
            this.hasIconFluctuatorColor = true;
            this.haloView.setStrokeColor(fluctuatorColor);
            return;
        }
        this.hasIconFluctuatorColor = false;
    }

    public void startFluctuator() {
        if (this.hasIconFluctuatorColor) {
            this.haloView.setVisibility(0);
            this.haloView.start();
        }
    }

    private final void stopFluctuator() {
        if (this.hasIconFluctuatorColor) {
            this.handler.removeCallbacks(this.fluctuatorRunnable);
            this.haloView.setVisibility(8);
            this.haloView.stop();
        }
    }

    public void select(@NotNull Model model, int pos, int duration) {
        Intrinsics.checkParameterIsNotNull(model, "model");
        if (this.hasIconFluctuatorColor) {
            this.haloView.setVisibility(4);
            this.haloView.stop();
        }
        if (this.isEnabled) {
            AnimatorSet animatorSet = new AnimatorSet();
            float[] fArr = new float[2];
            PropertyValuesHolder clickDownXPropertyHolder = PropertyValuesHolder.ofFloat(View.SCALE_X, new float[]{1.0f, 0.8f});
            fArr = new float[2];
            PropertyValuesHolder clickDownYPropertyHolder = PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[]{1.0f, 0.8f});
            ObjectAnimator.ofPropertyValuesHolder(this.switchContainer, new PropertyValuesHolder[]{clickDownXPropertyHolder, clickDownYPropertyHolder}).setDuration((long) duration);
            fArr = new float[2];
            PropertyValuesHolder clickUpXPropertyHolder = PropertyValuesHolder.ofFloat(View.SCALE_X, new float[]{0.8f, 1.0f});
            fArr = new float[2];
            PropertyValuesHolder clickUpYPropertyHolder = PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[]{0.8f, 1.0f});
            ObjectAnimator.ofPropertyValuesHolder(this.switchContainer, new PropertyValuesHolder[]{clickUpXPropertyHolder, clickUpYPropertyHolder}).setDuration((long) duration);
            AnimatorSet clickAnimatorSet = new AnimatorSet();
            clickAnimatorSet.playSequentially(new Animator[]{clickDownAnimator, clickUpAnimator});
            AnimatorSet switchAnimatorSet = new AnimatorSet();
            LayerDrawable layerDrawable = this.switchBackground.getBackground();
            if (layerDrawable == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.graphics.drawable.LayerDrawable");
            }
            ClipDrawable clipDrawable = layerDrawable.getDrawable(1);
            if (clipDrawable == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.graphics.drawable.ClipDrawable");
            }
            clipDrawable = clipDrawable;
            MarginLayoutParams marginLayoutParamas = this.switchThumb.getLayoutParams();
            if (marginLayoutParamas == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            }
            marginLayoutParamas = marginLayoutParamas;
            ValueAnimator turnOnThumbAnimator = null;
            ValueAnimator clipLevelAnimator = null;
            float[] fArr2;
            if (this.isOn) {
                turnOnThumbAnimator = ValueAnimator.ofFloat(new float[]{(float) this.thumbMargin, 0.0f});
                turnOnThumbAnimator.setDuration(100);
                fArr2 = new float[2];
                clipLevelAnimator = ValueAnimator.ofFloat(new float[]{5000.0f, 1000.0f});
                clickAnimatorSet.setDuration(100);
            } else {
                turnOnThumbAnimator = ValueAnimator.ofFloat(new float[]{0.0f, (float) this.thumbMargin});
                turnOnThumbAnimator.setDuration(100);
                fArr2 = new float[2];
                clipLevelAnimator = ValueAnimator.ofFloat(new float[]{3000.0f, 7000.0f});
                clickAnimatorSet.setDuration(100);
            }
            if (clipLevelAnimator != null) {
                clipLevelAnimator.addUpdateListener(new SwitchViewHolder$select$1(this, clipDrawable));
            }
            if (turnOnThumbAnimator != null) {
                turnOnThumbAnimator.addUpdateListener(new SwitchViewHolder$select$2(this, marginLayoutParamas));
            }
            switchAnimatorSet.playTogether(new Animator[]{turnOnThumbAnimator, clipLevelAnimator});
            animatorSet.play(switchAnimatorSet).after(clickAnimatorSet);
            animatorSet.addListener(new SwitchViewHolder$select$3(this, model, pos));
            animatorSet.start();
            return;
        }
        this.vlist.unlock();
    }

    private final int getSubTitleColor() {
        if (this.extras == null) {
            return 0;
        }
        return getTextColor(Model.SUBTITLE_COLOR);
    }

    private final int getSubTitle2Color() {
        if (this.extras == null) {
            return 0;
        }
        return getTextColor(Model.SUBTITLE_2_COLOR);
    }

    private final int getTextColor(String property) {
        int i = 0;
        if (this.extras == null) {
            return i;
        }
        String str = (String) this.extras.get(property);
        if (str == null) {
            return i;
        }
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException e) {
            return i;
        }
    }

    public void copyAndPosition(@NotNull ImageView imageC, @NotNull TextView titleC, @NotNull TextView subTitleC, @NotNull TextView subTitle2C, boolean setImage) {
        Intrinsics.checkParameterIsNotNull(imageC, "imageC");
        Intrinsics.checkParameterIsNotNull(titleC, "titleC");
        Intrinsics.checkParameterIsNotNull(subTitleC, "subTitleC");
        Intrinsics.checkParameterIsNotNull(subTitle2C, "subTitle2C");
    }

    public final void drawSwitch(boolean isOn, boolean isEnabled) {
        if (isEnabled) {
            this.switchThumb.setBackgroundResource(R.drawable.switch_thumb_enabled);
        } else {
            this.switchThumb.setBackgroundResource(R.drawable.switch_thumb_disabled);
        }
        MarginLayoutParams layoutParams;
        if (isEnabled && isOn) {
            this.switchBackground.setBackgroundResource(R.drawable.switch_button_off);
            LayerDrawable layerDrawable = this.switchBackground.getBackground();
            if (layerDrawable == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.graphics.drawable.LayerDrawable");
            }
            ClipDrawable backgroundClipDrawable = layerDrawable.getDrawable(1);
            if (backgroundClipDrawable == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.graphics.drawable.ClipDrawable");
            }
            backgroundClipDrawable.setLevel(5000);
            layoutParams = this.switchThumb.getLayoutParams();
            if (layoutParams == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            }
            layoutParams = layoutParams;
            layoutParams.leftMargin = this.thumbMargin;
            this.switchThumb.setLayoutParams(layoutParams);
            return;
        }
        this.switchBackground.setBackgroundResource(R.drawable.switch_button_off);
        Drawable background = this.switchBackground.getBackground();
        if (background == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.graphics.drawable.LayerDrawable");
        }
        Drawable drawable = ((LayerDrawable) background).getDrawable(1);
        if (drawable == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.graphics.drawable.ClipDrawable");
        }
        ((ClipDrawable) drawable).setLevel(0);
        LayoutParams layoutParams2 = this.switchThumb.getLayoutParams();
        if (layoutParams2 == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
        }
        layoutParams = (MarginLayoutParams) layoutParams2;
        layoutParams.leftMargin = 0;
        this.switchThumb.setLayoutParams(layoutParams);
    }

    public void preBind(@NotNull Model model, @NotNull ModelState modelState) {
        Intrinsics.checkParameterIsNotNull(model, "model");
        Intrinsics.checkParameterIsNotNull(modelState, "modelState");
        this.isOn = model.isOn;
        this.isEnabled = model.isEnabled;
        drawSwitch(this.isOn, this.isEnabled);
        MarginLayoutParams layoutParams = this.title.getLayoutParams();
        if (layoutParams == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
        }
        layoutParams.topMargin = (int) model.fontInfo.titleFontTopMargin;
        this.title.setTextSize(model.fontInfo.titleFontSize);
        this.titleSelectedTopMargin = (float) ((int) model.fontInfo.titleFontTopMargin);
        LayoutParams layoutParams2 = this.subTitle.getLayoutParams();
        if (layoutParams2 == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
        }
        ((MarginLayoutParams) layoutParams2).topMargin = (int) model.fontInfo.subTitleFontTopMargin;
        if (model.subTitle_2Lines) {
            this.subTitle.setTextAppearance(HudApplication.getAppContext(), R.style.vlist_subtitle_2_line);
            this.subTitle.setSingleLine(false);
            this.subTitle.setMaxLines(2);
            this.subTitle.setEllipsize(TruncateAt.END);
        } else {
            this.subTitle.setTextAppearance(HudApplication.getAppContext(), R.style.vlist_subtitle);
            this.subTitle.setSingleLine(true);
            this.subTitle.setEllipsize((TruncateAt) null);
        }
        this.subTitle.setTextSize(model.fontInfo.subTitleFontSize);
        layoutParams = this.subTitle2.getLayoutParams();
        if (layoutParams == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
        }
        layoutParams.topMargin = (int) model.fontInfo.subTitle2FontTopMargin;
        this.subTitle2.setTextSize(model.fontInfo.subTitle2FontSize);
        this.titleUnselectedScale = model.fontInfo.titleScale;
    }

    public void bind(@NotNull Model model, @NotNull ModelState modelState) {
        Intrinsics.checkParameterIsNotNull(model, "model");
        Intrinsics.checkParameterIsNotNull(modelState, "modelState");
        this.isOn = model.isOn;
        this.isEnabled = model.isEnabled;
        drawSwitch(this.isOn, this.isEnabled);
        if (modelState.updateTitle) {
            String str = model.title;
            Intrinsics.checkExpressionValueIsNotNull(str, "model.title");
            setTitle(str);
        }
        if (modelState.updateSubTitle) {
            if (model.subTitle != null) {
                setSubTitle(model.subTitle, model.subTitleFormatted);
            } else {
                setSubTitle(null, false);
            }
        }
        if (modelState.updateSubTitle2) {
            if (model.subTitle2 != null) {
                setSubTitle2(model.subTitle2, model.subTitle2Formatted);
            } else {
                setSubTitle2(null, false);
            }
        }
        this.textAnimationDisabled = model.noTextAnimation;
        this.iconScaleAnimationDisabled = model.noImageScaleAnimation;
        setIconFluctuatorColor(model.iconFluctuatorColor);
    }

    @NotNull
    public ModelType getModelType() {
        return ModelType.SWITCH;
    }
}
