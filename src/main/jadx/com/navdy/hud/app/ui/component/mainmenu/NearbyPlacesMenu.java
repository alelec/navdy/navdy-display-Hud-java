package com.navdy.hud.app.ui.component.mainmenu;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.content.ContextCompat;
import android.view.View;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification;
import com.navdy.hud.app.maps.MapsNotification;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu;
import com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel;
import com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent;
import com.navdy.service.library.events.places.PlaceType;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import java.util.ArrayList;
import java.util.List;

class NearbyPlacesMenu implements IMenu {
    private static final Model atm;
    private static final Model back;
    private static final Model coffee;
    private static final Model food;
    private static final Model gas;
    private static final Model groceryStore;
    private static final Logger logger = new Logger(NearbyPlacesMenu.class);
    private static final Model parking;
    private static final Resources resources;
    private static final String search = resources.getString(R.string.carousel_menu_search_title);
    private static final int searchColor;
    private int backSelection;
    private int backSelectionId;
    private final Bus bus;
    private List<Model> cachedList;
    private final NotificationManager notificationManager = NotificationManager.getInstance();
    private final IMenu parent;
    private final Presenter presenter;
    private final VerticalMenuComponent vscrollComponent;

    static {
        Context context = HudApplication.getAppContext();
        resources = context.getResources();
        searchColor = ContextCompat.getColor(context, R.color.mm_search);
        String title = resources.getString(R.string.back);
        int fluctuatorColor = ContextCompat.getColor(context, R.color.mm_back);
        back = IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, title, null);
        title = resources.getString(R.string.carousel_search_gas);
        fluctuatorColor = ContextCompat.getColor(context, R.color.mm_search_gas);
        gas = IconBkColorViewHolder.buildModel(R.id.search_menu_gas, R.drawable.icon_place_gas, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, title, null);
        title = resources.getString(R.string.carousel_search_parking);
        fluctuatorColor = ContextCompat.getColor(context, R.color.mm_search_parking);
        parking = IconBkColorViewHolder.buildModel(R.id.search_menu_parking, R.drawable.icon_place_parking, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, title, null);
        title = resources.getString(R.string.carousel_search_food);
        fluctuatorColor = ContextCompat.getColor(context, R.color.mm_search_food);
        food = IconBkColorViewHolder.buildModel(R.id.search_menu_food, R.drawable.icon_place_restaurant, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, title, null);
        title = resources.getString(R.string.carousel_search_grocery_store);
        fluctuatorColor = ContextCompat.getColor(context, R.color.mm_search_grocery_store);
        groceryStore = IconBkColorViewHolder.buildModel(R.id.search_menu_grocery_store, R.drawable.icon_place_store, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, title, null);
        title = resources.getString(R.string.carousel_search_coffee);
        fluctuatorColor = ContextCompat.getColor(context, R.color.mm_search_coffee);
        coffee = IconBkColorViewHolder.buildModel(R.id.search_menu_coffee, R.drawable.icon_place_coffee, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, title, null);
        title = resources.getString(R.string.carousel_search_atm);
        fluctuatorColor = ContextCompat.getColor(context, R.color.mm_search_atm);
        atm = IconBkColorViewHolder.buildModel(R.id.search_menu_atm, R.drawable.icon_place_a_t_m, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, title, null);
    }

    NearbyPlacesMenu(Bus bus, VerticalMenuComponent vscrollComponent, Presenter presenter, IMenu parent) {
        this.bus = bus;
        this.vscrollComponent = vscrollComponent;
        this.presenter = presenter;
        this.parent = parent;
    }

    public List<Model> getItems() {
        if (this.cachedList != null) {
            return this.cachedList;
        }
        List<Model> list = new ArrayList();
        list.add(back);
        list.add(gas);
        list.add(parking);
        list.add(food);
        list.add(groceryStore);
        list.add(coffee);
        list.add(atm);
        this.cachedList = list;
        return list;
    }

    public int getInitialSelection() {
        return 1;
    }

    public VerticalFastScrollIndex getScrollIndex() {
        return null;
    }

    public void setBackSelectionPos(int n) {
        this.backSelection = n;
    }

    public void setBackSelectionId(int id) {
        this.backSelectionId = id;
    }

    public void setSelectedIcon() {
        this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_mm_search_2, searchColor, null, 1.0f);
        this.vscrollComponent.selectedText.setText(search);
    }

    public boolean selectItem(ItemSelectionState selection) {
        logger.v("select id:" + selection.id + " pos:" + selection.pos);
        switch (selection.id) {
            case R.id.menu_back:
                logger.v("back");
                AnalyticsSupport.recordNearbySearchReturnMainMenu();
                this.presenter.loadMenu(this.parent, MenuLevel.BACK_TO_PARENT, this.backSelection, this.backSelectionId);
                break;
            case R.id.search_menu_atm:
                logger.v("atm");
                launchSearch(PlaceType.PLACE_TYPE_ATM);
                break;
            case R.id.search_menu_coffee:
                logger.v("coffee");
                launchSearch(PlaceType.PLACE_TYPE_COFFEE);
                break;
            case R.id.search_menu_food:
                logger.v("food");
                launchSearch(PlaceType.PLACE_TYPE_RESTAURANT);
                break;
            case R.id.search_menu_gas:
                logger.v("gas");
                launchSearch(PlaceType.PLACE_TYPE_GAS);
                break;
            case R.id.search_menu_grocery_store:
                logger.v("grocery store");
                launchSearch(PlaceType.PLACE_TYPE_STORE);
                break;
            case R.id.search_menu_hospital:
                logger.v("hospital");
                launchSearch(PlaceType.PLACE_TYPE_HOSPITAL);
                break;
            case R.id.search_menu_parking:
                logger.v("parking");
                launchSearch(PlaceType.PLACE_TYPE_PARKING);
                break;
        }
        return false;
    }

    private void launchSearch(final PlaceType placeType) {
        if (HereMapsManager.getInstance().isInitialized()) {
            this.presenter.performSelectionAnimation(new Runnable() {
                public void run() {
                    NearbyPlacesMenu.this.presenter.close(new Runnable() {
                        public void run() {
                            NearbyPlacesMenu.logger.v("addNotification for quick search");
                            NearbyPlacesMenu.this.notificationManager.addNotification(new NearbyPlaceSearchNotification(placeType));
                        }
                    });
                }
            });
            return;
        }
        logger.w("Here maps engine not initialized, exit");
        this.presenter.performSelectionAnimation(new Runnable() {
            public void run() {
                NearbyPlacesMenu.this.presenter.close();
                MapsNotification.showMapsEngineNotInitializedToast();
            }
        });
    }

    public Menu getType() {
        return Menu.SEARCH;
    }

    public boolean isItemClickable(int id, int pos) {
        return true;
    }

    public Model getModelfromPos(int pos) {
        if (this.cachedList == null || this.cachedList.size() <= pos) {
            return null;
        }
        return (Model) this.cachedList.get(pos);
    }

    public boolean isBindCallsEnabled() {
        return false;
    }

    public void onBindToView(Model model, View view, int pos, ModelState state) {
    }

    public IMenu getChildMenu(IMenu parent, String args, String path) {
        return null;
    }

    public void onUnload(MenuLevel level) {
    }

    public void onItemSelected(ItemSelectionState selection) {
    }

    public void onScrollIdle() {
    }

    public void onFastScrollStart() {
    }

    public void onFastScrollEnd() {
    }

    public void showToolTip() {
    }

    public boolean isFirstItemEmpty() {
        return true;
    }
}
