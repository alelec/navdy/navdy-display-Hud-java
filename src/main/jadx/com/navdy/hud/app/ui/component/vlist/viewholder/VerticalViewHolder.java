package com.navdy.hud.app.ui.component.vlist.viewholder;

import android.animation.AnimatorSet;
import android.content.res.Resources;
import android.os.Handler;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.widget.TextView;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType;
import com.navdy.service.library.log.Logger;
import java.util.HashMap;

public abstract class VerticalViewHolder extends ViewHolder {
    public static final String EMPTY = "";
    public static final int NO_COLOR = 0;
    public static final int iconMargin;
    public static final int listItemHeight;
    public static final int mainIconSize;
    public static final int rootTopOffset;
    static final Logger sLogger = new Logger(VerticalViewHolder.class);
    public static final int scrollDistance = listItemHeight;
    public static final int selectedIconSize;
    public static final int selectedImageX;
    public static final int selectedImageY;
    public static final int selectedTextX;
    public static final int subTitle2Color;
    public static final int subTitleColor;
    public static final float subTitleHeight;
    public static final int textLeftMargin;
    public static final float titleHeight;
    public static final int unselectedIconSize;
    protected State currentState;
    protected HashMap<String, String> extras;
    protected Handler handler;
    protected Interpolator interpolator = VerticalList.getInterpolator();
    protected AnimatorSet itemAnimatorSet;
    public ViewGroup layout;
    protected int pos = -1;
    protected VerticalList vlist;

    public enum AnimationType {
        NONE,
        MOVE,
        INIT
    }

    public enum State {
        SELECTED,
        UNSELECTED
    }

    public abstract void bind(Model model, ModelState modelState);

    public abstract void clearAnimation();

    public abstract void copyAndPosition(ImageView imageView, TextView textView, TextView textView2, TextView textView3, boolean z);

    public abstract ModelType getModelType();

    public abstract void select(Model model, int i, int i2);

    public abstract void setItemState(State state, AnimationType animationType, int i, boolean z);

    static {
        Resources resources = HudApplication.getAppContext().getResources();
        selectedIconSize = resources.getDimensionPixelSize(R.dimen.vlist_image);
        unselectedIconSize = resources.getDimensionPixelSize(R.dimen.vlist_small_image);
        selectedImageX = resources.getDimensionPixelSize(R.dimen.vmenu_sel_image_x);
        selectedImageY = resources.getDimensionPixelSize(R.dimen.vmenu_sel_image_y);
        mainIconSize = resources.getDimensionPixelSize(R.dimen.vlist_image_frame);
        textLeftMargin = resources.getDimensionPixelSize(R.dimen.vlist_text_left_margin);
        listItemHeight = resources.getDimensionPixelSize(R.dimen.vlist_item_height);
        titleHeight = resources.getDimension(R.dimen.vlist_title);
        subTitleHeight = resources.getDimension(R.dimen.vlist_subtitle);
        selectedTextX = resources.getDimensionPixelSize(R.dimen.vmenu_sel_text_x);
        rootTopOffset = resources.getDimensionPixelSize(R.dimen.vmenu_root_top_offset);
        iconMargin = resources.getDimensionPixelOffset(R.dimen.vlist_icon_list_margin);
        subTitleColor = resources.getColor(R.color.vlist_subtitle);
        subTitle2Color = resources.getColor(R.color.vlist_subtitle);
    }

    public VerticalViewHolder(ViewGroup layout, VerticalList vlist, Handler handler) {
        super(layout);
        this.layout = layout;
        this.vlist = vlist;
        this.handler = handler;
    }

    public void setPos(int pos) {
        this.pos = pos;
        this.currentState = null;
    }

    public int getPos() {
        return this.pos;
    }

    public void setExtras(HashMap<String, String> extras) {
        this.extras = extras;
    }

    public final void setState(State state, AnimationType animation, int duration) {
        setState(state, animation, duration, true);
    }

    public final void setState(State state, AnimationType animation, int duration, boolean startFluctuator) {
        switch (getModelType()) {
            case BLANK:
                return;
            default:
                if (sLogger.isLoggable(2)) {
                    sLogger.v("setState {" + this.pos + ", " + state + "} animate=" + animation + " dur=" + duration + " current-raw {" + this.vlist.getRawPosition() + "} vh-id{" + System.identityHashCode(this) + "}");
                }
                if (this.currentState != state) {
                    clearAnimation();
                    this.currentState = state;
                    if (state == State.SELECTED) {
                        this.vlist.selectedList.add(Integer.valueOf(this.pos));
                    } else {
                        this.vlist.selectedList.remove(Integer.valueOf(this.pos));
                    }
                    setItemState(state, animation, duration, startFluctuator);
                    if (this.itemAnimatorSet != null) {
                        this.itemAnimatorSet.setDuration((long) duration);
                        this.itemAnimatorSet.setInterpolator(this.interpolator);
                        this.itemAnimatorSet.start();
                        return;
                    }
                    return;
                } else if (state == State.SELECTED) {
                    this.vlist.selectedList.add(Integer.valueOf(this.pos));
                    return;
                } else {
                    this.vlist.selectedList.remove(Integer.valueOf(this.pos));
                    return;
                }
        }
    }

    public void stopAnimation() {
        if (this.itemAnimatorSet != null && this.itemAnimatorSet.isRunning()) {
            this.itemAnimatorSet.removeAllListeners();
            this.itemAnimatorSet.cancel();
        }
        this.itemAnimatorSet = null;
    }

    public State getState() {
        return this.currentState;
    }

    public void startFluctuator() {
    }

    public void preBind(Model model, ModelState modelState) {
    }

    public boolean handleKey(CustomKeyEvent event) {
        return false;
    }

    public boolean hasToolTip() {
        return false;
    }
}
