package com.navdy.hud.app.ui.component.homescreen;

import android.content.Context;
import com.navdy.hud.app.R;
import com.navdy.hud.app.maps.widget.TrafficIncidentWidgetPresenter;
import com.navdy.hud.app.view.CalendarWidgetPresenter;
import com.navdy.hud.app.view.ClockWidgetPresenter;
import com.navdy.hud.app.view.ClockWidgetPresenter.ClockType;
import com.navdy.hud.app.view.CompassPresenter;
import com.navdy.hud.app.view.DashboardWidgetPresenter;
import com.navdy.hud.app.view.DriveScoreGaugePresenter;
import com.navdy.hud.app.view.ETAGaugePresenter;
import com.navdy.hud.app.view.EmptyGaugePresenter;
import com.navdy.hud.app.view.EngineTemperaturePresenter;
import com.navdy.hud.app.view.FuelGaugePresenter2;
import com.navdy.hud.app.view.GForcePresenter;
import com.navdy.hud.app.view.MPGGaugePresenter;
import com.navdy.hud.app.view.MusicWidgetPresenter;
import com.navdy.hud.app.view.SpeedLimitSignPresenter;
import java.util.HashMap;

public class DashWidgetPresenterFactory {
    private static final HashMap<String, Integer> RESOURCE_MAP = new HashMap();

    static {
        RESOURCE_MAP.put(SmartDashWidgetManager.EMPTY_WIDGET_ID, Integer.valueOf(0));
        RESOURCE_MAP.put(SmartDashWidgetManager.MPG_GRAPH_WIDGET_ID, Integer.valueOf(R.drawable.asset_dash20_proto_gauge_mpg));
        RESOURCE_MAP.put(SmartDashWidgetManager.MPG_AVG_WIDGET_ID, Integer.valueOf(R.drawable.asset_dash20_proto_gauge_clock_analog));
        RESOURCE_MAP.put(SmartDashWidgetManager.WEATHER_WIDGET_ID, Integer.valueOf(R.drawable.asset_dash20_proto_gauge_weather));
    }

    public static DashboardWidgetPresenter createDashWidgetPresenter(Context context, String widgetIdentifier) {
        boolean z = true;
        switch (widgetIdentifier.hashCode()) {
            case -2114596188:
                if (widgetIdentifier.equals(SmartDashWidgetManager.ANALOG_CLOCK_WIDGET_ID)) {
                    z = true;
                    break;
                }
                break;
            case -1874661839:
                if (widgetIdentifier.equals(SmartDashWidgetManager.COMPASS_WIDGET_ID)) {
                    z = true;
                    break;
                }
                break;
            case -1515555394:
                if (widgetIdentifier.equals(SmartDashWidgetManager.MUSIC_WIDGET_ID)) {
                    z = true;
                    break;
                }
                break;
            case -1158963060:
                if (widgetIdentifier.equals(SmartDashWidgetManager.MPG_AVG_WIDGET_ID)) {
                    z = true;
                    break;
                }
                break;
            case -856367506:
                if (widgetIdentifier.equals(SmartDashWidgetManager.ETA_GAUGE_ID)) {
                    z = true;
                    break;
                }
                break;
            case -538792631:
                if (widgetIdentifier.equals(SmartDashWidgetManager.DIGITAL_CLOCK_2_WIDGET_ID)) {
                    z = true;
                    break;
                }
                break;
            case -131933527:
                if (widgetIdentifier.equals(SmartDashWidgetManager.ENGINE_TEMPERATURE_GAUGE_ID)) {
                    z = true;
                    break;
                }
                break;
            case 460083427:
                if (widgetIdentifier.equals(SmartDashWidgetManager.DRIVE_SCORE_GAUGE_ID)) {
                    z = true;
                    break;
                }
                break;
            case 641719909:
                if (widgetIdentifier.equals(SmartDashWidgetManager.CALENDAR_WIDGET_ID)) {
                    z = true;
                    break;
                }
                break;
            case 662413980:
                if (widgetIdentifier.equals(SmartDashWidgetManager.DIGITAL_CLOCK_WIDGET_ID)) {
                    z = true;
                    break;
                }
                break;
            case 811991446:
                if (widgetIdentifier.equals(SmartDashWidgetManager.EMPTY_WIDGET_ID)) {
                    z = false;
                    break;
                }
                break;
            case 1119776967:
                if (widgetIdentifier.equals(SmartDashWidgetManager.SPEED_LIMIT_SIGN_GAUGE_ID)) {
                    z = true;
                    break;
                }
                break;
            case 1168400042:
                if (widgetIdentifier.equals(SmartDashWidgetManager.FUEL_GAUGE_ID)) {
                    z = true;
                    break;
                }
                break;
            case 1872543020:
                if (widgetIdentifier.equals(SmartDashWidgetManager.TRAFFIC_INCIDENT_GAUGE_ID)) {
                    z = true;
                    break;
                }
                break;
            case 2057821183:
                if (widgetIdentifier.equals(SmartDashWidgetManager.GFORCE_WIDGET_ID)) {
                    z = true;
                    break;
                }
                break;
        }
        switch (z) {
            case false:
                return new EmptyGaugePresenter(context);
            case true:
                return new FuelGaugePresenter2(context);
            case true:
                return new CompassPresenter(context);
            case true:
                return new ClockWidgetPresenter(context, ClockType.ANALOG);
            case true:
                return new ClockWidgetPresenter(context, ClockType.DIGITAL1);
            case true:
                return new ClockWidgetPresenter(context, ClockType.DIGITAL2);
            case true:
                return new TrafficIncidentWidgetPresenter();
            case true:
                return new CalendarWidgetPresenter(context);
            case true:
                return new ETAGaugePresenter(context);
            case true:
                return new MPGGaugePresenter(context);
            case true:
                return new MusicWidgetPresenter(context);
            case true:
                return new SpeedLimitSignPresenter(context);
            case true:
                return new GForcePresenter(context);
            case true:
                return new EngineTemperaturePresenter(context);
            case true:
                return new DriveScoreGaugePresenter(context, R.layout.drive_score_gauge_layout, true);
            default:
                return new ImageWidgetPresenter(context, ((Integer) RESOURCE_MAP.get(widgetIdentifier)).intValue(), widgetIdentifier);
        }
    }
}
