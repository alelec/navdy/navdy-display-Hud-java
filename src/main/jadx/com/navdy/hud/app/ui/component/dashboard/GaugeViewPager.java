package com.navdy.hud.app.ui.component.dashboard;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import com.navdy.hud.app.R;
import com.navdy.hud.app.view.DashboardWidgetPresenter;
import com.navdy.service.library.log.Logger;
import java.util.LinkedList;
import java.util.Queue;

public class GaugeViewPager extends FrameLayout {
    public static final int FAST_SCROLL_ANIMATION_DURATION = 100;
    public static final int REGULAR_SCROLL_ANIMATION_DURATION = 200;
    private static final Logger sLogger = new Logger(GaugeViewPager.class);
    private Operation currentOperation;
    private volatile boolean isAnimationRunning;
    private Adapter mAdapter;
    private ChangeListener mChangeListener;
    private int mCurrentPosition;
    private int mNextPosition;
    private int mPreviousPosition;
    private LinearLayout mSlider;
    private Queue<Operation> operationQueue;
    final ValueAnimator positionAnimator;

    public interface Adapter {
        int getCount();

        int getExcludedPosition();

        DashboardWidgetPresenter getPresenter(int i);

        View getView(int i, View view, ViewGroup viewGroup, boolean z);
    }

    public interface ChangeListener {
        void onGaugeChanged(int i);
    }

    enum Operation {
        NEXT,
        PREVIOUS
    }

    public GaugeViewPager(Context context) {
        this(context, null);
    }

    public GaugeViewPager(Context context, AttributeSet attrs) {
        this(context, attrs, -1);
    }

    public GaugeViewPager(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mCurrentPosition = -1;
        this.mNextPosition = -1;
        this.mPreviousPosition = -1;
        this.operationQueue = new LinkedList();
        this.isAnimationRunning = false;
        this.positionAnimator = new ValueAnimator();
        this.positionAnimator.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                MarginLayoutParams marginParams = (MarginLayoutParams) GaugeViewPager.this.mSlider.getLayoutParams();
                marginParams.topMargin = ((Integer) valueAnimator.getAnimatedValue()).intValue();
                GaugeViewPager.this.mSlider.setLayoutParams(marginParams);
            }
        });
        this.positionAnimator.addListener(new AnimatorListener() {
            public void onAnimationStart(Animator animator) {
                GaugeViewPager.this.isAnimationRunning = true;
            }

            public void onAnimationEnd(Animator animator) {
                GaugeViewPager.this.getHandler().post(new Runnable() {
                    public void run() {
                        GaugeViewPager.this.setSelectedPosition(GaugeViewPager.this.currentOperation == Operation.NEXT ? GaugeViewPager.this.mNextPosition : GaugeViewPager.this.mPreviousPosition);
                        Operation nextOperationToBePerformed = GaugeViewPager.this.operationQueue.size() > 0 ? (Operation) GaugeViewPager.this.operationQueue.remove() : null;
                        if (nextOperationToBePerformed != null) {
                            boolean z;
                            GaugeViewPager gaugeViewPager = GaugeViewPager.this;
                            if (GaugeViewPager.this.operationQueue.size() > 0) {
                                z = true;
                            } else {
                                z = false;
                            }
                            gaugeViewPager.performOperation(nextOperationToBePerformed, z);
                            return;
                        }
                        GaugeViewPager.this.isAnimationRunning = false;
                    }
                });
            }

            public void onAnimationCancel(Animator animator) {
            }

            public void onAnimationRepeat(Animator animator) {
            }
        });
    }

    public GaugeViewPager(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.mCurrentPosition = -1;
        this.mNextPosition = -1;
        this.mPreviousPosition = -1;
        this.operationQueue = new LinkedList();
        this.isAnimationRunning = false;
        this.positionAnimator = new ValueAnimator();
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        removeAllViews();
        this.mSlider = new LinearLayout(getContext());
        this.mSlider.setOrientation(1);
        this.mSlider.setLayoutParams(new MarginLayoutParams(-1, -2));
        addView(this.mSlider);
        LayoutInflater.from(getContext()).inflate(R.layout.gauge_view_scrims, this, true);
        int i = 0;
        while (i < 3) {
            FrameLayout frameLayout = new FrameLayout(getContext());
            if (i == 0 || i == 1) {
                frameLayout.setLayoutParams(new MarginLayoutParams(getResources().getDimensionPixelSize(R.dimen.gauge_frame_width), getResources().getDimensionPixelSize(R.dimen.gauge_frame_height) + getResources().getDimensionPixelSize(R.dimen.gauge_transition_margin)));
            } else {
                frameLayout.setLayoutParams(new MarginLayoutParams(getResources().getDimensionPixelSize(R.dimen.gauge_frame_width), getResources().getDimensionPixelSize(R.dimen.gauge_frame_height)));
            }
            this.mSlider.addView(frameLayout);
            i++;
        }
        ((MarginLayoutParams) this.mSlider.getLayoutParams()).topMargin = -getMaxTopMarginValue();
    }

    private int getMaxTopMarginValue() {
        return getResources().getDimensionPixelSize(R.dimen.gauge_transition_margin) + getResources().getDimensionPixelSize(R.dimen.gauge_frame_height);
    }

    public void setSelectedPosition(int position) {
        if (this.mAdapter == null) {
            throw new IllegalStateException("Set the Adapter before setting the position");
        }
        int oldPosition = this.mCurrentPosition;
        if (position >= 0 && position < this.mAdapter.getCount()) {
            int nextPosition = ((this.mAdapter.getCount() + position) - 1) % this.mAdapter.getCount();
            int previousPosition = (position + 1) % this.mAdapter.getCount();
            if (nextPosition == this.mAdapter.getExcludedPosition()) {
                nextPosition = ((this.mAdapter.getCount() + nextPosition) - 1) % this.mAdapter.getCount();
            }
            if (previousPosition == this.mAdapter.getExcludedPosition()) {
                previousPosition = (previousPosition + 1) % this.mAdapter.getCount();
            }
            this.mNextPosition = nextPosition;
            this.mPreviousPosition = previousPosition;
            this.mCurrentPosition = position;
            populateView(0, this.mNextPosition);
            populateView(1, this.mCurrentPosition);
            populateView(2, this.mPreviousPosition);
            ((MarginLayoutParams) this.mSlider.getLayoutParams()).topMargin = -getMaxTopMarginValue();
            if (this.mChangeListener != null && this.mCurrentPosition != oldPosition) {
                this.mChangeListener.onGaugeChanged(this.mCurrentPosition);
            }
        }
    }

    public void updateState() {
        setSelectedPosition(getSelectedPosition());
    }

    private void populateView(int localPosition, int adapterPosition) {
        boolean z = true;
        ViewGroup localView = (ViewGroup) this.mSlider.getChildAt(localPosition);
        View child = localView.getChildAt(0);
        Adapter adapter = this.mAdapter;
        if (localPosition != 1) {
            z = false;
        }
        View newView = adapter.getView(adapterPosition, child, localView, z);
        if (newView != child) {
            localView.removeAllViews();
            localView.addView(newView);
        }
    }

    public int getSelectedPosition() {
        return this.mCurrentPosition;
    }

    public void setAdapter(Adapter adapter) {
        this.mAdapter = adapter;
        setSelectedPosition(0);
    }

    public void moveNext() {
        if (this.isAnimationRunning) {
            if (this.operationQueue.peek() != Operation.NEXT) {
                this.operationQueue.clear();
            }
            this.operationQueue.add(Operation.NEXT);
            return;
        }
        performOperation(Operation.NEXT, false);
    }

    public void movePrevious() {
        if (this.isAnimationRunning) {
            if (this.operationQueue.peek() != Operation.PREVIOUS) {
                this.operationQueue.clear();
            }
            this.operationQueue.add(Operation.PREVIOUS);
            return;
        }
        performOperation(Operation.PREVIOUS, false);
    }

    private void performOperation(Operation operation, boolean fastScroll) {
        this.currentOperation = operation;
        if (operation == Operation.NEXT) {
            updateWidgetView(this.mNextPosition);
            this.positionAnimator.setIntValues(new int[]{-getMaxTopMarginValue(), 0});
        } else if (operation == Operation.PREVIOUS) {
            updateWidgetView(this.mPreviousPosition);
            this.positionAnimator.setIntValues(new int[]{-getMaxTopMarginValue(), (-getMaxTopMarginValue()) * 2});
        }
        this.positionAnimator.setDuration(fastScroll ? 100 : 200);
        this.positionAnimator.start();
    }

    public void setChangeListener(ChangeListener changeListener) {
        this.mChangeListener = changeListener;
    }

    private void updateWidgetView(int position) {
        DashboardWidgetPresenter presenter = this.mAdapter.getPresenter(position);
        if (presenter != null) {
            sLogger.v("widget:: update before animation");
            presenter.setWidgetVisibleToUser(true);
        }
    }
}
