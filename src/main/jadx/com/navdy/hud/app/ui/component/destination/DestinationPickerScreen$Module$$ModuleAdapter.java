package com.navdy.hud.app.ui.component.destination;

import com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.Module;
import dagger.internal.ModuleAdapter;

public final class DestinationPickerScreen$Module$$ModuleAdapter extends ModuleAdapter<Module> {
    private static final Class<?>[] INCLUDES = new Class[0];
    private static final String[] INJECTS = new String[]{"members/com.navdy.hud.app.ui.component.destination.DestinationPickerView"};
    private static final Class<?>[] STATIC_INJECTIONS = new Class[0];

    public DestinationPickerScreen$Module$$ModuleAdapter() {
        super(Module.class, INJECTS, STATIC_INJECTIONS, false, INCLUDES, true, false);
    }
}
