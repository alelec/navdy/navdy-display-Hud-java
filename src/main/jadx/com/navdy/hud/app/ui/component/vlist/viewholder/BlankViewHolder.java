package com.navdy.hud.app.ui.component.vlist.viewholder;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State;
import com.navdy.service.library.log.Logger;

public class BlankViewHolder extends VerticalViewHolder {
    private static final Logger sLogger = VerticalViewHolder.sLogger;

    public static Model buildModel() {
        Model model = new Model();
        model.type = ModelType.BLANK;
        return model;
    }

    public static BlankViewHolder buildViewHolder(ViewGroup parent, VerticalList vlist, Handler handler) {
        return new BlankViewHolder((ViewGroup) LayoutInflater.from(parent.getContext()).inflate(R.layout.vlist_blank_item, parent, false), vlist, handler);
    }

    private BlankViewHolder(ViewGroup layout, VerticalList vlist, Handler handler) {
        super(layout, vlist, handler);
    }

    public ModelType getModelType() {
        return ModelType.BLANK;
    }

    public void setItemState(State state, AnimationType animation, int duration, boolean startFluctuator) {
    }

    public void bind(Model model, ModelState modelState) {
    }

    public void clearAnimation() {
    }

    public void select(Model model, int pos, int duration) {
    }

    public void copyAndPosition(ImageView imageC, TextView titleC, TextView subTitleC, TextView subTitle2C, boolean setImage) {
    }
}
