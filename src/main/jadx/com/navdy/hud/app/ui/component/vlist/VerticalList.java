package com.navdy.hud.app.ui.component.vlist;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.AnimatorSet.Builder;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.support.v7.widget.RecyclerView.RecycledViewPool;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.text.Layout.Alignment;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.widget.TextView;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.audio.SoundUtils;
import com.navdy.hud.app.audio.SoundUtils.Sound;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.ui.component.carousel.CarouselIndicator;
import com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation;
import com.navdy.hud.app.ui.component.image.IconColorImageView.IconShape;
import com.navdy.hud.app.ui.component.vlist.viewholder.BlankViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.ScrollableViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import com.navdy.hud.app.ui.interpolator.FastOutSlowInInterpolator;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.hud.app.util.ViewUtil;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

public class VerticalList {
    private static final Model BLANK_ITEM_BOTTOM = BlankViewHolder.buildModel();
    private static final Model BLANK_ITEM_TOP = BlankViewHolder.buildModel();
    private static final FontSize[] FONT_SIZES = new FontSize[]{FontSize.FONT_SIZE_26, FontSize.FONT_SIZE_26, FontSize.FONT_SIZE_22, FontSize.FONT_SIZE_22_2, FontSize.FONT_SIZE_18, FontSize.FONT_SIZE_18_2, FontSize.FONT_SIZE_16, FontSize.FONT_SIZE_16_2};
    public static final float ICON_BK_COLOR_SCALE_FACTOR = 0.83f;
    public static final float ICON_SCALE_FACTOR = 0.6f;
    public static final int ITEM_BACK_FADE_IN_DURATION = 100;
    public static final int ITEM_INIT_ANIMATION_DURATION = 100;
    public static final int ITEM_MOVE_ANIMATION_DURATION = 230;
    public static final int ITEM_SCROLL_ANIMATION = 150;
    public static final int ITEM_SELECT_ANIMATION_DURATION = 50;
    public static final int LOADING_ADAPTER_POS = 1;
    private static final int MAX_OFF_SCREEN_VIEWS = 5;
    private static final float MAX_SCROLL_BY = 2.0f;
    private static final float SCROLL_BY_INCREMENT = 0.5f;
    private static final int[] SINGLE_LINE_MAX_LINES = new int[]{1, 1, 1, 1};
    private static final float START_SCROLL_BY = 1.0f;
    public static final float TEXT_SCALE_FACTOR_16 = 1.0f;
    public static final float TEXT_SCALE_FACTOR_18 = 1.0f;
    public static final float TEXT_SCALE_FACTOR_22 = 0.81f;
    public static final float TEXT_SCALE_FACTOR_26 = 0.69f;
    private static final float[] TITLE_SIZES = new float[]{vlistTitle_26, vlistTitle_22, vlistTitle_18, vlistTitle_16};
    private static final int[] TWO_LINE_MAX_LINES = new int[]{1, 2, 2, 2};
    public static final TextView fontSizeTextView;
    private static Handler handler = new Handler(Looper.getMainLooper());
    private static final Logger sLogger = new Logger(VerticalList.class);
    private static final HashMap<FontSize, FontInfo> tit_subt_map = new HashMap();
    private static final HashMap<FontSize, FontInfo> tit_subt_map_2_lines = new HashMap();
    private static final HashMap<FontSize, FontInfo> tit_subt_subt2_map = new HashMap();
    public static final int vlistTitleTextW;
    public static final float vlistTitle_16;
    public static final float vlistTitle_16_16_subtitle_top;
    public static final float vlistTitle_16_16_title_top;
    public static final float vlistTitle_16_top_m_2;
    public static final float vlistTitle_16_top_m_3;
    public static final float vlistTitle_18;
    public static final float vlistTitle_18_16_subtitle_top;
    public static final float vlistTitle_18_16_title_top;
    public static final float vlistTitle_18_top_m_2;
    public static final float vlistTitle_18_top_m_3;
    public static final float vlistTitle_22;
    public static final float vlistTitle_22_16_subtitle_top;
    public static final float vlistTitle_22_16_title_top;
    public static final float vlistTitle_22_18_subtitle_top;
    public static final float vlistTitle_22_18_title_top;
    public static final float vlistTitle_22_top_m_2;
    public static final float vlistTitle_22_top_m_3;
    public static final float vlistTitle_26;
    public static final float vlistTitle_26_16_subtitle_top;
    public static final float vlistTitle_26_16_title_top;
    public static final float vlistTitle_26_18_subtitle_top;
    public static final float vlistTitle_26_18_title_top;
    public static final float vlistTitle_26_top_m_2;
    public static final float vlistTitle_26_top_m_3;
    public static final float vlistsubTitle2_16_top_m_3;
    public static final float vlistsubTitle2_18_top_m_3;
    public static final float vlistsubTitle2_22_top_m_3;
    public static final float vlistsubTitle2_26_top_m_3;
    public static final float vlistsubTitle_16_top_m_2;
    public static final float vlistsubTitle_16_top_m_3;
    public static final float vlistsubTitle_18_top_m_2;
    public static final float vlistsubTitle_18_top_m_3;
    public static final float vlistsubTitle_22_top_m_2;
    public static final float vlistsubTitle_22_top_m_3;
    public static final float vlistsubTitle_26_top_m_2;
    public static final float vlistsubTitle_26_top_m_3;
    private int actualScrollY;
    public VerticalAdapter adapter;
    final int animationDuration;
    volatile boolean bindCallbacks;
    Callback callback;
    ContainerCallback containerCallback;
    private ArrayList<ViewHolder> copyList;
    private int currentMiddlePosition;
    private volatile float currentScrollBy;
    boolean firstEntryBlank;
    boolean hasScrollableElement;
    private CarouselIndicator indicator;
    private DefaultAnimationListener indicatorAnimationListener;
    private AnimatorSet indicatorAnimatorSet;
    private volatile boolean initialPosChanged;
    private Runnable initialPosCheckRunnable;
    private volatile boolean isScrollBy;
    private volatile boolean isScrolling;
    private volatile boolean isSelectedOperationPending;
    private final ItemSelectionState itemSelectionState;
    private final KeyHandlerState keyHandlerState;
    private int lastScrollState;
    private VerticalLayoutManager layoutManager;
    private volatile boolean lockList;
    private List<Model> modelList;
    boolean reCalcScrollPos;
    private VerticalRecyclerView recyclerView;
    private volatile int scrollByUpReceived;
    int scrollItemEndY;
    int scrollItemHeight;
    int scrollItemIndex;
    int scrollItemStartY;
    private OnScrollListener scrollListener;
    private volatile int scrollPendingPos;
    public HashSet<Integer> selectedList;
    boolean sendScrollIdleEvent;
    private boolean switchingScrollBoundary;
    private boolean targetFound;
    public int targetPos;
    private boolean twoLineTitles;
    public boolean waitForTarget;

    public interface Callback {
        void onBindToView(Model model, View view, int i, ModelState modelState);

        void onItemSelected(ItemSelectionState itemSelectionState);

        void onLoad();

        void onScrollIdle();

        void select(ItemSelectionState itemSelectionState);
    }

    public interface ContainerCallback {
        void hideToolTips();

        boolean isCloseMenuVisible();

        boolean isFastScrolling();

        void showToolTips();
    }

    public enum Direction {
        UP,
        DOWN
    }

    public static class FontInfo {
        public float subTitle2FontSize;
        public float subTitle2FontTopMargin;
        public float subTitleFontSize;
        public float subTitleFontTopMargin;
        public float titleFontSize;
        public float titleFontTopMargin;
        public float titleScale;
        public boolean titleSingleLine = true;
    }

    public enum FontSize {
        FONT_SIZE_26,
        FONT_SIZE_22,
        FONT_SIZE_22_2,
        FONT_SIZE_18,
        FONT_SIZE_18_2,
        FONT_SIZE_16,
        FONT_SIZE_16_2,
        FONT_SIZE_26_18,
        FONT_SIZE_22_18,
        FONT_SIZE_22_2_18,
        FONT_SIZE_18_16,
        FONT_SIZE_18_2_16,
        FONT_SIZE_16_16,
        FONT_SIZE_16_2_16,
        FONT_SIZE_26_16,
        FONT_SIZE_22_16,
        FONT_SIZE_22_2_16
    }

    public static class ItemSelectionState {
        public int id = -1;
        public Model model;
        public int pos = -1;
        public int subId = -1;
        public int subPosition = -1;

        public void set(Model model, int id, int pos, int subId, int subPosition) {
            this.model = model;
            this.id = id;
            this.pos = pos;
            this.subId = subId;
            this.subPosition = subPosition;
        }
    }

    public static class KeyHandlerState {
        public boolean keyHandled;
        public boolean listMoved;

        void clear() {
            this.keyHandled = false;
            this.listMoved = false;
        }
    }

    public static class Model {
        public static final String INITIALS = "INITIAL";
        public static final String SUBTITLE_2_COLOR = "SUBTITLE_2_COLOR";
        public static final String SUBTITLE_COLOR = "SUBTITLE_COLOR";
        public int currentIconSelection;
        boolean dontStartFluctuator;
        public HashMap<String, String> extras;
        public FontInfo fontInfo;
        public FontSize fontSize;
        boolean fontSizeCheckDone;
        public int icon = 0;
        public int iconDeselectedColor = -1;
        public int[] iconDeselectedColors;
        public int iconFluctuatorColor = -1;
        public int[] iconFluctuatorColors;
        public int[] iconIds;
        public int[] iconList;
        public int iconSelectedColor = -1;
        public int[] iconSelectedColors;
        public IconShape iconShape;
        public int iconSize = -1;
        public int iconSmall = -1;
        public int id;
        public boolean isEnabled;
        public boolean isOn;
        boolean needsRebind;
        public boolean noImageScaleAnimation;
        public boolean noTextAnimation;
        public int scrollItemLayoutId = -1;
        public Object state;
        public String subTitle;
        public String subTitle2;
        public boolean subTitle2Formatted;
        public boolean subTitleFormatted;
        public boolean subTitle_2Lines;
        public boolean supportsToolTip;
        public String title;
        public ModelType type;

        public Model(Model m) {
            this.type = m.type;
            this.id = m.id;
            this.icon = m.icon;
            this.iconSelectedColor = m.iconSelectedColor;
            this.iconDeselectedColor = m.iconDeselectedColor;
            this.iconSmall = m.iconSmall;
            this.iconFluctuatorColor = m.iconFluctuatorColor;
            this.title = m.title;
            this.subTitle = m.subTitle;
            this.subTitle2 = m.subTitle2;
            this.subTitle_2Lines = m.subTitle_2Lines;
            this.subTitleFormatted = m.subTitleFormatted;
            this.subTitle2Formatted = m.subTitle2Formatted;
            this.scrollItemLayoutId = m.scrollItemLayoutId;
            this.iconList = m.iconList;
            this.iconIds = m.iconIds;
            this.iconSelectedColors = m.iconSelectedColors;
            this.iconDeselectedColors = m.iconDeselectedColors;
            this.iconFluctuatorColors = m.iconFluctuatorColors;
            this.currentIconSelection = m.currentIconSelection;
            this.supportsToolTip = m.supportsToolTip;
            this.extras = m.extras;
            this.state = m.state;
            this.iconShape = m.iconShape;
            this.needsRebind = m.needsRebind;
            this.dontStartFluctuator = m.dontStartFluctuator;
            this.fontSizeCheckDone = m.fontSizeCheckDone;
            this.fontInfo = m.fontInfo;
            this.fontSize = m.fontSize;
            this.noTextAnimation = m.noTextAnimation;
            this.noImageScaleAnimation = m.noImageScaleAnimation;
            this.iconSize = m.iconSize;
        }

        public void clear() {
            this.id = 0;
            this.icon = 0;
            this.iconSelectedColor = -1;
            this.iconDeselectedColor = -1;
            this.iconSmall = -1;
            this.iconFluctuatorColor = -1;
            this.title = null;
            this.subTitle = null;
            this.subTitle2 = null;
            this.subTitle_2Lines = false;
            this.subTitleFormatted = false;
            this.subTitle2Formatted = false;
            this.scrollItemLayoutId = -1;
            this.iconList = null;
            this.iconIds = null;
            this.currentIconSelection = 0;
            this.extras = null;
            this.state = null;
            this.iconShape = null;
            this.needsRebind = false;
            this.dontStartFluctuator = false;
            this.fontSizeCheckDone = false;
            this.fontInfo = null;
            this.fontSize = null;
            this.noTextAnimation = false;
            this.noImageScaleAnimation = false;
            this.iconSize = -1;
        }
    }

    public static class ModelState {
        public boolean updateImage;
        public boolean updateSmallImage;
        public boolean updateSubTitle;
        public boolean updateSubTitle2;
        public boolean updateTitle;

        ModelState() {
            reset();
        }

        void reset() {
            this.updateImage = true;
            this.updateSmallImage = true;
            this.updateTitle = true;
            this.updateSubTitle = true;
            this.updateSubTitle2 = true;
        }
    }

    public enum ModelType {
        BLANK,
        TITLE,
        TITLE_SUBTITLE,
        ICON_BKCOLOR,
        TWO_ICONS,
        ICON,
        LOADING,
        ICON_OPTIONS,
        SCROLL_CONTENT,
        LOADING_CONTENT,
        SWITCH
    }

    static {
        Context context = HudApplication.getAppContext();
        Resources resources = context.getResources();
        vlistTitleTextW = resources.getDimensionPixelSize(R.dimen.vlist_title_text_len);
        vlistTitle_26 = resources.getDimension(R.dimen.vlist_title);
        vlistTitle_22 = resources.getDimension(R.dimen.vlist_title_22);
        vlistTitle_18 = resources.getDimension(R.dimen.vlist_title_18);
        vlistTitle_16 = resources.getDimension(R.dimen.vlist_title_16);
        vlistTitle_16_top_m_3 = resources.getDimension(R.dimen.vlist_16_title_top_m_3);
        vlistsubTitle_16_top_m_3 = resources.getDimension(R.dimen.vlist_16_subtitle_top_m_3);
        vlistsubTitle2_16_top_m_3 = resources.getDimension(R.dimen.vlist_16_subtitle2_top_m_3);
        vlistTitle_18_top_m_3 = resources.getDimension(R.dimen.vlist_18_title_top_m_3);
        vlistsubTitle_18_top_m_3 = resources.getDimension(R.dimen.vlist_18_subtitle_top_m_3);
        vlistsubTitle2_18_top_m_3 = resources.getDimension(R.dimen.vlist_18_subtitle2_top_m_3);
        vlistTitle_22_top_m_3 = resources.getDimension(R.dimen.vlist_22_title_top_m_3);
        vlistsubTitle_22_top_m_3 = resources.getDimension(R.dimen.vlist_22_subtitle_top_m_3);
        vlistsubTitle2_22_top_m_3 = resources.getDimension(R.dimen.vlist_22_subtitle2_top_m_3);
        vlistTitle_26_top_m_3 = resources.getDimension(R.dimen.vlist_26_title_top_m_3);
        vlistsubTitle_26_top_m_3 = resources.getDimension(R.dimen.vlist_26_subtitle_top_m_3);
        vlistsubTitle2_26_top_m_3 = resources.getDimension(R.dimen.vlist_26_subtitle2_top_m_3);
        vlistTitle_16_top_m_2 = resources.getDimension(R.dimen.vlist_16_title_top_m_2);
        vlistsubTitle_16_top_m_2 = resources.getDimension(R.dimen.vlist_16_subtitle_top_m_2);
        vlistTitle_18_top_m_2 = resources.getDimension(R.dimen.vlist_18_title_top_m_2);
        vlistsubTitle_18_top_m_2 = resources.getDimension(R.dimen.vlist_18_subtitle_top_m_2);
        vlistTitle_22_top_m_2 = resources.getDimension(R.dimen.vlist_22_title_top_m_2);
        vlistsubTitle_22_top_m_2 = resources.getDimension(R.dimen.vlist_22_subtitle_top_m_2);
        vlistTitle_26_top_m_2 = resources.getDimension(R.dimen.vlist_26_title_top_m_2);
        vlistsubTitle_26_top_m_2 = resources.getDimension(R.dimen.vlist_26_subtitle_top_m_2);
        vlistTitle_26_18_title_top = resources.getDimension(R.dimen.vlist_26_18_title_top);
        vlistTitle_26_18_subtitle_top = resources.getDimension(R.dimen.vlist_26_18_subtitle_top);
        vlistTitle_22_18_title_top = resources.getDimension(R.dimen.vlist_22_18_title_top);
        vlistTitle_22_18_subtitle_top = resources.getDimension(R.dimen.vlist_22_18_subtitle_top);
        vlistTitle_18_16_title_top = resources.getDimension(R.dimen.vlist_18_16_title_top);
        vlistTitle_18_16_subtitle_top = resources.getDimension(R.dimen.vlist_18_16_subtitle_top);
        vlistTitle_16_16_title_top = resources.getDimension(R.dimen.vlist_16_16_title_top);
        vlistTitle_16_16_subtitle_top = resources.getDimension(R.dimen.vlist_16_16_subtitle_top);
        vlistTitle_26_16_title_top = resources.getDimension(R.dimen.vlist_26_16_title_top);
        vlistTitle_26_16_subtitle_top = resources.getDimension(R.dimen.vlist_26_16_subtitle_top);
        vlistTitle_22_16_title_top = resources.getDimension(R.dimen.vlist_22_16_title_top);
        vlistTitle_22_16_subtitle_top = resources.getDimension(R.dimen.vlist_22_16_subtitle_top);
        FontInfo fontInfo = new FontInfo();
        fontInfo.titleFontSize = vlistTitle_26;
        fontInfo.titleFontTopMargin = vlistTitle_26_top_m_3;
        fontInfo.titleScale = 0.69f;
        fontInfo.subTitleFontSize = vlistTitle_16;
        fontInfo.subTitleFontTopMargin = vlistsubTitle_26_top_m_3;
        fontInfo.subTitle2FontSize = vlistTitle_16;
        fontInfo.subTitle2FontTopMargin = vlistsubTitle2_26_top_m_3;
        tit_subt_subt2_map.put(FontSize.FONT_SIZE_26, fontInfo);
        fontInfo = new FontInfo();
        fontInfo.titleFontSize = vlistTitle_22;
        fontInfo.titleFontTopMargin = vlistTitle_22_top_m_3;
        fontInfo.titleScale = 0.81f;
        fontInfo.titleSingleLine = false;
        fontInfo.subTitleFontSize = vlistTitle_16;
        fontInfo.subTitleFontTopMargin = vlistsubTitle_22_top_m_3;
        fontInfo.subTitle2FontSize = vlistTitle_16;
        fontInfo.subTitle2FontTopMargin = vlistsubTitle2_22_top_m_3;
        tit_subt_subt2_map.put(FontSize.FONT_SIZE_22, fontInfo);
        fontInfo = new FontInfo();
        fontInfo.titleFontSize = vlistTitle_22;
        fontInfo.titleFontTopMargin = vlistTitle_22_top_m_3;
        fontInfo.titleScale = 0.81f;
        fontInfo.titleSingleLine = false;
        fontInfo.subTitleFontSize = vlistTitle_16;
        fontInfo.subTitleFontTopMargin = vlistsubTitle_22_top_m_3 + 3.0f;
        fontInfo.subTitle2FontSize = vlistTitle_16;
        fontInfo.subTitle2FontTopMargin = vlistsubTitle2_22_top_m_3 + 3.0f;
        tit_subt_subt2_map.put(FontSize.FONT_SIZE_22_2, fontInfo);
        fontInfo = new FontInfo();
        fontInfo.titleFontSize = vlistTitle_18;
        fontInfo.titleFontTopMargin = vlistTitle_18_top_m_3;
        fontInfo.titleScale = 1.0f;
        fontInfo.subTitleFontSize = vlistTitle_16;
        fontInfo.subTitleFontTopMargin = vlistsubTitle_18_top_m_3;
        fontInfo.subTitle2FontSize = vlistTitle_16;
        fontInfo.subTitle2FontTopMargin = vlistsubTitle2_18_top_m_3;
        tit_subt_subt2_map.put(FontSize.FONT_SIZE_18, fontInfo);
        fontInfo = new FontInfo();
        fontInfo.titleFontSize = vlistTitle_18;
        fontInfo.titleFontTopMargin = vlistTitle_18_top_m_3;
        fontInfo.titleScale = 1.0f;
        fontInfo.titleSingleLine = false;
        fontInfo.subTitleFontSize = vlistTitle_16;
        fontInfo.subTitleFontTopMargin = vlistsubTitle_18_top_m_3;
        fontInfo.subTitle2FontSize = vlistTitle_16;
        fontInfo.subTitle2FontTopMargin = vlistsubTitle2_18_top_m_3;
        tit_subt_subt2_map.put(FontSize.FONT_SIZE_18_2, fontInfo);
        fontInfo = new FontInfo();
        fontInfo.titleFontSize = vlistTitle_16;
        fontInfo.titleFontTopMargin = vlistTitle_16_top_m_3;
        fontInfo.titleScale = 1.0f;
        fontInfo.subTitleFontSize = vlistTitle_16;
        fontInfo.subTitleFontTopMargin = vlistsubTitle_16_top_m_3;
        fontInfo.subTitle2FontSize = vlistTitle_16;
        fontInfo.subTitle2FontTopMargin = vlistsubTitle2_16_top_m_3;
        tit_subt_subt2_map.put(FontSize.FONT_SIZE_16, fontInfo);
        fontInfo = new FontInfo();
        fontInfo.titleFontSize = vlistTitle_16;
        fontInfo.titleFontTopMargin = vlistTitle_16_top_m_3;
        fontInfo.titleScale = 1.0f;
        fontInfo.titleSingleLine = false;
        fontInfo.subTitleFontSize = vlistTitle_16;
        fontInfo.subTitleFontTopMargin = vlistsubTitle_16_top_m_3;
        fontInfo.subTitle2FontSize = vlistTitle_16;
        fontInfo.subTitle2FontTopMargin = vlistsubTitle2_16_top_m_3;
        tit_subt_subt2_map.put(FontSize.FONT_SIZE_16_2, fontInfo);
        fontInfo = new FontInfo();
        fontInfo.titleFontSize = vlistTitle_26;
        fontInfo.titleFontTopMargin = vlistTitle_26_top_m_2;
        fontInfo.titleScale = 0.69f;
        fontInfo.subTitleFontSize = vlistTitle_16;
        fontInfo.subTitleFontTopMargin = vlistsubTitle_26_top_m_2;
        tit_subt_map.put(FontSize.FONT_SIZE_26, fontInfo);
        fontInfo = new FontInfo();
        fontInfo.titleFontSize = vlistTitle_22;
        fontInfo.titleFontTopMargin = vlistTitle_22_top_m_2;
        fontInfo.titleScale = 0.81f;
        fontInfo.subTitleFontSize = vlistTitle_16;
        fontInfo.subTitleFontTopMargin = vlistsubTitle_22_top_m_2;
        tit_subt_map.put(FontSize.FONT_SIZE_22, fontInfo);
        fontInfo = new FontInfo();
        fontInfo.titleFontSize = vlistTitle_22;
        fontInfo.titleFontTopMargin = vlistTitle_22_top_m_2 + 3.0f;
        fontInfo.titleScale = 0.81f;
        fontInfo.titleSingleLine = false;
        fontInfo.subTitleFontSize = vlistTitle_16;
        fontInfo.subTitleFontTopMargin = vlistsubTitle_22_top_m_2 + 9.0f;
        tit_subt_map.put(FontSize.FONT_SIZE_22_2, fontInfo);
        fontInfo = new FontInfo();
        fontInfo.titleFontSize = vlistTitle_18;
        fontInfo.titleFontTopMargin = vlistTitle_18_top_m_2;
        fontInfo.titleScale = 1.0f;
        fontInfo.subTitleFontSize = vlistTitle_16;
        fontInfo.subTitleFontTopMargin = vlistsubTitle_18_top_m_2;
        tit_subt_map.put(FontSize.FONT_SIZE_18, fontInfo);
        fontInfo = new FontInfo();
        fontInfo.titleFontSize = vlistTitle_18;
        fontInfo.titleFontTopMargin = vlistTitle_18_top_m_2 + 1.0f;
        fontInfo.titleScale = 1.0f;
        fontInfo.titleSingleLine = false;
        fontInfo.subTitleFontSize = vlistTitle_16;
        fontInfo.subTitleFontTopMargin = vlistsubTitle_18_top_m_2 + 7.0f;
        tit_subt_map.put(FontSize.FONT_SIZE_18_2, fontInfo);
        fontInfo = new FontInfo();
        fontInfo.titleFontSize = vlistTitle_16;
        fontInfo.titleFontTopMargin = vlistTitle_16_top_m_2;
        fontInfo.titleScale = 1.0f;
        fontInfo.subTitleFontSize = vlistTitle_16;
        fontInfo.subTitleFontTopMargin = vlistsubTitle_16_top_m_2;
        tit_subt_map.put(FontSize.FONT_SIZE_16, fontInfo);
        fontInfo = new FontInfo();
        fontInfo.titleFontSize = vlistTitle_16;
        fontInfo.titleFontTopMargin = vlistTitle_16_top_m_2 + 1.0f;
        fontInfo.titleScale = 1.0f;
        fontInfo.titleSingleLine = false;
        fontInfo.subTitleFontSize = vlistTitle_16;
        fontInfo.subTitleFontTopMargin = vlistsubTitle_16_top_m_2 + 7.0f;
        tit_subt_map.put(FontSize.FONT_SIZE_16_2, fontInfo);
        fontInfo = new FontInfo();
        fontInfo.titleFontSize = vlistTitle_26;
        fontInfo.titleFontTopMargin = vlistTitle_26_18_title_top;
        fontInfo.titleScale = 0.69f;
        fontInfo.subTitleFontSize = vlistTitle_18;
        fontInfo.subTitleFontTopMargin = vlistTitle_26_18_subtitle_top;
        tit_subt_map_2_lines.put(FontSize.FONT_SIZE_26_18, fontInfo);
        fontInfo = new FontInfo();
        fontInfo.titleFontSize = vlistTitle_22;
        fontInfo.titleFontTopMargin = vlistTitle_22_18_title_top;
        fontInfo.titleScale = 0.81f;
        fontInfo.subTitleFontSize = vlistTitle_18;
        fontInfo.subTitleFontTopMargin = vlistTitle_22_18_subtitle_top;
        tit_subt_map_2_lines.put(FontSize.FONT_SIZE_22_18, fontInfo);
        fontInfo = new FontInfo();
        fontInfo.titleFontSize = vlistTitle_22;
        fontInfo.titleFontTopMargin = vlistTitle_22_18_title_top;
        fontInfo.titleScale = 0.81f;
        fontInfo.titleSingleLine = false;
        fontInfo.subTitleFontSize = vlistTitle_18;
        fontInfo.subTitleFontTopMargin = vlistTitle_22_18_subtitle_top;
        tit_subt_map_2_lines.put(FontSize.FONT_SIZE_22_2_18, fontInfo);
        fontInfo = new FontInfo();
        fontInfo.titleFontSize = vlistTitle_18;
        fontInfo.titleFontTopMargin = vlistTitle_18_16_title_top;
        fontInfo.titleScale = 1.0f;
        fontInfo.subTitleFontSize = vlistTitle_16;
        fontInfo.subTitleFontTopMargin = vlistTitle_18_16_subtitle_top;
        tit_subt_map_2_lines.put(FontSize.FONT_SIZE_18_16, fontInfo);
        fontInfo = new FontInfo();
        fontInfo.titleFontSize = vlistTitle_18;
        fontInfo.titleFontTopMargin = vlistTitle_18_top_m_2 + 1.0f;
        fontInfo.titleScale = 1.0f;
        fontInfo.titleSingleLine = false;
        fontInfo.subTitleFontSize = vlistTitle_16;
        fontInfo.subTitleFontTopMargin = vlistsubTitle_18_top_m_2 + 7.0f;
        tit_subt_map_2_lines.put(FontSize.FONT_SIZE_18_2_16, fontInfo);
        fontInfo = new FontInfo();
        fontInfo.titleFontSize = vlistTitle_16;
        fontInfo.titleFontTopMargin = vlistTitle_16_16_title_top;
        fontInfo.titleScale = 1.0f;
        fontInfo.subTitleFontSize = vlistTitle_16;
        fontInfo.subTitleFontTopMargin = vlistTitle_16_16_subtitle_top;
        tit_subt_map_2_lines.put(FontSize.FONT_SIZE_16_16, fontInfo);
        fontInfo = new FontInfo();
        fontInfo.titleFontSize = vlistTitle_16;
        fontInfo.titleFontTopMargin = vlistTitle_16_16_title_top;
        fontInfo.titleScale = 1.0f;
        fontInfo.titleSingleLine = false;
        fontInfo.subTitleFontSize = vlistTitle_16;
        fontInfo.subTitleFontTopMargin = vlistTitle_16_16_subtitle_top;
        tit_subt_map_2_lines.put(FontSize.FONT_SIZE_16_2_16, fontInfo);
        fontInfo = new FontInfo();
        fontInfo.titleFontSize = vlistTitle_26;
        fontInfo.titleFontTopMargin = vlistTitle_26_16_title_top;
        fontInfo.titleScale = 0.69f;
        fontInfo.subTitleFontSize = vlistTitle_16;
        fontInfo.subTitleFontTopMargin = vlistTitle_26_16_subtitle_top;
        tit_subt_map_2_lines.put(FontSize.FONT_SIZE_26_16, fontInfo);
        fontInfo = new FontInfo();
        fontInfo.titleFontSize = vlistTitle_22;
        fontInfo.titleFontTopMargin = vlistTitle_22_16_title_top;
        fontInfo.titleScale = 0.81f;
        fontInfo.subTitleFontSize = vlistTitle_16;
        fontInfo.subTitleFontTopMargin = vlistTitle_22_16_subtitle_top;
        tit_subt_map_2_lines.put(FontSize.FONT_SIZE_22_16, fontInfo);
        fontInfo = new FontInfo();
        fontInfo.titleFontSize = vlistTitle_22;
        fontInfo.titleFontTopMargin = vlistTitle_22_top_m_2 + 1.0f;
        fontInfo.titleScale = 0.81f;
        fontInfo.titleSingleLine = false;
        fontInfo.subTitleFontSize = vlistTitle_16;
        fontInfo.subTitleFontTopMargin = vlistTitle_22_16_subtitle_top + 6.0f;
        tit_subt_map_2_lines.put(FontSize.FONT_SIZE_22_2_16, fontInfo);
        fontSizeTextView = new TextView(context);
        fontSizeTextView.setTextAppearance(context, R.style.vlist_title);
    }

    public static FontInfo getFontInfo(FontSize fontSize) {
        return (FontInfo) tit_subt_map.get(fontSize);
    }

    private void onItemSelected() {
        int userPos = getCurrentPosition();
        Model current = this.adapter.getModel(this.currentMiddlePosition);
        if (current != null) {
            int subId = -1;
            int subPos = -1;
            if (current.type == ModelType.ICON_OPTIONS) {
                VerticalViewHolder iconOptionsViewHolder = (VerticalViewHolder) this.recyclerView.findViewHolderForAdapterPosition(this.currentMiddlePosition);
                if (iconOptionsViewHolder instanceof IconOptionsViewHolder) {
                    IconOptionsViewHolder iconOptionsVH = (IconOptionsViewHolder) iconOptionsViewHolder;
                    subId = iconOptionsVH.getCurrentSelectionId();
                    subPos = iconOptionsVH.getCurrentSelection();
                }
            }
            this.itemSelectionState.set(current, current.id, userPos, subId, subPos);
            this.callback.onItemSelected(this.itemSelectionState);
        }
    }

    public VerticalList(VerticalRecyclerView recyclerView, CarouselIndicator indicator, Callback callback, ContainerCallback containerCallback) {
        this(recyclerView, indicator, callback, containerCallback, false);
    }

    public VerticalList(VerticalRecyclerView recyclerView, CarouselIndicator indicator, Callback callback, ContainerCallback containerCallback, boolean twoLineTitles) {
        this.scrollPendingPos = -1;
        this.indicatorAnimationListener = new DefaultAnimationListener() {
            public void onAnimationEnd(Animator animation) {
                if (VerticalList.this.containerCallback == null || !VerticalList.this.containerCallback.isFastScrolling()) {
                    VerticalList.this.indicator.setCurrentItem(VerticalList.this.getCurrentPosition());
                }
                VerticalList.this.indicatorAnimatorSet = null;
            }
        };
        this.scrollItemIndex = -1;
        this.scrollItemStartY = -1;
        this.scrollItemEndY = -1;
        this.scrollItemHeight = -1;
        this.initialPosCheckRunnable = new Runnable() {
            public void run() {
                if (VerticalList.this.initialPosChanged) {
                    VerticalList.sLogger.v("initial pos changed");
                    return;
                }
                int n = VerticalList.this.layoutManager.findFirstCompletelyVisibleItemPosition();
                int raw = VerticalList.this.getRawPosition();
                if (n != 0 || VerticalList.this.adapter.getItemCount() <= 3) {
                    VerticalList.sLogger.w("initial scroll worked firstV=" + n + " raw=" + raw + " scrollY=" + VerticalList.this.actualScrollY);
                } else {
                    VerticalList.sLogger.w("initial scroll did not work, firstV=" + n + " raw=" + raw + " scrollY=" + VerticalList.this.actualScrollY);
                }
                VerticalList.this.onItemSelected();
            }
        };
        this.keyHandlerState = new KeyHandlerState();
        this.itemSelectionState = new ItemSelectionState();
        this.copyList = new ArrayList();
        this.scrollListener = new OnScrollListener() {
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                VerticalList.this.initialPosChanged = true;
                VerticalList.this.lastScrollState = newState;
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == 0) {
                    boolean z;
                    if (VerticalList.this.reCalcScrollPos) {
                        VerticalList.this.calculateScrollRange();
                    }
                    VerticalList.this.isScrolling = false;
                    VerticalList.this.isScrollBy = false;
                    VerticalList.this.switchingScrollBoundary = false;
                    VerticalList.this.scrollByUpReceived = 0;
                    int pos = VerticalList.this.getPositionFromScrollY(VerticalList.this.actualScrollY);
                    VerticalViewHolder vhCurrent = (VerticalViewHolder) recyclerView.findViewHolderForAdapterPosition(pos);
                    Logger access$500 = VerticalList.sLogger;
                    StringBuilder append = new StringBuilder().append("onScrollStateChanged idle pos:").append(pos).append(" current=").append(VerticalList.this.currentMiddlePosition).append(" scrollY=").append(VerticalList.this.actualScrollY).append(" vh=");
                    if (vhCurrent != null) {
                        z = true;
                    } else {
                        z = false;
                    }
                    access$500.v(append.append(z).toString());
                    moveItemToSelectedState(vhCurrent, pos);
                    int upPos = VerticalList.this.currentMiddlePosition - 1;
                    if (upPos >= 0) {
                        moveItemToUnSelectedState((VerticalViewHolder) recyclerView.findViewHolderForAdapterPosition(upPos), upPos);
                    }
                    int downPos = VerticalList.this.currentMiddlePosition + 1;
                    moveItemToUnSelectedState((VerticalViewHolder) recyclerView.findViewHolderForAdapterPosition(downPos), downPos);
                    VerticalList.this.onItemSelected();
                    if (VerticalList.this.isSelectedOperationPending) {
                        VerticalList.this.isSelectedOperationPending = false;
                        VerticalList.this.select(true);
                    } else if (VerticalList.this.scrollPendingPos != -1) {
                        int p = VerticalList.this.scrollPendingPos;
                        VerticalList.this.scrollPendingPos = -1;
                        VerticalList.this.scrollToPosition(p);
                        VerticalList.sLogger.v("onScrolled idle scrollToPos:" + p);
                    }
                    if (VerticalList.this.sendScrollIdleEvent) {
                        VerticalList.this.callback.onScrollIdle();
                    }
                }
            }

            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                VerticalList.this.actualScrollY = VerticalList.this.actualScrollY + dy;
                if (VerticalList.this.waitForTarget) {
                    VerticalList.this.targetFound(0);
                }
            }

            private void moveItemToSelectedState(VerticalViewHolder vh, int pos) {
                if (vh == null) {
                    return;
                }
                if (pos != VerticalList.this.currentMiddlePosition || vh.getState() == State.UNSELECTED) {
                    VerticalList.this.currentMiddlePosition = pos;
                    if (VerticalList.this.isCloseMenuVisible()) {
                        VerticalList.sLogger.v("onScrollStateChanged idle-select-un pos:" + pos);
                        vh.setState(State.UNSELECTED, AnimationType.NONE, VerticalList.ITEM_MOVE_ANIMATION_DURATION);
                        return;
                    }
                    VerticalList.sLogger.v("onScrollStateChanged idle-select pos:" + pos);
                    vh.setState(State.SELECTED, AnimationType.NONE, VerticalList.ITEM_MOVE_ANIMATION_DURATION);
                } else if (pos == VerticalList.this.currentMiddlePosition && VerticalList.this.isCloseMenuVisible() && vh.getState() == State.SELECTED) {
                    VerticalList.sLogger.v("onScrollStateChanged idle-select-un-2 pos:" + pos);
                    vh.setState(State.UNSELECTED, AnimationType.NONE, VerticalList.ITEM_MOVE_ANIMATION_DURATION);
                }
            }

            private void moveItemToUnSelectedState(VerticalViewHolder vh, int pos) {
                if (vh != null && vh.getModelType() != ModelType.BLANK && vh.getState() == State.SELECTED) {
                    VerticalList.sLogger.v("onScrollStateChanged idle-unselect pos:" + pos);
                    vh.setState(State.UNSELECTED, AnimationType.NONE, VerticalList.ITEM_MOVE_ANIMATION_DURATION);
                }
            }
        };
        this.selectedList = new HashSet();
        if (recyclerView == null || indicator == null || callback == null) {
            throw new IllegalArgumentException();
        }
        sLogger.v("ctor");
        this.twoLineTitles = twoLineTitles;
        this.animationDuration = ITEM_MOVE_ANIMATION_DURATION;
        Context context = recyclerView.getContext();
        this.recyclerView = recyclerView;
        this.recyclerView.setItemViewCacheSize(5);
        RecycledViewPool viewPool = this.recyclerView.getRecycledViewPool();
        viewPool.setMaxRecycledViews(ModelType.BLANK.ordinal(), 2);
        viewPool.setMaxRecycledViews(ModelType.ICON.ordinal(), 10);
        viewPool.setMaxRecycledViews(ModelType.ICON_BKCOLOR.ordinal(), 10);
        viewPool.setMaxRecycledViews(ModelType.TWO_ICONS.ordinal(), 10);
        viewPool.setMaxRecycledViews(ModelType.TITLE.ordinal(), 1);
        viewPool.setMaxRecycledViews(ModelType.TITLE_SUBTITLE.ordinal(), 1);
        viewPool.setMaxRecycledViews(ModelType.LOADING.ordinal(), 1);
        viewPool.setMaxRecycledViews(ModelType.ICON_OPTIONS.ordinal(), 1);
        viewPool.setMaxRecycledViews(ModelType.SCROLL_CONTENT.ordinal(), 0);
        viewPool.setMaxRecycledViews(ModelType.LOADING_CONTENT.ordinal(), 10);
        this.recyclerView.setOverScrollMode(2);
        this.recyclerView.setVerticalScrollBarEnabled(false);
        this.recyclerView.setHorizontalScrollBarEnabled(false);
        this.indicator = indicator;
        this.indicator.setOrientation(Orientation.VERTICAL);
        this.callback = callback;
        this.containerCallback = containerCallback;
        this.layoutManager = new VerticalLayoutManager(context, this, callback, recyclerView);
        this.layoutManager.setOrientation(1);
        this.recyclerView.setLayoutManager(this.layoutManager);
        this.recyclerView.addOnScrollListener(this.scrollListener);
    }

    public void setBindCallbacks(boolean enable) {
        this.bindCallbacks = enable;
    }

    public void setScrollIdleEvent(boolean send) {
        this.sendScrollIdleEvent = send;
    }

    public void updateView(List<Model> list, int initialSelection, boolean firstEntryBlank) {
        updateView(list, initialSelection, firstEntryBlank, false, -1);
    }

    public void updateViewWithScrollableContent(List<Model> list, int initialSelection, boolean firstEntryBlank) {
        int scrollItemCount = 0;
        int index = -1;
        int counter = 0;
        for (Model model : list) {
            if (model.type == ModelType.SCROLL_CONTENT) {
                scrollItemCount++;
                index = counter;
            }
            counter++;
        }
        if (list.size() == 1 || scrollItemCount == 0 || scrollItemCount > 1) {
            throw new RuntimeException("invalid scroll item model size=" + list.size() + " count=" + scrollItemCount);
        }
        updateView(list, initialSelection, firstEntryBlank, true, index);
    }

    public boolean allowsTwoLineTitles() {
        return this.twoLineTitles;
    }

    private void updateView(List<Model> list, int initialSelection, boolean firstEntryBlank, boolean hasScrollableElement, int scrollIndex) {
        if (!GenericUtil.isMainThread()) {
            throw new RuntimeException("updateView can only be called from main thread");
        } else if (isLocked()) {
            sLogger.w("cannot update vlist during lock");
        } else {
            int len = list.size();
            if (len == 0) {
                throw new IllegalArgumentException("empty list now allowed");
            }
            clearAllAnimations();
            this.recyclerView.stopScroll();
            sLogger.v("updateView [" + len + "] sel:" + initialSelection + " firstEntryBlank:" + firstEntryBlank + " scrollContent:" + hasScrollableElement + " scrollIndex:" + scrollIndex);
            this.modelList = new ArrayList(list);
            if (len <= 1) {
                initialSelection = 0;
            }
            int origSelection = initialSelection;
            this.firstEntryBlank = firstEntryBlank;
            this.hasScrollableElement = hasScrollableElement;
            this.scrollItemIndex = scrollIndex;
            this.scrollItemStartY = -1;
            this.scrollItemEndY = -1;
            this.scrollItemHeight = -1;
            addExtraItems(this.modelList);
            this.currentMiddlePosition = initialSelection + 1;
            if (hasScrollableElement && firstEntryBlank) {
                this.scrollItemIndex++;
            }
            int indicatorLen = len;
            if (!firstEntryBlank) {
                indicatorLen--;
            }
            this.indicator.setItemCount(indicatorLen);
            this.indicator.setCurrentItem(origSelection);
            this.layoutManager.clear();
            this.adapter = new VerticalAdapter(this.modelList, this);
            this.adapter.setHasStableIds(true);
            int cacheIndexSize = 0;
            if (hasScrollableElement) {
                int[] cacheIndex;
                if (list.size() == this.scrollItemIndex) {
                    cacheIndex = new int[]{this.scrollItemIndex - 1};
                    cacheIndexSize = 1;
                } else {
                    cacheIndex = new int[]{this.scrollItemIndex - 1, this.scrollItemIndex + 1};
                    cacheIndexSize = 2;
                }
                this.adapter.setViewHolderCacheIndex(cacheIndex);
            }
            this.recyclerView.setAdapter(this.adapter);
            this.initialPosChanged = false;
            int scrollPos = this.currentMiddlePosition - 1;
            boolean specialScrollPos = false;
            if (hasScrollableElement) {
                sLogger.v("updateView initial:" + initialSelection + " scrollIndex=" + scrollIndex);
                if (initialSelection == scrollIndex) {
                    scrollPos = this.currentMiddlePosition;
                    this.actualScrollY = (initialSelection + 1) * VerticalViewHolder.listItemHeight;
                    sLogger.v("updateView: scroll index selected:" + this.actualScrollY);
                } else if (initialSelection < this.scrollItemIndex) {
                    this.actualScrollY = VerticalViewHolder.listItemHeight * initialSelection;
                } else {
                    this.reCalcScrollPos = true;
                    sLogger.v("reCalcScrollPos no ht");
                    this.actualScrollY = VerticalViewHolder.listItemHeight * initialSelection;
                    if (initialSelection == scrollIndex + 1) {
                        specialScrollPos = true;
                    }
                }
            } else {
                this.actualScrollY = VerticalViewHolder.listItemHeight * initialSelection;
            }
            if (specialScrollPos) {
                sLogger.v("updateView special scroll");
                this.layoutManager.scrollToPositionWithOffset(scrollPos + 1, VerticalViewHolder.listItemHeight);
            } else {
                this.layoutManager.scrollToPositionWithOffset(scrollPos, 0);
            }
            sLogger.v("updateView scroll to " + scrollPos + " scrollPos=" + this.actualScrollY + " middle=" + getCurrentPosition() + " raw=" + getRawPosition() + " adapter-cache:" + cacheIndexSize + " scrollIndex:" + scrollIndex);
            handler.post(this.initialPosCheckRunnable);
        }
    }

    public void scrollToPosition(int pos) {
        Model model = this.adapter.getModel(pos);
        if (model != null) {
            if (isLocked()) {
                sLogger.v("scrollToPos list locked:" + pos);
            } else if (this.isScrolling) {
                sLogger.v("scrollToPos wait for scroll idle:" + pos);
                this.scrollPendingPos = pos;
            } else {
                sLogger.v("scrollToPos:" + pos + " , " + model.title);
                this.currentMiddlePosition = pos;
                int scrollPos = this.currentMiddlePosition - 1;
                if (scrollPos < 0) {
                    scrollPos = 0;
                }
                this.actualScrollY = VerticalViewHolder.listItemHeight * scrollPos;
                this.adapter.setInitialState(false);
                this.layoutManager.scrollToPositionWithOffset(scrollPos, 0);
                sLogger.v("scrollToPos scroll to " + scrollPos + " scrollPos=" + this.actualScrollY + " middle=" + getCurrentPosition() + " raw=" + getRawPosition());
                this.scrollListener.onScrollStateChanged(this.recyclerView, 0);
            }
        }
    }

    private void addExtraItems(List<Model> list) {
        if (this.firstEntryBlank) {
            list.add(0, BLANK_ITEM_TOP);
        }
        list.add(BLANK_ITEM_BOTTOM);
    }

    public KeyHandlerState up() {
        this.keyHandlerState.clear();
        if (isLocked()) {
            sLogger.v("up locked");
            return this.keyHandlerState;
        } else if (this.switchingScrollBoundary) {
            this.keyHandlerState.keyHandled = true;
            return this.keyHandlerState;
        } else if (this.reCalcScrollPos && this.isScrolling) {
            this.keyHandlerState.keyHandled = true;
            return this.keyHandlerState;
        } else {
            if (this.hasScrollableElement && isScrollPosInScrollItem()) {
                int scrollDistance = canScrollUp();
                if (scrollDistance != -1) {
                    if (this.isScrolling) {
                        this.currentScrollBy += 0.5f;
                        if (this.currentScrollBy > 2.0f) {
                            this.currentScrollBy = 2.0f;
                        }
                    } else {
                        this.currentScrollBy = 1.0f;
                    }
                    float distance = ((float) VerticalViewHolder.scrollDistance) * this.currentScrollBy;
                    float scrollBy = ((float) scrollDistance) > distance ? distance : (float) scrollDistance;
                    this.isScrolling = true;
                    this.recyclerView.smoothScrollBy(0, (int) (-scrollBy));
                    SoundUtils.playSound(Sound.MENU_MOVE);
                    this.keyHandlerState.keyHandled = true;
                    return this.keyHandlerState;
                }
            }
            if (!isTop()) {
                this.keyHandlerState.keyHandled = true;
                if (isOverrideKey(CustomKeyEvent.LEFT)) {
                    SoundUtils.playSound(Sound.MENU_MOVE);
                    this.keyHandlerState.listMoved = false;
                    return this.keyHandlerState;
                }
                this.keyHandlerState.listMoved = true;
                up(this.currentMiddlePosition, this.currentMiddlePosition - 1);
                return this.keyHandlerState;
            } else if (isOverrideKey(CustomKeyEvent.LEFT)) {
                SoundUtils.playSound(Sound.MENU_MOVE);
                this.keyHandlerState.keyHandled = true;
                this.keyHandlerState.listMoved = false;
                return this.keyHandlerState;
            } else {
                if (this.isScrollBy) {
                    this.scrollByUpReceived++;
                    this.keyHandlerState.keyHandled = true;
                }
                if (this.isScrolling) {
                    this.keyHandlerState.keyHandled = true;
                }
                sLogger.v("cannot go up:" + this.currentMiddlePosition + " isScrolling:" + this.isScrolling);
                return this.keyHandlerState;
            }
        }
    }

    private void up(int oldPos, int newPos) {
        SoundUtils.playSound(Sound.MENU_MOVE);
        this.currentMiddlePosition = newPos;
        this.isScrolling = true;
        this.keyHandlerState.keyHandled = true;
        if (this.hasScrollableElement) {
            boolean updateIndicator = true;
            boolean highlightItem = false;
            boolean checkAdapterCache = false;
            if (isScrollItemInMiddle()) {
                this.switchingScrollBoundary = true;
                this.recyclerView.smoothScrollBy(0, -VerticalViewHolder.listItemHeight);
                highlightItem = true;
            } else if (isScrollItemAlignedToTopEdge()) {
                this.isScrollBy = true;
                this.switchingScrollBoundary = true;
                updateIndicator = true;
                highlightItem = true;
                checkAdapterCache = true;
                this.layoutManager.smoothScrollToPosition(this.currentMiddlePosition - 1, -1);
            } else if (this.currentMiddlePosition == this.scrollItemIndex) {
                float distance = (float) (VerticalViewHolder.listItemHeight * 2);
                this.switchingScrollBoundary = true;
                this.recyclerView.smoothScrollBy(0, (int) (-distance));
                updateIndicator = true;
                highlightItem = true;
            } else {
                this.layoutManager.smoothScrollToPosition(oldPos, 1);
            }
            animate(oldPos, newPos, updateIndicator, highlightItem, checkAdapterCache);
            return;
        }
        animate(oldPos, newPos);
        this.layoutManager.smoothScrollToPosition(oldPos, 1);
    }

    public KeyHandlerState down() {
        this.keyHandlerState.clear();
        if (isLocked()) {
            sLogger.v("down locked");
            return this.keyHandlerState;
        } else if (this.switchingScrollBoundary) {
            this.keyHandlerState.keyHandled = true;
            return this.keyHandlerState;
        } else {
            if (this.hasScrollableElement) {
                int oldPos;
                if (this.currentMiddlePosition + 1 == this.scrollItemIndex) {
                    oldPos = this.currentMiddlePosition;
                    this.currentMiddlePosition++;
                    SoundUtils.playSound(Sound.MENU_MOVE);
                    this.switchingScrollBoundary = true;
                    this.isScrolling = true;
                    this.layoutManager.smoothScrollToPosition(this.currentMiddlePosition, -1);
                    animate(oldPos, this.currentMiddlePosition, true, true, false);
                    this.keyHandlerState.listMoved = true;
                    this.keyHandlerState.keyHandled = true;
                    return this.keyHandlerState;
                } else if (isScrollPosInScrollItem()) {
                    int scrollDistance = canScrollDown();
                    float distance;
                    if (scrollDistance != -1) {
                        SoundUtils.playSound(Sound.MENU_MOVE);
                        if (this.isScrolling) {
                            this.currentScrollBy += 0.5f;
                            if (this.currentScrollBy > 2.0f) {
                                this.currentScrollBy = 2.0f;
                            }
                        } else {
                            this.currentScrollBy = 1.0f;
                        }
                        distance = ((float) VerticalViewHolder.scrollDistance) * this.currentScrollBy;
                        float scrollBy = ((float) scrollDistance) > distance ? distance : (float) scrollDistance;
                        this.isScrolling = true;
                        this.recyclerView.smoothScrollBy(0, (int) scrollBy);
                        this.keyHandlerState.keyHandled = true;
                        return this.keyHandlerState;
                    } else if (!isBottom()) {
                        SoundUtils.playSound(Sound.MENU_MOVE);
                        distance = (float) (VerticalViewHolder.listItemHeight * 2);
                        oldPos = this.currentMiddlePosition;
                        int newPos = this.currentMiddlePosition + 1;
                        this.switchingScrollBoundary = true;
                        this.isScrolling = true;
                        this.currentMiddlePosition = newPos;
                        this.recyclerView.smoothScrollBy(0, (int) distance);
                        animate(oldPos, newPos, true, true, true);
                        this.keyHandlerState.listMoved = true;
                        this.keyHandlerState.keyHandled = true;
                        return this.keyHandlerState;
                    }
                }
            }
            if (isBottom()) {
                sLogger.v("cannot go down:" + this.currentMiddlePosition);
                return this.keyHandlerState;
            }
            this.keyHandlerState.keyHandled = true;
            if (isOverrideKey(CustomKeyEvent.RIGHT)) {
                SoundUtils.playSound(Sound.MENU_MOVE);
                this.keyHandlerState.listMoved = false;
                return this.keyHandlerState;
            }
            this.keyHandlerState.listMoved = true;
            down(this.currentMiddlePosition, this.currentMiddlePosition + 1);
            return this.keyHandlerState;
        }
    }

    private void down(int oldPos, int newPos) {
        SoundUtils.playSound(Sound.MENU_MOVE);
        this.currentMiddlePosition = newPos;
        animate(oldPos, newPos);
        this.isScrolling = true;
        this.layoutManager.smoothScrollToPosition(oldPos, -1);
    }

    public void select() {
        select(false);
    }

    private void select(boolean ignoreLock) {
        if (!isLocked() || ignoreLock) {
            this.lockList = true;
            if (this.isScrolling) {
                this.isSelectedOperationPending = true;
                return;
            }
            SoundUtils.playSound(Sound.MENU_SELECT);
            int pos = getRawPosition();
            Model model = (Model) this.modelList.get(pos);
            VerticalViewHolder vh = (VerticalViewHolder) this.recyclerView.findViewHolderForAdapterPosition(pos);
            if (vh != null) {
                vh.select(model, getCurrentPosition(), 50);
            }
        }
    }

    public void lock() {
        this.lockList = true;
    }

    public void unlock() {
        unlock(true);
    }

    public void unlock(boolean startFluctuator) {
        this.lockList = false;
        VerticalViewHolder vh = (VerticalViewHolder) this.recyclerView.findViewHolderForAdapterPosition(getRawPosition());
        if (vh == null) {
            return;
        }
        if (startFluctuator) {
            vh.startFluctuator();
        } else {
            vh.clearAnimation();
        }
    }

    public boolean isTop() {
        if (this.currentMiddlePosition == 1) {
            return true;
        }
        return false;
    }

    public boolean isBottom() {
        if (this.currentMiddlePosition == this.modelList.size() - 2) {
            return true;
        }
        return false;
    }

    public int getCurrentPosition() {
        return this.currentMiddlePosition - 1;
    }

    public Model getCurrentModel() {
        return this.adapter.getModel(getRawPosition());
    }

    public VerticalViewHolder getCurrentViewHolder() {
        return getViewHolder(this.currentMiddlePosition);
    }

    public VerticalViewHolder getViewHolder(int pos) {
        if (this.adapter.getModel(pos) != null) {
            return (VerticalViewHolder) this.recyclerView.findViewHolderForAdapterPosition(this.currentMiddlePosition);
        }
        sLogger.i("getViewHolder: invalid pos:" + pos);
        return null;
    }

    public void setViewHolderState(int pos, State state, AnimationType animationType, int animationDuration) {
        VerticalViewHolder vh = getViewHolder(pos);
        if (vh != null && vh.getState() != state) {
            sLogger.v("setViewHolderState:" + pos + GlanceConstants.COLON_SEPARATOR + state);
            vh.setState(state, animationType, animationDuration);
        }
    }

    public int getRawPosition() {
        return this.currentMiddlePosition;
    }

    private void animate(int oldPos, int newPos) {
        animate(oldPos, newPos, true, false, false);
    }

    private void animate(int oldPos, int newPos, boolean updateIndicator, boolean performSelection, boolean checkAdapterCache) {
        if (oldPos != -1) {
            VerticalViewHolder vh;
            ViewHolder viewHolder = this.recyclerView.findViewHolderForAdapterPosition(oldPos);
            if (viewHolder != null) {
                vh = (VerticalViewHolder) viewHolder;
                if (vh.getState() == State.SELECTED) {
                    vh.setState(State.UNSELECTED, AnimationType.MOVE, ITEM_MOVE_ANIMATION_DURATION);
                }
            } else {
                sLogger.v("viewHolder {" + oldPos + "} not found");
            }
            if (performSelection) {
                viewHolder = this.recyclerView.findViewHolderForAdapterPosition(newPos);
                if (viewHolder != null) {
                    vh = (VerticalViewHolder) viewHolder;
                    if (vh.getState() == State.UNSELECTED && !isCloseMenuVisible()) {
                        vh.setState(State.SELECTED, AnimationType.MOVE, ITEM_MOVE_ANIMATION_DURATION);
                    }
                } else if (checkAdapterCache) {
                    vh = this.adapter.getHolderForPos(newPos);
                    if (vh == null) {
                        sLogger.v("viewHolder-sel {" + newPos + "} not found in adapter");
                        this.adapter.setHighlightIndex(newPos);
                    } else if (vh.getState() == State.UNSELECTED && !isCloseMenuVisible()) {
                        vh.setState(State.SELECTED, AnimationType.MOVE, ITEM_MOVE_ANIMATION_DURATION);
                    }
                } else {
                    sLogger.v("viewHolder-sel {" + newPos + "} not found");
                }
            }
        }
        if (updateIndicator) {
            animateIndicator(getCurrentPosition(), this.animationDuration);
        }
    }

    public void animate(int pos, boolean selected, int duration, boolean notAllowStateChange) {
        animate(pos, selected, duration, notAllowStateChange, false);
    }

    public void animate(int pos, boolean selected, int duration, boolean notAllowStateChange, boolean changeToolTips) {
        if (duration == -1) {
            duration = this.animationDuration;
        }
        ViewHolder viewHolder = this.recyclerView.findViewHolderForAdapterPosition(pos);
        if (viewHolder != null) {
            VerticalViewHolder vh = (VerticalViewHolder) viewHolder;
            if (selected) {
                if (sLogger.isLoggable(2)) {
                    sLogger.v("animate:" + pos + " selected");
                }
                vh.setState(State.SELECTED, AnimationType.MOVE, duration);
                if (vh.hasToolTip() && this.containerCallback != null) {
                    this.containerCallback.showToolTips();
                    return;
                }
                return;
            }
            if (sLogger.isLoggable(2)) {
                sLogger.v("animate:" + pos + " un-selected");
            }
            vh.setState(State.UNSELECTED, AnimationType.MOVE, duration);
            if (vh.hasToolTip() && this.containerCallback != null) {
                this.containerCallback.hideToolTips();
            }
        } else if (sLogger.isLoggable(2)) {
            sLogger.v("animate:" + pos + " cannot find viewholder");
        }
    }

    public void animateIndicator(int pos, int duration) {
        if (this.containerCallback == null || !this.containerCallback.isFastScrolling()) {
            if (this.indicatorAnimatorSet != null && this.indicatorAnimatorSet.isRunning()) {
                this.indicatorAnimatorSet.removeAllListeners();
                this.indicatorAnimatorSet.cancel();
                this.indicator.setCurrentItem(getCurrentPosition());
            }
            this.indicatorAnimatorSet = this.indicator.getItemMoveAnimator(pos, -1);
            if (this.indicatorAnimatorSet != null) {
                this.indicatorAnimatorSet.setDuration((long) duration);
                this.indicatorAnimatorSet.addListener(this.indicatorAnimationListener);
                this.indicatorAnimatorSet.start();
            }
        }
    }

    public static Interpolator getInterpolator() {
        return new FastOutSlowInInterpolator();
    }

    public void targetFound(int dy) {
        if (!this.hasScrollableElement || !isScrollPosInScrollItem()) {
            int middlePos = getPositionFromScrollY(this.actualScrollY + dy);
            boolean verbose = sLogger.isLoggable(2);
            if (dy != 0 || !this.waitForTarget || middlePos == this.targetPos) {
                this.waitForTarget = false;
                this.targetFound = false;
                this.targetPos = -1;
                VerticalViewHolder viewHolderSel = (VerticalViewHolder) this.recyclerView.findViewHolderForAdapterPosition(middlePos);
                if (viewHolderSel == null) {
                    this.waitForTarget = true;
                    this.targetPos = middlePos;
                    return;
                }
                this.targetFound = true;
                this.currentMiddlePosition = middlePos;
                HashSet<Integer> temp = this.selectedList;
                this.selectedList = new HashSet();
                if (verbose) {
                    sLogger.v("targetFound selected list size = " + temp.size());
                }
                if (temp.size() > 0) {
                    Iterator<Integer> iterator = temp.iterator();
                    while (iterator.hasNext()) {
                        VerticalViewHolder viewHolder = (VerticalViewHolder) this.recyclerView.findViewHolderForAdapterPosition(((Integer) iterator.next()).intValue());
                        if (viewHolder != null && viewHolder.getState() == State.SELECTED) {
                            viewHolder.setState(State.UNSELECTED, AnimationType.MOVE, ITEM_MOVE_ANIMATION_DURATION);
                        }
                    }
                }
                if (viewHolderSel != null && viewHolderSel.getState() == State.UNSELECTED && !isCloseMenuVisible()) {
                    viewHolderSel.setState(State.SELECTED, AnimationType.MOVE, ITEM_MOVE_ANIMATION_DURATION);
                }
            }
        }
    }

    private boolean isScrollItemInMiddle() {
        if (this.hasScrollableElement && this.scrollItemIndex > 0 && this.actualScrollY == VerticalViewHolder.listItemHeight * (this.scrollItemIndex - 1)) {
            return true;
        }
        return false;
    }

    private boolean isScrollItemAlignedToTopEdge() {
        if (this.hasScrollableElement && this.actualScrollY == VerticalViewHolder.listItemHeight * this.scrollItemIndex) {
            return true;
        }
        return false;
    }

    private boolean isScrollItemAlignedToBottomEdge() {
        if (this.hasScrollableElement && this.actualScrollY == this.scrollItemEndY + VerticalViewHolder.listItemHeight) {
            return true;
        }
        return false;
    }

    private boolean isScrollPosInScrollItem() {
        if (!this.hasScrollableElement) {
            return false;
        }
        calculateScrollRange();
        if (this.scrollItemStartY == -1 || this.actualScrollY < this.scrollItemStartY || this.actualScrollY > this.scrollItemEndY) {
            return false;
        }
        return true;
    }

    private int canScrollDown() {
        if (!this.hasScrollableElement) {
            return -1;
        }
        calculateScrollRange();
        if (this.scrollItemStartY == -1 || this.actualScrollY >= this.scrollItemEndY) {
            return -1;
        }
        return this.scrollItemEndY - this.actualScrollY;
    }

    private int canScrollUp() {
        if (!this.hasScrollableElement) {
            return -1;
        }
        calculateScrollRange();
        if (this.scrollItemStartY == -1) {
            return -1;
        }
        int scrollItemY = VerticalViewHolder.listItemHeight * this.scrollItemIndex;
        if (this.actualScrollY > scrollItemY) {
            return this.actualScrollY - scrollItemY;
        }
        return -1;
    }

    private int getExtraItemCount() {
        return this.adapter.getItemCount() - 3;
    }

    private boolean isLocked() {
        return this.lockList;
    }

    public void performSelectAction(ItemSelectionState selection) {
        this.callback.select(selection);
    }

    public void addCurrentHighlightAnimation(Builder builder) {
        VerticalViewHolder vh = (VerticalViewHolder) this.recyclerView.findViewHolderForAdapterPosition(this.currentMiddlePosition - 1);
        if (vh != null) {
            builder.with(ObjectAnimator.ofFloat(vh.layout, View.ALPHA, new float[]{0.0f}));
        }
        vh = (VerticalViewHolder) this.recyclerView.findViewHolderForAdapterPosition(this.currentMiddlePosition + 1);
        if (vh != null) {
            builder.with(ObjectAnimator.ofFloat(vh.layout, View.ALPHA, new float[]{0.0f}));
        }
    }

    public void copyAndPosition(ImageView imageView, TextView title, TextView subTitle, TextView subTitle2, boolean setImage) {
        VerticalViewHolder vh = (VerticalViewHolder) this.recyclerView.findViewHolderForAdapterPosition(this.currentMiddlePosition);
        if (vh != null) {
            vh.copyAndPosition(imageView, title, subTitle, subTitle2, setImage);
        }
    }

    public void refreshData(int pos) {
        refreshData(pos, (Model) null);
    }

    public void refreshData(int pos, Model newModel) {
        refreshData(pos, newModel, false);
    }

    public void refreshData(int pos, Model newModel, boolean dontStartFluctuator) {
        if (this.firstEntryBlank) {
            pos++;
        }
        Model currentModel = this.adapter.getModel(pos);
        if (currentModel == null) {
            sLogger.i("refreshData: invalid model pos:" + pos);
            return;
        }
        if (newModel == null) {
            currentModel.needsRebind = true;
            currentModel.dontStartFluctuator = dontStartFluctuator;
        } else {
            newModel.needsRebind = true;
            this.adapter.updateModel(pos, newModel);
        }
        this.adapter.notifyItemChanged(pos);
    }

    public void refreshData(int pos, Model[] newModels) {
        if (this.firstEntryBlank) {
            pos++;
        }
        int itemsUpdated = 0;
        for (int i = 0; i < newModels.length; i++) {
            int newPos = pos + i;
            if (this.adapter.getModel(newPos) == null) {
                sLogger.i("refreshData(s): invalid model pos:" + newPos);
            } else {
                itemsUpdated++;
                this.adapter.updateModel(newPos, newModels[i]);
            }
        }
        this.adapter.setInitialState(false);
        this.adapter.notifyItemRangeChanged(pos, itemsUpdated);
        sLogger.i("refreshData(s) pos=" + pos + " len=" + itemsUpdated);
    }

    public static ImageView findImageView(View layout) {
        return (ImageView) layout.findViewById(R.id.big);
    }

    public static ImageView findSmallImageView(View layout) {
        return (ImageView) layout.findViewById(R.id.small);
    }

    public static View findCrossFadeImageView(View layout) {
        return layout.findViewById(R.id.vlist_image);
    }

    public void cancelLoadingAnimation(int pos) {
        int userpos = pos;
        if (this.firstEntryBlank) {
            userpos++;
        }
        VerticalViewHolder vh = (VerticalViewHolder) this.recyclerView.findViewHolderForAdapterPosition(userpos);
        if (vh != null && vh.getModelType() == ModelType.LOADING) {
            vh.clearAnimation();
        }
    }

    private boolean isOverrideKey(CustomKeyEvent keyEvent) {
        VerticalViewHolder vh = (VerticalViewHolder) this.recyclerView.findViewHolderForAdapterPosition(this.currentMiddlePosition);
        if (vh == null || !vh.handleKey(keyEvent)) {
            return false;
        }
        return true;
    }

    public boolean isFirstEntryBlank() {
        return this.firstEntryBlank;
    }

    public boolean hasScrollItem() {
        return this.hasScrollableElement;
    }

    public ItemSelectionState getItemSelectionState() {
        return this.itemSelectionState;
    }

    public void callItemSelected(ItemSelectionState itemSelectionState) {
        if (this.callback != null) {
            this.callback.onItemSelected(itemSelectionState);
        }
    }

    public void clearAllAnimations() {
        int count = this.recyclerView.getChildCount();
        int cleared = 0;
        for (int i = 0; i < count; i++) {
            try {
                VerticalViewHolder holder = (VerticalViewHolder) this.recyclerView.findViewHolderForAdapterPosition(i);
                if (holder != null) {
                    holder.clearAnimation();
                    cleared++;
                }
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
        sLogger.v("clearAllAnimations current:" + cleared);
        RecycledViewPool pool = this.recyclerView.getRecycledViewPool();
        if (pool != null) {
            this.copyList.clear();
            clearViewAnimation(pool, ModelType.ICON_BKCOLOR.ordinal());
            clearViewAnimation(pool, ModelType.TWO_ICONS.ordinal());
            clearViewAnimation(pool, ModelType.ICON.ordinal());
            clearViewAnimation(pool, ModelType.LOADING.ordinal());
            clearViewAnimation(pool, ModelType.LOADING_CONTENT.ordinal());
            clearViewAnimation(pool, ModelType.ICON_OPTIONS.ordinal());
            cleanupView(pool, ModelType.SCROLL_CONTENT.ordinal());
            int n = this.copyList.size();
            if (n > 0) {
                Iterator it = this.copyList.iterator();
                while (it.hasNext()) {
                    pool.putRecycledView((ViewHolder) it.next());
                }
                sLogger.v("clearAllAnimations pool:" + n);
                this.copyList.clear();
            }
        }
    }

    private void clearViewAnimation(RecycledViewPool pool, int viewType) {
        while (true) {
            VerticalViewHolder viewHolder = (VerticalViewHolder) pool.getRecycledView(viewType);
            if (viewHolder != null) {
                viewHolder.clearAnimation();
                this.copyList.add(viewHolder);
            } else {
                return;
            }
        }
    }

    private void cleanupView(RecycledViewPool pool, int viewType) {
        while (true) {
            VerticalViewHolder viewHolder = (VerticalViewHolder) pool.getRecycledView(viewType);
            if (viewHolder != null) {
                viewHolder.clearAnimation();
                viewHolder.layout.removeAllViews();
            } else {
                return;
            }
        }
    }

    public static void setFontSize(Model model, boolean allowTwoLineTitles) {
        model.fontSizeCheckDone = true;
        model.subTitle_2Lines = false;
        if (model.title == null) {
            model.fontInfo = (FontInfo) tit_subt_map.get(FontSize.FONT_SIZE_26);
            model.fontSize = FontSize.FONT_SIZE_26;
        } else if (model.subTitle2 != null || model.subTitle == null) {
            setTitleSize(model, allowTwoLineTitles);
        } else {
            setTitleSubTitleSize(model, allowTwoLineTitles);
        }
    }

    private static void setTitleSubTitleSize(Model model, boolean allowTwoLineTitles) {
        TextPaint paint = fontSizeTextView.getPaint();
        fontSizeTextView.setTextSize(vlistTitle_18);
        StaticLayout layout = new StaticLayout(model.subTitle, paint, vlistTitleTextW, Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
        int subTitleLines = layout.getLineCount();
        int subTitleTotal = subTitleLines;
        if (!TextUtils.isEmpty(model.subTitle2)) {
            subTitleTotal++;
        }
        if (subTitleTotal > 1) {
            allowTwoLineTitles = false;
        }
        setTitleSize(model, allowTwoLineTitles);
        if (subTitleLines != 1) {
            model.subTitle_2Lines = true;
            FontSize newFontSize = null;
            if (layout.getLineCount() != 2) {
                switch (model.fontSize) {
                    case FONT_SIZE_26:
                        newFontSize = FontSize.FONT_SIZE_26_16;
                        break;
                    case FONT_SIZE_22:
                        newFontSize = FontSize.FONT_SIZE_22_16;
                        break;
                    case FONT_SIZE_22_2:
                        newFontSize = FontSize.FONT_SIZE_22_2_16;
                        break;
                    case FONT_SIZE_18:
                        newFontSize = FontSize.FONT_SIZE_18_16;
                        break;
                    case FONT_SIZE_18_2:
                        newFontSize = FontSize.FONT_SIZE_18_2_16;
                        break;
                    case FONT_SIZE_16:
                        newFontSize = FontSize.FONT_SIZE_16_16;
                        break;
                    case FONT_SIZE_16_2:
                        newFontSize = FontSize.FONT_SIZE_16_2_16;
                        break;
                }
            }
            switch (model.fontSize) {
                case FONT_SIZE_26:
                    newFontSize = FontSize.FONT_SIZE_26_18;
                    break;
                case FONT_SIZE_22:
                    newFontSize = FontSize.FONT_SIZE_22_18;
                    break;
                case FONT_SIZE_22_2:
                    newFontSize = FontSize.FONT_SIZE_22_2_16;
                    break;
                case FONT_SIZE_18:
                    newFontSize = FontSize.FONT_SIZE_18_16;
                    break;
                case FONT_SIZE_18_2:
                    newFontSize = FontSize.FONT_SIZE_18_2_16;
                    break;
                case FONT_SIZE_16:
                    newFontSize = FontSize.FONT_SIZE_16_16;
                    break;
                case FONT_SIZE_16_2:
                    newFontSize = FontSize.FONT_SIZE_16_2_16;
                    break;
            }
            if (newFontSize != null) {
                model.fontInfo = (FontInfo) tit_subt_map_2_lines.get(newFontSize);
                model.fontSize = newFontSize;
                sLogger.d("Adjusting size for " + model.title + " to " + newFontSize);
            }
        }
    }

    private static void setTitleSize(Model model, boolean allowTwoLines) {
        fontSizeTextView.setText(model.title);
        int[] index = new int[2];
        ViewUtil.autosize(fontSizeTextView, allowTwoLines ? TWO_LINE_MAX_LINES : SINGLE_LINE_MAX_LINES, vlistTitleTextW, TITLE_SIZES, index);
        FontSize fontSize = FONT_SIZES[(index[0] * 2) + (index[1] - 1)];
        sLogger.d("Setting size for " + model.title + " to " + fontSize);
        if (model.subTitle == null || model.subTitle2 == null) {
            model.fontInfo = (FontInfo) tit_subt_map.get(fontSize);
        } else {
            model.fontInfo = (FontInfo) tit_subt_subt2_map.get(fontSize);
        }
        model.fontSize = fontSize;
    }

    private boolean isCloseMenuVisible() {
        if (this.containerCallback != null) {
            return this.containerCallback.isCloseMenuVisible();
        }
        return false;
    }

    private void calculateScrollRange() {
        VerticalViewHolder viewHolder = (VerticalViewHolder) this.recyclerView.findViewHolderForAdapterPosition(this.scrollItemIndex);
        if (!(viewHolder instanceof ScrollableViewHolder) || viewHolder.layout.getChildCount() <= 0) {
            sLogger.v("calculateScrollRange:" + this.scrollItemIndex + " vh not found");
            return;
        }
        this.scrollItemStartY = VerticalViewHolder.listItemHeight * this.scrollItemIndex;
        this.scrollItemHeight = viewHolder.layout.getChildAt(0).getMeasuredHeight();
        this.scrollItemEndY = (this.scrollItemStartY + this.scrollItemHeight) - (VerticalViewHolder.listItemHeight * 3);
        if (this.reCalcScrollPos) {
            sLogger.v("calculateScrollPos: recalc scrollItemHeight=" + this.scrollItemHeight + " scrollItemStartY=" + this.scrollItemStartY + " scrollItemEndY=" + this.scrollItemEndY);
            calculateScrollPos(this.currentMiddlePosition);
        }
    }

    private int getPositionFromScrollY(int scrollPos) {
        if (!this.hasScrollableElement) {
            return (scrollPos / VerticalViewHolder.listItemHeight) + 1;
        }
        if (this.scrollItemStartY == -1) {
            calculateScrollRange();
        }
        if (scrollPos < this.scrollItemStartY) {
            return (scrollPos / VerticalViewHolder.listItemHeight) + 1;
        }
        if (scrollPos > this.scrollItemEndY) {
            return ((((scrollPos - this.scrollItemEndY) - (VerticalViewHolder.listItemHeight * 2)) / VerticalViewHolder.listItemHeight) + 1) + this.scrollItemIndex;
        }
        return this.scrollItemIndex;
    }

    private void calculateScrollPos(int pos) {
        if (this.scrollItemHeight != -1 && pos == this.scrollItemIndex + 1) {
            this.reCalcScrollPos = false;
            this.actualScrollY = this.scrollItemIndex * VerticalViewHolder.listItemHeight;
            this.actualScrollY += VerticalViewHolder.listItemHeight * 2;
            sLogger.v("onScrollStateChanged(( calculateScrollPos:" + this.actualScrollY + " pos=" + pos + " scrollIndex=" + this.scrollItemIndex);
        }
    }
}
