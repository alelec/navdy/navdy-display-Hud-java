package com.navdy.hud.app.ui.component.mainmenu;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Pair;
import android.view.View;
import com.amazonaws.services.s3.internal.Constants;
import com.makeramen.RoundedTransformationBuilder;
import com.navdy.hud.app.ExtensionsKt;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.hud.app.manager.MusicManager;
import com.navdy.hud.app.manager.MusicManager.MediaControl;
import com.navdy.hud.app.manager.MusicManager.MusicUpdateListener;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.screen.BaseScreen;
import com.navdy.hud.app.storage.cache.MessageCache;
import com.navdy.hud.app.ui.component.image.IconColorImageView;
import com.navdy.hud.app.ui.component.image.IconColorImageView.IconShape;
import com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu;
import com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel;
import com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState;
import com.navdy.hud.app.ui.component.vlist.VerticalModelCache;
import com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.LoadingViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State;
import com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent;
import com.navdy.hud.app.ui.framework.IScreenAnimationListener;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.util.MusicArtworkCache;
import com.navdy.hud.app.util.MusicArtworkCache.Callback;
import com.navdy.hud.app.util.picasso.PicassoUtil;
import com.navdy.service.library.events.DeviceInfo.Platform;
import com.navdy.service.library.events.MessageStore;
import com.navdy.service.library.events.audio.MusicArtworkRequest;
import com.navdy.service.library.events.audio.MusicArtworkResponse;
import com.navdy.service.library.events.audio.MusicCapability;
import com.navdy.service.library.events.audio.MusicCapability.MusicAuthorizationStatus;
import com.navdy.service.library.events.audio.MusicCharacterMap;
import com.navdy.service.library.events.audio.MusicCollectionInfo;
import com.navdy.service.library.events.audio.MusicCollectionInfo.Builder;
import com.navdy.service.library.events.audio.MusicCollectionRequest;
import com.navdy.service.library.events.audio.MusicCollectionResponse;
import com.navdy.service.library.events.audio.MusicCollectionSource;
import com.navdy.service.library.events.audio.MusicCollectionType;
import com.navdy.service.library.events.audio.MusicEvent;
import com.navdy.service.library.events.audio.MusicEvent.Action;
import com.navdy.service.library.events.audio.MusicShuffleMode;
import com.navdy.service.library.events.audio.MusicTrackInfo;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.ScalingUtilities;
import com.navdy.service.library.util.ScalingUtilities.ScalingLogic;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.squareup.wire.Message;
import com.squareup.wire.Wire;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import javax.inject.Inject;
import mortar.Mortar;
import okio.ByteString;

public class MusicMenu2 implements IMenu {
    private static Map<MusicCollectionType, String> analyticsTypeStringMap = new HashMap();
    private static int androidBgColor = resources.getColor(R.color.music_android_local);
    private static int appleMusicBgColor = resources.getColor(R.color.music_apple_music);
    private static int applePodcastsBgColor = resources.getColor(R.color.music_apple_podcasts);
    private static final Object artworkRequestLock = new Object();
    private static Stack<Pair<MusicArtworkRequest, MusicCollectionInfo>> artworkRequestQueue = new Stack();
    private static int artworkSize = resources.getDimensionPixelSize(R.dimen.vmenu_selected_image);
    private static Model back = IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, backColor, bgColorUnselected, backColor, resources.getString(R.string.back), null);
    private static int backColor = resources.getColor(R.color.mm_back);
    private static int bgColorUnselected = resources.getColor(R.color.grey_4a);
    private static BusDelegate busDelegate;
    private static MusicCollectionInfo infoForCurrentArtworkRequest = null;
    private static Map<String, MusicMenuData> lastPlayedMusicMenuData = new HashMap();
    private static final Logger logger = new Logger(MusicMenu2.class);
    private static Map<MusicCollectionSource, Integer> menuSourceIconMap = new HashMap();
    private static Map<MusicCollectionSource, String> menuSourceStringMap = new HashMap();
    private static Map<MusicCollectionType, Integer> menuTypeIconMap = new HashMap();
    private static Map<MusicCollectionType, String> menuTypeStringMap = new HashMap();
    private static int mmMusicColor = resources.getColor(R.color.mm_music);
    private static Resources resources = HudApplication.getAppContext().getResources();
    private static String shuffle = resources.getString(R.string.shuffle);
    private static String shuffleOff = resources.getString(R.string.shuffle_off);
    private static String shuffleOn = resources.getString(R.string.shuffle_on);
    private static String shufflePlay = resources.getString(R.string.shuffle_play);
    private static int sourceBgColor = resources.getColor(17170443);
    private static int transparentColor = resources.getColor(17170445);
    private boolean anyPlayersPermitted = true;
    private long artWorkRequestTime = 0;
    private int backSelection;
    private int backSelectionId;
    private int bgColorSelected;
    private Bus bus;
    private List<Model> cachedList = null;
    private MusicCollectionRequest currentMusicCollectionRequest;
    private long currentMusicCollectionRequestTime;
    private VerticalFastScrollIndex fastScrollIndex;
    private Handler handler = new Handler(Looper.getMainLooper());
    private int highlightedItem = 1;
    private boolean initialized = false;
    private boolean isRequestingMusicCollection = false;
    private boolean isShuffling = false;
    private final IndexOffset loadingOffset = new IndexOffset();
    private MusicMenuData menuData;
    private AnimationDrawable musicAnimation;
    @Inject
    MusicArtworkCache musicArtworkCache;
    @Inject
    MessageCache<MusicCollectionResponse> musicCollectionResponseMessageCache;
    private boolean musicCollectionSyncComplete = false;
    private final MusicManager musicManager;
    private int nextRequestLimit = -1;
    private int nextRequestOffset = -1;
    private int nowPlayingTrackPosition;
    private IMenu parentMenu;
    private boolean partialRefresh;
    private String path = null;
    private Presenter presenter;
    private int requestMusicOffset = -1;
    private Map<String, Integer> requestedArtworkModelPositions = new HashMap();
    private List<Model> returnToCacheList = null;
    private Map<String, Integer> trackIdToPositionMap = null;
    private VerticalMenuComponent vmenuComponent;

    private static class BusDelegate implements MusicUpdateListener {
        Presenter presenter;

        BusDelegate(Presenter presenter) {
            this.presenter = presenter;
        }

        @Subscribe
        public void onMusicCollectionResponse(MusicCollectionResponse musicCollectionResponse) {
            IMenu menu = this.presenter.getCurrentMenu();
            if (menu != null && menu.getType() == Menu.MUSIC) {
                ((MusicMenu2) menu).onMusicCollectionResponse(musicCollectionResponse);
            }
        }

        @Subscribe
        public void onMusicArtworkResponse(MusicArtworkResponse musicArtworkResponse) {
            IMenu menu = this.presenter.getCurrentMenu();
            if (menu != null && menu.getType() == Menu.MUSIC) {
                ((MusicMenu2) menu).onMusicArtworkResponse(musicArtworkResponse);
            }
        }

        public void onAlbumArtUpdate(@Nullable ByteString artwork, boolean animate) {
        }

        public void onTrackUpdated(MusicTrackInfo trackInfo, Set<MediaControl> set, boolean willOpenNotification) {
            IMenu menu = this.presenter.getCurrentMenu();
            if (menu != null && menu.getType() == Menu.MUSIC) {
                ((MusicMenu2) menu).onTrackUpdated(trackInfo);
            }
        }
    }

    private static class IndexOffset {
        int limit;
        int offset;

        private IndexOffset() {
        }

        /* synthetic */ IndexOffset(AnonymousClass1 x0) {
            this();
        }

        void clear() {
            this.offset = 0;
            this.limit = -1;
        }
    }

    private class MusicMenuData {
        MusicCollectionInfo musicCollectionInfo;
        List<MusicCollectionInfo> musicCollections = null;
        List<MusicCharacterMap> musicIndex = null;
        List<MusicTrackInfo> musicTracks = null;

        MusicMenuData() {
        }

        MusicMenuData(MusicCollectionInfo musicCollectionInfo, List<MusicCollectionInfo> musicCollections) {
            this.musicCollectionInfo = musicCollectionInfo;
            this.musicCollections = musicCollections;
        }
    }

    static {
        menuSourceStringMap.put(MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL, resources.getString(R.string.local_music));
        menuSourceStringMap.put(MusicCollectionSource.COLLECTION_SOURCE_IOS_MEDIA_PLAYER, resources.getString(R.string.music_library));
        menuSourceIconMap.put(MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL, Integer.valueOf(R.drawable.icon_google_pm));
        menuSourceIconMap.put(MusicCollectionSource.COLLECTION_SOURCE_IOS_MEDIA_PLAYER, Integer.valueOf(R.drawable.icon_apple_music));
        menuTypeStringMap.put(MusicCollectionType.COLLECTION_TYPE_ALBUMS, resources.getString(R.string.albums));
        menuTypeStringMap.put(MusicCollectionType.COLLECTION_TYPE_ARTISTS, resources.getString(R.string.artists));
        menuTypeStringMap.put(MusicCollectionType.COLLECTION_TYPE_PLAYLISTS, resources.getString(R.string.playlists));
        menuTypeStringMap.put(MusicCollectionType.COLLECTION_TYPE_PODCASTS, resources.getString(R.string.podcasts));
        menuTypeIconMap.put(MusicCollectionType.COLLECTION_TYPE_ALBUMS, Integer.valueOf(R.drawable.icon_album));
        menuTypeIconMap.put(MusicCollectionType.COLLECTION_TYPE_ARTISTS, Integer.valueOf(R.drawable.icon_artist));
        menuTypeIconMap.put(MusicCollectionType.COLLECTION_TYPE_PLAYLISTS, Integer.valueOf(R.drawable.icon_playlist));
        menuTypeIconMap.put(MusicCollectionType.COLLECTION_TYPE_PODCASTS, Integer.valueOf(R.drawable.icon_podcasts));
        analyticsTypeStringMap.put(MusicCollectionType.COLLECTION_TYPE_ALBUMS, "Album");
        analyticsTypeStringMap.put(MusicCollectionType.COLLECTION_TYPE_ARTISTS, "Artist");
        analyticsTypeStringMap.put(MusicCollectionType.COLLECTION_TYPE_PLAYLISTS, "Playlist");
        analyticsTypeStringMap.put(MusicCollectionType.COLLECTION_TYPE_PODCASTS, "Podcast");
    }

    private void onTrackUpdated(MusicTrackInfo musicTrackInfo) {
        if (isThisQueuePlaying()) {
            String trackId = musicTrackInfo.trackId;
            if (!(this.trackIdToPositionMap == null || trackId == null || !this.trackIdToPositionMap.containsKey(trackId))) {
                Integer pos = (Integer) this.trackIdToPositionMap.get(trackId);
                if (this.nowPlayingTrackPosition > 0 && this.nowPlayingTrackPosition != pos.intValue()) {
                    stopAudioAnimation();
                    this.presenter.refreshDataforPos(this.nowPlayingTrackPosition);
                }
                this.presenter.refreshDataforPos(pos.intValue());
                this.nowPlayingTrackPosition = pos.intValue();
            }
            boolean shuffle = this.musicManager.isShuffling();
            if (this.isShuffling != shuffle) {
                this.isShuffling = shuffle;
                this.vmenuComponent.verticalList.refreshData(1, getShuffleModel());
                return;
            }
            return;
        }
        logger.w("This queue isn't playing");
    }

    MusicMenu2(Bus bus, VerticalMenuComponent vmenuComponent, Presenter presenter, IMenu parentMenu, MusicMenuData menuData) {
        this.bus = bus;
        this.vmenuComponent = vmenuComponent;
        this.presenter = presenter;
        this.parentMenu = parentMenu;
        if (menuData != null) {
            this.menuData = menuData;
        } else {
            this.menuData = new MusicMenuData();
        }
        Mortar.inject(HudApplication.getAppContext(), this);
        logger.d("MusicMenu " + this.menuData.musicCollectionInfo);
        this.musicManager = RemoteDeviceManager.getInstance().getMusicManager();
    }

    public List<Model> getItems() {
        logger.v("getItems " + getPath());
        setupMenu();
        List<Model> list = new ArrayList();
        this.returnToCacheList = new ArrayList();
        Model emptyBack;
        if (!this.musicManager.hasMusicCapabilities()) {
            logger.e("Navigated to Music menu with no music capabilities");
            list.add(back);
        } else if (!this.anyPlayersPermitted) {
            String title;
            String subtitle;
            if (RemoteDeviceManager.getInstance().getRemoteDevicePlatform() == Platform.PLATFORM_iOS) {
                title = resources.getString(R.string.music_enable_permissions_title_ios);
                subtitle = resources.getString(R.string.music_enable_permissions_subtitle_ios);
            } else {
                title = resources.getString(R.string.music_enable_permissions_title_android);
                subtitle = resources.getString(R.string.music_enable_permissions_subtitle_android);
            }
            emptyBack = IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, backColor, bgColorUnselected, backColor, title, subtitle);
            list.add(emptyBack);
            this.returnToCacheList.add(emptyBack);
        } else if (this.menuData.musicCollectionInfo.size == null || this.menuData.musicCollectionInfo.size.intValue() != 0) {
            list.add(back);
            int i;
            Model model;
            Model contentPlaceHolder;
            if (this.menuData.musicCollectionInfo.collectionSource == null) {
                if (busDelegate == null && this.parentMenu.getType() == Menu.MAIN) {
                    logger.v("bus-register getItems");
                    busDelegate = new BusDelegate(this.presenter);
                    this.bus.register(busDelegate);
                    this.presenter.setScrollIdleEvents(true);
                }
                for (i = 0; i < this.menuData.musicCollections.size(); i++) {
                    MusicCollectionInfo collection = (MusicCollectionInfo) this.menuData.musicCollections.get(i);
                    model = IconBkColorViewHolder.buildModel(i, ((Integer) menuSourceIconMap.get(collection.collectionSource)).intValue(), sourceBgColor, bgColorUnselected, sourceBgColor, (String) menuSourceStringMap.get(collection.collectionSource), null);
                    model.state = collection;
                    list.add(model);
                    this.returnToCacheList.add(model);
                    List<MusicCollectionType> collectionTypes = this.musicManager.getCollectionTypesForSource(collection.collectionSource);
                    if (collection.collectionSource == MusicCollectionSource.COLLECTION_SOURCE_IOS_MEDIA_PLAYER) {
                        if (collectionTypes.contains(MusicCollectionType.COLLECTION_TYPE_PODCASTS)) {
                            Model podcastsModel = IconBkColorViewHolder.buildModel(R.id.apple_podcasts, ((Integer) menuTypeIconMap.get(MusicCollectionType.COLLECTION_TYPE_PODCASTS)).intValue(), applePodcastsBgColor, bgColorUnselected, applePodcastsBgColor, resources.getString(R.string.apple_podcasts), null);
                            podcastsModel.state = MusicCollectionType.COLLECTION_TYPE_PODCASTS;
                            list.add(podcastsModel);
                            this.returnToCacheList.add(podcastsModel);
                        }
                    }
                }
            } else if (this.menuData.musicCollectionInfo.collectionType == null) {
                for (i = 0; i < this.menuData.musicCollections.size(); i++) {
                    MusicCollectionType collectionType = ((MusicCollectionInfo) this.menuData.musicCollections.get(i)).collectionType;
                    if (this.menuData.musicCollectionInfo.collectionSource != MusicCollectionSource.COLLECTION_SOURCE_IOS_MEDIA_PLAYER || collectionType != MusicCollectionType.COLLECTION_TYPE_PODCASTS) {
                        model = IconBkColorViewHolder.buildModel(i, ((Integer) menuTypeIconMap.get(collectionType)).intValue(), this.bgColorSelected, bgColorUnselected, this.bgColorSelected, (String) menuTypeStringMap.get(collectionType), null);
                        model.state = collectionType;
                        list.add(model);
                        this.returnToCacheList.add(model);
                    }
                }
            } else if (this.menuData.musicCollections == null && this.menuData.musicTracks == null) {
                list.add(LoadingViewHolder.buildModel());
                requestMusicCollection(0, -1, isIndexSupported(this.menuData.musicCollectionInfo));
            } else if (this.menuData.musicCollections != null && this.menuData.musicCollections.size() > 0) {
                logger.i("Loading music collections for type " + this.backSelection);
                if (this.menuData.musicIndex != null && this.fastScrollIndex == null) {
                    this.fastScrollIndex = checkCharacterMap(this.menuData.musicIndex, 0, 0, false);
                }
                Integer collectionIcon = null;
                if (!(this.menuData.musicCollectionInfo == null || this.menuData.musicCollectionInfo.collectionType == null)) {
                    collectionIcon = (Integer) menuTypeIconMap.get(this.menuData.musicCollectionInfo.collectionType);
                }
                if (collectionIcon == null) {
                    collectionIcon = Integer.valueOf(0);
                }
                if (this.menuData.musicCollectionInfo.collectionType == MusicCollectionType.COLLECTION_TYPE_ARTISTS && !TextUtils.isEmpty(this.menuData.musicCollectionInfo.collectionId) && ((Boolean) Wire.get(this.menuData.musicCollectionInfo.canShuffle, Boolean.valueOf(false))).booleanValue()) {
                    list.add(getShuffleModel());
                    this.highlightedItem = 2;
                }
                for (i = 0; i < this.menuData.musicCollections.size(); i++) {
                    MusicCollectionInfo collectionInfo = (MusicCollectionInfo) this.menuData.musicCollections.get(i);
                    if (collectionInfo == null) {
                        contentPlaceHolder = ContentLoadingViewHolder.buildModel(collectionIcon.intValue(), this.bgColorSelected, bgColorUnselected);
                        list.add(contentPlaceHolder);
                        this.returnToCacheList.add(contentPlaceHolder);
                    } else {
                        model = buildModelfromCollectionInfo(i, collectionInfo);
                        list.add(model);
                        this.returnToCacheList.add(model);
                    }
                }
            } else if (this.menuData.musicTracks == null || this.menuData.musicTracks.size() <= 0) {
                logger.e("No collections or tracks in this collection...");
            } else {
                int icon;
                if (this.trackIdToPositionMap == null) {
                    this.trackIdToPositionMap = new HashMap();
                }
                if (this.menuData.musicCollectionInfo.collectionType == MusicCollectionType.COLLECTION_TYPE_PODCASTS) {
                    icon = R.drawable.icon_play_music;
                } else {
                    icon = R.drawable.icon_song;
                    if (this.menuData.musicTracks.size() > 1) {
                        list.add(getShuffleModel());
                        if (this.menuData.musicCollectionInfo.collectionType != MusicCollectionType.COLLECTION_TYPE_PLAYLISTS) {
                            this.highlightedItem = 2;
                        }
                    }
                }
                for (i = 0; i < this.menuData.musicTracks.size(); i++) {
                    MusicTrackInfo musicTrackInfo = (MusicTrackInfo) this.menuData.musicTracks.get(i);
                    if (musicTrackInfo == null) {
                        contentPlaceHolder = ContentLoadingViewHolder.buildModel(icon, this.bgColorSelected, bgColorUnselected);
                        list.add(contentPlaceHolder);
                        this.returnToCacheList.add(contentPlaceHolder);
                    } else {
                        model = IconBkColorViewHolder.buildModel(i, icon, this.bgColorSelected, bgColorUnselected, this.bgColorSelected, musicTrackInfo.name, musicTrackInfo.author);
                        model.state = musicTrackInfo;
                        list.add(model);
                        this.returnToCacheList.add(model);
                        this.trackIdToPositionMap.put(musicTrackInfo.trackId, Integer.valueOf(list.size() - 1));
                    }
                }
                if (this.menuData.musicTracks.size() < this.menuData.musicCollectionInfo.size.intValue()) {
                    list.add(LoadingViewHolder.buildModel());
                }
            }
            this.cachedList = list;
        } else {
            emptyBack = IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, backColor, bgColorUnselected, backColor, resources.getString(R.string.back), resources.getString(R.string.empty_queue, new Object[]{menuTypeStringMap.get(this.menuData.musicCollectionInfo.collectionType)}));
            list.add(emptyBack);
            this.returnToCacheList.add(emptyBack);
        }
        return list;
    }

    private Model getShuffleModel() {
        String shuffleTitle;
        String shuffleSubtitle = null;
        if (isThisQueuePlaying()) {
            shuffleTitle = shuffle;
            if (this.musicManager.isShuffling()) {
                shuffleSubtitle = shuffleOn;
                this.isShuffling = true;
            } else {
                shuffleSubtitle = shuffleOff;
            }
        } else {
            shuffleTitle = shufflePlay;
        }
        return IconBkColorViewHolder.buildModel(R.id.music_browser_shuffle, R.drawable.icon_shuffle, this.bgColorSelected, bgColorUnselected, this.bgColorSelected, shuffleTitle, shuffleSubtitle);
    }

    private void setupMenu() {
        if (busDelegate == null) {
            logger.v("bus-register ctor");
            busDelegate = new BusDelegate(this.presenter);
            this.bus.register(busDelegate);
            this.presenter.setScrollIdleEvents(true);
            this.musicManager.addMusicUpdateListener(busDelegate);
        }
        if (!this.initialized) {
            this.initialized = true;
            if (this.musicManager.hasMusicCapabilities()) {
                if (this.menuData.musicCollectionInfo == null) {
                    List<MusicCapability> capabilities = this.musicManager.getMusicCapabilities().capabilities;
                    this.anyPlayersPermitted = false;
                    List<MusicCollectionSource> musicCollectionSources = new ArrayList(capabilities.size());
                    for (MusicCapability musicCapability : capabilities) {
                        if (musicCapability.authorizationStatus == MusicAuthorizationStatus.MUSIC_AUTHORIZATION_AUTHORIZED) {
                            this.anyPlayersPermitted = true;
                            musicCollectionSources.add(musicCapability.collectionSource);
                        }
                    }
                    if (capabilities.size() == 1 && ((MusicCapability) capabilities.get(0)).collectionSource == MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL) {
                        this.menuData.musicCollectionInfo = new Builder().collectionSource(MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL).build();
                        List<MusicCollectionType> collectionTypes = this.musicManager.getCollectionTypesForSource(MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL);
                        this.menuData.musicCollections = new ArrayList(collectionTypes.size());
                        for (MusicCollectionType collectionType : collectionTypes) {
                            this.menuData.musicCollections.add(new Builder().collectionSource(MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL).collectionType(collectionType).build());
                        }
                    } else {
                        this.menuData.musicCollectionInfo = new Builder().build();
                        this.menuData.musicCollections = new ArrayList(musicCollectionSources.size());
                        for (MusicCollectionSource collectionSource : musicCollectionSources) {
                            this.menuData.musicCollections.add(new Builder().collectionSource(collectionSource).build());
                        }
                    }
                }
                setupIconBgColor();
                return;
            }
            logger.e("Navigated to Music menu with no music capabilities");
        }
    }

    private void requestMusicCollection(int offset, int limit, boolean askForIndex) {
        logger.d("requestMusicCollection Offset : " + offset + ", Limit : " + limit + ", AFI : " + askForIndex);
        if (this.musicCollectionSyncComplete) {
            logger.i("sync is complete");
        } else if (this.isRequestingMusicCollection) {
            logger.i("Request already in process next=" + offset + " limit:" + limit);
            this.nextRequestOffset = offset;
            this.nextRequestLimit = limit;
        } else {
            this.nextRequestOffset = -1;
            this.nextRequestLimit = -1;
            this.isRequestingMusicCollection = true;
            this.requestMusicOffset = offset;
            if (limit == -1) {
                limit = 25;
            }
            logger.i("request music =" + offset + " limit:" + limit);
            if (this.menuData.musicCollections == null && this.menuData.musicTracks == null) {
                limit = 50;
            }
            MusicCollectionRequest.Builder builder = new MusicCollectionRequest.Builder().collectionSource(this.menuData.musicCollectionInfo.collectionSource).collectionType(this.menuData.musicCollectionInfo.collectionType).collectionId(this.menuData.musicCollectionInfo.collectionId).offset(Integer.valueOf(offset)).limit(Integer.valueOf(limit));
            if (this.menuData.musicCollectionInfo.collectionType == MusicCollectionType.COLLECTION_TYPE_ARTISTS) {
                builder.groupBy(MusicCollectionType.COLLECTION_TYPE_ALBUMS);
            }
            if (askForIndex) {
                builder.includeCharacterMap(Boolean.valueOf(true));
            }
            final MusicCollectionRequest musicCollectionRequest = builder.build();
            this.currentMusicCollectionRequest = musicCollectionRequest;
            this.currentMusicCollectionRequestTime = SystemClock.elapsedRealtime();
            logger.i("got-asked offset=" + offset + " limit=" + limit);
            logger.d("[Timing] MusicCollectionRequest " + musicCollectionRequest);
            if (this.musicManager.isMusicLibraryCachingEnabled()) {
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        String cacheKeyForRequest = ExtensionsKt.cacheKey(musicCollectionRequest);
                        MusicCollectionResponse response = (MusicCollectionResponse) MusicMenu2.this.musicCollectionResponseMessageCache.get(cacheKeyForRequest);
                        if (response != null) {
                            MusicMenu2.logger.d("Cache hit for MusicCollectionRequest " + musicCollectionRequest + ", Key :" + cacheKeyForRequest);
                            MusicMenu2.this.bus.post(response);
                            return;
                        }
                        MusicMenu2.logger.d("Cache miss for MusicCollectionRequest " + musicCollectionRequest + ", Key :" + cacheKeyForRequest);
                        MusicMenu2.this.bus.post(new RemoteEvent(musicCollectionRequest));
                    }
                }, 1);
            } else {
                this.bus.post(new RemoteEvent(musicCollectionRequest));
            }
        }
    }

    private void postOrQueueArtworkRequest(MusicArtworkRequest artworkRequest, MusicCollectionInfo collectionInfo) {
        synchronized (artworkRequestLock) {
            if (artworkRequestQueue.isEmpty() && infoForCurrentArtworkRequest == null) {
                logger.d("REQUESTQUEUE postOrQueueArtworkRequest posting: " + artworkRequest + " title:" + collectionInfo.name);
                infoForCurrentArtworkRequest = collectionInfo;
                this.artWorkRequestTime = SystemClock.elapsedRealtime();
                logger.d("[Timing] Making MusicArtWorkRequest " + artworkRequest);
                this.bus.post(new RemoteEvent(artworkRequest));
            } else {
                logger.d("REQUESTQUEUE postOrQueueArtworkRequest queueing: " + artworkRequest + " title:" + collectionInfo.name);
                artworkRequestQueue.push(new Pair(artworkRequest, collectionInfo));
            }
        }
    }

    private void purgeArtworkQueue() {
        synchronized (artworkRequestLock) {
            int size = artworkRequestQueue.size();
            artworkRequestQueue.clear();
            logger.d("REQUESTQUEUE purged: " + size);
        }
    }

    public int getInitialSelection() {
        logger.i("getInitialSelection " + this.highlightedItem);
        return this.highlightedItem;
    }

    public VerticalFastScrollIndex getScrollIndex() {
        return this.fastScrollIndex;
    }

    public void setBackSelectionPos(int n) {
        this.backSelection = n;
    }

    public void setBackSelectionId(int id) {
        logger.i("setBackSelectionId: " + id);
        this.backSelectionId = id;
    }

    public void setSelectedIcon() {
        logger.i("setSelectedIcon");
        setupMenu();
        if (this.parentMenu.getType() == Menu.MAIN) {
            this.vmenuComponent.setSelectedIconColorImage(R.drawable.icon_mm_music_2, mmMusicColor, null, 1.0f);
            this.vmenuComponent.selectedText.setText(R.string.music);
        } else if (this.menuData.musicCollectionInfo.collectionType == null) {
            this.vmenuComponent.setSelectedIconColorImage(((Integer) menuSourceIconMap.get(this.menuData.musicCollectionInfo.collectionSource)).intValue(), sourceBgColor, null, 1.0f);
            this.vmenuComponent.selectedText.setText((CharSequence) menuSourceStringMap.get(this.menuData.musicCollectionInfo.collectionSource));
        } else if (TextUtils.isEmpty(this.menuData.musicCollectionInfo.collectionId)) {
            this.vmenuComponent.setSelectedIconColorImage(((Integer) menuTypeIconMap.get(this.menuData.musicCollectionInfo.collectionType)).intValue(), this.bgColorSelected, null, 1.0f);
            this.vmenuComponent.selectedText.setText((CharSequence) menuTypeStringMap.get(this.menuData.musicCollectionInfo.collectionType));
        } else {
            Bitmap artwork = PicassoUtil.getBitmapfromCache(collectionIdString(this.menuData.musicCollectionInfo));
            if (artwork != null) {
                this.vmenuComponent.setSelectedIconImage(artwork);
            } else {
                this.vmenuComponent.setSelectedIconColorImage(((Integer) menuTypeIconMap.get(this.menuData.musicCollectionInfo.collectionType)).intValue(), this.bgColorSelected, null, 1.0f, this.menuData.musicCollectionInfo.collectionType == MusicCollectionType.COLLECTION_TYPE_ARTISTS ? IconShape.CIRCLE : IconShape.SQUARE);
                this.musicArtworkCache.getArtwork(this.menuData.musicCollectionInfo, new Callback() {
                    public void onHit(@NonNull byte[] data) {
                        MusicMenu2.this.handleArtwork(data, null, MusicMenu2.this.collectionIdString(MusicMenu2.this.menuData.musicCollectionInfo), false);
                    }

                    public void onMiss() {
                        MusicMenu2.this.postOrQueueArtworkRequest(new MusicArtworkRequest.Builder().collectionSource(MusicMenu2.this.menuData.musicCollectionInfo.collectionSource).collectionType(MusicMenu2.this.menuData.musicCollectionInfo.collectionType).collectionId(MusicMenu2.this.menuData.musicCollectionInfo.collectionId).size(Integer.valueOf(200)).build(), MusicMenu2.this.menuData.musicCollectionInfo);
                    }
                });
            }
            this.vmenuComponent.selectedText.setText(this.menuData.musicCollectionInfo.name);
        }
    }

    public boolean selectItem(ItemSelectionState selection) {
        logger.i("onItemClicked " + selection.id + ", " + selection.pos);
        MusicCollectionInfo collectionInfo;
        if (selection.id == R.id.menu_back) {
            this.presenter.loadMenu(this.parentMenu, MenuLevel.BACK_TO_PARENT, this.backSelection, this.backSelectionId);
            this.backSelection = 0;
            this.backSelectionId = 0;
        } else if (selection.id == R.id.music_browser_shuffle) {
            if (isThisQueuePlaying()) {
                MusicShuffleMode shuffleMode;
                if (this.musicManager.isShuffling()) {
                    shuffleMode = MusicShuffleMode.MUSIC_SHUFFLE_MODE_OFF;
                } else {
                    shuffleMode = MusicShuffleMode.MUSIC_SHUFFLE_MODE_SONGS;
                }
                this.bus.post(new RemoteEvent(new MusicEvent.Builder().action(Action.MUSIC_ACTION_MODE_CHANGE).shuffleMode(shuffleMode).build()));
                this.handler.post(new Runnable() {
                    public void run() {
                        MusicMenu2.this.presenter.reset();
                        MusicMenu2.this.vmenuComponent.verticalList.unlock();
                    }
                });
            } else {
                collectionInfo = this.menuData.musicCollectionInfo;
                this.bus.post(new RemoteEvent(new MusicEvent.Builder().collectionSource(collectionInfo.collectionSource).collectionType(collectionInfo.collectionType).collectionId(collectionInfo.collectionId).action(Action.MUSIC_ACTION_PLAY).shuffleMode(MusicShuffleMode.MUSIC_SHUFFLE_MODE_SONGS).build()));
                showMusicPlayer();
            }
        } else if (this.menuData.musicTracks == null || this.menuData.musicTracks.size() <= 0) {
            this.presenter.loadMenu(getMusicPlayerMenu(selection.id, selection.pos), MenuLevel.SUB_LEVEL, selection.pos, 0);
        } else {
            Model model = null;
            if (this.cachedList != null && selection.pos >= 0 && selection.pos < this.cachedList.size()) {
                model = (Model) this.cachedList.get(selection.pos);
            }
            logger.v("onItemClicked track:" + (model != null ? model.title : Constants.NULL_VERSION_ID));
            collectionInfo = this.menuData.musicCollectionInfo;
            this.bus.post(new RemoteEvent(new MusicEvent.Builder().collectionSource(collectionInfo.collectionSource).collectionType(collectionInfo.collectionType).collectionId(collectionInfo.collectionId).index(Integer.valueOf(selection.id)).action(Action.MUSIC_ACTION_PLAY).shuffleMode(MusicShuffleMode.MUSIC_SHUFFLE_MODE_OFF).build()));
            showMusicPlayer();
            AnalyticsSupport.recordMusicBrowsePlayAction((String) analyticsTypeStringMap.get(collectionInfo.collectionType));
        }
        return true;
    }

    private void showMusicPlayer() {
        logger.i("showMusicPlayer");
        final UIStateManager uiStateManager = RemoteDeviceManager.getInstance().getUiStateManager();
        uiStateManager.addScreenAnimationListener(new IScreenAnimationListener() {
            public void onStart(BaseScreen in, BaseScreen out) {
            }

            public void onStop(BaseScreen in, BaseScreen out) {
                MusicMenu2.this.musicManager.showMusicNotification();
                final AnonymousClass4 listener = this;
                MusicMenu2.this.handler.post(new Runnable() {
                    public void run() {
                        uiStateManager.removeScreenAnimationListener(listener);
                    }
                });
            }
        });
        this.presenter.close();
        clearMenuData();
        storeMenuData();
        this.musicManager.setMusicMenuPath(getPath());
    }

    public Menu getType() {
        return Menu.MUSIC;
    }

    public boolean isItemClickable(int id, int pos) {
        return true;
    }

    public Model getModelfromPos(int pos) {
        if (this.cachedList == null || this.cachedList.size() <= pos) {
            return null;
        }
        return (Model) this.cachedList.get(pos);
    }

    public boolean isBindCallsEnabled() {
        return true;
    }

    private String collectionIdString(MusicCollectionInfo info) {
        if (info == null) {
            return null;
        }
        MusicCollectionInfo defaults = (MusicCollectionInfo) MessageStore.removeNulls(info);
        return ((String) menuSourceStringMap.get(defaults.collectionSource)) + " - " + ((String) menuTypeStringMap.get(defaults.collectionType)) + " - " + defaults.collectionId;
    }

    private String collectionIdString(MusicArtworkResponse artworkResponse) {
        return ((String) menuSourceStringMap.get(artworkResponse.collectionSource)) + " - " + ((String) menuTypeStringMap.get(artworkResponse.collectionType)) + " - " + artworkResponse.collectionId;
    }

    public void onBindToView(Model model, View view, final int pos, ModelState state) {
        if (this.partialRefresh && pos == this.vmenuComponent.verticalList.getRawPosition()) {
            this.partialRefresh = false;
            logger.v("partial refresh:" + pos);
            this.vmenuComponent.verticalList.setViewHolderState(pos, State.SELECTED, AnimationType.NONE, 0);
        }
        logger.v("onBindToView:" + pos + " current=" + this.vmenuComponent.verticalList.getCurrentPosition());
        IconColorImageView imageView;
        IconColorImageView smallImageView;
        if (model.state instanceof MusicTrackInfo) {
            imageView = (IconColorImageView) VerticalList.findImageView(view);
            smallImageView = (IconColorImageView) VerticalList.findSmallImageView(view);
            MusicTrackInfo musicTrackInfo = model.state;
            MusicTrackInfo nowPlayingTrack = this.musicManager.getCurrentTrack();
            if (nowPlayingTrack == null || !TextUtils.equals(nowPlayingTrack.trackId, musicTrackInfo.trackId)) {
                state.updateImage = true;
                state.updateSmallImage = true;
                return;
            }
            state.updateImage = false;
            state.updateSmallImage = false;
            if (MusicManager.tryingToPlay(nowPlayingTrack.playbackState)) {
                startAudioAnimation(imageView);
            } else {
                imageView.setImageResource(R.drawable.audio_seq_32_sm);
                stopAudioAnimation();
            }
            smallImageView.setImageResource(R.drawable.audio_seq_32_sm);
        } else if (model.state instanceof MusicCollectionInfo) {
            final MusicCollectionInfo collectionInfo = model.state;
            if (!TextUtils.isEmpty(collectionInfo.collectionId)) {
                imageView = (IconColorImageView) VerticalList.findImageView(view);
                smallImageView = (IconColorImageView) VerticalList.findSmallImageView(view);
                imageView.setTag(null);
                String idString = collectionIdString(collectionInfo);
                Bitmap bitmap = PicassoUtil.getBitmapfromCache(idString);
                if (bitmap != null) {
                    imageView.setImageBitmap(bitmap);
                    smallImageView.setImageBitmap(bitmap);
                    state.updateImage = false;
                    state.updateSmallImage = false;
                    return;
                }
                imageView.setImageBitmap(null);
                smallImageView.setImageBitmap(null);
                if (this.requestedArtworkModelPositions.get(idString) != null) {
                    logger.i("Already requested artwork for collection: " + idString);
                    return;
                }
                this.requestedArtworkModelPositions.put(idString, Integer.valueOf(pos));
                logger.i("Checking cache for artwork for collection: " + collectionInfo);
                this.musicArtworkCache.getArtwork(collectionInfo, new Callback() {
                    public void onHit(@NonNull byte[] data) {
                        MusicMenu2.this.handleArtwork(data, Integer.valueOf(pos), MusicMenu2.this.collectionIdString(collectionInfo), false);
                    }

                    public void onMiss() {
                        MusicMenu2.logger.i("Requesting artwork for collection: " + collectionInfo);
                        MusicMenu2.this.postOrQueueArtworkRequest(new MusicArtworkRequest.Builder().collectionSource(collectionInfo.collectionSource).collectionType(collectionInfo.collectionType).collectionId(collectionInfo.collectionId).size(Integer.valueOf(200)).build(), collectionInfo);
                    }
                });
            }
        } else {
            state.updateImage = true;
            state.updateSmallImage = true;
        }
    }

    public IMenu getChildMenu(IMenu parent, String args, String path) {
        logger.v("getChildMenu:" + args + HereManeuverDisplayBuilder.COMMA + path);
        if (this.musicManager.hasMusicCapabilities()) {
            String[] backData = args.split(GlanceConstants.COLON_SEPARATOR);
            IMenu menu = getMusicPlayerMenu(Integer.valueOf(backData[0]).intValue(), Integer.valueOf(backData[1]).intValue());
            String element = null;
            if (path != null) {
                if (path.indexOf(HereManeuverDisplayBuilder.SLASH) == 0) {
                    element = path.substring(1);
                    int index = element.indexOf(HereManeuverDisplayBuilder.SLASH);
                    if (index >= 0) {
                        path = path.substring(index + 1);
                        element = element.substring(0, index);
                    } else {
                        path = null;
                    }
                } else {
                    path = null;
                }
            }
            if (TextUtils.isEmpty(element)) {
                return menu;
            }
            return menu.getChildMenu(this, element, path);
        }
        logger.e("No music capabilities...");
        return null;
    }

    public void onUnload(MenuLevel level) {
        switch (level) {
            case BACK_TO_PARENT:
                if (busDelegate != null && this.parentMenu.getType() == Menu.MAIN) {
                    this.bus.unregister(busDelegate);
                    this.presenter.setScrollIdleEvents(false);
                    purgeArtworkQueue();
                    infoForCurrentArtworkRequest = null;
                    this.isRequestingMusicCollection = false;
                    this.musicManager.removeMusicUpdateListener(busDelegate);
                    stopAudioAnimation();
                    busDelegate = null;
                    logger.v("onUnload-back bus-unregister");
                }
                if (this.returnToCacheList != null) {
                    logger.v("onUnload add to cache");
                    VerticalModelCache.addToCache(this.returnToCacheList);
                    this.returnToCacheList = null;
                    return;
                }
                return;
            case CLOSE:
                if (busDelegate != null) {
                    this.bus.unregister(busDelegate);
                    this.presenter.setScrollIdleEvents(false);
                    purgeArtworkQueue();
                    infoForCurrentArtworkRequest = null;
                    this.isRequestingMusicCollection = false;
                    this.musicManager.removeMusicUpdateListener(busDelegate);
                    stopAudioAnimation();
                    busDelegate = null;
                    logger.v("onUnload-close bus-unregister");
                }
                if (this.returnToCacheList != null) {
                    logger.v("onUnload add to cache");
                    VerticalModelCache.addToCache(this.returnToCacheList);
                    this.returnToCacheList = null;
                }
                if (this.parentMenu != null && this.parentMenu.getType() == Menu.MUSIC) {
                    this.parentMenu.onUnload(MenuLevel.CLOSE);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onItemSelected(ItemSelectionState selection) {
    }

    public void onScrollIdle() {
        if (!this.musicCollectionSyncComplete) {
            boolean hasCollectionInfo;
            if (this.menuData.musicCollections == null || this.menuData.musicCollections.size() <= 0) {
                hasCollectionInfo = false;
            } else {
                hasCollectionInfo = true;
            }
            boolean hasTrackInfo;
            if (this.menuData.musicTracks == null || this.menuData.musicTracks.size() <= 0) {
                hasTrackInfo = false;
            } else {
                hasTrackInfo = true;
            }
            if (this.menuData.musicCollectionInfo != null && this.menuData.musicCollectionInfo.collectionType != null && this.cachedList != null) {
                if (hasCollectionInfo || hasTrackInfo) {
                    int currentPos = this.vmenuComponent.verticalList.getCurrentPosition() - 1;
                    if (hasShuffleEntry()) {
                        currentPos--;
                    }
                    if (currentPos < 0) {
                        currentPos = 0;
                    }
                    int size = hasCollectionInfo ? this.menuData.musicCollections.size() : this.menuData.musicTracks.size();
                    if (currentPos >= size) {
                        logger.v("onScrollIdle pos(" + currentPos + ") >= size(" + size + HereManeuverDisplayBuilder.CLOSE_BRACKET);
                        return;
                    }
                    Message obj = hasCollectionInfo ? (Message) this.menuData.musicCollections.get(currentPos) : (Message) this.menuData.musicTracks.get(currentPos);
                    if (obj != null) {
                        int newPos = currentPos - 1;
                        if (newPos >= 0) {
                            obj = hasCollectionInfo ? (Message) this.menuData.musicCollections.get(newPos) : (Message) this.menuData.musicTracks.get(newPos);
                            if (obj == null) {
                                currentPos--;
                            }
                        }
                        if (obj != null) {
                            newPos = currentPos + 1;
                            if (newPos <= size - 1) {
                                obj = hasCollectionInfo ? (Message) this.menuData.musicCollections.get(newPos) : (Message) this.menuData.musicTracks.get(newPos);
                            }
                            if (obj == null) {
                                currentPos++;
                            }
                        }
                    }
                    if (obj == null) {
                        calculateOffset(currentPos, 25, hasCollectionInfo);
                        logger.v("onScrollIdle newOffset:" + this.loadingOffset.offset + " count=" + this.loadingOffset.limit);
                        requestMusicCollection(this.loadingOffset.offset, this.loadingOffset.limit, false);
                    }
                }
            }
        }
    }

    public void onFastScrollStart() {
    }

    public void onFastScrollEnd() {
        logger.v("REQUESTQUEUE onFastScrollEnd");
        purgeArtworkQueue();
    }

    public void showToolTip() {
    }

    public boolean isFirstItemEmpty() {
        return true;
    }

    private void onMusicCollectionResponse(MusicCollectionResponse musicCollectionResponse) {
        if (!TextUtils.equals(collectionIdString(musicCollectionResponse.collectionInfo), collectionIdString(this.menuData.musicCollectionInfo))) {
            logger.w("wrong collection");
        } else if (this.requestMusicOffset == -1) {
            logger.w("no outstanding request");
        } else {
            int i;
            long musicCollectionRequestResponseTime = SystemClock.elapsedRealtime() - this.currentMusicCollectionRequestTime;
            if (this.musicManager.isMusicLibraryCachingEnabled()) {
                this.musicCollectionResponseMessageCache.put(ExtensionsKt.cacheKey(this.currentMusicCollectionRequest), (Message) musicCollectionResponse);
            }
            if (this.menuData.musicCollectionInfo == null) {
                this.menuData.musicCollectionInfo = musicCollectionResponse.collectionInfo;
            } else {
                MusicCollectionInfo backupMusicCollectionInfo = this.menuData.musicCollectionInfo;
                Builder builder = new Builder(musicCollectionResponse.collectionInfo);
                if (TextUtils.isEmpty(musicCollectionResponse.collectionInfo.name)) {
                    builder.name(backupMusicCollectionInfo.name);
                }
                if (TextUtils.isEmpty(musicCollectionResponse.collectionInfo.subtitle)) {
                    builder.subtitle(backupMusicCollectionInfo.subtitle);
                }
                this.menuData.musicCollectionInfo = builder.build();
            }
            boolean firstResponse = true;
            Model[] updatedModels = null;
            int total;
            int current;
            int remaining;
            int items;
            if (musicCollectionResponse.musicCollections != null && musicCollectionResponse.musicCollections.size() > 0) {
                if (this.menuData.musicCollections == null) {
                    this.menuData.musicCollections = new ArrayList();
                } else {
                    firstResponse = false;
                }
                logger.d("[Timing] MusicCollectionRequest , received response  in : " + musicCollectionRequestResponseTime + " MS, Collections : " + musicCollectionResponse.musicCollections.size());
                if (firstResponse) {
                    this.menuData.musicCollections.addAll(musicCollectionResponse.musicCollections);
                    total = this.menuData.musicCollectionInfo.size.intValue();
                    current = musicCollectionResponse.musicCollections.size();
                    this.fastScrollIndex = checkCharacterMap(musicCollectionResponse.characterMap, total, current, true);
                    if (this.fastScrollIndex != null) {
                        this.menuData.musicIndex = musicCollectionResponse.characterMap;
                    }
                    if (total != current) {
                        remaining = total - current;
                        for (i = 0; i < remaining; i++) {
                            this.menuData.musicCollections.add(null);
                        }
                        logger.v("1-resp added[" + remaining + "]");
                    }
                } else {
                    total = this.menuData.musicCollectionInfo.size.intValue();
                    items = musicCollectionResponse.musicCollections.size();
                    updatedModels = new Model[items];
                    logger.v("2-resp offset[" + this.requestMusicOffset + "] total[" + total + "] got[" + items + "]");
                    for (i = 0; i < items; i++) {
                        MusicCollectionInfo collectionInfo = (MusicCollectionInfo) musicCollectionResponse.musicCollections.get(i);
                        this.menuData.musicCollections.set(this.requestMusicOffset + i, collectionInfo);
                        updatedModels[i] = buildModelfromCollectionInfo(this.requestMusicOffset + i, collectionInfo);
                    }
                }
            } else if (musicCollectionResponse.musicTracks == null || musicCollectionResponse.musicTracks.size() <= 0) {
                logger.w("No collections or tracks in this collection...");
                this.musicCollectionSyncComplete = true;
            } else {
                if (this.menuData.musicTracks == null) {
                    this.menuData.musicTracks = new ArrayList();
                } else {
                    firstResponse = false;
                }
                logger.d("[Timing] MusicCollectionRequest , received response  in : " + musicCollectionRequestResponseTime + " MS, Tracks : " + musicCollectionResponse.musicTracks.size());
                if (firstResponse) {
                    this.menuData.musicTracks.addAll(musicCollectionResponse.musicTracks);
                    total = this.menuData.musicCollectionInfo.size.intValue();
                    current = this.menuData.musicTracks.size();
                    this.fastScrollIndex = checkCharacterMap(musicCollectionResponse.characterMap, total, current, true);
                    if (this.fastScrollIndex != null) {
                        this.menuData.musicIndex = musicCollectionResponse.characterMap;
                    }
                    if (total != current) {
                        remaining = total - current;
                        for (i = 0; i < remaining; i++) {
                            this.menuData.musicTracks.add(null);
                        }
                        logger.v("1-resp-track added[" + remaining + "]");
                    }
                } else {
                    total = this.menuData.musicCollectionInfo.size.intValue();
                    items = musicCollectionResponse.musicTracks.size();
                    updatedModels = new Model[items];
                    logger.v("2-resp-track offset[" + this.requestMusicOffset + "] total[" + total + "]  got[" + items + "]");
                    for (i = 0; i < items; i++) {
                        MusicTrackInfo trackInfo = (MusicTrackInfo) musicCollectionResponse.musicTracks.get(i);
                        this.menuData.musicTracks.set(this.requestMusicOffset + i, trackInfo);
                        Model model = IconBkColorViewHolder.buildModel(this.requestMusicOffset + i, R.drawable.icon_song, this.bgColorSelected, bgColorUnselected, this.bgColorSelected, trackInfo.name, trackInfo.author);
                        model.state = trackInfo;
                        updatedModels[i] = model;
                    }
                }
            }
            if (firstResponse || updatedModels == null) {
                this.presenter.cancelLoadingAnimation(1);
                this.highlightedItem = this.vmenuComponent.verticalList.getCurrentPosition();
                this.presenter.updateCurrentMenu(this);
            } else {
                int pos = this.requestMusicOffset + 1;
                if (hasShuffleEntry()) {
                    pos++;
                }
                logger.v("updatemodels pos=" + pos + " len=" + updatedModels.length);
                this.partialRefresh = true;
                if (this.cachedList != null) {
                    int len = this.cachedList.size();
                    for (i = 0; i < updatedModels.length; i++) {
                        int newPos = pos + i;
                        if (newPos >= len) {
                            logger.v("invalid index:" + newPos + " size:" + len);
                            break;
                        }
                        this.cachedList.set(newPos, updatedModels[i]);
                    }
                }
                this.presenter.refreshData(pos, updatedModels, this);
            }
            this.requestMusicOffset = -1;
            this.isRequestingMusicCollection = false;
            if (this.nextRequestOffset != -1) {
                boolean request = false;
                if (this.menuData.musicCollections != null) {
                    if (this.menuData.musicCollections.get(this.nextRequestOffset) == null) {
                        request = true;
                    }
                } else if (this.menuData.musicTracks != null && this.menuData.musicTracks.get(this.nextRequestOffset) == null) {
                    request = true;
                }
                if (request) {
                    requestMusicCollection(this.nextRequestOffset, this.nextRequestLimit, false);
                    logger.v("pending request:" + this.nextRequestLimit + " limit=" + this.nextRequestLimit);
                }
                this.nextRequestOffset = -1;
                this.nextRequestLimit = -1;
            }
        }
    }

    private void onMusicArtworkResponse(final MusicArtworkResponse artworkResponse) {
        logger.d("REQUESTQUEUE onMusicArtworkResponse " + artworkResponse);
        final String idString = collectionIdString(artworkResponse);
        final Integer pos = (Integer) this.requestedArtworkModelPositions.get(idString);
        if (pos != null || TextUtils.equals(idString, collectionIdString(this.menuData.musicCollectionInfo))) {
            long artWorkResponseTime = SystemClock.elapsedRealtime() - this.artWorkRequestTime;
            if (artworkResponse.photo != null) {
                logger.d("[Timing] MusicArtworkRequest, response received in " + artWorkResponseTime + " MS, Size : " + artworkResponse.photo.size());
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        byte[] byteArray = artworkResponse.photo.toByteArray();
                        if (byteArray == null || byteArray.length == 0) {
                            MusicMenu2.logger.w("Received photo has null or empty byte array");
                        } else {
                            MusicMenu2.this.handleArtwork(byteArray, pos, idString, true);
                        }
                    }
                }, 1);
            } else {
                logger.w("No photo in artwork response");
            }
            synchronized (artworkRequestLock) {
                if (artworkRequestQueue.isEmpty()) {
                    logger.d("REQUESTQUEUE onMusicArtworkResponse queue empty");
                    infoForCurrentArtworkRequest = null;
                } else {
                    Pair<MusicArtworkRequest, MusicCollectionInfo> artworkRequestPair = (Pair) artworkRequestQueue.pop();
                    MusicArtworkRequest artworkRequest = artworkRequestPair.first;
                    logger.d("REQUESTQUEUE onMusicArtworkResponse posting: " + artworkRequest);
                    infoForCurrentArtworkRequest = (MusicCollectionInfo) artworkRequestPair.second;
                    this.artWorkRequestTime = SystemClock.elapsedRealtime();
                    logger.d("[Timing] Making MusicArtWorkRequest " + artworkRequest);
                    this.bus.post(new RemoteEvent(artworkRequest));
                }
            }
            return;
        }
        logger.w("Wrong menu");
    }

    private void handleArtwork(byte[] byteArray, Integer pos, String idString, boolean saveInDiskCache) {
        Bitmap artwork;
        int size = artworkSize;
        Bitmap bitmap = ScalingUtilities.decodeByteArray(byteArray, size, size, ScalingLogic.FIT);
        if (MusicCollectionType.COLLECTION_TYPE_ARTISTS == this.menuData.musicCollectionInfo.collectionType) {
            artwork = new RoundedTransformationBuilder().oval(true).build().transform(bitmap);
        } else {
            artwork = bitmap;
        }
        final String str = idString;
        final Integer num = pos;
        final boolean z = saveInDiskCache;
        final byte[] bArr = byteArray;
        this.handler.post(new Runnable() {
            public void run() {
                MusicCollectionInfo info;
                PicassoUtil.setBitmapInCache(str, artwork);
                if (num != null) {
                    if (MusicMenu2.this.presenter.getCurrentMenu() == MusicMenu2.this) {
                        MusicMenu2.logger.d("Refreshing artwork for position: " + num);
                        MusicMenu2.this.presenter.refreshDataforPos(num.intValue());
                    }
                    int size = MusicMenu2.this.cachedList.size();
                    if (num.intValue() < 0 || num.intValue() >= size) {
                        MusicMenu2.logger.e("Refreshing artwork invalid index: " + num + " size=" + size);
                        return;
                    }
                    MusicCollectionInfo state = ((Model) MusicMenu2.this.cachedList.get(num.intValue())).state;
                    if (state instanceof MusicCollectionInfo) {
                        info = state;
                    } else {
                        MusicMenu2.logger.e("Something has gone terribly wrong.");
                        return;
                    }
                }
                MusicMenu2.this.vmenuComponent.setSelectedIconImage(artwork);
                info = MusicMenu2.this.menuData.musicCollectionInfo;
                if (z && info != null && info.collectionType != null) {
                    MusicMenu2.logger.d("Setting bitmap in cache for collection: " + info);
                    MusicMenu2.this.musicArtworkCache.putArtwork(info, bArr);
                }
            }
        });
    }

    private MusicMenu2 getMusicPlayerMenu(int id, int pos) {
        MusicMenuData musicMenuData;
        logger.d("getMusicPlayerMenu " + id + ", " + pos);
        String subPath = getSubPath(id, pos);
        if (lastPlayedMusicMenuData.containsKey(subPath)) {
            musicMenuData = (MusicMenuData) lastPlayedMusicMenuData.get(subPath);
        } else {
            MusicCollectionInfo info = null;
            List<MusicCollectionInfo> collections = null;
            if (this.menuData.musicCollectionInfo.collectionSource == null) {
                if (id == R.id.apple_podcasts) {
                    info = new Builder().collectionSource(MusicCollectionSource.COLLECTION_SOURCE_IOS_MEDIA_PLAYER).collectionType(MusicCollectionType.COLLECTION_TYPE_PODCASTS).build();
                } else {
                    MusicCollectionInfo collectionInfo = (MusicCollectionInfo) this.menuData.musicCollections.get(id);
                    info = new Builder().collectionSource(collectionInfo.collectionSource).build();
                    List<MusicCollectionType> collectionTypes = this.musicManager.getCollectionTypesForSource(collectionInfo.collectionSource);
                    collections = new ArrayList(collectionTypes.size());
                    for (MusicCollectionType collectionType : collectionTypes) {
                        collections.add(new Builder().collectionSource(collectionInfo.collectionSource).collectionType(collectionType).build());
                    }
                }
            } else if (this.menuData.musicCollectionInfo.collectionType == null) {
                info = new Builder().collectionSource(this.menuData.musicCollectionInfo.collectionSource).collectionType(((MusicCollectionInfo) this.menuData.musicCollections.get(id)).collectionType).build();
            } else if (this.menuData.musicCollections.size() > 0) {
                info = (MusicCollectionInfo) this.menuData.musicCollections.get(id);
                logger.i("fetching collection " + info.collectionId);
            }
            musicMenuData = new MusicMenuData(info, collections);
        }
        MusicMenu2 menu = new MusicMenu2(this.bus, this.vmenuComponent, this.presenter, this, musicMenuData);
        menu.setBackSelectionPos(pos);
        menu.setBackSelectionId(id);
        return menu;
    }

    public String getPath() {
        if (this.path == null) {
            if (this.parentMenu.getType() == Menu.MUSIC) {
                this.path = ((MusicMenu2) this.parentMenu).getPath() + HereManeuverDisplayBuilder.SLASH + this.backSelectionId + GlanceConstants.COLON_SEPARATOR + this.backSelection;
            } else {
                this.path = HereManeuverDisplayBuilder.SLASH + Menu.MUSIC.name();
            }
        }
        return this.path;
    }

    private String getSubPath(int id, int pos) {
        return getPath() + HereManeuverDisplayBuilder.SLASH + id + GlanceConstants.COLON_SEPARATOR + pos;
    }

    public static void clearMenuData() {
        lastPlayedMusicMenuData.clear();
    }

    private void storeMenuData() {
        if (this.parentMenu.getType() == Menu.MUSIC) {
            ((MusicMenu2) this.parentMenu).storeMenuData();
        }
        lastPlayedMusicMenuData.put(getPath(), this.menuData);
    }

    private void setupIconBgColor() {
        if (this.menuData.musicCollectionInfo == null) {
            return;
        }
        if (this.menuData.musicCollectionInfo.collectionSource == MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL) {
            this.bgColorSelected = androidBgColor;
        } else if (this.menuData.musicCollectionInfo.collectionType == MusicCollectionType.COLLECTION_TYPE_PODCASTS) {
            this.bgColorSelected = applePodcastsBgColor;
        } else {
            this.bgColorSelected = appleMusicBgColor;
        }
    }

    private void startAudioAnimation(IconColorImageView imageView) {
        imageView.animate().cancel();
        imageView.setImageResource(R.drawable.audio_loop);
        AnimationDrawable frameAnimation = (AnimationDrawable) imageView.getDrawable();
        frameAnimation.start();
        this.musicAnimation = frameAnimation;
        logger.v("startAudioAnimation");
    }

    private void stopAudioAnimation() {
        if (this.musicAnimation != null && this.musicAnimation.isRunning()) {
            this.musicAnimation.stop();
            logger.v("stopAudioAnimation");
        }
        this.musicAnimation = null;
    }

    private boolean isThisQueuePlaying() {
        return TextUtils.equals(getPath(), this.musicManager.getMusicMenuPath());
    }

    private VerticalFastScrollIndex checkCharacterMap(List<MusicCharacterMap> list, int total, int current, boolean check) {
        boolean hasCharacterMap = list != null;
        int size = -1;
        if (hasCharacterMap) {
            size = list.size();
        }
        if (hasCharacterMap) {
            if (!check || (total > 40 && size >= 2)) {
                VerticalFastScrollIndex scrollIndex = buildIndexFromCharacterMap(list);
                if (scrollIndex != null) {
                    logger.v("checkCharacterMap index-yes len[" + total + "] map[" + size + "] index[" + scrollIndex.getEntryCount() + "]");
                    return scrollIndex;
                }
                logger.v("checkCharacterMap index-error len[" + total + "] map[" + size + "]");
                return scrollIndex;
            }
            logger.v("checkCharacterMap index-no created len[" + total + "] index-len[" + size + "]");
        }
        return null;
    }

    private VerticalFastScrollIndex buildIndexFromCharacterMap(List<MusicCharacterMap> list) {
        try {
            VerticalFastScrollIndex.Builder builder = new VerticalFastScrollIndex.Builder();
            for (MusicCharacterMap map : list) {
                builder.setEntry(map.character.charAt(0), map.offset.intValue());
            }
            builder.positionOffset(1);
            return builder.build();
        } catch (Throwable t) {
            logger.e("buildIndexFromCharacterMap", t);
            return null;
        }
    }

    private boolean isIndexSupported(MusicCollectionInfo collectionInfo) {
        if (!(collectionInfo == null || collectionInfo.collectionType == null)) {
            switch (collectionInfo.collectionType) {
                case COLLECTION_TYPE_PLAYLISTS:
                case COLLECTION_TYPE_ARTISTS:
                case COLLECTION_TYPE_ALBUMS:
                    return true;
            }
        }
        return false;
    }

    private void calculateOffset(int currentPos, int pageLimit, boolean hasCollectionInfo) {
        int top = (currentPos / pageLimit) * pageLimit;
        int bottom = Math.min((hasCollectionInfo ? this.menuData.musicCollections.size() : this.menuData.musicTracks.size()) - 1, top + pageLimit);
        this.loadingOffset.clear();
        this.loadingOffset.offset = top;
        this.loadingOffset.limit = (bottom - top) + 1;
    }

    private Model buildModelfromCollectionInfo(int id, MusicCollectionInfo collectionInfo) {
        String subtitle;
        if (collectionInfo.subtitle != null) {
            subtitle = collectionInfo.subtitle;
        } else {
            subtitle = resources.getQuantityString(collectionInfo.collectionType == MusicCollectionType.COLLECTION_TYPE_PODCASTS ? R.plurals.episodes_count : R.plurals.songs_count, collectionInfo.size.intValue(), new Object[]{collectionInfo.size});
        }
        Model model = IconBkColorViewHolder.buildModel(id, ((Integer) menuTypeIconMap.get(collectionInfo.collectionType)).intValue(), this.bgColorSelected, bgColorUnselected, transparentColor, collectionInfo.name, subtitle, null, collectionInfo.collectionType == MusicCollectionType.COLLECTION_TYPE_ARTISTS ? IconShape.CIRCLE : IconShape.SQUARE);
        model.state = collectionInfo;
        return model;
    }

    private boolean hasShuffleEntry() {
        if (this.cachedList == null || this.cachedList.size() < 2 || ((Model) this.cachedList.get(1)).id != R.id.music_browser_shuffle) {
            return false;
        }
        return true;
    }
}
