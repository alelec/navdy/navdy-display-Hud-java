package com.navdy.hud.app.ui.component.homescreen;

import android.widget.TextView;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;

public class OpenRoadView$$ViewInjector {
    public static void inject(Finder finder, OpenRoadView target, Object source) {
        target.openMapRoadInfo = (TextView) finder.findRequiredView(source, R.id.openMapRoadInfo, "field 'openMapRoadInfo'");
    }

    public static void reset(OpenRoadView target) {
        target.openMapRoadInfo = null;
    }
}
