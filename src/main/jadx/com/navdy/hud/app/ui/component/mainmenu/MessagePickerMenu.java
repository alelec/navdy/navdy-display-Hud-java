package com.navdy.hud.app.ui.component.mainmenu;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.content.ContextCompat;
import android.view.View;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.framework.contacts.Contact;
import com.navdy.hud.app.framework.glance.GlanceHandler;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu;
import com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel;
import com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.TypeIntrinsics;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000x\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 <2\u00020\u0001:\u0001<B=\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0001\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\b\u0010\f\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\rJ(\u0010\u0014\u001a\u0004\u0018\u00010\u00012\b\u0010\u0006\u001a\u0004\u0018\u00010\u00012\b\u0010\u0015\u001a\u0004\u0018\u00010\t2\b\u0010\u0016\u001a\u0004\u0018\u00010\tH\u0016J\b\u0010\u0017\u001a\u00020\u000fH\u0016J\u000e\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012H\u0016J\u0012\u0010\u0019\u001a\u0004\u0018\u00010\u00132\u0006\u0010\u001a\u001a\u00020\u000fH\u0016J\n\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0016J\b\u0010\u001d\u001a\u00020\u001eH\u0016J\b\u0010\u001f\u001a\u00020 H\u0016J\b\u0010!\u001a\u00020 H\u0016J\u0018\u0010\"\u001a\u00020 2\u0006\u0010#\u001a\u00020\u000f2\u0006\u0010\u001a\u001a\u00020\u000fH\u0016J.\u0010$\u001a\u00020%2\b\u0010&\u001a\u0004\u0018\u00010\u00132\b\u0010'\u001a\u0004\u0018\u00010(2\u0006\u0010\u001a\u001a\u00020\u000f2\b\u0010)\u001a\u0004\u0018\u00010*H\u0016J\b\u0010+\u001a\u00020%H\u0016J\b\u0010,\u001a\u00020%H\u0016J\u0012\u0010-\u001a\u00020%2\b\u0010.\u001a\u0004\u0018\u00010/H\u0016J\b\u00100\u001a\u00020%H\u0016J\u0012\u00101\u001a\u00020%2\b\u00102\u001a\u0004\u0018\u000103H\u0016J\u0010\u00104\u001a\u00020 2\u0006\u0010.\u001a\u00020/H\u0016J\u0010\u00105\u001a\u00020%2\u0006\u00106\u001a\u00020\tH\u0002J\u0010\u00107\u001a\u00020%2\u0006\u0010#\u001a\u00020\u000fH\u0016J\u0010\u00108\u001a\u00020%2\u0006\u00109\u001a\u00020\u000fH\u0016J\b\u0010:\u001a\u00020%H\u0016J\b\u0010;\u001a\u00020%H\u0016R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\u0013\u0018\u00010\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u0004\u0018\u00010\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006="}, d2 = {"Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;", "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;", "vscrollComponent", "Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;", "presenter", "Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;", "parent", "messages", "", "", "contact", "Lcom/navdy/hud/app/framework/contacts/Contact;", "notificationId", "(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Ljava/util/List;Lcom/navdy/hud/app/framework/contacts/Contact;Ljava/lang/String;)V", "backSelection", "", "backSelectionId", "cachedList", "", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;", "getChildMenu", "args", "path", "getInitialSelection", "getItems", "getModelfromPos", "pos", "getScrollIndex", "Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;", "getType", "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;", "isBindCallsEnabled", "", "isFirstItemEmpty", "isItemClickable", "id", "onBindToView", "", "model", "view", "Landroid/view/View;", "state", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;", "onFastScrollEnd", "onFastScrollStart", "onItemSelected", "selection", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;", "onScrollIdle", "onUnload", "level", "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;", "selectItem", "sendMessage", "message", "setBackSelectionId", "setBackSelectionPos", "n", "setSelectedIcon", "showToolTip", "Companion", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
/* compiled from: MessagePickerMenu.kt */
public final class MessagePickerMenu implements IMenu {
    public static final Companion Companion = new Companion();
    private static final int SELECTION_ANIMATION_DELAY = 1000;
    private static final Model back = IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, Companion.getFluctuatorColor(), MainMenu.bkColorUnselected, Companion.getFluctuatorColor(), Companion.getResources().getString(R.string.back), null);
    private static final int baseMessageId = 100;
    private static final Context context = HudApplication.getAppContext();
    private static final int fluctuatorColor = ContextCompat.getColor(Companion.getContext(), R.color.mm_back);
    private static final Logger logger = new Logger("MessagePickerMenu");
    private static final int messageColor = ContextCompat.getColor(Companion.getContext(), R.color.share_location);
    private static final Resources resources = HudApplication.getAppContext().getResources();
    private int backSelection;
    private int backSelectionId;
    private List<Model> cachedList;
    private final Contact contact;
    private final List<String> messages;
    private final String notificationId;
    private final IMenu parent;
    private final Presenter presenter;
    private final VerticalMenuComponent vscrollComponent;

    @Metadata(bv = {1, 0, 1}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u001c\u0010\u0007\u001a\n \t*\u0004\u0018\u00010\b0\bX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\f\u001a\u00020\u0004X\u0082D\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u0006R\u001c\u0010\u000e\u001a\n \t*\u0004\u0018\u00010\u000f0\u000fX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0014\u0010\u0012\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0006R\u0014\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0014\u0010\u0018\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0006R\u001c\u0010\u001a\u001a\n \t*\u0004\u0018\u00010\u001b0\u001bX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001d\u00a8\u0006\u001e"}, d2 = {"Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;", "", "()V", "SELECTION_ANIMATION_DELAY", "", "getSELECTION_ANIMATION_DELAY", "()I", "back", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;", "kotlin.jvm.PlatformType", "getBack", "()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;", "baseMessageId", "getBaseMessageId", "context", "Landroid/content/Context;", "getContext", "()Landroid/content/Context;", "fluctuatorColor", "getFluctuatorColor", "logger", "Lcom/navdy/service/library/log/Logger;", "getLogger", "()Lcom/navdy/service/library/log/Logger;", "messageColor", "getMessageColor", "resources", "Landroid/content/res/Resources;", "getResources", "()Landroid/content/res/Resources;", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
    /* compiled from: MessagePickerMenu.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker $constructor_marker) {
            this();
        }

        private final Logger getLogger() {
            return MessagePickerMenu.logger;
        }

        private final int getSELECTION_ANIMATION_DELAY() {
            return MessagePickerMenu.SELECTION_ANIMATION_DELAY;
        }

        private final Context getContext() {
            return MessagePickerMenu.context;
        }

        private final Resources getResources() {
            return MessagePickerMenu.resources;
        }

        private final int getFluctuatorColor() {
            return MessagePickerMenu.fluctuatorColor;
        }

        private final int getMessageColor() {
            return MessagePickerMenu.messageColor;
        }

        private final Model getBack() {
            return MessagePickerMenu.back;
        }

        private final int getBaseMessageId() {
            return MessagePickerMenu.baseMessageId;
        }
    }

    public MessagePickerMenu(@NotNull VerticalMenuComponent vscrollComponent, @NotNull Presenter presenter, @NotNull IMenu parent, @NotNull List<String> messages, @NotNull Contact contact, @Nullable String notificationId) {
        Intrinsics.checkParameterIsNotNull(vscrollComponent, "vscrollComponent");
        Intrinsics.checkParameterIsNotNull(presenter, "presenter");
        Intrinsics.checkParameterIsNotNull(parent, "parent");
        Intrinsics.checkParameterIsNotNull(messages, "messages");
        Intrinsics.checkParameterIsNotNull(contact, "contact");
        this.vscrollComponent = vscrollComponent;
        this.presenter = presenter;
        this.parent = parent;
        this.messages = messages;
        this.contact = contact;
        this.notificationId = notificationId;
    }

    @NotNull
    public List<Model> getItems() {
        if (this.cachedList != null) {
            List list = this.cachedList;
            if (list != null) {
                return TypeIntrinsics.asMutableList(list);
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.collections.MutableList<com.navdy.hud.app.ui.component.vlist.VerticalList.Model>");
        }
        ArrayList list2 = new ArrayList();
        list2.add(Companion.getBack());
        int counter = 0;
        for (String msg : this.messages) {
            int counter2 = counter + 1;
            Model model = IconBkColorViewHolder.buildModel(Companion.getBaseMessageId() + counter, R.drawable.icon_message, Companion.getMessageColor(), MainMenu.bkColorUnselected, Companion.getMessageColor(), msg, null);
            Intrinsics.checkExpressionValueIsNotNull(model, "IconBkColorViewHolder.bu\u2026 messageColor, msg, null)");
            list2.add(model);
            counter = counter2;
        }
        this.cachedList = list2;
        return list2;
    }

    public int getInitialSelection() {
        return 1;
    }

    @Nullable
    public VerticalFastScrollIndex getScrollIndex() {
        return null;
    }

    public void setBackSelectionPos(int n) {
        this.backSelection = n;
    }

    public void setBackSelectionId(int id) {
        this.backSelectionId = id;
    }

    public void setSelectedIcon() {
        this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_message, Companion.getMessageColor(), null, 1.0f);
        this.vscrollComponent.selectedText.setText(Companion.getResources().getString(R.string.message));
    }

    public boolean selectItem(@NotNull ItemSelectionState selection) {
        Intrinsics.checkParameterIsNotNull(selection, "selection");
        Companion.getLogger().v("select id:" + selection.id + " pos:" + selection.pos);
        switch (selection.id) {
            case R.id.menu_back:
                AnalyticsSupport.recordMenuSelection("back");
                this.presenter.loadMenu(this.parent, MenuLevel.BACK_TO_PARENT, this.backSelection, this.backSelectionId);
                this.backSelectionId = 0;
                break;
            default:
                this.presenter.performSelectionAnimation(new MessagePickerMenu$selectItem$1(this, selection), Companion.getSELECTION_ANIMATION_DELAY());
                break;
        }
        return true;
    }

    @NotNull
    public Menu getType() {
        return Menu.MESSAGE_PICKER;
    }

    public boolean isItemClickable(int id, int pos) {
        return true;
    }

    @Nullable
    public Model getModelfromPos(int pos) {
        List list = this.cachedList;
        if (list == null || list.size() <= pos) {
            return null;
        }
        return (Model) list.get(pos);
    }

    public boolean isBindCallsEnabled() {
        return false;
    }

    public void onBindToView(@Nullable Model model, @Nullable View view, int pos, @Nullable ModelState state) {
    }

    @Nullable
    public IMenu getChildMenu(@Nullable IMenu parent, @Nullable String args, @Nullable String path) {
        return null;
    }

    public void onUnload(@Nullable MenuLevel level) {
    }

    public void onItemSelected(@Nullable ItemSelectionState selection) {
    }

    public void onScrollIdle() {
    }

    public void onFastScrollStart() {
    }

    public void onFastScrollEnd() {
    }

    public void showToolTip() {
    }

    private final void sendMessage(String message) {
        Companion.getLogger().v("sendMessage:" + this.contact.number);
        GlanceHandler glanceHandler = GlanceHandler.getInstance();
        if (this.notificationId != null) {
            NotificationManager.getInstance().removeNotification(this.notificationId);
        }
        if (glanceHandler.sendMessage(this.contact.number, message, this.contact.name)) {
            glanceHandler.sendSmsSuccessNotification(UUID.randomUUID().toString(), this.contact.number, message, this.contact.name);
        } else {
            glanceHandler.sendSmsFailedNotification(this.contact.number, message, this.contact.name);
        }
    }

    public boolean isFirstItemEmpty() {
        return true;
    }
}
