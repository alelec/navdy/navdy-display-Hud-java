package com.navdy.hud.app.ui.component.tbt;

import android.animation.AnimatorSet.Builder;
import android.content.Context;
import android.content.res.Resources;
import android.text.Layout.Alignment;
import android.text.SpannableStringBuilder;
import android.text.StaticLayout;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.TextView;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.maps.MapEvents.ManeuverDisplay;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState;
import com.navdy.hud.app.view.MainView.CustomAnimationMode;
import com.navdy.service.library.log.Logger;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import net.hockeyapp.android.LoginActivity;
import org.jetbrains.annotations.NotNull;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000e\u0018\u0000 82\u00020\u0001:\u000289B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B\u0017\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007B\u001f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ(\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u00102\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010!\u001a\u00020\tH\u0002J\u0006\u0010\"\u001a\u00020\u001dJ\u0010\u0010#\u001a\u00020$2\u0006\u0010\u001e\u001a\u00020\u0010H\u0002J\u001a\u0010%\u001a\u00020\u001d2\u0006\u0010&\u001a\u00020\f2\n\u0010'\u001a\u00060(R\u00020)J \u0010*\u001a\u00020\u001d2\u0006\u0010+\u001a\u00020,2\u0006\u0010-\u001a\u00020\u000e2\u0006\u0010.\u001a\u00020\u000eH\u0002J\u000e\u0010/\u001a\u00020\u001d2\u0006\u0010&\u001a\u00020\fJ\u0010\u00100\u001a\u00020\u001d2\u0006\u00101\u001a\u00020\tH\u0002J\u0018\u00102\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u00102\u0006\u0010.\u001a\u00020\u000eH\u0002J8\u00103\u001a\u00020\u000e2\u0006\u00104\u001a\u00020 2\u0006\u0010!\u001a\u00020\t2\u0006\u0010\u001e\u001a\u00020\u00102\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u00105\u001a\u00020\u000e2\u0006\u00106\u001a\u00020\u000eH\u0002J\"\u00107\u001a\u00020\u001d2\u0006\u0010+\u001a\u00020,2\b\b\u0002\u0010-\u001a\u00020\u000e2\b\b\u0002\u0010.\u001a\u00020\u000eR\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0014\u001a\u0004\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0015\u001a\u0004\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0017\u001a\u00060\u0018j\u0002`\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006:"}, d2 = {"Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;", "Landroid/widget/TextView;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "currentMode", "Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;", "instructionWidthIncludesNext", "", "lastInstruction", "", "lastInstructionWidth", "lastManeuverState", "Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;", "lastRoadText", "lastTurnText", "sizeCalculationTextView", "stringBuilder", "Ljava/lang/StringBuilder;", "Lkotlin/text/StringBuilder;", "widthInfo", "Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;", "calculateTextWidth", "", "instruction", "textSize", "", "maxWidth", "clear", "getBoldText", "Landroid/text/SpannableStringBuilder;", "getCustomAnimator", "mode", "mainBuilder", "Landroid/animation/AnimatorSet$Builder;", "Landroid/animation/AnimatorSet;", "setInstruction", "event", "Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;", "adjustWidthForNextManeuver", "nextManeuverVisible", "setMode", "setViewWidth", "width", "setWidth", "tryTextSize", "fontSize", "check2Lines", "applyBold", "updateDisplay", "Companion", "WidthInfo", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
/* compiled from: TbtTextInstructionView.kt */
public final class TbtTextInstructionView extends TextView {
    public static final Companion Companion = new Companion();
    private static final int fullWidth = Companion.getResources().getDimensionPixelSize(R.dimen.tbt_instruction_full_w);
    private static final Logger logger = new Logger("TbtTextInstructionView");
    private static final int mediumWidth = Companion.getResources().getDimensionPixelSize(R.dimen.tbt_instruction_medium_w);
    private static final Resources resources;
    private static final int singleLineMinWidth = Companion.getResources().getDimensionPixelSize(R.dimen.tbt_instruction_min_w);
    private static final float size18 = Companion.getResources().getDimension(R.dimen.tbt_instruction_18);
    private static final float size20 = Companion.getResources().getDimension(R.dimen.tbt_instruction_20);
    private static final float size22 = Companion.getResources().getDimension(R.dimen.tbt_instruction_22);
    private static final float size24 = Companion.getResources().getDimension(R.dimen.tbt_instruction_24);
    private static final float size26 = Companion.getResources().getDimension(R.dimen.tbt_instruction_26);
    private static final int smallWidth = Companion.getResources().getDimensionPixelSize(R.dimen.tbt_instruction_small_w);
    private HashMap _$_findViewCache;
    private CustomAnimationMode currentMode;
    private boolean instructionWidthIncludesNext;
    private String lastInstruction;
    private int lastInstructionWidth = -1;
    private ManeuverState lastManeuverState;
    private String lastRoadText;
    private String lastTurnText;
    private final TextView sizeCalculationTextView = new TextView(HudApplication.getAppContext());
    private final StringBuilder stringBuilder = new StringBuilder();
    private final WidthInfo widthInfo = new WidthInfo();

    @Metadata(bv = {1, 0, 1}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0007\n\u0002\b\r\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u000b\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u0006R\u0014\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0014\u0010\u0011\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0006R\u0014\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0014\u0010\u0017\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0016R\u0014\u0010\u0019\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0016R\u0014\u0010\u001b\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u0016R\u0014\u0010\u001d\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u0016R\u0014\u0010\u001f\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b \u0010\u0006\u00a8\u0006!"}, d2 = {"Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;", "", "()V", "fullWidth", "", "getFullWidth", "()I", "logger", "Lcom/navdy/service/library/log/Logger;", "getLogger", "()Lcom/navdy/service/library/log/Logger;", "mediumWidth", "getMediumWidth", "resources", "Landroid/content/res/Resources;", "getResources", "()Landroid/content/res/Resources;", "singleLineMinWidth", "getSingleLineMinWidth", "size18", "", "getSize18", "()F", "size20", "getSize20", "size22", "getSize22", "size24", "getSize24", "size26", "getSize26", "smallWidth", "getSmallWidth", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
    /* compiled from: TbtTextInstructionView.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker $constructor_marker) {
            this();
        }

        private final Logger getLogger() {
            return TbtTextInstructionView.logger;
        }

        private final Resources getResources() {
            return TbtTextInstructionView.resources;
        }

        private final int getFullWidth() {
            return TbtTextInstructionView.fullWidth;
        }

        private final int getMediumWidth() {
            return TbtTextInstructionView.mediumWidth;
        }

        private final int getSmallWidth() {
            return TbtTextInstructionView.smallWidth;
        }

        private final int getSingleLineMinWidth() {
            return TbtTextInstructionView.singleLineMinWidth;
        }

        private final float getSize26() {
            return TbtTextInstructionView.size26;
        }

        private final float getSize24() {
            return TbtTextInstructionView.size24;
        }

        private final float getSize22() {
            return TbtTextInstructionView.size22;
        }

        private final float getSize20() {
            return TbtTextInstructionView.size20;
        }

        private final float getSize18() {
            return TbtTextInstructionView.size18;
        }
    }

    @Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000b\n\u0002\u0010\u0002\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u000f\u001a\u00020\u0010R\u001a\u0010\u0003\u001a\u00020\u0004X\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\u00020\u0004X\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u0006\"\u0004\b\u000b\u0010\bR\u001a\u0010\f\u001a\u00020\u0004X\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u0006\"\u0004\b\u000e\u0010\b\u00a8\u0006\u0011"}, d2 = {"Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;", "", "()V", "lineWidth1", "", "getLineWidth1$app_hudRelease", "()I", "setLineWidth1$app_hudRelease", "(I)V", "lineWidth2", "getLineWidth2$app_hudRelease", "setLineWidth2$app_hudRelease", "numLines", "getNumLines$app_hudRelease", "setNumLines$app_hudRelease", "clear", "", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
    /* compiled from: TbtTextInstructionView.kt */
    private static final class WidthInfo {
        private int lineWidth1;
        private int lineWidth2;
        private int numLines;

        public final int getNumLines$app_hudRelease() {
            return this.numLines;
        }

        public final void setNumLines$app_hudRelease(int <set-?>) {
            this.numLines = <set-?>;
        }

        public final int getLineWidth1$app_hudRelease() {
            return this.lineWidth1;
        }

        public final void setLineWidth1$app_hudRelease(int <set-?>) {
            this.lineWidth1 = <set-?>;
        }

        public final int getLineWidth2$app_hudRelease() {
            return this.lineWidth2;
        }

        public final void setLineWidth2$app_hudRelease(int <set-?>) {
            this.lineWidth2 = <set-?>;
        }

        public final void clear() {
            this.numLines = 0;
            this.lineWidth1 = 0;
            this.lineWidth2 = 0;
        }
    }

    public void _$_clearFindViewByIdCache() {
        if (this._$_findViewCache != null) {
            this._$_findViewCache.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        view = findViewById(i);
        this._$_findViewCache.put(Integer.valueOf(i), view);
        return view;
    }

    static {
        Resources resources = HudApplication.getAppContext().getResources();
        Intrinsics.checkExpressionValueIsNotNull(resources, "HudApplication.getAppContext().resources");
        resources = resources;
    }

    public TbtTextInstructionView(@NotNull Context context) {
        super(context);
        Intrinsics.checkParameterIsNotNull(context, "context");
    }

    public TbtTextInstructionView(@NotNull Context context, @NotNull AttributeSet attrs) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(attrs, "attrs");
        super(context, attrs);
    }

    public TbtTextInstructionView(@NotNull Context context, @NotNull AttributeSet attrs, int defStyleAttr) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(attrs, "attrs");
        super(context, attrs, defStyleAttr);
    }

    public final void setMode(@NotNull CustomAnimationMode mode) {
        Intrinsics.checkParameterIsNotNull(mode, LoginActivity.EXTRA_MODE);
        if (!Intrinsics.areEqual(this.currentMode,  mode)) {
            if (this.lastInstructionWidth != -1) {
                boolean setWidth = false;
                MarginLayoutParams lytParams = getLayoutParams();
                if (lytParams == null) {
                    throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
                }
                lytParams = lytParams;
                switch (mode) {
                    case EXPAND:
                        if (Intrinsics.areEqual(this.currentMode, CustomAnimationMode.SHRINK_LEFT) && lytParams.width == Companion.getMediumWidth()) {
                            setWidth = true;
                            break;
                        }
                    case SHRINK_LEFT:
                        if (Intrinsics.areEqual(this.currentMode, CustomAnimationMode.EXPAND) && this.lastInstructionWidth > Companion.getMediumWidth()) {
                            setWidth = true;
                            break;
                        }
                }
                if (setWidth) {
                    String str = this.lastInstruction;
                    if (str != null) {
                        setWidth(str, true);
                    }
                }
            } else if (Intrinsics.areEqual( mode, CustomAnimationMode.EXPAND)) {
                setViewWidth(Companion.getFullWidth());
            } else {
                setViewWidth(Companion.getMediumWidth());
            }
            this.currentMode = mode;
        }
    }

    public final void getCustomAnimator(@NotNull CustomAnimationMode mode, @NotNull Builder mainBuilder) {
        Intrinsics.checkParameterIsNotNull(mode, LoginActivity.EXTRA_MODE);
        Intrinsics.checkParameterIsNotNull(mainBuilder, "mainBuilder");
    }

    public final void clear() {
        setText("");
        this.lastTurnText = (String) null;
        this.lastRoadText = (String) null;
        this.lastInstruction = (String) null;
        this.lastInstructionWidth = -1;
        this.instructionWidthIncludesNext = false;
    }

    public static /* bridge */ /* synthetic */ void updateDisplay$default(TbtTextInstructionView tbtTextInstructionView, ManeuverDisplay maneuverDisplay, boolean z, boolean z2, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        if ((i & 4) != 0) {
            z2 = false;
        }
        tbtTextInstructionView.updateDisplay(maneuverDisplay, z, z2);
    }

    public final void updateDisplay(@NotNull ManeuverDisplay event, boolean adjustWidthForNextManeuver, boolean nextManeuverVisible) {
        Intrinsics.checkParameterIsNotNull(event, "event");
        setInstruction(event, adjustWidthForNextManeuver, nextManeuverVisible);
        this.lastManeuverState = event.maneuverState;
    }

    private final void setInstruction(ManeuverDisplay event, boolean adjustWidthForNextManeuver, boolean nextManeuverVisible) {
        if (!TextUtils.equals(event.pendingTurn, this.lastTurnText) || !TextUtils.equals(event.pendingRoad, this.lastRoadText) || adjustWidthForNextManeuver) {
            this.lastTurnText = event.pendingTurn;
            this.lastRoadText = event.pendingRoad;
            this.stringBuilder.setLength(0);
            if (this.lastTurnText != null && HereManeuverDisplayBuilder.canShowTurnText(this.lastTurnText)) {
                this.stringBuilder.append(this.lastTurnText);
                this.stringBuilder.append(" ");
            }
            if (this.lastRoadText != null) {
                this.stringBuilder.append(this.lastRoadText);
            }
            String str = this.stringBuilder.toString();
            this.lastInstruction = str;
            Intrinsics.checkExpressionValueIsNotNull(str, "str");
            setWidth(str, nextManeuverVisible);
        }
    }

    private final void setWidth(String instruction, boolean nextManeuverVisible) {
        this.lastInstructionWidth = -1;
        int maxWidth = 0;
        CustomAnimationMode customAnimationMode = this.currentMode;
        if (customAnimationMode != null) {
            switch (customAnimationMode) {
                case EXPAND:
                    if (!nextManeuverVisible) {
                        maxWidth = Companion.getFullWidth();
                        break;
                    } else {
                        maxWidth = Companion.getMediumWidth();
                        break;
                    }
                case SHRINK_LEFT:
                    maxWidth = Companion.getSmallWidth();
                    break;
            }
        }
        if (!tryTextSize(Companion.getSize26(), maxWidth, instruction, this.widthInfo, false, false)) {
            if (!tryTextSize(Companion.getSize24(), maxWidth, instruction, this.widthInfo, true, false)) {
                if (!tryTextSize(Companion.getSize22(), maxWidth, instruction, this.widthInfo, true, false)) {
                    if (!tryTextSize(Companion.getSize20(), maxWidth, instruction, this.widthInfo, true, true)) {
                        setTextSize(Companion.getSize18());
                        setText(getBoldText(instruction));
                        setViewWidth(maxWidth);
                        this.lastInstructionWidth = maxWidth;
                    }
                }
            }
        }
    }

    private final void calculateTextWidth(String instruction, float textSize, WidthInfo widthInfo, int maxWidth) {
        widthInfo.clear();
        this.sizeCalculationTextView.setTextSize(textSize);
        StaticLayout layout = new StaticLayout(instruction, this.sizeCalculationTextView.getPaint(), maxWidth, Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
        widthInfo.setNumLines$app_hudRelease(layout.getLineCount());
        widthInfo.setLineWidth1$app_hudRelease((int) layout.getLineMax(0));
        if (widthInfo.getNumLines$app_hudRelease() > 1) {
            widthInfo.setLineWidth2$app_hudRelease((int) layout.getLineMax(1));
        }
    }

    private final void setViewWidth(int width) {
        MarginLayoutParams lytParams = getLayoutParams();
        if (lytParams == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
        }
        lytParams.width = width;
        requestLayout();
    }

    private final SpannableStringBuilder getBoldText(String instruction) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        spannableStringBuilder.append(instruction);
        spannableStringBuilder.setSpan(new StyleSpan(1), 0, spannableStringBuilder.length(), 33);
        return spannableStringBuilder;
    }

    private final boolean tryTextSize(float fontSize, int maxWidth, String instruction, WidthInfo widthInfo, boolean check2Lines, boolean applyBold) {
        calculateTextWidth(instruction, fontSize, widthInfo, maxWidth);
        CharSequence instruction2;
        if (widthInfo.getNumLines$app_hudRelease() == 1) {
            setTextSize(fontSize);
            if (applyBold) {
                instruction2 = getBoldText(instruction);
            } else {
                instruction = instruction;
            }
            setText(instruction);
            int newWidth = widthInfo.getLineWidth1$app_hudRelease();
            if (newWidth < Companion.getSingleLineMinWidth()) {
                newWidth = Companion.getSingleLineMinWidth();
            }
            setViewWidth(newWidth);
            this.lastInstructionWidth = newWidth;
            return true;
        } else if (!check2Lines || widthInfo.getNumLines$app_hudRelease() != 2) {
            return false;
        } else {
            setTextSize(fontSize);
            if (applyBold) {
                instruction2 = getBoldText(instruction);
            } else {
                instruction = instruction;
            }
            setText(instruction);
            setViewWidth(maxWidth);
            this.lastInstructionWidth = maxWidth;
            return true;
        }
    }
}
