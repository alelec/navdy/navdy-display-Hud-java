package com.navdy.hud.app.ui.activity;

import com.navdy.hud.app.ui.activity.Main.Module;
import dagger.internal.ModuleAdapter;

public final class Main$Module$$ModuleAdapter extends ModuleAdapter<Module> {
    private static final Class<?>[] INCLUDES = new Class[0];
    private static final String[] INJECTS = new String[]{"members/com.navdy.hud.app.ui.activity.MainActivity", "members/com.navdy.hud.app.view.MainView", "members/com.navdy.hud.app.view.NotificationView", "members/com.navdy.hud.app.presenter.NotificationPresenter", "members/com.navdy.hud.app.ui.framework.UIStateManager", "members/com.navdy.hud.app.view.ContainerView", "members/com.navdy.hud.app.debug.GestureEngine"};
    private static final Class<?>[] STATIC_INJECTIONS = new Class[0];

    public Main$Module$$ModuleAdapter() {
        super(Module.class, INJECTS, STATIC_INJECTIONS, false, INCLUDES, true, false);
    }
}
