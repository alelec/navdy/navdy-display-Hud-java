package com.navdy.hud.app.maps.here;

import com.here.android.mpa.common.Image;
import com.here.android.mpa.guidance.NavigationManager.AspectRatio;
import com.here.android.mpa.guidance.NavigationManager.RealisticViewListener;
import com.here.android.mpa.guidance.NavigationManager.RealisticViewMode;
import com.navdy.hud.app.maps.MapEvents.DisplayJunction;
import com.navdy.hud.app.maps.MapEvents.HideSignPostJunction;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;
import java.lang.ref.WeakReference;

public class HereRealisticViewListener extends RealisticViewListener {
    private static final HideSignPostJunction HIDE_SIGN_POST_JUNCTION = new HideSignPostJunction();
    private static final Logger sLogger = new Logger(HereRealisticViewListener.class);
    private final Bus bus;
    private final HereNavController navController;
    private boolean running;
    private Runnable start = new Runnable() {
        public void run() {
            HereRealisticViewListener.this.navController.setRealisticViewMode(RealisticViewMode.NIGHT);
            HereRealisticViewListener.this.navController.addRealisticViewAspectRatio(AspectRatio.AR_3x5);
            HereRealisticViewListener.this.navController.addRealisticViewListener(new WeakReference(HereRealisticViewListener.this));
            HereRealisticViewListener.sLogger.v("added realistic view listener");
        }
    };
    private Runnable stop = new Runnable() {
        public void run() {
            HereRealisticViewListener.this.navController.setRealisticViewMode(RealisticViewMode.OFF);
            HereRealisticViewListener.this.navController.removeRealisticViewListener(HereRealisticViewListener.this);
            HereRealisticViewListener.sLogger.v("removed realistic view listener");
        }
    };

    HereRealisticViewListener(Bus bus, HereNavController navController) {
        this.navController = navController;
        this.bus = bus;
    }

    synchronized void start() {
        if (!this.running) {
            TaskManager.getInstance().execute(this.start, 15);
            this.running = true;
            sLogger.v("start:added realistic view listener");
        }
    }

    synchronized void stop() {
        if (this.running) {
            TaskManager.getInstance().execute(this.stop, 15);
            this.running = false;
            clearDisplayEvent();
            sLogger.v("stop:remove realistic view listener");
        }
    }

    public void onRealisticViewNextManeuver(AspectRatio ratio, Image img1, Image img2) {
    }

    public void onRealisticViewShow(AspectRatio ratio, Image img1, Image img2) {
        this.bus.post(new DisplayJunction(img1, img2));
    }

    public void onRealisticViewHide() {
        clearDisplayEvent();
    }

    private void clearDisplayEvent() {
        this.bus.post(HIDE_SIGN_POST_JUNCTION);
    }
}
