package com.navdy.hud.app.maps.here;

import com.here.android.mpa.guidance.NavigationManager.NewInstructionEventListener;
import com.here.android.mpa.routing.Maneuver;
import com.here.android.mpa.routing.Maneuver.Action;
import com.here.android.mpa.routing.Maneuver.Icon;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.hud.app.maps.MapEvents.ManeuverEvent;
import com.navdy.hud.app.maps.MapEvents.ManeuverEvent.Type;
import com.navdy.hud.app.maps.notification.RouteCalculationNotification;
import com.navdy.service.library.events.audio.SpeechRequest.Category;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;

class HereNewManeuverListener extends NewInstructionEventListener {
    private final Bus bus;
    private boolean hasNewRoute;
    private final HereNavigationManager hereNavigationManager;
    private final Logger logger;
    private final HereNavController navController;
    private Maneuver navManeuver;
    private final String tag;
    private final boolean verbose;

    HereNewManeuverListener(Logger logger, String tag, boolean verbose, HereNavController navController, HereNavigationManager hereNavigationManager, Bus bus) {
        this.logger = logger;
        this.tag = tag;
        this.verbose = verbose;
        this.navController = navController;
        this.hereNavigationManager = hereNavigationManager;
        this.bus = bus;
    }

    public void onNewInstructionEvent() {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                try {
                    Maneuver nextManeuver = HereNewManeuverListener.this.navController.getNextManeuver();
                    HereNewManeuverListener.this.navManeuver = nextManeuver;
                    if (nextManeuver == null) {
                        HereNewManeuverListener.this.logger.w(HereNewManeuverListener.this.tag + "There is no next maneuver");
                        return;
                    }
                    Maneuver maneuverAfterNext = HereNewManeuverListener.this.navController.getAfterNextManeuver();
                    if (HereNewManeuverListener.this.verbose) {
                        HereNewManeuverListener.this.logger.v(HereNewManeuverListener.this.tag + " " + HereNewManeuverListener.this.navController.getState().name() + " current road[" + HereMapUtil.getCurrentRoadName() + "] maneuver road name[" + nextManeuver.getRoadName() + "] maneuver next roadname[" + nextManeuver.getNextRoadName() + "] turn[" + nextManeuver.getTurn().name() + "] action[" + nextManeuver.getAction().name() + "] icon[" + nextManeuver.getIcon().name() + "]");
                    }
                    if (nextManeuver.getAction() == Action.END || nextManeuver.getIcon() == Icon.END) {
                        HereNewManeuverListener.this.logger.v(HereNewManeuverListener.this.tag + " " + HereNewManeuverListener.this.navController.getState() + " LAST MANEUVER RECEIVED");
                        maneuverAfterNext = null;
                    }
                    boolean clearPreviousRoute = HereNewManeuverListener.this.hasNewRoute;
                    HereNewManeuverListener.this.hasNewRoute = false;
                    HereNewManeuverListener.this.hereNavigationManager.updateNavigationInfo(nextManeuver, maneuverAfterNext, null, clearPreviousRoute);
                    if (HereNewManeuverListener.this.hereNavigationManager.isShownFirstManeuver()) {
                        HereNewManeuverListener.this.bus.post(new ManeuverEvent(Type.INTERMEDIATE, nextManeuver));
                        return;
                    }
                    HereNewManeuverListener.this.hereNavigationManager.setShownFirstManeuver(true);
                    if (HereNewManeuverListener.this.hereNavigationManager.getNavigationSessionPreference().spokenTurnByTurn) {
                        String tts = HereRouteManager.buildStartRouteTTS();
                        HereNewManeuverListener.this.logger.v("tts[" + tts + "]");
                        HereNewManeuverListener.this.bus.post(RouteCalculationNotification.CANCEL_CALC_TTS);
                        TTSUtils.sendSpeechRequest(tts, Category.SPEECH_TURN_BY_TURN, RouteCalculationNotification.ROUTE_CALC_TTS_ID);
                    }
                    HereNewManeuverListener.this.bus.post(new ManeuverEvent(Type.FIRST, nextManeuver));
                } catch (Throwable t) {
                    HereNewManeuverListener.this.logger.e(t);
                }
            }
        }, 20);
    }

    void setNewRoute() {
        this.logger.v("setNewRoute");
        this.hasNewRoute = true;
        if (this.logger.isLoggable(2)) {
            this.logger.v("setNewRoute: send empty maneuver");
        }
        this.bus.post(HereManeuverDisplayBuilder.EMPTY_MANEUVER_DISPLAY);
    }

    Maneuver getNavManeuver() {
        return this.navManeuver;
    }

    void clearNavManeuver() {
        this.navManeuver = null;
    }
}
