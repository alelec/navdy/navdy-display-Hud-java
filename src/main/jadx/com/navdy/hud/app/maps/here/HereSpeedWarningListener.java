package com.navdy.hud.app.maps.here;

import android.content.Context;
import com.here.android.mpa.guidance.NavigationManager.SpeedWarningListener;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.phonecall.CallUtils;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.manager.SpeedManager.SpeedUnit;
import com.navdy.hud.app.maps.MapsEventHandler;
import com.navdy.service.library.events.audio.SpeechRequest.Category;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;

public class HereSpeedWarningListener extends SpeedWarningListener {
    private static final int BUFFER_SPEED = 7;
    private Bus bus;
    private Context context = HudApplication.getAppContext();
    private HereNavigationManager hereNavigationManager;
    private Logger logger;
    private MapsEventHandler mapsEventHandler;
    private SpeedManager speedManager = SpeedManager.getInstance();
    private boolean speedWarningOn;
    private String tag;
    private boolean verbose;
    private int warningSpeed;

    HereSpeedWarningListener(Logger logger, String tag, boolean verbose, Bus bus, MapsEventHandler mapsEventHandler, HereNavigationManager hereNavigationManager) {
        this.logger = logger;
        this.tag = tag;
        this.verbose = verbose;
        this.bus = bus;
        this.mapsEventHandler = mapsEventHandler;
        this.hereNavigationManager = hereNavigationManager;
    }

    public void onSpeedExceeded(String roadName, float speedLimit) {
        this.bus.post(HereNavigationManager.SPEED_EXCEEDED);
        this.logger.w(this.tag + " speed exceeded:" + roadName + HereManeuverDisplayBuilder.COMMA + speedLimit);
        if (!this.speedWarningOn) {
            this.warningSpeed = -1;
        }
        this.speedWarningOn = true;
        if (Boolean.TRUE.equals(MapsEventHandler.getInstance().getNavigationPreferences().spokenSpeedLimitWarnings) && !CallUtils.isPhoneCallInProgress()) {
            int currentSpeed = this.speedManager.getCurrentSpeed();
            if (currentSpeed <= 0) {
                this.logger.w(this.tag + "no obd or gps speed available");
                return;
            }
            int speedToConsider;
            SpeedUnit speedUnit = this.speedManager.getSpeedUnit();
            int speedAllowed = SpeedManager.convert((double) speedLimit, SpeedUnit.METERS_PER_SECOND, speedUnit);
            if (this.warningSpeed == -1) {
                speedToConsider = speedAllowed + 7;
            } else {
                speedToConsider = this.warningSpeed + (this.warningSpeed / 10);
            }
            if (currentSpeed >= speedToConsider) {
                this.logger.w(this.tag + " speed exceeded current[" + currentSpeed + "] threshold[" + speedToConsider + "] allowed[" + speedAllowed + "] " + speedUnit.name());
                this.warningSpeed = currentSpeed;
                String str;
                switch (speedUnit) {
                    case MILES_PER_HOUR:
                        str = this.hereNavigationManager.TTS_MILES;
                        break;
                    case KILOMETERS_PER_HOUR:
                        str = this.hereNavigationManager.TTS_KMS;
                        break;
                    case METERS_PER_SECOND:
                        str = this.hereNavigationManager.TTS_METERS;
                        break;
                    default:
                        str = "";
                        break;
                }
                TTSUtils.sendSpeechRequest(this.context.getString(R.string.tts_speed_warning, new Object[]{Integer.valueOf(speedAllowed)}), Category.SPEECH_SPEED_WARNING, null);
                return;
            }
            this.logger.w(this.tag + " speed exceeded current[" + currentSpeed + "] threshold[" + speedToConsider + "] allowed[" + speedAllowed + "] " + speedUnit.name());
        }
    }

    public void onSpeedExceededEnd(String roadName, float speedLimit) {
        this.speedWarningOn = false;
        this.warningSpeed = -1;
        this.bus.post(HereNavigationManager.SPEED_NORMAL);
        this.logger.w(this.tag + "speed normal:" + roadName + HereManeuverDisplayBuilder.COMMA + speedLimit);
    }
}
