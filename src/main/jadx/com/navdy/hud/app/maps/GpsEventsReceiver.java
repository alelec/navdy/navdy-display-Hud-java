package com.navdy.hud.app.maps;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import com.navdy.hud.app.device.gps.GpsConstants;
import com.navdy.hud.app.device.gps.GpsDeadReckoningManager;
import com.navdy.hud.app.device.gps.GpsUtils.GpsSatelliteData;
import com.navdy.hud.app.device.gps.GpsUtils.GpsSwitch;
import com.navdy.hud.app.event.DrivingStateChange;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.service.library.log.Logger;

public class GpsEventsReceiver extends BroadcastReceiver {
    private static final DrivingStateChange DRIVING_STARTED = new DrivingStateChange(true);
    private static final DrivingStateChange DRIVING_STOPPED = new DrivingStateChange(false);
    private Logger sLogger = new Logger(GpsEventsReceiver.class);

    public void onReceive(Context context, Intent intent) {
        reportGpsEvent(intent);
    }

    private void reportGpsEvent(Intent intent) {
        try {
            String action = intent.getAction();
            Bundle b = intent.getExtras();
            Object obj = -1;
            switch (action.hashCode()) {
                case -2045233493:
                    if (action.equals(GpsConstants.GPS_SATELLITE_STATUS)) {
                        obj = 7;
                        break;
                    }
                    break;
                case -1801456507:
                    if (action.equals(GpsConstants.GPS_EVENT_DEAD_RECKONING_STARTED)) {
                        obj = null;
                        break;
                    }
                    break;
                case -1788590639:
                    if (action.equals(GpsConstants.GPS_EVENT_DEAD_RECKONING_STOPPED)) {
                        obj = 1;
                        break;
                    }
                    break;
                case -629625847:
                    if (action.equals(GpsConstants.GPS_EVENT_DRIVING_STARTED)) {
                        obj = 3;
                        break;
                    }
                    break;
                case -616759979:
                    if (action.equals(GpsConstants.GPS_EVENT_DRIVING_STOPPED)) {
                        obj = 4;
                        break;
                    }
                    break;
                case -192082374:
                    if (action.equals(GpsConstants.GPS_EVENT_ENABLE_ESF_RAW)) {
                        obj = 6;
                        break;
                    }
                    break;
                case -107791191:
                    if (action.equals(GpsConstants.GPS_EVENT_SWITCH)) {
                        obj = 2;
                        break;
                    }
                    break;
                case 897686227:
                    if (action.equals(GpsConstants.GPS_EVENT_WARM_RESET_UBLOX)) {
                        obj = 5;
                        break;
                    }
                    break;
                case 1315284313:
                    if (action.equals(GpsConstants.GPS_COLLECT_LOGS)) {
                        obj = 8;
                        break;
                    }
                    break;
            }
            switch (obj) {
                case null:
                    TTSUtils.debugShowDRStarted("");
                    return;
                case 1:
                    TTSUtils.debugShowDREnded();
                    return;
                case 2:
                    TTSUtils.debugShowGpsSwitch(b.getString("title"), b.getString("info"));
                    RemoteDeviceManager.getInstance().getBus().post(new GpsSwitch(b.getBoolean(GpsConstants.USING_PHONE_LOCATION), b.getBoolean(GpsConstants.USING_UBLOX_LOCATION)));
                    return;
                case 3:
                    this.sLogger.i("Driving started");
                    RemoteDeviceManager.getInstance().getBus().post(DRIVING_STARTED);
                    return;
                case 4:
                    this.sLogger.i("Driving stopped");
                    RemoteDeviceManager.getInstance().getBus().post(DRIVING_STOPPED);
                    return;
                case 5:
                    this.sLogger.v("warm reset ublox");
                    GpsDeadReckoningManager.getInstance().sendWarmReset();
                    TTSUtils.debugShowGpsReset("Warm Reset Ublox");
                    return;
                case 6:
                    this.sLogger.v("esf-raw");
                    GpsDeadReckoningManager.getInstance().enableEsfRaw();
                    return;
                case 7:
                    Bundle data = intent.getBundleExtra(GpsConstants.GPS_EVENT_SATELLITE_DATA);
                    if (data != null) {
                        RemoteDeviceManager.getInstance().getBus().post(new GpsSatelliteData(data));
                        return;
                    }
                    return;
                case 8:
                    String path = intent.getStringExtra(GpsConstants.GPS_EXTRA_LOG_PATH);
                    if (TextUtils.isEmpty(path)) {
                        this.sLogger.w("Invalid gps log path:" + path);
                        return;
                    } else {
                        GpsDeadReckoningManager.getInstance().dumpGpsInfo(path);
                        return;
                    }
                default:
                    return;
            }
        } catch (Throwable t) {
            this.sLogger.e(t);
        }
        this.sLogger.e(t);
    }
}
