package com.navdy.hud.app.maps.here;

import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import org.json.JSONArray;
import org.json.JSONObject;

public class HereOfflineMapsVersion {
    static final Logger sLogger = new Logger(HereOfflineMapsVersion.class);
    private Date date;
    private List<String> packages;
    private String rawDate;
    private String version;

    public HereOfflineMapsVersion(String offlineMapsData) {
        try {
            JSONObject jsonObject = new JSONObject(offlineMapsData);
            this.rawDate = jsonObject.getString("version");
            this.date = parseMapDate(this.rawDate);
            this.version = jsonObject.getString("here_map_version");
            JSONArray offlineMaps = jsonObject.getJSONArray("map_packages");
            int len = offlineMaps.length();
            this.packages = new ArrayList(len);
            for (int i = 0; i < len; i++) {
                this.packages.add(offlineMaps.getString(i));
            }
        } catch (Exception e) {
            sLogger.e("Failed to parse off-line maps version info", e);
        }
    }

    public String getRawDate() {
        return this.rawDate;
    }

    public Date getDate() {
        return this.date;
    }

    public List<String> getPackages() {
        return this.packages;
    }

    public String getVersion() {
        return this.version;
    }

    private Date parseMapDate(String str) throws Exception {
        int y = -1;
        int m = -1;
        int d = -1;
        StringTokenizer tokenizer = new StringTokenizer(str, GlanceConstants.PERIOD);
        while (tokenizer.hasMoreElements()) {
            String token = tokenizer.nextToken();
            if (y == -1) {
                y = Integer.parseInt(token);
            } else if (m == -1) {
                m = Integer.parseInt(token);
            } else {
                d = Integer.parseInt(token);
            }
        }
        if (y == -1 || m == -1 || d == -1) {
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.set(1, y);
        calendar.set(2, m - 1);
        calendar.set(5, d);
        return calendar.getTime();
    }
}
