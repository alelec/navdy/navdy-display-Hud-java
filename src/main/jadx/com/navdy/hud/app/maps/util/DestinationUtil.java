package com.navdy.hud.app.maps.util;

public class DestinationUtil {
    public static java.util.List<com.navdy.hud.app.ui.component.destination.DestinationParcelable> convert(android.content.Context r35, java.util.List<com.navdy.service.library.events.destination.Destination> r36, int r37, int r38, boolean r39) {
        /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: Unknown predecessor block by arg (r25_1 'currentPosition' com.navdy.service.library.events.location.LatLong) in PHI: PHI: (r25_2 'currentPosition' com.navdy.service.library.events.location.LatLong) = (r25_0 'currentPosition' com.navdy.service.library.events.location.LatLong), (r25_1 'currentPosition' com.navdy.service.library.events.location.LatLong) binds: {(r25_0 'currentPosition' com.navdy.service.library.events.location.LatLong)=B:3:0x0012, (r25_1 'currentPosition' com.navdy.service.library.events.location.LatLong)=B:4:0x0014}
	at jadx.core.dex.instructions.PhiInsn.replaceArg(PhiInsn.java:78)
	at jadx.core.dex.visitors.ModVisitor.processInvoke(ModVisitor.java:222)
	at jadx.core.dex.visitors.ModVisitor.replaceStep(ModVisitor.java:83)
	at jadx.core.dex.visitors.ModVisitor.visit(ModVisitor.java:68)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
	at java.util.ArrayList.forEach(Unknown Source)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:34)
	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:59)
	at java.lang.Iterable.forEach(Unknown Source)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:39)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:282)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:200)
*/
        /*
        r3 = com.navdy.hud.app.maps.here.HereMapsManager.getInstance();
        r30 = r3.getLocationFixManager();
        r24 = 0;
        r25 = 0;
        if (r30 == 0) goto L_0x0012;
    L_0x000e:
        r24 = r30.getLastGeoCoordinate();
    L_0x0012:
        if (r24 == 0) goto L_0x002b;
    L_0x0014:
        r25 = new com.navdy.service.library.events.location.LatLong;
        r4 = r24.getLatitude();
        r3 = java.lang.Double.valueOf(r4);
        r4 = r24.getLongitude();
        r4 = java.lang.Double.valueOf(r4);
        r0 = r25;
        r0.<init>(r3, r4);
    L_0x002b:
        r34 = new java.util.ArrayList;
        r3 = r36.size();
        r0 = r34;
        r0.<init>(r3);
        r31 = 0;
    L_0x0038:
        r3 = r36.size();
        r0 = r31;
        if (r0 >= r3) goto L_0x01bc;
    L_0x0040:
        r0 = r36;
        r1 = r31;
        r26 = r0.get(r1);
        r26 = (com.navdy.service.library.events.destination.Destination) r26;
        r27 = -1082130432; // 0xffffffffbf800000 float:-1.0 double:NaN;
        r14 = 0;
        r16 = 0;
        r10 = 0;
        r12 = 0;
        r7 = 0;
        r0 = r26;
        r3 = r0.navigation_position;
        if (r3 == 0) goto L_0x0099;
    L_0x005b:
        r0 = r26;
        r3 = r0.navigation_position;
        r3 = r3.latitude;
        r10 = r3.doubleValue();
        r0 = r26;
        r3 = r0.navigation_position;
        r3 = r3.longitude;
        r12 = r3.doubleValue();
        r0 = r26;
        r3 = r0.navigation_position;
        r3 = r3.latitude;
        r4 = r3.doubleValue();
        r8 = 0;
        r3 = (r4 > r8 ? 1 : (r4 == r8 ? 0 : -1));
        if (r3 == 0) goto L_0x0183;
    L_0x007f:
        r0 = r26;
        r3 = r0.navigation_position;
        r3 = r3.longitude;
        r4 = r3.doubleValue();
        r8 = 0;
        r3 = (r4 > r8 ? 1 : (r4 == r8 ? 0 : -1));
        if (r3 == 0) goto L_0x0183;
    L_0x008f:
        r0 = r26;
        r3 = r0.navigation_position;
        r0 = r25;
        r27 = com.navdy.hud.app.maps.util.MapUtils.distanceBetween(r3, r0);
    L_0x0099:
        r28 = 0;
        r3 = -1082130432; // 0xffffffffbf800000 float:-1.0 double:NaN;
        r3 = (r27 > r3 ? 1 : (r27 == r3 ? 0 : -1));
        if (r3 == 0) goto L_0x00fe;
    L_0x00a1:
        r29 = new com.navdy.hud.app.maps.util.DistanceConverter$Distance;
        r29.<init>();
        r3 = com.navdy.hud.app.manager.SpeedManager.getInstance();
        r3 = r3.getSpeedUnit();
        r0 = r27;
        r1 = r29;
        com.navdy.hud.app.maps.util.DistanceConverter.convertToDistance(r3, r0, r1);
        r0 = r29;
        r3 = r0.value;
        r0 = r29;
        r4 = r0.unit;
        r0 = r39;
        r28 = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.getFormattedDistance(r3, r4, r0);
        r3 = " ";
        r0 = r28;
        r32 = r0.indexOf(r3);
        r3 = -1;
        r0 = r32;
        if (r0 == r3) goto L_0x00fe;
    L_0x00d0:
        r3 = new java.lang.StringBuilder;
        r3.<init>();
        r4 = "<b>";
        r3 = r3.append(r4);
        r4 = 0;
        r0 = r28;
        r1 = r32;
        r4 = r0.substring(r4, r1);
        r3 = r3.append(r4);
        r4 = "</b>";
        r3 = r3.append(r4);
        r0 = r28;
        r1 = r32;
        r4 = r0.substring(r1);
        r3 = r3.append(r4);
        r7 = r3.toString();
    L_0x00fe:
        r0 = r26;
        r3 = r0.display_position;
        if (r3 == 0) goto L_0x0118;
    L_0x0104:
        r0 = r26;
        r3 = r0.display_position;
        r3 = r3.latitude;
        r14 = r3.doubleValue();
        r0 = r26;
        r3 = r0.display_position;
        r3 = r3.longitude;
        r16 = r3.doubleValue();
    L_0x0118:
        r0 = r26;
        r3 = r0.place_type;
        r33 = com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.getPlaceTypeHolder(r3);
        if (r33 == 0) goto L_0x01b5;
    L_0x0122:
        r0 = r33;
        r0 = r0.iconRes;
        r18 = r0;
        r0 = r33;
        r3 = r0.colorRes;
        r0 = r35;
        r20 = android.support.v4.content.ContextCompat.getColor(r0, r3);
    L_0x0132:
        r2 = new com.navdy.hud.app.ui.component.destination.DestinationParcelable;
        r3 = 0;
        r0 = r26;
        r4 = r0.destination_title;
        r0 = r26;
        r5 = r0.destination_subtitle;
        r6 = 0;
        r8 = 1;
        r0 = r26;
        r9 = r0.full_address;
        r19 = 0;
        r22 = com.navdy.hud.app.ui.component.destination.DestinationParcelable.DestinationType.DESTINATION;
        r0 = r26;
        r0 = r0.place_type;
        r23 = r0;
        r21 = r38;
        r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10, r12, r14, r16, r18, r19, r20, r21, r22, r23);
        r0 = r26;
        r3 = r0.identifier;
        r2.setIdentifier(r3);
        r0 = r26;
        r3 = r0.place_id;
        r2.setPlaceId(r3);
        r0 = r28;
        r2.distanceStr = r0;
        r0 = r26;
        r3 = r0.contacts;
        r3 = com.navdy.hud.app.framework.contacts.ContactUtil.fromContacts(r3);
        r2.setContacts(r3);
        r0 = r26;
        r3 = r0.phoneNumbers;
        r3 = com.navdy.hud.app.framework.contacts.ContactUtil.fromPhoneNumbers(r3);
        r2.setPhoneNumbers(r3);
        r0 = r34;
        r0.add(r2);
        r31 = r31 + 1;
        goto L_0x0038;
    L_0x0183:
        r0 = r26;
        r3 = r0.display_position;
        if (r3 == 0) goto L_0x0099;
    L_0x0189:
        r0 = r26;
        r3 = r0.display_position;
        r3 = r3.latitude;
        r4 = r3.doubleValue();
        r8 = 0;
        r3 = (r4 > r8 ? 1 : (r4 == r8 ? 0 : -1));
        if (r3 == 0) goto L_0x0099;
    L_0x0199:
        r0 = r26;
        r3 = r0.display_position;
        r3 = r3.longitude;
        r4 = r3.doubleValue();
        r8 = 0;
        r3 = (r4 > r8 ? 1 : (r4 == r8 ? 0 : -1));
        if (r3 == 0) goto L_0x0099;
    L_0x01a9:
        r0 = r26;
        r3 = r0.display_position;
        r0 = r25;
        r27 = com.navdy.hud.app.maps.util.MapUtils.distanceBetween(r3, r0);
        goto L_0x0099;
    L_0x01b5:
        r18 = R.drawable.icon_mm_places_2; // 0x7f020184 float:1.728075E38 double:1.0527737993E-314;
        r20 = r37;
        goto L_0x0132;
    L_0x01bc:
        return r34;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.navdy.hud.app.maps.util.DestinationUtil.convert(android.content.Context, java.util.List, int, int, boolean):java.util.List<com.navdy.hud.app.ui.component.destination.DestinationParcelable>");
    }
}
