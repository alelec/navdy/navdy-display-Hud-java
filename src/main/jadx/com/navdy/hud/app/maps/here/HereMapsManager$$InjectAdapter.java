package com.navdy.hud.app.maps.here;

import android.content.SharedPreferences;
import com.navdy.hud.app.device.PowerManager;
import com.navdy.hud.app.profile.DriverProfileManager;
import com.squareup.otto.Bus;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class HereMapsManager$$InjectAdapter extends Binding<HereMapsManager> implements MembersInjector<HereMapsManager> {
    private Binding<Bus> bus;
    private Binding<DriverProfileManager> mDriverProfileManager;
    private Binding<PowerManager> powerManager;
    private Binding<SharedPreferences> sharedPreferences;

    public HereMapsManager$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.maps.here.HereMapsManager", false, HereMapsManager.class);
    }

    public void attach(Linker linker) {
        this.bus = linker.requestBinding("com.squareup.otto.Bus", HereMapsManager.class, getClass().getClassLoader());
        this.powerManager = linker.requestBinding("com.navdy.hud.app.device.PowerManager", HereMapsManager.class, getClass().getClassLoader());
        this.sharedPreferences = linker.requestBinding("android.content.SharedPreferences", HereMapsManager.class, getClass().getClassLoader());
        this.mDriverProfileManager = linker.requestBinding("com.navdy.hud.app.profile.DriverProfileManager", HereMapsManager.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.bus);
        injectMembersBindings.add(this.powerManager);
        injectMembersBindings.add(this.sharedPreferences);
        injectMembersBindings.add(this.mDriverProfileManager);
    }

    public void injectMembers(HereMapsManager object) {
        object.bus = (Bus) this.bus.get();
        object.powerManager = (PowerManager) this.powerManager.get();
        object.sharedPreferences = (SharedPreferences) this.sharedPreferences.get();
        object.mDriverProfileManager = (DriverProfileManager) this.mDriverProfileManager.get();
    }
}
