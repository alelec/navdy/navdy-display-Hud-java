package com.navdy.hud.app.maps.here;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.text.TextUtils;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.GeoPosition;
import com.here.android.mpa.common.Image;
import com.here.android.mpa.common.MatchedGeoPosition;
import com.here.android.mpa.common.PositioningManager.LocationMethod;
import com.here.android.mpa.mapping.MapMarker;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.debug.DriveRecorder;
import com.navdy.hud.app.device.gps.GpsConstants;
import com.navdy.hud.app.device.gps.GpsUtils;
import com.navdy.hud.app.device.gps.GpsUtils.GpsSwitch;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.maps.MapEvents.GPSSpeedEvent;
import com.navdy.hud.app.maps.MapEvents.GpsStatusChange;
import com.navdy.hud.app.maps.MapEvents.LocationFix;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.service.library.events.debug.StartDriveRecordingEvent;
import com.navdy.service.library.events.debug.StopDriveRecordingEvent;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class HereLocationFixManager {
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'_'HH_mm_ss.SSS", Locale.US);
    private static final byte[] RECORD_MARKER = "<< Marker >>\n".getBytes();
    private static final Logger sLogger = new Logger(HereLocationFixManager.class);
    private LocationListener androidGpsLocationListener = null;
    private final Bus bus;
    private Runnable checkLocationFix = new Runnable() {
        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            try {
                long diff = SystemClock.elapsedRealtime() - HereLocationFixManager.this.lastAndroidLocationTime;
                if (HereLocationFixManager.this.locationFix) {
                    if (diff >= 3000) {
                        HereLocationFixManager.sLogger.i("lost location fix[" + diff + "]");
                        HereLocationFixManager.this.locationFix = false;
                        HereLocationFixManager.this.bus.post(new LocationFix(false, false, false));
                    } else if (HereLocationFixManager.sLogger.isLoggable(2)) {
                        HereLocationFixManager.sLogger.v("still has location fix [" + diff + "]");
                    }
                } else if (HereLocationFixManager.this.lastAndroidLocation == null || diff > 3000) {
                    if (HereLocationFixManager.sLogger.isLoggable(2)) {
                        HereLocationFixManager.sLogger.v("still don't have location fix [" + diff + "]");
                    }
                    if (HereLocationFixManager.this.firstLocationFixCheck) {
                        HereLocationFixManager.this.bus.post(new LocationFix(false, false, false));
                    }
                } else {
                    HereLocationFixManager.sLogger.v("got location fix [" + diff + "] " + HereLocationFixManager.this.lastAndroidLocation);
                    HereLocationFixManager.this.locationFix = true;
                    HereLocationFixManager.this.bus.post(HereLocationFixManager.this.getLocationFixEvent());
                }
                HereLocationFixManager.this.handler.postDelayed(HereLocationFixManager.this.checkLocationFix, 1000);
                HereLocationFixManager.this.firstLocationFixCheck = false;
            } catch (Throwable th) {
                HereLocationFixManager.this.handler.postDelayed(HereLocationFixManager.this.checkLocationFix, 1000);
                HereLocationFixManager.this.firstLocationFixCheck = false;
            }
        }
    };
    private long counter;
    private LocationListener debugTTSLocationListener = null;
    private boolean firstLocationFixCheck = true;
    private GeoPosition geoPosition;
    private Handler handler = new Handler(Looper.getMainLooper());
    private volatile boolean hereGpsSignal;
    private volatile boolean isRecording;
    private Location lastAndroidLocation;
    private long lastAndroidLocationPostTime;
    private long lastAndroidLocationTime;
    private GpsSwitch lastGpsSwitchEvent;
    private long lastHerelocationUpdateTime;
    private volatile long lastLocationTime;
    private volatile boolean locationFix;
    private final LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            if (!HereLocationFixManager.this.locationFix) {
                HereLocationFixManager.this.setMaptoLocation(location);
            }
            if (GpsUtils.isDebugRawGpsPosEnabled()) {
                try {
                    if (HereLocationFixManager.this.rawLocationMarker != null && HereLocationFixManager.this.rawLocationMarkerAdded) {
                        HereLocationFixManager.this.rawLocationMarker.setCoordinate(new GeoCoordinate(location.getLatitude(), location.getLongitude()));
                    }
                } catch (Throwable t) {
                    HereLocationFixManager.sLogger.e(t);
                }
            }
            if (HereLocationFixManager.this.isRecording) {
                HereLocationFixManager.this.recordRawLocation(location);
            }
            HereLocationFixManager.this.lastAndroidLocationTime = SystemClock.elapsedRealtime();
            HereLocationFixManager.this.lastAndroidLocation = location;
            if (HereLocationFixManager.this.lastAndroidLocationTime - HereLocationFixManager.this.lastAndroidLocationPostTime >= 1000) {
                if (!TextUtils.equals(location.getProvider(), GpsConstants.NAVDY_GPS_PROVIDER)) {
                    HereLocationFixManager.this.sendLocation(location);
                }
                HereLocationFixManager.this.lastAndroidLocationPostTime = HereLocationFixManager.this.lastAndroidLocationTime;
            }
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onProviderDisabled(String provider) {
        }
    };
    private LocationMethod locationMethod;
    private MapMarker rawLocationMarker;
    private boolean rawLocationMarkerAdded;
    private FileOutputStream recordingFile;
    private volatile String recordingLabel;
    private final SpeedManager speedManager;

    HereLocationFixManager(Bus bus) {
        this.bus = bus;
        this.speedManager = SpeedManager.getInstance();
        final LocationManager locationManager = (LocationManager) HudApplication.getAppContext().getSystemService("location");
        Looper locationReceiverLooper = HereMapsManager.getInstance().getBkLocationReceiverLooper();
        if (TTSUtils.isDebugTTSEnabled() && DeviceUtil.isNavdyDevice() && locationManager.getProvider("gps") != null) {
            final long elapsedRealtime = SystemClock.elapsedRealtime();
            this.debugTTSLocationListener = new LocationListener() {
                boolean toastSent = false;

                public void onLocationChanged(Location location) {
                    if (this.toastSent) {
                        HereLocationFixManager.sLogger.v("ublox first fix toast already posted");
                        return;
                    }
                    locationManager.removeUpdates(this);
                    this.toastSent = true;
                    long t2 = SystemClock.elapsedRealtime() - elapsedRealtime;
                    HereLocationFixManager.sLogger.v("got first u-blox fix, unregister (" + t2 + HereManeuverDisplayBuilder.CLOSE_BRACKET);
                    if (t2 < 10000) {
                        HereLocationFixManager.this.handler.postDelayed(new Runnable() {
                            public void run() {
                                TTSUtils.debugShowGotUbloxFix();
                            }
                        }, 10000 - t2);
                    } else {
                        TTSUtils.debugShowGotUbloxFix();
                    }
                }

                public void onStatusChanged(String provider, int status, Bundle extras) {
                }

                public void onProviderEnabled(String provider) {
                }

                public void onProviderDisabled(String provider) {
                }
            };
            locationManager.requestLocationUpdates("gps", 0, 0.0f, this.debugTTSLocationListener, locationReceiverLooper);
        }
        Location last = null;
        if (locationManager.getProvider(GpsConstants.NAVDY_GPS_PROVIDER) != null) {
            last = locationManager.getLastKnownLocation(GpsConstants.NAVDY_GPS_PROVIDER);
            locationManager.requestLocationUpdates(GpsConstants.NAVDY_GPS_PROVIDER, 0, 0.0f, this.locationListener, locationReceiverLooper);
            sLogger.v("installed gps listener");
        }
        if (locationManager.getProvider("gps") != null) {
            this.androidGpsLocationListener = new LocationListener() {
                public void onLocationChanged(Location location) {
                    if (HereLocationFixManager.this.isRecording && !DeviceUtil.isNavdyDevice()) {
                        HereLocationFixManager.this.recordRawLocation(location);
                    }
                    HereLocationFixManager.this.sendLocation(location);
                }

                public void onStatusChanged(String provider, int status, Bundle extras) {
                }

                public void onProviderEnabled(String provider) {
                }

                public void onProviderDisabled(String provider) {
                }
            };
            locationManager.requestLocationUpdates("gps", 0, 0.0f, this.androidGpsLocationListener, locationReceiverLooper);
        }
        sLogger.v("installed gps listener");
        if (locationManager.getProvider("network") != null) {
            if (last == null) {
                last = locationManager.getLastKnownLocation("network");
            }
            locationManager.requestLocationUpdates("network", 0, 0.0f, this.locationListener, locationReceiverLooper);
            sLogger.v("installed n/w listener");
        }
        sLogger.v("got last location from location manager:" + last);
        if (!(this.locationFix || last == null)) {
            sLogger.v("sending to map");
            setMaptoLocation(last);
        }
        this.bus.register(this);
        this.handler.postDelayed(this.checkLocationFix, 1000);
        StartDriveRecordingEvent event = RemoteDeviceManager.getInstance().getConnectionHandler().getDriverRecordingEvent();
        if (event != null) {
            onStartDriveRecording(event);
        }
        sLogger.v("initialized");
    }

    public void onHerePositionUpdated(LocationMethod locationMethod, GeoPosition geoPosition, boolean isMapMatched) {
        if (sLogger.isLoggable(2)) {
            Logger logger = sLogger;
            StringBuilder append = new StringBuilder().append("onHerePositionUpdated [");
            long j = this.counter + 1;
            this.counter = j;
            logger.v(append.append(j).append("] geoPosition[").append(geoPosition.getCoordinate()).append("] method [").append(locationMethod.name()).append("] valid[").append(geoPosition.isValid()).append("]").toString());
        }
        if (locationMethod != null && geoPosition != null) {
            this.locationMethod = locationMethod;
            this.geoPosition = geoPosition;
            this.lastHerelocationUpdateTime = SystemClock.elapsedRealtime();
            if (this.isRecording) {
                recordHereLocation(geoPosition, locationMethod, isMapMatched);
            }
        }
    }

    public boolean hasLocationFix() {
        return this.locationFix;
    }

    public GeoCoordinate getLastGeoCoordinate() {
        if (this.geoPosition == null) {
            return null;
        }
        return this.geoPosition.getCoordinate();
    }

    @Subscribe
    public void onHereGpsEvent(GpsStatusChange event) {
        this.hereGpsSignal = event.connected;
        sLogger.v("here gps event connected [" + this.hereGpsSignal + "]");
    }

    @Subscribe
    public void onGpsEventSwitch(GpsSwitch event) {
        sLogger.v("Gps-Switch phone=" + event.usingPhone + " ublox=" + event.usingUblox);
        this.lastGpsSwitchEvent = event;
    }

    public void setMaptoLocation(Location location) {
        try {
            if (!this.locationFix && this.geoPosition == null) {
                sLogger.v("marking location fix:" + location);
                this.geoPosition = new GeoPosition(new GeoCoordinate(location.getLatitude(), location.getLongitude(), location.getAltitude()));
                this.locationMethod = LocationMethod.NETWORK;
                this.lastHerelocationUpdateTime = SystemClock.elapsedRealtime();
                this.locationFix = true;
                this.bus.post(new LocationFix(true, true, false));
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    @Subscribe
    public void onStartDriveRecording(final StartDriveRecordingEvent event) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                try {
                    if (HereLocationFixManager.this.isRecording) {
                        HereLocationFixManager.sLogger.v("already recording");
                        return;
                    }
                    HereLocationFixManager.this.recordingLabel = event.label;
                    HereLocationFixManager.this.isRecording = true;
                    File driveLogsDir = DriveRecorder.getDriveLogsDir(HereLocationFixManager.this.recordingLabel);
                    if (!driveLogsDir.exists()) {
                        driveLogsDir.mkdirs();
                    }
                    File driveLogFile = new File(driveLogsDir, HereLocationFixManager.this.recordingLabel + ".here");
                    HereLocationFixManager.this.recordingFile = new FileOutputStream(driveLogFile);
                    HereLocationFixManager.sLogger.v("created recording file [" + HereLocationFixManager.this.recordingLabel + "] path:" + driveLogFile.getAbsolutePath());
                } catch (Throwable t) {
                    HereLocationFixManager.sLogger.e(t);
                    HereLocationFixManager.this.isRecording = false;
                    HereLocationFixManager.this.recordingLabel = null;
                    IOUtils.closeStream(HereLocationFixManager.this.recordingFile);
                    HereLocationFixManager.this.recordingFile = null;
                }
            }
        }, 9);
    }

    @Subscribe
    public void onStopDriveRecording(StopDriveRecordingEvent event) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                try {
                    if (HereLocationFixManager.this.isRecording) {
                        HereLocationFixManager.this.isRecording = false;
                        IOUtils.fileSync(HereLocationFixManager.this.recordingFile);
                        IOUtils.closeStream(HereLocationFixManager.this.recordingFile);
                        HereLocationFixManager.this.recordingFile = null;
                        HereLocationFixManager.sLogger.v("stopped recording file [" + HereLocationFixManager.this.recordingLabel + "]");
                        HereLocationFixManager.this.recordingLabel = null;
                        return;
                    }
                    HereLocationFixManager.sLogger.v("not recording");
                } catch (Throwable t) {
                    HereLocationFixManager.sLogger.e(t);
                }
            }
        }, 9);
    }

    private void recordRawLocation(final Location location) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                try {
                    String provider = location.getProvider();
                    if (GpsConstants.NAVDY_GPS_PROVIDER.equals(provider)) {
                        provider = "gps";
                    }
                    HereLocationFixManager.this.recordingFile.write((location.getLatitude() + HereManeuverDisplayBuilder.COMMA + location.getLongitude() + HereManeuverDisplayBuilder.COMMA + location.getBearing() + HereManeuverDisplayBuilder.COMMA + location.getSpeed() + HereManeuverDisplayBuilder.COMMA + location.getAccuracy() + HereManeuverDisplayBuilder.COMMA + location.getAltitude() + HereManeuverDisplayBuilder.COMMA + location.getTime() + HereManeuverDisplayBuilder.COMMA + "raw" + HereManeuverDisplayBuilder.COMMA + provider + GlanceConstants.NEWLINE).getBytes());
                } catch (Throwable t) {
                    HereLocationFixManager.sLogger.e(t);
                }
            }
        }, 9);
    }

    private void recordHereLocation(final GeoPosition geoPosition, final LocationMethod locationMethod, final boolean isMapMatched) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                try {
                    GeoCoordinate coordinate = geoPosition.getCoordinate();
                    String additional = "";
                    if (geoPosition instanceof MatchedGeoPosition) {
                        MatchedGeoPosition matchedGeoPosition = geoPosition;
                        additional = HereManeuverDisplayBuilder.COMMA + matchedGeoPosition.getMatchQuality() + HereManeuverDisplayBuilder.COMMA + matchedGeoPosition.isExtrapolated() + HereManeuverDisplayBuilder.COMMA + matchedGeoPosition.isOnStreet();
                    }
                    HereLocationFixManager.this.recordingFile.write((coordinate.getLatitude() + HereManeuverDisplayBuilder.COMMA + coordinate.getLongitude() + HereManeuverDisplayBuilder.COMMA + geoPosition.getHeading() + HereManeuverDisplayBuilder.COMMA + geoPosition.getSpeed() + HereManeuverDisplayBuilder.COMMA + geoPosition.getLatitudeAccuracy() + HereManeuverDisplayBuilder.COMMA + coordinate.getAltitude() + HereManeuverDisplayBuilder.COMMA + geoPosition.getTimestamp().getTime() + HereManeuverDisplayBuilder.COMMA + "here" + HereManeuverDisplayBuilder.COMMA + locationMethod.name() + HereManeuverDisplayBuilder.COMMA + isMapMatched + additional + GlanceConstants.NEWLINE).getBytes());
                } catch (Throwable t) {
                    HereLocationFixManager.sLogger.e(t);
                }
            }
        }, 9);
    }

    public void recordMarker() {
        if (this.isRecording) {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    try {
                        HereLocationFixManager.this.recordingFile.write(HereLocationFixManager.RECORD_MARKER);
                    } catch (Throwable t) {
                        HereLocationFixManager.sLogger.e(t);
                    }
                }
            }, 9);
        }
    }

    public boolean isRecording() {
        return this.isRecording;
    }

    public void addMarkers(HereMapController mapController) {
        if (GpsUtils.isDebugRawGpsPosEnabled()) {
            try {
                if (this.rawLocationMarker == null) {
                    this.rawLocationMarker = new MapMarker();
                    Image image = new Image();
                    image.setImageResource(R.drawable.icon_position_raw_gps);
                    this.rawLocationMarker.setIcon(image);
                    sLogger.v("marker created");
                }
                if (!this.rawLocationMarkerAdded) {
                    mapController.addMapObject(this.rawLocationMarker);
                    this.rawLocationMarkerAdded = true;
                    sLogger.v("marker added");
                }
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    public void removeMarkers(HereMapController mapController) {
        if (GpsUtils.isDebugRawGpsPosEnabled() && this.rawLocationMarker != null && this.rawLocationMarkerAdded) {
            mapController.removeMapObject(this.rawLocationMarker);
            this.rawLocationMarkerAdded = false;
            sLogger.v("marker removed");
        }
    }

    public void shutdown() {
        LocationManager locationManager = (LocationManager) HudApplication.getAppContext().getSystemService("location");
        locationManager.removeUpdates(this.locationListener);
        if (this.androidGpsLocationListener != null) {
            locationManager.removeUpdates(this.androidGpsLocationListener);
        }
        if (this.debugTTSLocationListener != null) {
            locationManager.removeUpdates(this.debugTTSLocationListener);
        }
    }

    public LocationFix getLocationFixEvent() {
        if (!this.locationFix) {
            return new LocationFix(false, false, false);
        }
        boolean phone = false;
        boolean gps = false;
        if (this.lastAndroidLocation != null) {
            if (TextUtils.equals(this.lastAndroidLocation.getProvider(), GpsConstants.NAVDY_GPS_PROVIDER)) {
                gps = true;
            } else {
                phone = true;
            }
        }
        return new LocationFix(true, phone, gps);
    }

    private void sendLocation(Location location) {
        if (this.speedManager.setGpsSpeed(location.getSpeed(), location.getElapsedRealtimeNanos() / 1000000)) {
            this.bus.post(new GPSSpeedEvent());
        }
        this.lastLocationTime = SystemClock.elapsedRealtime();
        this.bus.post(location);
    }

    public long getLastLocationTime() {
        return this.lastLocationTime;
    }
}
