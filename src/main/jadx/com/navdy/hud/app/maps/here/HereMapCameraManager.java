package com.navdy.hud.app.maps.here;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.GeoPosition;
import com.here.android.mpa.mapping.Map.Animation;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.manager.SpeedManager.SpeedUnit;
import com.navdy.hud.app.maps.MapEvents.DialMapZoom;
import com.navdy.hud.app.maps.MapEvents.DialMapZoom.Type;
import com.navdy.hud.app.maps.MapEvents.LocationFix;
import com.navdy.hud.app.maps.MapEvents.MapUIReady;
import com.navdy.hud.app.maps.here.HereMapController.State;
import com.navdy.hud.app.profile.DriverProfile;
import com.navdy.hud.app.ui.component.homescreen.HomeScreenView;
import com.navdy.hud.app.ui.component.homescreen.NavigationView;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.service.library.events.preferences.LocalPreferences;
import com.navdy.service.library.events.preferences.LocalPreferences.Builder;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

public final class HereMapCameraManager {
    public static final float DEFAULT_TILT = 60.0f;
    public static final double DEFAULT_ZOOM_LEVEL = 16.5d;
    private static final int EASE_OUT_DYNAMIC_ZOOM_TILT_INTERVAL = 10000;
    private static final double HIGH_SPEED_THRESHOLD = 29.0576d;
    private static final float HIGH_SPEED_TILT = 70.0f;
    private static final double HIGH_SPEED_ZOOM = 15.25d;
    private static final double LOW_SPEED_THRESHOLD = 8.9408d;
    private static final float LOW_SPEED_TILT = 60.0f;
    private static final double LOW_SPEED_ZOOM = 16.5d;
    private static final double MAX_ZOOM_LEVEL_DIAL = 16.5d;
    private static final double MEDIUM_SPEED_THRESHOLD = 20.1168d;
    private static final double MEDIUM_SPEED_ZOOM = 16.0d;
    private static final double MIN_SPEED_DIFFERENCE = 1.5d;
    private static final float MIN_TILT_DIFFERENCE_TRIGGER = 5.0f;
    private static final double MIN_ZOOM_DIFFERENCE_TRIGGER = 0.25d;
    public static final double MIN_ZOOM_LEVEL_DIAL = 12.0d;
    private static final double MPH_TO_MS = 0.44704d;
    private static final int N_ZOOM_TILT_ANIMATION_STEPS = 5;
    private static final float OVERVIEW_MAP_ZOOM_LEVEL = 10000.0f;
    private static final double VERY_HIGH_SPEED_THRESHOLD = 33.528d;
    private static final double VERY_HIGH_SPEED_ZOOM = 14.75d;
    private static final long ZOOM_LOCK_DURATION = 10000;
    private static final double ZOOM_STEP = 0.25d;
    private static final int ZOOM_TILT_ANIMATION_INTERVAL = 200;
    private static HereMapCameraManager sInstance = new HereMapCameraManager();
    private static final Logger sLogger = new Logger(HereMapCameraManager.class);
    private volatile boolean animationOn;
    private float currentDynamicTilt;
    private double currentDynamicZoom;
    private GeoCoordinate currentGeoCoordinate;
    private GeoPosition currentGeoPosition;
    private Runnable easeOutDynamicZoomTilt = new Runnable() {
        public void run() {
            TaskManager.getInstance().execute(HereMapCameraManager.this.easeOutDynamicZoomTiltBk, 16);
        }
    };
    private Runnable easeOutDynamicZoomTiltBk = new Runnable() {
        public void run() {
            if (HereMapCameraManager.this.zoomActionOn) {
                HereMapCameraManager.sLogger.v("easeout user zoom action is on");
                return;
            }
            boolean isSignificantZoomDiff;
            boolean isSignificantTiltDiff;
            if (Math.abs(HereMapCameraManager.this.mapController.getZoomLevel() - HereMapCameraManager.this.currentDynamicZoom) >= 0.25d) {
                isSignificantZoomDiff = true;
            } else {
                isSignificantZoomDiff = false;
            }
            if (Math.abs(HereMapCameraManager.this.mapController.getTilt() - HereMapCameraManager.this.currentDynamicTilt) > HereMapCameraManager.MIN_TILT_DIFFERENCE_TRIGGER) {
                isSignificantTiltDiff = true;
            } else {
                isSignificantTiltDiff = false;
            }
            if (isSignificantZoomDiff || isSignificantTiltDiff) {
                HereMapCameraManager.this.lastDynamicZoomTime = SystemClock.elapsedRealtime();
                HereMapCameraManager.this.animateCamera();
                HereMapCameraManager.sLogger.v("easeout zoom=" + isSignificantZoomDiff + " tilt=" + isSignificantTiltDiff + " current-zoom=" + HereMapCameraManager.this.currentDynamicZoom + " current-tilt=" + HereMapCameraManager.this.currentDynamicTilt);
                return;
            }
            HereMapCameraManager.sLogger.v("easeout not significant current-zoom=" + HereMapCameraManager.this.currentDynamicZoom + " current-tilt=" + HereMapCameraManager.this.currentDynamicTilt);
        }
    };
    private volatile boolean goBackfromOverviewOn;
    private volatile boolean goToOverviewOn;
    private final Handler handler = new Handler(Looper.getMainLooper());
    private HereMapAnimator hereMapAnimator;
    private final HereNavigationManager hereNavigationManager = HereNavigationManager.getInstance();
    private HomeScreenView homeScreenView;
    private boolean init;
    private boolean initialZoom;
    private boolean isFullAnimationSettingEnabled;
    private long lastDynamicZoomTime;
    private double lastRecordedSpeed;
    private LocalPreferences localPreferences;
    private HereMapController mapController;
    private boolean mapUIReady;
    private volatile int nTimesAnimated;
    private NavigationView navigationView;
    private SpeedManager speedManager = SpeedManager.getInstance();
    private Runnable unlockZoom = new Runnable() {
        public void run() {
            HereMapCameraManager.this.handler.removeCallbacks(HereMapCameraManager.this.easeOutDynamicZoomTilt);
            HereMapCameraManager.this.zoomActionOn = false;
            if (HereMapCameraManager.this.mapController.getState() == State.ROUTE_PICKER) {
                HereMapCameraManager.sLogger.v("unlocking: in route search, no-op");
            } else {
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        if (HereMapCameraManager.this.isManualZoom()) {
                            float zoomLevel;
                            HereMapCameraManager.sLogger.v("unlocking dial zoom:manual");
                            if (HereMapCameraManager.this.isOverViewMapVisible()) {
                                zoomLevel = HereMapCameraManager.OVERVIEW_MAP_ZOOM_LEVEL;
                            } else {
                                zoomLevel = (float) HereMapCameraManager.this.mapController.getZoomLevel();
                            }
                            HereMapCameraManager.this.localPreferences = new Builder(HereMapCameraManager.this.localPreferences).manualZoomLevel(Float.valueOf(zoomLevel)).build();
                            DriverProfileHelper.getInstance().getDriverProfileManager().updateLocalPreferences(HereMapCameraManager.this.localPreferences);
                            HereMapCameraManager.sLogger.v("unlocking dial zoom:manual pref saved");
                            return;
                        }
                        HereMapCameraManager.sLogger.v("unlocking dial zoom:auto");
                        HereMapCameraManager.this.zoomToDefault();
                    }
                }, 2);
            }
        }
    };
    private volatile boolean zoomActionOn;

    /* renamed from: com.navdy.hud.app.maps.here.HereMapCameraManager$9 */
    static /* synthetic */ class AnonymousClass9 {
        static final /* synthetic */ int[] $SwitchMap$com$navdy$hud$app$maps$MapEvents$DialMapZoom$Type = new int[Type.values().length];
        static final /* synthetic */ int[] $SwitchMap$com$navdy$hud$app$maps$here$HereMapController$State = new int[State.values().length];

        static {
            try {
                $SwitchMap$com$navdy$hud$app$maps$MapEvents$DialMapZoom$Type[Type.IN.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$maps$MapEvents$DialMapZoom$Type[Type.OUT.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$maps$here$HereMapController$State[State.OVERVIEW.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$maps$here$HereMapController$State[State.AR_MODE.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
        }
    }

    public static HereMapCameraManager getInstance() {
        return sInstance;
    }

    private HereMapCameraManager() {
    }

    public synchronized void initialize(HereMapController mapController, Bus bus, HereMapAnimator hereMapAnimator) {
        boolean z = true;
        synchronized (this) {
            if (!this.init) {
                this.mapController = mapController;
                this.hereMapAnimator = hereMapAnimator;
                if (hereMapAnimator.getAnimationMode() != AnimationMode.ZOOM_TILT_ANIMATION) {
                    z = false;
                }
                this.isFullAnimationSettingEnabled = z;
                DriverProfile profile = DriverProfileHelper.getInstance().getCurrentProfile();
                this.localPreferences = profile.getLocalPreferences();
                sLogger.v("localPreferences [" + profile.getProfileName() + "] " + this.localPreferences);
                setZoom();
                this.currentDynamicZoom = 16.5d;
                this.currentDynamicTilt = 60.0f;
                bus.register(this);
                this.init = true;
            }
        }
    }

    public boolean setZoom() {
        if (!this.init) {
            return false;
        }
        if (this.currentGeoPosition == null || this.localPreferences == null) {
            return false;
        }
        if (!getNavigationView()) {
            return false;
        }
        State state = this.mapController.getState();
        if (isManualZoom()) {
            sLogger.v("setZoom: manual: " + state);
            double level = (double) this.localPreferences.manualZoomLevel.floatValue();
            if (level != 10000.0d) {
                sLogger.v("setZoom: normal zoom level: " + level);
                if (level < 12.0d || level > 16.5d) {
                    double prevLevel = level;
                    level = 16.5d;
                    sLogger.w("setZoom: normal zoom level changed from [" + prevLevel + "] to [" + 16.5d + "]");
                }
                if (this.mapController.getState() == State.OVERVIEW || this.mapController.getState() == State.TRANSITION) {
                    sLogger.v("setZoom: normal zoom level, showRouteMap");
                    this.navigationView.showRouteMap(this.currentGeoPosition, level, this.currentDynamicTilt);
                } else if (this.mapController.getState() == State.AR_MODE) {
                    this.mapController.setCenter(this.currentGeoPosition.getCoordinate(), Animation.NONE, level, (float) this.currentGeoPosition.getHeading(), this.currentDynamicTilt);
                    sLogger.v("setZoom: normal zoom level, already in route map, setcenter");
                } else {
                    sLogger.v("setZoom: normal zoom level, incorrect state:" + state);
                }
            } else if (state == State.AR_MODE) {
                sLogger.v("setZoom: overview map zoom level, showOverviewMap");
                this.navigationView.showOverviewMap(this.hereNavigationManager.getCurrentRoute(), this.hereNavigationManager.getCurrentMapRoute(), this.currentGeoPosition.getCoordinate(), this.hereNavigationManager.hasArrived());
            } else {
                sLogger.v("setZoom: overview map zoom level, not in ar mode:" + state + ", set to OVERVIEW");
                this.mapController.setState(State.OVERVIEW);
                this.navigationView.showStartMarker();
            }
        } else {
            sLogger.v("setZoom: automatic: " + state);
            if (state == State.OVERVIEW || state == State.TRANSITION) {
                sLogger.v("setZoom:automatic showRouteMap");
                this.navigationView.showRouteMap(this.currentGeoPosition, this.currentDynamicZoom, this.currentDynamicTilt);
            } else if (this.mapController.getState() == State.AR_MODE) {
                sLogger.v("setZoom:automatic already in route map, setCenter");
                this.mapController.setCenter(this.currentGeoPosition.getCoordinate(), Animation.NONE, this.currentDynamicZoom, (float) this.currentGeoPosition.getHeading(), this.currentDynamicTilt);
            } else {
                sLogger.v("setZoom:automatic incorrect state:" + state);
            }
        }
        return true;
    }

    public void onGeoPositionChange(GeoPosition geoPosition) {
        this.currentGeoPosition = geoPosition;
        this.currentGeoCoordinate = geoPosition.getCoordinate();
        if (this.localPreferences.manualZoom == null || !this.localPreferences.manualZoom.booleanValue()) {
            double speed = (double) this.speedManager.getObdSpeed();
            if (speed != -1.0d) {
                speed = (double) SpeedManager.convert(speed, this.speedManager.getSpeedUnit(), SpeedUnit.METERS_PER_SECOND);
            } else {
                speed = this.currentGeoPosition.getSpeed();
            }
            if (speed != this.lastRecordedSpeed) {
                double lastSpeed = this.lastRecordedSpeed;
                boolean isSignificantSpeedDiff = Math.abs(speed - this.lastRecordedSpeed) >= MIN_SPEED_DIFFERENCE;
                this.lastRecordedSpeed = speed;
                if (isSignificantSpeedDiff) {
                    this.handler.removeCallbacks(this.easeOutDynamicZoomTilt);
                    this.currentDynamicZoom = getDynamicZoom(speed);
                    this.currentDynamicTilt = getDynamicTilt(speed);
                    if (!this.zoomActionOn) {
                        boolean isSignificantZoomDiff = Math.abs(this.mapController.getZoomLevel() - this.currentDynamicZoom) >= 0.25d;
                        boolean isSignificantTiltDiff = Math.abs(this.mapController.getTilt() - this.currentDynamicTilt) > MIN_TILT_DIFFERENCE_TRIGGER;
                        if (isSignificantZoomDiff || isSignificantTiltDiff) {
                            long lastZoomInterval = SystemClock.elapsedRealtime() - this.lastDynamicZoomTime;
                            if (lastZoomInterval < ZOOM_LOCK_DURATION) {
                                long timeLeft = ZOOM_LOCK_DURATION - lastZoomInterval;
                                this.handler.postDelayed(this.easeOutDynamicZoomTilt, timeLeft);
                                if (sLogger.isLoggable(2)) {
                                    sLogger.v("significant too early =" + lastZoomInterval + " scheduled =" + timeLeft);
                                    return;
                                }
                                return;
                            }
                            sLogger.v("significant zoom=" + isSignificantZoomDiff + " tilt=" + isSignificantTiltDiff + " current-zoom=" + this.currentDynamicZoom + " current-tilt=" + this.currentDynamicTilt + " speed = " + speed + " lastSpeed=" + lastSpeed);
                            this.lastDynamicZoomTime = SystemClock.elapsedRealtime();
                            animateCamera();
                        }
                    }
                }
            } else if (sLogger.isLoggable(2)) {
                sLogger.v("speed not changed :" + speed);
            }
        }
    }

    @Subscribe
    public void onLocalPreferencesChanged(LocalPreferences preferences) {
        if (preferences.equals(this.localPreferences)) {
            sLogger.v("localPref not changed:" + preferences);
            return;
        }
        sLogger.v("localPref changed:" + preferences);
        this.localPreferences = preferences;
        setZoom();
    }

    @Subscribe
    public void onDriverProfileChanged(DriverProfileChanged event) {
        DriverProfile profile = DriverProfileHelper.getInstance().getCurrentProfile();
        this.localPreferences = profile.getLocalPreferences();
        sLogger.v("driver changed[" + profile.getProfileName() + "] " + this.localPreferences);
        setZoom();
    }

    @Subscribe
    public void onLocationFixChangeEvent(LocationFix event) {
        if (this.init && this.mapUIReady) {
            setInitialZoom();
        }
    }

    @Subscribe
    public void onMapUIReady(MapUIReady event) {
        this.mapUIReady = true;
        setInitialZoom();
    }

    @Subscribe
    public void onDialZoom(final DialMapZoom event) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                try {
                    switch (AnonymousClass9.$SwitchMap$com$navdy$hud$app$maps$here$HereMapController$State[HereMapCameraManager.this.mapController.getState().ordinal()]) {
                        case 1:
                        case 2:
                            GeoCoordinate c;
                            if (HereMapCameraManager.this.currentGeoPosition == null) {
                                c = HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
                            } else {
                                c = HereMapCameraManager.this.currentGeoPosition.getCoordinate();
                            }
                            if (c == null) {
                                HereMapCameraManager.sLogger.i("no location");
                                return;
                            }
                            final GeoCoordinate coordinate = c;
                            if (!HereMapCameraManager.this.animationOn && !HereMapCameraManager.this.goBackfromOverviewOn && !HereMapCameraManager.this.goToOverviewOn && (!HereMapCameraManager.this.isOverViewMapVisible() || event.type != Type.OUT)) {
                                double currentMapZoom = HereMapCameraManager.this.mapController.getZoomLevel();
                                double zoomStep = 0.0d;
                                HereMapCameraManager.this.zoomActionOn = true;
                                HereMapCameraManager.this.handler.removeCallbacks(HereMapCameraManager.this.unlockZoom);
                                switch (AnonymousClass9.$SwitchMap$com$navdy$hud$app$maps$MapEvents$DialMapZoom$Type[event.type.ordinal()]) {
                                    case 1:
                                        if (HereMapCameraManager.this.isOverViewMapVisible()) {
                                            HereMapCameraManager.this.goToOverviewOn = false;
                                            if (!HereMapCameraManager.this.goBackfromOverviewOn) {
                                                HereMapCameraManager.sLogger.v("goBackfromOverview:on");
                                                HereMapCameraManager.this.goBackfromOverviewOn = true;
                                                HereMapCameraManager.this.handler.post(new Runnable() {
                                                    public void run() {
                                                        HereMapCameraManager.this.goBackFromOverview(new Runnable() {
                                                            public void run() {
                                                                HereMapCameraManager.sLogger.v("goBackfromOverview:complete");
                                                                HereMapCameraManager.this.goBackfromOverviewOn = false;
                                                            }
                                                        });
                                                        HereMapCameraManager.this.handler.postDelayed(HereMapCameraManager.this.unlockZoom, HereMapCameraManager.ZOOM_LOCK_DURATION);
                                                    }
                                                });
                                                return;
                                            }
                                            return;
                                        } else if (!HereMapCameraManager.this.goBackfromOverviewOn) {
                                            zoomStep = 0.25d;
                                            break;
                                        } else {
                                            return;
                                        }
                                    case 2:
                                        zoomStep = -0.25d;
                                        break;
                                }
                                double newZoomLevel = currentMapZoom + zoomStep;
                                if (currentMapZoom < 16.5d && newZoomLevel >= 16.5d) {
                                    newZoomLevel = 16.5d;
                                }
                                boolean isInZoomRange = newZoomLevel <= 16.5d && newZoomLevel >= 12.0d;
                                boolean enteredOverviewRange = currentMapZoom + zoomStep < 12.0d;
                                if (isInZoomRange) {
                                    long l1 = SystemClock.elapsedRealtime();
                                    if (HereMapCameraManager.this.isFullAnimationSettingEnabled) {
                                        HereMapCameraManager.this.hereMapAnimator.setZoom(newZoomLevel);
                                    } else {
                                        HereMapCameraManager.this.mapController.setCenter(coordinate, Animation.NONE, newZoomLevel, -1.0f, -1.0f);
                                    }
                                    HereMapCameraManager.sLogger.v("zoomLevel:" + newZoomLevel + " time:" + (SystemClock.elapsedRealtime() - l1));
                                } else if (enteredOverviewRange) {
                                    HereMapCameraManager.this.goBackfromOverviewOn = false;
                                    HereMapCameraManager.this.goToOverviewOn = true;
                                    HereMapCameraManager.sLogger.v("goToOverviewOn:on");
                                    HereMapCameraManager.this.handler.post(new Runnable() {
                                        public void run() {
                                            HereMapCameraManager.this.goToOverview(coordinate, new Runnable() {
                                                public void run() {
                                                    HereMapCameraManager.this.goToOverviewOn = false;
                                                    HereMapCameraManager.sLogger.v("goToOverviewOn:complete");
                                                }
                                            });
                                        }
                                    });
                                    HereMapCameraManager.sLogger.v("zoomLevel: overview");
                                }
                                HereMapCameraManager.this.handler.postDelayed(HereMapCameraManager.this.unlockZoom, HereMapCameraManager.ZOOM_LOCK_DURATION);
                                return;
                            } else if (HereMapCameraManager.sLogger.isLoggable(2)) {
                                Logger access$400 = HereMapCameraManager.sLogger;
                                StringBuilder append = new StringBuilder().append("onDialZoom no-op, animationOn: ").append(HereMapCameraManager.this.animationOn).append(" goBackfromOverviewOn:").append(HereMapCameraManager.this.goBackfromOverviewOn).append(" goToOverviewOn:").append(HereMapCameraManager.this.goToOverviewOn).append(" overviewMap on-out:");
                                boolean z = HereMapCameraManager.this.isOverViewMapVisible() && event.type == Type.OUT;
                                access$400.v(append.append(z).toString());
                                return;
                            } else {
                                return;
                            }
                        default:
                            return;
                    }
                } catch (Throwable t) {
                    HereMapCameraManager.sLogger.e("onDialZoom", t);
                }
                HereMapCameraManager.sLogger.e("onDialZoom", t);
            }
        }, 16);
    }

    private void setInitialZoom() {
        if (!this.initialZoom) {
            sLogger.v("setInitialZoom:" + this.currentGeoPosition);
            if (this.currentGeoPosition == null) {
                GeoCoordinate coordinate = HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
                if (coordinate != null) {
                    this.currentGeoPosition = new GeoPosition(coordinate);
                    this.currentGeoCoordinate = coordinate;
                } else {
                    return;
                }
            }
            this.initialZoom = setZoom();
            sLogger.v("setInitialZoom:" + this.initialZoom);
        }
    }

    private void animateCamera() {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                if (HereMapCameraManager.this.isFullAnimationSettingEnabled) {
                    HereMapCameraManager.this.hereMapAnimator.setZoom(HereMapCameraManager.this.currentDynamicZoom);
                    HereMapCameraManager.this.hereMapAnimator.setTilt(HereMapCameraManager.this.currentDynamicTilt);
                    return;
                }
                HereMapCameraManager.this.nTimesAnimated = 0;
                HereMapCameraManager.this.animationOn = true;
                HereMapCameraManager.this.handler.post(HereMapCameraManager.this.getAnimateCameraRunnable((HereMapCameraManager.this.currentDynamicZoom - HereMapCameraManager.this.mapController.getZoomLevel()) / 5.0d, (HereMapCameraManager.this.currentDynamicTilt - HereMapCameraManager.this.mapController.getTilt()) / HereMapCameraManager.MIN_TILT_DIFFERENCE_TRIGGER));
            }
        }, 2);
    }

    private void goToOverview(GeoCoordinate coordinate, Runnable endAction) {
        if (!getNavigationView()) {
            return;
        }
        if (this.hereNavigationManager.getCurrentRoute() != null) {
            this.navigationView.animateToOverview(coordinate, this.hereNavigationManager.getCurrentRoute(), this.hereNavigationManager.getCurrentMapRoute(), endAction, this.hereNavigationManager.hasArrived());
            return;
        }
        this.navigationView.animateToOverview(coordinate, null, null, endAction, false);
    }

    private void goBackFromOverview(final Runnable endAction) {
        if (getNavigationView()) {
            this.navigationView.animateBackfromOverview(new Runnable() {
                public void run() {
                    if (endAction != null) {
                        HereMapCameraManager.this.handler.post(endAction);
                    }
                }
            });
        }
    }

    private double getDynamicZoom(double speed) {
        if (speed <= LOW_SPEED_THRESHOLD) {
            return 16.5d;
        }
        if (speed > LOW_SPEED_THRESHOLD && speed <= MEDIUM_SPEED_THRESHOLD) {
            return getLinearMapSection(speed, LOW_SPEED_THRESHOLD, MEDIUM_SPEED_THRESHOLD, 16.5d, MEDIUM_SPEED_ZOOM);
        }
        if (speed > MEDIUM_SPEED_THRESHOLD && speed <= HIGH_SPEED_THRESHOLD) {
            return getLinearMapSection(speed, MEDIUM_SPEED_THRESHOLD, HIGH_SPEED_THRESHOLD, MEDIUM_SPEED_ZOOM, HIGH_SPEED_ZOOM);
        }
        if (speed <= HIGH_SPEED_THRESHOLD || speed > VERY_HIGH_SPEED_THRESHOLD) {
            return VERY_HIGH_SPEED_ZOOM;
        }
        return getLinearMapSection(speed, HIGH_SPEED_THRESHOLD, VERY_HIGH_SPEED_THRESHOLD, HIGH_SPEED_ZOOM, VERY_HIGH_SPEED_ZOOM);
    }

    private float getDynamicTilt(double speed) {
        if (speed <= LOW_SPEED_THRESHOLD) {
            return 60.0f;
        }
        if (speed <= LOW_SPEED_THRESHOLD || speed > HIGH_SPEED_THRESHOLD) {
            return HIGH_SPEED_TILT;
        }
        return (float) getLinearMapSection(speed, LOW_SPEED_THRESHOLD, HIGH_SPEED_THRESHOLD, 60.0d, 70.0d);
    }

    private double getLinearMapSection(double x, double xStart, double xEnd, double yStart, double yEnd) {
        return (((x - xStart) * (yEnd - yStart)) / (xEnd - xStart)) + yStart;
    }

    private void zoomToDefault() {
        if (!this.init) {
            return;
        }
        if (isManualZoom()) {
            sLogger.v("zoomToDefault: manual zoom enabled");
        } else if (!getNavigationView()) {
            sLogger.v("zoomToDefault: no view");
        } else if (this.navigationView.isOverviewMapMode()) {
            goBackFromOverview(new Runnable() {
                public void run() {
                    HereMapCameraManager.this.animateCamera();
                }
            });
        } else {
            animateCamera();
        }
    }

    public boolean isManualZoom() {
        return this.localPreferences != null && Boolean.TRUE.equals(this.localPreferences.manualZoom);
    }

    public boolean isOverviewZoomLevel() {
        return isManualZoom() && this.localPreferences.manualZoomLevel.floatValue() == OVERVIEW_MAP_ZOOM_LEVEL;
    }

    private boolean getNavigationView() {
        if (this.navigationView != null) {
            return true;
        }
        UIStateManager uiStateManager = RemoteDeviceManager.getInstance().getUiStateManager();
        this.navigationView = uiStateManager.getNavigationView();
        this.homeScreenView = uiStateManager.getHomescreenView();
        if (this.navigationView == null) {
            return false;
        }
        return true;
    }

    private boolean isOverViewMapVisible() {
        return getNavigationView() && this.navigationView.isOverviewMapMode();
    }

    private Runnable getAnimateCameraRunnable(final double zoomStep, final float tiltStep) {
        return new Runnable() {
            public void run() {
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        GeoCoordinate coordinate;
                        if (HereMapCameraManager.this.currentGeoPosition == null) {
                            coordinate = HereMapCameraManager.this.mapController.getCenter();
                        } else {
                            coordinate = HereMapCameraManager.this.currentGeoPosition.getCoordinate();
                        }
                        double newZoom = HereMapCameraManager.this.mapController.getZoomLevel() + zoomStep;
                        float newTilt = HereMapCameraManager.this.mapController.getTilt() + tiltStep;
                        if (newZoom > 16.5d) {
                            newZoom = 16.5d;
                        }
                        if (newTilt > HereMapCameraManager.HIGH_SPEED_TILT) {
                            newTilt = HereMapCameraManager.HIGH_SPEED_TILT;
                        }
                        HereMapCameraManager.this.mapController.setCenter(coordinate, Animation.NONE, newZoom, -1.0f, newTilt);
                        HereMapCameraManager.this.nTimesAnimated = HereMapCameraManager.this.nTimesAnimated + 1;
                        if (HereMapCameraManager.this.nTimesAnimated == 5) {
                            HereMapCameraManager.this.animationOn = false;
                            HereMapCameraManager.this.nTimesAnimated = 0;
                            return;
                        }
                        HereMapCameraManager.this.handler.postDelayed(HereMapCameraManager.this.getAnimateCameraRunnable(zoomStep, tiltStep), 200);
                    }
                }, 17);
            }
        };
    }

    public GeoPosition getLastGeoPosition() {
        return this.currentGeoPosition;
    }

    public GeoCoordinate getLastGeoCoordinate() {
        return this.currentGeoCoordinate;
    }

    public float getLastTilt() {
        return this.currentDynamicTilt;
    }

    public double getLastZoom() {
        if (!isManualZoom()) {
            return this.currentDynamicZoom;
        }
        if (this.localPreferences.manualZoomLevel.floatValue() == OVERVIEW_MAP_ZOOM_LEVEL) {
            return 12.0d;
        }
        return (double) this.localPreferences.manualZoomLevel.floatValue();
    }
}
