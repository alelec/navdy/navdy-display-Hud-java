package com.navdy.hud.app.maps.here;

import com.here.android.mpa.guidance.NavigationManager.NavigationManagerEventListener;
import com.here.android.mpa.guidance.NavigationManager.NavigationMode;
import com.here.android.mpa.routing.Route;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;

class HereNavigationEventListener extends NavigationManagerEventListener {
    private final HereNavigationManager hereNavigationManager;
    private final Logger logger;
    private final String tag;

    HereNavigationEventListener(Logger logger, String tag, HereNavigationManager hereNavigationManager) {
        this.logger = logger;
        this.tag = tag;
        this.hereNavigationManager = hereNavigationManager;
    }

    public void onNavigationModeChanged() {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                NavigationMode mode = HereNavigationEventListener.this.hereNavigationManager.getHereNavigationState();
                HereNavigationEventListener.this.logger.i(HereNavigationEventListener.this.tag + " onNavigationModeChanged HERE mode=" + mode + " Navdy=" + HereNavigationEventListener.this.hereNavigationManager.getNavigationState());
            }
        }, 2);
    }

    public void onRouteUpdated(Route updatedRoute) {
        this.logger.i(this.tag + " onRouteUpdated:" + updatedRoute.toString());
    }

    public void onEnded(NavigationMode mode) {
        this.logger.i(this.tag + " onEnded:" + mode);
        if (this.hereNavigationManager.isLastManeuver()) {
            this.logger.i(this.tag + " onEnded: last maneuver on, marking arrived");
            this.hereNavigationManager.arrived();
        }
    }
}
