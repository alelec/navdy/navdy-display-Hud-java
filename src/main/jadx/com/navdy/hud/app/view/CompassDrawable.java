package com.navdy.hud.app.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import com.navdy.hud.app.R;
import com.navdy.hud.app.view.drawable.GaugeDrawable;

public class CompassDrawable extends GaugeDrawable {
    private static final double SEGMENT_ANGLE = 22.5d;
    private static final int TEXT_PADDING = 6;
    private static final int TOTAL_SEGMENTS = 16;
    private static String[] headings;
    private static int majorTickHeight = 10;
    private static int minorTickHeight = 5;
    private static int[] widths;
    private Drawable mIndicatorDrawable;
    private int mIndicatorHeight;
    private int mIndicatorTopMargin;
    private int mIndicatorWidth;
    private Path mPath;
    private int mStripHeight;
    private int mStripTopMargin;
    private int mStripWidth;
    private Paint mTextPaint = new Paint();

    public CompassDrawable(Context context) {
        super(context, 0);
        Resources resources = context.getResources();
        this.mIndicatorDrawable = resources.getDrawable(R.drawable.icon_gauge_compass_heading_indicator);
        this.mIndicatorWidth = resources.getDimensionPixelSize(R.dimen.compass_indicator_width);
        this.mIndicatorHeight = resources.getDimensionPixelSize(R.dimen.compass_indicator_height);
        this.mIndicatorTopMargin = resources.getDimensionPixelSize(R.dimen.compass_indicator_top_margin);
        this.mStripWidth = resources.getDimensionPixelSize(R.dimen.compass_strip_width);
        this.mStripTopMargin = resources.getDimensionPixelSize(R.dimen.compass_strip_margin_top);
        this.mStripHeight = resources.getDimensionPixelSize(R.dimen.compass_strip_height);
        this.mPaint.setAntiAlias(true);
        this.mPaint.setStyle(Style.FILL);
        this.mPaint.setColor(resources.getColor(R.color.compass_frame_color));
        this.mTextPaint.setStrokeWidth(0.0f);
        this.mTextPaint.setColor(-1);
        this.mTextPaint.setAntiAlias(false);
        this.mTextPaint.setTextAlign(Align.LEFT);
        this.mTextPaint.setTypeface(Typeface.create("sans-serif", 1));
        this.mTextPaint.setTextSize((float) resources.getDimensionPixelSize(R.dimen.compass_text_height));
        this.mPath = new Path();
        Rect measuringRect = new Rect();
        if (headings == null) {
            majorTickHeight = resources.getDimensionPixelSize(R.dimen.compass_major_tick_height);
            minorTickHeight = resources.getDimensionPixelSize(R.dimen.compass_minor_tick_height);
            headings = resources.getStringArray(R.array.compass_points);
            widths = new int[headings.length];
            for (int i = 0; i < widths.length; i++) {
                this.mTextPaint.getTextBounds(headings[i], 0, headings[i].length(), measuringRect);
                widths[i] = measuringRect.width();
            }
        }
    }

    protected void onBoundsChange(Rect bounds) {
        super.onBoundsChange(bounds);
        this.mPath.reset();
        this.mPath.addOval(0.0f, 0.0f, (float) bounds.width(), (float) bounds.height(), Direction.CW);
    }

    private void drawCompassPoints(Canvas canvas, float heading, int left, int top, int right, int bottom, boolean showHeadings) {
        int width = right - left;
        float center = (float) ((width / 2) + left);
        int segmentVisualSize = this.mStripWidth / 16;
        int segmentsToDraw = ((int) Math.ceil((double) (width / segmentVisualSize))) + 1;
        float segment = (heading % 360.0f) / 22.5f;
        int tick = (int) segment;
        float tickOffset = segment - ((float) tick);
        int rightTick = segmentsToDraw / 2;
        for (int i = (-segmentsToDraw) / 2; i < rightTick; i++) {
            int x = (int) (((double) (((((float) i) + tickOffset) * ((float) segmentVisualSize)) + center)) + 0.5d);
            int tickIndex = ((tick - i) + 16) % 16;
            boolean majorTick = (tickIndex & 1) == 0;
            int height = majorTick ? majorTickHeight : minorTickHeight;
            this.mTextPaint.setAntiAlias(false);
            canvas.drawLine((float) x, (float) (bottom - height), (float) x, (float) bottom, this.mTextPaint);
            if (majorTick && showHeadings) {
                int headingIndex = tickIndex / 2;
                String text = headings[headingIndex];
                this.mTextPaint.setAntiAlias(true);
                canvas.drawText(text, (float) (x - (widths[headingIndex] / 2)), (float) ((bottom - height) - 6), this.mTextPaint);
            }
        }
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        Rect bounds = getBounds();
        int width = bounds.width();
        int height = bounds.height();
        canvas.clipRect(bounds);
        canvas.drawArc(0.0f, 0.0f, (float) width, (float) height, 0.0f, 360.0f, true, this.mPaint);
        int top = bounds.top + this.mIndicatorTopMargin;
        this.mIndicatorDrawable.setBounds((bounds.left + (width / 2)) - (this.mIndicatorWidth / 2), top, (bounds.left + (width / 2)) + (this.mIndicatorWidth / 2), top + this.mIndicatorHeight);
        this.mIndicatorDrawable.draw(canvas);
        canvas.clipPath(this.mPath);
        drawCompassPoints(canvas, this.mValue, 0, this.mStripTopMargin, width, this.mStripTopMargin + this.mStripHeight, this.mValue != 0.0f);
    }
}
