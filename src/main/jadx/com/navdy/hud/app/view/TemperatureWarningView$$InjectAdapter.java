package com.navdy.hud.app.view;

import com.navdy.hud.app.screen.TemperatureWarningScreen.Presenter;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class TemperatureWarningView$$InjectAdapter extends Binding<TemperatureWarningView> implements MembersInjector<TemperatureWarningView> {
    private Binding<Presenter> mPresenter;

    public TemperatureWarningView$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.view.TemperatureWarningView", false, TemperatureWarningView.class);
    }

    public void attach(Linker linker) {
        this.mPresenter = linker.requestBinding("com.navdy.hud.app.screen.TemperatureWarningScreen$Presenter", TemperatureWarningView.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mPresenter);
    }

    public void injectMembers(TemperatureWarningView object) {
        object.mPresenter = (Presenter) this.mPresenter.get();
    }
}
