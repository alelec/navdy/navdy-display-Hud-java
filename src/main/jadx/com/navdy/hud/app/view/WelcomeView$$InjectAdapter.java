package com.navdy.hud.app.view;

import com.navdy.hud.app.screen.WelcomeScreen.Presenter;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class WelcomeView$$InjectAdapter extends Binding<WelcomeView> implements MembersInjector<WelcomeView> {
    private Binding<Presenter> presenter;

    public WelcomeView$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.view.WelcomeView", false, WelcomeView.class);
    }

    public void attach(Linker linker) {
        this.presenter = linker.requestBinding("com.navdy.hud.app.screen.WelcomeScreen$Presenter", WelcomeView.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.presenter);
    }

    public void injectMembers(WelcomeView object) {
        object.presenter = (Presenter) this.presenter.get();
    }
}
