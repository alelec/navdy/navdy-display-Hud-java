package com.navdy.hud.app.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.music.AlbumArtImageView;
import com.navdy.hud.app.framework.music.OutlineTextView;
import com.navdy.hud.app.manager.MusicManager;
import com.navdy.hud.app.manager.MusicManager.MediaControl;
import com.navdy.hud.app.manager.MusicManager.MusicUpdateListener;
import com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager;
import com.navdy.service.library.events.audio.MusicTrackInfo;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.ScalingUtilities;
import com.navdy.service.library.util.ScalingUtilities.ScalingLogic;
import java.util.Set;
import javax.inject.Inject;
import mortar.Mortar;
import okio.ByteString;

public class MusicWidgetPresenter extends DashboardWidgetPresenter implements MusicUpdateListener {
    private static final float ARTWORK_ALPHA_NOT_PLAYING = 0.5f;
    private static final float ARTWORK_ALPHA_PLAYING = 1.0f;
    private static Logger logger = new Logger(MusicWidgetPresenter.class);
    private Bitmap albumArt;
    private boolean animateNextArtworkTransition;
    private OutlineTextView artistText;
    private Handler handler = new Handler();
    private AlbumArtImageView image;
    private CharSequence lastAlbumArtHash;
    private String musicGaugeName;
    @Inject
    MusicManager musicManager;
    private OutlineTextView stateText;
    private OutlineTextView titleText;
    private MusicTrackInfo trackInfo = null;

    public MusicWidgetPresenter(Context context) {
        this.musicGaugeName = context.getResources().getString(R.string.widget_music);
        Mortar.inject(HudApplication.getAppContext(), this);
    }

    public void setView(DashboardWidgetView dashboardWidgetView, Bundle arguments) {
        logger.d("setView " + dashboardWidgetView);
        if (dashboardWidgetView != null) {
            dashboardWidgetView.setContentView((int) R.layout.music_widget);
            this.titleText = (OutlineTextView) dashboardWidgetView.findViewById(R.id.txt_name);
            this.artistText = (OutlineTextView) dashboardWidgetView.findViewById(R.id.txt_author);
            this.stateText = (OutlineTextView) dashboardWidgetView.findViewById(R.id.txt_player_state);
            this.image = (AlbumArtImageView) dashboardWidgetView.findViewById(R.id.img_album_art);
        }
        super.setView(dashboardWidgetView);
    }

    public Drawable getDrawable() {
        return null;
    }

    protected void updateGauge() {
        this.image.setArtworkBitmap(this.albumArt, this.animateNextArtworkTransition);
        if (this.trackInfo == null || this.trackInfo == MusicManager.EMPTY_TRACK) {
            this.titleText.setText("");
            this.artistText.setText("");
            this.stateText.setVisibility(4);
            return;
        }
        if (MusicManager.tryingToPlay(this.trackInfo.playbackState)) {
            this.image.setAlpha(1.0f);
            this.stateText.setVisibility(4);
        } else {
            this.image.setAlpha(0.5f);
            this.stateText.setVisibility(0);
        }
        updateMetadata();
    }

    private void updateMetadata() {
        CharSequence currentTitle = this.titleText.getText();
        if (this.trackInfo.name == null || !this.trackInfo.name.equals(currentTitle)) {
            this.titleText.setText(this.trackInfo.name);
        }
        CharSequence currentArtist = this.artistText.getText();
        if (this.trackInfo.author == null || !this.trackInfo.author.equals(currentArtist)) {
            this.artistText.setText(this.trackInfo.author);
        }
    }

    public String getWidgetIdentifier() {
        logger.d("getWidgetIdentifier");
        return SmartDashWidgetManager.MUSIC_WIDGET_ID;
    }

    public String getWidgetName() {
        return this.musicGaugeName;
    }

    public void onTrackUpdated(MusicTrackInfo trackInfo, Set<MediaControl> set, boolean willOpenNotification) {
        this.trackInfo = trackInfo;
        if (willOpenNotification) {
            logger.d("onTrackUpdated, delayed!");
            this.handler.postDelayed(new Runnable() {
                public void run() {
                    MusicWidgetPresenter.this.reDraw();
                }
            }, 250);
            return;
        }
        logger.d("onTrackUpdated (immediate)");
        reDraw();
    }

    public void onAlbumArtUpdate(@Nullable final ByteString photo, boolean animate) {
        logger.d("onAlbumArtUpdate " + photo + ", " + animate);
        this.animateNextArtworkTransition = animate;
        if (photo == null) {
            logger.v("ByteString image to set in notification is null");
            resetArtwork();
            reDraw();
            return;
        }
        String hash = photo.md5().hex();
        if (!TextUtils.equals(this.lastAlbumArtHash, hash) || this.albumArt == null) {
            this.lastAlbumArtHash = hash;
            TaskManager.getInstance().execute(new Runnable() {
                /* JADX WARNING: inconsistent code. */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public void run() {
                    MusicWidgetPresenter.logger.d("PhotoUpdate runnable " + photo);
                    byte[] byteArray = photo.toByteArray();
                    if (byteArray == null || byteArray.length <= 0) {
                        MusicWidgetPresenter.logger.i("Received photo has null or empty byte array");
                        MusicWidgetPresenter.this.resetArtwork();
                        MusicWidgetPresenter.this.redrawOnMainThread();
                        return;
                    }
                    int size = HudApplication.getAppContext().getResources().getDimensionPixelSize(R.dimen.music_gauge_album_art_size);
                    Bitmap artwork = null;
                    try {
                        artwork = ScalingUtilities.decodeByteArray(byteArray, size, size, ScalingLogic.FIT);
                        MusicWidgetPresenter.this.albumArt = ScalingUtilities.createScaledBitmap(artwork, size, size, ScalingLogic.FIT);
                        MusicWidgetPresenter.this.redrawOnMainThread();
                        if (artwork != null && artwork != MusicWidgetPresenter.this.albumArt) {
                            artwork.recycle();
                        }
                    } catch (Exception e) {
                        MusicWidgetPresenter.logger.e("Error updating the art work received ", e);
                        MusicWidgetPresenter.this.resetArtwork();
                        MusicWidgetPresenter.this.redrawOnMainThread();
                        if (artwork != null && artwork != MusicWidgetPresenter.this.albumArt) {
                            artwork.recycle();
                        }
                    } catch (Throwable th) {
                        if (!(artwork == null || artwork == MusicWidgetPresenter.this.albumArt)) {
                            artwork.recycle();
                        }
                    }
                }
            }, 1);
            return;
        }
        logger.d("Already have this artwork, ignoring");
    }

    private void resetArtwork() {
        this.albumArt = null;
        this.lastAlbumArtHash = null;
    }

    private void redrawOnMainThread() {
        this.handler.post(new Runnable() {
            public void run() {
                MusicWidgetPresenter.this.reDraw();
            }
        });
    }

    public void setWidgetVisibleToUser(boolean b) {
        logger.d("setWidgetVisibleToUser " + b);
        if (b) {
            logger.v("setWidgetVisibleToUser: add music update listener");
            this.musicManager.addMusicUpdateListener(this);
            this.trackInfo = this.musicManager.getCurrentTrack();
        } else {
            logger.v("setWidgetVisibleToUser: remove music update listener");
            this.musicManager.removeMusicUpdateListener(this);
        }
        super.setWidgetVisibleToUser(b);
    }
}
