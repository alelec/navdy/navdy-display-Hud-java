package com.navdy.hud.app.view;

import com.navdy.hud.app.screen.GestureLearningScreen.Presenter;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class ScrollableTextPresenterLayout$$InjectAdapter extends Binding<ScrollableTextPresenterLayout> implements MembersInjector<ScrollableTextPresenterLayout> {
    private Binding<Presenter> mPresenter;

    public ScrollableTextPresenterLayout$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.view.ScrollableTextPresenterLayout", false, ScrollableTextPresenterLayout.class);
    }

    public void attach(Linker linker) {
        this.mPresenter = linker.requestBinding("com.navdy.hud.app.screen.GestureLearningScreen$Presenter", ScrollableTextPresenterLayout.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mPresenter);
    }

    public void injectMembers(ScrollableTextPresenterLayout object) {
        object.mPresenter = (Presenter) this.mPresenter.get();
    }
}
