package com.navdy.hud.app.view;

import com.navdy.hud.app.screen.OSUpdateConfirmationScreen.Presenter;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class UpdateConfirmationView$$InjectAdapter extends Binding<UpdateConfirmationView> implements MembersInjector<UpdateConfirmationView> {
    private Binding<Presenter> mPresenter;

    public UpdateConfirmationView$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.view.UpdateConfirmationView", false, UpdateConfirmationView.class);
    }

    public void attach(Linker linker) {
        this.mPresenter = linker.requestBinding("com.navdy.hud.app.screen.OSUpdateConfirmationScreen$Presenter", UpdateConfirmationView.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mPresenter);
    }

    public void injectMembers(UpdateConfirmationView object) {
        object.mPresenter = (Presenter) this.mPresenter.get();
    }
}
