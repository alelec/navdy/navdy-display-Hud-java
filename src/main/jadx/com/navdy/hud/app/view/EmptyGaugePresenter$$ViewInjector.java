package com.navdy.hud.app.view;

import android.widget.TextView;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;

public class EmptyGaugePresenter$$ViewInjector {
    public static void inject(Finder finder, EmptyGaugePresenter target, Object source) {
        target.mEmptyGaugeText = (TextView) finder.findRequiredView(source, R.id.txt_empty_gauge, "field 'mEmptyGaugeText'");
    }

    public static void reset(EmptyGaugePresenter target) {
        target.mEmptyGaugeText = null;
    }
}
