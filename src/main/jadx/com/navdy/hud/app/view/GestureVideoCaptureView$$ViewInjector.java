package com.navdy.hud.app.view;

import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.ChoiceLayout;

public class GestureVideoCaptureView$$ViewInjector {
    public static void inject(Finder finder, GestureVideoCaptureView target, Object source) {
        target.sideImageView = (ImageView) finder.findRequiredView(source, R.id.sideImage, "field 'sideImageView'");
        target.mainImageView = (ImageView) finder.findRequiredView(source, R.id.image, "field 'mainImageView'");
        target.mainText = (TextView) finder.findRequiredView(source, R.id.title1, "field 'mainText'");
        target.secondaryText = (TextView) finder.findRequiredView(source, R.id.title3, "field 'secondaryText'");
        target.mChoiceLayout = (ChoiceLayout) finder.findRequiredView(source, R.id.choiceLayout, "field 'mChoiceLayout'");
    }

    public static void reset(GestureVideoCaptureView target) {
        target.sideImageView = null;
        target.mainImageView = null;
        target.mainText = null;
        target.secondaryText = null;
        target.mChoiceLayout = null;
    }
}
