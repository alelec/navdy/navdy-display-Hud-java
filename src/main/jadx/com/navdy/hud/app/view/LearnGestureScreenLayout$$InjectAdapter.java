package com.navdy.hud.app.view;

import com.navdy.hud.app.screen.GestureLearningScreen.Presenter;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class LearnGestureScreenLayout$$InjectAdapter extends Binding<LearnGestureScreenLayout> implements MembersInjector<LearnGestureScreenLayout> {
    private Binding<Presenter> mPresenter;

    public LearnGestureScreenLayout$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.view.LearnGestureScreenLayout", false, LearnGestureScreenLayout.class);
    }

    public void attach(Linker linker) {
        this.mPresenter = linker.requestBinding("com.navdy.hud.app.screen.GestureLearningScreen$Presenter", LearnGestureScreenLayout.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mPresenter);
    }

    public void injectMembers(LearnGestureScreenLayout object) {
        object.mPresenter = (Presenter) this.mPresenter.get();
    }
}
