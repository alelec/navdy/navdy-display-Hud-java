package com.navdy.hud.app.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.Optional;
import com.navdy.hud.app.R;

public class DashboardWidgetView extends RelativeLayout {
    @InjectView(R.id.custom_drawable)
    @Optional
    protected View mCustomView;
    private int mLayout;

    public DashboardWidgetView(Context context) {
        this(context, null);
    }

    public DashboardWidgetView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DashboardWidgetView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray customAttributes = context.obtainStyledAttributes(attrs, R.styleable.GaugeView, defStyleAttr, 0);
        if (customAttributes != null) {
            setContentView(customAttributes.getResourceId(1, R.layout.small_gauge_view));
            ButterKnife.inject((View) this);
            customAttributes.recycle();
        }
    }

    public boolean setContentView(View view) {
        this.mLayout = -1;
        removeAllViews();
        if (view == null) {
            return false;
        }
        addView(view);
        return true;
    }

    public boolean setContentView(int layoutResId) {
        if (layoutResId <= 0 || layoutResId == this.mLayout) {
            return false;
        }
        this.mLayout = layoutResId;
        removeAllViews();
        LayoutInflater.from(getContext()).inflate(layoutResId, this);
        ButterKnife.inject((View) this);
        return true;
    }

    public View getCustomView() {
        return this.mCustomView;
    }

    public void clear() {
        if (this.mCustomView != null) {
            this.mCustomView.setBackground(null);
        }
    }
}
