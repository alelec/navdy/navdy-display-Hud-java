package com.navdy.hud.app.view;

import android.widget.TextView;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;

public class SpeedLimitSignPresenter$$ViewInjector {
    public static void inject(Finder finder, SpeedLimitSignPresenter target, Object source) {
        target.speedLimitSignView = (SpeedLimitSignView) finder.findRequiredView(source, R.id.speed_limit_sign, "field 'speedLimitSignView'");
        target.speedLimitUnavailableText = (TextView) finder.findRequiredView(source, R.id.txt_speed_limit_unavailable, "field 'speedLimitUnavailableText'");
    }

    public static void reset(SpeedLimitSignPresenter target) {
        target.speedLimitSignView = null;
        target.speedLimitUnavailableText = null;
    }
}
