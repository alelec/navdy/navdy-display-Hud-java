package com.navdy.hud.app.service.pandora.messages;

import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException;
import com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException;
import java.nio.ByteBuffer;

public class ReturnTrackAlbumArtSegment extends BaseIncomingMessage {
    public byte[] data;
    public byte segmentIndex;
    public byte totalSegments;
    public int trackToken;

    protected static BaseIncomingMessage innerBuildFromPayload(byte[] payload) throws MessageWrongWayException, CorruptedPayloadException {
        ByteBuffer buffer = ByteBuffer.wrap(payload);
        buffer.get();
        ReturnTrackAlbumArtSegment result = new ReturnTrackAlbumArtSegment();
        result.trackToken = buffer.getInt();
        result.segmentIndex = buffer.get();
        result.totalSegments = buffer.get();
        result.data = new byte[buffer.remaining()];
        buffer.get(result.data);
        return result;
    }

    public String toString() {
        return "Got artwork's segment " + (this.segmentIndex + 1) + HereManeuverDisplayBuilder.SLASH + this.totalSegments + " for track with token " + this.trackToken;
    }
}
