package com.navdy.hud.app.service;

import com.squareup.otto.Bus;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;
import javax.inject.Provider;

public final class GestureVideosSyncService$$InjectAdapter extends Binding<GestureVideosSyncService> implements Provider<GestureVideosSyncService>, MembersInjector<GestureVideosSyncService> {
    private Binding<Bus> bus;
    private Binding<S3FileUploadService> supertype;

    public GestureVideosSyncService$$InjectAdapter() {
        super("com.navdy.hud.app.service.GestureVideosSyncService", "members/com.navdy.hud.app.service.GestureVideosSyncService", false, GestureVideosSyncService.class);
    }

    public void attach(Linker linker) {
        this.bus = linker.requestBinding("com.squareup.otto.Bus", GestureVideosSyncService.class, getClass().getClassLoader());
        Linker linker2 = linker;
        this.supertype = linker2.requestBinding("members/com.navdy.hud.app.service.S3FileUploadService", GestureVideosSyncService.class, getClass().getClassLoader(), false, true);
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.bus);
        injectMembersBindings.add(this.supertype);
    }

    public GestureVideosSyncService get() {
        GestureVideosSyncService result = new GestureVideosSyncService();
        injectMembers(result);
        return result;
    }

    public void injectMembers(GestureVideosSyncService object) {
        object.bus = (Bus) this.bus.get();
        this.supertype.injectMembers(object);
    }
}
