package com.navdy.hud.app.service.pandora.messages;

import com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException;
import com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException;

public class UpdateTrackCompleted extends BaseIncomingOneIntMessage {
    public UpdateTrackCompleted(int value) {
        super(value);
    }

    protected static BaseIncomingMessage innerBuildFromPayload(byte[] payload) throws MessageWrongWayException, CorruptedPayloadException {
        return new UpdateTrackCompleted(BaseIncomingOneIntMessage.parseIntValue(payload));
    }

    public String toString() {
        return "Track ended - token: " + this.value;
    }
}
