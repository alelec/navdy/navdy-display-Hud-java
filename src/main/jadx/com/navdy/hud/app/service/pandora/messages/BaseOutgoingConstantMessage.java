package com.navdy.hud.app.service.pandora.messages;

import com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException;
import com.navdy.hud.app.service.pandora.exceptions.StringOverflowException;
import com.navdy.hud.app.service.pandora.exceptions.UnexpectedEndOfStringException;
import java.io.IOException;

abstract class BaseOutgoingConstantMessage extends BaseOutgoingMessage {
    private byte[] preBuiltPayload = null;

    BaseOutgoingConstantMessage() {
    }

    public byte[] buildPayload() throws IOException, StringOverflowException, UnexpectedEndOfStringException, MessageWrongWayException {
        if (this.preBuiltPayload == null) {
            this.preBuiltPayload = super.buildPayload();
        }
        return this.preBuiltPayload;
    }
}
