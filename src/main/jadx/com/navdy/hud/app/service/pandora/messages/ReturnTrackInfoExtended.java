package com.navdy.hud.app.service.pandora.messages;

import com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException;
import com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException;
import com.navdy.hud.app.service.pandora.exceptions.StringOverflowException;
import com.navdy.hud.app.service.pandora.exceptions.UnterminatedStringException;
import java.nio.ByteBuffer;

public class ReturnTrackInfoExtended extends BaseIncomingMessage {
    public String album;
    public int albumArtLength;
    public String artist;
    public short duration;
    public short elapsed;
    public byte identityFlags;
    public byte permissionFlags;
    public byte rating;
    public String title;
    public int trackToken;

    protected static BaseIncomingMessage innerBuildFromPayload(byte[] payload) throws MessageWrongWayException, CorruptedPayloadException {
        ByteBuffer buffer = ByteBuffer.wrap(payload);
        buffer.get();
        ReturnTrackInfoExtended result = new ReturnTrackInfoExtended();
        result.trackToken = buffer.getInt();
        result.albumArtLength = buffer.getInt();
        result.duration = buffer.getShort();
        result.elapsed = buffer.getShort();
        result.rating = buffer.get();
        result.permissionFlags = buffer.get();
        result.identityFlags = buffer.get();
        try {
            result.title = BaseIncomingMessage.getString(buffer);
            result.artist = BaseIncomingMessage.getString(buffer);
            result.album = BaseIncomingMessage.getString(buffer);
            return result;
        } catch (UnterminatedStringException e) {
            throw new CorruptedPayloadException();
        } catch (StringOverflowException e2) {
            throw new CorruptedPayloadException();
        }
    }

    public String toString() {
        return "Got extended track's info for track with token: " + this.trackToken;
    }
}
