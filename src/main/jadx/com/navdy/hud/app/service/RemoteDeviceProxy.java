package com.navdy.hud.app.service;

import android.content.Context;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.device.RemoteDevice.Listener;
import com.navdy.service.library.device.RemoteDevice.PostEventHandler;
import com.navdy.service.library.device.connection.Connection.DisconnectCause;
import com.navdy.service.library.events.NavdyEvent;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.squareup.otto.Subscribe;

public class RemoteDeviceProxy extends RemoteDevice {
    private volatile int bandwidthLevel = 1;
    private boolean connected;
    protected ConnectionServiceProxy proxy;

    public RemoteDeviceProxy(ConnectionServiceProxy proxy, Context context, NavdyDeviceId deviceId) {
        super(context, deviceId, true);
        this.proxy = proxy;
        this.proxy.getBus().register(this);
        this.bandwidthLevel = 1;
        this.connected = true;
    }

    @Subscribe
    public void onConnectionStateChange(ConnectionStateChange event) {
        switch (event.state) {
            case CONNECTION_CONNECTED:
                this.connected = true;
                dispatchConnectEvent();
                return;
            case CONNECTION_DISCONNECTED:
                this.connected = false;
                dispatchDisconnectEvent(DisconnectCause.NORMAL);
                return;
            default:
                return;
        }
    }

    protected void dispatchNavdyEvent(final NavdyEvent event) {
        dispatchToListeners(new EventDispatcher() {
            public void dispatchEvent(RemoteDevice source, Listener listener) {
                listener.onNavdyEventReceived(source, event);
            }
        });
    }

    @Subscribe
    public void onNavdyEvent(NavdyEvent event) {
        dispatchNavdyEvent(event);
    }

    public boolean postEvent(NavdyEvent event) {
        this.proxy.postRemoteEvent(getDeviceId(), event);
        return true;
    }

    public boolean postEvent(NavdyEvent event, PostEventHandler postEventHandler) {
        if (postEventHandler != null) {
            notSupported();
            return false;
        }
        this.proxy.postRemoteEvent(getDeviceId(), event);
        return true;
    }

    public boolean connect() {
        notSupported();
        return false;
    }

    public boolean disconnect() {
        notSupported();
        return false;
    }

    public boolean isConnected() {
        return this.connected;
    }

    private void notSupported() {
        throw new UnsupportedOperationException("Can't call this method on remoteDeviceProxy");
    }

    public void setLinkBandwidthLevel(int bandwidthLevel) {
        this.bandwidthLevel = bandwidthLevel;
    }

    public int getLinkBandwidthLevel() {
        return this.bandwidthLevel;
    }
}
