package com.navdy.hud.app.service.pandora.messages;

import com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException;
import com.navdy.hud.app.service.pandora.exceptions.StringOverflowException;
import com.navdy.hud.app.service.pandora.exceptions.UnexpectedEndOfStringException;
import com.navdy.service.library.util.IOUtils;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

public abstract class BaseOutgoingMessage extends BaseMessage {
    protected static ByteArrayOutputStream putBoolean(ByteArrayOutputStream outStream, boolean value) throws IOException, StringOverflowException {
        outStream.write(value ? 1 : 0);
        return outStream;
    }

    protected static ByteArrayOutputStream putByte(ByteArrayOutputStream outStream, byte value) throws IOException, StringOverflowException {
        outStream.write(value);
        return outStream;
    }

    protected static ByteArrayOutputStream putShort(ByteArrayOutputStream outStream, short value) throws IOException, StringOverflowException {
        outStream.write(ByteBuffer.allocate(2).putShort(value).array());
        return outStream;
    }

    protected static ByteArrayOutputStream putInt(ByteArrayOutputStream outStream, int value) throws IOException, StringOverflowException {
        outStream.write(ByteBuffer.allocate(4).putInt(value).array());
        return outStream;
    }

    protected static ByteArrayOutputStream putFixedLengthASCIIString(ByteArrayOutputStream outStream, int fixedLength, String value) throws IOException, StringOverflowException {
        byte[] bytes = value.getBytes(FIXED_LENGTH_STRING_ENCODING);
        if (bytes.length > fixedLength) {
            throw new StringOverflowException();
        }
        outStream.write(bytes);
        for (int i = bytes.length; i < fixedLength; i++) {
            outStream.write(0);
        }
        return outStream;
    }

    protected static ByteArrayOutputStream putString(ByteArrayOutputStream outStream, String value) throws IOException, StringOverflowException, UnexpectedEndOfStringException {
        if (value.length() > 247) {
            throw new StringOverflowException();
        } else if (value.indexOf(0) >= 0) {
            throw new UnexpectedEndOfStringException();
        } else {
            outStream.write(value.getBytes(STRING_ENCODING));
            outStream.write(0);
            return outStream;
        }
    }

    protected ByteArrayOutputStream putThis(ByteArrayOutputStream os) throws IOException, StringOverflowException, UnexpectedEndOfStringException, MessageWrongWayException {
        throw new MessageWrongWayException();
    }

    public byte[] buildPayload() throws IOException, StringOverflowException, UnexpectedEndOfStringException, MessageWrongWayException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        try {
            putThis(os);
            byte[] result = os.toByteArray();
            return result;
        } finally {
            IOUtils.closeStream(os);
        }
    }
}
