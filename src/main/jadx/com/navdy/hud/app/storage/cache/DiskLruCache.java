package com.navdy.hud.app.storage.cache;

import android.content.Context;
import android.util.LruCache;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

public class DiskLruCache {
    private Context appContext = HudApplication.getAppContext();
    private String cacheName;
    private Logger logger;
    private MemoryLruCache lruCache;
    private int maxSize;
    private File rootPath;
    private TaskManager taskManager = TaskManager.getInstance();

    private static class FileEntry {
        String name;
        boolean removed;
        int size;

        FileEntry(String name, int size) {
            this.name = name;
            this.size = size;
        }
    }

    private static class MemoryLruCache extends LruCache<String, FileEntry> {
        private DiskLruCache parent;

        public MemoryLruCache(DiskLruCache parent, int maxSize) {
            super(maxSize);
            this.parent = parent;
        }

        protected int sizeOf(String key, FileEntry value) {
            return value.size;
        }

        protected void entryRemoved(boolean evicted, String key, FileEntry oldValue, FileEntry newValue) {
            if (oldValue != null) {
                this.parent.removeFile(oldValue);
                oldValue.removed = true;
            }
        }
    }

    public DiskLruCache(String cacheName, String rootPath, int maxSize) {
        this.logger = new Logger("DLC-" + cacheName);
        this.cacheName = cacheName;
        this.rootPath = new File(rootPath);
        this.rootPath.mkdirs();
        this.maxSize = maxSize;
        this.lruCache = new MemoryLruCache(this, maxSize);
        init();
    }

    private void init() {
        this.logger.v("init start");
        File[] list = this.rootPath.listFiles();
        if (list != null) {
            ArrayList<File> cacheFiles = new ArrayList();
            for (int i = 0; i < list.length; i++) {
                if (list[i].isFile()) {
                    cacheFiles.add(list[i]);
                }
            }
            int n = cacheFiles.size();
            this.logger.v("init: files in cache[" + n + "]");
            if (n == 0) {
                this.logger.v("init: cache empty");
                return;
            }
            Collections.sort(cacheFiles, new Comparator<File>() {
                public int compare(File lhs, File rhs) {
                    return Long.compare(lhs.lastModified(), rhs.lastModified());
                }
            });
            Iterator<File> iterator = cacheFiles.iterator();
            while (iterator.hasNext()) {
                File f = (File) iterator.next();
                String name = f.getName();
                this.lruCache.put(name, new FileEntry(name, (int) f.length()));
            }
            this.logger.v("init complete");
        }
    }

    private void addFile(final String name, final byte[] val) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                Throwable t;
                Throwable th;
                FileOutputStream fileOutputStream = null;
                try {
                    FileOutputStream fileOutputStream2 = new FileOutputStream(DiskLruCache.this.getFilePath(name));
                    try {
                        fileOutputStream2.write(val);
                        IOUtils.fileSync(fileOutputStream2);
                        IOUtils.closeStream(fileOutputStream2);
                        fileOutputStream = fileOutputStream2;
                    } catch (Throwable th2) {
                        th = th2;
                        fileOutputStream = fileOutputStream2;
                        IOUtils.closeStream(fileOutputStream);
                        throw th;
                    }
                } catch (Throwable th3) {
                    t = th3;
                    DiskLruCache.this.logger.e(t);
                    IOUtils.closeStream(fileOutputStream);
                }
            }
        }, 22);
    }

    private void removeFile(final FileEntry entry) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                if (!IOUtils.deleteFile(DiskLruCache.this.appContext, DiskLruCache.this.getFilePath(entry.name))) {
                    DiskLruCache.this.logger.v("file not deleted [" + entry.name + "]");
                }
            }
        }, 22);
    }

    public void updateFile(final FileEntry entry) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                File f = new File(DiskLruCache.this.getFilePath(entry.name));
                if (!f.setLastModified(System.currentTimeMillis())) {
                    DiskLruCache.this.logger.v("file time not changed [" + f.getAbsolutePath() + "]");
                }
            }
        }, 22);
    }

    public synchronized void put(String name, byte[] val) {
        FileEntry entry = new FileEntry(name, val.length);
        this.lruCache.put(name, entry);
        if (!entry.removed) {
            addFile(name, val);
        }
    }

    public synchronized boolean contains(String name) {
        return ((FileEntry) this.lruCache.get(name)) != null;
    }

    public synchronized byte[] get(String name) {
        byte[] bArr = null;
        synchronized (this) {
            GenericUtil.checkNotOnMainThread();
            FileEntry entry = (FileEntry) this.lruCache.get(name);
            if (entry != null) {
                updateFile(entry);
                try {
                    bArr = IOUtils.readBinaryFile(getFilePath(name));
                } catch (Throwable ex) {
                    this.logger.e(ex);
                }
            }
        }
        return bArr;
    }

    public synchronized void clear() {
        this.lruCache.evictAll();
    }

    public synchronized void remove(String name) {
        this.lruCache.remove(name);
    }

    private String getFilePath(String name) {
        return this.rootPath + File.separator + name;
    }
}
