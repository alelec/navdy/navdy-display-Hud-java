package com.navdy.hud.app.storage.db;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.os.Build;
import android.text.TextUtils;
import android.util.Pair;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.device.PowerManager;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.manager.UpdateReminderManager;
import com.navdy.hud.app.settings.HUDSettings;
import com.navdy.hud.app.storage.PathManager;
import com.navdy.hud.app.storage.db.table.FavoriteContactsTable;
import com.navdy.hud.app.storage.db.table.MusicArtworkCacheTable;
import com.navdy.hud.app.storage.db.table.RecentCallsTable;
import com.navdy.hud.app.storage.db.table.VinInformationTable;
import com.navdy.hud.app.util.SerialNumber;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import java.io.File;
import java.io.FilenameFilter;
import java.util.List;

public class HudDatabase extends SQLiteOpenHelper {
    private static final String DATABASE_FILE = (PathManager.getInstance().getDatabaseDir() + File.separator + DATABASE_NAME);
    private static final String DATABASE_NAME = "hud.db";
    private static final int DATABASE_VERSION = 17;
    private static final String UPGRADE_METHOD_PREFIX = "upgradeDatabase_";
    private static final int VERSION_1 = 1;
    private static final int VERSION_10 = 10;
    private static final int VERSION_11 = 11;
    private static final int VERSION_12 = 12;
    private static final int VERSION_13 = 13;
    private static final int VERSION_14 = 14;
    private static final int VERSION_15 = 15;
    private static final int VERSION_16 = 16;
    private static final int VERSION_17 = 17;
    private static final int VERSION_2 = 2;
    private static final int VERSION_3 = 3;
    private static final int VERSION_4 = 4;
    private static final int VERSION_5 = 5;
    private static final int VERSION_6 = 6;
    private static final int VERSION_7 = 7;
    private static final int VERSION_8 = 8;
    private static final int VERSION_9 = 9;
    private static final Context context = HudApplication.getAppContext();
    private static final Object lockObj = new Object();
    private static volatile boolean sDBError;
    private static volatile Throwable sDBErrorStr;
    private static DatabaseErrorHandler sDatabaseErrorHandler = new DatabaseErrorHandler() {
        public void onCorruption(SQLiteDatabase dbObj) {
            HudDatabase.sLogger.e("**** DB Corrupt ****");
            HudDatabase.sDBError = true;
            HudDatabase.sDBErrorStr = null;
            try {
                if (dbObj.isOpen()) {
                    List<Pair<String, String>> attachedDbs = null;
                    try {
                        attachedDbs = dbObj.getAttachedDbs();
                    } catch (Throwable e) {
                        HudDatabase.sLogger.i(e);
                    } catch (Throwable th) {
                        Throwable th2 = th;
                        if (attachedDbs != null) {
                            for (Pair<String, String> p : attachedDbs) {
                                IOUtils.deleteFile(HudDatabase.context, (String) p.second);
                            }
                        }
                        HudDatabase.deleteDatabaseFile();
                    }
                    try {
                        dbObj.close();
                    } catch (Throwable e2) {
                        HudDatabase.sLogger.i(e2);
                    }
                    if (attachedDbs != null) {
                        for (Pair<String, String> p2 : attachedDbs) {
                            IOUtils.deleteFile(HudDatabase.context, (String) p2.second);
                        }
                    }
                    HudDatabase.deleteDatabaseFile();
                    return;
                }
                HudDatabase.deleteDatabaseFile();
            } catch (Throwable t) {
                HudDatabase.sLogger.e(t);
            }
        }
    };
    private static Logger sLogger = new Logger(HudDatabase.class);
    private static volatile HudDatabase sSingleton;

    public static HudDatabase getInstance() {
        if (sSingleton == null) {
            synchronized (lockObj) {
                if (sSingleton == null) {
                    try {
                        if (!isDatabaseStable(DATABASE_FILE)) {
                            sLogger.i("db is not stable, deleting it");
                            deleteDatabaseFile();
                            sLogger.i("db delete complete");
                        }
                        sLogger.v("creating db-instance");
                        sSingleton = new HudDatabase();
                        sLogger.v("created db-instance");
                    } catch (Throwable t) {
                        sDBError = true;
                        sDBErrorStr = t;
                        sLogger.e(t);
                    }
                }
            }
        }
        return sSingleton;
    }

    private HudDatabase() {
        super(context, DATABASE_FILE, null, 17, sDatabaseErrorHandler);
    }

    public void onCreate(SQLiteDatabase db) {
        sDBError = false;
        sDBErrorStr = null;
        sLogger.v("onCreate::start::");
        db.beginTransaction();
        try {
            RecentCallsTable.createTable(db);
            FavoriteContactsTable.createTable(db);
            VinInformationTable.createTable(db);
            MusicArtworkCacheTable.createTable(db);
            db.setTransactionSuccessful();
            sLogger.v("onCreate::end::");
        } finally {
            db.endTransaction();
        }
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        sLogger.v("onUpgrade::start:: " + oldVersion + " ==>" + newVersion);
        Object[] params = new Object[]{db};
        Class[] reflectionParams = new Class[]{SQLiteDatabase.class};
        int i = oldVersion + 1;
        while (i <= newVersion) {
            try {
                sLogger.v("onUpgrade:: " + i);
                HudDatabase.class.getMethod(UPGRADE_METHOD_PREFIX + i, reflectionParams).invoke(null, params);
                i++;
            } catch (Throwable t) {
                sLogger.e(t);
                RuntimeException runtimeException = new RuntimeException(t);
            }
        }
        sLogger.v("onUpgrade::end:: " + oldVersion + " ==>" + newVersion);
    }

    public void onOpen(SQLiteDatabase db) {
        sLogger.v("onOpen::");
        super.onOpen(db);
    }

    public synchronized SQLiteDatabase getWritableDatabase() {
        SQLiteDatabase db;
        db = null;
        try {
            db = super.getWritableDatabase();
        } catch (Throwable ex) {
            sLogger.e(ex);
            if (ex instanceof SQLiteDatabaseCorruptException) {
                sLogger.e(":: db corrupted");
            } else {
                sDBError = true;
                sDBErrorStr = ex;
            }
        }
        return db;
    }

    private static boolean isDatabaseIntegrityFine(SQLiteDatabase db) {
        SQLiteStatement stmt = null;
        try {
            long l1 = System.currentTimeMillis();
            stmt = db.compileStatement("PRAGMA integrity_check");
            String result = stmt.simpleQueryForString();
            sLogger.v("db integrity check took:" + (System.currentTimeMillis() - l1));
            boolean z;
            if (TextUtils.isEmpty(result) || !"ok".equalsIgnoreCase(result)) {
                z = false;
                if (stmt != null) {
                    stmt.close();
                }
                return z;
            }
            z = true;
            return z;
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    private static boolean isDatabaseStable(String dbFilePath) {
        File dbFile = new File(dbFilePath);
        if (!dbFile.exists()) {
            return true;
        }
        try {
            SQLiteDatabase dbase = SQLiteDatabase.openDatabase(dbFilePath, null, 16);
            if (dbase == null) {
                sLogger.e("dbase could not be opened");
                return false;
            }
            try {
                if (isDatabaseIntegrityFine(dbase)) {
                    if (dbase.inTransaction()) {
                        sLogger.v("dbase in transaction");
                        dbase.endTransaction();
                    }
                    if (dbase.isDbLockedByCurrentThread()) {
                        sLogger.v("dbase is currently locked");
                    }
                    long l1 = System.currentTimeMillis();
                    dbase.execSQL("VACUUM");
                    sLogger.i("dbase VACCUM complete in " + (System.currentTimeMillis() - l1));
                    dbase.close();
                    if (dbFile.exists() && dbFile.canRead()) {
                        return true;
                    }
                    sLogger.e("dbase not stable");
                    return false;
                }
                sLogger.e("data integrity is not valid");
                return false;
            } catch (Throwable e) {
                sLogger.e(e);
            } finally {
                dbase.close();
            }
        } catch (SQLiteException e2) {
            sLogger.e("dbase could not be opened", e2);
            return false;
        }
    }

    public static void deleteDatabaseFile() {
        sLogger.v("deleteDatabaseFile");
        try {
            String str;
            File dbFile = new File(DATABASE_FILE);
            if (dbFile.exists()) {
                str = dbFile.getAbsolutePath();
                sLogger.e("delete:" + IOUtils.deleteFile(context, str) + " - " + str);
            }
            File file = new File(DATABASE_FILE + "-journal");
            if (file.exists()) {
                str = file.getAbsolutePath();
                sLogger.e("delete:" + IOUtils.deleteFile(context, str) + " - " + str);
            }
            file = new File(DATABASE_FILE + "-shm");
            if (file.exists()) {
                str = file.getAbsolutePath();
                sLogger.e("delete:" + IOUtils.deleteFile(context, str) + " - " + str);
            }
            file = new File(DATABASE_FILE + "-wal");
            if (file.exists()) {
                str = file.getAbsolutePath();
                sLogger.e("delete:" + IOUtils.deleteFile(context, str) + " - " + str);
            }
            SQLiteDatabase.deleteDatabase(dbFile);
        } catch (Throwable th) {
            sLogger.e("Error while trying to delete dbase");
        }
    }

    public static boolean isInErrorState() {
        return sDBError;
    }

    public static Throwable getErrorStr() {
        return sDBErrorStr;
    }

    public static void upgradeDatabase_2(SQLiteDatabase db) {
        RecentCallsTable.upgradeDatabase_2(db);
        FavoriteContactsTable.createTable(db);
    }

    public static void upgradeDatabase_3(SQLiteDatabase db) {
    }

    public static void upgradeDatabase_4(SQLiteDatabase db) {
    }

    public static void upgradeDatabase_5(SQLiteDatabase db) {
    }

    public static void upgradeDatabase_6(SQLiteDatabase db) {
        RemoteDeviceManager.getInstance().getSharedPreferences().edit().putString(HUDSettings.MAP_SCHEME, "carnav.day").apply();
        sLogger.v("map scheme changed to carnav.day");
    }

    public static void upgradeDatabase_7(SQLiteDatabase db) {
        File dir = new File(PathManager.getInstance().getHereVoiceSkinsPath(), "en-US_TTS");
        IOUtils.deleteDirectory(HudApplication.getAppContext(), dir);
        sLogger.v("deleted old here voice skins [" + dir + "]");
    }

    public static void upgradeDatabase_8(SQLiteDatabase db) {
        RemoteDeviceManager.getInstance().getSharedPreferences().edit().putString(HUDSettings.LED_BRIGHTNESS, "255").apply();
        sLogger.v("default max LED brightness changed to 255");
    }

    public static void upgradeDatabase_9(SQLiteDatabase db) {
        RemoteDeviceManager.getInstance().getSharedPreferences().edit().putBoolean(HUDSettings.GESTURE_ENGINE, false).apply();
        sLogger.v("disabling gesture engine");
    }

    @SuppressLint({"CommitPrefEdits"})
    public static void upgradeDatabase_10(SQLiteDatabase db) {
        RemoteDeviceManager.getInstance().getSharedPreferences().edit().putBoolean(HUDSettings.GESTURE_ENGINE, true).commit();
        sLogger.v("enabling gesture engine");
    }

    public static void upgradeDatabase_11(SQLiteDatabase db) {
        try {
            String property = "persist.sys.hud_gps";
            sLogger.v("trying to set [" + property + "]");
            char c = SerialNumber.instance.revisionCode.charAt(0);
            if (c < '4' || c > '6') {
                sLogger.v("not setting property [" + Build.SERIAL + "]");
                return;
            }
            SystemProperties.set(property, "-1");
            sLogger.v("setting property for [" + Build.SERIAL + "] to [-1] digit[" + c + "]");
        } catch (Throwable t) {
            sLogger.e("dbupgrade --> 11 failed", t);
        }
    }

    public static void upgradeDatabase_12(SQLiteDatabase db) {
        try {
            Context context = HudApplication.getAppContext();
            IOUtils.deleteDirectory(context, new File(context.getFilesDir(), ".Fabric"));
            sLogger.v("dbupgrade --> Fabric dir removed");
        } catch (Throwable t) {
            sLogger.e("dbupgrade --> 12 failed", t);
        }
    }

    public static void upgradeDatabase_13(SQLiteDatabase db) {
        VinInformationTable.createTable(db);
    }

    public static void upgradeDatabase_14(SQLiteDatabase db) {
        try {
            String NATIVE_CRASH_PATH = "/data/anr";
            String NATIVE_CRASH_PATTERN = "native_crash_";
            sLogger.v("looking for native crash files");
            File f = new File(NATIVE_CRASH_PATH);
            String[] list;
            String TOMBSTONE_PATH;
            if (f.exists() && f.isDirectory()) {
                list = f.list(new FilenameFilter() {
                    public boolean accept(File dir, String filename) {
                        if (filename.contains("native_crash_")) {
                            return true;
                        }
                        return false;
                    }
                });
                if (list == null || list.length == 0) {
                    sLogger.v("no native crash files");
                    sLogger.v("looking for tombstone");
                    TOMBSTONE_PATH = "/data/tombstones";
                    f = new File(TOMBSTONE_PATH);
                    if (f.exists()) {
                    }
                    sLogger.v("no tombstones to delete");
                    return;
                }
                for (String str : list) {
                    File nativeCrashFile = new File(NATIVE_CRASH_PATH + File.separator + str);
                    sLogger.v("removing native crash file[" + nativeCrashFile.getAbsolutePath() + "] :" + nativeCrashFile.delete());
                }
                sLogger.v("looking for tombstone");
                TOMBSTONE_PATH = "/data/tombstones";
                f = new File(TOMBSTONE_PATH);
                if (f.exists()) {
                }
                sLogger.v("no tombstones to delete");
                return;
            }
            sLogger.v("no native crashes to delete");
            sLogger.v("looking for tombstone");
            TOMBSTONE_PATH = "/data/tombstones";
            f = new File(TOMBSTONE_PATH);
            if (f.exists() || !f.isDirectory()) {
                sLogger.v("no tombstones to delete");
                return;
            }
            list = f.list();
            if (list == null || list.length == 0) {
                sLogger.v("no tombstones");
                return;
            }
            for (String str2 : list) {
                File tombstoneFile = new File(TOMBSTONE_PATH + File.separator + str2);
                sLogger.v("removing tombstone file[" + tombstoneFile.getAbsolutePath() + "] :" + tombstoneFile.delete());
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    @SuppressLint({"CommitPrefEdits"})
    public static void upgradeDatabase_15(SQLiteDatabase db) {
        RemoteDeviceManager.getInstance().getSharedPreferences().edit().remove(PowerManager.LAST_LOW_VOLTAGE_EVENT).commit();
    }

    public static void upgradeDatabase_16(SQLiteDatabase db) {
        MusicArtworkCacheTable.createTable(db);
    }

    @SuppressLint({"CommitPrefEdits"})
    public static void upgradeDatabase_17(SQLiteDatabase db) {
        RemoteDeviceManager.getInstance().getSharedPreferences().edit().remove(UpdateReminderManager.DIAL_REMINDER_TIME).commit();
    }
}
