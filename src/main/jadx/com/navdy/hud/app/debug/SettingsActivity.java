package com.navdy.hud.app.debug;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceActivity.Header;
import android.preference.PreferenceFragment;
import com.navdy.hud.app.R;
import java.util.List;

public class SettingsActivity extends PreferenceActivity {
    public static final String RESTART_ON_CRASH_KEY = "restart_on_crash_preference";
    public static final String START_ON_BOOT_KEY = "start_on_boot_preference";
    public static final String START_VIDEO_ON_BOOT_KEY = "start_video_on_boot_preference";

    public static class DeveloperPreferencesFragment extends PreferenceFragment {
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.developer_preferences);
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void onBuildHeaders(List<Header> target) {
        loadHeadersFromResource(R.xml.preference_headers, target);
    }
}
