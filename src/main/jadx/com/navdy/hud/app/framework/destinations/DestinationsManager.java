package com.navdy.hud.app.framework.destinations;

import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.contacts.ContactUtil;
import com.navdy.hud.app.framework.destinations.Destination.DestinationType;
import com.navdy.hud.app.framework.destinations.Destination.FavoriteDestinationType;
import com.navdy.hud.app.framework.destinations.Destination.PlaceCategory;
import com.navdy.hud.app.framework.fuel.FuelRoutingManager;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.here.HereMapUtil;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.hud.app.maps.here.HereRouteManager;
import com.navdy.hud.app.obd.ObdManager;
import com.navdy.hud.app.service.ConnectionHandler.DeviceSyncEvent;
import com.navdy.hud.app.util.DateUtil;
import com.navdy.hud.app.util.FeatureUtil.Feature;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.hud.app.util.RemoteCapabilitiesUtil;
import com.navdy.service.library.events.FavoriteDestinationsRequest;
import com.navdy.service.library.events.MessageStore;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState;
import com.navdy.service.library.events.destination.Destination;
import com.navdy.service.library.events.destination.Destination.Builder;
import com.navdy.service.library.events.destination.Destination.FavoriteType;
import com.navdy.service.library.events.destination.Destination.SuggestionType;
import com.navdy.service.library.events.destination.RecommendedDestinationsRequest;
import com.navdy.service.library.events.destination.RecommendedDestinationsUpdate;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.events.location.LatLong;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;
import com.navdy.service.library.events.places.FavoriteDestinationsUpdate;
import com.navdy.service.library.events.places.PlaceType;
import com.navdy.service.library.events.places.SuggestedDestination;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.UUID;

public class DestinationsManager {
    private static final FavoriteDestinationsChanged FAVORITE_DESTINATIONS_CHANGED = new FavoriteDestinationsChanged();
    public static final String FAVORITE_DESTINATIONS_FILENAME = "favoriteDestinations.pb";
    private static final char[] INITIALS_ARRAY = new char[4];
    public static final int MAX_DESTINATIONS = 30;
    private static final int MAX_INITIALS = 4;
    private static final RecentDestinationsChanged RECENT_DESTINATIONS_CHANGED = new RecentDestinationsChanged();
    private static final SuggestedDestinationsChanged SUGGESTED_DESTINATIONS_CHANGED = new SuggestedDestinationsChanged();
    private static final DestinationsManager sInstance = new DestinationsManager();
    private static final Logger sLogger = new Logger(DestinationsManager.class);
    private final Bus bus = RemoteDeviceManager.getInstance().getBus();
    private volatile List<Destination> favoriteDestinations;
    private FavoriteDestinationsUpdate lastUpdate;
    private volatile List<Destination> recentDestinations;
    Resources resources = HudApplication.getAppContext().getResources();
    private SuggestedDestination suggestedDestination;
    private volatile List<Destination> suggestedDestinations;

    public static class FavoriteDestinationsChanged {
    }

    public static class GasDestination {
        public Destination destination;
        public boolean showFirst;
    }

    public static class RecentDestinationsChanged {
    }

    public static class SuggestedDestinationsChanged {
    }

    public static DestinationsManager getInstance() {
        return sInstance;
    }

    private DestinationsManager() {
        this.bus.register(this);
    }

    public List<Destination> getFavoriteDestinations() {
        return this.favoriteDestinations;
    }

    private void setFavoriteDestinations(List<Destination> favoriteDestinations) {
        if (favoriteDestinations != this.favoriteDestinations) {
            this.favoriteDestinations = favoriteDestinations;
            this.bus.post(FAVORITE_DESTINATIONS_CHANGED);
        }
    }

    public List<Destination> getSuggestedDestinations() {
        List<Destination> list = new ArrayList();
        if (this.suggestedDestinations != null) {
            list.addAll(this.suggestedDestinations);
        }
        return list;
    }

    public void setSuggestedDestinations(List<Destination> suggestedDestinations) {
        this.suggestedDestinations = suggestedDestinations;
        this.bus.post(SUGGESTED_DESTINATIONS_CHANGED);
    }

    public List<Destination> getRecentDestinations() {
        return this.recentDestinations;
    }

    public void setRecentDestinations(List<Destination> recentDestinations) {
        this.recentDestinations = recentDestinations;
        this.bus.post(RECENT_DESTINATIONS_CHANGED);
    }

    @Subscribe
    public void onDeviceSyncRequired(DeviceSyncEvent event) {
        sLogger.v("syncDestinations");
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                DestinationsManager.this.requestDestinations();
            }
        }, 10);
    }

    @Subscribe
    public void onConnectionStatusChange(ConnectionStateChange event) {
        if (event.state == ConnectionState.CONNECTION_DISCONNECTED) {
            clearDestinations();
        }
    }

    @Subscribe
    public void onDriverProfileChanged(DriverProfileChanged event) {
        buildDestinations();
    }

    @Subscribe
    public void onFavoriteDestinationsUpdate(final FavoriteDestinationsUpdate response) {
        sLogger.v("received FavoriteDestinationsUpdate: " + response);
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                try {
                    if (response.status == RequestStatus.REQUEST_SUCCESS) {
                        DestinationsManager.this.parseDestinations(response);
                    } else if (response.status == RequestStatus.REQUEST_VERSION_IS_CURRENT) {
                        DestinationsManager.sLogger.v("fav-destination response: version" + response.serial_number + " is current");
                    } else {
                        DestinationsManager.sLogger.e("sent fav-destination response error: " + response.status);
                    }
                } catch (Throwable t) {
                    DestinationsManager.sLogger.e(t);
                }
            }
        }, 1);
    }

    @Subscribe
    public void onRecommendedDestinationsUpdate(final RecommendedDestinationsUpdate response) {
        sLogger.v("received RecommendedDestinationsUpdate:" + response);
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                try {
                    if (response.status == RequestStatus.REQUEST_SUCCESS) {
                        DestinationsManager.this.parseDestinations(response);
                    } else if (response.status == RequestStatus.REQUEST_VERSION_IS_CURRENT) {
                        DestinationsManager.sLogger.v("fav-destination response: version" + response.serial_number + " is current");
                    } else {
                        DestinationsManager.sLogger.e("sent fav-destination response error: " + response.status);
                    }
                } catch (Throwable t) {
                    DestinationsManager.sLogger.e(t);
                }
            }
        }, 1);
    }

    @Subscribe
    public void onDestinationSuggestion(final SuggestedDestination suggestedDestination) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                DestinationsManager.sLogger.v("onDestinationSuggestion:" + suggestedDestination);
                HereMapsManager hereMapsManager = HereMapsManager.getInstance();
                if (!hereMapsManager.isInitialized()) {
                    DestinationsManager.sLogger.e("onDestinationSuggestion: Map engine not initialized, cannot process suggestion, at this time");
                    DestinationsManager.this.suggestedDestination = suggestedDestination;
                } else if (HereNavigationManager.getInstance().isNavigationModeOn()) {
                    DestinationsManager.sLogger.e("onDestinationSuggestion: nav mode is on");
                } else if (RemoteDeviceManager.getInstance().getUiStateManager().getHomescreenView() == null || !HereRouteManager.isUIShowingRouteCalculation()) {
                    DestinationsManager.this.suggestedDestination = suggestedDestination;
                    if (HereMapUtil.getSavedRouteData(DestinationsManager.sLogger).navigationRouteRequest != null) {
                        DestinationsManager.sLogger.e("onDestinationSuggestion: has saved route data");
                    } else if (hereMapsManager.getLocationFixManager().getLastGeoCoordinate() == null) {
                        DestinationsManager.sLogger.e("onDestinationSuggestion: no current position");
                    } else {
                        DestinationsManager.this.launchSuggestedDestination();
                    }
                } else {
                    DestinationsManager.sLogger.e("onDestinationSuggestion: route calc is on");
                }
            }
        }, 3);
    }

    private void requestDestinations() {
        long favoritesVersion;
        GenericUtil.checkNotOnMainThread();
        if (this.lastUpdate != null) {
            favoritesVersion = this.lastUpdate.serial_number.longValue();
        } else {
            favoritesVersion = 0;
        }
        this.bus.post(new RemoteEvent(new FavoriteDestinationsRequest(Long.valueOf(favoritesVersion))));
        this.bus.post(new RemoteEvent(new RecommendedDestinationsRequest(Long.valueOf(0))));
        sLogger.v("sent favorite destinations request with current version=" + favoritesVersion);
        sLogger.v("sent recommended destinations request with current version");
    }

    private void buildDestinations() {
        if (GenericUtil.isMainThread()) {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    DestinationsManager.this.load();
                }
            }, 10);
        } else {
            load();
        }
    }

    private void load() {
        try {
            parseDestinations((FavoriteDestinationsUpdate) new MessageStore(DriverProfileHelper.getInstance().getCurrentProfile().getPreferencesDirectory()).readMessage(FAVORITE_DESTINATIONS_FILENAME, FavoriteDestinationsUpdate.class));
        } catch (Throwable t) {
            setFavoriteDestinations(null);
            sLogger.e(t);
        }
    }

    private void clearDestinations() {
        this.lastUpdate = null;
        setFavoriteDestinations(null);
        setRecentDestinations(null);
        setSuggestedDestinations(null);
    }

    private void parseDestinations(FavoriteDestinationsUpdate response) {
        sLogger.v("parsing FavoriteDestinationsUpdate");
        this.lastUpdate = response;
        List<Destination> destinations = null;
        if (response == null || response.destinations == null) {
            sLogger.w("fav-destination list returned is null");
        } else {
            new MessageStore(DriverProfileHelper.getInstance().getCurrentProfile().getPreferencesDirectory()).writeMessage(response, FAVORITE_DESTINATIONS_FILENAME);
            destinations = transform(response.destinations);
        }
        setFavoriteDestinations(destinations);
    }

    private void parseDestinations(RecommendedDestinationsUpdate response) {
        sLogger.v("parsing RecommendedDestinationsUpdate");
        if (response.destinations != null) {
            List<Destination> destinations = transform(response.destinations);
            List<Destination> recentDestinations = new ArrayList();
            List<Destination> suggestedDestinations = new ArrayList();
            if (destinations.size() > 0) {
                for (Destination destination : destinations) {
                    if (destination.recommendation) {
                        suggestedDestinations.add(destination);
                    } else {
                        recentDestinations.add(destination);
                    }
                }
            }
            setRecentDestinations(recentDestinations);
            setSuggestedDestinations(suggestedDestinations);
            return;
        }
        sLogger.w("rec-destination list returned is null");
    }

    @NonNull
    private List<Destination> transform(List<Destination> protoDestinations) {
        List<Destination> destinations = new ArrayList();
        for (Destination d : protoDestinations) {
            destinations.add(transformToInternalDestination(d));
            if (destinations.size() == 30) {
                sLogger.v("exceeded max size");
                break;
            }
        }
        return destinations;
    }

    public Destination transformToInternalDestination(Destination d) {
        if (d == null) {
            return null;
        }
        FavoriteDestinationType favoriteDestinationType;
        boolean suggested = Boolean.TRUE.equals(d.is_recommendation);
        int color = -1;
        if (d.suggestion_type == null || d.suggestion_type != SuggestionType.SUGGESTION_CALENDAR) {
            favoriteDestinationType = FavoriteDestinationType.buildFromValue(d.favorite_type.getValue());
        } else {
            suggested = true;
            color = this.resources.getColor(R.color.suggested_dest_cal);
            favoriteDestinationType = FavoriteDestinationType.FAVORITE_CALENDAR;
        }
        String initials = getInitials(d.destination_title, favoriteDestinationType);
        String recentTimeLabel = null;
        PlaceCategory placeCategory = getPlaceCategory(d);
        if (placeCategory == PlaceCategory.RECENT) {
            recentTimeLabel = getRecentTimeLabel(d);
        }
        if (suggested) {
            color = getSuggestionLabelColor(d, favoriteDestinationType, placeCategory);
        }
        return new Destination(d.navigation_position != null ? d.navigation_position.latitude.doubleValue() : 0.0d, d.navigation_position != null ? d.navigation_position.longitude.doubleValue() : 0.0d, d.display_position != null ? d.display_position.latitude.doubleValue() : 0.0d, d.display_position != null ? d.display_position.longitude.doubleValue() : 0.0d, d.full_address, d.destination_title, d.destination_subtitle, d.identifier, favoriteDestinationType, DestinationType.DEFAULT, initials, placeCategory, recentTimeLabel, color, suggested, d.destinationIcon != null ? d.destinationIcon.intValue() : 0, d.destinationIconBkColor != null ? d.destinationIconBkColor.intValue() : 0, d.place_id, d.place_type, d.destinationDistance, ContactUtil.fromPhoneNumbers(d.phoneNumbers), ContactUtil.fromContacts(d.contacts));
    }

    public Destination transformToProtoDestination(Destination d) {
        Boolean isRecommended = null;
        FavoriteType favoriteType = FavoriteType.FAVORITE_NONE;
        SuggestionType suggestionType = null;
        if (d.favoriteDestinationType != null) {
            switch (d.favoriteDestinationType) {
                case FAVORITE_CALENDAR:
                    suggestionType = SuggestionType.SUGGESTION_CALENDAR;
                    break;
                default:
                    switch (d.favoriteDestinationType) {
                        case FAVORITE_HOME:
                            favoriteType = FavoriteType.FAVORITE_HOME;
                            break;
                        case FAVORITE_WORK:
                            favoriteType = FavoriteType.FAVORITE_WORK;
                            break;
                        case FAVORITE_CONTACT:
                            favoriteType = FavoriteType.FAVORITE_CONTACT;
                            break;
                        case FAVORITE_CUSTOM:
                            favoriteType = FavoriteType.FAVORITE_CUSTOM;
                            break;
                    }
                    break;
            }
        }
        if (d.placeCategory != null) {
            switch (d.placeCategory) {
                case SUGGESTED:
                    isRecommended = Boolean.valueOf(true);
                    break;
                case SUGGESTED_RECENT:
                    isRecommended = Boolean.valueOf(true);
                    suggestionType = SuggestionType.SUGGESTION_RECENT;
                    break;
            }
        }
        PlaceType placeType = PlaceType.PLACE_TYPE_UNKNOWN;
        if (d.placeType != null) {
            placeType = d.placeType;
        }
        Builder builder = new Builder().navigation_position(new LatLong(Double.valueOf(d.navigationPositionLatitude), Double.valueOf(d.navigationPositionLongitude))).display_position(new LatLong(Double.valueOf(d.displayPositionLatitude), Double.valueOf(d.displayPositionLongitude))).full_address(d.fullAddress).destination_title(d.destinationTitle).destination_subtitle(d.destinationSubtitle).identifier(d.identifier).is_recommendation(isRecommended).place_type(placeType).suggestion_type(suggestionType).place_id(d.destinationPlaceId).favorite_type(favoriteType).contacts(ContactUtil.toContacts(d.contacts)).phoneNumbers(ContactUtil.toPhoneNumbers(d.phoneNumbers));
        if (d.destinationIcon != 0) {
            builder.destinationIcon(Integer.valueOf(d.destinationIcon));
        }
        if (d.destinationIconBkColor != 0) {
            builder.destinationIconBkColor(Integer.valueOf(d.destinationIconBkColor));
        }
        if (d.distanceStr != null) {
            builder.destinationDistance(d.distanceStr);
        }
        return builder.build();
    }

    public void requestNavigation(Destination destination) {
        switch (destination.destinationType) {
            case DEFAULT:
                handleDefaultDestination(destination, false);
                return;
            case FIND_GAS:
                handleFindGasDestination(destination);
                return;
            default:
                return;
        }
    }

    public void requestNavigationWithNavLookup(Destination destination) {
        handleDefaultDestination(destination, true);
    }

    private void handleDefaultDestination(Destination destination, boolean navCoordinateLookup) {
        Coordinate navCoordinate;
        sLogger.v("handleDefaultDestination [" + destination.toString() + "] lookup=" + navCoordinateLookup);
        if (navCoordinateLookup) {
            if (!RemoteCapabilitiesUtil.supportsNavigationCoordinateLookup()) {
                sLogger.v("handleDefaultDestination client does not support nav lookup");
            } else if (destination.navigationPositionLatitude == 0.0d || destination.navigationPositionLongitude == 0.0d) {
                sLogger.v("handleDefaultDestination do nav lookup");
                HereRouteManager.startNavigationLookup(destination);
                return;
            } else {
                sLogger.v("handleDefaultDestination nav coordinate exists already");
            }
        }
        Coordinate displayCoordinate = null;
        if (destination.navigationPositionLatitude == 0.0d && destination.navigationPositionLongitude == 0.0d) {
            navCoordinate = new Coordinate.Builder().latitude(Double.valueOf(destination.displayPositionLatitude)).longitude(Double.valueOf(destination.displayPositionLongitude)).build();
        } else {
            navCoordinate = new Coordinate.Builder().latitude(Double.valueOf(destination.navigationPositionLatitude)).longitude(Double.valueOf(destination.navigationPositionLongitude)).build();
            displayCoordinate = new Coordinate.Builder().latitude(Double.valueOf(destination.displayPositionLatitude)).longitude(Double.valueOf(destination.displayPositionLongitude)).build();
        }
        NavigationRouteRequest.Builder builder = new NavigationRouteRequest.Builder().destination(navCoordinate).label(destination.destinationTitle).streetAddress(destination.fullAddress).destination_identifier(destination.identifier).requestDestination(transformToProtoDestination(destination)).originDisplay(Boolean.valueOf(true)).geoCodeStreetAddress(Boolean.valueOf(false)).destinationType(getDestinationType(destination.favoriteDestinationType)).requestId(UUID.randomUUID().toString());
        if (displayCoordinate != null) {
            builder.destinationDisplay(displayCoordinate);
        }
        NavigationRouteRequest request = builder.build();
        sLogger.v("launched navigation route request");
        this.bus.post(new RemoteEvent(request));
        this.bus.post(request);
    }

    private void handleFindGasDestination(Destination destination) {
        NotificationManager.getInstance().removeNotification(FuelRoutingManager.LOW_FUEL_ID);
        FuelRoutingManager fuelRoutingManager = FuelRoutingManager.getInstance();
        fuelRoutingManager.showFindingGasStationToast();
        fuelRoutingManager.findGasStations(-1, false);
    }

    private FavoriteType getDestinationType(FavoriteDestinationType type) {
        switch (type) {
            case FAVORITE_HOME:
                return FavoriteType.FAVORITE_HOME;
            case FAVORITE_WORK:
                return FavoriteType.FAVORITE_WORK;
            case FAVORITE_CUSTOM:
                return FavoriteType.FAVORITE_CUSTOM;
            default:
                return FavoriteType.FAVORITE_NONE;
        }
    }

    public void goToSuggestedDestination() {
        if (this.suggestedDestination == null) {
            sLogger.e("gotToSuggestedDestination : Suggested destination is null, cannot to navigate");
            return;
        }
        Destination destination = this.suggestedDestination.destination;
        if (destination == null) {
            sLogger.e("gotToSuggestedDestination : Destination in Suggested destination is null, cannot to navigate");
        } else if (HereNavigationManager.getInstance().isNavigationModeOn()) {
            sLogger.e("gotToSuggestedDestination: Cannot navigate as the user is already navigating");
        } else {
            NavigationRouteRequest routeRequest = getNavigationRouteRequestForDestination(destination);
            sLogger.v("launched navigation route request");
            this.bus.post(new RemoteEvent(routeRequest));
            this.bus.post(routeRequest);
        }
    }

    public static String getInitials(String text, FavoriteDestinationType favoriteDestinationType) {
        if (favoriteDestinationType != null) {
            switch (favoriteDestinationType) {
                case FAVORITE_NONE:
                case FAVORITE_CUSTOM:
                    return getDestinationInitials(text);
                case FAVORITE_CONTACT:
                    return ContactUtil.getInitials(text);
            }
        }
        return null;
    }

    private static String getDestinationInitials(String text) {
        try {
            if (TextUtils.isEmpty(text)) {
                return null;
            }
            text = GenericUtil.removePunctuation(text).toUpperCase().trim();
            String firstWord = null;
            boolean firstNumber = false;
            int index = text.indexOf(" ");
            if (index >= 0) {
                firstWord = text.substring(0, index).trim();
            }
            try {
                Integer.parseInt(firstWord != null ? firstWord : text);
                firstNumber = true;
                if (firstWord == null) {
                    firstWord = text;
                }
            } catch (Throwable th) {
            }
            if (firstNumber) {
                if (firstWord.length() > 4) {
                    return null;
                }
                return firstWord;
            } else if (firstWord == null) {
                return text.substring(0, 1);
            } else {
                synchronized (INITIALS_ARRAY) {
                    StringTokenizer tokenizer = new StringTokenizer(text, " ");
                    int count = 0;
                    while (true) {
                        int count2 = count;
                        if (tokenizer.hasMoreElements()) {
                            count = count2 + 1;
                            INITIALS_ARRAY[count2] = ((String) tokenizer.nextElement()).charAt(0);
                            if (count == 4 && tokenizer.hasMoreElements()) {
                                return null;
                            }
                        } else {
                            firstWord = new String(INITIALS_ARRAY, 0, count2);
                            return firstWord;
                        }
                    }
                }
            }
        } catch (Throwable t) {
            sLogger.e(t);
            return null;
        }
    }

    private PlaceCategory getPlaceCategory(Destination d) {
        boolean suggested = false;
        if (Boolean.TRUE.equals(d.is_recommendation)) {
            suggested = true;
        }
        boolean recent = false;
        if (d.suggestion_type != null && d.suggestion_type == SuggestionType.SUGGESTION_RECENT) {
            recent = true;
        }
        if (suggested && recent) {
            return PlaceCategory.SUGGESTED_RECENT;
        }
        if (suggested) {
            return PlaceCategory.SUGGESTED;
        }
        if (recent) {
            return PlaceCategory.RECENT;
        }
        return null;
    }

    private String getRecentTimeLabel(Destination d) {
        if (d.suggestion_type == null || d.suggestion_type != SuggestionType.SUGGESTION_RECENT || d.last_navigated_to == null || d.last_navigated_to.longValue() <= 0) {
            return null;
        }
        return DateUtil.getDateLabel(new Date(d.last_navigated_to.longValue()));
    }

    private int getSuggestionLabelColor(Destination d, FavoriteDestinationType favoriteDestinationType, PlaceCategory placeCategory) {
        if (!Boolean.TRUE.equals(d.is_recommendation)) {
            return -1;
        }
        Resources resources = HudApplication.getAppContext().getResources();
        if (placeCategory == PlaceCategory.SUGGESTED_RECENT) {
            return resources.getColor(R.color.suggested_dest_work);
        }
        switch (favoriteDestinationType) {
            case FAVORITE_HOME:
                return resources.getColor(R.color.suggested_dest_home);
            case FAVORITE_WORK:
                return resources.getColor(R.color.suggested_dest_work);
            case FAVORITE_CONTACT:
                return resources.getColor(R.color.suggested_dest_contact);
            case FAVORITE_CALENDAR:
                return resources.getColor(R.color.suggested_dest_cal);
            default:
                return resources.getColor(R.color.suggested_dest_fav);
        }
    }

    public boolean launchSuggestedDestination() {
        if (this.suggestedDestination != null) {
            sLogger.v("launchSuggestedDestination: suggested destination toast");
            DestinationSuggestionToast.showSuggestion(this.suggestedDestination);
            return true;
        }
        sLogger.v("launchSuggestedDestination: no suggested destination available");
        return false;
    }

    public void clearSuggestedDestination() {
        sLogger.v("clearSuggestedDestination");
        DestinationSuggestionToast.dismissSuggestionToast();
        this.suggestedDestination = null;
    }

    public GasDestination getGasDestination() {
        GasDestination gasDestinationContainer = null;
        if (RemoteDeviceManager.getInstance().getFeatureUtil().isFeatureEnabled(Feature.FUEL_ROUTING) && FuelRoutingManager.getInstance().isAvailable()) {
            FuelRoutingManager fuelRoutingManager = FuelRoutingManager.getInstance();
            if (fuelRoutingManager.isBusy()) {
                sLogger.v("fuel route manager is busy");
            } else {
                boolean addFirst;
                sLogger.v("fuel route manager is not busy");
                boolean hasFuelPid = false;
                ObdManager obdManager = ObdManager.getInstance();
                if (obdManager.isConnected() && obdManager.getFuelLevel() != -1) {
                    hasFuelPid = true;
                }
                Destination gasDestination = Destination.getGasDestination();
                if (!hasFuelPid) {
                    addFirst = false;
                } else if (fuelRoutingManager.getFuelGlanceDismissTime() > 0) {
                    addFirst = true;
                } else {
                    addFirst = false;
                }
                gasDestinationContainer = new GasDestination();
                gasDestinationContainer.destination = gasDestination;
                if (addFirst) {
                    gasDestinationContainer.showFirst = true;
                } else {
                    gasDestinationContainer.showFirst = false;
                }
            }
        }
        return gasDestinationContainer;
    }

    public static NavigationRouteRequest getNavigationRouteRequestForDestination(Destination destination) {
        Coordinate navCoordinate;
        Coordinate displayCoordinate = null;
        if (destination.navigation_position == null || (destination.navigation_position.latitude.doubleValue() == 0.0d && destination.navigation_position.longitude.doubleValue() == 0.0d)) {
            navCoordinate = new Coordinate.Builder().latitude(destination.display_position.latitude).longitude(destination.display_position.longitude).build();
        } else {
            navCoordinate = new Coordinate.Builder().latitude(destination.display_position.latitude).longitude(destination.display_position.longitude).build();
            displayCoordinate = new Coordinate.Builder().latitude(destination.navigation_position.latitude).longitude(destination.navigation_position.longitude).build();
        }
        NavigationRouteRequest.Builder builder = new NavigationRouteRequest.Builder().destination(navCoordinate).label(destination.destination_title).streetAddress(destination.full_address).destination_identifier(destination.identifier).originDisplay(Boolean.valueOf(true)).geoCodeStreetAddress(Boolean.valueOf(false)).autoNavigate(Boolean.valueOf(true)).requestId(UUID.randomUUID().toString());
        if (displayCoordinate != null) {
            builder.destinationDisplay(displayCoordinate);
        }
        return builder.build();
    }
}
