package com.navdy.hud.app.framework.voice;

import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;

public class VoiceSearchTipsView$$ViewInjector {
    public static void inject(Finder finder, VoiceSearchTipsView target, Object source) {
        target.tipsText1 = (TextView) finder.findRequiredView(source, R.id.tip_text_1, "field 'tipsText1'");
        target.tipsText2 = (TextView) finder.findRequiredView(source, R.id.tip_text_2, "field 'tipsText2'");
        target.tipsIcon1 = (ImageView) finder.findRequiredView(source, R.id.tip_icon_1, "field 'tipsIcon1'");
        target.tipsIcon2 = (ImageView) finder.findRequiredView(source, R.id.tip_icon_2, "field 'tipsIcon2'");
        target.holder1 = (ViewGroup) finder.findRequiredView(source, R.id.tip_holder_1, "field 'holder1'");
        target.holder2 = (ViewGroup) finder.findRequiredView(source, R.id.tip_holder_2, "field 'holder2'");
        target.voiceSearchInformationView = finder.findRequiredView(source, R.id.voice_search_information, "field 'voiceSearchInformationView'");
        target.audioSourceText = (TextView) finder.findRequiredView(source, R.id.audio_source_text, "field 'audioSourceText'");
        target.audioSourceIcon = (ImageView) finder.findRequiredView(source, R.id.audio_source_icon, "field 'audioSourceIcon'");
        target.localeTagText = (TextView) finder.findRequiredView(source, R.id.locale_tag_text, "field 'localeTagText'");
        target.localeFullNameText = (TextView) finder.findRequiredView(source, R.id.locale_full_text, "field 'localeFullNameText'");
    }

    public static void reset(VoiceSearchTipsView target) {
        target.tipsText1 = null;
        target.tipsText2 = null;
        target.tipsIcon1 = null;
        target.tipsIcon2 = null;
        target.holder1 = null;
        target.holder2 = null;
        target.voiceSearchInformationView = null;
        target.audioSourceText = null;
        target.audioSourceIcon = null;
        target.localeTagText = null;
        target.localeFullNameText = null;
    }
}
