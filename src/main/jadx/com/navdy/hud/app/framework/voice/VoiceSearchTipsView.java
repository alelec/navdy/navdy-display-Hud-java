package com.navdy.hud.app.framework.voice;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.activity.Main.INotificationExtensionView;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import java.util.Locale;

public class VoiceSearchTipsView extends RelativeLayout implements INotificationExtensionView {
    public static final int ANIMATION_DURATION = 600;
    public static final int ANIMATION_INTERVAL = 3000;
    private static int TIPS_COUNT;
    private static TypedArray VOICE_SEARCH_TIPS_ICONS;
    private static String[] VOICE_SEARCH_TIPS_TEXTS;
    private AnimatorSet animatorSet;
    @InjectView(R.id.audio_source_icon)
    public ImageView audioSourceIcon;
    @InjectView(R.id.audio_source_text)
    public TextView audioSourceText;
    private int currentTipIndex;
    private Animator fadeIn;
    private Animator fadeOut;
    private Animator fadeOutVoiceSearchInformationView;
    private Handler handler;
    @InjectView(R.id.tip_holder_1)
    ViewGroup holder1;
    @InjectView(R.id.tip_holder_2)
    ViewGroup holder2;
    private boolean isListeningOverBluetooth;
    private String langauge;
    private String languageName;
    @InjectView(R.id.locale_full_text)
    public TextView localeFullNameText;
    @InjectView(R.id.locale_tag_text)
    public TextView localeTagText;
    private Runnable runnable;
    private boolean runningForFirstTime;
    @InjectView(R.id.tip_icon_1)
    ImageView tipsIcon1;
    @InjectView(R.id.tip_icon_2)
    ImageView tipsIcon2;
    @InjectView(R.id.tip_text_1)
    TextView tipsText1;
    @InjectView(R.id.tip_text_2)
    TextView tipsText2;
    @InjectView(R.id.voice_search_information)
    public View voiceSearchInformationView;

    static {
        TIPS_COUNT = 0;
        Resources resources = HudApplication.getAppContext().getResources();
        VOICE_SEARCH_TIPS_TEXTS = resources.getStringArray(R.array.voice_search_tips);
        TIPS_COUNT = VOICE_SEARCH_TIPS_TEXTS.length;
        VOICE_SEARCH_TIPS_ICONS = resources.obtainTypedArray(R.array.voice_search_tips_icons);
    }

    public VoiceSearchTipsView(Context context) {
        this(context, null);
    }

    public VoiceSearchTipsView(Context context, AttributeSet attrs) {
        this(context, attrs, -1);
    }

    public VoiceSearchTipsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.isListeningOverBluetooth = false;
        this.currentTipIndex = 0;
        this.runningForFirstTime = true;
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View) this);
        this.fadeIn = AnimatorInflater.loadAnimator(getContext(), 17498112);
        this.fadeOut = AnimatorInflater.loadAnimator(getContext(), 17498113);
        this.fadeIn.setTarget(this.holder2);
        this.fadeOut.setTarget(this.holder1);
        this.fadeOutVoiceSearchInformationView = AnimatorInflater.loadAnimator(getContext(), 17498113);
        this.fadeOutVoiceSearchInformationView.setTarget(this.voiceSearchInformationView);
        this.fadeOutVoiceSearchInformationView.addListener(new DefaultAnimationListener() {
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                VoiceSearchTipsView.this.handler.post(VoiceSearchTipsView.this.runnable);
            }
        });
        this.animatorSet = new AnimatorSet();
        this.animatorSet.addListener(new DefaultAnimationListener() {
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                VoiceSearchTipsView.this.currentTipIndex = (VoiceSearchTipsView.this.currentTipIndex + 1) % VoiceSearchTipsView.TIPS_COUNT;
                String tipText = VoiceSearchTipsView.VOICE_SEARCH_TIPS_TEXTS[VoiceSearchTipsView.this.currentTipIndex];
                int icon = VoiceSearchTipsView.this.getIconIdForIndex(VoiceSearchTipsView.this.currentTipIndex);
                VoiceSearchTipsView.this.tipsText1.setText(tipText);
                VoiceSearchTipsView.this.tipsIcon1.setImageResource(icon);
                VoiceSearchTipsView.this.holder1.setAlpha(1.0f);
                VoiceSearchTipsView.this.holder2.setAlpha(0.0f);
            }
        });
        this.animatorSet.setDuration(600);
        this.animatorSet.playTogether(new Animator[]{this.fadeIn, this.fadeOut});
        this.handler = new Handler();
        this.runnable = new Runnable() {
            public void run() {
                String tipText;
                int icon;
                if (VoiceSearchTipsView.this.runningForFirstTime) {
                    VoiceSearchTipsView.this.currentTipIndex = 0;
                    tipText = VoiceSearchTipsView.VOICE_SEARCH_TIPS_TEXTS[VoiceSearchTipsView.this.currentTipIndex];
                    icon = VoiceSearchTipsView.this.getIconIdForIndex(VoiceSearchTipsView.this.currentTipIndex);
                    VoiceSearchTipsView.this.tipsText1.setText(tipText);
                    VoiceSearchTipsView.this.tipsIcon1.setImageResource(icon);
                    VoiceSearchTipsView.this.holder1.setAlpha(1.0f);
                    VoiceSearchTipsView.this.holder2.setAlpha(0.0f);
                    VoiceSearchTipsView.this.runningForFirstTime = false;
                } else {
                    int nextTipIndex = (VoiceSearchTipsView.this.currentTipIndex + 1) % VoiceSearchTipsView.TIPS_COUNT;
                    tipText = VoiceSearchTipsView.VOICE_SEARCH_TIPS_TEXTS[nextTipIndex];
                    icon = VoiceSearchTipsView.this.getIconIdForIndex(nextTipIndex);
                    VoiceSearchTipsView.this.tipsText2.setText(tipText);
                    VoiceSearchTipsView.this.tipsIcon2.setImageResource(icon);
                    VoiceSearchTipsView.this.animatorSet.start();
                }
                VoiceSearchTipsView.this.handler.postDelayed(this, 3000);
            }
        };
    }

    public void setListeningOverBluetooth(boolean isListeningOverBluetooth) {
        this.isListeningOverBluetooth = isListeningOverBluetooth;
    }

    public void setVoiceSearchLocale(Locale locale) {
        this.langauge = locale.getLanguage();
        this.languageName = locale.getDisplayLanguage();
    }

    private int getIconIdForIndex(int index) {
        return VOICE_SEARCH_TIPS_ICONS.getResourceId(index, -1);
    }

    public void onStart() {
        this.currentTipIndex = 0;
        if (this.isListeningOverBluetooth) {
            this.audioSourceIcon.setImageResource(R.drawable.icon_badge_bt_tips);
            this.audioSourceText.setText(R.string.bluetooth_microphone);
        } else {
            this.audioSourceIcon.setImageResource(R.drawable.icon_badge_phone_source);
            this.audioSourceText.setText(R.string.phone_microphone);
        }
        this.localeTagText.setText(this.langauge);
        this.localeFullNameText.setText(this.languageName);
        this.handler.postDelayed(new Runnable() {
            public void run() {
                VoiceSearchTipsView.this.fadeOutVoiceSearchInformationView.start();
            }
        }, 3000);
    }

    public void onStop() {
        this.handler.removeCallbacks(this.runnable);
    }
}
