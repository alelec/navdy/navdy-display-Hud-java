package com.navdy.hud.app.framework.notifications;

import com.navdy.service.library.log.Logger;

public class NotificationHelper {
    static Logger sLogger = NotificationManager.sLogger;

    public static boolean isNotificationRemovable(String id) {
        boolean z = true;
        switch (id.hashCode()) {
            case -1409926357:
                if (id.equals(NotificationId.PHONE_CALL_NOTIFICATION_ID)) {
                    z = false;
                    break;
                }
                break;
            case -54194414:
                if (id.equals(NotificationId.TRAFFIC_REROUTE_NOTIFICATION_ID)) {
                    z = true;
                    break;
                }
                break;
        }
        switch (z) {
            case false:
            case true:
                return false;
            default:
                return true;
        }
    }
}
