package com.navdy.hud.app.framework.notifications;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.amazonaws.services.s3.internal.Constants;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.device.light.HUDLightUtils;
import com.navdy.hud.app.device.light.LED.Settings;
import com.navdy.hud.app.device.light.LightManager;
import com.navdy.hud.app.event.ShowScreenWithArgs;
import com.navdy.hud.app.event.Shutdown.Reason;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.connection.ConnectionNotification;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.hud.app.framework.glance.GlanceHandler;
import com.navdy.hud.app.framework.glance.GlanceHelper;
import com.navdy.hud.app.framework.glance.GlanceViewCache;
import com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType;
import com.navdy.hud.app.framework.notifications.NotificationAnimator.Operation;
import com.navdy.hud.app.framework.notifications.NotificationAnimator.OperationInfo;
import com.navdy.hud.app.framework.phonecall.CallManager.CallAccepted;
import com.navdy.hud.app.framework.phonecall.CallManager.CallEnded;
import com.navdy.hud.app.framework.phonecall.CallNotification;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.framework.toast.ToastManager.ShowToast;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.screen.BaseScreen;
import com.navdy.hud.app.ui.activity.Main;
import com.navdy.hud.app.ui.component.carousel.CarouselIndicator;
import com.navdy.hud.app.ui.component.carousel.ProgressIndicator;
import com.navdy.hud.app.ui.component.homescreen.HomeScreenView;
import com.navdy.hud.app.ui.component.image.InitialsImageView;
import com.navdy.hud.app.ui.component.image.InitialsImageView.Style;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import com.navdy.hud.app.ui.framework.INotificationAnimationListener;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.ui.framework.UIStateManager.Mode;
import com.navdy.hud.app.view.NotificationView;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.events.glances.ClearGlances;
import com.navdy.service.library.events.preferences.NotificationPreferences;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.TreeMap;
import javax.inject.Inject;
import mortar.Mortar;

public final class NotificationManager {
    private static final int EXPAND_VIEW_ANIMATION_DURATION = 250;
    public static final int EXPAND_VIEW_ANIMATION_START_DELAY_DURATION = 250;
    private static final int EXPAND_VIEW_ELEMENT_ANIMATION_DURATION = 100;
    private static final int EXPAND_VIEW_QUICK_DURATION = 0;
    public static final String EXTRA_VOICE_SEARCH_NOTIFICATION = "extra_voice_search_notification";
    private static final NotificationChange NOTIFICATION_CHANGE = new NotificationChange();
    private static final NotificationManager sInstance = new NotificationManager();
    static final Logger sLogger = new Logger(NotificationManager.class);
    private HashSet<String> NOTIFS_NOT_ALLOWED_ON_DISCONNECT = new HashSet();
    private HashSet<String> NOTIFS_REMOVED_ON_DISCONNECT = new HashSet();
    int animationTranslation;
    @Inject
    Bus bus;
    private Comparator<Info> comparator = new Comparator<Info>() {
        public int compare(Info lhs, Info rhs) {
            int diff = lhs.priority - rhs.priority;
            return diff != 0 ? diff : (int) (rhs.uniqueCounter - lhs.uniqueCounter);
        }
    };
    private Info currentNotification;
    private boolean deleteAllGlances;
    private boolean disconnected = true;
    private boolean enable = true;
    private boolean enablePhoneCalls = true;
    private View expandedNotifCoverView;
    private FrameLayout expandedNotifView;
    private boolean expandedWithStack;
    private Settings gestureEnabledSettings;
    private Handler handler = new Handler(Looper.getMainLooper());
    private Runnable ignoreScrollRunnable = new Runnable() {
        public void run() {
            NotificationManager.sLogger.v("limit reached:true");
            NotificationManager.this.scrollState = ScrollState.LIMIT_REACHED;
        }
    };
    private volatile boolean isAnimating;
    private volatile boolean isCollapsed;
    private volatile boolean isExpanded;
    private Info lastStackCurrentNotification;
    private Object lockObj = new Object();
    private Main mainScreen;
    private INotificationAnimationListener notifAnimationListener = new INotificationAnimationListener() {
        public void onStart(String id, NotificationType type, Mode mode) {
            NotificationManager.sLogger.v("notif-anim-start [" + id + "] type[" + type + "] mode [" + mode + "]");
            NotificationManager.this.isAnimating = true;
            if (mode == Mode.EXPAND) {
                NotificationManager.this.isCollapsed = false;
                NotificationView notificationView = NotificationManager.this.getNotificationView();
                notificationView.border.stopTimeout(true, null);
                NotificationManager.sLogger.v("notif-anim-start showing notif [" + NotificationManager.this.currentNotification.notification.getId() + "]");
                View view = NotificationManager.this.currentNotification.notification.getView(notificationView.getContext());
                NotificationManager.this.currentNotification.pushBackDuetoHighPriority = false;
                NotificationManager.this.currentNotification.startCalled = true;
                NotificationManager.this.currentNotification.notification.onStart(NotificationManager.this.notificationController);
                NotificationManager.sLogger.v("current notification [" + NotificationManager.this.currentNotification.notification.getId() + "] called start[" + System.identityHashCode(NotificationManager.this.currentNotification.notification) + "]");
                notificationView.addCustomView(NotificationManager.this.currentNotification.notification, view);
                NotificationManager.this.setNotificationColor();
                NotificationManager.this.currentNotification.notification.onNotificationEvent(Mode.EXPAND);
                return;
            }
            NotificationManager.this.isExpanded = false;
            HUDLightUtils.removeSettings(NotificationManager.this.gestureEnabledSettings);
        }

        public void onStop(String id, NotificationType type, Mode mode) {
            NotificationManager.sLogger.v("notif-anim-stop [" + id + "] type[" + type + "] mode [" + mode + "]");
            try {
                INotification pending = NotificationManager.this.pendingNotification;
                NotificationManager.this.pendingNotification = null;
                if (mode == Mode.COLLAPSE) {
                    boolean notificationAdded = false;
                    NotificationManager.this.isCollapsed = true;
                    NotificationView view = NotificationManager.this.getNotificationView();
                    NotificationManager.this.hideNotificationCoverView();
                    HomeScreenView homeScreenView;
                    if (NotificationManager.this.deleteAllGlances) {
                        NotificationManager.this.deleteAllGlances = false;
                        NotificationManager.sLogger.v("*** user selected delete all glances");
                        view.removeCustomView();
                        synchronized (NotificationManager.this.lockObj) {
                            NotificationManager.this.removeAllNotification();
                        }
                        if (null == null) {
                            homeScreenView = NotificationManager.this.uiStateManager.getHomescreenView();
                            if (homeScreenView.hasModeView()) {
                                NotificationManager.sLogger.v("mode-view: notif onStop show");
                                homeScreenView.animateInModeView();
                            }
                        }
                        NotificationManager.this.isAnimating = false;
                        return;
                    }
                    NotificationManager.this.cleanupViews(view);
                    if (NotificationManager.this.currentNotification == null) {
                        if (null == null) {
                            homeScreenView = NotificationManager.this.uiStateManager.getHomescreenView();
                            if (homeScreenView.hasModeView()) {
                                NotificationManager.sLogger.v("mode-view: notif onStop show");
                                homeScreenView.animateInModeView();
                            }
                        }
                        NotificationManager.this.isAnimating = false;
                        return;
                    }
                    NotificationManager.this.currentNotification.notification.onNotificationEvent(Mode.COLLAPSE);
                    String notifId = NotificationManager.this.currentNotification.notification.getId();
                    NotificationManager.sLogger.v("notif-anim-start [" + notifId + "]");
                    NotificationManager.this.notificationController.stopTimeout(false);
                    if (!(NotificationManager.this.currentNotification.removed || NotificationManager.this.currentNotification.resurrected || NotificationManager.this.currentNotification.notification.isAlive())) {
                        NotificationManager.sLogger.v("notif-anim-stop not alive, marked removed");
                        NotificationManager.this.currentNotification.removed = true;
                    }
                    if (NotificationManager.this.currentNotification.removed) {
                        NotificationManager.sLogger.v("notif-anim-stop removed [" + notifId + "]");
                        synchronized (NotificationManager.this.lockObj) {
                            NotificationManager.this.removeNotificationfromStack(NotificationManager.this.currentNotification, false);
                        }
                    } else {
                        NotificationManager.sLogger.v("notif-anim-stop pushed back [" + notifId + "]");
                        if (NotificationManager.this.currentNotification.startCalled) {
                            NotificationManager.this.currentNotification.startCalled = false;
                            NotificationManager.this.currentNotification.notification.onStop();
                        }
                    }
                    try {
                        view.removeCustomView();
                        if (NotificationManager.this.currentNotification.resurrected) {
                            NotificationManager.sLogger.v("notif-anim-stop notif resurrected");
                            NotificationManager.this.currentNotification.resurrected = false;
                            NotificationManager.this.isAnimating = false;
                            NotificationManager.this.showNotification(true);
                            if (!true) {
                                homeScreenView = NotificationManager.this.uiStateManager.getHomescreenView();
                                if (homeScreenView.hasModeView()) {
                                    NotificationManager.sLogger.v("mode-view: notif onStop show");
                                    homeScreenView.animateInModeView();
                                }
                            }
                            NotificationManager.this.isAnimating = false;
                            return;
                        }
                        NotificationManager.this.currentNotification = null;
                        if (NotificationManager.this.stagedNotification == null && NotificationManager.this.pendingScreen != null && NotificationManager.this.isScreenHighPriority(NotificationManager.this.pendingScreen)) {
                            NotificationManager.this.bus.post(new ShowScreenWithArgs(NotificationManager.this.pendingScreen, NotificationManager.this.pendingScreenArgs, NotificationManager.this.pendingScreenArgs2, false));
                            NotificationManager.sLogger.v("launched pending screen hp:" + NotificationManager.this.pendingScreen);
                            NotificationManager.this.pendingScreen = null;
                            NotificationManager.this.pendingScreenArgs = null;
                            NotificationManager.this.pendingScreenArgs2 = null;
                            if (null == null) {
                                homeScreenView = NotificationManager.this.uiStateManager.getHomescreenView();
                                if (homeScreenView.hasModeView()) {
                                    NotificationManager.sLogger.v("mode-view: notif onStop show");
                                    homeScreenView.animateInModeView();
                                }
                            }
                            NotificationManager.this.isAnimating = false;
                            return;
                        } else if (NotificationManager.this.stagedNotification != null) {
                            NotificationManager.sLogger.v("notif-anim-stop staged-notif [" + NotificationManager.this.stagedNotification.notification.getId() + "]");
                            NotificationManager.this.currentNotification = NotificationManager.this.stagedNotification;
                            NotificationManager.this.stagedNotification = null;
                            NotificationManager.this.isAnimating = false;
                            NotificationManager.this.showNotification();
                            if (!true) {
                                homeScreenView = NotificationManager.this.uiStateManager.getHomescreenView();
                                if (homeScreenView.hasModeView()) {
                                    NotificationManager.sLogger.v("mode-view: notif onStop show");
                                    homeScreenView.animateInModeView();
                                }
                            }
                            NotificationManager.this.isAnimating = false;
                            return;
                        } else {
                            NotificationManager.sLogger.v("notif-anim-stop no staged-notif, check if there is pending one");
                            synchronized (NotificationManager.this.lockObj) {
                                if (NotificationManager.this.notificationPriorityMap.size() > 0) {
                                    Info info = (Info) NotificationManager.this.notificationPriorityMap.lastKey();
                                    if (info.pushBackDuetoHighPriority) {
                                        NotificationManager.sLogger.v("notif-anim-stop found pending [" + info.notification.getId() + "]");
                                        NotificationManager.this.currentNotification = info;
                                        NotificationManager.this.isAnimating = false;
                                        notificationAdded = true;
                                        NotificationManager.this.showNotification();
                                        pending = null;
                                    }
                                }
                            }
                            if (NotificationManager.this.pendingShutdownArgs != null) {
                                NotificationManager.this.bus.post(new ShowScreenWithArgs(Screen.SCREEN_SHUTDOWN_CONFIRMATION, NotificationManager.this.pendingShutdownArgs, false));
                                NotificationManager.this.pendingShutdownArgs = null;
                            } else if (pending != null) {
                                NotificationManager.sLogger.v("adding pending notification:" + pending + " current = " + NotificationManager.this.currentNotification);
                                NotificationManager.this.isAnimating = false;
                                NotificationManager.this.addNotification(pending);
                            } else if (NotificationManager.this.pendingScreen != null) {
                                NotificationManager.this.bus.post(new ShowScreenWithArgs(NotificationManager.this.pendingScreen, NotificationManager.this.pendingScreenArgs, NotificationManager.this.pendingScreenArgs2, false));
                                NotificationManager.sLogger.v("launched pending screen:" + NotificationManager.this.pendingScreen);
                            }
                            NotificationManager.this.pendingScreen = null;
                            NotificationManager.this.pendingScreenArgs = null;
                            NotificationManager.this.pendingScreenArgs2 = null;
                            if (!notificationAdded) {
                                homeScreenView = NotificationManager.this.uiStateManager.getHomescreenView();
                                if (homeScreenView.hasModeView()) {
                                    NotificationManager.sLogger.v("mode-view: notif onStop show");
                                    homeScreenView.animateInModeView();
                                }
                            }
                        }
                    } catch (Throwable th) {
                        if (!notificationAdded) {
                            homeScreenView = NotificationManager.this.uiStateManager.getHomescreenView();
                            if (homeScreenView.hasModeView()) {
                                NotificationManager.sLogger.v("mode-view: notif onStop show");
                                homeScreenView.animateInModeView();
                            }
                        }
                    }
                } else {
                    NotificationManager.this.isExpanded = true;
                    if (NotificationManager.this.currentNotification == null || NotificationManager.this.currentNotification.removed || NotificationManager.this.stagedNotification != null) {
                        NotificationManager.sLogger.v("notif-anim-start got removed/changed while it was animating, hide it");
                        NotificationManager.this.isAnimating = false;
                        NotificationManager.this.hideNotification();
                    } else {
                        NotificationManager.this.setNotificationColor();
                        NotificationManager.this.notifView.showNextNotificationColor();
                        NotificationManager.this.notificationController.startTimeout(NotificationManager.this.currentNotification.notification.getTimeout());
                    }
                    NotificationManager.this.gestureEnabledSettings = HUDLightUtils.showGestureDetectionEnabled(HudApplication.getAppContext(), LightManager.getInstance(), "Notification");
                }
                NotificationManager.this.isAnimating = false;
            } catch (Throwable th2) {
                NotificationManager.this.isAnimating = false;
            }
        }
    };
    private CarouselIndicator notifIndicator;
    private ProgressIndicator notifScrollIndicator;
    private NotificationView notifView;
    private INotificationController notificationController = new INotificationController() {
        public void startTimeout(int timeout) {
            NotificationView notificationView = NotificationManager.this.getNotificationView();
            if (notificationView != null) {
                notificationView.border.startTimeout(timeout);
            }
        }

        public void stopTimeout(boolean force) {
            NotificationView notificationView = NotificationManager.this.getNotificationView();
            if (notificationView != null) {
                notificationView.border.stopTimeout(force, null);
            }
        }

        public void resetTimeout() {
            NotificationView notificationView = NotificationManager.this.getNotificationView();
            if (notificationView != null) {
                notificationView.border.resetTimeout();
            }
        }

        public void expandNotification(boolean withStack) {
            NotificationManager.sLogger.v("expandNotification  running=" + NotificationManager.this.operationRunning + " qsize:" + NotificationManager.this.operationQueue.size());
            NotificationView notificationView = NotificationManager.this.getNotificationView();
            if (notificationView == null || NotificationManager.this.currentNotification == null) {
                NotificationManager.sLogger.i("cannot display expand notif:no current notif");
            } else if (NotificationManager.this.isAnimating) {
                NotificationManager.sLogger.i("animation in progress, ignore expand");
            } else if (isExpanded()) {
                NotificationManager.sLogger.v("already expanded");
            } else {
                final FrameLayout layout = NotificationManager.this.getExpandedNotificationView();
                final View layoutCover = NotificationManager.this.getExpandedNotificationCoverView();
                final CarouselIndicator indicator = NotificationManager.this.getNotificationIndicator();
                NotificationManager.this.scrollState = ScrollState.NONE;
                if (layout.getChildCount() > 0) {
                    layout.removeAllViews();
                }
                if (withStack) {
                    NotificationPreferences preferences = DriverProfileHelper.getInstance().getCurrentProfile().getNotificationPreferences();
                    NotificationManager.this.ttsOn = preferences.readAloud.booleanValue();
                    NotificationManager.this.showOn = preferences.showContent.booleanValue();
                    NotificationManager.sLogger.v("expanded with[" + NotificationManager.this.currentNotification.notification.getId() + "] tts=" + NotificationManager.this.ttsOn + " show=" + NotificationManager.this.showOn);
                }
                View tempExpandedView = null;
                if (!withStack || (withStack && NotificationManager.this.showOn)) {
                    tempExpandedView = NotificationManager.this.currentNotification.notification.getExpandedView(notificationView.getContext(), null);
                    if (tempExpandedView == null) {
                        NotificationManager.sLogger.w("cannot expand, view is null");
                        NotificationManager.this.ttsOn = false;
                        NotificationManager.this.showOn = false;
                        return;
                    }
                }
                final View expandedView = tempExpandedView;
                NotificationManager.this.expandedWithStack = withStack;
                if (withStack) {
                    NotificationManager.this.stackCurrentNotification = NotificationManager.this.currentNotification;
                    NotificationManager.this.lastStackCurrentNotification = null;
                    if (NotificationManager.sLogger.isLoggable(2)) {
                        NotificationManager.this.printPriorityMap();
                    }
                    layout.setTag(Boolean.valueOf(true));
                } else {
                    layout.setTag(null);
                    NotificationManager.this.stackCurrentNotification = null;
                    NotificationManager.this.lastStackCurrentNotification = null;
                }
                notificationView.border.stopTimeout(true, null);
                notificationView.hideNextNotificationColor();
                final INotification notificationObj = NotificationManager.this.currentNotification.notification;
                if (withStack) {
                    int notificationCount = NotificationManager.this.getNotificationCount();
                    if (NotificationManager.this.isPhoneNotifPresentAndNotAlive()) {
                        notificationCount--;
                    }
                    int notificationIndex = NotificationManager.this.getNotificationIndex(NotificationManager.this.currentNotification);
                    NotificationManager.sLogger.v("expandNotification index:" + notificationIndex);
                    if (notificationIndex == -1) {
                        notificationIndex = 0;
                    } else {
                        notificationIndex++;
                    }
                    notificationCount += 2;
                    if (notificationIndex >= notificationCount) {
                        notificationIndex = notificationCount - 1;
                    }
                    NotificationManager.this.updateExpandedIndicator(notificationCount, notificationIndex, NotificationManager.this.currentNotification.notification.getColor());
                }
                final Runnable endAction = new Runnable() {
                    public void run() {
                        NotificationManager.sLogger.v("calling onExpandedNotificationEvent-expand");
                        notificationObj.onExpandedNotificationEvent(Mode.EXPAND);
                        indicator.setVisibility(0);
                        NotificationManager.this.displayScrollingIndicator(indicator.getCurrentItem());
                    }
                };
                if (!NotificationManager.this.expandedWithStack || (NotificationManager.this.expandedWithStack && NotificationManager.this.showOn)) {
                    notificationView.animate().x(0.0f).withEndAction(new Runnable() {
                        public void run() {
                            NotificationAnimator.animateExpandedViews(expandedView, null, layout, NotificationManager.this.animationTranslation, 250, 0, endAction);
                        }
                    }).withStartAction(new Runnable() {
                        public void run() {
                            layoutCover.setVisibility(0);
                            layout.setVisibility(0);
                        }
                    }).start();
                } else {
                    endAction.run();
                }
            }
        }

        public void collapseNotification(boolean completely, boolean quickly) {
            NotificationManager.this.collapseNotificationInternal(completely, quickly, false);
        }

        public void moveNext(boolean gesture) {
            NotificationManager.this.moveNext(gesture);
        }

        public void movePrevious(boolean gesture) {
            NotificationManager.this.movePrevious(gesture);
        }

        public boolean isExpandedWithStack() {
            return NotificationManager.this.isExpanded();
        }

        public boolean isExpanded() {
            return NotificationManager.this.isExpandedNotificationVisible();
        }

        public boolean isTtsOn() {
            return NotificationManager.this.ttsOn;
        }

        public boolean isShowOn() {
            return NotificationManager.this.showOn;
        }

        public Context getUIContext() {
            return NotificationManager.this.getUIContext();
        }
    };
    private long notificationCounter;
    private HashMap<String, Info> notificationIdMap = new HashMap();
    private TreeMap<Info, Object> notificationPriorityMap = new TreeMap(this.comparator);
    private ArrayList<Info> notificationSavedList = new ArrayList();
    private NavdyDeviceId notificationSavedListDeviceId;
    private Queue<OperationInfo> operationQueue = new LinkedList();
    private boolean operationRunning;
    INotification pendingNotification;
    private Screen pendingScreen;
    private Bundle pendingScreenArgs;
    private Object pendingScreenArgs2;
    private Bundle pendingShutdownArgs = null;
    private Resources resources = HudApplication.getAppContext().getResources();
    private IProgressUpdate scrollProgress = new IProgressUpdate() {
        public void onPosChange(int pos) {
            if (NotificationManager.this.stackCurrentNotification != null && NotificationManager.this.stackCurrentNotification.notification.supportScroll()) {
                if (pos <= 0) {
                    pos = 1;
                } else if (pos > 100) {
                    pos = 100;
                }
                if (NotificationManager.this.notifScrollIndicator.getCurrentItem() != pos) {
                    if (pos == 1 || pos == 100) {
                        NotificationManager.this.startScrollThresholdTimer();
                    }
                    NotificationManager.this.notifScrollIndicator.setCurrentItem(pos);
                }
            }
        }
    };
    private ScrollState scrollState = ScrollState.NONE;
    private boolean showOn;
    private Info stackCurrentNotification;
    private Info stagedNotification;
    private boolean ttsOn;
    @Inject
    UIStateManager uiStateManager;

    static class Info {
        INotification notification;
        int priority;
        boolean pushBackDuetoHighPriority;
        boolean removed;
        boolean resurrected;
        boolean startCalled;
        long uniqueCounter;

        Info() {
        }
    }

    public static class NotificationChange {
    }

    private enum ScrollState {
        NONE,
        HANDLED,
        NOT_HANDLED,
        LIMIT_REACHED
    }

    public static NotificationManager getInstance() {
        return sInstance;
    }

    private NotificationManager() {
        Mortar.inject(HudApplication.getAppContext(), this);
        this.uiStateManager.addNotificationAnimationListener(this.notifAnimationListener);
        this.NOTIFS_REMOVED_ON_DISCONNECT.add(NotificationId.PHONE_CALL_NOTIFICATION_ID);
        this.NOTIFS_REMOVED_ON_DISCONNECT.add(NotificationId.MUSIC_NOTIFICATION_ID);
        this.NOTIFS_NOT_ALLOWED_ON_DISCONNECT.add(NotificationId.PHONE_CALL_NOTIFICATION_ID);
        this.NOTIFS_NOT_ALLOWED_ON_DISCONNECT.add(NotificationId.MUSIC_NOTIFICATION_ID);
        this.animationTranslation = (int) HudApplication.getAppContext().getResources().getDimension(R.dimen.glance_anim_translation);
        this.bus.register(this);
    }

    public void addNotification(INotification notification) {
        addNotification(notification, null);
    }

    public void addNotification(final INotification notification, final EnumSet<Screen> allowedScreen) {
        if (notification != null) {
            if (this.uiStateManager.getRootScreen() == null) {
                sLogger.v("notification not accepted, Main screen not on [" + notification.getId() + "]");
            } else if (this.disconnected && this.NOTIFS_NOT_ALLOWED_ON_DISCONNECT.contains(notification.getId())) {
                sLogger.i("notification not allowed during disconnect[" + notification.getId() + "]");
            } else if (Looper.getMainLooper() != Looper.myLooper()) {
                this.handler.post(new Runnable() {
                    public void run() {
                        NotificationManager.this.addNotificationInternal(notification, allowedScreen);
                    }
                });
            } else {
                addNotificationInternal(notification, allowedScreen);
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void addNotificationInternal(INotification notification, EnumSet<Screen> allowedScreen) {
        if (this.mainScreen == null) {
            getNotificationView();
            if (this.mainScreen == null) {
                return;
            }
        }
        String id = notification.getId();
        if (TextUtils.isEmpty(id)) {
            sLogger.w("notification id is null");
            return;
        }
        NotificationType type = notification.getType();
        if (type == null) {
            sLogger.w("notification type is null");
            return;
        }
        sLogger.v("addNotif type[" + notification.getType() + "] id[" + id + "]");
        synchronized (this.lockObj) {
            if (!(this.currentNotification == null || isNotificationViewShowing())) {
                sLogger.v("addNotif current-notification marked null:" + (this.currentNotification.notification != null ? this.currentNotification.notification.getType() : "unk"));
                this.currentNotification = null;
            }
            if (this.currentNotification == null || notification.canAddToStackIfCurrentExists()) {
                if (allowedScreen != null) {
                    if (this.currentNotification != null) {
                        sLogger.v("addNotif screen constraint failed, current notification active");
                        return;
                    }
                    BaseScreen currentScreen = this.uiStateManager.getCurrentScreen();
                    if (currentScreen == null || !allowedScreen.contains(currentScreen.getScreen())) {
                        sLogger.v("addNotif screen constraint failed, current screen:" + currentScreen);
                        return;
                    } else if (this.mainScreen.isNotificationExpanding() || this.mainScreen.isNotificationCollapsing() || this.mainScreen.isNotificationViewShowing() || this.mainScreen.isScreenAnimating()) {
                        sLogger.v("addNotif screen constraint failed, mainscreen animating");
                        return;
                    }
                }
                if (this.currentNotification == null || !TextUtils.equals(this.currentNotification.notification.getId(), notification.getId())) {
                    Info info = (Info) this.notificationIdMap.get(id);
                    if (info == null) {
                        info = new Info();
                        info.notification = notification;
                        info.priority = type.getPriority();
                        long j = this.notificationCounter + 1;
                        this.notificationCounter = j;
                        info.uniqueCounter = j;
                        this.notificationIdMap.put(id, info);
                        this.notificationPriorityMap.put(info, info);
                        sLogger.v("addNotif added to stack");
                        if (this.currentNotification == null) {
                            sLogger.v("addNotif make current [" + id + "]");
                            this.currentNotification = info;
                            if (isExpanded()) {
                                this.currentNotification.pushBackDuetoHighPriority = true;
                                hideNotification();
                            } else if (!showNotification()) {
                                sLogger.v("addNotif not making current, disabled");
                                this.currentNotification.pushBackDuetoHighPriority = true;
                                this.currentNotification = null;
                            }
                        } else {
                            boolean highPriority = info.priority > this.currentNotification.priority;
                            boolean purgeable = this.currentNotification.notification.isPurgeable();
                            if (highPriority || purgeable) {
                                sLogger.v("addNotif high priority[" + highPriority + "] purgeable[" + purgeable + "]");
                                if (this.stagedNotification == null) {
                                    if (this.currentNotification.notification.isAlive()) {
                                        this.currentNotification.pushBackDuetoHighPriority = true;
                                    } else {
                                        this.currentNotification.removed = true;
                                        sLogger.v("addNotif current notification not alive anymore [" + this.currentNotification.notification.getType() + "]");
                                    }
                                    this.stagedNotification = info;
                                    if (this.isAnimating) {
                                        sLogger.v("addNotif anim in progress");
                                    } else if (this.isExpanded) {
                                        sLogger.v("addNotif hideNotif");
                                        hideNotification();
                                    } else {
                                        if (this.currentNotification.startCalled) {
                                            sLogger.v("addNotif showNotif");
                                        } else {
                                            sLogger.v("addNotif swapping notif");
                                            this.stagedNotification = null;
                                            this.currentNotification = info;
                                        }
                                        showNotification();
                                    }
                                } else if (TextUtils.equals(this.stagedNotification.notification.getId(), info.notification.getId())) {
                                    sLogger.v("addNotif stage notif already added");
                                    return;
                                } else if (info.priority > this.stagedNotification.priority) {
                                    sLogger.v("addNotif replacing staged notif since it high priority than staged [" + this.stagedNotification.notification.getId() + "]");
                                    this.stagedNotification.pushBackDuetoHighPriority = true;
                                    this.stagedNotification = info;
                                    if (this.isAnimating) {
                                        sLogger.v("addNotif anim in progress");
                                    } else if (this.isExpanded) {
                                        sLogger.v("addNotif-1 hideNotif");
                                        hideNotification();
                                        return;
                                    } else {
                                        sLogger.v("addNotif-1 showNotif");
                                        showNotification();
                                    }
                                } else {
                                    sLogger.v("addNotif not replacing staged notif staged [" + this.stagedNotification.notification.getId() + "]");
                                    return;
                                }
                            }
                            sLogger.v("addNotif lower priority than current[" + this.currentNotification.notification.getType() + "] update color");
                            info.pushBackDuetoHighPriority = true;
                            setNotificationColor();
                            if (!(this.isAnimating || this.isExpanded || isNotificationViewShowing())) {
                                sLogger.v("add notif lower priority, but view hidden");
                                showNotification();
                            }
                        }
                        postNotificationChange();
                        updateExpandedIndicator();
                    } else if (this.currentNotification == null) {
                        sLogger.v("addNotif already exist on stack, making it current");
                        this.currentNotification = info;
                        showNotification();
                    } else {
                        sLogger.v("addNotif already exist on stack, no-op");
                    }
                } else if (this.currentNotification.removed) {
                    sLogger.v("addNotif notif was marked to remove,resurrecting it back");
                    this.currentNotification.removed = false;
                    this.currentNotification.resurrected = true;
                } else if (this.currentNotification.startCalled) {
                    sLogger.v("addNotif already on screen,update");
                    notification.onUpdate();
                } else {
                    sLogger.v("addNotif not on screen yet");
                    if (!this.mainScreen.isNotificationViewShowing()) {
                        showNotification();
                    }
                }
            } else if (!TextUtils.equals(this.currentNotification.notification.getId(), notification.getId())) {
                sLogger.v("addNotif cannot be added current rule-2");
            } else if (this.currentNotification.startCalled) {
                sLogger.v("addNotif updated");
                this.currentNotification.notification.onUpdate();
            } else {
                sLogger.v("addNotif cannot be added current rule-1");
            }
        }
    }

    public void addPendingNotification(INotification notification) {
        this.pendingNotification = notification;
        sLogger.v("addPendingNotification:" + this.pendingNotification);
    }

    public void removeNotification(final String id) {
        if (Looper.getMainLooper() != Looper.myLooper()) {
            this.handler.post(new Runnable() {
                public void run() {
                    NotificationManager.this.removeNotification(id, true);
                }
            });
        } else {
            removeNotification(id, true);
        }
    }

    public void removeNotification(String id, boolean remove) {
        removeNotification(id, remove, null, null, null);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void removeNotification(String id, boolean remove, Screen screen, Bundle screenArgs, Object screenArgs2) {
        if (!TextUtils.isEmpty(id)) {
            synchronized (this.lockObj) {
                this.pendingScreen = screen;
                this.pendingScreenArgs = screenArgs;
                this.pendingScreenArgs2 = screenArgs2;
                Info info = (Info) this.notificationIdMap.get(id);
                if (info == null) {
                } else if (!this.notificationController.isExpandedWithStack() || NotificationHelper.isNotificationRemovable(id)) {
                    info.removed = remove;
                    if (info.removed && info.resurrected) {
                        sLogger.v("removeNotification [" + id + "] resurrected:false");
                        info.resurrected = false;
                    }
                    if (!(info.removed || info.notification.isAlive())) {
                        sLogger.v("removeNotification [" + id + "] is not alive");
                        info.removed = true;
                    }
                    if (!info.removed) {
                        sLogger.v("removeNotification [" + id + "] is still alive");
                    }
                    if (this.currentNotification == null || !TextUtils.equals(this.currentNotification.notification.getId(), info.notification.getId())) {
                        sLogger.v("bkgnd notification being removed");
                        removeNotificationfromStack(info, false);
                        setNotificationColor();
                    } else if (this.isExpanded && !this.isAnimating) {
                        hideNotification();
                    }
                } else {
                    sLogger.v("removeNotification, handleRemoveNotificationInExpandedMode");
                    info.removed = true;
                    handleRemoveNotificationInExpandedMode(id, true);
                }
            }
        }
    }

    private void removeNotificationfromStack(Info info, boolean internal) {
        if (info != null) {
            if (this.notificationIdMap.remove(info.notification.getId()) == null) {
                sLogger.v("removeNotification, not found in id map");
            }
            if (this.notificationPriorityMap.remove(info) == null) {
                sLogger.v("removeNotification, not found in priority map");
            }
            if (info.startCalled) {
                info.startCalled = false;
                info.notification.onStop();
            }
            String stagedId = null;
            if (this.stagedNotification != null) {
                stagedId = this.stagedNotification.notification.getId();
                if (TextUtils.equals(this.stagedNotification.notification.getId(), info.notification.getId())) {
                    this.stagedNotification = null;
                    sLogger.v("cancelling staged notification, getting removed");
                }
            }
            postNotificationChange();
            sLogger.v("removed notification from stack [" + info.notification.getId() + "] staged[" + stagedId + "]");
            if (!internal) {
                updateExpandedIndicator();
            }
        }
    }

    public void currentNotificationTimeout() {
        if (this.currentNotification != null) {
            sLogger.v("currentNotificationTimeout");
            AnalyticsSupport.recordGlanceAction(AnalyticsSupport.ANALYTICS_EVENT_GLANCE_DISMISS, this.currentNotification.notification, "timeout");
            removeNotification(this.currentNotification.notification.getId(), false);
        }
    }

    public INotification getCurrentNotification() {
        if (this.currentNotification == null) {
            return null;
        }
        return this.currentNotification.notification;
    }

    public INotification getNotification(String id) {
        INotification iNotification = null;
        if (!TextUtils.isEmpty(id)) {
            synchronized (this.lockObj) {
                Info info = (Info) this.notificationIdMap.get(id);
                if (info != null) {
                    iNotification = info.notification;
                }
            }
        }
        return iNotification;
    }

    public int getNotificationCount() {
        int size;
        synchronized (this.lockObj) {
            size = this.notificationIdMap.size();
        }
        return size;
    }

    public boolean isNotificationPresent(String id) {
        boolean containsKey;
        synchronized (this.lockObj) {
            containsKey = this.notificationIdMap.containsKey(id);
        }
        return containsKey;
    }

    public boolean showNotification() {
        if (!this.enable) {
            if (this.enablePhoneCalls && isCurrentNotificationId(NotificationId.PHONE_CALL_NOTIFICATION_ID)) {
                sLogger.v("notification only phone-notif allowed");
            } else {
                sLogger.v("notification not enabled don't show notification");
                return false;
            }
        }
        if (this.uiStateManager.isWelcomeScreenOn()) {
            sLogger.v("welcome screen on, don't show notification");
            return false;
        }
        showNotification(false);
        return true;
    }

    private void showNotification(boolean ignoreAnimation) {
        sLogger.v("show notification isExpanded:" + this.isExpanded + " anim:" + this.isAnimating + " ignore:" + ignoreAnimation);
        if (!this.isExpanded && !this.isAnimating) {
            this.bus.post(new ShowScreenWithArgs(Screen.SCREEN_NOTIFICATION, null, null, ignoreAnimation));
        }
    }

    public void hideNotification() {
        if (this.isExpanded && !this.isAnimating) {
            if (isExpanded()) {
                sLogger.v("hideNotification:expand hide");
                animateOutExpandedView(true, this.currentNotification, false);
                return;
            }
            hideNotificationInternal();
        }
    }

    private void hideNotificationInternal() {
        NotificationView view = getNotificationView();
        if (view.border.isVisible()) {
            sLogger.v("hideNotification:border visible");
            view.border.stopTimeout(true, new Runnable() {
                public void run() {
                    NotificationManager.sLogger.v("hideNotification-anim:screen_back");
                    NotificationManager.this.bus.post(new ShowScreenWithArgs(Screen.SCREEN_BACK, null, null, true));
                }
            });
            return;
        }
        sLogger.v("hideNotification:screen_back");
        this.bus.post(new ShowScreenWithArgs(Screen.SCREEN_BACK, null, null, true));
    }

    public boolean isCurrentNotificationId(String id) {
        if (this.currentNotification != null && TextUtils.equals(this.currentNotification.notification.getId(), id)) {
            return true;
        }
        return false;
    }

    private Main getMainScreen() {
        if (this.mainScreen == null) {
            this.mainScreen = this.uiStateManager.getRootScreen();
        }
        return this.mainScreen;
    }

    private NotificationView getNotificationView() {
        if (this.notifView != null) {
            return this.notifView;
        }
        if (this.mainScreen == null) {
            getMainScreen();
        }
        if (this.mainScreen != null) {
            this.notifView = this.mainScreen.getNotificationView();
        }
        return this.notifView;
    }

    private FrameLayout getExpandedNotificationView() {
        if (this.expandedNotifView != null) {
            return this.expandedNotifView;
        }
        if (this.mainScreen == null) {
            getMainScreen();
        }
        if (this.mainScreen != null) {
            this.expandedNotifView = this.mainScreen.getExpandedNotificationView();
        }
        return this.expandedNotifView;
    }

    private View getExpandedNotificationCoverView() {
        if (this.expandedNotifCoverView != null) {
            return this.expandedNotifCoverView;
        }
        if (this.mainScreen == null) {
            getMainScreen();
        }
        if (this.mainScreen != null) {
            this.expandedNotifCoverView = this.mainScreen.getExpandedNotificationCoverView();
        }
        return this.expandedNotifCoverView;
    }

    private CarouselIndicator getNotificationIndicator() {
        if (this.notifIndicator != null) {
            return this.notifIndicator;
        }
        if (this.mainScreen == null) {
            getMainScreen();
        }
        if (this.mainScreen != null) {
            this.notifIndicator = this.mainScreen.getNotificationIndicator();
            this.notifScrollIndicator = this.mainScreen.getNotificationScrollIndicator();
        }
        return this.notifIndicator;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean makeNotificationCurrent(boolean makeAllVisible) {
        synchronized (this.lockObj) {
            if (this.currentNotification != null) {
                if (this.notificationPriorityMap.size() == 0) {
                    if (this.currentNotification.startCalled) {
                        this.currentNotification.notification.onStop();
                    }
                    this.currentNotification = null;
                    return false;
                }
                Info highest = (Info) this.notificationPriorityMap.lastKey();
                if (!(highest == null || highest == this.currentNotification)) {
                    sLogger.v("mknc:current[" + this.currentNotification.notification.getId() + "] is not highest[" + highest.notification.getId() + "]");
                    if (this.currentNotification.startCalled) {
                        this.currentNotification.startCalled = false;
                        this.currentNotification.notification.onStop();
                        sLogger.v("mknc: stop called");
                    }
                    this.currentNotification = highest;
                }
            } else if (this.notificationPriorityMap.size() > 0) {
                if (makeAllVisible) {
                    sLogger.v("make all visible");
                    for (Info info : this.notificationPriorityMap.keySet()) {
                        info.pushBackDuetoHighPriority = true;
                    }
                }
                this.currentNotification = (Info) this.notificationPriorityMap.lastKey();
                return true;
            } else {
                return false;
            }
        }
    }

    public void setNotificationColor() {
        NotificationView notificationView = getNotificationView();
        if (notificationView != null) {
            if (this.currentNotification != null) {
                synchronized (this.lockObj) {
                    Info info = getLowerNotification(this.currentNotification);
                    if (info == null) {
                        notificationView.resetNextNotificationColor();
                    } else {
                        notificationView.setNextNotificationColor(info.notification.getColor());
                    }
                }
                return;
            }
            notificationView.resetNextNotificationColor();
        }
    }

    public int getNotificationColor() {
        int i = -1;
        synchronized (this.lockObj) {
            if (this.notificationPriorityMap.size() > 0) {
                Info info = (Info) this.notificationPriorityMap.lastKey();
                if (info.removed) {
                } else {
                    i = info.notification.getColor();
                }
            }
        }
        return i;
    }

    public void handleDisconnect(boolean linkLost) {
        if (!this.disconnected) {
            sLogger.v("client disconnected: save stack :" + linkLost);
            this.disconnected = true;
            saveStack();
            if (isExpanded() || isExpandedNotificationVisible()) {
                collapseExpandedNotification(true, true);
            } else {
                collapseNotification();
            }
            RemoteDeviceManager.getInstance().getCallManager().disconnect();
        }
    }

    public void handleConnect() {
        sLogger.v("client connected: restore stack");
        this.disconnected = false;
        restoreStack();
        this.notificationSavedListDeviceId = RemoteDeviceManager.getInstance().getDeviceId();
    }

    public void showHideToast(boolean connect) {
        ToastManager toastManager = ToastManager.getInstance();
        if (this.uiStateManager.isWelcomeScreenOn()) {
            sLogger.v("welcome screen on, no disconnect notification");
            hideConnectionToast(connect);
        } else if (connect) {
            if (!toastManager.isCurrentToast("connection#toast")) {
                ConnectionNotification.showConnectedToast();
            }
        } else if (!toastManager.isCurrentToast(ConnectionNotification.DISCONNECT_ID)) {
            ConnectionNotification.showDisconnectedToast(false);
        }
    }

    public void hideConnectionToast(boolean connected) {
        ToastManager toastManager = ToastManager.getInstance();
        if (connected) {
            toastManager.dismissCurrentToast(ConnectionNotification.DISCONNECT_ID);
        } else {
            toastManager.dismissCurrentToast("connection#toast");
        }
    }

    private void saveStack() {
        synchronized (this.lockObj) {
            ToastManager toastManager = ToastManager.getInstance();
            toastManager.dismissCurrentToast(CallNotification.CALL_NOTIFICATION_TOAST_ID);
            toastManager.clearPendingToast(CallNotification.CALL_NOTIFICATION_TOAST_ID);
            RemoteDeviceManager.getInstance().getCallManager().clearCallStack();
            this.notificationSavedList.clear();
            int n = this.notificationIdMap.size();
            sLogger.v("saveStack:" + n);
            if (n > 0) {
                Iterator<String> iterator = this.notificationIdMap.keySet().iterator();
                while (iterator.hasNext()) {
                    String id = (String) iterator.next();
                    Info info = (Info) this.notificationIdMap.get(id);
                    if (this.NOTIFS_REMOVED_ON_DISCONNECT.contains(id)) {
                        this.notificationPriorityMap.remove(info);
                        iterator.remove();
                        sLogger.v("saveStack removed:" + id);
                    } else if (NotificationType.GLANCE == info.notification.getType()) {
                        sLogger.v("saveStack saved:" + id);
                        this.notificationSavedList.add(info);
                        iterator.remove();
                        this.notificationPriorityMap.remove(info);
                    } else {
                        sLogger.v("saveStack left:" + id);
                    }
                }
            }
            this.currentNotification = null;
            this.stagedNotification = null;
            GlanceHandler.getInstance().saveState();
        }
        this.uiStateManager.enableNotificationColor(true);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void restoreStack() {
        synchronized (this.lockObj) {
            int n = this.notificationSavedList.size();
            sLogger.v("restoreStack:" + n);
            if (n > 0) {
                NavdyDeviceId deviceId = RemoteDeviceManager.getInstance().getDeviceId();
                if (deviceId == null) {
                    sLogger.w("restoreStack no device id");
                    this.notificationSavedList.clear();
                } else if (deviceId.equals(this.notificationSavedListDeviceId)) {
                    Iterator it = this.notificationSavedList.iterator();
                    while (it.hasNext()) {
                        Info info = (Info) it.next();
                        String id = info.notification.getId();
                        sLogger.w("restoreStack restored:" + id);
                        this.notificationIdMap.put(id, info);
                        this.notificationPriorityMap.put(info, info);
                    }
                    GlanceHandler.getInstance().restoreState();
                } else {
                    sLogger.w("restoreStack device id does not match current[" + deviceId + "] saved[" + this.notificationSavedListDeviceId + "]");
                    this.notificationSavedList.clear();
                    GlanceHandler.getInstance().clearState();
                }
            }
        }
    }

    public boolean isExpandedNotificationVisible() {
        View view = getExpandedNotificationView();
        if (view == null || view.getVisibility() != 0) {
            return false;
        }
        return true;
    }

    public boolean isExpanded() {
        return this.expandedWithStack;
    }

    private void animateOutExpandedView(boolean hideNotification, boolean quick) {
        animateOutExpandedView(hideNotification, this.currentNotification, quick);
    }

    private void animateOutExpandedView(boolean hideNotification, Info notificationObj, boolean quick) {
        if (isExpanded() || isExpandedNotificationVisible()) {
            runOperation(Operation.COLLAPSE_VIEW, false, hideNotification, quick, false, null, false);
        }
    }

    public boolean collapseNotification() {
        if (this.mainScreen == null) {
            getNotificationView();
            if (this.mainScreen == null) {
                return false;
            }
        }
        if (!this.mainScreen.isNotificationViewShowing() || this.mainScreen.isNotificationCollapsing() || this.mainScreen.isNotificationExpanding()) {
            return false;
        }
        hideNotification();
        return true;
    }

    public boolean expandNotification() {
        if (this.mainScreen == null) {
            getNotificationView();
            if (this.mainScreen == null) {
                return false;
            }
        }
        if (this.mainScreen.isNotificationViewShowing() || this.mainScreen.isNotificationExpanding() || !makeNotificationCurrent(true)) {
            return false;
        }
        showNotification();
        return true;
    }

    public boolean isNotificationViewShowing() {
        if (this.mainScreen == null) {
            getNotificationView();
            if (this.mainScreen == null) {
                return false;
            }
        }
        if (this.mainScreen.isNotificationViewShowing() || this.mainScreen.isNotificationExpanding()) {
            return true;
        }
        return false;
    }

    public String getTopNotificationId() {
        String id;
        synchronized (this.lockObj) {
            if (this.currentNotification != null) {
                id = this.currentNotification.notification.getId();
            } else if (this.notificationPriorityMap.size() > 0) {
                id = ((Info) this.notificationPriorityMap.lastKey()).notification.getId();
            } else {
                id = null;
            }
        }
        return id;
    }

    public void collapseExpandedNotification(boolean completely, boolean quickly) {
        this.notificationController.collapseNotification(completely, quickly);
    }

    public View getExpandedViewChild() {
        FrameLayout lyt = getExpandedNotificationView();
        if (lyt == null || lyt.getChildCount() == 0) {
            return null;
        }
        return lyt.getChildAt(0);
    }

    private void updateExpandedIndicator() {
        if (isExpanded() && !this.deleteAllGlances) {
            if (isAnimating()) {
                sLogger.v("updateExpandedIndicator animating no-op");
            } else {
                runOperation(Operation.UPDATE_INDICATOR, false, false, false, false, null, false);
            }
        }
    }

    private void updateExpandedIndicatorInternal() {
        View view = getExpandedNotificationView();
        if (isExpanded() && view.getTag() != null) {
            View child;
            int oldCount = this.notifIndicator.getItemCount();
            int currentItem = this.notifIndicator.getCurrentItem();
            boolean onDeleteIndex = isDeleteAllIndex(currentItem, oldCount);
            int currentCount = getNotificationCount();
            if (isPhoneNotifPresentAndNotAlive()) {
                currentCount--;
            }
            int n = currentCount + 2;
            int color = GlanceConstants.colorDeleteAll;
            if (currentItem >= n) {
                currentItem = n - 1;
            }
            if (this.stackCurrentNotification != null) {
                currentItem = getNotificationIndex(this.stackCurrentNotification);
                if (currentItem != -1) {
                    color = this.stackCurrentNotification.notification.getColor();
                    currentItem++;
                }
            }
            if (isDeleteAllIndex(currentItem, n)) {
                color = GlanceConstants.colorDeleteAll;
            } else if (onDeleteIndex) {
                Info newInfo = getNotificationByIndex(currentCount - 1);
                if (newInfo == null) {
                    color = GlanceConstants.colorDeleteAll;
                    currentItem = n - 1;
                } else {
                    this.lastStackCurrentNotification = newInfo;
                    updateExpandedIndicator(n, n - 1, newInfo.notification.getColor());
                    movePrevInternal(false);
                    return;
                }
            }
            updateExpandedIndicator(n, currentItem, color);
            if (this.showOn) {
                child = getExpandedViewChild();
            } else {
                if (this.notifView == null) {
                    getNotificationView();
                }
                child = this.notifView.getCurrentNotificationViewChild();
            }
            if (GlanceHelper.isDeleteAllView(child)) {
                TextView textView;
                if (this.showOn) {
                    textView = (TextView) child.findViewById(R.id.textView);
                } else {
                    textView = (TextView) child.findViewById(R.id.subTitle);
                }
                updateDeleteAllGlanceCount(textView, child);
            }
            runQueuedOperation();
        }
    }

    public void updateExpandedIndicator(int count, int currentItem, int color) {
        this.notifIndicator.setItemCount(count);
        this.notifIndicator.setCurrentItem(currentItem, color);
        displayScrollingIndicator(currentItem);
        sLogger.v("updateExpandedIndicator count =" + count + " currentitem=" + currentItem);
    }

    public int getExpandedIndicatorCurrentItem() {
        return this.notifIndicator.getCurrentItem();
    }

    public int getExpandedIndicatorCount() {
        return this.notifIndicator.getItemCount();
    }

    public Context getUIContext() {
        return getNotificationView().getContext();
    }

    public boolean handleKey(CustomKeyEvent event) {
        getNotificationView();
        sLogger.v("handleKey: running=" + this.operationRunning + " qsize:" + this.operationQueue.size() + " animating:" + this.isAnimating);
        if (this.isAnimating) {
            sLogger.v("handleKey: animating no-op");
            return false;
        }
        if (!this.operationRunning && isExpanded() && this.stackCurrentNotification != null && this.showOn && this.stackCurrentNotification.notification.supportScroll() && this.stackCurrentNotification.startCalled) {
            boolean scrollKey;
            if (event == CustomKeyEvent.LEFT || event == CustomKeyEvent.RIGHT) {
                scrollKey = true;
            } else {
                scrollKey = false;
            }
            if (scrollKey) {
                if (this.stackCurrentNotification.notification.onKey(event)) {
                    this.scrollState = ScrollState.HANDLED;
                    this.handler.removeCallbacks(this.ignoreScrollRunnable);
                    return true;
                }
                switch (this.scrollState) {
                    case NOT_HANDLED:
                        return true;
                    case HANDLED:
                        startScrollThresholdTimer();
                        return true;
                }
            }
        }
        this.scrollState = ScrollState.NONE;
        switch (event) {
            case LEFT:
                movePrevious(false);
                return true;
            case RIGHT:
                moveNext(false);
                return true;
            case SELECT:
                executeItem();
                return true;
            case POWER_BUTTON_LONG_PRESS:
                sLogger.v("power button click: show shutdown");
                this.pendingShutdownArgs = Reason.POWER_BUTTON.asBundle();
                collapseExpandedNotification(true, true);
                return true;
            default:
                return false;
        }
    }

    public void viewSwitchAnimation(View small, View large, Info notifInfo, boolean previous, int color) {
        final View expandViewOut = getExpandedViewChild();
        int pos = this.notifIndicator.getCurrentItem();
        if (previous) {
            pos--;
        } else {
            pos++;
        }
        final int item = pos;
        final int i = color;
        final Info info = notifInfo;
        Runnable endAction = new Runnable() {
            public void run() {
                INotification iNotification;
                synchronized (NotificationManager.this.lockObj) {
                    NotificationManager.this.notifIndicator.setCurrentItem(item, i);
                    if (NotificationManager.this.expandedWithStack) {
                        if (NotificationManager.this.stackCurrentNotification != null && NotificationManager.this.stackCurrentNotification.startCalled) {
                            NotificationManager.this.stackCurrentNotification.startCalled = false;
                            NotificationManager.this.stackCurrentNotification.notification.onStop();
                        }
                        if (info == null) {
                            NotificationManager.this.lastStackCurrentNotification = NotificationManager.this.stackCurrentNotification;
                        }
                        NotificationManager.this.stackCurrentNotification = info;
                        if (expandViewOut != null && GlanceHelper.isDeleteAllView(expandViewOut)) {
                            NotificationManager.this.expandedNotifView.removeView(expandViewOut);
                            NotificationManager.sLogger.v("delete all big view removed");
                            expandViewOut.setTag(null);
                            GlanceViewCache.putView(ViewType.BIG_TEXT, expandViewOut);
                        }
                        if (NotificationManager.sLogger.isLoggable(2)) {
                            NotificationManager.this.printPriorityMap();
                        }
                        if (NotificationManager.this.stackCurrentNotification != null) {
                            NotificationManager.this.stackCurrentNotification.notification.onExpandedNotificationSwitched();
                        }
                        NotificationManager.this.displayScrollingIndicator(item);
                    }
                    NotificationManager.this.runQueuedOperation();
                }
                String str = AnalyticsSupport.ANALYTICS_EVENT_GLANCE_ADVANCE;
                if (NotificationManager.this.stackCurrentNotification != null) {
                    iNotification = NotificationManager.this.stackCurrentNotification.notification;
                } else {
                    iNotification = null;
                }
                AnalyticsSupport.recordGlanceAction(str, iNotification, null);
            }
        };
        if (small != null) {
            int i2;
            final View notifViewOut = this.notifView.getCurrentNotificationViewChild();
            final ViewGroup notifViewOutParent = this.notifView.getNotificationContainer();
            if (previous) {
                i2 = -this.animationTranslation;
            } else {
                i2 = this.animationTranslation;
            }
            final Info info2 = notifInfo;
            final View view = large;
            final Runnable runnable = endAction;
            NotificationAnimator.animateNotifViews(small, notifViewOut, notifViewOutParent, i2, 250, 250, new Runnable() {
                public void run() {
                    if (info2 != null) {
                        AnimatorSet set = info2.notification.getViewSwitchAnimation(true);
                        if (set != null) {
                            set.setDuration(100);
                            set.start();
                        }
                    }
                    if (notifViewOut != null && GlanceHelper.isDeleteAllView(notifViewOut)) {
                        notifViewOutParent.removeView(notifViewOut);
                        NotificationManager.sLogger.v("delete all small view removed");
                        notifViewOut.setTag(null);
                        NotificationManager.this.cleanupView(notifViewOut);
                    }
                    if (view == null) {
                        runnable.run();
                    }
                }
            });
        }
        if (large != null) {
            NotificationAnimator.animateExpandedViews(large, expandViewOut, this.expandedNotifView, previous ? -this.animationTranslation : this.animationTranslation, 250, 250, endAction);
        }
        if (isExpanded()) {
            hideScrollingIndicator();
            if (this.stackCurrentNotification != null && this.stackCurrentNotification.notification.supportScroll()) {
                ((IScrollEvent) this.stackCurrentNotification.notification).setListener(null);
            }
        }
        AnimatorSet indicatorAnimator = this.notifIndicator.getItemMoveAnimator(item, color);
        if (indicatorAnimator != null) {
            indicatorAnimator.start();
        }
    }

    public void printPriorityMap() {
        synchronized (this.lockObj) {
            if (this.stackCurrentNotification == null) {
                sLogger.v("notif-debug-map stack current = null");
            } else {
                sLogger.v("notif-debug-map stack current = " + this.stackCurrentNotification.notification.getId());
            }
            if (this.currentNotification == null) {
                sLogger.v("notif-debug-map current = null");
            } else {
                sLogger.v("notif-debug-map current = " + this.currentNotification.notification.getId());
            }
            sLogger.v("notif-debug-map-start");
            for (Info info : this.notificationPriorityMap.keySet()) {
                sLogger.v("notif-debug [" + info.notification.getId() + "] priority[" + info.priority + "] counter [" + info.uniqueCounter + "]");
            }
            sLogger.v("notif-debug-map-stop");
        }
    }

    public boolean isCurrentItemDeleteAll() {
        if (this.notifIndicator == null) {
            return false;
        }
        return isDeleteAllIndex(this.notifIndicator.getCurrentItem(), this.notifIndicator.getItemCount());
    }

    private boolean isDeleteAllIndex(int index, int itemCount) {
        if (index == 0 || index == itemCount - 1) {
            return true;
        }
        return false;
    }

    private void deleteAllGlances() {
        synchronized (this.lockObj) {
            if (this.deleteAllGlances) {
                animateOutExpandedView(true, true);
            }
        }
    }

    private void removeAllNotification() {
        this.expandedWithStack = false;
        synchronized (this.lockObj) {
            if (this.currentNotification != null && this.currentNotification.startCalled && NotificationHelper.isNotificationRemovable(this.currentNotification.notification.getId())) {
                sLogger.v("removeAllNotification calling stop: " + this.currentNotification.notification.getId());
                this.currentNotification.startCalled = false;
                this.currentNotification.notification.onStop();
            }
            int n = this.notificationIdMap.size();
            sLogger.v("removeAllNotification:" + n);
            Info current = null;
            if (n > 0) {
                Iterator<String> iterator = this.notificationIdMap.keySet().iterator();
                while (iterator.hasNext()) {
                    String id = (String) iterator.next();
                    Info info = (Info) this.notificationIdMap.get(id);
                    if (NotificationHelper.isNotificationRemovable(id)) {
                        this.notificationPriorityMap.remove(info);
                        iterator.remove();
                        sLogger.v("removeAllNotification removed:" + id);
                    } else {
                        current = info;
                        sLogger.v("removeAllNotification left:" + id);
                    }
                }
            }
            this.stackCurrentNotification = null;
            this.lastStackCurrentNotification = null;
            this.currentNotification = current;
            postNotificationChange();
        }
    }

    @Subscribe
    public void onShowToast(ShowToast event) {
        if (this.deleteAllGlances && TextUtils.equals(event.name, GlanceHelper.DELETE_ALL_GLANCES_TOAST_ID)) {
            sLogger.v("showToast: called deleteAllGlances");
            deleteAllGlances();
        }
    }

    private int getNotificationIndex(Info info) {
        synchronized (this.lockObj) {
            int index = -1;
            for (Info info2 : this.notificationPriorityMap.descendingKeySet()) {
                index++;
                if (info2 == info) {
                    return index;
                }
            }
            return -1;
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private Info getNotificationByIndex(int index) {
        synchronized (this.lockObj) {
            if (index >= 0) {
                if (index < this.notificationPriorityMap.size()) {
                    int i = 0;
                    for (Info info : this.notificationPriorityMap.descendingKeySet()) {
                        if (i == index) {
                            return info;
                        }
                        i++;
                    }
                    return null;
                }
            }
        }
    }

    private void updateDeleteAllGlanceCount(TextView textView, View container) {
        if (getNotificationCount() - getNonRemovableNotifCount() == 0) {
            textView.setText(GlanceConstants.glancesCannotDelete);
            container.setTag(R.id.glance_can_delete, Integer.valueOf(1));
            return;
        }
        if (this.showOn) {
            textView.setText(this.resources.getString(R.string.glances_dismiss_all, new Object[]{Integer.valueOf(count)}));
        } else {
            textView.setText(this.resources.getString(R.string.count, new Object[]{Integer.valueOf(count)}));
        }
        container.setTag(R.id.glance_can_delete, null);
    }

    private void cleanupViews(NotificationView view) {
        sLogger.v("*** cleaning up expanded view");
        this.expandedWithStack = false;
        ViewGroup parent = getExpandedNotificationView();
        View child = getExpandedViewChild();
        if (child != null) {
            parent.removeView(child);
            if (GlanceHelper.isDeleteAllView(child)) {
                child.setTag(null);
                child.setAlpha(1.0f);
                GlanceViewCache.putView(ViewType.BIG_TEXT, child);
            }
            sLogger.v("*** removed expanded view");
        }
        ViewGroup parentNotif = this.notifView.getNotificationContainer();
        if (parentNotif.getChildCount() > 0) {
            View childNotif = parentNotif.getChildAt(0);
            parentNotif.removeView(childNotif);
            if (GlanceHelper.isDeleteAllView(childNotif)) {
                childNotif.setTag(null);
                cleanupView(childNotif);
            }
            sLogger.v("*** removed notif view");
        }
        if (this.stackCurrentNotification != null && this.stackCurrentNotification.startCalled) {
            this.stackCurrentNotification.startCalled = false;
            this.stackCurrentNotification.notification.onStop();
        }
        this.stackCurrentNotification = null;
        this.lastStackCurrentNotification = null;
        parent.setVisibility(8);
        CarouselIndicator indicator = getNotificationIndicator();
        if (indicator != null) {
            indicator.setVisibility(8);
            hideScrollingIndicator();
        }
        setNotificationColor();
        view.showNextNotificationColor();
    }

    private Info getHigherNotification(Info info) {
        while (true) {
            Info higher = (Info) this.notificationPriorityMap.higherKey(info);
            if (higher == null) {
                return null;
            }
            if (!(higher.notification instanceof CallNotification) || higher.notification.isAlive()) {
                return higher;
            }
            info = higher;
        }
    }

    private Info getHighestNotification() {
        Info info;
        synchronized (this.lockObj) {
            info = (Info) this.notificationPriorityMap.lastKey();
        }
        return info;
    }

    private Info getLowestNotification() {
        Info info;
        synchronized (this.lockObj) {
            info = (Info) this.notificationPriorityMap.firstKey();
        }
        return info;
    }

    private Info getLowerNotification(Info info) {
        while (true) {
            Info lower = (Info) this.notificationPriorityMap.lowerKey(info);
            if (lower == null) {
                return null;
            }
            if (!(lower.notification instanceof CallNotification) || lower.notification.isAlive()) {
                return lower;
            }
            info = lower;
        }
    }

    private Info getNotificationFromId(String str) {
        Info info;
        synchronized (this.lockObj) {
            info = (Info) this.notificationIdMap.get(str);
        }
        return info;
    }

    private boolean isPhoneNotifPresentAndNotAlive() {
        CallNotification callNotification = (CallNotification) getNotification(NotificationId.PHONE_CALL_NOTIFICATION_ID);
        if (callNotification == null || callNotification.isAlive()) {
            return false;
        }
        return true;
    }

    private int getNonRemovableNotifCount() {
        int count = 0;
        if (getNotification(NotificationId.PHONE_CALL_NOTIFICATION_ID) != null) {
            count = 0 + 1;
        }
        if (getNotification(NotificationId.TRAFFIC_REROUTE_NOTIFICATION_ID) != null) {
            return count + 1;
        }
        return count;
    }

    private boolean isNotifCurrent(String id) {
        if (this.notificationController.isExpandedWithStack() && this.expandedWithStack) {
            if (this.stackCurrentNotification != null && TextUtils.equals(id, this.stackCurrentNotification.notification.getId()) && this.stackCurrentNotification.startCalled) {
                return true;
            }
            return false;
        } else if (this.currentNotification != null && TextUtils.equals(id, this.currentNotification.notification.getId()) && this.currentNotification.startCalled) {
            return true;
        } else {
            return false;
        }
    }

    public void postNotificationChange() {
        this.bus.post(NOTIFICATION_CHANGE);
    }

    @Subscribe
    public void onCallEnded(CallEnded event) {
        if (this.notificationController.isExpandedWithStack()) {
            handleRemoveNotificationInExpandedMode(NotificationId.PHONE_CALL_NOTIFICATION_ID, false);
        } else {
            sLogger.v("phone-end");
        }
    }

    private void handleRemoveNotificationInExpandedMode(String id, boolean collapse) {
        if (isExpanded() && !this.deleteAllGlances) {
            runOperation(Operation.REMOVE_NOTIFICATION, false, false, false, false, id, collapse);
        }
    }

    private void handleRemoveNotificationInExpandedModeInternal(String id, boolean collapse) {
        if (isNotifCurrent(id)) {
            sLogger.v("notif-end: is current:" + id);
            if (collapse) {
                collapseNotificationInternal(false, false, true);
            }
        } else if (getNotificationCount() == 1) {
            sLogger.v("remove-end: no more glance");
            collapseExpandedViewInternal(true, true);
        } else {
            Logger logger;
            StringBuilder append;
            String str;
            sLogger.v("remove-end: not current, remove:" + id);
            removeNotificationfromStack(getNotificationFromId(id), true);
            if (this.lastStackCurrentNotification != null && TextUtils.equals(this.lastStackCurrentNotification.notification.getId(), id)) {
                if (this.stackCurrentNotification == null) {
                    this.lastStackCurrentNotification = getLowerNotification(this.lastStackCurrentNotification);
                    logger = sLogger;
                    append = new StringBuilder().append("after remove notif is:");
                    if (this.lastStackCurrentNotification == null) {
                        str = Constants.NULL_VERSION_ID;
                    } else {
                        str = this.lastStackCurrentNotification.notification.getId();
                    }
                    logger.v(append.append(str).toString());
                } else {
                    this.lastStackCurrentNotification = null;
                }
            }
            if (this.currentNotification != null && TextUtils.equals(this.currentNotification.notification.getId(), id)) {
                this.currentNotification = this.stackCurrentNotification;
                logger = sLogger;
                append = new StringBuilder().append("after remove current notif set:");
                if (this.currentNotification == null) {
                    str = Constants.NULL_VERSION_ID;
                } else {
                    str = this.currentNotification.notification.getId();
                }
                logger.v(append.append(str).toString());
            }
            postNotificationChange();
            updateExpandedIndicatorInternal();
        }
    }

    private void collapseNotificationInternal(boolean completely, boolean quickly, boolean internal) {
        sLogger.v("collapseNotificationInternal");
        if (getNotificationView() == null) {
            return;
        }
        if (!isExpanded() && !isExpandedNotificationVisible()) {
            return;
        }
        if (internal) {
            collapseExpandedViewInternal(completely, quickly);
        } else {
            animateOutExpandedView(completely, this.currentNotification, quickly);
        }
    }

    @Subscribe
    public void onCallAccepted(CallAccepted event) {
        sLogger.v("phone-start");
        updateExpandedIndicator();
    }

    public boolean isNotificationMarkedForRemoval(String id) {
        boolean z;
        synchronized (this.lockObj) {
            Info info = getNotificationFromId(id);
            if (info != null) {
                z = info.removed;
            } else {
                z = false;
            }
        }
        return z;
    }

    private void cleanupView(View view) {
        ViewType viewType = (ViewType) view.getTag(R.id.glance_view_type);
        view.setTag(R.id.glance_view_type, null);
        view.setTag(R.id.glance_can_delete, null);
        if (viewType != null) {
            switch (viewType) {
                case SMALL_IMAGE:
                    ((ImageView) view.findViewById(R.id.imageView)).setImageResource(0);
                    GlanceViewCache.putView(ViewType.SMALL_IMAGE, view);
                    return;
                case SMALL_GLANCE_MESSAGE:
                    TextView mainTitle = (TextView) view.findViewById(R.id.mainTitle);
                    mainTitle.setAlpha(1.0f);
                    mainTitle.setVisibility(0);
                    TextView subTitle = (TextView) view.findViewById(R.id.subTitle);
                    subTitle.setAlpha(1.0f);
                    subTitle.setVisibility(0);
                    View choiceLayout = view.findViewById(R.id.choiceLayout);
                    if (choiceLayout != null) {
                        choiceLayout.setAlpha(1.0f);
                        choiceLayout.setVisibility(0);
                        choiceLayout.setTag(null);
                        choiceLayout.setTag(R.id.message_prev_choice, null);
                        choiceLayout.setTag(R.id.message_secondary_screen, null);
                    }
                    InitialsImageView mainImage = (InitialsImageView) view.findViewById(R.id.mainImage);
                    mainImage.setImage(0, null, Style.DEFAULT);
                    mainImage.setTag(null);
                    ((ImageView) view.findViewById(R.id.sideImage)).setImageResource(0);
                    view.setAlpha(1.0f);
                    GlanceViewCache.putView(ViewType.SMALL_GLANCE_MESSAGE, view);
                    return;
                default:
                    return;
            }
        }
    }

    public void setExpandedStack(boolean b) {
        this.expandedWithStack = b;
    }

    public void movePrevious(boolean gesture) {
        if (!isExpanded() && !isExpandedNotificationVisible()) {
            return;
        }
        if (this.deleteAllGlances) {
            sLogger.v("movePrevious: no-op deleteGlances set");
        } else {
            runOperation(Operation.MOVE_PREV, false, false, false, gesture, null, false);
        }
    }

    public void moveNext(boolean gesture) {
        if (!isExpanded() && !isExpandedNotificationVisible()) {
            return;
        }
        if (this.deleteAllGlances) {
            sLogger.v("moveNext: no-op deleteGlances set");
        } else {
            runOperation(Operation.MOVE_NEXT, false, false, false, gesture, null, false);
        }
    }

    public void executeItem() {
        if (!isExpanded() && !isExpandedNotificationVisible()) {
            return;
        }
        if (this.deleteAllGlances) {
            sLogger.v("executeItem: no-op deleteGlances set");
        } else {
            runOperation(Operation.SELECT, false, false, false, false, null, false);
        }
    }

    private void runOperation(Operation operation, boolean ignoreCurrent, boolean hideNotification, boolean quick, boolean gesture, String id, boolean collapse) {
        if (ignoreCurrent || !this.operationRunning) {
            sLogger.v("run operation=" + operation);
            this.operationRunning = true;
            switch (operation) {
                case MOVE_NEXT:
                    moveNextInternal(gesture);
                    if (this.deleteAllGlances) {
                        AnalyticsSupport.recordGlanceAction(AnalyticsSupport.ANALYTICS_EVENT_GLANCE_DELETE_ALL, null, gesture ? "swipe" : "dial");
                        return;
                    } else {
                        AnalyticsSupport.setGlanceNavigationSource(gesture ? "swipe" : "dial");
                        return;
                    }
                case MOVE_PREV:
                    movePrevInternal(gesture);
                    AnalyticsSupport.setGlanceNavigationSource(gesture ? "swipe" : "dial");
                    return;
                case SELECT:
                    executeItemInternal();
                    if (this.deleteAllGlances) {
                        AnalyticsSupport.recordGlanceAction(AnalyticsSupport.ANALYTICS_EVENT_GLANCE_DELETE_ALL, null, gesture ? "swipe" : "dial");
                        return;
                    }
                    return;
                case COLLAPSE_VIEW:
                    collapseExpandedViewInternal(hideNotification, quick);
                    return;
                case UPDATE_INDICATOR:
                    updateExpandedIndicatorInternal();
                    return;
                case REMOVE_NOTIFICATION:
                    handleRemoveNotificationInExpandedModeInternal(id, collapse);
                    return;
                default:
                    return;
            }
        }
        if (this.operationQueue.size() >= 3) {
            switch (operation) {
                case MOVE_NEXT:
                case MOVE_PREV:
                    return;
            }
        }
        this.operationQueue.add(new OperationInfo(operation, hideNotification, quick, gesture, id, collapse));
    }

    private void runQueuedOperation() {
        if (this.operationQueue.size() > 0) {
            final OperationInfo operationInfo = (OperationInfo) this.operationQueue.remove();
            this.handler.post(new Runnable() {
                public void run() {
                    NotificationManager.this.runOperation(operationInfo.operation, true, operationInfo.hideNotification, operationInfo.quick, operationInfo.gesture, operationInfo.id, operationInfo.collapse);
                }
            });
            return;
        }
        sLogger.v("operationRunning=false");
        this.operationRunning = false;
    }

    public void clearOperationQueue() {
        sLogger.v("clearOperationQueue:" + this.operationQueue.size());
        this.operationQueue.clear();
        this.operationRunning = false;
    }

    private void moveNextInternal(boolean gesture) {
        boolean verbose = sLogger.isLoggable(2);
        if (verbose) {
            sLogger.v("moveNextInternal:" + gesture);
        }
        if (isExpanded()) {
            handleExpandedMove(CustomKeyEvent.RIGHT, gesture);
        } else if (isExpandedNotificationVisible()) {
            int current = this.notifIndicator.getCurrentItem();
            if (current == this.notifIndicator.getItemCount() - 1) {
                runQueuedOperation();
                if (verbose) {
                    sLogger.v("moveNextInternal:cannot go next");
                    return;
                }
                return;
            }
            current++;
            if (verbose) {
                sLogger.v("moveNextInternal:going to " + current);
            }
            viewSwitchAnimation(null, this.currentNotification.notification.getExpandedView(this.notifView.getContext(), Integer.valueOf(current)), null, false, this.currentNotification.notification.getExpandedViewIndicatorColor());
        }
    }

    private void movePrevInternal(boolean gesture) {
        boolean verbose = sLogger.isLoggable(2);
        if (verbose) {
            sLogger.v("movePrevInternal:" + gesture);
        }
        if (isExpanded()) {
            handleExpandedMove(CustomKeyEvent.LEFT, gesture);
        } else if (isExpandedNotificationVisible()) {
            int current = this.notifIndicator.getCurrentItem();
            int count = this.notifIndicator.getItemCount();
            if (current == 0) {
                runQueuedOperation();
                if (verbose) {
                    sLogger.v("movePrevInternal: cannot go prev");
                    return;
                }
                return;
            }
            current--;
            if (verbose) {
                sLogger.v("movePrevInternal: going to " + current);
            }
            viewSwitchAnimation(null, this.currentNotification.notification.getExpandedView(this.notifView.getContext(), Integer.valueOf(current)), null, true, GlanceConstants.colorWhite);
        }
    }

    private void executeItemInternal() {
        boolean verbose = sLogger.isLoggable(2);
        if (isExpanded()) {
            if (this.stackCurrentNotification != null) {
                if (verbose) {
                    sLogger.v("executeItemInternal: expanded :" + this.stackCurrentNotification);
                }
                clearOperationQueue();
                this.stackCurrentNotification.notification.onKey(CustomKeyEvent.SELECT);
                return;
            }
            View view;
            if (verbose) {
                sLogger.v("executeItemInternal: expanded check for delete:");
            }
            if (this.showOn) {
                view = getExpandedViewChild();
            } else {
                view = this.notifView.getCurrentNotificationViewChild();
            }
            if (!GlanceHelper.isDeleteAllView(view)) {
                return;
            }
            if (view.getTag(R.id.glance_can_delete) != null) {
                sLogger.v("executeItemInternal: no glance to delete");
                runQueuedOperation();
                return;
            }
            if (verbose) {
                sLogger.v("executeItemInternal: delete all");
            }
            this.deleteAllGlances = true;
            clearOperationQueue();
            GlanceHelper.showGlancesDeletedToast();
            sLogger.v("executeItemInternal: show delete all toast");
        } else if (isExpandedNotificationVisible()) {
            if (verbose) {
                sLogger.v("executeItemInternal: expandedNoStack :" + this.currentNotification);
            }
            if (this.currentNotification != null) {
                clearOperationQueue();
                this.currentNotification.notification.onKey(CustomKeyEvent.SELECT);
                return;
            }
            runQueuedOperation();
        }
    }

    private void collapseExpandedViewInternal(final boolean hideNotification, boolean quick) {
        sLogger.v("collapseExpandedViewInternal");
        this.isAnimating = true;
        clearOperationQueue();
        final Runnable runnable = new Runnable() {
            public void run() {
                NotificationManager.this.getExpandedNotificationCoverView().setVisibility(8);
                NotificationManager.this.expandedWithStack = false;
                boolean hide = hideNotification;
                if (NotificationManager.this.stackCurrentNotification != null && NotificationManager.this.stackCurrentNotification != NotificationManager.this.currentNotification) {
                    NotificationManager.sLogger.v("stackchange:current and stack are diff");
                    if (NotificationManager.this.currentNotification != null && NotificationManager.this.currentNotification.startCalled) {
                        NotificationManager.sLogger.v("stackchange:stopping current:" + NotificationManager.this.currentNotification.notification.getId());
                        NotificationManager.this.currentNotification.startCalled = false;
                        NotificationManager.this.currentNotification.notification.onStop();
                    }
                    NotificationManager.this.currentNotification = NotificationManager.this.stackCurrentNotification;
                    if (NotificationManager.this.currentNotification != null) {
                        NotificationManager.this.notifView.switchNotfication(NotificationManager.this.currentNotification.notification);
                        if (NotificationManager.this.currentNotification.removed) {
                            hide = true;
                            NotificationManager.sLogger.v("stackchange:hide-diff-remove");
                        } else if (!NotificationManager.this.currentNotification.startCalled) {
                            NotificationManager.sLogger.v("stackchange:starting stacked:" + NotificationManager.this.currentNotification.notification.getId());
                            NotificationManager.this.currentNotification.notification.getView(NotificationManager.this.notifView.getContext());
                            NotificationManager.this.currentNotification.startCalled = true;
                            NotificationManager.this.currentNotification.notification.onStart(NotificationManager.this.notificationController);
                        }
                        if (!hide) {
                            NotificationManager.sLogger.v("calling onExpandedNotificationEvent-collapse-");
                            NotificationManager.this.currentNotification.notification.onExpandedNotificationEvent(Mode.COLLAPSE);
                        }
                    }
                    NotificationManager.this.setNotificationColor();
                    NotificationManager.this.notifView.showNextNotificationColor();
                } else if (NotificationManager.this.currentNotification != null) {
                    if (NotificationManager.this.currentNotification.removed) {
                        hide = true;
                        NotificationManager.sLogger.v("stackchange:hide-remove");
                    } else if (!(NotificationManager.this.currentNotification.startCalled || NotificationManager.this.currentNotification.removed)) {
                        NotificationManager.this.currentNotification.notification.getView(NotificationManager.this.notifView.getContext());
                        NotificationManager.this.currentNotification.startCalled = true;
                        NotificationManager.this.currentNotification.notification.onStart(NotificationManager.this.notificationController);
                    }
                }
                NotificationManager.this.stackCurrentNotification = null;
                NotificationManager.this.lastStackCurrentNotification = null;
                NotificationManager.this.clearOperationQueue();
                if (hide) {
                    NotificationManager.this.hideNotificationInternal();
                } else {
                    NotificationManager.this.isAnimating = false;
                }
            }
        };
        final ViewGroup parent = getExpandedNotificationView();
        final View child = getExpandedViewChild();
        Runnable expandOutRunnable = new Runnable() {
            public void run() {
                NotificationManager.this.hideScrollingIndicator();
                if (child != null) {
                    parent.removeView(child);
                }
                if (GlanceHelper.isDeleteAllView(child)) {
                    child.setTag(null);
                    child.setAlpha(1.0f);
                    GlanceViewCache.putView(ViewType.BIG_TEXT, child);
                }
                NotificationAnimator.animateExpandedViewOut(NotificationManager.this.notifView, NotificationManager.this.uiStateManager.getMainPanelWidth() - NotificationManager.this.uiStateManager.getSidePanelWidth(), NotificationManager.this.expandedNotifView, NotificationManager.this.notifIndicator, runnable, NotificationManager.this.currentNotification, NotificationManager.this.showOn, NotificationManager.this.isExpandedNotificationVisible());
            }
        };
        if ((this.showOn || isExpandedNotificationVisible()) && child != null) {
            NotificationAnimator.animateChildViewOut(child, quick ? 0 : 250, expandOutRunnable);
        } else {
            expandOutRunnable.run();
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void handleExpandedMove(CustomKeyEvent event, boolean gesture) {
        sLogger.v("handleExpandedMove key:" + event + " gesture:" + gesture);
        Info info = null;
        boolean prev = false;
        boolean deleteAll = false;
        boolean runQueuedOperation = false;
        int currentItem = this.notifIndicator.getCurrentItem();
        int indicatorCount = this.notifIndicator.getItemCount();
        try {
            View view;
            switch (event) {
                case LEFT:
                    if (currentItem == 0) {
                        runQueuedOperation = true;
                        if (sLogger.isLoggable(2)) {
                            sLogger.v("handleExpandedMove: cannot go up");
                        }
                        if (1 != null) {
                            runQueuedOperation();
                            return;
                        }
                        return;
                    }
                    if (currentItem == 1) {
                        deleteAll = true;
                    } else {
                        synchronized (this.lockObj) {
                            if (this.stackCurrentNotification != null) {
                                info = getHigherNotification(this.stackCurrentNotification);
                                if (info == null) {
                                    deleteAll = true;
                                    if (sLogger.isLoggable(2)) {
                                        sLogger.v("handleExpandedMove: cannot go up, no notification left");
                                    }
                                }
                            } else {
                                info = this.lastStackCurrentNotification;
                                this.lastStackCurrentNotification = null;
                                if (info == null) {
                                    sLogger.v("handleExpandedMove: no prev notification");
                                    if (currentItem == indicatorCount - 1) {
                                        info = getLowestNotification();
                                        sLogger.v("handleExpandedMove: got lowest:" + info);
                                    }
                                }
                            }
                        }
                    }
                    prev = true;
                case RIGHT:
                    if (currentItem == indicatorCount - 1) {
                        if (gesture) {
                            if (this.showOn) {
                                view = getExpandedViewChild();
                            } else {
                                view = this.notifView.getCurrentNotificationViewChild();
                            }
                            if (view.getTag(R.id.glance_can_delete) != null) {
                                sLogger.v("no glance to delete:right");
                                runQueuedOperation = true;
                            } else {
                                this.deleteAllGlances = true;
                                clearOperationQueue();
                                GlanceHelper.showGlancesDeletedToast();
                                if (sLogger.isLoggable(2)) {
                                    sLogger.v("handleExpandedMove: delete all glances");
                                }
                            }
                        } else {
                            if (sLogger.isLoggable(2)) {
                                sLogger.v("handleExpandedMove: cannot go down");
                            }
                            runQueuedOperation = true;
                        }
                        if (runQueuedOperation) {
                            runQueuedOperation();
                            return;
                        }
                        return;
                    } else if (currentItem == indicatorCount - 2) {
                        deleteAll = true;
                    } else {
                        synchronized (this.lockObj) {
                            if (this.stackCurrentNotification != null) {
                                info = getLowerNotification(this.stackCurrentNotification);
                            } else {
                                info = this.lastStackCurrentNotification;
                                this.lastStackCurrentNotification = null;
                                if (info == null) {
                                    sLogger.v("handleExpandedMove: no next notification");
                                    if (currentItem == 0) {
                                        info = getHighestNotification();
                                        sLogger.v("handleExpandedMove: got highest:" + info);
                                    }
                                }
                            }
                        }
                    }
                case SELECT:
                    if (this.stackCurrentNotification != null) {
                        this.stackCurrentNotification.notification.onKey(event);
                        if (null != null) {
                            runQueuedOperation();
                            return;
                        }
                        return;
                    }
                    if (this.showOn) {
                        view = getExpandedViewChild();
                    } else {
                        view = this.notifView.getCurrentNotificationViewChild();
                    }
                    if (GlanceHelper.isDeleteAllView(view)) {
                        if (view.getTag(R.id.glance_can_delete) != null) {
                            sLogger.v("no glance to delete:select");
                            if (true) {
                                runQueuedOperation();
                                return;
                            }
                            return;
                        }
                        this.deleteAllGlances = true;
                        GlanceHelper.showGlancesDeletedToast();
                        if (null != null) {
                            runQueuedOperation();
                            return;
                        }
                        return;
                    } else if (null != null) {
                        runQueuedOperation();
                        return;
                    } else {
                        return;
                    }
                default:
                    Context uiContext = this.notifView.getContext();
                    final boolean previous = prev;
                    if (info != null || deleteAll) {
                        synchronized (this.lockObj) {
                            View s;
                            View l;
                            int c;
                            final Info notifInfo = info;
                            if (info != null) {
                                if (sLogger.isLoggable(2)) {
                                    sLogger.v("handleExpandedMove: going to " + notifInfo.notification.getId());
                                }
                                s = notifInfo.notification.getView(uiContext);
                                if (this.showOn) {
                                    l = notifInfo.notification.getExpandedView(uiContext, null);
                                } else {
                                    l = null;
                                }
                                notifInfo.startCalled = true;
                                notifInfo.notification.onStart(this.notificationController);
                                c = notifInfo.notification.getColor();
                            } else {
                                ImageView imageView;
                                if (sLogger.isLoggable(2)) {
                                    sLogger.v("handleExpandedMove: going to delete all");
                                }
                                if (this.showOn) {
                                    s = GlanceViewCache.getView(ViewType.SMALL_IMAGE, uiContext);
                                    s.setTag(R.id.glance_view_type, ViewType.SMALL_IMAGE);
                                    imageView = (ImageView) s.findViewById(R.id.imageView);
                                } else {
                                    s = GlanceViewCache.getView(ViewType.SMALL_GLANCE_MESSAGE, uiContext);
                                    s.setTag(R.id.glance_view_type, ViewType.SMALL_GLANCE_MESSAGE);
                                    imageView = (ImageView) s.findViewById(R.id.mainImage);
                                    TextView mainTitle = (TextView) s.findViewById(R.id.mainTitle);
                                    mainTitle.setText(GlanceConstants.glancesDismissAll);
                                    mainTitle.setAlpha(1.0f);
                                    mainTitle.setVisibility(0);
                                    TextView subTitle = (TextView) s.findViewById(R.id.subTitle);
                                    if (getNotificationCount() - getNonRemovableNotifCount() == 0) {
                                        s.setTag(R.id.glance_can_delete, Integer.valueOf(1));
                                    } else {
                                        s.setTag(R.id.glance_can_delete, null);
                                    }
                                    subTitle.setText(this.resources.getString(R.string.count, new Object[]{Integer.valueOf(count)}));
                                    subTitle.setAlpha(1.0f);
                                    subTitle.setVisibility(0);
                                    View choiceLayout = s.findViewById(R.id.choiceLayout);
                                    if (choiceLayout != null) {
                                        choiceLayout.setVisibility(4);
                                    }
                                }
                                s.setTag(GlanceConstants.DELETE_ALL_VIEW_TAG);
                                imageView.setImageResource(R.drawable.icon_dismiss_all_glances);
                                if (this.showOn) {
                                    l = GlanceViewCache.getView(ViewType.BIG_TEXT, uiContext);
                                    l.setTag(GlanceConstants.DELETE_ALL_VIEW_TAG);
                                    updateDeleteAllGlanceCount((TextView) l.findViewById(R.id.textView), l);
                                } else {
                                    l = null;
                                }
                                c = GlanceConstants.colorDeleteAll;
                            }
                            final View small = s;
                            final View large = l;
                            final int color = c;
                            if (this.stackCurrentNotification != null) {
                                AnimatorSet set = this.stackCurrentNotification.notification.getViewSwitchAnimation(false);
                                if (set != null) {
                                    set.addListener(new DefaultAnimationListener() {
                                        public void onAnimationEnd(Animator animation) {
                                            NotificationManager.this.viewSwitchAnimation(small, large, notifInfo, previous, color);
                                        }
                                    });
                                    set.setDuration(100);
                                    set.start();
                                } else {
                                    viewSwitchAnimation(small, large, notifInfo, previous, color);
                                }
                            } else {
                                viewSwitchAnimation(small, large, notifInfo, previous, color);
                            }
                        }
                    } else {
                        if (sLogger.isLoggable(2)) {
                            sLogger.v("handleExpandedMove: no-op");
                        }
                        runQueuedOperation = true;
                    }
                    if (runQueuedOperation) {
                        runQueuedOperation();
                        return;
                    }
                    return;
            }
        } catch (Throwable th) {
            if (runQueuedOperation) {
                runQueuedOperation();
            }
        }
        if (runQueuedOperation) {
            runQueuedOperation();
        }
    }

    private void displayScrollingIndicator(final int index) {
        if (!isExpanded() || !this.showOn) {
            return;
        }
        if (this.stackCurrentNotification == null || !this.stackCurrentNotification.notification.supportScroll()) {
            hideScrollingIndicator();
            return;
        }
        final IScrollEvent scroll = this.stackCurrentNotification.notification;
        this.notifScrollIndicator.setBackgroundColor(this.stackCurrentNotification.notification.getColor());
        this.notifIndicator.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                RectF rectF = NotificationManager.this.notifIndicator.getItemPos(index);
                if (rectF != null) {
                    if (rectF.left != 0.0f || rectF.top != 0.0f) {
                        NotificationManager.this.setNotifScrollIndicatorXY(rectF, scroll);
                    } else {
                        return;
                    }
                }
                NotificationManager.this.notifIndicator.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
    }

    private void hideScrollingIndicator() {
        if (this.notifScrollIndicator != null) {
            ((View) this.notifScrollIndicator.getParent()).setVisibility(8);
        }
    }

    private void setNotifScrollIndicatorXY(RectF rectF, IScrollEvent scroll) {
        scroll.setListener(this.scrollProgress);
        this.notifScrollIndicator.setCurrentItem(1);
        View parent = (View) this.notifScrollIndicator.getParent();
        parent.setX(this.notifIndicator.getX() + ((float) GlanceConstants.scrollingIndicatorLeftPadding));
        parent.setY(((this.notifIndicator.getY() + rectF.top) + ((float) (GlanceConstants.scrollingIndicatorCircleSize / 2))) - ((float) (GlanceConstants.scrollingIndicatorHeight / 2)));
        parent.setVisibility(0);
    }

    private void startScrollThresholdTimer() {
        this.scrollState = ScrollState.NOT_HANDLED;
        this.handler.removeCallbacks(this.ignoreScrollRunnable);
        this.handler.postDelayed(this.ignoreScrollRunnable, 750);
    }

    public void enableNotifications(boolean enable) {
        sLogger.v("notification enabled[" + enable + "]");
        this.enable = enable;
        enablePhoneNotifications(enable);
    }

    public boolean isNotificationsEnabled() {
        return this.enable;
    }

    public void enablePhoneNotifications(boolean enable) {
        sLogger.v("notification enabled phone[" + enable + "]");
        this.enablePhoneCalls = enable;
    }

    public boolean isPhoneNotificationsEnabled() {
        return this.enablePhoneCalls;
    }

    public boolean isAnimating() {
        return this.isAnimating;
    }

    @Subscribe
    public void onNotificationPreference(NotificationPreferences preferences) {
        if (!preferences.enabled.booleanValue()) {
            clearAllGlances();
        }
    }

    @Subscribe
    public void onClearGlances(ClearGlances clearGlances) {
        clearAllGlances();
    }

    private void clearAllGlances() {
        sLogger.v("glances turned off");
        if (isExpanded() || isExpandedNotificationVisible()) {
            sLogger.v("glances turned off: expanded mode");
            this.deleteAllGlances = true;
            clearOperationQueue();
            collapseExpandedNotification(true, true);
        } else if (isNotificationViewShowing()) {
            sLogger.v("glances turned off: notif showing");
            this.deleteAllGlances = true;
            collapseNotification();
        } else {
            sLogger.v("glances turned off: notif not showing");
            synchronized (this.lockObj) {
                removeAllNotification();
            }
        }
    }

    public void hideNotificationCoverView() {
        View layoutCover = getExpandedNotificationCoverView();
        if (layoutCover != null && layoutCover.getVisibility() == 0) {
            sLogger.v("hideNotificationCoverView");
            layoutCover.setVisibility(8);
        }
    }

    private boolean isScreenHighPriority(Screen screen) {
        switch (screen) {
            case SCREEN_DESTINATION_PICKER:
                return true;
            default:
                return false;
        }
    }
}
