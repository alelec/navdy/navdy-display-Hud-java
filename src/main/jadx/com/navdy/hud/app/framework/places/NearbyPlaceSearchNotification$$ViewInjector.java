package com.navdy.hud.app.framework.places;

import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.ChoiceLayout2;
import com.navdy.hud.app.ui.component.image.IconColorImageView;

public class NearbyPlaceSearchNotification$$ViewInjector {
    public static void inject(Finder finder, NearbyPlaceSearchNotification target, Object source) {
        target.title = (TextView) finder.findRequiredView(source, R.id.title, "field 'title'");
        target.subTitle = (TextView) finder.findRequiredView(source, R.id.subTitle, "field 'subTitle'");
        target.iconColorView = (IconColorImageView) finder.findRequiredView(source, R.id.iconColorView, "field 'iconColorView'");
        target.resultsCount = (TextView) finder.findRequiredView(source, R.id.resultsCount, "field 'resultsCount'");
        target.resultsLabel = (TextView) finder.findRequiredView(source, R.id.resultsLabel, "field 'resultsLabel'");
        target.statusBadge = (ImageView) finder.findRequiredView(source, R.id.iconSpinner, "field 'statusBadge'");
        target.iconSide = (IconColorImageView) finder.findRequiredView(source, R.id.iconSide, "field 'iconSide'");
        target.choiceLayout = (ChoiceLayout2) finder.findRequiredView(source, R.id.choiceLayout, "field 'choiceLayout'");
    }

    public static void reset(NearbyPlaceSearchNotification target) {
        target.title = null;
        target.subTitle = null;
        target.iconColorView = null;
        target.resultsCount = null;
        target.resultsLabel = null;
        target.statusBadge = null;
        target.iconSide = null;
        target.choiceLayout = null;
    }
}
