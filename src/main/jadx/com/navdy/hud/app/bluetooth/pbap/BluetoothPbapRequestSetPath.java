package com.navdy.hud.app.bluetooth.pbap;

import android.util.Log;
import com.navdy.hud.app.bluetooth.obex.ClientSession;
import com.navdy.hud.app.bluetooth.obex.HeaderSet;
import com.navdy.hud.app.bluetooth.obex.ResponseCodes;
import java.io.IOException;

final class BluetoothPbapRequestSetPath extends BluetoothPbapRequest {
    private static final String TAG = "BTPbapReqSetPath";
    private SetPathDir mDir;

    private enum SetPathDir {
        ROOT,
        UP,
        DOWN
    }

    public BluetoothPbapRequestSetPath(String name) {
        this.mDir = SetPathDir.DOWN;
        this.mHeaderSet.setHeader(1, name);
    }

    public BluetoothPbapRequestSetPath(boolean goUp) {
        this.mHeaderSet.setEmptyNameHeader();
        if (goUp) {
            this.mDir = SetPathDir.UP;
        } else {
            this.mDir = SetPathDir.ROOT;
        }
    }

    public void execute(ClientSession session) {
        Log.v(TAG, "execute");
        HeaderSet hs = null;
        try {
            switch (this.mDir) {
                case ROOT:
                case DOWN:
                    hs = session.setPath(this.mHeaderSet, false, false);
                    break;
                case UP:
                    hs = session.setPath(this.mHeaderSet, true, false);
                    break;
            }
            this.mResponseCode = hs.getResponseCode();
        } catch (IOException e) {
            this.mResponseCode = ResponseCodes.OBEX_HTTP_INTERNAL_ERROR;
        }
    }
}
