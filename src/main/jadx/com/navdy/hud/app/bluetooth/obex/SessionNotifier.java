package com.navdy.hud.app.bluetooth.obex;

import java.io.IOException;

public interface SessionNotifier {
    ObexSession acceptAndOpen(ServerRequestHandler serverRequestHandler) throws IOException;

    ObexSession acceptAndOpen(ServerRequestHandler serverRequestHandler, Authenticator authenticator) throws IOException;
}
