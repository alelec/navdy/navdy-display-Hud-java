package com.navdy.hud.app.bluetooth.vcard.exception;

public class VCardInvalidLineException extends VCardException {
    public VCardInvalidLineException(String message) {
        super(message);
    }
}
