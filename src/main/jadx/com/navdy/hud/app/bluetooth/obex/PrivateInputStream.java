package com.navdy.hud.app.bluetooth.obex;

import java.io.IOException;
import java.io.InputStream;

public final class PrivateInputStream extends InputStream {
    private byte[] mData = new byte[0];
    private int mIndex = 0;
    private boolean mOpen = true;
    private BaseStream mParent;

    public PrivateInputStream(BaseStream p) {
        this.mParent = p;
    }

    public synchronized int available() throws IOException {
        ensureOpen();
        return this.mData.length - this.mIndex;
    }

    public synchronized int read() throws IOException {
        int i;
        ensureOpen();
        while (this.mData.length == this.mIndex) {
            if (!this.mParent.continueOperation(true, true)) {
                i = -1;
                break;
            }
        }
        byte[] bArr = this.mData;
        int i2 = this.mIndex;
        this.mIndex = i2 + 1;
        i = bArr[i2] & 255;
        return i;
    }

    public int read(byte[] b) throws IOException {
        return read(b, 0, b.length);
    }

    public synchronized int read(byte[] b, int offset, int length) throws IOException {
        int i;
        if (b == null) {
            throw new IOException("buffer is null");
        }
        if ((offset | length) >= 0) {
            if (length <= b.length - offset) {
                ensureOpen();
                int currentDataLength = this.mData.length - this.mIndex;
                int remainReadLength = length;
                int offset1 = offset;
                int result = 0;
                while (currentDataLength <= remainReadLength) {
                    System.arraycopy(this.mData, this.mIndex, b, offset1, currentDataLength);
                    this.mIndex += currentDataLength;
                    offset1 += currentDataLength;
                    result += currentDataLength;
                    remainReadLength -= currentDataLength;
                    if (this.mParent.continueOperation(true, true)) {
                        currentDataLength = this.mData.length - this.mIndex;
                    } else {
                        i = result == 0 ? -1 : result;
                    }
                }
                if (remainReadLength > 0) {
                    System.arraycopy(this.mData, this.mIndex, b, offset1, remainReadLength);
                    this.mIndex += remainReadLength;
                    result += remainReadLength;
                }
                i = result;
            }
        }
        throw new ArrayIndexOutOfBoundsException("index outof bound");
        return i;
    }

    public synchronized void writeBytes(byte[] body, int start) {
        byte[] temp = new byte[((body.length - start) + (this.mData.length - this.mIndex))];
        System.arraycopy(this.mData, this.mIndex, temp, 0, this.mData.length - this.mIndex);
        System.arraycopy(body, start, temp, this.mData.length - this.mIndex, body.length - start);
        this.mData = temp;
        this.mIndex = 0;
        notifyAll();
    }

    private void ensureOpen() throws IOException {
        this.mParent.ensureOpen();
        if (!this.mOpen) {
            throw new IOException("Input stream is closed");
        }
    }

    public void close() throws IOException {
        this.mOpen = false;
        this.mParent.streamClosed(true);
    }
}
