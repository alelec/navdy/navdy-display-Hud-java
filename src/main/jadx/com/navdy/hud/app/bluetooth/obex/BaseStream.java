package com.navdy.hud.app.bluetooth.obex;

import java.io.IOException;

public interface BaseStream {
    boolean continueOperation(boolean z, boolean z2) throws IOException;

    void ensureNotDone() throws IOException;

    void ensureOpen() throws IOException;

    void streamClosed(boolean z) throws IOException;
}
