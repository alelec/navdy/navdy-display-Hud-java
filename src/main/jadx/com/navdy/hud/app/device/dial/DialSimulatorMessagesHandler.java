package com.navdy.hud.app.device.dial;

import android.app.Instrumentation;
import android.content.Context;
import android.view.KeyEvent;
import android.view.ViewConfiguration;
import com.navdy.service.library.events.input.DialSimulationEvent;
import com.navdy.service.library.events.input.DialSimulationEvent.DialInputAction;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Subscribe;
import java.util.HashMap;
import java.util.Map;

public class DialSimulatorMessagesHandler {
    private static final int LONG_PRESS_KEY_UP_FLAGS_SIMULATION = 640;
    private static final Map<DialInputAction, Integer> SIMULATION_TO_KEY_EVENT = new HashMap<DialInputAction, Integer>() {
        {
            put(DialInputAction.DIAL_LEFT_TURN, Integer.valueOf(21));
            put(DialInputAction.DIAL_RIGHT_TURN, Integer.valueOf(22));
            put(DialInputAction.DIAL_CLICK, Integer.valueOf(66));
        }
    };
    private int longPressTimeThreshold = ViewConfiguration.getLongPressTimeout();

    private class SimulatedKeyEventRunnable implements Runnable {
        private DialInputAction eventAction;

        public SimulatedKeyEventRunnable(DialInputAction eventAction) {
            this.eventAction = eventAction;
        }

        public void run() {
            Instrumentation instrumentation = new Instrumentation();
            if (this.eventAction == DialInputAction.DIAL_LONG_CLICK) {
                KeyEvent event = new KeyEvent(0, 66);
                int initFlags = event.getFlags();
                event = KeyEvent.changeTimeRepeat(event, event.getEventTime(), 1, initFlags | 128);
                instrumentation.sendKeySync(event);
                instrumentation.sendKeySync(KeyEvent.changeAction(KeyEvent.changeTimeRepeat(event, event.getEventTime() + ((long) DialSimulatorMessagesHandler.this.longPressTimeThreshold), 0, initFlags | DialSimulatorMessagesHandler.LONG_PRESS_KEY_UP_FLAGS_SIMULATION), 1));
                return;
            }
            instrumentation.sendKeyDownUpSync(((Integer) DialSimulatorMessagesHandler.SIMULATION_TO_KEY_EVENT.get(this.eventAction)).intValue());
        }
    }

    public DialSimulatorMessagesHandler(Context context) {
        ViewConfiguration.get(context);
    }

    @Subscribe
    public void onDialSimulationEvent(DialSimulationEvent event) {
        if (event != null) {
            TaskManager.getInstance().execute(new SimulatedKeyEventRunnable(event.dialAction), 1);
        }
    }
}
