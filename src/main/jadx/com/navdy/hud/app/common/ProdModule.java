package com.navdy.hud.app.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport.NotificationReceiver;
import com.navdy.hud.app.analytics.AnalyticsSupport.TemperatureReporter;
import com.navdy.hud.app.analytics.TelemetryDataManager;
import com.navdy.hud.app.ancs.AncsServiceConnector;
import com.navdy.hud.app.config.SettingsManager;
import com.navdy.hud.app.debug.DebugReceiver;
import com.navdy.hud.app.debug.DriveRecorder;
import com.navdy.hud.app.device.PowerManager;
import com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.network.NetworkStateManager;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.phonecall.CallManager;
import com.navdy.hud.app.framework.trips.TripManager;
import com.navdy.hud.app.framework.voice.VoiceSearchHandler;
import com.navdy.hud.app.gesture.GestureServiceConnector;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.hud.app.manager.MusicManager;
import com.navdy.hud.app.manager.PairingManager;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.manager.UpdateReminderManager;
import com.navdy.hud.app.maps.MapSchemeController;
import com.navdy.hud.app.maps.MapsEventHandler;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.maps.here.HereRegionManager;
import com.navdy.hud.app.obd.CarMDVinDecoder;
import com.navdy.hud.app.obd.ObdManager;
import com.navdy.hud.app.profile.DriverProfileManager;
import com.navdy.hud.app.service.ConnectionHandler;
import com.navdy.hud.app.service.ConnectionServiceProxy;
import com.navdy.hud.app.service.GestureVideosSyncService;
import com.navdy.hud.app.service.ObdCANBusDataUploadService;
import com.navdy.hud.app.service.ShutdownMonitor;
import com.navdy.hud.app.service.pandora.PandoraManager;
import com.navdy.hud.app.storage.PathManager;
import com.navdy.hud.app.storage.cache.DiskLruCacheAdapter;
import com.navdy.hud.app.storage.cache.MessageCache;
import com.navdy.hud.app.ui.activity.Main.Presenter.DeferredServices;
import com.navdy.hud.app.ui.component.mainmenu.MusicMenu2;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.util.FeatureUtil;
import com.navdy.hud.app.util.MusicArtworkCache;
import com.navdy.hud.app.util.OTAUpdateService;
import com.navdy.hud.app.util.ReportIssueService;
import com.navdy.hud.app.view.EngineTemperaturePresenter;
import com.navdy.hud.app.view.MusicWidgetPresenter;
import com.navdy.service.library.events.audio.MusicCollectionResponse;
import com.navdy.service.library.network.http.HttpManager;
import com.navdy.service.library.network.http.IHttpManager;
import com.squareup.otto.Bus;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;

@Module(injects = {ConnectionServiceProxy.class, HereMapsManager.class, HereRegionManager.class, ObdManager.class, OTAUpdateService.class, MapsEventHandler.class, RemoteDeviceManager.class, MapSchemeController.class, DriverProfileHelper.class, NotificationManager.class, TripManager.class, DeferredServices.class, DriverProfileHelper.class, DebugReceiver.class, NetworkStateManager.class, ShutdownMonitor.class, ReportIssueService.class, CarMDVinDecoder.class, DriveRecorder.class, HudApplication.class, SpeedManager.class, MusicWidgetPresenter.class, MusicMenu2.class, UpdateReminderManager.class, TemperatureReporter.class, NotificationReceiver.class, GestureVideosSyncService.class, ObdCANBusDataUploadService.class, EngineTemperaturePresenter.class}, library = true)
public class ProdModule {
    public static final int MUSIC_DATA_CACHE_SIZE = 2000000;
    public static final String MUSIC_RESPONSE_CACHE = "MUSIC_RESPONSE_CACHE";
    public static final String PREF_NAME = "App";
    private Context context;

    public ProdModule(Context context) {
        this.context = context;
    }

    @Singleton
    @Provides
    Bus provideBus() {
        return new MainThreadBus();
    }

    @Singleton
    @Provides
    SharedPreferences provideSharedPreferences() {
        PreferenceManager.setDefaultValues(this.context, PREF_NAME, 0, R.xml.developer_preferences, true);
        return this.context.getSharedPreferences(PREF_NAME, 0);
    }

    @Singleton
    @Provides
    ConnectionServiceProxy provideConnectionServiceProxy(Bus bus) {
        return new ConnectionServiceProxy(this.context, bus);
    }

    @Singleton
    @Provides
    TimeHelper provideTimeHelper(Bus bus) {
        return new TimeHelper(this.context, bus);
    }

    @Singleton
    @Provides
    PowerManager providePowerManager(Bus bus, SharedPreferences preferences) {
        return new PowerManager(bus, this.context, preferences);
    }

    @Singleton
    @Provides
    SettingsManager provideSettingsManager(Bus bus, SharedPreferences preferences) {
        return new SettingsManager(bus, preferences);
    }

    @Singleton
    @Provides
    AncsServiceConnector provideAncsServiceConnector(Bus bus) {
        return new AncsServiceConnector(this.context, bus);
    }

    @Singleton
    @Provides
    InputManager provideInputManager(Bus bus, PowerManager powerManager, UIStateManager uiStateManager) {
        return new InputManager(bus, powerManager, uiStateManager);
    }

    @Singleton
    @Provides
    DialSimulatorMessagesHandler provideDialSimulatorMessagesHandler() {
        return new DialSimulatorMessagesHandler(this.context);
    }

    @Singleton
    @Provides
    CallManager provideCallManager(Bus bus) {
        return new CallManager(bus, this.context);
    }

    @Singleton
    @Provides
    PandoraManager providePandoraManager(Bus bus) {
        return new PandoraManager(bus, this.context.getResources());
    }

    @Singleton
    @Provides
    MusicManager provideMusicManager(MessageCache<MusicCollectionResponse> musicCollectionResponseMessageCache, Bus bus, UIStateManager uiStateManager, PandoraManager pandoraManager, MusicArtworkCache artworkCache) {
        return new MusicManager(musicCollectionResponseMessageCache, bus, this.context.getResources(), uiStateManager, pandoraManager, artworkCache);
    }

    @Singleton
    @Provides
    ConnectionHandler provideConnectionHandler(ConnectionServiceProxy connectionServiceProxy, PowerManager powerManager, DriverProfileManager driverProfileManager, UIStateManager uiStateManager, TimeHelper timeHelper) {
        return new ConnectionHandler(this.context, connectionServiceProxy, powerManager, driverProfileManager, uiStateManager, timeHelper);
    }

    @Singleton
    @Provides
    GestureServiceConnector provideGestureServiceConnector(Bus bus, PowerManager powerManager) {
        return new GestureServiceConnector(bus, powerManager);
    }

    @Singleton
    @Provides
    UIStateManager provideUIStateManager() {
        return new UIStateManager();
    }

    @Singleton
    @Provides
    PathManager providePathManager() {
        return PathManager.getInstance();
    }

    @Singleton
    @Provides
    DriverProfileManager provideDriverProfileManager(Bus bus, PathManager pathManager, TimeHelper timeHelper) {
        return new DriverProfileManager(bus, pathManager, timeHelper);
    }

    @Singleton
    @Provides
    TripManager provideTripManager(Bus bus, SharedPreferences preferences) {
        return new TripManager(bus, preferences);
    }

    @Singleton
    @Provides
    PairingManager providePairingManager() {
        return new PairingManager();
    }

    @Singleton
    @Provides
    IHttpManager provideHttpManager() {
        return new HttpManager();
    }

    @Singleton
    @Provides
    DriveRecorder provideObdDataReceorder(Bus bus, ConnectionHandler connectionHandler) {
        return new DriveRecorder(bus, connectionHandler);
    }

    @Singleton
    @Provides
    MusicArtworkCache provideArtworkCache() {
        return new MusicArtworkCache();
    }

    @Singleton
    @Provides
    VoiceSearchHandler provideVoiceSearchHandler(Bus bus, FeatureUtil featureUtil) {
        return new VoiceSearchHandler(this.context, bus, featureUtil);
    }

    @Singleton
    @Provides
    FeatureUtil provideFeatureUtil(SharedPreferences sharedPreferences) {
        return new FeatureUtil(sharedPreferences);
    }

    @Singleton
    @Provides
    TelemetryDataManager provideTelemetryDataManager(UIStateManager uiStateManager, PowerManager powerManager, Bus bus, SharedPreferences sharedPreferences, TripManager tripManager) {
        return new TelemetryDataManager(uiStateManager, powerManager, bus, sharedPreferences, tripManager);
    }

    @Singleton
    @Provides
    MessageCache<MusicCollectionResponse> provideMusicCollectionResponseMessageCache(PathManager pathManager) {
        return new MessageCache(new DiskLruCacheAdapter(MUSIC_RESPONSE_CACHE, pathManager.getMusicDiskCacheFolder(), MUSIC_DATA_CACHE_SIZE), MusicCollectionResponse.class);
    }
}
