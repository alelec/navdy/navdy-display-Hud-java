package com.navdy.hud.app.common;

import android.content.SharedPreferences;
import com.navdy.hud.app.analytics.TelemetryDataManager;
import com.navdy.hud.app.ancs.AncsServiceConnector;
import com.navdy.hud.app.config.SettingsManager;
import com.navdy.hud.app.debug.DriveRecorder;
import com.navdy.hud.app.device.PowerManager;
import com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler;
import com.navdy.hud.app.framework.phonecall.CallManager;
import com.navdy.hud.app.framework.trips.TripManager;
import com.navdy.hud.app.framework.voice.VoiceSearchHandler;
import com.navdy.hud.app.gesture.GestureServiceConnector;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.hud.app.manager.MusicManager;
import com.navdy.hud.app.manager.PairingManager;
import com.navdy.hud.app.profile.DriverProfileManager;
import com.navdy.hud.app.service.ConnectionHandler;
import com.navdy.hud.app.service.ConnectionServiceProxy;
import com.navdy.hud.app.service.pandora.PandoraManager;
import com.navdy.hud.app.storage.PathManager;
import com.navdy.hud.app.storage.cache.MessageCache;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.util.FeatureUtil;
import com.navdy.hud.app.util.MusicArtworkCache;
import com.navdy.service.library.events.audio.MusicCollectionResponse;
import com.navdy.service.library.network.http.IHttpManager;
import com.squareup.otto.Bus;
import dagger.internal.Binding;
import dagger.internal.BindingsGroup;
import dagger.internal.Linker;
import dagger.internal.ModuleAdapter;
import dagger.internal.ProvidesBinding;
import java.util.Set;
import javax.inject.Provider;

public final class ProdModule$$ModuleAdapter extends ModuleAdapter<ProdModule> {
    private static final Class<?>[] INCLUDES = new Class[0];
    private static final String[] INJECTS = new String[]{"members/com.navdy.hud.app.service.ConnectionServiceProxy", "members/com.navdy.hud.app.maps.here.HereMapsManager", "members/com.navdy.hud.app.maps.here.HereRegionManager", "members/com.navdy.hud.app.obd.ObdManager", "members/com.navdy.hud.app.util.OTAUpdateService", "members/com.navdy.hud.app.maps.MapsEventHandler", "members/com.navdy.hud.app.manager.RemoteDeviceManager", "members/com.navdy.hud.app.maps.MapSchemeController", "members/com.navdy.hud.app.framework.DriverProfileHelper", "members/com.navdy.hud.app.framework.notifications.NotificationManager", "members/com.navdy.hud.app.framework.trips.TripManager", "members/com.navdy.hud.app.ui.activity.Main$Presenter$DeferredServices", "members/com.navdy.hud.app.framework.DriverProfileHelper", "members/com.navdy.hud.app.debug.DebugReceiver", "members/com.navdy.hud.app.framework.network.NetworkStateManager", "members/com.navdy.hud.app.service.ShutdownMonitor", "members/com.navdy.hud.app.util.ReportIssueService", "members/com.navdy.hud.app.obd.CarMDVinDecoder", "members/com.navdy.hud.app.debug.DriveRecorder", "members/com.navdy.hud.app.HudApplication", "members/com.navdy.hud.app.manager.SpeedManager", "members/com.navdy.hud.app.view.MusicWidgetPresenter", "members/com.navdy.hud.app.ui.component.mainmenu.MusicMenu2", "members/com.navdy.hud.app.manager.UpdateReminderManager", "members/com.navdy.hud.app.analytics.AnalyticsSupport$TemperatureReporter", "members/com.navdy.hud.app.analytics.AnalyticsSupport$NotificationReceiver", "members/com.navdy.hud.app.service.GestureVideosSyncService", "members/com.navdy.hud.app.service.ObdCANBusDataUploadService", "members/com.navdy.hud.app.view.EngineTemperaturePresenter"};
    private static final Class<?>[] STATIC_INJECTIONS = new Class[0];

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideAncsServiceConnectorProvidesAdapter extends ProvidesBinding<AncsServiceConnector> implements Provider<AncsServiceConnector> {
        private Binding<Bus> bus;
        private final ProdModule module;

        public ProvideAncsServiceConnectorProvidesAdapter(ProdModule module) {
            super("com.navdy.hud.app.ancs.AncsServiceConnector", true, "com.navdy.hud.app.common.ProdModule", "provideAncsServiceConnector");
            this.module = module;
            setLibrary(true);
        }

        public void attach(Linker linker) {
            this.bus = linker.requestBinding("com.squareup.otto.Bus", ProdModule.class, getClass().getClassLoader());
        }

        public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> set) {
            getBindings.add(this.bus);
        }

        public AncsServiceConnector get() {
            return this.module.provideAncsServiceConnector((Bus) this.bus.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideArtworkCacheProvidesAdapter extends ProvidesBinding<MusicArtworkCache> implements Provider<MusicArtworkCache> {
        private final ProdModule module;

        public ProvideArtworkCacheProvidesAdapter(ProdModule module) {
            super("com.navdy.hud.app.util.MusicArtworkCache", true, "com.navdy.hud.app.common.ProdModule", "provideArtworkCache");
            this.module = module;
            setLibrary(true);
        }

        public MusicArtworkCache get() {
            return this.module.provideArtworkCache();
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideBusProvidesAdapter extends ProvidesBinding<Bus> implements Provider<Bus> {
        private final ProdModule module;

        public ProvideBusProvidesAdapter(ProdModule module) {
            super("com.squareup.otto.Bus", true, "com.navdy.hud.app.common.ProdModule", "provideBus");
            this.module = module;
            setLibrary(true);
        }

        public Bus get() {
            return this.module.provideBus();
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideCallManagerProvidesAdapter extends ProvidesBinding<CallManager> implements Provider<CallManager> {
        private Binding<Bus> bus;
        private final ProdModule module;

        public ProvideCallManagerProvidesAdapter(ProdModule module) {
            super("com.navdy.hud.app.framework.phonecall.CallManager", true, "com.navdy.hud.app.common.ProdModule", "provideCallManager");
            this.module = module;
            setLibrary(true);
        }

        public void attach(Linker linker) {
            this.bus = linker.requestBinding("com.squareup.otto.Bus", ProdModule.class, getClass().getClassLoader());
        }

        public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> set) {
            getBindings.add(this.bus);
        }

        public CallManager get() {
            return this.module.provideCallManager((Bus) this.bus.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideConnectionHandlerProvidesAdapter extends ProvidesBinding<ConnectionHandler> implements Provider<ConnectionHandler> {
        private Binding<ConnectionServiceProxy> connectionServiceProxy;
        private Binding<DriverProfileManager> driverProfileManager;
        private final ProdModule module;
        private Binding<PowerManager> powerManager;
        private Binding<TimeHelper> timeHelper;
        private Binding<UIStateManager> uiStateManager;

        public ProvideConnectionHandlerProvidesAdapter(ProdModule module) {
            super("com.navdy.hud.app.service.ConnectionHandler", true, "com.navdy.hud.app.common.ProdModule", "provideConnectionHandler");
            this.module = module;
            setLibrary(true);
        }

        public void attach(Linker linker) {
            this.connectionServiceProxy = linker.requestBinding("com.navdy.hud.app.service.ConnectionServiceProxy", ProdModule.class, getClass().getClassLoader());
            this.powerManager = linker.requestBinding("com.navdy.hud.app.device.PowerManager", ProdModule.class, getClass().getClassLoader());
            this.driverProfileManager = linker.requestBinding("com.navdy.hud.app.profile.DriverProfileManager", ProdModule.class, getClass().getClassLoader());
            this.uiStateManager = linker.requestBinding("com.navdy.hud.app.ui.framework.UIStateManager", ProdModule.class, getClass().getClassLoader());
            this.timeHelper = linker.requestBinding("com.navdy.hud.app.common.TimeHelper", ProdModule.class, getClass().getClassLoader());
        }

        public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> set) {
            getBindings.add(this.connectionServiceProxy);
            getBindings.add(this.powerManager);
            getBindings.add(this.driverProfileManager);
            getBindings.add(this.uiStateManager);
            getBindings.add(this.timeHelper);
        }

        public ConnectionHandler get() {
            return this.module.provideConnectionHandler((ConnectionServiceProxy) this.connectionServiceProxy.get(), (PowerManager) this.powerManager.get(), (DriverProfileManager) this.driverProfileManager.get(), (UIStateManager) this.uiStateManager.get(), (TimeHelper) this.timeHelper.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideConnectionServiceProxyProvidesAdapter extends ProvidesBinding<ConnectionServiceProxy> implements Provider<ConnectionServiceProxy> {
        private Binding<Bus> bus;
        private final ProdModule module;

        public ProvideConnectionServiceProxyProvidesAdapter(ProdModule module) {
            super("com.navdy.hud.app.service.ConnectionServiceProxy", true, "com.navdy.hud.app.common.ProdModule", "provideConnectionServiceProxy");
            this.module = module;
            setLibrary(true);
        }

        public void attach(Linker linker) {
            this.bus = linker.requestBinding("com.squareup.otto.Bus", ProdModule.class, getClass().getClassLoader());
        }

        public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> set) {
            getBindings.add(this.bus);
        }

        public ConnectionServiceProxy get() {
            return this.module.provideConnectionServiceProxy((Bus) this.bus.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideDialSimulatorMessagesHandlerProvidesAdapter extends ProvidesBinding<DialSimulatorMessagesHandler> implements Provider<DialSimulatorMessagesHandler> {
        private final ProdModule module;

        public ProvideDialSimulatorMessagesHandlerProvidesAdapter(ProdModule module) {
            super("com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler", true, "com.navdy.hud.app.common.ProdModule", "provideDialSimulatorMessagesHandler");
            this.module = module;
            setLibrary(true);
        }

        public DialSimulatorMessagesHandler get() {
            return this.module.provideDialSimulatorMessagesHandler();
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideDriverProfileManagerProvidesAdapter extends ProvidesBinding<DriverProfileManager> implements Provider<DriverProfileManager> {
        private Binding<Bus> bus;
        private final ProdModule module;
        private Binding<PathManager> pathManager;
        private Binding<TimeHelper> timeHelper;

        public ProvideDriverProfileManagerProvidesAdapter(ProdModule module) {
            super("com.navdy.hud.app.profile.DriverProfileManager", true, "com.navdy.hud.app.common.ProdModule", "provideDriverProfileManager");
            this.module = module;
            setLibrary(true);
        }

        public void attach(Linker linker) {
            this.bus = linker.requestBinding("com.squareup.otto.Bus", ProdModule.class, getClass().getClassLoader());
            this.pathManager = linker.requestBinding("com.navdy.hud.app.storage.PathManager", ProdModule.class, getClass().getClassLoader());
            this.timeHelper = linker.requestBinding("com.navdy.hud.app.common.TimeHelper", ProdModule.class, getClass().getClassLoader());
        }

        public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> set) {
            getBindings.add(this.bus);
            getBindings.add(this.pathManager);
            getBindings.add(this.timeHelper);
        }

        public DriverProfileManager get() {
            return this.module.provideDriverProfileManager((Bus) this.bus.get(), (PathManager) this.pathManager.get(), (TimeHelper) this.timeHelper.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideFeatureUtilProvidesAdapter extends ProvidesBinding<FeatureUtil> implements Provider<FeatureUtil> {
        private final ProdModule module;
        private Binding<SharedPreferences> sharedPreferences;

        public ProvideFeatureUtilProvidesAdapter(ProdModule module) {
            super("com.navdy.hud.app.util.FeatureUtil", true, "com.navdy.hud.app.common.ProdModule", "provideFeatureUtil");
            this.module = module;
            setLibrary(true);
        }

        public void attach(Linker linker) {
            this.sharedPreferences = linker.requestBinding("android.content.SharedPreferences", ProdModule.class, getClass().getClassLoader());
        }

        public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> set) {
            getBindings.add(this.sharedPreferences);
        }

        public FeatureUtil get() {
            return this.module.provideFeatureUtil((SharedPreferences) this.sharedPreferences.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideGestureServiceConnectorProvidesAdapter extends ProvidesBinding<GestureServiceConnector> implements Provider<GestureServiceConnector> {
        private Binding<Bus> bus;
        private final ProdModule module;
        private Binding<PowerManager> powerManager;

        public ProvideGestureServiceConnectorProvidesAdapter(ProdModule module) {
            super("com.navdy.hud.app.gesture.GestureServiceConnector", true, "com.navdy.hud.app.common.ProdModule", "provideGestureServiceConnector");
            this.module = module;
            setLibrary(true);
        }

        public void attach(Linker linker) {
            this.bus = linker.requestBinding("com.squareup.otto.Bus", ProdModule.class, getClass().getClassLoader());
            this.powerManager = linker.requestBinding("com.navdy.hud.app.device.PowerManager", ProdModule.class, getClass().getClassLoader());
        }

        public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> set) {
            getBindings.add(this.bus);
            getBindings.add(this.powerManager);
        }

        public GestureServiceConnector get() {
            return this.module.provideGestureServiceConnector((Bus) this.bus.get(), (PowerManager) this.powerManager.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideHttpManagerProvidesAdapter extends ProvidesBinding<IHttpManager> implements Provider<IHttpManager> {
        private final ProdModule module;

        public ProvideHttpManagerProvidesAdapter(ProdModule module) {
            super("com.navdy.service.library.network.http.IHttpManager", true, "com.navdy.hud.app.common.ProdModule", "provideHttpManager");
            this.module = module;
            setLibrary(true);
        }

        public IHttpManager get() {
            return this.module.provideHttpManager();
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideInputManagerProvidesAdapter extends ProvidesBinding<InputManager> implements Provider<InputManager> {
        private Binding<Bus> bus;
        private final ProdModule module;
        private Binding<PowerManager> powerManager;
        private Binding<UIStateManager> uiStateManager;

        public ProvideInputManagerProvidesAdapter(ProdModule module) {
            super("com.navdy.hud.app.manager.InputManager", true, "com.navdy.hud.app.common.ProdModule", "provideInputManager");
            this.module = module;
            setLibrary(true);
        }

        public void attach(Linker linker) {
            this.bus = linker.requestBinding("com.squareup.otto.Bus", ProdModule.class, getClass().getClassLoader());
            this.powerManager = linker.requestBinding("com.navdy.hud.app.device.PowerManager", ProdModule.class, getClass().getClassLoader());
            this.uiStateManager = linker.requestBinding("com.navdy.hud.app.ui.framework.UIStateManager", ProdModule.class, getClass().getClassLoader());
        }

        public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> set) {
            getBindings.add(this.bus);
            getBindings.add(this.powerManager);
            getBindings.add(this.uiStateManager);
        }

        public InputManager get() {
            return this.module.provideInputManager((Bus) this.bus.get(), (PowerManager) this.powerManager.get(), (UIStateManager) this.uiStateManager.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideMusicCollectionResponseMessageCacheProvidesAdapter extends ProvidesBinding<MessageCache<MusicCollectionResponse>> implements Provider<MessageCache<MusicCollectionResponse>> {
        private final ProdModule module;
        private Binding<PathManager> pathManager;

        public ProvideMusicCollectionResponseMessageCacheProvidesAdapter(ProdModule module) {
            super("com.navdy.hud.app.storage.cache.MessageCache<com.navdy.service.library.events.audio.MusicCollectionResponse>", true, "com.navdy.hud.app.common.ProdModule", "provideMusicCollectionResponseMessageCache");
            this.module = module;
            setLibrary(true);
        }

        public void attach(Linker linker) {
            this.pathManager = linker.requestBinding("com.navdy.hud.app.storage.PathManager", ProdModule.class, getClass().getClassLoader());
        }

        public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> set) {
            getBindings.add(this.pathManager);
        }

        public MessageCache<MusicCollectionResponse> get() {
            return this.module.provideMusicCollectionResponseMessageCache((PathManager) this.pathManager.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideMusicManagerProvidesAdapter extends ProvidesBinding<MusicManager> implements Provider<MusicManager> {
        private Binding<MusicArtworkCache> artworkCache;
        private Binding<Bus> bus;
        private final ProdModule module;
        private Binding<MessageCache<MusicCollectionResponse>> musicCollectionResponseMessageCache;
        private Binding<PandoraManager> pandoraManager;
        private Binding<UIStateManager> uiStateManager;

        public ProvideMusicManagerProvidesAdapter(ProdModule module) {
            super("com.navdy.hud.app.manager.MusicManager", true, "com.navdy.hud.app.common.ProdModule", "provideMusicManager");
            this.module = module;
            setLibrary(true);
        }

        public void attach(Linker linker) {
            this.musicCollectionResponseMessageCache = linker.requestBinding("com.navdy.hud.app.storage.cache.MessageCache<com.navdy.service.library.events.audio.MusicCollectionResponse>", ProdModule.class, getClass().getClassLoader());
            this.bus = linker.requestBinding("com.squareup.otto.Bus", ProdModule.class, getClass().getClassLoader());
            this.uiStateManager = linker.requestBinding("com.navdy.hud.app.ui.framework.UIStateManager", ProdModule.class, getClass().getClassLoader());
            this.pandoraManager = linker.requestBinding("com.navdy.hud.app.service.pandora.PandoraManager", ProdModule.class, getClass().getClassLoader());
            this.artworkCache = linker.requestBinding("com.navdy.hud.app.util.MusicArtworkCache", ProdModule.class, getClass().getClassLoader());
        }

        public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> set) {
            getBindings.add(this.musicCollectionResponseMessageCache);
            getBindings.add(this.bus);
            getBindings.add(this.uiStateManager);
            getBindings.add(this.pandoraManager);
            getBindings.add(this.artworkCache);
        }

        public MusicManager get() {
            return this.module.provideMusicManager((MessageCache) this.musicCollectionResponseMessageCache.get(), (Bus) this.bus.get(), (UIStateManager) this.uiStateManager.get(), (PandoraManager) this.pandoraManager.get(), (MusicArtworkCache) this.artworkCache.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideObdDataReceorderProvidesAdapter extends ProvidesBinding<DriveRecorder> implements Provider<DriveRecorder> {
        private Binding<Bus> bus;
        private Binding<ConnectionHandler> connectionHandler;
        private final ProdModule module;

        public ProvideObdDataReceorderProvidesAdapter(ProdModule module) {
            super("com.navdy.hud.app.debug.DriveRecorder", true, "com.navdy.hud.app.common.ProdModule", "provideObdDataReceorder");
            this.module = module;
            setLibrary(true);
        }

        public void attach(Linker linker) {
            this.bus = linker.requestBinding("com.squareup.otto.Bus", ProdModule.class, getClass().getClassLoader());
            this.connectionHandler = linker.requestBinding("com.navdy.hud.app.service.ConnectionHandler", ProdModule.class, getClass().getClassLoader());
        }

        public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> set) {
            getBindings.add(this.bus);
            getBindings.add(this.connectionHandler);
        }

        public DriveRecorder get() {
            return this.module.provideObdDataReceorder((Bus) this.bus.get(), (ConnectionHandler) this.connectionHandler.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvidePairingManagerProvidesAdapter extends ProvidesBinding<PairingManager> implements Provider<PairingManager> {
        private final ProdModule module;

        public ProvidePairingManagerProvidesAdapter(ProdModule module) {
            super("com.navdy.hud.app.manager.PairingManager", true, "com.navdy.hud.app.common.ProdModule", "providePairingManager");
            this.module = module;
            setLibrary(true);
        }

        public PairingManager get() {
            return this.module.providePairingManager();
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvidePandoraManagerProvidesAdapter extends ProvidesBinding<PandoraManager> implements Provider<PandoraManager> {
        private Binding<Bus> bus;
        private final ProdModule module;

        public ProvidePandoraManagerProvidesAdapter(ProdModule module) {
            super("com.navdy.hud.app.service.pandora.PandoraManager", true, "com.navdy.hud.app.common.ProdModule", "providePandoraManager");
            this.module = module;
            setLibrary(true);
        }

        public void attach(Linker linker) {
            this.bus = linker.requestBinding("com.squareup.otto.Bus", ProdModule.class, getClass().getClassLoader());
        }

        public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> set) {
            getBindings.add(this.bus);
        }

        public PandoraManager get() {
            return this.module.providePandoraManager((Bus) this.bus.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvidePathManagerProvidesAdapter extends ProvidesBinding<PathManager> implements Provider<PathManager> {
        private final ProdModule module;

        public ProvidePathManagerProvidesAdapter(ProdModule module) {
            super("com.navdy.hud.app.storage.PathManager", true, "com.navdy.hud.app.common.ProdModule", "providePathManager");
            this.module = module;
            setLibrary(true);
        }

        public PathManager get() {
            return this.module.providePathManager();
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvidePowerManagerProvidesAdapter extends ProvidesBinding<PowerManager> implements Provider<PowerManager> {
        private Binding<Bus> bus;
        private final ProdModule module;
        private Binding<SharedPreferences> preferences;

        public ProvidePowerManagerProvidesAdapter(ProdModule module) {
            super("com.navdy.hud.app.device.PowerManager", true, "com.navdy.hud.app.common.ProdModule", "providePowerManager");
            this.module = module;
            setLibrary(true);
        }

        public void attach(Linker linker) {
            this.bus = linker.requestBinding("com.squareup.otto.Bus", ProdModule.class, getClass().getClassLoader());
            this.preferences = linker.requestBinding("android.content.SharedPreferences", ProdModule.class, getClass().getClassLoader());
        }

        public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> set) {
            getBindings.add(this.bus);
            getBindings.add(this.preferences);
        }

        public PowerManager get() {
            return this.module.providePowerManager((Bus) this.bus.get(), (SharedPreferences) this.preferences.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideSettingsManagerProvidesAdapter extends ProvidesBinding<SettingsManager> implements Provider<SettingsManager> {
        private Binding<Bus> bus;
        private final ProdModule module;
        private Binding<SharedPreferences> preferences;

        public ProvideSettingsManagerProvidesAdapter(ProdModule module) {
            super("com.navdy.hud.app.config.SettingsManager", true, "com.navdy.hud.app.common.ProdModule", "provideSettingsManager");
            this.module = module;
            setLibrary(true);
        }

        public void attach(Linker linker) {
            this.bus = linker.requestBinding("com.squareup.otto.Bus", ProdModule.class, getClass().getClassLoader());
            this.preferences = linker.requestBinding("android.content.SharedPreferences", ProdModule.class, getClass().getClassLoader());
        }

        public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> set) {
            getBindings.add(this.bus);
            getBindings.add(this.preferences);
        }

        public SettingsManager get() {
            return this.module.provideSettingsManager((Bus) this.bus.get(), (SharedPreferences) this.preferences.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideSharedPreferencesProvidesAdapter extends ProvidesBinding<SharedPreferences> implements Provider<SharedPreferences> {
        private final ProdModule module;

        public ProvideSharedPreferencesProvidesAdapter(ProdModule module) {
            super("android.content.SharedPreferences", true, "com.navdy.hud.app.common.ProdModule", "provideSharedPreferences");
            this.module = module;
            setLibrary(true);
        }

        public SharedPreferences get() {
            return this.module.provideSharedPreferences();
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideTelemetryDataManagerProvidesAdapter extends ProvidesBinding<TelemetryDataManager> implements Provider<TelemetryDataManager> {
        private Binding<Bus> bus;
        private final ProdModule module;
        private Binding<PowerManager> powerManager;
        private Binding<SharedPreferences> sharedPreferences;
        private Binding<TripManager> tripManager;
        private Binding<UIStateManager> uiStateManager;

        public ProvideTelemetryDataManagerProvidesAdapter(ProdModule module) {
            super("com.navdy.hud.app.analytics.TelemetryDataManager", true, "com.navdy.hud.app.common.ProdModule", "provideTelemetryDataManager");
            this.module = module;
            setLibrary(true);
        }

        public void attach(Linker linker) {
            this.uiStateManager = linker.requestBinding("com.navdy.hud.app.ui.framework.UIStateManager", ProdModule.class, getClass().getClassLoader());
            this.powerManager = linker.requestBinding("com.navdy.hud.app.device.PowerManager", ProdModule.class, getClass().getClassLoader());
            this.bus = linker.requestBinding("com.squareup.otto.Bus", ProdModule.class, getClass().getClassLoader());
            this.sharedPreferences = linker.requestBinding("android.content.SharedPreferences", ProdModule.class, getClass().getClassLoader());
            this.tripManager = linker.requestBinding("com.navdy.hud.app.framework.trips.TripManager", ProdModule.class, getClass().getClassLoader());
        }

        public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> set) {
            getBindings.add(this.uiStateManager);
            getBindings.add(this.powerManager);
            getBindings.add(this.bus);
            getBindings.add(this.sharedPreferences);
            getBindings.add(this.tripManager);
        }

        public TelemetryDataManager get() {
            return this.module.provideTelemetryDataManager((UIStateManager) this.uiStateManager.get(), (PowerManager) this.powerManager.get(), (Bus) this.bus.get(), (SharedPreferences) this.sharedPreferences.get(), (TripManager) this.tripManager.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideTimeHelperProvidesAdapter extends ProvidesBinding<TimeHelper> implements Provider<TimeHelper> {
        private Binding<Bus> bus;
        private final ProdModule module;

        public ProvideTimeHelperProvidesAdapter(ProdModule module) {
            super("com.navdy.hud.app.common.TimeHelper", true, "com.navdy.hud.app.common.ProdModule", "provideTimeHelper");
            this.module = module;
            setLibrary(true);
        }

        public void attach(Linker linker) {
            this.bus = linker.requestBinding("com.squareup.otto.Bus", ProdModule.class, getClass().getClassLoader());
        }

        public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> set) {
            getBindings.add(this.bus);
        }

        public TimeHelper get() {
            return this.module.provideTimeHelper((Bus) this.bus.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideTripManagerProvidesAdapter extends ProvidesBinding<TripManager> implements Provider<TripManager> {
        private Binding<Bus> bus;
        private final ProdModule module;
        private Binding<SharedPreferences> preferences;

        public ProvideTripManagerProvidesAdapter(ProdModule module) {
            super("com.navdy.hud.app.framework.trips.TripManager", true, "com.navdy.hud.app.common.ProdModule", "provideTripManager");
            this.module = module;
            setLibrary(true);
        }

        public void attach(Linker linker) {
            this.bus = linker.requestBinding("com.squareup.otto.Bus", ProdModule.class, getClass().getClassLoader());
            this.preferences = linker.requestBinding("android.content.SharedPreferences", ProdModule.class, getClass().getClassLoader());
        }

        public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> set) {
            getBindings.add(this.bus);
            getBindings.add(this.preferences);
        }

        public TripManager get() {
            return this.module.provideTripManager((Bus) this.bus.get(), (SharedPreferences) this.preferences.get());
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideUIStateManagerProvidesAdapter extends ProvidesBinding<UIStateManager> implements Provider<UIStateManager> {
        private final ProdModule module;

        public ProvideUIStateManagerProvidesAdapter(ProdModule module) {
            super("com.navdy.hud.app.ui.framework.UIStateManager", true, "com.navdy.hud.app.common.ProdModule", "provideUIStateManager");
            this.module = module;
            setLibrary(true);
        }

        public UIStateManager get() {
            return this.module.provideUIStateManager();
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideVoiceSearchHandlerProvidesAdapter extends ProvidesBinding<VoiceSearchHandler> implements Provider<VoiceSearchHandler> {
        private Binding<Bus> bus;
        private Binding<FeatureUtil> featureUtil;
        private final ProdModule module;

        public ProvideVoiceSearchHandlerProvidesAdapter(ProdModule module) {
            super("com.navdy.hud.app.framework.voice.VoiceSearchHandler", true, "com.navdy.hud.app.common.ProdModule", "provideVoiceSearchHandler");
            this.module = module;
            setLibrary(true);
        }

        public void attach(Linker linker) {
            this.bus = linker.requestBinding("com.squareup.otto.Bus", ProdModule.class, getClass().getClassLoader());
            this.featureUtil = linker.requestBinding("com.navdy.hud.app.util.FeatureUtil", ProdModule.class, getClass().getClassLoader());
        }

        public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> set) {
            getBindings.add(this.bus);
            getBindings.add(this.featureUtil);
        }

        public VoiceSearchHandler get() {
            return this.module.provideVoiceSearchHandler((Bus) this.bus.get(), (FeatureUtil) this.featureUtil.get());
        }
    }

    public ProdModule$$ModuleAdapter() {
        super(ProdModule.class, INJECTS, STATIC_INJECTIONS, false, INCLUDES, true, true);
    }

    public void getBindings(BindingsGroup bindings, ProdModule module) {
        bindings.contributeProvidesBinding("com.squareup.otto.Bus", new ProvideBusProvidesAdapter(module));
        bindings.contributeProvidesBinding("android.content.SharedPreferences", new ProvideSharedPreferencesProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.service.ConnectionServiceProxy", new ProvideConnectionServiceProxyProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.common.TimeHelper", new ProvideTimeHelperProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.device.PowerManager", new ProvidePowerManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.config.SettingsManager", new ProvideSettingsManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.ancs.AncsServiceConnector", new ProvideAncsServiceConnectorProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.manager.InputManager", new ProvideInputManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler", new ProvideDialSimulatorMessagesHandlerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.framework.phonecall.CallManager", new ProvideCallManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.service.pandora.PandoraManager", new ProvidePandoraManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.manager.MusicManager", new ProvideMusicManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.service.ConnectionHandler", new ProvideConnectionHandlerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.gesture.GestureServiceConnector", new ProvideGestureServiceConnectorProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.ui.framework.UIStateManager", new ProvideUIStateManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.storage.PathManager", new ProvidePathManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.profile.DriverProfileManager", new ProvideDriverProfileManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.framework.trips.TripManager", new ProvideTripManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.manager.PairingManager", new ProvidePairingManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.service.library.network.http.IHttpManager", new ProvideHttpManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.debug.DriveRecorder", new ProvideObdDataReceorderProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.util.MusicArtworkCache", new ProvideArtworkCacheProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.framework.voice.VoiceSearchHandler", new ProvideVoiceSearchHandlerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.util.FeatureUtil", new ProvideFeatureUtilProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.analytics.TelemetryDataManager", new ProvideTelemetryDataManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.hud.app.storage.cache.MessageCache<com.navdy.service.library.events.audio.MusicCollectionResponse>", new ProvideMusicCollectionResponseMessageCacheProvidesAdapter(module));
    }
}
