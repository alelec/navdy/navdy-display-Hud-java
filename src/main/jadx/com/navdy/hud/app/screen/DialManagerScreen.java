package com.navdy.hud.app.screen;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.device.dial.DialConstants;
import com.navdy.hud.app.device.dial.DialManager;
import com.navdy.hud.app.device.light.HUDLightUtils;
import com.navdy.hud.app.device.light.LightManager;
import com.navdy.hud.app.event.ShowScreenWithArgs;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.gesture.GestureServiceConnector;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.ui.framework.BasePresenter;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.view.DialManagerView;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.events.ui.ShowScreen.Builder;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import flow.Flow.Direction;
import flow.Layout;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.inject.Inject;
import javax.inject.Singleton;

@Layout(R.layout.screen_dial_manager)
public class DialManagerScreen extends BaseScreen {
    private static Presenter presenter;
    private static final Logger sLogger = new Logger(DialManagerScreen.class);

    @dagger.Module(addsTo = com.navdy.hud.app.ui.activity.Main.Module.class, injects = {DialManagerView.class})
    public class Module {
    }

    @Singleton
    public static class Presenter extends BasePresenter<DialManagerView> {
        @Inject
        Bus bus;
        private DialManager dialManager;
        @Inject
        GestureServiceConnector gestureServiceConnector;
        private Handler handler = new Handler(Looper.getMainLooper());
        private AtomicBoolean isLedTurnedBlue = new AtomicBoolean(false);
        private boolean scanningMode;
        @Inject
        UIStateManager uiStateManager;

        public Bus getBus() {
            return this.bus;
        }

        public void onLoad(Bundle savedInstanceState) {
            DialManagerScreen.sLogger.v("onLoad");
            DialManagerScreen.presenter = this;
            this.dialManager = DialManager.getInstance();
            this.uiStateManager.enableSystemTray(false);
            this.uiStateManager.enableNotificationColor(false);
            DialManagerScreen.sLogger.v("systemtray:invisible");
            String dialName = null;
            if (savedInstanceState != null) {
                dialName = savedInstanceState.getString(DialConstants.OTA_DIAL_NAME_KEY, null);
            }
            updateView(dialName);
            super.onLoad(savedInstanceState);
        }

        protected void onUnload() {
            DialManagerScreen.sLogger.v("onUnload");
            DialManagerScreen.presenter = null;
            showPairingLed(false);
            this.uiStateManager.enableSystemTray(true);
            this.uiStateManager.enableNotificationColor(true);
            NotificationManager.getInstance().enableNotifications(true);
            DialManagerScreen.sLogger.v("systemtray:visible");
            super.onUnload();
        }

        public void updateView(String dialName) {
            DialManagerView view = (DialManagerView) getView();
            if (view == null) {
                return;
            }
            if (dialName != null) {
                DialManagerScreen.sLogger.v("show searching for dial view");
                this.scanningMode = true;
                NotificationManager.getInstance().enableNotifications(false);
                view.showDialSearching(dialName);
                showPairingLed(true);
            } else if (this.dialManager.isDialConnected()) {
                DialManagerScreen.sLogger.v("show dial connected view");
                view.showDialConnected();
            } else {
                DialManagerScreen.sLogger.v("show dial pairing view");
                this.scanningMode = true;
                NotificationManager.getInstance().enableNotifications(false);
                view.showDialPairing();
                showPairingLed(true);
            }
        }

        public void dismiss() {
            exitScreen();
        }

        public void showPairingLed(boolean show) {
            if (show) {
                if (this.isLedTurnedBlue.compareAndSet(false, true)) {
                    HUDLightUtils.showPairing(HudApplication.getAppContext(), LightManager.getInstance(), true);
                }
            } else if (this.isLedTurnedBlue.compareAndSet(true, false)) {
                HUDLightUtils.showPairing(HudApplication.getAppContext(), LightManager.getInstance(), false);
            }
        }

        public Handler getHandler() {
            return this.handler;
        }

        private void exitScreen() {
            if (RemoteDeviceManager.getInstance().isRemoteDeviceConnected()) {
                DialManagerScreen.sLogger.v("go to default screen");
                this.bus.post(new Builder().screen(RemoteDeviceManager.getInstance().getUiStateManager().getDefaultMainActiveScreen()).build());
                return;
            }
            DialManagerScreen.sLogger.v("go to welcome screen");
            Bundle args = new Bundle();
            args.putString(WelcomeScreen.ARG_ACTION, WelcomeScreen.ACTION_RECONNECT);
            this.bus.post(new ShowScreenWithArgs(Screen.SCREEN_WELCOME, args, false));
        }

        public boolean isScanningMode() {
            return this.scanningMode;
        }
    }

    public Screen getScreen() {
        return Screen.SCREEN_DIAL_PAIRING;
    }

    public String getMortarScopeName() {
        return getClass().getName();
    }

    public Object getDaggerModule() {
        return new Module();
    }

    public int getAnimationIn(Direction direction) {
        return -1;
    }

    public int getAnimationOut(Direction direction) {
        return -1;
    }

    public boolean isScanningMode() {
        if (presenter != null) {
            return presenter.isScanningMode();
        }
        return false;
    }
}
