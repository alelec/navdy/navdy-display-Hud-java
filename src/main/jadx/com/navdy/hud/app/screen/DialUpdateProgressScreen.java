package com.navdy.hud.app.screen;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.device.dial.DialConstants;
import com.navdy.hud.app.device.dial.DialFirmwareUpdater.Error;
import com.navdy.hud.app.device.dial.DialFirmwareUpdater.UpdateProgressListener;
import com.navdy.hud.app.device.dial.DialFirmwareUpdater.Versions;
import com.navdy.hud.app.device.dial.DialManager;
import com.navdy.hud.app.event.ShowScreenWithArgs;
import com.navdy.hud.app.event.Shutdown;
import com.navdy.hud.app.event.Shutdown.Reason;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.framework.toast.ToastManager.ToastParams;
import com.navdy.hud.app.framework.toast.ToastPresenter;
import com.navdy.hud.app.service.ShutdownMonitor;
import com.navdy.hud.app.ui.framework.BasePresenter;
import com.navdy.hud.app.view.DialUpdateProgressView;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.events.ui.ShowScreen.Builder;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import dagger.Provides;
import flow.Layout;
import javax.inject.Inject;
import javax.inject.Singleton;

@Layout(R.layout.screen_dial_update_progress)
public class DialUpdateProgressScreen extends BaseScreen {
    private static final String DIAL_UPDATE_ERROR_TOAST_ID = "dial-fw-update-err";
    public static final String EXTRA_PROGRESS_CAUSE = "PROGRESS_CAUSE";
    public static final int POST_OTA_PAIRING_DELAY = 6000;
    public static final int POST_OTA_SHUTDOWN_DELAY = 30000;
    public static final int SETTINGS_SCREEN = 1;
    public static final int SHUTDOWN_SCREEN = 2;
    private static Handler handler = new Handler(Looper.getMainLooper());
    private static final Logger sLogger = new Logger(DialUpdateProgressScreen.class);
    public static boolean updateStarted = false;

    @dagger.Module(addsTo = com.navdy.hud.app.ui.activity.Main.Module.class, injects = {DialUpdateProgressView.class})
    public class Module {
        @Provides
        DialUpdateProgressScreen provideScreen() {
            return DialUpdateProgressScreen.this;
        }
    }

    @Singleton
    public static class Presenter extends BasePresenter<DialUpdateProgressView> {
        private int cause;
        @Inject
        Bus mBus;
        @Inject
        SharedPreferences mPreferences;
        @Inject
        DialUpdateProgressScreen mScreen;

        public void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            if (this.mScreen != null) {
                Bundle args = this.mScreen.arguments;
                if (args != null) {
                    this.cause = args.getInt(DialUpdateProgressScreen.EXTRA_PROGRESS_CAUSE, 2);
                }
            }
        }

        public void finish(Error error) {
            DialUpdateProgressScreen.updateStarted = false;
            switch (this.cause) {
                case 2:
                    DialUpdateProgressScreen.handler.postDelayed(new Runnable() {
                        public void run() {
                            Presenter.this.mBus.post(new Shutdown(Reason.DIAL_OTA));
                        }
                    }, 30000);
                    break;
            }
            ShutdownMonitor.getInstance().disableScreenDim(false);
            if (error != Error.NONE) {
                ToastManager.getInstance().disableToasts(false);
                NotificationManager.getInstance().enableNotifications(true);
                this.mBus.post(new Builder().screen(Screen.SCREEN_BACK).build());
                cancelUpdate();
                showDialUpdateErrorToast(error);
                return;
            }
            DialUpdateProgressScreen.handler.postDelayed(new Runnable() {
                public void run() {
                    Bundle args = new Bundle();
                    args.putString(DialConstants.OTA_DIAL_NAME_KEY, DialManager.getInstance().getDialFirmwareUpdater().getDialName());
                    Presenter.this.mBus.post(new ShowScreenWithArgs(Screen.SCREEN_DIAL_PAIRING, args, false));
                }
            }, 6000);
        }

        public Versions getVersions() {
            return DialManager.getInstance().getDialFirmwareUpdater().getVersions();
        }

        public void startUpdate() {
            if (!DialUpdateProgressScreen.updateStarted) {
                DialUpdateProgressScreen.updateStarted = true;
                ShutdownMonitor.getInstance().disableScreenDim(true);
                DialManager.getInstance().getDialFirmwareUpdater().startUpdate(new UpdateProgressListener() {
                    public void onProgress(final int percentage) {
                        DialUpdateProgressScreen.handler.post(new Runnable() {
                            public void run() {
                                DialUpdateProgressView view = (DialUpdateProgressView) Presenter.this.getView();
                                if (view != null) {
                                    view.setProgress(percentage);
                                }
                            }
                        });
                    }

                    public void onFinished(final Error error, String detail) {
                        DialUpdateProgressScreen.sLogger.v("onFinished:" + error + " , " + detail);
                        DialUpdateProgressScreen.handler.post(new Runnable() {
                            public void run() {
                                Presenter.this.finish(error);
                            }
                        });
                    }
                });
            }
        }

        public void cancelUpdate() {
            DialManager.getInstance().getDialFirmwareUpdater().cancelUpdate();
        }

        private void showDialUpdateErrorToast(Error error) {
            Resources resources = HudApplication.getAppContext().getResources();
            Bundle bundle = new Bundle();
            bundle.putInt(ToastPresenter.EXTRA_TIMEOUT, 10000);
            bundle.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, R.drawable.icon_dial_update);
            bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE, resources.getString(R.string.dial_update_err));
            bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, error.name());
            bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, R.style.Glances_1);
            bundle.putBoolean(ToastPresenter.EXTRA_DEFAULT_CHOICE, true);
            ToastManager toastManager = ToastManager.getInstance();
            toastManager.dismissCurrentToast(DialUpdateProgressScreen.DIAL_UPDATE_ERROR_TOAST_ID);
            toastManager.addToast(new ToastParams(DialUpdateProgressScreen.DIAL_UPDATE_ERROR_TOAST_ID, bundle, null, true, false));
        }
    }

    public String getMortarScopeName() {
        return DialUpdateProgressScreen.class.getName();
    }

    public Object getDaggerModule() {
        return new Module();
    }

    public Screen getScreen() {
        return Screen.SCREEN_DIAL_UPDATE_PROGRESS;
    }
}
