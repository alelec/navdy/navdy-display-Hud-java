package com.navdy.hud.app.audio;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.SoundPool;
import android.media.SoundPool.Builder;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.service.library.log.Logger;
import java.util.HashMap;

public class SoundUtils {
    private static boolean initialized;
    private static final Logger sLogger = new Logger(SoundUtils.class);
    private static HashMap<Sound, Integer> soundIdMap = new HashMap();
    private static SoundPool soundPool;

    public enum Sound {
        MENU_MOVE,
        MENU_SELECT,
        STARTUP,
        SHUTDOWN,
        ALERT_POSITIVE,
        ALERT_NEGATIVE
    }

    public static void init() {
        if (!initialized) {
            GenericUtil.checkNotOnMainThread();
            initialized = true;
            Context context = HudApplication.getAppContext();
            soundPool = new Builder().setAudioAttributes(new AudioAttributes.Builder().setUsage(13).setContentType(4).build()).build();
            soundIdMap.put(Sound.MENU_MOVE, Integer.valueOf(soundPool.load(context, R.raw.sound_menu_move, 1)));
            soundIdMap.put(Sound.MENU_SELECT, Integer.valueOf(soundPool.load(context, R.raw.sound_menu_select, 1)));
            soundIdMap.put(Sound.STARTUP, Integer.valueOf(soundPool.load(context, R.raw.sound_startup, 1)));
            soundIdMap.put(Sound.SHUTDOWN, Integer.valueOf(soundPool.load(context, R.raw.sound_shutdown, 1)));
            soundIdMap.put(Sound.ALERT_POSITIVE, Integer.valueOf(soundPool.load(context, R.raw.sound_alert_positive, 1)));
            soundIdMap.put(Sound.ALERT_NEGATIVE, Integer.valueOf(soundPool.load(context, R.raw.sound_alert_negative, 1)));
            sLogger.v("sound pool created");
        }
    }

    public static void playSound(Sound sound) {
        try {
            if (initialized) {
                Integer soundId = (Integer) soundIdMap.get(sound);
                if (soundId == null) {
                    sLogger.v("soundId not found:" + sound);
                } else {
                    soundPool.play(soundId.intValue(), 1.0f, 1.0f, 0, 0, 1.0f);
                }
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }
}
