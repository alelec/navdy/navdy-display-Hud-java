package com.navdy.hud.app.util;

import android.content.Context;
import android.os.Looper;
import android.text.TextUtils;
import android.view.inputmethod.InputMethodManager;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.events.DeviceInfo.Platform;
import com.navdy.service.library.log.Logger;
import java.lang.reflect.Method;

public class GenericUtil {
    private static final Logger sLogger = new Logger(GenericUtil.class);

    public static String getErrorMessage(Throwable t) {
        if (t == null) {
            return HudApplication.getAppContext().getResources().getString(R.string.operation_failed);
        }
        String str = t.getMessage();
        return TextUtils.isEmpty(str) ? t.toString() : str;
    }

    public static int getDrawableResourceIdFromName(String idName) {
        Context context = HudApplication.getAppContext();
        return context.getResources().getIdentifier(idName, "drawable", context.getPackageName());
    }

    public static int getIdsResourceIdFromName(String idName) {
        Context context = HudApplication.getAppContext();
        return context.getResources().getIdentifier(idName, "id", context.getPackageName());
    }

    public static int getStringResourceIdFromName(String idName) {
        Context context = HudApplication.getAppContext();
        return context.getResources().getIdentifier(idName, "string", context.getPackageName());
    }

    public static void checkNotOnMainThread() {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            throw new RuntimeException("cannot call on mainthread");
        }
    }

    public static boolean isMainThread() {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            return true;
        }
        return false;
    }

    public static String normalizeToFilename(String str) {
        return TextUtils.isEmpty(str) ? str : str.replaceAll("[^a-zA-Z0-9\\._]+", "");
    }

    public static String removePunctuation(String str) {
        return TextUtils.isEmpty(str) ? str : str.replaceAll("\\p{Punct}+", " ");
    }

    public static void sleep(int millis) {
        try {
            Thread.sleep((long) millis);
        } catch (Throwable th) {
        }
    }

    public static int indexOf(byte[] srcBytes, byte[] searchBytes, int startIndex, int endIndex) {
        if (searchBytes.length == 0 || (endIndex - startIndex) + 1 < searchBytes.length) {
            return -1;
        }
        int loopEndIdx;
        int maxScanStartPosIdx = srcBytes.length - searchBytes.length;
        if (endIndex < maxScanStartPosIdx) {
            loopEndIdx = endIndex;
        } else {
            loopEndIdx = maxScanStartPosIdx;
        }
        for (int i = startIndex; i <= loopEndIdx; i++) {
            boolean found = true;
            for (int j = 0; j < searchBytes.length; j++) {
                if (srcBytes[i + j] != searchBytes[j]) {
                    found = false;
                    break;
                }
            }
            if (found) {
                return i;
            }
        }
        return -1;
    }

    public static void clearInputMethodManagerFocusLeak() {
        InputMethodManager inputMethodManager = (InputMethodManager) HudApplication.getAppContext().getSystemService("input_method");
        try {
            Method finishInputLockedMethod = InputMethodManager.class.getDeclaredMethod("finishInputLocked", new Class[0]);
            finishInputLockedMethod.setAccessible(true);
            sLogger.v("calling finishInputLocked");
            finishInputLockedMethod.invoke(inputMethodManager, new Object[0]);
            sLogger.v("called finishInputLocked");
        } catch (Throwable t) {
            sLogger.v("finishInputLocked", t);
        }
    }

    public static boolean isClientiOS() {
        DeviceInfo deviceInfo = RemoteDeviceManager.getInstance().getRemoteDeviceInfo();
        if (deviceInfo == null || deviceInfo.platform != Platform.PLATFORM_iOS) {
            return false;
        }
        return true;
    }
}
