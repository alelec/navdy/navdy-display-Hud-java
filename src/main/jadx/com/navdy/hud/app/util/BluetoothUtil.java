package com.navdy.hud.app.util;

import android.bluetooth.BluetoothDevice;
import com.navdy.service.library.log.Logger;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;

public class BluetoothUtil {
    public static final int PAIRING_CONSENT = 3;
    public static final int PAIRING_VARIANT_DISPLAY_PASSKEY = 4;
    public static final int PAIRING_VARIANT_DISPLAY_PIN = 5;
    public static final Logger sLogger = new Logger(BluetoothUtil.class);

    public static byte[] convertPinToBytes(String pin) {
        if (pin == null) {
            return null;
        }
        try {
            byte[] pinBytes = pin.getBytes("UTF-8");
            if (pinBytes.length <= 0 || pinBytes.length > 16) {
                return null;
            }
            return pinBytes;
        } catch (UnsupportedEncodingException e) {
            sLogger.e("UTF-8 not supported?!?");
            return null;
        }
    }

    public static void removeBond(BluetoothDevice device) {
        try {
            Method m = device.getClass().getMethod("removeBond", (Class[]) null);
            if (m != null) {
                sLogger.i("removingBond for " + device);
                m.invoke(device, (Object[]) null);
                sLogger.i("removedBond for " + device);
                return;
            }
            sLogger.e("cannot get to removeBond api");
        } catch (Throwable e) {
            sLogger.e("Failed to removeBond", e);
        }
    }

    public static void cancelBondProcess(BluetoothDevice device) {
        try {
            Method m = device.getClass().getMethod("cancelBondProcess", (Class[]) null);
            if (m != null) {
                sLogger.i("cancellingBond for " + device);
                m.invoke(device, (Object[]) null);
                sLogger.i("cancelledBond for " + device);
                return;
            }
            sLogger.e("cannot get to cancelBondProcess api");
        } catch (Throwable e) {
            sLogger.e("Failed to cancelBondProcess", e);
        }
    }

    public static void cancelUserConfirmation(BluetoothDevice device) {
        try {
            sLogger.e("cancelling cancelPairingUserInput");
            device.getClass().getMethod("cancelPairingUserInput", new Class[]{Boolean.TYPE}).invoke(device, new Object[0]);
            sLogger.e("cancelled cancelPairingUserInput");
        } catch (Throwable throwable) {
            sLogger.e(throwable);
        }
    }

    public static void confirmPairing(BluetoothDevice device, int variant, int pin) {
        sLogger.i("Confirming pairing for " + device + " variant" + variant + " pin:" + pin);
        if (variant == 4 || variant == 2) {
            device.setPairingConfirmation(true);
        } else if (variant == 5) {
            device.setPin(convertPinToBytes(String.format("%04d", new Object[]{Integer.valueOf(pin)})));
        } else if (variant == 3) {
            device.setPairingConfirmation(true);
        }
    }
}
