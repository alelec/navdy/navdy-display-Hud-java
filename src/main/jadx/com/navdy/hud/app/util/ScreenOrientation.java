package com.navdy.hud.app.util;

import android.app.Activity;
import android.util.DisplayMetrics;
import com.navdy.service.library.log.Logger;

public class ScreenOrientation {
    private static final Logger sLogger = new Logger(ScreenOrientation.class);

    public static boolean isPortrait(Activity activity) {
        return isPortrait(getCurrentOrientation(activity));
    }

    public static boolean isPortrait(int orientation) {
        return orientation == 1 || orientation == 9;
    }

    public static int getCurrentOrientation(Activity activity) {
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        if (((rotation == 0 || rotation == 2) && height > width) || ((rotation == 1 || rotation == 3) && width > height)) {
            switch (rotation) {
                case 0:
                    return 1;
                case 1:
                    return 0;
                case 2:
                    return 9;
                case 3:
                    return 8;
                default:
                    sLogger.e("Unknown screen orientation. Defaulting to portrait.");
                    return 1;
            }
        }
        switch (rotation) {
            case 0:
                return 0;
            case 1:
                return 1;
            case 2:
                return 8;
            case 3:
                return 9;
            default:
                sLogger.e("Unknown screen orientation. Defaulting to landscape.");
                return 0;
        }
    }
}
