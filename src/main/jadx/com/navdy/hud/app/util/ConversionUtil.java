package com.navdy.hud.app.util;

public class ConversionUtil {
    public static final int MAX_KMPL = 45;
    public static final int MAX_MPG = 100;

    public static double convertLpHundredKmToMPG(double value) {
        if (value > 0.0d) {
            return Math.min(235.214d / value, 100.0d);
        }
        return 0.0d;
    }

    public static double convertLpHundredKmToKMPL(double value) {
        if (value > 0.0d) {
            return Math.min(100.0d / value, 45.0d);
        }
        return 0.0d;
    }
}
