package com.navdy.obd;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class VoltageSettings implements Parcelable {
    public static final Creator<VoltageSettings> CREATOR = new Creator<VoltageSettings>() {
        public VoltageSettings createFromParcel(Parcel source) {
            return new VoltageSettings(source);
        }

        public VoltageSettings[] newArray(int size) {
            return new VoltageSettings[size];
        }
    };
    public static final float DEFAULT_CHARGING_VOLTAGE = 13.1f;
    public static final float DEFAULT_ENGINE_OFF_VOLTAGE = 12.9f;
    public static final float DEFAULT_LOW_BATTERY_VOLTAGE = 12.2f;
    public final float chargingVoltage;
    public final float engineOffVoltage;
    public final float lowBatteryVoltage;

    public VoltageSettings() {
        this.lowBatteryVoltage = 12.2f;
        this.engineOffVoltage = 12.9f;
        this.chargingVoltage = 13.1f;
    }

    public VoltageSettings(float lowerBatteryVoltage, float engineOffVoltage, float chargingVoltage) {
        this.lowBatteryVoltage = lowerBatteryVoltage;
        this.engineOffVoltage = engineOffVoltage;
        this.chargingVoltage = chargingVoltage;
    }

    public VoltageSettings(Parcel in) {
        this.lowBatteryVoltage = in.readFloat();
        this.engineOffVoltage = in.readFloat();
        this.chargingVoltage = in.readFloat();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeFloat(this.lowBatteryVoltage);
        dest.writeFloat(this.engineOffVoltage);
        dest.writeFloat(this.chargingVoltage);
    }

    public String toString() {
        StringBuffer sb = new StringBuffer("VoltageSettings{");
        sb.append("lowBatteryVoltage=").append(this.lowBatteryVoltage);
        sb.append(", engineOffVoltage=").append(this.engineOffVoltage);
        sb.append(", chargingVoltage=").append(this.chargingVoltage);
        sb.append('}');
        return sb.toString();
    }
}
