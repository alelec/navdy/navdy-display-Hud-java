package com.navdy.service.library.network;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import com.navdy.service.library.device.connection.ServiceAddress;
import java.io.IOException;
import java.util.UUID;

public class BTSocketFactory implements SocketFactory {
    private final String address;
    private final boolean secure;
    private final UUID serviceUUID;

    public BTSocketFactory(String address, UUID serviceUUID) {
        this(address, serviceUUID, true);
    }

    public BTSocketFactory(String address, UUID serviceUUID, boolean secure) {
        this.address = address;
        this.serviceUUID = serviceUUID;
        this.secure = secure;
    }

    public BTSocketFactory(ServiceAddress address) {
        this(address.getAddress(), UUID.fromString(address.getService()));
    }

    public SocketAdapter build() throws IOException {
        BluetoothDevice device = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(this.address);
        if (this.secure) {
            return new BTSocketAdapter(device.createRfcommSocketToServiceRecord(this.serviceUUID));
        }
        return new BTSocketAdapter(device.createInsecureRfcommSocketToServiceRecord(this.serviceUUID));
    }
}
