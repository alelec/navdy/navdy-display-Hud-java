package com.navdy.service.library.device;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.SharedPreferences;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import com.navdy.service.library.device.connection.BTConnectionInfo;
import com.navdy.service.library.device.connection.ConnectionInfo;
import com.navdy.service.library.device.connection.ConnectionService;
import com.navdy.service.library.device.connection.ConnectionType;
import com.navdy.service.library.device.connection.ServiceAddress;
import com.navdy.service.library.device.discovery.RemoteDeviceScanner;
import com.navdy.service.library.device.discovery.RemoteDeviceScanner.Listener;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.Listenable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class RemoteDeviceRegistry extends Listenable<DeviceListUpdatedListener> implements Listener {
    public static final int MAX_PAIRED_DEVICES = 5;
    public static final String PREFS_FILE_DEVICE_REGISTRY = "DeviceRegistry";
    public static final String PREFS_KEY_DEFAULT_CONNECTION_INFO = "DefaultConnectionInfo";
    public static final String PREFS_KEY_PAIRED_CONNECTION_INFOS = "PairedConnectionInfos";
    public static final Logger sLogger = new Logger(RemoteDeviceRegistry.class);
    private static RemoteDeviceRegistry sRemoteDeviceRegistry;
    protected Context mContext;
    protected Gson mGson = new GsonBuilder().registerTypeAdapterFactory(ConnectionInfo.connectionInfoAdapter).create();
    protected Set<ConnectionInfo> mKnownConnectionInfo = new HashSet();
    protected HashSet<RemoteDeviceScanner> mRemoteDeviceScanners = new HashSet();
    protected SharedPreferences mSharedPrefs;
    private ArrayList<ConnectionInfo> pairedConnections;

    protected interface DeviceListEventDispatcher extends EventDispatcher<RemoteDeviceRegistry, DeviceListUpdatedListener> {
    }

    public interface DeviceListUpdatedListener extends Listenable.Listener {
        void onDeviceListChanged(Set<ConnectionInfo> set);
    }

    public static RemoteDeviceRegistry getInstance(Context c) {
        if (sRemoteDeviceRegistry == null) {
            sRemoteDeviceRegistry = new RemoteDeviceRegistry(c.getApplicationContext());
        }
        return sRemoteDeviceRegistry;
    }

    private RemoteDeviceRegistry(Context context) {
        this.mContext = context;
        loadPairedConnections();
    }

    public Set<ConnectionInfo> getKnownConnectionInfo() {
        return this.mKnownConnectionInfo;
    }

    public void addRemoteDeviceScanner(RemoteDeviceScanner scanner) {
        scanner.addListener(this);
        this.mRemoteDeviceScanners.add(scanner);
    }

    public void removeRemoteDeviceScanner(RemoteDeviceScanner scanner) {
        scanner.removeListener(this);
        this.mRemoteDeviceScanners.remove(scanner);
    }

    public void startScanning() {
        this.mKnownConnectionInfo.clear();
        Iterator it = this.mRemoteDeviceScanners.iterator();
        while (it.hasNext()) {
            ((RemoteDeviceScanner) it.next()).startScan();
        }
    }

    public void stopScanning() {
        Iterator it = this.mRemoteDeviceScanners.iterator();
        while (it.hasNext()) {
            ((RemoteDeviceScanner) it.next()).stopScan();
        }
    }

    public void refresh() {
        sLogger.i("refreshing paired connections info");
        loadPairedConnections();
    }

    public void addDiscoveredConnectionInfo(ConnectionInfo connectionInfo) {
        if (this.mKnownConnectionInfo.contains(connectionInfo)) {
            sLogger.d("Already contains: " + connectionInfo);
            return;
        }
        this.mKnownConnectionInfo.add(connectionInfo);
        sendDeviceListUpdate();
    }

    protected void sendDeviceListUpdate() {
        dispatchToListeners(new DeviceListEventDispatcher() {
            public void dispatchEvent(RemoteDeviceRegistry source, DeviceListUpdatedListener listener) {
                listener.onDeviceListChanged(new HashSet(RemoteDeviceRegistry.this.mKnownConnectionInfo));
            }
        });
    }

    public void setDefaultConnectionInfo(ConnectionInfo connectionInfo) {
        try {
            this.mSharedPrefs.edit().putString(PREFS_KEY_DEFAULT_CONNECTION_INFO, this.mGson.toJson( connectionInfo, (Type) ConnectionInfo.class)).apply();
        } catch (Exception e) {
            sLogger.e("Exception " + e);
        }
    }

    public ConnectionInfo getDefaultConnectionInfo() {
        ConnectionInfo connectionInfo = null;
        try {
            String connectionInfoText = this.mSharedPrefs.getString(PREFS_KEY_DEFAULT_CONNECTION_INFO, null);
            if (connectionInfoText == null) {
                return null;
            }
            connectionInfo = (ConnectionInfo) this.mGson.fromJson(connectionInfoText, ConnectionInfo.class);
            return connectionInfo;
        } catch (JsonParseException e) {
            sLogger.e("Unable to read connection info: ", e);
        }
    }

    public void addPairedConnection(ConnectionInfo connectionInfo) {
        this.pairedConnections.remove(connectionInfo);
        if (this.pairedConnections.size() >= 5) {
            this.pairedConnections.remove(4);
        }
        this.pairedConnections.add(0, connectionInfo);
        savePairedConnections();
    }

    public void removePairedConnection(BluetoothDevice device) {
        Iterator<ConnectionInfo> listIterator = this.pairedConnections.listIterator();
        while (listIterator.hasNext()) {
            ConnectionInfo info = (ConnectionInfo) listIterator.next();
            if ((info instanceof BTConnectionInfo) && info.getAddress().getAddress().equals(device.getAddress())) {
                listIterator.remove();
            }
        }
        savePairedConnections();
    }

    public ConnectionInfo findDevice(NavdyDeviceId deviceId) {
        Iterator<ConnectionInfo> listIterator = this.pairedConnections.listIterator();
        while (listIterator.hasNext()) {
            ConnectionInfo info = (ConnectionInfo) listIterator.next();
            if (info.getDeviceId().equals(deviceId)) {
                return info;
            }
        }
        return null;
    }

    public void removePairedConnection(ConnectionInfo connectionInfo) {
        this.pairedConnections.remove(connectionInfo);
        savePairedConnections();
    }

    public List<ConnectionInfo> getPairedConnections() {
        return this.pairedConnections;
    }

    public boolean hasPaired() {
        return this.pairedConnections.size() > 0;
    }

    public void savePairedConnections() {
        try {
            this.mSharedPrefs.edit().putString(PREFS_KEY_PAIRED_CONNECTION_INFOS, this.mGson.toJson(this.pairedConnections, new TypeToken<List<ConnectionInfo>>() {
            }.getType())).commit();
        } catch (Exception e) {
            sLogger.e("Exception saving paired connections", e);
        }
    }

    private void loadPairedConnections() {
        this.mSharedPrefs = this.mContext.getSharedPreferences(PREFS_FILE_DEVICE_REGISTRY, 4);
        ArrayList<ConnectionInfo> list = new ArrayList();
        try {
            String connectionInfoText = this.mSharedPrefs.getString(PREFS_KEY_PAIRED_CONNECTION_INFOS, null);
            Type listType = new TypeToken<List<ConnectionInfo>>() {
            }.getType();
            if (connectionInfoText != null) {
                list = (ArrayList) this.mGson.fromJson(connectionInfoText, listType);
            }
        } catch (JsonParseException e) {
            sLogger.e("Unable to read connection infos: ", e);
        }
        sLogger.v("Read pairing list of:" + Arrays.toString(list.toArray()));
        this.pairedConnections = list;
    }

    public void onScanStarted(RemoteDeviceScanner scanner) {
        sLogger.e("Scan started: " + scanner.toString());
    }

    public void onScanStopped(RemoteDeviceScanner scanner) {
        sLogger.e("Scan stopped: " + scanner.toString());
    }

    public void onDiscovered(RemoteDeviceScanner scanner, List<ConnectionInfo> devices) {
        for (ConnectionInfo connectionInfo : devices) {
            if (connectionInfo == null) {
                sLogger.e("missing connection info.");
            } else {
                addDiscoveredConnectionInfo(connectionInfo);
                if (!((connectionInfo instanceof BTConnectionInfo) || connectionInfo.getDeviceId().getBluetoothAddress() == null)) {
                    addDiscoveredConnectionInfo(new BTConnectionInfo(connectionInfo.getDeviceId(), new ServiceAddress(connectionInfo.getDeviceId().getBluetoothAddress(), ConnectionService.NAVDY_PROTO_SERVICE_UUID.toString()), ConnectionType.BT_PROTOBUF));
                }
            }
        }
    }

    public void onLost(RemoteDeviceScanner scanner, List<ConnectionInfo> list) {
    }

    public ConnectionInfo getLastPairedDevice() {
        try {
            if (this.pairedConnections.size() > 0) {
                return (ConnectionInfo) this.pairedConnections.get(0);
            }
            return null;
        } catch (Throwable th) {
            return null;
        }
    }
}
