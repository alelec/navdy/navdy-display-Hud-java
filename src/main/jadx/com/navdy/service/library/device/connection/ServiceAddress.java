package com.navdy.service.library.device.connection;

import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;

public class ServiceAddress {
    private final String address;
    private final String protocol;
    private final String service;

    public ServiceAddress(String device, String service) {
        this.address = device;
        this.service = service;
        this.protocol = "NA";
    }

    public ServiceAddress(String device, String service, String protocol) {
        this.address = device;
        this.service = service;
        this.protocol = protocol;
    }

    public String getAddress() {
        return this.address;
    }

    public String getService() {
        return this.service;
    }

    public String getProtocol() {
        return this.protocol;
    }

    public String toString() {
        return this.address + HereManeuverDisplayBuilder.SLASH + this.service + HereManeuverDisplayBuilder.SLASH + this.protocol;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ServiceAddress that = (ServiceAddress) o;
        if (this.address.equals(that.address) && this.service.equals(that.service)) {
            return this.protocol.equals(that.protocol);
        }
        return false;
    }

    public int hashCode() {
        return (((this.address.hashCode() * 31) + this.service.hashCode()) * 31) + this.protocol.hashCode();
    }
}
