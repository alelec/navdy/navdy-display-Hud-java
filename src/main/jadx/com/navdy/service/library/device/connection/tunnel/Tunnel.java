package com.navdy.service.library.device.connection.tunnel;

import android.util.Log;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.service.library.device.connection.ProxyService;
import com.navdy.service.library.network.SocketAcceptor;
import com.navdy.service.library.network.SocketAdapter;
import com.navdy.service.library.network.SocketFactory;
import com.navdy.service.library.util.IOUtils;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;

public class Tunnel extends ProxyService {
    static final String TAG = Tunnel.class.getSimpleName();
    private final SocketAcceptor acceptor;
    private final SocketFactory connector;
    final ArrayList<TransferThread> transferThreads = new ArrayList();

    public Tunnel(SocketAcceptor acceptor, SocketFactory connector) {
        setName(TAG);
        this.acceptor = acceptor;
        this.connector = connector;
    }

    public void run() {
        Log.d(TAG, String.format("start: %s -> %s", new Object[]{this.acceptor, this.connector}));
        while (true) {
            try {
                SocketAdapter fromSock = this.acceptor.accept();
                Log.v(TAG, "accepted connection (" + fromSock + HereManeuverDisplayBuilder.CLOSE_BRACKET);
                SocketAdapter toSock = null;
                InputStream fromInput = null;
                InputStream toInput = null;
                OutputStream fromOutput = null;
                OutputStream toOutput = null;
                try {
                    toSock = this.connector.build();
                    toSock.connect();
                    Log.v(TAG, "connected (" + toSock + HereManeuverDisplayBuilder.CLOSE_BRACKET);
                    Log.d(TAG, String.format("starting transfer: %s -> %s", new Object[]{fromSock, toSock}));
                    fromInput = fromSock.getInputStream();
                    toOutput = toSock.getOutputStream();
                    TransferThread tt1 = new TransferThread(this, fromInput, toOutput, true);
                    toInput = toSock.getInputStream();
                    fromOutput = fromSock.getOutputStream();
                    TransferThread tt2 = new TransferThread(this, toInput, fromOutput, false);
                    tt1.start();
                    tt2.start();
                    tt1.join();
                    tt2.join();
                    fromSock.close();
                    toSock.close();
                } catch (InterruptedException e) {
                } catch (IOException e2) {
                    Log.e(TAG, "transfer failed:" + e2.getMessage());
                    IOUtils.closeStream(toInput);
                    IOUtils.closeStream(toOutput);
                    IOUtils.closeStream(toSock);
                    IOUtils.closeStream(fromInput);
                    IOUtils.closeStream(fromOutput);
                    IOUtils.closeStream(fromSock);
                }
            } catch (Exception e3) {
                Log.e(TAG, "accept failed:" + e3.getMessage());
            }
        }
        cancel();
    }

    public void cancel() {
        try {
            this.acceptor.close();
        } catch (IOException e) {
            Log.e(TAG, "close failed:" + e.getMessage());
        }
        Iterator it = ((ArrayList) this.transferThreads.clone()).iterator();
        while (it.hasNext()) {
            ((TransferThread) it.next()).cancel();
        }
    }
}
