package com.navdy.service.library.events.audio;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.ProtoField;

public final class MusicCollectionFilter extends Message {
    public static final String DEFAULT_FIELD = "";
    public static final String DEFAULT_VALUE = "";
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String field;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String value;

    public static final class Builder extends com.squareup.wire.Message.Builder<MusicCollectionFilter> {
        public String field;
        public String value;

        public Builder(MusicCollectionFilter message) {
            super(message);
            if (message != null) {
                this.field = message.field;
                this.value = message.value;
            }
        }

        public Builder field(String field) {
            this.field = field;
            return this;
        }

        public Builder value(String value) {
            this.value = value;
            return this;
        }

        public MusicCollectionFilter build() {
            return new MusicCollectionFilter();
        }
    }

    public MusicCollectionFilter(String field, String value) {
        this.field = field;
        this.value = value;
    }

    private MusicCollectionFilter(Builder builder) {
        this(builder.field, builder.value);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof MusicCollectionFilter)) {
            return false;
        }
        MusicCollectionFilter o = (MusicCollectionFilter) other;
        if (equals( this.field,  o.field) && equals( this.value,  o.value)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        if (this.field != null) {
            result = this.field.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 37;
        if (this.value != null) {
            i = this.value.hashCode();
        }
        result = i2 + i;
        this.hashCode = result;
        return result;
    }
}
