package com.navdy.service.library.events.preferences;

import com.squareup.wire.ProtoEnum;

public enum MiddleGauge implements ProtoEnum {
    SPEEDOMETER(1),
    TACHOMETER(2);
    
    private final int value;

    private MiddleGauge(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
