package com.navdy.service.library.events.debug;

import com.squareup.wire.Message;

public final class StopDrivePlaybackEvent extends Message {
    private static final long serialVersionUID = 0;

    public static final class Builder extends com.squareup.wire.Message.Builder<StopDrivePlaybackEvent> {
        public Builder(StopDrivePlaybackEvent message) {
            super(message);
        }

        public StopDrivePlaybackEvent build() {
            return new StopDrivePlaybackEvent();
        }
    }

    private StopDrivePlaybackEvent(Builder builder) {
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        return other instanceof StopDrivePlaybackEvent;
    }

    public int hashCode() {
        return 0;
    }
}
