package com.navdy.service.library.events.audio;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;

public final class SpeechRequest extends Message {
    public static final Category DEFAULT_CATEGORY = Category.SPEECH_TURN_BY_TURN;
    public static final String DEFAULT_ID = "";
    public static final String DEFAULT_LANGUAGE = "";
    public static final Boolean DEFAULT_SENDSTATUS = Boolean.valueOf(false);
    public static final String DEFAULT_WORDS = "";
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 2, type = Datatype.ENUM)
    public final Category category;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String id;
    @ProtoField(tag = 5, type = Datatype.STRING)
    public final String language;
    @ProtoField(tag = 4, type = Datatype.BOOL)
    public final Boolean sendStatus;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String words;

    public static final class Builder extends com.squareup.wire.Message.Builder<SpeechRequest> {
        public Category category;
        public String id;
        public String language;
        public Boolean sendStatus;
        public String words;

        public Builder(SpeechRequest message) {
            super(message);
            if (message != null) {
                this.words = message.words;
                this.category = message.category;
                this.id = message.id;
                this.sendStatus = message.sendStatus;
                this.language = message.language;
            }
        }

        public Builder words(String words) {
            this.words = words;
            return this;
        }

        public Builder category(Category category) {
            this.category = category;
            return this;
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder sendStatus(Boolean sendStatus) {
            this.sendStatus = sendStatus;
            return this;
        }

        public Builder language(String language) {
            this.language = language;
            return this;
        }

        public SpeechRequest build() {
            checkRequiredFields();
            return new SpeechRequest();
        }
    }

    public enum Category implements ProtoEnum {
        SPEECH_TURN_BY_TURN(1),
        SPEECH_REROUTE(2),
        SPEECH_SPEED_WARNING(3),
        SPEECH_GPS_CONNECTIVITY(4),
        SPEECH_MESSAGE_READ_OUT(5),
        SPEECH_WELCOME_MESSAGE(6),
        SPEECH_NOTIFICATION(7),
        SPEECH_CAMERA_WARNING(8);
        
        private final int value;

        private Category(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public SpeechRequest(String words, Category category, String id, Boolean sendStatus, String language) {
        this.words = words;
        this.category = category;
        this.id = id;
        this.sendStatus = sendStatus;
        this.language = language;
    }

    private SpeechRequest(Builder builder) {
        this(builder.words, builder.category, builder.id, builder.sendStatus, builder.language);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof SpeechRequest)) {
            return false;
        }
        SpeechRequest o = (SpeechRequest) other;
        if (equals( this.words,  o.words) && equals( this.category,  o.category) && equals( this.id,  o.id) && equals( this.sendStatus,  o.sendStatus) && equals( this.language,  o.language)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.words != null ? this.words.hashCode() : 0) * 37;
        if (this.category != null) {
            hashCode = this.category.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.id != null) {
            hashCode = this.id.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.sendStatus != null) {
            hashCode = this.sendStatus.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.language != null) {
            i = this.language.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
