package com.navdy.service.library.events.navigation;

import com.squareup.wire.ProtoEnum;

public enum NavigationSessionState implements ProtoEnum {
    NAV_SESSION_ENGINE_NOT_READY(1),
    NAV_SESSION_LOCATION_SERVICES_NOT_READY(2),
    NAV_SESSION_STOPPED(3),
    NAV_SESSION_STARTED(4),
    NAV_SESSION_PAUSED(5),
    NAV_SESSION_ERROR(6),
    NAV_SESSION_REROUTED(7),
    NAV_SESSION_ENGINE_READY(8),
    NAV_SESSION_ARRIVED(9);
    
    private final int value;

    private NavigationSessionState(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
