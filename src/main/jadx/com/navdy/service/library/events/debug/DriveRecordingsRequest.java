package com.navdy.service.library.events.debug;

import com.squareup.wire.Message;

public final class DriveRecordingsRequest extends Message {
    private static final long serialVersionUID = 0;

    public static final class Builder extends com.squareup.wire.Message.Builder<DriveRecordingsRequest> {
        public Builder(DriveRecordingsRequest message) {
            super(message);
        }

        public DriveRecordingsRequest build() {
            return new DriveRecordingsRequest();
        }
    }

    private DriveRecordingsRequest(Builder builder) {
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        return other instanceof DriveRecordingsRequest;
    }

    public int hashCode() {
        return 0;
    }
}
