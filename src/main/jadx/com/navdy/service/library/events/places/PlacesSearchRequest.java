package com.navdy.service.library.events.places;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class PlacesSearchRequest extends Message {
    public static final Integer DEFAULT_MAXRESULTS = Integer.valueOf(0);
    public static final String DEFAULT_REQUESTID = "";
    public static final Integer DEFAULT_SEARCHAREA = Integer.valueOf(0);
    public static final String DEFAULT_SEARCHQUERY = "";
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 3, type = Datatype.INT32)
    public final Integer maxResults;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String requestId;
    @ProtoField(tag = 2, type = Datatype.INT32)
    public final Integer searchArea;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String searchQuery;

    public static final class Builder extends com.squareup.wire.Message.Builder<PlacesSearchRequest> {
        public Integer maxResults;
        public String requestId;
        public Integer searchArea;
        public String searchQuery;

        public Builder(PlacesSearchRequest message) {
            super(message);
            if (message != null) {
                this.searchQuery = message.searchQuery;
                this.searchArea = message.searchArea;
                this.maxResults = message.maxResults;
                this.requestId = message.requestId;
            }
        }

        public Builder searchQuery(String searchQuery) {
            this.searchQuery = searchQuery;
            return this;
        }

        public Builder searchArea(Integer searchArea) {
            this.searchArea = searchArea;
            return this;
        }

        public Builder maxResults(Integer maxResults) {
            this.maxResults = maxResults;
            return this;
        }

        public Builder requestId(String requestId) {
            this.requestId = requestId;
            return this;
        }

        public PlacesSearchRequest build() {
            checkRequiredFields();
            return new PlacesSearchRequest();
        }
    }

    public PlacesSearchRequest(String searchQuery, Integer searchArea, Integer maxResults, String requestId) {
        this.searchQuery = searchQuery;
        this.searchArea = searchArea;
        this.maxResults = maxResults;
        this.requestId = requestId;
    }

    private PlacesSearchRequest(Builder builder) {
        this(builder.searchQuery, builder.searchArea, builder.maxResults, builder.requestId);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof PlacesSearchRequest)) {
            return false;
        }
        PlacesSearchRequest o = (PlacesSearchRequest) other;
        if (equals( this.searchQuery,  o.searchQuery) && equals( this.searchArea,  o.searchArea) && equals( this.maxResults,  o.maxResults) && equals( this.requestId,  o.requestId)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.searchQuery != null ? this.searchQuery.hashCode() : 0) * 37;
        if (this.searchArea != null) {
            hashCode = this.searchArea.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.maxResults != null) {
            hashCode = this.maxResults.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.requestId != null) {
            i = this.requestId.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
