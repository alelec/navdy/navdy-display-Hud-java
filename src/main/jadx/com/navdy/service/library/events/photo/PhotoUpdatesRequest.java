package com.navdy.service.library.events.photo;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class PhotoUpdatesRequest extends Message {
    public static final PhotoType DEFAULT_PHOTOTYPE = PhotoType.PHOTO_ALBUM_ART;
    public static final Boolean DEFAULT_START = Boolean.valueOf(false);
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 2, type = Datatype.ENUM)
    public final PhotoType photoType;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.BOOL)
    public final Boolean start;

    public static final class Builder extends com.squareup.wire.Message.Builder<PhotoUpdatesRequest> {
        public PhotoType photoType;
        public Boolean start;

        public Builder(PhotoUpdatesRequest message) {
            super(message);
            if (message != null) {
                this.start = message.start;
                this.photoType = message.photoType;
            }
        }

        public Builder start(Boolean start) {
            this.start = start;
            return this;
        }

        public Builder photoType(PhotoType photoType) {
            this.photoType = photoType;
            return this;
        }

        public PhotoUpdatesRequest build() {
            checkRequiredFields();
            return new PhotoUpdatesRequest();
        }
    }

    public PhotoUpdatesRequest(Boolean start, PhotoType photoType) {
        this.start = start;
        this.photoType = photoType;
    }

    private PhotoUpdatesRequest(Builder builder) {
        this(builder.start, builder.photoType);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof PhotoUpdatesRequest)) {
            return false;
        }
        PhotoUpdatesRequest o = (PhotoUpdatesRequest) other;
        if (equals( this.start,  o.start) && equals( this.photoType,  o.photoType)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        if (this.start != null) {
            result = this.start.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 37;
        if (this.photoType != null) {
            i = this.photoType.hashCode();
        }
        result = i2 + i;
        this.hashCode = result;
        return result;
    }
}
