package com.navdy.service.library.events.preferences;

import com.navdy.service.library.events.settings.DateTimeConfiguration.Clock;
import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.ProtoField;

public final class LocalPreferences extends Message {
    public static final Clock DEFAULT_CLOCKFORMAT = Clock.CLOCK_24_HOUR;
    public static final Boolean DEFAULT_MANUALZOOM = Boolean.valueOf(false);
    public static final Float DEFAULT_MANUALZOOMLEVEL = Float.valueOf(0.0f);
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 3, type = Datatype.ENUM)
    public final Clock clockFormat;
    @ProtoField(tag = 1, type = Datatype.BOOL)
    public final Boolean manualZoom;
    @ProtoField(tag = 2, type = Datatype.FLOAT)
    public final Float manualZoomLevel;

    public static final class Builder extends com.squareup.wire.Message.Builder<LocalPreferences> {
        public Clock clockFormat;
        public Boolean manualZoom;
        public Float manualZoomLevel;

        public Builder(LocalPreferences message) {
            super(message);
            if (message != null) {
                this.manualZoom = message.manualZoom;
                this.manualZoomLevel = message.manualZoomLevel;
                this.clockFormat = message.clockFormat;
            }
        }

        public Builder manualZoom(Boolean manualZoom) {
            this.manualZoom = manualZoom;
            return this;
        }

        public Builder manualZoomLevel(Float manualZoomLevel) {
            this.manualZoomLevel = manualZoomLevel;
            return this;
        }

        public Builder clockFormat(Clock clockFormat) {
            this.clockFormat = clockFormat;
            return this;
        }

        public LocalPreferences build() {
            return new LocalPreferences();
        }
    }

    public LocalPreferences(Boolean manualZoom, Float manualZoomLevel, Clock clockFormat) {
        this.manualZoom = manualZoom;
        this.manualZoomLevel = manualZoomLevel;
        this.clockFormat = clockFormat;
    }

    private LocalPreferences(Builder builder) {
        this(builder.manualZoom, builder.manualZoomLevel, builder.clockFormat);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof LocalPreferences)) {
            return false;
        }
        LocalPreferences o = (LocalPreferences) other;
        if (equals( this.manualZoom,  o.manualZoom) && equals( this.manualZoomLevel,  o.manualZoomLevel) && equals( this.clockFormat,  o.clockFormat)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.manualZoom != null ? this.manualZoom.hashCode() : 0) * 37;
        if (this.manualZoomLevel != null) {
            hashCode = this.manualZoomLevel.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.clockFormat != null) {
            i = this.clockFormat.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
