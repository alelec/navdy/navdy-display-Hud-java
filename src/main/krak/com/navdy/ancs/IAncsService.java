package com.navdy.ancs;

abstract public interface IAncsService extends android.os.IInterface {
    abstract public void addListener(com.navdy.ancs.IAncsServiceListener arg);
    
    
    abstract public void connect(String arg);
    
    
    abstract public void connectToDevice(android.os.ParcelUuid arg, String arg0);
    
    
    abstract public void connectToService(android.os.ParcelUuid arg);
    
    
    abstract public void disconnect();
    
    
    abstract public int getState();
    
    
    abstract public void performNotificationAction(int arg, int arg0);
    
    
    abstract public void removeListener(com.navdy.ancs.IAncsServiceListener arg);
    
    
    abstract public void setNotificationFilter(java.util.List arg, long arg0);
}
