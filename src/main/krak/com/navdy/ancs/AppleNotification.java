package com.navdy.ancs;

public class AppleNotification implements android.os.Parcelable {
    final public static int ACTION_ID_NEGATIVE = 1;
    final public static int ACTION_ID_POSITIVE = 0;
    final public static int ATTRIBUTE_APP_IDENTIFIER = 0;
    final public static int ATTRIBUTE_DATE = 5;
    final public static int ATTRIBUTE_MESSAGE = 3;
    final public static int ATTRIBUTE_MESSAGE_SIZE = 4;
    final public static int ATTRIBUTE_NEGATIVE_ACTION_LABEL = 7;
    final public static int ATTRIBUTE_POSITIVE_ACTION_LABEL = 6;
    final public static int ATTRIBUTE_SUBTITLE = 2;
    final public static int ATTRIBUTE_TITLE = 1;
    final public static int CATEGORY_BUSINESS_AND_FINANCE = 9;
    final public static int CATEGORY_EMAIL = 6;
    final public static int CATEGORY_ENTERTAINMENT = 11;
    final public static int CATEGORY_HEALTH_AND_FITNESS = 8;
    final public static int CATEGORY_INCOMING_CALL = 1;
    final public static int CATEGORY_LOCATION = 10;
    final public static int CATEGORY_MISSED_CALL = 2;
    final public static int CATEGORY_NEWS = 7;
    final public static int CATEGORY_OTHER = 0;
    final public static int CATEGORY_SCHEDULE = 5;
    final public static int CATEGORY_SOCIAL = 4;
    final public static int CATEGORY_VOICE_MAIL = 3;
    final public static android.os.Parcelable$Creator CREATOR;
    final public static int EVENT_ADDED = 0;
    final static int EVENT_FLAG_IMPORTANT = 2;
    final static int EVENT_FLAG_NEGATIVE_ACTION = 16;
    final static int EVENT_FLAG_POSITIVE_ACTION = 8;
    final static int EVENT_FLAG_PRE_EXISTING = 4;
    final static int EVENT_FLAG_SILENT = 1;
    final public static int EVENT_MODIFIED = 1;
    final public static int EVENT_REMOVED = 2;
    final private static String TAG;
    private String appId;
    private String appName;
    private int categoryCount;
    private int categoryId;
    private java.util.Date date;
    private int eventFlags;
    private int eventId;
    private String message;
    private String negativeActionLabel;
    private int notificationUid;
    private String positiveActionLabel;
    private String subTitle;
    private String title;
    
    static {
        TAG = com.navdy.ancs.AppleNotification.class.getSimpleName();
        CREATOR = (android.os.Parcelable$Creator)new com.navdy.ancs.AppleNotification$1();
    }
    
    public AppleNotification(int i, int i0, int i1, int i2, int i3) {
        this.eventId = i;
        this.categoryId = i1;
        this.categoryCount = i2;
        this.eventFlags = i0;
        this.notificationUid = i3;
    }
    
    public AppleNotification(android.os.Parcel a) {
        this(a.readInt(), a.readInt(), a.readInt(), a.readInt(), a.readInt());
        this.setTitle(a.readString());
        this.setSubTitle(a.readString());
        this.setMessage(a.readString());
        long j = a.readLong();
        if (j != 0L) {
            this.setDate(new java.util.Date(j));
        }
        this.setAppId(a.readString());
        this.setPositiveActionLabel(a.readString());
        this.setNegativeActionLabel(a.readString());
        this.setAppName(a.readString());
    }
    
    public static int byteToInt(byte a) {
        int i = a & 255;
        return i;
    }
    
    public static int bytesToInt(byte a, byte a0, byte a1, byte a2) {
        return com.navdy.ancs.AppleNotification.byteToInt(a) + (com.navdy.ancs.AppleNotification.byteToInt(a0) << 8) + (com.navdy.ancs.AppleNotification.byteToInt(a1) << 16) + (com.navdy.ancs.AppleNotification.byteToInt(a2) << 24);
    }
    
    public static int bytesToInt(byte[] a, int i) {
        int i0 = a[i];
        int i1 = a[i + 1];
        int i2 = a[i + 2];
        int i3 = a[i + 3];
        return com.navdy.ancs.AppleNotification.bytesToInt((byte)i0, (byte)i1, (byte)i2, (byte)i3);
    }
    
    public static int bytesToShort(byte a, byte a0) {
        return com.navdy.ancs.AppleNotification.byteToInt(a) + (com.navdy.ancs.AppleNotification.byteToInt(a0) << 8);
    }
    
    public static int bytesToShort(byte[] a, int i) {
        int i0 = a[i];
        int i1 = a[i + 1];
        return com.navdy.ancs.AppleNotification.bytesToShort((byte)i0, (byte)i1);
    }
    
    public static com.navdy.ancs.AppleNotification parse(byte[] a) {
        int i = a[0];
        int i0 = i & 255;
        int i1 = a[1];
        int i2 = i1 & 255;
        int i3 = a[2];
        int i4 = i3 & 255;
        int i5 = a[3];
        return new com.navdy.ancs.AppleNotification(i0, i2, i4, i5 & 255, com.navdy.ancs.AppleNotification.bytesToInt(a, 4));
    }
    
    public int describeContents() {
        return 0;
    }
    
    public String getAppId() {
        return this.appId;
    }
    
    public String getAppName() {
        return this.appName;
    }
    
    public int getCategoryCount() {
        return this.categoryCount;
    }
    
    public int getCategoryId() {
        return this.categoryId;
    }
    
    public java.util.Date getDate() {
        return this.date;
    }
    
    public int getEventFlags() {
        return this.eventFlags;
    }
    
    public int getEventId() {
        return this.eventId;
    }
    
    public String getMessage() {
        return this.message;
    }
    
    public String getNegativeActionLabel() {
        return this.negativeActionLabel;
    }
    
    public int getNotificationUid() {
        return this.notificationUid;
    }
    
    public String getPositiveActionLabel() {
        return this.positiveActionLabel;
    }
    
    public String getSubTitle() {
        return this.subTitle;
    }
    
    public String getTitle() {
        return this.title;
    }
    
    public boolean hasNegativeAction() {
        return (this.eventFlags & 16) != 0;
    }
    
    public boolean hasPositiveAction() {
        return (this.eventFlags & 8) != 0;
    }
    
    public boolean isImportant() {
        return (this.eventFlags & 2) != 0;
    }
    
    public boolean isPreExisting() {
        return (this.eventFlags & 4) != 0;
    }
    
    public boolean isSilent() {
        return (this.eventFlags & 1) != 0;
    }
    
    public void setAppId(String s) {
        this.appId = s;
    }
    
    public void setAppName(String s) {
        this.appName = s;
    }
    
    public void setAttribute(int i, String s) {
        label0: {
            switch(i) {
                case 7: {
                    this.setNegativeActionLabel(s);
                    break label0;
                }
                case 6: {
                    this.setPositiveActionLabel(s);
                    break label0;
                }
                case 5: {
                    java.text.SimpleDateFormat a = new java.text.SimpleDateFormat("yyyyMMdd'T'HHmmSS", java.util.Locale.ENGLISH);
                    try {
                        this.setDate(((java.text.DateFormat)a).parse(s));
                    } catch(java.text.ParseException ignoredException) {
                        break;
                    }
                    break label0;
                }
                case 3: {
                    this.setMessage(s);
                    break label0;
                }
                case 2: {
                    this.setSubTitle(s);
                    break label0;
                }
                case 1: {
                    this.setTitle(s);
                    break label0;
                }
                case 0: {
                    this.setAppId(s);
                    break label0;
                }
                default: {
                    android.util.Log.d(TAG, new StringBuilder().append("Unhandled attribute - id:").append(i).append(" val:").append(s).toString());
                    break label0;
                }
            }
            android.util.Log.e(TAG, new StringBuilder().append("Unable to parse date - ").append(s).toString());
        }
    }
    
    public void setDate(java.util.Date a) {
        this.date = a;
    }
    
    public void setMessage(String s) {
        this.message = s;
    }
    
    public void setNegativeActionLabel(String s) {
        this.negativeActionLabel = s;
    }
    
    public void setPositiveActionLabel(String s) {
        this.positiveActionLabel = s;
    }
    
    public void setSubTitle(String s) {
        this.subTitle = s;
    }
    
    public void setTitle(String s) {
        this.title = s;
    }
    
    public String toString() {
        StringBuilder a = new StringBuilder(new StringBuilder().append("Notification{eventId=").append(this.eventId).append(", categoryId=").append(this.categoryId).append(", categoryCount=").append(this.categoryCount).append(", eventFlags=").append(this.eventFlags).append(", notificationUid=").append(this.notificationUid).toString());
        if (this.appId != null) {
            a.append(", appId='");
            a.append(this.appId);
            a.append((char)39);
        }
        if (this.appName != null) {
            a.append(", appName='");
            a.append(this.appName);
            a.append((char)39);
        }
        if (this.title != null) {
            a.append(", title='");
            a.append(this.title);
            a.append((char)39);
        }
        if (this.subTitle != null) {
            a.append(", subTitle='");
            a.append(this.subTitle);
            a.append((char)39);
        }
        if (this.message != null) {
            a.append(", message='");
            a.append(this.message);
            a.append((char)39);
        }
        if (this.positiveActionLabel != null) {
            a.append(", positiveAction='");
            a.append(this.positiveActionLabel);
            a.append((char)39);
        }
        if (this.negativeActionLabel != null) {
            a.append(", negativeAction='");
            a.append(this.negativeActionLabel);
            a.append((char)39);
        }
        if (this.date != null) {
            a.append(", date='");
            a.append(this.date);
            a.append((char)39);
        }
        a.append("}");
        return a.toString();
    }
    
    public void writeToParcel(android.os.Parcel a, int i) {
        a.writeInt(this.eventId);
        a.writeInt(this.eventFlags);
        a.writeInt(this.categoryId);
        a.writeInt(this.categoryCount);
        a.writeInt(this.notificationUid);
        a.writeString(this.title);
        a.writeString(this.subTitle);
        a.writeString(this.message);
        a.writeLong((this.date == null) ? 0L : this.date.getTime());
        a.writeString(this.appId);
        a.writeString(this.positiveActionLabel);
        a.writeString(this.negativeActionLabel);
        a.writeString(this.appName);
    }
}
