package com.navdy.ancs;

abstract public interface IAncsServiceListener extends android.os.IInterface {
    abstract public void onConnectionStateChange(int arg);
    
    
    abstract public void onNotification(com.navdy.ancs.AppleNotification arg);
}
