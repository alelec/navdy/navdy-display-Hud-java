package com.navdy.hud.app.util.picasso;

public class PicassoUtil {
    final private static String DISK_CACHE_SCHEME = "diskcache://";
    final private static String DISK_CACHE_SCHEME_NAME = "diskcache";
    final private static int DISK_CACHE_SIZE = 10485760;
    final public static int IMAGE_MEMORY_CACHE = 8388608;
    private static com.navdy.hud.app.storage.cache.DiskLruCache diskLruCache;
    private static volatile boolean initialized;
    private static Object lockObj;
    private static com.navdy.hud.app.util.picasso.PicassoLruCache lruCache;
    private static com.squareup.picasso.Picasso picasso;
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.util.picasso.PicassoUtil.class);
        lockObj = new Object();
    }
    
    public PicassoUtil() {
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    static com.navdy.hud.app.storage.cache.DiskLruCache access$100() {
        return diskLruCache;
    }
    
    public static void clearCache() {
        if (lruCache != null) {
            lruCache.clear();
        }
    }
    
    public static android.graphics.Bitmap getBitmapfromCache(java.io.File a) {
        android.graphics.Bitmap a0 = (lruCache == null) ? null : lruCache.getBitmap(new StringBuilder().append("file://").append(a.getAbsolutePath()).toString());
        return a0;
    }
    
    public static android.graphics.Bitmap getBitmapfromCache(String s) {
        android.graphics.Bitmap a = (lruCache == null) ? null : lruCache.getBitmap(s);
        return a;
    }
    
    public static com.navdy.hud.app.storage.cache.DiskLruCache getDiskLruCache() {
        return diskLruCache;
    }
    
    public static android.net.Uri getDiskcacheUri(String s) {
        return android.net.Uri.parse(new StringBuilder().append("diskcache://").append(s).toString());
    }
    
    public static com.squareup.picasso.Picasso getInstance() {
        if (!initialized) {
            com.navdy.hud.app.util.picasso.PicassoUtil.initPicasso(com.navdy.hud.app.HudApplication.getAppContext());
        }
        return picasso;
    }
    
    public static void initPicasso(android.content.Context a) {
        Object a0 = null;
        Throwable a1 = null;
        boolean b = initialized;
        label0: {
            if (!b) {
                synchronized(lockObj) {
                    boolean b0 = initialized;
                    label3: {
                        Throwable a2 = null;
                        if (b0) {
                            break label3;
                        }
                        try {
                            lruCache = new com.navdy.hud.app.util.picasso.PicassoLruCache(8388608);
                            lruCache.init();
                            String s = com.navdy.hud.app.storage.PathManager.getInstance().getImageDiskCacheFolder();
                            label2: {
                                Throwable a3 = null;
                                label1: try {
                                    diskLruCache = new com.navdy.hud.app.storage.cache.DiskLruCache("imagecache", s, 10485760);
                                    break label2;
                                } catch(Throwable a4) {
                                    sLogger.e(new StringBuilder().append("Error creating disk cache: ").append(a4).toString());
                                    try {
                                        com.navdy.service.library.util.IOUtils.deleteDirectory(a, new java.io.File(s));
                                        diskLruCache = new com.navdy.hud.app.storage.cache.DiskLruCache("imagecache", s, 10485760);
                                    } catch(Throwable a5) {
                                        a3 = a5;
                                        break label1;
                                    }
                                    break label2;
                                }
                                sLogger.e(new StringBuilder().append("Error re-creating disk cache: ").append(a3).toString());
                            }
                            picasso = new com.squareup.picasso.Picasso$Builder(a).memoryCache((com.squareup.picasso.Cache)lruCache).addRequestHandler((com.squareup.picasso.RequestHandler)new com.navdy.hud.app.util.picasso.PicassoUtil$1()).build();
                            com.squareup.picasso.Picasso.setSingletonInstance(picasso);
                            initialized = true;
                            break label3;
                        } catch(Throwable a6) {
                            a2 = a6;
                        }
                        sLogger.e(a2);
                    }
                    /*monexit(a0)*/;
                }
            }
            return;
        }
        while(true) {
            try {
                /*monexit(a0)*/;
            } catch(IllegalMonitorStateException | NullPointerException a8) {
                Throwable a9 = a8;
                a1 = a9;
                continue;
            }
            throw a1;
        }
    }
    
    public static boolean isImageAvailableInCache(java.io.File a) {
        return com.navdy.hud.app.util.picasso.PicassoUtil.getBitmapfromCache(a) != null;
    }
    
    public static void setBitmapInCache(String s, android.graphics.Bitmap a) {
        if (lruCache != null) {
            lruCache.setBitmap(s, a);
        }
    }
}
