package com.navdy.hud.app.util;

class MusicArtworkCache$1 implements Runnable {
    final com.navdy.hud.app.util.MusicArtworkCache this$0;
    final com.navdy.hud.app.util.MusicArtworkCache$CacheEntryHelper val$cacheEntryHelper;
    final byte[] val$data;
    
    MusicArtworkCache$1(com.navdy.hud.app.util.MusicArtworkCache a, com.navdy.hud.app.util.MusicArtworkCache$CacheEntryHelper a0, byte[] a1) {
        super();
        this.this$0 = a;
        this.val$cacheEntryHelper = a0;
        this.val$data = a1;
    }
    
    public void run() {
        com.navdy.hud.app.storage.cache.DiskLruCache a = com.navdy.hud.app.util.picasso.PicassoUtil.getDiskLruCache();
        if (a != null) {
            a.put(com.navdy.hud.app.util.MusicArtworkCache$CacheEntryHelper.access$200(this.val$cacheEntryHelper), this.val$data);
            com.navdy.hud.app.util.MusicArtworkCache.access$300(this.this$0, this.val$cacheEntryHelper);
        } else {
            com.navdy.hud.app.util.MusicArtworkCache.access$100().e("No disk cache");
        }
    }
}
