package com.navdy.hud.app.util.os;


public enum CpuProfiler$Usage {
    HIGH(0),
    NORMAL(1);

    private int value;
    CpuProfiler$Usage(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class CpuProfiler$Usage extends Enum {
//    final private static com.navdy.hud.app.util.os.CpuProfiler$Usage[] $VALUES;
//    final public static com.navdy.hud.app.util.os.CpuProfiler$Usage HIGH;
//    final public static com.navdy.hud.app.util.os.CpuProfiler$Usage NORMAL;
//    
//    static {
//        HIGH = new com.navdy.hud.app.util.os.CpuProfiler$Usage("HIGH", 0);
//        NORMAL = new com.navdy.hud.app.util.os.CpuProfiler$Usage("NORMAL", 1);
//        com.navdy.hud.app.util.os.CpuProfiler$Usage[] a = new com.navdy.hud.app.util.os.CpuProfiler$Usage[2];
//        a[0] = HIGH;
//        a[1] = NORMAL;
//        $VALUES = a;
//    }
//    
//    private CpuProfiler$Usage(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.util.os.CpuProfiler$Usage valueOf(String s) {
//        return (com.navdy.hud.app.util.os.CpuProfiler$Usage)Enum.valueOf(com.navdy.hud.app.util.os.CpuProfiler$Usage.class, s);
//    }
//    
//    public static com.navdy.hud.app.util.os.CpuProfiler$Usage[] values() {
//        return $VALUES.clone();
//    }
//}
//