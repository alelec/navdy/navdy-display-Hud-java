package com.navdy.hud.app.util;

public class NotificationActionCache {
    private String handler;
    private int lastId;
    
    public NotificationActionCache(Class a) {
        this.lastId = 1;
        this.handler = a.getName();
    }
    
    public com.navdy.service.library.events.notification.NotificationAction buildAction(int i, int i0, int i1) {
        return this.buildAction(i, i0, i1, (String)null);
    }
    
    public com.navdy.service.library.events.notification.NotificationAction buildAction(int i, int i0, int i1, int i2) {
        String s = this.handler;
        int i3 = this.lastId;
        this.lastId = i3 + 1;
        return new com.navdy.service.library.events.notification.NotificationAction(s, Integer.valueOf(i3), Integer.valueOf(i), Integer.valueOf(i0), (Integer)null, (String)null, Integer.valueOf(i1), Integer.valueOf(i2));
    }
    
    public com.navdy.service.library.events.notification.NotificationAction buildAction(int i, int i0, int i1, String s) {
        String s0 = this.handler;
        int i2 = this.lastId;
        this.lastId = i2 + 1;
        return new com.navdy.service.library.events.notification.NotificationAction(s0, Integer.valueOf(i2), Integer.valueOf(i), Integer.valueOf(i0), Integer.valueOf(i1), s, (Integer)null, (Integer)null);
    }
    
    public void markComplete(com.navdy.service.library.events.notification.NotificationAction a) {
    }
    
    public boolean validAction(com.navdy.service.library.events.notification.NotificationAction a) {
        return a.handler.equals(this.handler);
    }
}
