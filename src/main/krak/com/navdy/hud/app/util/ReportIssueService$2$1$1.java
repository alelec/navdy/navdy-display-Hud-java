package com.navdy.hud.app.util;

class ReportIssueService$2$1$1 implements com.navdy.service.library.network.http.services.JiraClient$ResultCallback {
    final com.navdy.hud.app.util.ReportIssueService$2$1 this$2;
    
    ReportIssueService$2$1$1(com.navdy.hud.app.util.ReportIssueService$2$1 a) {
        super();
        this.this$2 = a;
    }
    
    public void onError(Throwable a) {
        com.navdy.hud.app.util.ReportIssueService.access$000().e("Error Assigning the ticket", a);
        com.navdy.hud.app.util.ReportIssueService.access$100(this.this$2.this$1.this$0, this.this$2.this$1.val$fileToSync);
    }
    
    public void onSuccess(Object a) {
        com.navdy.hud.app.util.ReportIssueService.access$000().d(new StringBuilder().append("Assigned the ticket successfully to ").append(a).toString());
        com.navdy.hud.app.util.ReportIssueService.access$100(this.this$2.this$1.this$0, this.this$2.this$1.val$fileToSync);
    }
}
