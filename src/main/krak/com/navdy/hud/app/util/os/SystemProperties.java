package com.navdy.hud.app.util.os;

public class SystemProperties {
    private static Class CLASS;
    final public static String TAG;
    
    static {
        TAG = com.navdy.hud.app.util.os.SystemProperties.class.getName();
        try {
            CLASS = Class.forName("android.os.SystemProperties");
        } catch(ClassNotFoundException ignoredException) {
        }
    }
    
    private SystemProperties() {
    }
    
    public static String get(String s) {
        String s0 = null;
        try {
            Class a = CLASS;
            Class[] a0 = new Class[1];
            a0[0] = String.class;
            java.lang.reflect.Method a1 = a.getMethod("get", a0);
            Object[] a2 = new Object[1];
            a2[0] = s;
            s0 = (String)a1.invoke(null, a2);
        } catch(Exception ignoredException) {
            s0 = null;
        }
        return s0;
    }
    
    public static String get(String s, String s0) {
        try {
            Class a = CLASS;
            Class[] a0 = new Class[2];
            a0[0] = String.class;
            a0[1] = String.class;
            java.lang.reflect.Method a1 = a.getMethod("get", a0);
            Object[] a2 = new Object[2];
            a2[0] = s;
            a2[1] = s0;
            s0 = (String)a1.invoke(null, a2);
        } catch(Exception ignoredException) {
        }
        return s0;
    }
    
    public static boolean getBoolean(String s, boolean b) {
        try {
            Class a = CLASS;
            Class[] a0 = new Class[2];
            a0[0] = String.class;
            a0[1] = Boolean.TYPE;
            java.lang.reflect.Method a1 = a.getMethod("getBoolean", a0);
            Object[] a2 = new Object[2];
            a2[0] = s;
            a2[1] = Boolean.valueOf(b);
            b = ((Boolean)a1.invoke(null, a2)).booleanValue();
        } catch(Exception ignoredException) {
        }
        return b;
    }
    
    public static float getFloat(String s, float f) {
        try {
            String s0 = com.navdy.hud.app.util.os.SystemProperties.get(s);
            if (s0 != null) {
                f = Float.parseFloat(s0);
            }
        } catch(Exception a) {
            android.util.Log.w(TAG, new StringBuilder().append("Failed to parse ").append(s).append(" as float - ").append(a.getMessage()).toString());
        }
        return f;
    }
    
    public static int getInt(String s, int i) {
        try {
            Class a = CLASS;
            Class[] a0 = new Class[2];
            a0[0] = String.class;
            a0[1] = Integer.TYPE;
            java.lang.reflect.Method a1 = a.getMethod("getInt", a0);
            Object[] a2 = new Object[2];
            a2[0] = s;
            a2[1] = Integer.valueOf(i);
            i = ((Integer)a1.invoke(null, a2)).intValue();
        } catch(Exception ignoredException) {
        }
        return i;
    }
    
    public static long getLong(String s, long j) {
        try {
            Class a = CLASS;
            Class[] a0 = new Class[2];
            a0[0] = String.class;
            a0[1] = Long.TYPE;
            java.lang.reflect.Method a1 = a.getMethod("getLong", a0);
            Object[] a2 = new Object[2];
            a2[0] = s;
            a2[1] = Long.valueOf(j);
            j = ((Long)a1.invoke(null, a2)).longValue();
        } catch(Exception ignoredException) {
        }
        return j;
    }
    
    public static void set(String s, String s0) {
        try {
            Class a = CLASS;
            Class[] a0 = new Class[2];
            a0[0] = String.class;
            a0[1] = String.class;
            java.lang.reflect.Method a1 = a.getMethod("set", a0);
            Object[] a2 = new Object[2];
            a2[0] = s;
            a2[1] = s0;
            a1.invoke(null, a2);
        } catch(Exception a3) {
            android.util.Log.e(TAG, new StringBuilder().append("Unable to set prop ").append(s).append(" to value:").append(s0).append(" because of:").append(a3.getMessage()).toString());
        }
    }
}
