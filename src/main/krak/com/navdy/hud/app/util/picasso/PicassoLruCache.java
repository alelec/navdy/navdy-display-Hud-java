package com.navdy.hud.app.util.picasso;

public class PicassoLruCache extends com.squareup.picasso.LruCache implements com.navdy.hud.app.util.picasso.PicassoItemCacheListener {
    private java.util.HashMap keyMap;
    
    public PicassoLruCache(int i) {
        super(i);
        this.keyMap = new java.util.HashMap();
    }
    
    private String getKey(String s) {
        if (s != null) {
            int i = s.indexOf("\n");
            if (i != -1) {
                s = s.substring(0, i);
            }
        } else {
            s = null;
        }
        return s;
    }
    
    public android.graphics.Bitmap getBitmap(String s) {
        android.graphics.Bitmap a = null;
        synchronized(this) {
            if (s != null) {
                try {
                    String s0 = (String)this.keyMap.get(s);
                    a = null;
                    if (s0 == null) {
                        break label0;
                    }
                    a = super.get(s0);
                    break label0;
                } catch(Throwable a1) {
                    a0 = a1;
                }
            } else {
                a = null;
                break label0;
            }
            /*monexit(this)*/;
            throw a0;
        }
        /*monexit(this)*/;
        return a;
    }
    
    public void init() {
        com.navdy.hud.app.util.picasso.PicassoCacheMap a = new com.navdy.hud.app.util.picasso.PicassoCacheMap(0, 0.75f);
        a.setListener((com.navdy.hud.app.util.picasso.PicassoItemCacheListener)this);
        java.lang.reflect.Field a0 = com.squareup.picasso.LruCache.class.getDeclaredField("map");
        a0.setAccessible(true);
        a0.set(this, a);
    }
    
    public void itemAdded(Object a) {
        this.itemAdded((String)a);
    }
    
    public void itemAdded(String s) {
        synchronized(this) {
            if (s == null) {
                break label1;
            }
            label0: {
                try {
                    this.keyMap.put(this.getKey(s), s);
                } catch(Throwable a0) {
                    a = a0;
                    break label0;
                }
                break label1;
            }
            /*monexit(this)*/;
            throw a;
        }
        /*monexit(this)*/;
    }
    
    public void itemRemoved(Object a) {
        this.itemRemoved((String)a);
    }
    
    public void itemRemoved(String s) {
        synchronized(this) {
            if (s == null) {
                break label1;
            }
            label0: {
                try {
                    this.keyMap.remove(this.getKey(s));
                } catch(Throwable a0) {
                    a = a0;
                    break label0;
                }
                break label1;
            }
            /*monexit(this)*/;
            throw a;
        }
        /*monexit(this)*/;
    }
    
    public void setBitmap(String s, android.graphics.Bitmap a) {
        synchronized(this) {
            if (s == null) {
                break label1;
            }
            if (a == null) {
                break label1;
            }
            label0: {
                try {
                    String s0 = this.getKey(s);
                    this.keyMap.put(s0, s);
                    super.set(s, a);
                } catch(Throwable a1) {
                    a0 = a1;
                    break label0;
                }
                break label1;
            }
            /*monexit(this)*/;
            throw a0;
        }
        /*monexit(this)*/;
    }
}
