package com.navdy.hud.app.util;

public class MusicArtworkCache {
    final private static String SEPARATOR = "_";
    private static java.util.Map collectionTypeMap;
    final private static com.navdy.service.library.log.Logger sLogger;
    final private Object dbLock;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.util.MusicArtworkCache.class);
        collectionTypeMap = (java.util.Map)new java.util.HashMap();
        collectionTypeMap.put(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PLAYLISTS, "playlist");
        collectionTypeMap.put(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ARTISTS, "music");
        collectionTypeMap.put(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ALBUMS, "music");
        collectionTypeMap.put(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PODCASTS, "podcast");
        collectionTypeMap.put(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_AUDIOBOOKS, "audiobook");
    }
    
    public MusicArtworkCache() {
        this.dbLock = new Object();
    }
    
    static java.util.Map access$000() {
        return collectionTypeMap;
    }
    
    static com.navdy.service.library.log.Logger access$100() {
        return sLogger;
    }
    
    static void access$300(com.navdy.hud.app.util.MusicArtworkCache a, com.navdy.hud.app.util.MusicArtworkCache$CacheEntryHelper a0) {
        a.createDbEntry(a0);
    }
    
    static String access$400(com.navdy.hud.app.util.MusicArtworkCache a, com.navdy.hud.app.util.MusicArtworkCache$CacheEntryHelper a0) {
        return a.getFileNameFromDb(a0);
    }
    
    private void createDbEntry(com.navdy.hud.app.util.MusicArtworkCache$CacheEntryHelper a) {
        sLogger.d(new StringBuilder().append("createDbEntry: ").append(a).toString());
        String s = a.author;
        label1: {
            label0: {
                if (s != null) {
                    break label0;
                }
                if (a.album != null) {
                    break label0;
                }
                if (a.name != null) {
                    break label0;
                }
                sLogger.w("Not cacheable");
                break label1;
            }
            android.content.ContentValues a0 = new android.content.ContentValues();
            a0.put("type", a.type);
            if (a.author != null) {
                a0.put("author", a.author);
            }
            if (a.album != null) {
                a0.put("album", a.album);
            }
            if (a.author != null) {
                a0.put("name", a.name);
            }
            a0.put("file_name", com.navdy.hud.app.util.MusicArtworkCache$CacheEntryHelper.access$200(a));
            synchronized(this.dbLock) {
                android.database.sqlite.SQLiteDatabase a2 = com.navdy.hud.app.storage.db.HudDatabase.getInstance().getWritableDatabase();
                if (a2 != null) {
                    long j = a2.insertWithOnConflict("music_artwork_cache", (String)null, a0, 4);
                    if (j == -1L) {
                        String[] a3 = new String[1];
                        a3[0] = String.valueOf(j);
                        a2.update("music_artwork_cache", a0, "rowid = ?", a3);
                    }
                    /*monexit(a1)*/;
                } else {
                    sLogger.e("Couldn't get db");
                    /*monexit(a1)*/;
                }
            }
        }
    }
    
    private void getArtworkInternal(com.navdy.hud.app.util.MusicArtworkCache$CacheEntryHelper a, com.navdy.hud.app.util.MusicArtworkCache$Callback a0) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.util.MusicArtworkCache$2(this, a, a0), 22);
    }
    
    private String getFileNameFromDb(com.navdy.hud.app.util.MusicArtworkCache$CacheEntryHelper a) {
        String s = null;
        sLogger.d(new StringBuilder().append("getFileNameFromDb: ").append(a).toString());
        label8: {
            android.database.Cursor a0 = null;
            label5: {
                label2: {
                    Object a1 = null;
                    label4: try {
                        a1 = null;
                        a1 = null;
                        java.util.ArrayList a2 = new java.util.ArrayList();
                        a1 = null;
                        a1 = null;
                        StringBuilder a3 = new StringBuilder("type = ?");
                        a1 = null;
                        a2.add(a.type);
                        a1 = null;
                        if (a.author != null) {
                            a1 = null;
                            a3.append(" AND author = ?");
                            a2.add(a.author);
                        }
                        a1 = null;
                        if (a.album != null) {
                            a1 = null;
                            a3.append(" AND album = ?");
                            a2.add(a.album);
                        }
                        a1 = null;
                        boolean b = android.text.TextUtils.equals((CharSequence)a.type, (CharSequence)"music");
                        label6: {
                            String s0 = null;
                            label7: {
                                if (!b) {
                                    break label7;
                                }
                                a1 = null;
                                if (a.name == null) {
                                    break label6;
                                }
                                a1 = null;
                                if (a.album != null) {
                                    break label6;
                                }
                            }
                            a1 = null;
                            a3.append(" AND name = ?");
                            if (a.name == null) {
                                s0 = "";
                            } else {
                                a1 = null;
                                s0 = a.name;
                            }
                            a1 = null;
                            a2.add(s0);
                        }
                        a1 = null;
                        synchronized(this.dbLock) {
                            a0 = null;
                            android.database.sqlite.SQLiteDatabase a5 = com.navdy.hud.app.storage.db.HudDatabase.getInstance().getWritableDatabase();
                            label3: {
                                if (a5 != null) {
                                    break label3;
                                }
                                com.navdy.service.library.log.Logger a6 = sLogger;
                                a0 = null;
                                a6.e("Couldn't get db");
                                /*monexit(a4)*/;
                                break label4;
                            }
                            a0 = null;
                            String s1 = a3.toString();
                            String[] a7 = (String[])a2.toArray((Object[])new String[a2.size()]);
                            a0 = null;
                            String[] a8 = new String[1];
                            a8[0] = "file_name";
                            a0 = a5.query("music_artwork_cache", a8, s1, a7, (String)null, (String)null, (String)null, "1");
                            label0: {
                                label1: {
                                    if (a0 == null) {
                                        break label1;
                                    }
                                    if (a0.moveToFirst()) {
                                        break label0;
                                    }
                                }
                                /*monexit(a4)*/;
                                break label2;
                            }
                            s = a0.getString(a0.getColumnIndex("file_name"));
                            /*monexit(a4)*/;
                            break label5;
                        }
                    } catch(Throwable a14) {
                        com.navdy.service.library.util.IOUtils.closeObject((java.io.Closeable)a1);
                        throw a14;
                    }
                    com.navdy.service.library.util.IOUtils.closeObject((java.io.Closeable)null);
                    s = null;
                    break label8;
                }
                com.navdy.service.library.util.IOUtils.closeObject((java.io.Closeable)a0);
                s = null;
                break label8;
            }
            com.navdy.service.library.util.IOUtils.closeObject((java.io.Closeable)a0);
        }
        return s;
    }
    
    private void putArtworkInternal(com.navdy.hud.app.util.MusicArtworkCache$CacheEntryHelper a, byte[] a0) {
        if (com.navdy.hud.app.util.MusicArtworkCache$CacheEntryHelper.access$200(a) != null) {
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.util.MusicArtworkCache$1(this, a, a0), 22);
        } else {
            sLogger.e(new StringBuilder().append("Couldn't generate a filename for: ").append(a).toString());
        }
    }
    
    public void getArtwork(com.navdy.service.library.events.audio.MusicCollectionInfo a, com.navdy.hud.app.util.MusicArtworkCache$Callback a0) {
        this.getArtworkInternal(new com.navdy.hud.app.util.MusicArtworkCache$CacheEntryHelper(this, a), a0);
    }
    
    public void getArtwork(String s, String s0, String s1, com.navdy.hud.app.util.MusicArtworkCache$Callback a) {
        this.getArtworkInternal(new com.navdy.hud.app.util.MusicArtworkCache$CacheEntryHelper(this, s, s0, s1), a);
    }
    
    public void putArtwork(com.navdy.service.library.events.audio.MusicCollectionInfo a, byte[] a0) {
        this.putArtworkInternal(new com.navdy.hud.app.util.MusicArtworkCache$CacheEntryHelper(this, a), a0);
    }
    
    public void putArtwork(String s, String s0, String s1, byte[] a) {
        this.putArtworkInternal(new com.navdy.hud.app.util.MusicArtworkCache$CacheEntryHelper(this, s, s0, s1), a);
    }
}
