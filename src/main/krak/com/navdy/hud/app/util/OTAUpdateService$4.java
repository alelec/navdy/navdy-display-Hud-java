package com.navdy.hud.app.util;

class OTAUpdateService$4 implements Runnable {
    final com.navdy.hud.app.util.OTAUpdateService this$0;
    
    OTAUpdateService$4(com.navdy.hud.app.util.OTAUpdateService a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        int i = 0;
        try {
            i = Integer.parseInt(android.os.Build.VERSION.INCREMENTAL);
        } catch(NumberFormatException ignoredException) {
            com.navdy.hud.app.util.OTAUpdateService.access$000().e(new StringBuilder().append("Cannot parse the incremental version of the build :").append(android.os.Build.VERSION.INCREMENTAL).toString());
            i = -1;
        }
        if (i == -1) {
            String s = this.this$0.sharedPreferences.getString("last_update_file_received", (String)null);
            if (!android.text.TextUtils.isEmpty((CharSequence)s)) {
                i = com.navdy.hud.app.util.OTAUpdateService.access$700(s);
            }
        }
        if (i > 0) {
            java.io.File a = new java.io.File(com.navdy.hud.app.storage.PathManager.getInstance().getDirectoryForFileType(com.navdy.service.library.events.file.FileType.FILE_TYPE_OTA));
            if (a.exists()) {
                java.io.File[] a0 = a.listFiles();
                if (a0 != null) {
                    int i0 = a0.length;
                    int i1 = 0;
                    while(i1 < i0) {
                        java.io.File a1 = a0[i1];
                        if (a1.isFile() && com.navdy.hud.app.util.OTAUpdateService.isOtaFile(a1.getName()) && com.navdy.hud.app.util.OTAUpdateService.access$700(a1.getName()) <= i) {
                            com.navdy.hud.app.util.OTAUpdateService.access$000().e(new StringBuilder().append("Clearing the old update file :").append(a1.getName()).toString());
                            com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.HudApplication.getAppContext(), a1.getAbsolutePath());
                        }
                        i1 = i1 + 1;
                    }
                }
            }
        }
    }
}
