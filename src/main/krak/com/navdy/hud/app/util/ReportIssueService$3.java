package com.navdy.hud.app.util;

class ReportIssueService$3 implements com.navdy.service.library.network.http.services.JiraClient$ResultCallback {
    final com.navdy.hud.app.util.ReportIssueService this$0;
    final java.io.File val$fileToSync;
    
    ReportIssueService$3(com.navdy.hud.app.util.ReportIssueService a, java.io.File a0) {
        super();
        this.this$0 = a;
        this.val$fileToSync = a0;
    }
    
    public void onError(Throwable a) {
        com.navdy.hud.app.util.ReportIssueService.access$000().e(new StringBuilder().append("Error during sync ").append(a).toString());
        com.navdy.hud.app.util.ReportIssueService.access$200(this.this$0);
    }
    
    public void onSuccess(Object a) {
        if (a instanceof String) {
            com.navdy.hud.app.util.ReportIssueService.access$000().d(new StringBuilder().append("Issue reported ").append(a).toString());
        }
        com.navdy.hud.app.util.ReportIssueService.access$000().d("Sync succeeded");
        com.navdy.hud.app.util.ReportIssueService.access$100(this.this$0, this.val$fileToSync);
    }
}
