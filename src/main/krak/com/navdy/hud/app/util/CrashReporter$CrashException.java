package com.navdy.hud.app.util;

public class CrashReporter$CrashException extends Exception {
    public CrashReporter$CrashException(String s) {
        super(s);
    }
    
    String extractCrashLocation(String s) {
        String s0 = null;
        java.util.regex.Pattern a = this.getLocationPattern();
        if (a == null) {
            s0 = "";
        } else if (s == null) {
            s0 = "";
        } else {
            java.util.regex.Matcher a0 = a.matcher((CharSequence)s);
            if (a0.find()) {
                if (a0.groupCount() < 1) {
                    s0 = a0.group();
                } else {
                    int i = 1;
                    while(true) {
                        if (i > a0.groupCount()) {
                            s0 = "";
                            break;
                        } else {
                            if (a0.group(i) == null) {
                                i = i + 1;
                                continue;
                            }
                            s0 = a0.group(i);
                            break;
                        }
                    }
                }
            } else {
                s0 = "";
            }
        }
        return s0;
    }
    
    public String filter(String s) {
        return s;
    }
    
    public String getCrashLocation() {
        return this.extractCrashLocation(this.getLocalizedMessage());
    }
    
    public java.util.regex.Pattern getLocationPattern() {
        return null;
    }
    
    public String toString() {
        String s = (this).getClass().getSimpleName();
        String s0 = this.getLocalizedMessage();
        String s1 = (s0 == null) ? "" : this.filter(s0);
        return new StringBuilder().append(s).append(": ").append(this.getCrashLocation()).append(s1).toString();
    }
}
