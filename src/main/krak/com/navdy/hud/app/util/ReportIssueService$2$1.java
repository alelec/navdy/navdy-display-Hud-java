package com.navdy.hud.app.util;

class ReportIssueService$2$1 implements com.navdy.service.library.network.http.services.JiraClient$ResultCallback {
    final com.navdy.hud.app.util.ReportIssueService$2 this$1;
    
    ReportIssueService$2$1(com.navdy.hud.app.util.ReportIssueService$2 a) {
        super();
        this.this$1 = a;
    }
    
    public void onError(Throwable a) {
        com.navdy.hud.app.util.ReportIssueService.access$200(this.this$1.this$0);
    }
    
    public void onSuccess(Object a) {
        com.navdy.hud.app.util.ReportIssueService.access$000().d(new StringBuilder().append("Files successfully attached, Assigning the ticket ot ").append(this.this$1.val$assigneeName).append(", Email : ").append(this.this$1.val$assigneeEmail).toString());
        this.this$1.this$0.mJiraClient.assignTicketForName((String)a, this.this$1.val$assigneeName, this.this$1.val$assigneeEmail, (com.navdy.service.library.network.http.services.JiraClient$ResultCallback)new com.navdy.hud.app.util.ReportIssueService$2$1$1(this));
    }
}
