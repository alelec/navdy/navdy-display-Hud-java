package com.navdy.hud.app.util;

abstract public interface Pauseable {
    abstract public void onPause();
    
    
    abstract public void onResume();
}
