package com.navdy.hud.app.settings;

public class MainScreenSettings$ScreenConfiguration implements android.os.Parcelable {
    final public static android.os.Parcelable$Creator CREATOR;
    protected android.graphics.Rect margins;
    protected float scaleX;
    protected float scaleY;
    
    static {
        CREATOR = (android.os.Parcelable$Creator)new com.navdy.hud.app.settings.MainScreenSettings$ScreenConfiguration$1();
    }
    
    public MainScreenSettings$ScreenConfiguration() {
        this(new android.graphics.Rect(), 1f, 1f);
    }
    
    public MainScreenSettings$ScreenConfiguration(android.graphics.Rect a, float f, float f0) {
        this.margins = a;
        this.scaleX = f;
        this.scaleY = f0;
    }
    
    public MainScreenSettings$ScreenConfiguration(android.os.Parcel a) {
        this((android.graphics.Rect)a.readParcelable(com.navdy.hud.app.settings.MainScreenSettings$ScreenConfiguration.class.getClassLoader()), a.readFloat(), a.readFloat());
    }
    
    public int describeContents() {
        return 0;
    }
    
    public android.graphics.Rect getMargins() {
        return this.margins;
    }
    
    public float getScaleX() {
        return this.scaleX;
    }
    
    public float getScaleY() {
        return this.scaleY;
    }
    
    public void setMargins(android.graphics.Rect a) {
        this.margins = a;
    }
    
    public void setScaleX(float f) {
        this.scaleX = f;
    }
    
    public void setScaleY(float f) {
        this.scaleY = f;
    }
    
    public void writeToParcel(android.os.Parcel a, int i) {
        a.writeParcelable((android.os.Parcelable)this.margins, 0);
        a.writeFloat(this.scaleX);
        a.writeFloat(this.scaleY);
    }
}
