package com.navdy.hud.app.settings;

class BrightnessControl$1 implements Runnable {
    final com.navdy.hud.app.settings.BrightnessControl this$0;
    final int val$val;
    
    BrightnessControl$1(com.navdy.hud.app.settings.BrightnessControl a, int i) {
        super();
        this.this$0 = a;
        this.val$val = i;
    }
    
    public void run() {
        android.provider.Settings$System.putInt(com.navdy.hud.app.settings.BrightnessControl.access$000(this.this$0).getContentResolver(), "screen_brightness", this.val$val);
    }
}
