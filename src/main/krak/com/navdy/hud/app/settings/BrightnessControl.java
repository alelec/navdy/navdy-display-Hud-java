package com.navdy.hud.app.settings;

public class BrightnessControl implements android.content.SharedPreferences$OnSharedPreferenceChangeListener {
    final public static String AUTOBRIGHTNESSD_PROPERTY = "hw.navdy.autobrightnessd";
    final public static String AUTO_BRIGHTNESS_PROPERTY = "persist.sys.autobrightness";
    final public static String DEFAULT_AUTO_BRIGHTNESS = "false";
    final public static String DEFAULT_AUTO_BRIGHTNESS_ADJUSTMENT = "0";
    final public static String DEFAULT_BRIGHTNESS = "128";
    final public static String DEFAULT_LED_BRIGHTNESS = "255";
    final public static String DISABLED = "disabled";
    final public static String ENABLED = "enabled";
    final public static int MAX_BRIGHTNESS_ADJUSTMENT = 64;
    final public static int MIN_BRIGHTNESS_ADJUSTMENT = -64;
    final private static int MIN_INITIAL_BRIGHTNESS = 128;
    final public static String OFF = "off";
    final public static String ON = "on";
    final private static String TAG;
    private String autoBrightnessAdjustmentKey;
    private String autoBrightnessKey;
    private String brightnessKey;
    private com.squareup.otto.Bus bus;
    private android.content.Context context;
    private float lastBrightness;
    private String ledBrightnessKey;
    private android.os.Handler mainHandler;
    private android.content.SharedPreferences preferences;
    
    static {
        TAG = com.navdy.hud.app.settings.BrightnessControl.class.getName();
    }
    
    public BrightnessControl(android.content.Context a, com.squareup.otto.Bus a0, android.content.SharedPreferences a1, String s, String s0, String s1, String s2) {
        this.lastBrightness = 0.5f;
        this.context = a;
        this.bus = a0;
        a0.register(this);
        this.preferences = a1;
        this.mainHandler = new android.os.Handler(a.getMainLooper());
        a1.registerOnSharedPreferenceChangeListener((android.content.SharedPreferences$OnSharedPreferenceChangeListener)this);
        this.brightnessKey = s;
        this.autoBrightnessKey = s0;
        this.autoBrightnessAdjustmentKey = s1;
        this.ledBrightnessKey = s2;
        com.navdy.hud.app.util.os.SystemProperties.set("hw.navdy.autobrightnessd", "0");
        boolean b = this.getAutoBrightnessProperty();
        a1.edit().putString(s0, b ? "true" : "false").apply();
        if (!b) {
            if (this.getBrightnessPreference() < 128) {
                a1.edit().putString(s, String.valueOf(128)).apply();
            }
            this.onSharedPreferenceChanged(a1, this.brightnessKey);
        }
        this.onSharedPreferenceChanged(a1, s0);
        this.onSharedPreferenceChanged(a1, s2);
    }
    
    static android.content.Context access$000(com.navdy.hud.app.settings.BrightnessControl a) {
        return a.context;
    }
    
    static float access$100(com.navdy.hud.app.settings.BrightnessControl a) {
        return a.lastBrightness;
    }
    
    static void access$200(com.navdy.hud.app.settings.BrightnessControl a, float f) {
        a.updateWindowBrightness(f);
    }
    
    static android.os.Handler access$300(com.navdy.hud.app.settings.BrightnessControl a) {
        return a.mainHandler;
    }
    
    private int getAutoBrightnessAdjustmentPreference() {
        return Integer.parseInt(this.preferences.getString(this.autoBrightnessAdjustmentKey, "0"));
    }
    
    final private boolean getAutoBrightnessProperty() {
        boolean b = false;
        String s = com.navdy.hud.app.util.os.SystemProperties.get("persist.sys.autobrightness", "on");
        boolean b0 = s.equals("on");
        label2: {
            label0: {
                label1: {
                    if (b0) {
                        break label1;
                    }
                    if (!s.equals("enabled")) {
                        break label0;
                    }
                }
                b = true;
                break label2;
            }
            b = false;
        }
        return b;
    }
    
    private int getBrightnessPreference() {
        return Integer.parseInt(this.preferences.getString(this.brightnessKey, "128"));
    }
    
    private void setAutoBrightnessAdjustment(int i) {
        label0: {
            android.provider.Settings$SettingNotFoundException a = null;
            if (i < -64) {
                break label0;
            }
            if (i > 64) {
                break label0;
            }
            label1: {
                int i0 = 0;
                try {
                    i0 = android.provider.Settings$System.getInt(this.context.getContentResolver(), "screen_brightness_mode");
                } catch(android.provider.Settings$SettingNotFoundException a0) {
                    a = a0;
                    break label1;
                }
                if (i0 != 1) {
                    break label0;
                }
                com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.settings.BrightnessControl$3(this, i), 1);
                break label0;
            }
            android.util.Log.e(TAG, "", (Throwable)a);
        }
    }
    
    private void updateWindowBrightness(float f) {
        android.view.WindowManager$LayoutParams a = ((android.app.Activity)this.context).getWindow().getAttributes();
        a.screenBrightness = f;
        ((android.app.Activity)this.context).getWindow().setAttributes(a);
    }
    
    public String getValue() {
        return Integer.toString(android.provider.Settings$System.getInt(this.context.getContentResolver(), "screen_brightness", -1));
    }
    
    public void onSharedPreferenceChanged(android.content.SharedPreferences a, String s) {
        if (s.equals(this.brightnessKey)) {
            this.setBrightnessValue(this.getBrightnessPreference());
        } else if (s.equals(this.autoBrightnessAdjustmentKey)) {
            this.setAutoBrightnessAdjustment(this.getAutoBrightnessAdjustmentPreference());
        } else if (s.equals(this.autoBrightnessKey)) {
            this.toggleAutoBrightness(Boolean.valueOf(a.getString(s, "false")).booleanValue());
        } else if (s.equals(this.ledBrightnessKey)) {
            this.setLEDValue(a.getString(s, "255"));
        }
    }
    
    public void setBrightnessValue(int i) {
        label0: {
            android.provider.Settings$SettingNotFoundException a = null;
            if (i < 0) {
                break label0;
            }
            if (i > 255) {
                break label0;
            }
            this.lastBrightness = (float)i / 255f;
            label1: {
                int i0 = 0;
                try {
                    i0 = android.provider.Settings$System.getInt(this.context.getContentResolver(), "screen_brightness_mode");
                } catch(android.provider.Settings$SettingNotFoundException a0) {
                    a = a0;
                    break label1;
                }
                if (i0 == 1) {
                    break label0;
                }
                com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.settings.BrightnessControl$1(this, i), 1);
                this.updateWindowBrightness(this.lastBrightness);
                break label0;
            }
            android.util.Log.e(TAG, "", (Throwable)a);
        }
    }
    
    public void setLEDValue(String s) {
        int i = Integer.parseInt(s);
        if (i >= 0 && i <= 255) {
            Object[] a = new Object[1];
            a[0] = Float.valueOf((float)i / 255f);
            com.navdy.hud.app.util.os.SystemProperties.set("hw.navdy.led_max_brightness", String.format("%f", a));
        }
    }
    
    public void toggleAutoBrightness(boolean b) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.settings.BrightnessControl$2(this, b), 1);
    }
}
