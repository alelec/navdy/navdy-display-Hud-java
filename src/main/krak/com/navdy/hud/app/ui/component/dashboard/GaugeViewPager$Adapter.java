package com.navdy.hud.app.ui.component.dashboard;

abstract public interface GaugeViewPager$Adapter {
    abstract public int getCount();
    
    
    abstract public int getExcludedPosition();
    
    
    abstract public com.navdy.hud.app.view.DashboardWidgetPresenter getPresenter(int arg);
    
    
    abstract public android.view.View getView(int arg, android.view.View arg0, android.view.ViewGroup arg1, boolean arg2);
}
