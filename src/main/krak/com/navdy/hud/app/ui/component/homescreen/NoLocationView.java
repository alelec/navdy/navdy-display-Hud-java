package com.navdy.hud.app.ui.component.homescreen;

public class NoLocationView extends android.widget.RelativeLayout {
    private com.squareup.otto.Bus bus;
    private com.navdy.service.library.log.Logger logger;
    private android.animation.ObjectAnimator noLocationAnimator;
    @InjectView(R.id.noLocationImage)
    android.widget.ImageView noLocationImage;
    @InjectView(R.id.noLocationTextContainer)
    android.widget.LinearLayout noLocationTextContainer;
    
    public NoLocationView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public NoLocationView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public NoLocationView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
    }
    
    static android.animation.ObjectAnimator access$000(com.navdy.hud.app.ui.component.homescreen.NoLocationView a) {
        return a.noLocationAnimator;
    }
    
    public void getCustomAnimator(com.navdy.hud.app.view.MainView$CustomAnimationMode a, android.animation.AnimatorSet$Builder a0) {
        switch(com.navdy.hud.app.ui.component.homescreen.NoLocationView$2.$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode[a.ordinal()]) {
            case 2: {
                a0.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator((android.view.View)this.noLocationImage, (float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.noLocationImageShrinkLeftX));
                a0.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator((android.view.View)this.noLocationTextContainer, (float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.noLocationTextShrinkLeftX));
                break;
            }
            case 1: {
                a0.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator((android.view.View)this.noLocationImage, (float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.noLocationImageX));
                a0.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator((android.view.View)this.noLocationTextContainer, (float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.noLocationTextX));
                break;
            }
        }
    }
    
    public void hideLocationUI() {
        this.logger.v("hideLocationUI");
        this.setVisibility(8);
        if (this.noLocationAnimator != null) {
            this.logger.v("cancelled animation");
            this.noLocationAnimator.cancel();
            this.noLocationImage.setRotation(0.0f);
        }
    }
    
    public boolean isAcquiringLocation() {
        return this.getVisibility() == 0;
    }
    
    protected void onFinishInflate() {
        this.logger = com.navdy.hud.app.ui.component.homescreen.HomeScreenView.sLogger;
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View)this);
        this.bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
        this.bus.register(this);
    }
    
    public void setView(com.navdy.hud.app.view.MainView$CustomAnimationMode a) {
        switch(com.navdy.hud.app.ui.component.homescreen.NoLocationView$2.$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode[a.ordinal()]) {
            case 2: {
                this.noLocationImage.setX((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.noLocationImageShrinkLeftX);
                this.noLocationTextContainer.setX((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.noLocationTextShrinkLeftX);
                break;
            }
            case 1: {
                this.noLocationImage.setX((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.noLocationImageX);
                this.noLocationTextContainer.setX((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.noLocationTextX);
                break;
            }
        }
    }
    
    public void showLocationUI() {
        this.logger.v("showLocationUI");
        this.setVisibility(0);
        if (this.noLocationAnimator == null) {
            android.widget.ImageView a = this.noLocationImage;
            android.util.Property a0 = android.view.View.ROTATION;
            float[] a1 = new float[1];
            a1[0] = 360f;
            this.noLocationAnimator = android.animation.ObjectAnimator.ofFloat(a, a0, a1);
            this.noLocationAnimator.setDuration(500L);
            this.noLocationAnimator.setInterpolator((android.animation.TimeInterpolator)new android.view.animation.AccelerateDecelerateInterpolator());
            this.noLocationAnimator.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.ui.component.homescreen.NoLocationView$1(this));
        }
        this.noLocationAnimator.start();
    }
}
