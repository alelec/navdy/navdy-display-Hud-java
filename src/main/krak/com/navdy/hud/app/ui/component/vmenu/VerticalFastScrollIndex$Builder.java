package com.navdy.hud.app.ui.component.vmenu;

public class VerticalFastScrollIndex$Builder {
    private java.util.LinkedHashMap map;
    private int positionOffset;
    
    public VerticalFastScrollIndex$Builder() {
        this.map = new java.util.LinkedHashMap();
    }
    
    public com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex build() {
        int i = this.map.size();
        String[] a = new String[i];
        int[] a0 = new int[i];
        java.util.Iterator a1 = this.map.keySet().iterator();
        int i0 = 0;
        Object a2 = a1;
        while(((java.util.Iterator)a2).hasNext()) {
            String s = (String)((java.util.Iterator)a2).next();
            a[i0] = s;
            a0[i0] = ((Integer)this.map.get(s)).intValue();
            i0 = i0 + 1;
        }
        return new com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex(a, (java.util.Map)this.map, a0, this.positionOffset, (com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex$1)null);
    }
    
    public com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex$Builder positionOffset(int i) {
        this.positionOffset = i;
        return this;
    }
    
    public com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex$Builder setEntry(char a, int i) {
        this.map.put(String.valueOf(a), Integer.valueOf(i));
        return this;
    }
}
