package com.navdy.hud.app.ui.component.mainmenu;

class MusicMenu2$5 implements com.navdy.hud.app.util.MusicArtworkCache$Callback {
    final com.navdy.hud.app.ui.component.mainmenu.MusicMenu2 this$0;
    final com.navdy.service.library.events.audio.MusicCollectionInfo val$collectionInfo;
    final int val$pos;
    
    MusicMenu2$5(com.navdy.hud.app.ui.component.mainmenu.MusicMenu2 a, int i, com.navdy.service.library.events.audio.MusicCollectionInfo a0) {
        super();
        this.this$0 = a;
        this.val$pos = i;
        this.val$collectionInfo = a0;
    }
    
    public void onHit(byte[] a) {
        com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.access$800(this.this$0, a, Integer.valueOf(this.val$pos), com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.access$700(this.this$0, this.val$collectionInfo), false);
    }
    
    public void onMiss() {
        com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.access$400().i(new StringBuilder().append("Requesting artwork for collection: ").append(this.val$collectionInfo).toString());
        com.navdy.service.library.events.audio.MusicArtworkRequest a = new com.navdy.service.library.events.audio.MusicArtworkRequest$Builder().collectionSource(this.val$collectionInfo.collectionSource).collectionType(this.val$collectionInfo.collectionType).collectionId(this.val$collectionInfo.collectionId).size(Integer.valueOf(200)).build();
        com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.access$900(this.this$0, a, this.val$collectionInfo);
    }
}
