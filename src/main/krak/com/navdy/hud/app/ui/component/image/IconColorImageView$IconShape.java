package com.navdy.hud.app.ui.component.image;


public enum IconColorImageView$IconShape {
    CIRCLE(0),
    SQUARE(1);

    private int value;
    IconColorImageView$IconShape(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class IconColorImageView$IconShape extends Enum {
//    final private static com.navdy.hud.app.ui.component.image.IconColorImageView$IconShape[] $VALUES;
//    final public static com.navdy.hud.app.ui.component.image.IconColorImageView$IconShape CIRCLE;
//    final public static com.navdy.hud.app.ui.component.image.IconColorImageView$IconShape SQUARE;
//    
//    static {
//        CIRCLE = new com.navdy.hud.app.ui.component.image.IconColorImageView$IconShape("CIRCLE", 0);
//        SQUARE = new com.navdy.hud.app.ui.component.image.IconColorImageView$IconShape("SQUARE", 1);
//        com.navdy.hud.app.ui.component.image.IconColorImageView$IconShape[] a = new com.navdy.hud.app.ui.component.image.IconColorImageView$IconShape[2];
//        a[0] = CIRCLE;
//        a[1] = SQUARE;
//        $VALUES = a;
//    }
//    
//    private IconColorImageView$IconShape(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.ui.component.image.IconColorImageView$IconShape valueOf(String s) {
//        return (com.navdy.hud.app.ui.component.image.IconColorImageView$IconShape)Enum.valueOf(com.navdy.hud.app.ui.component.image.IconColorImageView$IconShape.class, s);
//    }
//    
//    public static com.navdy.hud.app.ui.component.image.IconColorImageView$IconShape[] values() {
//        return $VALUES.clone();
//    }
//}
//