package com.navdy.hud.app.ui.component.mainmenu;


public enum IMenu$MenuLevel {
    BACK_TO_PARENT(0),
    SUB_LEVEL(1),
    CLOSE(2),
    REFRESH_CURRENT(3),
    ROOT(4);

    private int value;
    IMenu$MenuLevel(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class IMenu$MenuLevel extends Enum {
//    final private static com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel[] $VALUES;
//    final public static com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel BACK_TO_PARENT;
//    final public static com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel CLOSE;
//    final public static com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel REFRESH_CURRENT;
//    final public static com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel ROOT;
//    final public static com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel SUB_LEVEL;
//    
//    static {
//        BACK_TO_PARENT = new com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel("BACK_TO_PARENT", 0);
//        SUB_LEVEL = new com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel("SUB_LEVEL", 1);
//        CLOSE = new com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel("CLOSE", 2);
//        REFRESH_CURRENT = new com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel("REFRESH_CURRENT", 3);
//        ROOT = new com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel("ROOT", 4);
//        com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel[] a = new com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel[5];
//        a[0] = BACK_TO_PARENT;
//        a[1] = SUB_LEVEL;
//        a[2] = CLOSE;
//        a[3] = REFRESH_CURRENT;
//        a[4] = ROOT;
//        $VALUES = a;
//    }
//    
//    private IMenu$MenuLevel(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel valueOf(String s) {
//        return (com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel)Enum.valueOf(com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.class, s);
//    }
//    
//    public static com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel[] values() {
//        return $VALUES.clone();
//    }
//}
//