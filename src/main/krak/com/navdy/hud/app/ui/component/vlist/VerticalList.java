package com.navdy.hud.app.ui.component.vlist;
import com.navdy.hud.app.R;

public class VerticalList {
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model BLANK_ITEM_BOTTOM;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model BLANK_ITEM_TOP;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize[] FONT_SIZES;
    final public static float ICON_BK_COLOR_SCALE_FACTOR = 0.83f;
    final public static float ICON_SCALE_FACTOR = 0.6f;
    final public static int ITEM_BACK_FADE_IN_DURATION = 100;
    final public static int ITEM_INIT_ANIMATION_DURATION = 100;
    final public static int ITEM_MOVE_ANIMATION_DURATION = 230;
    final public static int ITEM_SCROLL_ANIMATION = 150;
    final public static int ITEM_SELECT_ANIMATION_DURATION = 50;
    final public static int LOADING_ADAPTER_POS = 1;
    final private static int MAX_OFF_SCREEN_VIEWS = 5;
    final private static float MAX_SCROLL_BY = 2f;
    final private static float SCROLL_BY_INCREMENT = 0.5f;
    final private static int[] SINGLE_LINE_MAX_LINES;
    final private static float START_SCROLL_BY = 1f;
    final public static float TEXT_SCALE_FACTOR_16 = 1f;
    final public static float TEXT_SCALE_FACTOR_18 = 1f;
    final public static float TEXT_SCALE_FACTOR_22 = 0.81f;
    final public static float TEXT_SCALE_FACTOR_26 = 0.69f;
    final private static float[] TITLE_SIZES;
    final private static int[] TWO_LINE_MAX_LINES;
    final public static android.widget.TextView fontSizeTextView;
    private static android.os.Handler handler;
    final private static com.navdy.service.library.log.Logger sLogger;
    final private static java.util.HashMap tit_subt_map;
    final private static java.util.HashMap tit_subt_map_2_lines;
    final private static java.util.HashMap tit_subt_subt2_map;
    final public static int vlistTitleTextW;
    final public static float vlistTitle_16;
    final public static float vlistTitle_16_16_subtitle_top;
    final public static float vlistTitle_16_16_title_top;
    final public static float vlistTitle_16_top_m_2;
    final public static float vlistTitle_16_top_m_3;
    final public static float vlistTitle_18;
    final public static float vlistTitle_18_16_subtitle_top;
    final public static float vlistTitle_18_16_title_top;
    final public static float vlistTitle_18_top_m_2;
    final public static float vlistTitle_18_top_m_3;
    final public static float vlistTitle_22;
    final public static float vlistTitle_22_16_subtitle_top;
    final public static float vlistTitle_22_16_title_top;
    final public static float vlistTitle_22_18_subtitle_top;
    final public static float vlistTitle_22_18_title_top;
    final public static float vlistTitle_22_top_m_2;
    final public static float vlistTitle_22_top_m_3;
    final public static float vlistTitle_26;
    final public static float vlistTitle_26_16_subtitle_top;
    final public static float vlistTitle_26_16_title_top;
    final public static float vlistTitle_26_18_subtitle_top;
    final public static float vlistTitle_26_18_title_top;
    final public static float vlistTitle_26_top_m_2;
    final public static float vlistTitle_26_top_m_3;
    final public static float vlistsubTitle2_16_top_m_3;
    final public static float vlistsubTitle2_18_top_m_3;
    final public static float vlistsubTitle2_22_top_m_3;
    final public static float vlistsubTitle2_26_top_m_3;
    final public static float vlistsubTitle_16_top_m_2;
    final public static float vlistsubTitle_16_top_m_3;
    final public static float vlistsubTitle_18_top_m_2;
    final public static float vlistsubTitle_18_top_m_3;
    final public static float vlistsubTitle_22_top_m_2;
    final public static float vlistsubTitle_22_top_m_3;
    final public static float vlistsubTitle_26_top_m_2;
    final public static float vlistsubTitle_26_top_m_3;
    private int actualScrollY;
    public com.navdy.hud.app.ui.component.vlist.VerticalAdapter adapter;
    final int animationDuration;
    volatile boolean bindCallbacks;
    com.navdy.hud.app.ui.component.vlist.VerticalList$Callback callback;
    com.navdy.hud.app.ui.component.vlist.VerticalList$ContainerCallback containerCallback;
    private java.util.ArrayList copyList;
    private int currentMiddlePosition;
    private volatile float currentScrollBy;
    boolean firstEntryBlank;
    boolean hasScrollableElement;
    private com.navdy.hud.app.ui.component.carousel.CarouselIndicator indicator;
    private com.navdy.hud.app.ui.framework.DefaultAnimationListener indicatorAnimationListener;
    private android.animation.AnimatorSet indicatorAnimatorSet;
    private volatile boolean initialPosChanged;
    private Runnable initialPosCheckRunnable;
    private volatile boolean isScrollBy;
    private volatile boolean isScrolling;
    private volatile boolean isSelectedOperationPending;
    final private com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState itemSelectionState;
    final private com.navdy.hud.app.ui.component.vlist.VerticalList$KeyHandlerState keyHandlerState;
    private int lastScrollState;
    private com.navdy.hud.app.ui.component.vlist.VerticalLayoutManager layoutManager;
    private volatile boolean lockList;
    private java.util.List modelList;
    boolean reCalcScrollPos;
    private com.navdy.hud.app.ui.component.vlist.VerticalRecyclerView recyclerView;
    private volatile int scrollByUpReceived;
    int scrollItemEndY;
    int scrollItemHeight;
    int scrollItemIndex;
    int scrollItemStartY;
    private android.support.v7.widget.RecyclerView$OnScrollListener scrollListener;
    private volatile int scrollPendingPos;
    public java.util.HashSet selectedList;
    boolean sendScrollIdleEvent;
    private boolean switchingScrollBoundary;
    private boolean targetFound;
    public int targetPos;
    private boolean twoLineTitles;
    public boolean waitForTarget;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.vlist.VerticalList.class);
        handler = new android.os.Handler(android.os.Looper.getMainLooper());
        tit_subt_subt2_map = new java.util.HashMap();
        tit_subt_map = new java.util.HashMap();
        tit_subt_map_2_lines = new java.util.HashMap();
        android.content.Context a = com.navdy.hud.app.HudApplication.getAppContext();
        android.content.res.Resources a0 = a.getResources();
        vlistTitleTextW = a0.getDimensionPixelSize(R.dimen.vlist_title_text_len);
        vlistTitle_26 = a0.getDimension(R.dimen.vlist_title);
        vlistTitle_22 = a0.getDimension(R.dimen.vlist_title_22);
        vlistTitle_18 = a0.getDimension(R.dimen.vlist_title_18);
        vlistTitle_16 = a0.getDimension(R.dimen.vlist_title_16);
        vlistTitle_16_top_m_3 = a0.getDimension(R.dimen.vlist_16_title_top_m_3);
        vlistsubTitle_16_top_m_3 = a0.getDimension(R.dimen.vlist_16_subtitle_top_m_3);
        vlistsubTitle2_16_top_m_3 = a0.getDimension(R.dimen.vlist_16_subtitle2_top_m_3);
        vlistTitle_18_top_m_3 = a0.getDimension(R.dimen.vlist_18_title_top_m_3);
        vlistsubTitle_18_top_m_3 = a0.getDimension(R.dimen.vlist_18_subtitle_top_m_3);
        vlistsubTitle2_18_top_m_3 = a0.getDimension(R.dimen.vlist_18_subtitle2_top_m_3);
        vlistTitle_22_top_m_3 = a0.getDimension(R.dimen.vlist_22_title_top_m_3);
        vlistsubTitle_22_top_m_3 = a0.getDimension(R.dimen.vlist_22_subtitle_top_m_3);
        vlistsubTitle2_22_top_m_3 = a0.getDimension(R.dimen.vlist_22_subtitle2_top_m_3);
        vlistTitle_26_top_m_3 = a0.getDimension(R.dimen.vlist_26_title_top_m_3);
        vlistsubTitle_26_top_m_3 = a0.getDimension(R.dimen.vlist_26_subtitle_top_m_3);
        vlistsubTitle2_26_top_m_3 = a0.getDimension(R.dimen.vlist_26_subtitle2_top_m_3);
        vlistTitle_16_top_m_2 = a0.getDimension(R.dimen.vlist_16_title_top_m_2);
        vlistsubTitle_16_top_m_2 = a0.getDimension(R.dimen.vlist_16_subtitle_top_m_2);
        vlistTitle_18_top_m_2 = a0.getDimension(R.dimen.vlist_18_title_top_m_2);
        vlistsubTitle_18_top_m_2 = a0.getDimension(R.dimen.vlist_18_subtitle_top_m_2);
        vlistTitle_22_top_m_2 = a0.getDimension(R.dimen.vlist_22_title_top_m_2);
        vlistsubTitle_22_top_m_2 = a0.getDimension(R.dimen.vlist_22_subtitle_top_m_2);
        vlistTitle_26_top_m_2 = a0.getDimension(R.dimen.vlist_26_title_top_m_2);
        vlistsubTitle_26_top_m_2 = a0.getDimension(R.dimen.vlist_26_subtitle_top_m_2);
        vlistTitle_26_18_title_top = a0.getDimension(R.dimen.vlist_26_18_title_top);
        vlistTitle_26_18_subtitle_top = a0.getDimension(R.dimen.vlist_26_18_subtitle_top);
        vlistTitle_22_18_title_top = a0.getDimension(R.dimen.vlist_22_18_title_top);
        vlistTitle_22_18_subtitle_top = a0.getDimension(R.dimen.vlist_22_18_subtitle_top);
        vlistTitle_18_16_title_top = a0.getDimension(R.dimen.vlist_18_16_title_top);
        vlistTitle_18_16_subtitle_top = a0.getDimension(R.dimen.vlist_18_16_subtitle_top);
        vlistTitle_16_16_title_top = a0.getDimension(R.dimen.vlist_16_16_title_top);
        vlistTitle_16_16_subtitle_top = a0.getDimension(R.dimen.vlist_16_16_subtitle_top);
        vlistTitle_26_16_title_top = a0.getDimension(R.dimen.vlist_26_16_title_top);
        vlistTitle_26_16_subtitle_top = a0.getDimension(R.dimen.vlist_26_16_subtitle_top);
        vlistTitle_22_16_title_top = a0.getDimension(R.dimen.vlist_22_16_title_top);
        vlistTitle_22_16_subtitle_top = a0.getDimension(R.dimen.vlist_22_16_subtitle_top);
        com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo a1 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo();
        a1.titleFontSize = vlistTitle_26;
        a1.titleFontTopMargin = vlistTitle_26_top_m_3;
        a1.titleScale = 0.69f;
        a1.subTitleFontSize = vlistTitle_16;
        a1.subTitleFontTopMargin = vlistsubTitle_26_top_m_3;
        a1.subTitle2FontSize = vlistTitle_16;
        a1.subTitle2FontTopMargin = vlistsubTitle2_26_top_m_3;
        tit_subt_subt2_map.put(com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_26, a1);
        com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo a2 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo();
        a2.titleFontSize = vlistTitle_22;
        a2.titleFontTopMargin = vlistTitle_22_top_m_3;
        a2.titleScale = 0.81f;
        a2.titleSingleLine = false;
        a2.subTitleFontSize = vlistTitle_16;
        a2.subTitleFontTopMargin = vlistsubTitle_22_top_m_3;
        a2.subTitle2FontSize = vlistTitle_16;
        a2.subTitle2FontTopMargin = vlistsubTitle2_22_top_m_3;
        tit_subt_subt2_map.put(com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_22, a2);
        com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo a3 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo();
        a3.titleFontSize = vlistTitle_22;
        a3.titleFontTopMargin = vlistTitle_22_top_m_3;
        a3.titleScale = 0.81f;
        a3.titleSingleLine = false;
        a3.subTitleFontSize = vlistTitle_16;
        a3.subTitleFontTopMargin = vlistsubTitle_22_top_m_3 + 3f;
        a3.subTitle2FontSize = vlistTitle_16;
        a3.subTitle2FontTopMargin = vlistsubTitle2_22_top_m_3 + 3f;
        tit_subt_subt2_map.put(com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_22_2, a3);
        com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo a4 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo();
        a4.titleFontSize = vlistTitle_18;
        a4.titleFontTopMargin = vlistTitle_18_top_m_3;
        a4.titleScale = 1f;
        a4.subTitleFontSize = vlistTitle_16;
        a4.subTitleFontTopMargin = vlistsubTitle_18_top_m_3;
        a4.subTitle2FontSize = vlistTitle_16;
        a4.subTitle2FontTopMargin = vlistsubTitle2_18_top_m_3;
        tit_subt_subt2_map.put(com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_18, a4);
        com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo a5 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo();
        a5.titleFontSize = vlistTitle_18;
        a5.titleFontTopMargin = vlistTitle_18_top_m_3;
        a5.titleScale = 1f;
        a5.titleSingleLine = false;
        a5.subTitleFontSize = vlistTitle_16;
        a5.subTitleFontTopMargin = vlistsubTitle_18_top_m_3;
        a5.subTitle2FontSize = vlistTitle_16;
        a5.subTitle2FontTopMargin = vlistsubTitle2_18_top_m_3;
        tit_subt_subt2_map.put(com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_18_2, a5);
        com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo a6 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo();
        a6.titleFontSize = vlistTitle_16;
        a6.titleFontTopMargin = vlistTitle_16_top_m_3;
        a6.titleScale = 1f;
        a6.subTitleFontSize = vlistTitle_16;
        a6.subTitleFontTopMargin = vlistsubTitle_16_top_m_3;
        a6.subTitle2FontSize = vlistTitle_16;
        a6.subTitle2FontTopMargin = vlistsubTitle2_16_top_m_3;
        tit_subt_subt2_map.put(com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_16, a6);
        com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo a7 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo();
        a7.titleFontSize = vlistTitle_16;
        a7.titleFontTopMargin = vlistTitle_16_top_m_3;
        a7.titleScale = 1f;
        a7.titleSingleLine = false;
        a7.subTitleFontSize = vlistTitle_16;
        a7.subTitleFontTopMargin = vlistsubTitle_16_top_m_3;
        a7.subTitle2FontSize = vlistTitle_16;
        a7.subTitle2FontTopMargin = vlistsubTitle2_16_top_m_3;
        tit_subt_subt2_map.put(com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_16_2, a7);
        com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo a8 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo();
        a8.titleFontSize = vlistTitle_26;
        a8.titleFontTopMargin = vlistTitle_26_top_m_2;
        a8.titleScale = 0.69f;
        a8.subTitleFontSize = vlistTitle_16;
        a8.subTitleFontTopMargin = vlistsubTitle_26_top_m_2;
        tit_subt_map.put(com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_26, a8);
        com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo a9 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo();
        a9.titleFontSize = vlistTitle_22;
        a9.titleFontTopMargin = vlistTitle_22_top_m_2;
        a9.titleScale = 0.81f;
        a9.subTitleFontSize = vlistTitle_16;
        a9.subTitleFontTopMargin = vlistsubTitle_22_top_m_2;
        tit_subt_map.put(com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_22, a9);
        com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo a10 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo();
        a10.titleFontSize = vlistTitle_22;
        a10.titleFontTopMargin = vlistTitle_22_top_m_2 + 3f;
        a10.titleScale = 0.81f;
        a10.titleSingleLine = false;
        a10.subTitleFontSize = vlistTitle_16;
        a10.subTitleFontTopMargin = vlistsubTitle_22_top_m_2 + 9f;
        tit_subt_map.put(com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_22_2, a10);
        com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo a11 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo();
        a11.titleFontSize = vlistTitle_18;
        a11.titleFontTopMargin = vlistTitle_18_top_m_2;
        a11.titleScale = 1f;
        a11.subTitleFontSize = vlistTitle_16;
        a11.subTitleFontTopMargin = vlistsubTitle_18_top_m_2;
        tit_subt_map.put(com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_18, a11);
        com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo a12 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo();
        a12.titleFontSize = vlistTitle_18;
        a12.titleFontTopMargin = vlistTitle_18_top_m_2 + 1f;
        a12.titleScale = 1f;
        a12.titleSingleLine = false;
        a12.subTitleFontSize = vlistTitle_16;
        a12.subTitleFontTopMargin = vlistsubTitle_18_top_m_2 + 7f;
        tit_subt_map.put(com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_18_2, a12);
        com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo a13 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo();
        a13.titleFontSize = vlistTitle_16;
        a13.titleFontTopMargin = vlistTitle_16_top_m_2;
        a13.titleScale = 1f;
        a13.subTitleFontSize = vlistTitle_16;
        a13.subTitleFontTopMargin = vlistsubTitle_16_top_m_2;
        tit_subt_map.put(com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_16, a13);
        com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo a14 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo();
        a14.titleFontSize = vlistTitle_16;
        a14.titleFontTopMargin = vlistTitle_16_top_m_2 + 1f;
        a14.titleScale = 1f;
        a14.titleSingleLine = false;
        a14.subTitleFontSize = vlistTitle_16;
        a14.subTitleFontTopMargin = vlistsubTitle_16_top_m_2 + 7f;
        tit_subt_map.put(com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_16_2, a14);
        com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo a15 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo();
        a15.titleFontSize = vlistTitle_26;
        a15.titleFontTopMargin = vlistTitle_26_18_title_top;
        a15.titleScale = 0.69f;
        a15.subTitleFontSize = vlistTitle_18;
        a15.subTitleFontTopMargin = vlistTitle_26_18_subtitle_top;
        tit_subt_map_2_lines.put(com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_26_18, a15);
        com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo a16 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo();
        a16.titleFontSize = vlistTitle_22;
        a16.titleFontTopMargin = vlistTitle_22_18_title_top;
        a16.titleScale = 0.81f;
        a16.subTitleFontSize = vlistTitle_18;
        a16.subTitleFontTopMargin = vlistTitle_22_18_subtitle_top;
        tit_subt_map_2_lines.put(com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_22_18, a16);
        com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo a17 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo();
        a17.titleFontSize = vlistTitle_22;
        a17.titleFontTopMargin = vlistTitle_22_18_title_top;
        a17.titleScale = 0.81f;
        a17.titleSingleLine = false;
        a17.subTitleFontSize = vlistTitle_18;
        a17.subTitleFontTopMargin = vlistTitle_22_18_subtitle_top;
        tit_subt_map_2_lines.put(com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_22_2_18, a17);
        com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo a18 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo();
        a18.titleFontSize = vlistTitle_18;
        a18.titleFontTopMargin = vlistTitle_18_16_title_top;
        a18.titleScale = 1f;
        a18.subTitleFontSize = vlistTitle_16;
        a18.subTitleFontTopMargin = vlistTitle_18_16_subtitle_top;
        tit_subt_map_2_lines.put(com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_18_16, a18);
        com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo a19 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo();
        a19.titleFontSize = vlistTitle_18;
        a19.titleFontTopMargin = vlistTitle_18_top_m_2 + 1f;
        a19.titleScale = 1f;
        a19.titleSingleLine = false;
        a19.subTitleFontSize = vlistTitle_16;
        a19.subTitleFontTopMargin = vlistsubTitle_18_top_m_2 + 7f;
        tit_subt_map_2_lines.put(com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_18_2_16, a19);
        com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo a20 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo();
        a20.titleFontSize = vlistTitle_16;
        a20.titleFontTopMargin = vlistTitle_16_16_title_top;
        a20.titleScale = 1f;
        a20.subTitleFontSize = vlistTitle_16;
        a20.subTitleFontTopMargin = vlistTitle_16_16_subtitle_top;
        tit_subt_map_2_lines.put(com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_16_16, a20);
        com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo a21 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo();
        a21.titleFontSize = vlistTitle_16;
        a21.titleFontTopMargin = vlistTitle_16_16_title_top;
        a21.titleScale = 1f;
        a21.titleSingleLine = false;
        a21.subTitleFontSize = vlistTitle_16;
        a21.subTitleFontTopMargin = vlistTitle_16_16_subtitle_top;
        tit_subt_map_2_lines.put(com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_16_2_16, a21);
        com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo a22 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo();
        a22.titleFontSize = vlistTitle_26;
        a22.titleFontTopMargin = vlistTitle_26_16_title_top;
        a22.titleScale = 0.69f;
        a22.subTitleFontSize = vlistTitle_16;
        a22.subTitleFontTopMargin = vlistTitle_26_16_subtitle_top;
        tit_subt_map_2_lines.put(com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_26_16, a22);
        com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo a23 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo();
        a23.titleFontSize = vlistTitle_22;
        a23.titleFontTopMargin = vlistTitle_22_16_title_top;
        a23.titleScale = 0.81f;
        a23.subTitleFontSize = vlistTitle_16;
        a23.subTitleFontTopMargin = vlistTitle_22_16_subtitle_top;
        tit_subt_map_2_lines.put(com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_22_16, a23);
        com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo a24 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo();
        a24.titleFontSize = vlistTitle_22;
        a24.titleFontTopMargin = vlistTitle_22_top_m_2 + 1f;
        a24.titleScale = 0.81f;
        a24.titleSingleLine = false;
        a24.subTitleFontSize = vlistTitle_16;
        a24.subTitleFontTopMargin = vlistTitle_22_16_subtitle_top + 6f;
        tit_subt_map_2_lines.put(com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_22_2_16, a24);
        fontSizeTextView = new android.widget.TextView(a);
        fontSizeTextView.setTextAppearance(a, R.style.vlist_title);
        BLANK_ITEM_TOP = com.navdy.hud.app.ui.component.vlist.viewholder.BlankViewHolder.buildModel();
        BLANK_ITEM_BOTTOM = com.navdy.hud.app.ui.component.vlist.viewholder.BlankViewHolder.buildModel();
        float[] a25 = new float[4];
        a25[0] = vlistTitle_26;
        a25[1] = vlistTitle_22;
        a25[2] = vlistTitle_18;
        a25[3] = vlistTitle_16;
        TITLE_SIZES = a25;
        int[] a26 = new int[4];
        a26[0] = 1;
        a26[1] = 2;
        a26[2] = 2;
        a26[3] = 2;
        TWO_LINE_MAX_LINES = a26;
        int[] a27 = new int[4];
        a27[0] = 1;
        a27[1] = 1;
        a27[2] = 1;
        a27[3] = 1;
        SINGLE_LINE_MAX_LINES = a27;
        com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize[] a28 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize[8];
        a28[0] = com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_26;
        a28[1] = com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_26;
        a28[2] = com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_22;
        a28[3] = com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_22_2;
        a28[4] = com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_18;
        a28[5] = com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_18_2;
        a28[6] = com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_16;
        a28[7] = com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_16_2;
        FONT_SIZES = a28;
    }
    
    public VerticalList(com.navdy.hud.app.ui.component.vlist.VerticalRecyclerView a, com.navdy.hud.app.ui.component.carousel.CarouselIndicator a0, com.navdy.hud.app.ui.component.vlist.VerticalList$Callback a1, com.navdy.hud.app.ui.component.vlist.VerticalList$ContainerCallback a2) {
        this(a, a0, a1, a2, false);
    }
    
    public VerticalList(com.navdy.hud.app.ui.component.vlist.VerticalRecyclerView a, com.navdy.hud.app.ui.component.carousel.CarouselIndicator a0, com.navdy.hud.app.ui.component.vlist.VerticalList$Callback a1, com.navdy.hud.app.ui.component.vlist.VerticalList$ContainerCallback a2, boolean b) {
        this.scrollPendingPos = -1;
        this.indicatorAnimationListener = new com.navdy.hud.app.ui.component.vlist.VerticalList$1(this);
        this.scrollItemIndex = -1;
        this.scrollItemStartY = -1;
        this.scrollItemEndY = -1;
        this.scrollItemHeight = -1;
        this.initialPosCheckRunnable = (Runnable)new com.navdy.hud.app.ui.component.vlist.VerticalList$2(this);
        this.keyHandlerState = new com.navdy.hud.app.ui.component.vlist.VerticalList$KeyHandlerState();
        this.itemSelectionState = new com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState();
        this.copyList = new java.util.ArrayList();
        this.scrollListener = new com.navdy.hud.app.ui.component.vlist.VerticalList$3(this);
        this.selectedList = new java.util.HashSet();
        if (a != null && a0 != null && a1 != null) {
            sLogger.v("ctor");
            this.twoLineTitles = b;
            this.animationDuration = 230;
            android.content.Context a3 = a.getContext();
            this.recyclerView = a;
            this.recyclerView.setItemViewCacheSize(5);
            android.support.v7.widget.RecyclerView$RecycledViewPool a4 = this.recyclerView.getRecycledViewPool();
            a4.setMaxRecycledViews(com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.BLANK.ordinal(), 2);
            a4.setMaxRecycledViews(com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.ICON.ordinal(), 10);
            a4.setMaxRecycledViews(com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.ICON_BKCOLOR.ordinal(), 10);
            a4.setMaxRecycledViews(com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.TWO_ICONS.ordinal(), 10);
            a4.setMaxRecycledViews(com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.TITLE.ordinal(), 1);
            a4.setMaxRecycledViews(com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.TITLE_SUBTITLE.ordinal(), 1);
            a4.setMaxRecycledViews(com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.LOADING.ordinal(), 1);
            a4.setMaxRecycledViews(com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.ICON_OPTIONS.ordinal(), 1);
            a4.setMaxRecycledViews(com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.SCROLL_CONTENT.ordinal(), 0);
            a4.setMaxRecycledViews(com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.LOADING_CONTENT.ordinal(), 10);
            this.recyclerView.setOverScrollMode(2);
            this.recyclerView.setVerticalScrollBarEnabled(false);
            this.recyclerView.setHorizontalScrollBarEnabled(false);
            this.indicator = a0;
            this.indicator.setOrientation(com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation.VERTICAL);
            this.callback = a1;
            this.containerCallback = a2;
            this.layoutManager = new com.navdy.hud.app.ui.component.vlist.VerticalLayoutManager(a3, this, a1, a);
            this.layoutManager.setOrientation(1);
            this.recyclerView.setLayoutManager((android.support.v7.widget.RecyclerView$LayoutManager)this.layoutManager);
            this.recyclerView.addOnScrollListener(this.scrollListener);
            return;
        }
        throw new IllegalArgumentException();
    }
    
    static com.navdy.hud.app.ui.component.carousel.CarouselIndicator access$000(com.navdy.hud.app.ui.component.vlist.VerticalList a) {
        return a.indicator;
    }
    
    static boolean access$1002(com.navdy.hud.app.ui.component.vlist.VerticalList a, boolean b) {
        a.isScrollBy = b;
        return b;
    }
    
    static android.animation.AnimatorSet access$102(com.navdy.hud.app.ui.component.vlist.VerticalList a, android.animation.AnimatorSet a0) {
        a.indicatorAnimatorSet = a0;
        return a0;
    }
    
    static boolean access$1102(com.navdy.hud.app.ui.component.vlist.VerticalList a, boolean b) {
        a.switchingScrollBoundary = b;
        return b;
    }
    
    static int access$1202(com.navdy.hud.app.ui.component.vlist.VerticalList a, int i) {
        a.scrollByUpReceived = i;
        return i;
    }
    
    static int access$1300(com.navdy.hud.app.ui.component.vlist.VerticalList a, int i) {
        return a.getPositionFromScrollY(i);
    }
    
    static int access$1400(com.navdy.hud.app.ui.component.vlist.VerticalList a) {
        return a.currentMiddlePosition;
    }
    
    static int access$1402(com.navdy.hud.app.ui.component.vlist.VerticalList a, int i) {
        a.currentMiddlePosition = i;
        return i;
    }
    
    static boolean access$1500(com.navdy.hud.app.ui.component.vlist.VerticalList a) {
        return a.isSelectedOperationPending;
    }
    
    static boolean access$1502(com.navdy.hud.app.ui.component.vlist.VerticalList a, boolean b) {
        a.isSelectedOperationPending = b;
        return b;
    }
    
    static void access$1600(com.navdy.hud.app.ui.component.vlist.VerticalList a, boolean b) {
        a.select(b);
    }
    
    static int access$1700(com.navdy.hud.app.ui.component.vlist.VerticalList a) {
        return a.scrollPendingPos;
    }
    
    static int access$1702(com.navdy.hud.app.ui.component.vlist.VerticalList a, int i) {
        a.scrollPendingPos = i;
        return i;
    }
    
    static boolean access$1800(com.navdy.hud.app.ui.component.vlist.VerticalList a) {
        return a.isCloseMenuVisible();
    }
    
    static boolean access$200(com.navdy.hud.app.ui.component.vlist.VerticalList a) {
        return a.initialPosChanged;
    }
    
    static boolean access$202(com.navdy.hud.app.ui.component.vlist.VerticalList a, boolean b) {
        a.initialPosChanged = b;
        return b;
    }
    
    static com.navdy.hud.app.ui.component.vlist.VerticalLayoutManager access$300(com.navdy.hud.app.ui.component.vlist.VerticalList a) {
        return a.layoutManager;
    }
    
    static int access$400(com.navdy.hud.app.ui.component.vlist.VerticalList a) {
        return a.actualScrollY;
    }
    
    static int access$402(com.navdy.hud.app.ui.component.vlist.VerticalList a, int i) {
        a.actualScrollY = i;
        return i;
    }
    
    static com.navdy.service.library.log.Logger access$500() {
        return sLogger;
    }
    
    static void access$600(com.navdy.hud.app.ui.component.vlist.VerticalList a) {
        a.onItemSelected();
    }
    
    static int access$702(com.navdy.hud.app.ui.component.vlist.VerticalList a, int i) {
        a.lastScrollState = i;
        return i;
    }
    
    static void access$800(com.navdy.hud.app.ui.component.vlist.VerticalList a) {
        a.calculateScrollRange();
    }
    
    static boolean access$902(com.navdy.hud.app.ui.component.vlist.VerticalList a, boolean b) {
        a.isScrolling = b;
        return b;
    }
    
    private void addExtraItems(java.util.List a) {
        if (this.firstEntryBlank) {
            a.add(0, BLANK_ITEM_TOP);
        }
        a.add(BLANK_ITEM_BOTTOM);
    }
    
    private void animate(int i, int i0) {
        this.animate(i, i0, true, false, false);
    }
    
    private void animate(int i, int i0, boolean b, boolean b0, boolean b1) {
        if (i != -1) {
            android.support.v7.widget.RecyclerView$ViewHolder a = this.recyclerView.findViewHolderForAdapterPosition(i);
            if (a == null) {
                sLogger.v(new StringBuilder().append("viewHolder {").append(i).append("} not found").toString());
            } else {
                com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder a0 = (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder)a;
                if (a0.getState() == com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.SELECTED) {
                    a0.setState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.UNSELECTED, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.MOVE, 230);
                }
            }
            if (b0) {
                android.support.v7.widget.RecyclerView$ViewHolder a1 = this.recyclerView.findViewHolderForAdapterPosition(i0);
                if (a1 == null) {
                    if (b1) {
                        com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder a2 = this.adapter.getHolderForPos(i0);
                        if (a2 != null) {
                            if (a2.getState() == com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.UNSELECTED && !this.isCloseMenuVisible()) {
                                a2.setState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.SELECTED, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.MOVE, 230);
                            }
                        } else {
                            sLogger.v(new StringBuilder().append("viewHolder-sel {").append(i0).append("} not found in adapter").toString());
                            this.adapter.setHighlightIndex(i0);
                        }
                    } else {
                        sLogger.v(new StringBuilder().append("viewHolder-sel {").append(i0).append("} not found").toString());
                    }
                } else {
                    com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder a3 = (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder)a1;
                    if (a3.getState() == com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.UNSELECTED && !this.isCloseMenuVisible()) {
                        a3.setState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.SELECTED, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.MOVE, 230);
                    }
                }
            }
        }
        if (b) {
            this.animateIndicator(this.getCurrentPosition(), this.animationDuration);
        }
    }
    
    private void calculateScrollPos(int i) {
        if (this.scrollItemHeight != -1 && i == this.scrollItemIndex + 1) {
            this.reCalcScrollPos = false;
            this.actualScrollY = this.scrollItemIndex * com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight;
            this.actualScrollY = this.actualScrollY + com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight * 2;
            sLogger.v(new StringBuilder().append("onScrollStateChanged(( calculateScrollPos:").append(this.actualScrollY).append(" pos=").append(i).append(" scrollIndex=").append(this.scrollItemIndex).toString());
        }
    }
    
    private void calculateScrollRange() {
        com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder a = (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder)this.recyclerView.findViewHolderForAdapterPosition(this.scrollItemIndex);
        boolean b = a instanceof com.navdy.hud.app.ui.component.vlist.viewholder.ScrollableViewHolder;
        label2: {
            label0: {
                label1: {
                    if (!b) {
                        break label1;
                    }
                    if (a.layout.getChildCount() > 0) {
                        break label0;
                    }
                }
                sLogger.v(new StringBuilder().append("calculateScrollRange:").append(this.scrollItemIndex).append(" vh not found").toString());
                break label2;
            }
            this.scrollItemStartY = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight * this.scrollItemIndex;
            this.scrollItemHeight = a.layout.getChildAt(0).getMeasuredHeight();
            this.scrollItemEndY = this.scrollItemStartY + this.scrollItemHeight - com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight * 3;
            if (this.reCalcScrollPos) {
                sLogger.v(new StringBuilder().append("calculateScrollPos: recalc scrollItemHeight=").append(this.scrollItemHeight).append(" scrollItemStartY=").append(this.scrollItemStartY).append(" scrollItemEndY=").append(this.scrollItemEndY).toString());
                this.calculateScrollPos(this.currentMiddlePosition);
            }
        }
    }
    
    private int canScrollDown() {
        int i = 0;
        if (this.hasScrollableElement) {
            this.calculateScrollRange();
            i = (this.scrollItemStartY == -1) ? -1 : (this.actualScrollY >= this.scrollItemEndY) ? -1 : this.scrollItemEndY - this.actualScrollY;
        } else {
            i = -1;
        }
        return i;
    }
    
    private int canScrollUp() {
        int i = 0;
        if (this.hasScrollableElement) {
            this.calculateScrollRange();
            if (this.scrollItemStartY == -1) {
                i = -1;
            } else {
                int i0 = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight * this.scrollItemIndex;
                i = (this.actualScrollY <= i0) ? -1 : this.actualScrollY - i0;
            }
        } else {
            i = -1;
        }
        return i;
    }
    
    private void cleanupView(android.support.v7.widget.RecyclerView$RecycledViewPool a, int i) {
        while(true) {
            com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder a0 = (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder)a.getRecycledView(i);
            if (a0 == null) {
                return;
            }
            a0.clearAnimation();
            a0.layout.removeAllViews();
        }
    }
    
    private void clearViewAnimation(android.support.v7.widget.RecyclerView$RecycledViewPool a, int i) {
        while(true) {
            com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder a0 = (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder)a.getRecycledView(i);
            if (a0 == null) {
                return;
            }
            a0.clearAnimation();
            this.copyList.add(a0);
        }
    }
    
    private void down(int i, int i0) {
        com.navdy.hud.app.audio.SoundUtils.playSound(com.navdy.hud.app.audio.SoundUtils$Sound.MENU_MOVE);
        this.currentMiddlePosition = i0;
        this.animate(i, i0);
        this.isScrolling = true;
        this.layoutManager.smoothScrollToPosition(i, -1);
    }
    
    public static android.view.View findCrossFadeImageView(android.view.View a) {
        return a.findViewById(R.id.vlist_image);
    }
    
    public static android.widget.ImageView findImageView(android.view.View a) {
        return (android.widget.ImageView)a.findViewById(R.id.big);
    }
    
    public static android.widget.ImageView findSmallImageView(android.view.View a) {
        return (android.widget.ImageView)a.findViewById(R.id.small);
    }
    
    private int getExtraItemCount() {
        return this.adapter.getItemCount() - 3;
    }
    
    public static com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo getFontInfo(com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize a) {
        return (com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo)tit_subt_map.get(a);
    }
    
    public static android.view.animation.Interpolator getInterpolator() {
        return (android.view.animation.Interpolator)new com.navdy.hud.app.ui.interpolator.FastOutSlowInInterpolator();
    }
    
    private int getPositionFromScrollY(int i) {
        int i0 = 0;
        if (this.hasScrollableElement) {
            if (this.scrollItemStartY == -1) {
                this.calculateScrollRange();
            }
            i0 = (i >= this.scrollItemStartY) ? (i <= this.scrollItemEndY) ? this.scrollItemIndex : (i - this.scrollItemEndY - com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight * 2) / com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight + 1 + this.scrollItemIndex : i / com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight + 1;
        } else {
            i0 = i / com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight + 1;
        }
        return i0;
    }
    
    private boolean isCloseMenuVisible() {
        return this.containerCallback != null && this.containerCallback.isCloseMenuVisible();
    }
    
    private boolean isLocked() {
        return this.lockList;
    }
    
    private boolean isOverrideKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        boolean b = false;
        com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder a0 = (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder)this.recyclerView.findViewHolderForAdapterPosition(this.currentMiddlePosition);
        label2: {
            label0: {
                label1: {
                    if (a0 == null) {
                        break label1;
                    }
                    if (a0.handleKey(a)) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    private boolean isScrollItemAlignedToBottomEdge() {
        boolean b = false;
        if (this.hasScrollableElement) {
            int i = this.scrollItemEndY;
            int i0 = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight;
            b = this.actualScrollY == i + i0;
        } else {
            b = false;
        }
        return b;
    }
    
    private boolean isScrollItemAlignedToTopEdge() {
        boolean b = false;
        if (this.hasScrollableElement) {
            int i = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight;
            int i0 = this.scrollItemIndex;
            b = this.actualScrollY == i * i0;
        } else {
            b = false;
        }
        return b;
    }
    
    private boolean isScrollItemInMiddle() {
        boolean b = false;
        if (this.hasScrollableElement) {
            if (this.scrollItemIndex > 0) {
                int i = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight;
                int i0 = this.scrollItemIndex;
                b = this.actualScrollY == i * (i0 - 1);
            } else {
                b = false;
            }
        } else {
            b = false;
        }
        return b;
    }
    
    private boolean isScrollPosInScrollItem() {
        boolean b = false;
        if (this.hasScrollableElement) {
            this.calculateScrollRange();
            b = this.scrollItemStartY != -1 && this.actualScrollY >= this.scrollItemStartY && this.actualScrollY <= this.scrollItemEndY;
        } else {
            b = false;
        }
        return b;
    }
    
    private void onItemSelected() {
        int i = this.getCurrentPosition();
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a = this.adapter.getModel(this.currentMiddlePosition);
        if (a != null) {
            int i0 = 0;
            int i1 = 0;
            if (a.type != com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.ICON_OPTIONS) {
                i0 = -1;
                i1 = -1;
            } else {
                com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder a0 = (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder)this.recyclerView.findViewHolderForAdapterPosition(this.currentMiddlePosition);
                if (a0 instanceof com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder) {
                    com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder a1 = (com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder)a0;
                    i0 = a1.getCurrentSelectionId();
                    i1 = a1.getCurrentSelection();
                } else {
                    i0 = -1;
                    i1 = -1;
                }
            }
            this.itemSelectionState.set(a, a.id, i, i0, i1);
            this.callback.onItemSelected(this.itemSelectionState);
        }
    }
    
    private void select(boolean b) {
        boolean b0 = this.isLocked();
        label0: {
            label1: {
                if (!b0) {
                    break label1;
                }
                if (!b) {
                    break label0;
                }
            }
            this.lockList = true;
            if (this.isScrolling) {
                this.isSelectedOperationPending = true;
            } else {
                com.navdy.hud.app.audio.SoundUtils.playSound(com.navdy.hud.app.audio.SoundUtils$Sound.MENU_SELECT);
                int i = this.getRawPosition();
                com.navdy.hud.app.ui.component.vlist.VerticalList$Model a = (com.navdy.hud.app.ui.component.vlist.VerticalList$Model)this.modelList.get(i);
                com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder a0 = (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder)this.recyclerView.findViewHolderForAdapterPosition(i);
                if (a0 != null) {
                    a0.select(a, this.getCurrentPosition(), 50);
                }
            }
        }
    }
    
    public static void setFontSize(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, boolean b) {
        a.fontSizeCheckDone = true;
        a.subTitle_2Lines = false;
        label2: if (a.title != null) {
            String s = a.subTitle2;
            label0: {
                label1: {
                    if (s != null) {
                        break label1;
                    }
                    if (a.subTitle != null) {
                        break label0;
                    }
                }
                com.navdy.hud.app.ui.component.vlist.VerticalList.setTitleSize(a, b);
                break label2;
            }
            com.navdy.hud.app.ui.component.vlist.VerticalList.setTitleSubTitleSize(a, b);
        } else {
            a.fontInfo = (com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo)tit_subt_map.get(com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_26);
            a.fontSize = com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_26;
        }
    }
    
    private static void setTitleSize(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, boolean b) {
        fontSizeTextView.setText((CharSequence)a.title);
        int[] a0 = new int[2];
        com.navdy.hud.app.util.ViewUtil.autosize(fontSizeTextView, b ? TWO_LINE_MAX_LINES : SINGLE_LINE_MAX_LINES, vlistTitleTextW, TITLE_SIZES, a0);
        int i = a0[0];
        int i0 = a0[1];
        com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize a1 = FONT_SIZES[i * 2 + (i0 - 1)];
        sLogger.d(new StringBuilder().append("Setting size for ").append(a.title).append(" to ").append(a1).toString());
        String s = a.subTitle;
        label1: {
            label0: {
                if (s == null) {
                    break label0;
                }
                if (a.subTitle2 == null) {
                    break label0;
                }
                a.fontInfo = (com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo)tit_subt_subt2_map.get(a1);
                break label1;
            }
            a.fontInfo = (com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo)tit_subt_map.get(a1);
        }
        a.fontSize = a1;
    }
    
    private static void setTitleSubTitleSize(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, boolean b) {
        android.text.TextPaint a0 = fontSizeTextView.getPaint();
        fontSizeTextView.setTextSize(vlistTitle_18);
        android.text.StaticLayout a1 = new android.text.StaticLayout((CharSequence)a.subTitle, a0, vlistTitleTextW, android.text.Layout$Alignment.ALIGN_NORMAL, 1f, 0.0f, false);
        int i = a1.getLineCount();
        if (((android.text.TextUtils.isEmpty((CharSequence)a.subTitle2)) ? i : i + 1) > 1) {
            b = false;
        }
        com.navdy.hud.app.ui.component.vlist.VerticalList.setTitleSize(a, b);
        if (i != 1) {
            com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize a2 = null;
            a.subTitle_2Lines = true;
            if (a1.getLineCount() != 2) {
                switch(com.navdy.hud.app.ui.component.vlist.VerticalList$4.$SwitchMap$com$navdy$hud$app$ui$component$vlist$VerticalList$FontSize[a.fontSize.ordinal()]) {
                    case 7: {
                        a2 = com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_16_2_16;
                        break;
                    }
                    case 6: {
                        a2 = com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_16_16;
                        break;
                    }
                    case 5: {
                        a2 = com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_18_2_16;
                        break;
                    }
                    case 4: {
                        a2 = com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_18_16;
                        break;
                    }
                    case 3: {
                        a2 = com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_22_2_16;
                        break;
                    }
                    case 2: {
                        a2 = com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_22_16;
                        break;
                    }
                    case 1: {
                        a2 = com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_26_16;
                        break;
                    }
                    default: {
                        a2 = null;
                    }
                }
            } else {
                int i0 = com.navdy.hud.app.ui.component.vlist.VerticalList$4.$SwitchMap$com$navdy$hud$app$ui$component$vlist$VerticalList$FontSize[a.fontSize.ordinal()];
                a2 = null;
                switch(i0) {
                    case 7: {
                        a2 = com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_16_2_16;
                        break;
                    }
                    case 6: {
                        a2 = com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_16_16;
                        break;
                    }
                    case 5: {
                        a2 = com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_18_2_16;
                        break;
                    }
                    case 4: {
                        a2 = com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_18_16;
                        break;
                    }
                    case 3: {
                        a2 = com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_22_2_16;
                        break;
                    }
                    case 2: {
                        a2 = com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_22_18;
                        break;
                    }
                    case 1: {
                        a2 = com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_26_18;
                        break;
                    }
                }
            }
            if (a2 != null) {
                a.fontInfo = (com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo)tit_subt_map_2_lines.get(a2);
                a.fontSize = a2;
                sLogger.d(new StringBuilder().append("Adjusting size for ").append(a.title).append(" to ").append(a2).toString());
            }
        }
    }
    
    private void up(int i, int i0) {
        com.navdy.hud.app.audio.SoundUtils.playSound(com.navdy.hud.app.audio.SoundUtils$Sound.MENU_MOVE);
        this.currentMiddlePosition = i0;
        this.isScrolling = true;
        this.keyHandlerState.keyHandled = true;
        if (this.hasScrollableElement) {
            boolean b = false;
            boolean b0 = false;
            if (this.isScrollItemInMiddle()) {
                this.switchingScrollBoundary = true;
                this.recyclerView.smoothScrollBy(0, -com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight);
                b = true;
                b0 = false;
            } else if (this.isScrollItemAlignedToTopEdge()) {
                this.isScrollBy = true;
                this.switchingScrollBoundary = true;
                this.layoutManager.smoothScrollToPosition(this.currentMiddlePosition - 1, -1);
                b = true;
                b0 = true;
            } else if (this.currentMiddlePosition != this.scrollItemIndex) {
                this.layoutManager.smoothScrollToPosition(i, 1);
                b = false;
                b0 = false;
            } else {
                float f = (float)(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight * 2);
                this.switchingScrollBoundary = true;
                this.recyclerView.smoothScrollBy(0, (int)(-f));
                b = true;
                b0 = false;
            }
            this.animate(i, i0, true, b, b0);
        } else {
            this.animate(i, i0);
            this.layoutManager.smoothScrollToPosition(i, 1);
        }
    }
    
    private void updateView(java.util.List a, int i, boolean b, boolean b0, int i0) {
        if (!com.navdy.hud.app.util.GenericUtil.isMainThread()) {
            throw new RuntimeException("updateView can only be called from main thread");
        }
        if (this.isLocked()) {
            sLogger.w("cannot update vlist during lock");
        } else {
            int i1 = 0;
            boolean b1 = false;
            int i2 = a.size();
            if (i2 == 0) {
                throw new IllegalArgumentException("empty list now allowed");
            }
            this.clearAllAnimations();
            this.recyclerView.stopScroll();
            sLogger.v(new StringBuilder().append("updateView [").append(i2).append("] sel:").append(i).append(" firstEntryBlank:").append(b).append(" scrollContent:").append(b0).append(" scrollIndex:").append(i0).toString());
            this.modelList = (java.util.List)new java.util.ArrayList((java.util.Collection)a);
            if (i2 <= 1) {
                i = 0;
            }
            this.firstEntryBlank = b;
            this.hasScrollableElement = b0;
            this.scrollItemIndex = i0;
            this.scrollItemStartY = -1;
            this.scrollItemEndY = -1;
            this.scrollItemHeight = -1;
            this.addExtraItems(this.modelList);
            this.currentMiddlePosition = i + 1;
            if (b0 && b) {
                this.scrollItemIndex = this.scrollItemIndex + 1;
            }
            if (!b) {
                i2 = i2 - 1;
            }
            this.indicator.setItemCount(i2);
            this.indicator.setCurrentItem(i);
            this.layoutManager.clear();
            this.adapter = new com.navdy.hud.app.ui.component.vlist.VerticalAdapter(this.modelList, this);
            this.adapter.setHasStableIds(true);
            if (b0) {
                int[] a0 = null;
                if (a.size() != this.scrollItemIndex) {
                    a0 = new int[2];
                    a0[0] = this.scrollItemIndex - 1;
                    a0[1] = this.scrollItemIndex + 1;
                    i1 = 2;
                } else {
                    a0 = new int[1];
                    a0[0] = this.scrollItemIndex - 1;
                    i1 = 1;
                }
                this.adapter.setViewHolderCacheIndex(a0);
            } else {
                i1 = 0;
            }
            this.recyclerView.setAdapter((android.support.v7.widget.RecyclerView$Adapter)this.adapter);
            this.initialPosChanged = false;
            int i3 = this.currentMiddlePosition - 1;
            if (b0) {
                sLogger.v(new StringBuilder().append("updateView initial:").append(i).append(" scrollIndex=").append(i0).toString());
                if (i != i0) {
                    if (i >= this.scrollItemIndex) {
                        this.reCalcScrollPos = true;
                        sLogger.v("reCalcScrollPos no ht");
                        this.actualScrollY = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight * i;
                        b1 = i == i0 + 1;
                    } else {
                        this.actualScrollY = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight * i;
                        b1 = false;
                    }
                } else {
                    i3 = this.currentMiddlePosition;
                    this.actualScrollY = (i + 1) * com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight;
                    sLogger.v(new StringBuilder().append("updateView: scroll index selected:").append(this.actualScrollY).toString());
                    b1 = false;
                }
            } else {
                this.actualScrollY = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight * i;
                b1 = false;
            }
            if (b1) {
                sLogger.v("updateView special scroll");
                this.layoutManager.scrollToPositionWithOffset(i3 + 1, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight);
            } else {
                this.layoutManager.scrollToPositionWithOffset(i3, 0);
            }
            sLogger.v(new StringBuilder().append("updateView scroll to ").append(i3).append(" scrollPos=").append(this.actualScrollY).append(" middle=").append(this.getCurrentPosition()).append(" raw=").append(this.getRawPosition()).append(" adapter-cache:").append(i1).append(" scrollIndex:").append(i0).toString());
            handler.post(this.initialPosCheckRunnable);
        }
    }
    
    public void addCurrentHighlightAnimation(android.animation.AnimatorSet$Builder a) {
        com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder a0 = (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder)this.recyclerView.findViewHolderForAdapterPosition(this.currentMiddlePosition - 1);
        if (a0 != null) {
            android.view.ViewGroup a1 = a0.layout;
            android.util.Property a2 = android.view.View.ALPHA;
            float[] a3 = new float[1];
            a3[0] = 0.0f;
            a.with((android.animation.Animator)android.animation.ObjectAnimator.ofFloat(a1, a2, a3));
        }
        com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder a4 = (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder)this.recyclerView.findViewHolderForAdapterPosition(this.currentMiddlePosition + 1);
        if (a4 != null) {
            android.view.ViewGroup a5 = a4.layout;
            android.util.Property a6 = android.view.View.ALPHA;
            float[] a7 = new float[1];
            a7[0] = 0.0f;
            a.with((android.animation.Animator)android.animation.ObjectAnimator.ofFloat(a5, a6, a7));
        }
    }
    
    public boolean allowsTwoLineTitles() {
        return this.twoLineTitles;
    }
    
    public void animate(int i, boolean b, int i0, boolean b0) {
        this.animate(i, b, i0, b0, false);
    }
    
    public void animate(int i, boolean b, int i0, boolean b0, boolean b1) {
        if (i0 == -1) {
            i0 = this.animationDuration;
        }
        android.support.v7.widget.RecyclerView$ViewHolder a = this.recyclerView.findViewHolderForAdapterPosition(i);
        if (a == null) {
            if (sLogger.isLoggable(2)) {
                sLogger.v(new StringBuilder().append("animate:").append(i).append(" cannot find viewholder").toString());
            }
        } else {
            com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder a0 = (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder)a;
            if (b) {
                if (sLogger.isLoggable(2)) {
                    sLogger.v(new StringBuilder().append("animate:").append(i).append(" selected").toString());
                }
                a0.setState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.SELECTED, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.MOVE, i0);
                if (a0.hasToolTip() && this.containerCallback != null) {
                    this.containerCallback.showToolTips();
                }
            } else {
                if (sLogger.isLoggable(2)) {
                    sLogger.v(new StringBuilder().append("animate:").append(i).append(" un-selected").toString());
                }
                a0.setState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.UNSELECTED, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.MOVE, i0);
                if (a0.hasToolTip() && this.containerCallback != null) {
                    this.containerCallback.hideToolTips();
                }
            }
        }
    }
    
    public void animateIndicator(int i, int i0) {
        com.navdy.hud.app.ui.component.vlist.VerticalList$ContainerCallback a = this.containerCallback;
        label0: {
            label1: {
                if (a == null) {
                    break label1;
                }
                if (this.containerCallback.isFastScrolling()) {
                    break label0;
                }
            }
            if (this.indicatorAnimatorSet != null && this.indicatorAnimatorSet.isRunning()) {
                this.indicatorAnimatorSet.removeAllListeners();
                this.indicatorAnimatorSet.cancel();
                this.indicator.setCurrentItem(this.getCurrentPosition());
            }
            this.indicatorAnimatorSet = this.indicator.getItemMoveAnimator(i, -1);
            if (this.indicatorAnimatorSet != null) {
                this.indicatorAnimatorSet.setDuration((long)i0);
                this.indicatorAnimatorSet.addListener((android.animation.Animator$AnimatorListener)this.indicatorAnimationListener);
                this.indicatorAnimatorSet.start();
            }
        }
    }
    
    public void callItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
        if (this.callback != null) {
            this.callback.onItemSelected(a);
        }
    }
    
    public void cancelLoadingAnimation(int i) {
        if (this.firstEntryBlank) {
            i = i + 1;
        }
        com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder a = (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder)this.recyclerView.findViewHolderForAdapterPosition(i);
        if (a != null && a.getModelType() == com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.LOADING) {
            a.clearAnimation();
        }
    }
    
    public void clearAllAnimations() {
        int i = this.recyclerView.getChildCount();
        int i0 = 0;
        int i1 = 0;
        while(i1 < i) {
            try {
                com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder a = (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder)this.recyclerView.findViewHolderForAdapterPosition(i1);
                if (a != null) {
                    a.clearAnimation();
                    i0 = i0 + 1;
                }
            } catch(Throwable a0) {
                sLogger.e(a0);
            }
            i1 = i1 + 1;
        }
        sLogger.v(new StringBuilder().append("clearAllAnimations current:").append(i0).toString());
        android.support.v7.widget.RecyclerView$RecycledViewPool a1 = this.recyclerView.getRecycledViewPool();
        if (a1 != null) {
            this.copyList.clear();
            this.clearViewAnimation(a1, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.ICON_BKCOLOR.ordinal());
            this.clearViewAnimation(a1, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.TWO_ICONS.ordinal());
            this.clearViewAnimation(a1, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.ICON.ordinal());
            this.clearViewAnimation(a1, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.LOADING.ordinal());
            this.clearViewAnimation(a1, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.LOADING_CONTENT.ordinal());
            this.clearViewAnimation(a1, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.ICON_OPTIONS.ordinal());
            this.cleanupView(a1, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.SCROLL_CONTENT.ordinal());
            int i2 = this.copyList.size();
            if (i2 > 0) {
                Object a2 = this.copyList.iterator();
                while(((java.util.Iterator)a2).hasNext()) {
                    a1.putRecycledView((android.support.v7.widget.RecyclerView$ViewHolder)((java.util.Iterator)a2).next());
                }
                sLogger.v(new StringBuilder().append("clearAllAnimations pool:").append(i2).toString());
                this.copyList.clear();
            }
        }
    }
    
    public void copyAndPosition(android.widget.ImageView a, android.widget.TextView a0, android.widget.TextView a1, android.widget.TextView a2, boolean b) {
        com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder a3 = (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder)this.recyclerView.findViewHolderForAdapterPosition(this.currentMiddlePosition);
        if (a3 != null) {
            a3.copyAndPosition(a, a0, a1, a2, b);
        }
    }
    
    public com.navdy.hud.app.ui.component.vlist.VerticalList$KeyHandlerState down() {
        com.navdy.hud.app.ui.component.vlist.VerticalList$KeyHandlerState a = null;
        this.keyHandlerState.clear();
        label0: if (this.isLocked()) {
            sLogger.v("down locked");
            a = this.keyHandlerState;
        } else if (this.switchingScrollBoundary) {
            this.keyHandlerState.keyHandled = true;
            a = this.keyHandlerState;
        } else {
            int i = 0;
            boolean b = this.hasScrollableElement;
            label2: {
                label1: {
                    if (!b) {
                        break label1;
                    }
                    int i0 = this.currentMiddlePosition + 1;
                    int i1 = this.scrollItemIndex;
                    label3: {
                        if (i0 != i1) {
                            break label3;
                        }
                        int i2 = this.currentMiddlePosition;
                        this.currentMiddlePosition = this.currentMiddlePosition + 1;
                        com.navdy.hud.app.audio.SoundUtils.playSound(com.navdy.hud.app.audio.SoundUtils$Sound.MENU_MOVE);
                        this.switchingScrollBoundary = true;
                        this.isScrolling = true;
                        this.layoutManager.smoothScrollToPosition(this.currentMiddlePosition, -1);
                        this.animate(i2, this.currentMiddlePosition, true, true, false);
                        this.keyHandlerState.listMoved = true;
                        this.keyHandlerState.keyHandled = true;
                        a = this.keyHandlerState;
                        break label0;
                    }
                    if (!this.isScrollPosInScrollItem()) {
                        break label1;
                    }
                    i = this.canScrollDown();
                    if (i != -1) {
                        break label2;
                    }
                    if (this.isBottom()) {
                        break label1;
                    }
                    com.navdy.hud.app.audio.SoundUtils.playSound(com.navdy.hud.app.audio.SoundUtils$Sound.MENU_MOVE);
                    float f = (float)(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight * 2);
                    int i3 = this.currentMiddlePosition;
                    int i4 = this.currentMiddlePosition + 1;
                    this.switchingScrollBoundary = true;
                    this.isScrolling = true;
                    this.currentMiddlePosition = i4;
                    this.recyclerView.smoothScrollBy(0, (int)f);
                    this.animate(i3, i4, true, true, true);
                    this.keyHandlerState.listMoved = true;
                    this.keyHandlerState.keyHandled = true;
                    a = this.keyHandlerState;
                    break label0;
                }
                if (this.isBottom()) {
                    sLogger.v(new StringBuilder().append("cannot go down:").append(this.currentMiddlePosition).toString());
                    a = this.keyHandlerState;
                    break label0;
                } else {
                    this.keyHandlerState.keyHandled = true;
                    if (this.isOverrideKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent.RIGHT)) {
                        com.navdy.hud.app.audio.SoundUtils.playSound(com.navdy.hud.app.audio.SoundUtils$Sound.MENU_MOVE);
                        this.keyHandlerState.listMoved = false;
                        a = this.keyHandlerState;
                        break label0;
                    } else {
                        this.keyHandlerState.listMoved = true;
                        this.down(this.currentMiddlePosition, this.currentMiddlePosition + 1);
                        a = this.keyHandlerState;
                        break label0;
                    }
                }
            }
            com.navdy.hud.app.audio.SoundUtils.playSound(com.navdy.hud.app.audio.SoundUtils$Sound.MENU_MOVE);
            if (this.isScrolling) {
                this.currentScrollBy = this.currentScrollBy + 0.5f;
                if (this.currentScrollBy > 2f) {
                    this.currentScrollBy = 2f;
                }
            } else {
                this.currentScrollBy = 1f;
            }
            float f0 = (float)com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.scrollDistance * this.currentScrollBy;
            if (!((float)i > f0)) {
                f0 = (float)i;
            }
            this.isScrolling = true;
            this.recyclerView.smoothScrollBy(0, (int)f0);
            this.keyHandlerState.keyHandled = true;
            a = this.keyHandlerState;
        }
        return a;
    }
    
    public com.navdy.hud.app.ui.component.vlist.VerticalList$Model getCurrentModel() {
        return this.adapter.getModel(this.getRawPosition());
    }
    
    public int getCurrentPosition() {
        return this.currentMiddlePosition - 1;
    }
    
    public com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder getCurrentViewHolder() {
        return this.getViewHolder(this.currentMiddlePosition);
    }
    
    public com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState getItemSelectionState() {
        return this.itemSelectionState;
    }
    
    public int getRawPosition() {
        return this.currentMiddlePosition;
    }
    
    public com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder getViewHolder(int i) {
        com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder a = null;
        if (this.adapter.getModel(i) != null) {
            a = (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder)this.recyclerView.findViewHolderForAdapterPosition(this.currentMiddlePosition);
        } else {
            sLogger.i(new StringBuilder().append("getViewHolder: invalid pos:").append(i).toString());
            a = null;
        }
        return a;
    }
    
    public boolean hasScrollItem() {
        return this.hasScrollableElement;
    }
    
    public boolean isBottom() {
        return this.currentMiddlePosition == this.modelList.size() - 2;
    }
    
    public boolean isFirstEntryBlank() {
        return this.firstEntryBlank;
    }
    
    public boolean isTop() {
        return this.currentMiddlePosition == 1;
    }
    
    public void lock() {
        this.lockList = true;
    }
    
    public void performSelectAction(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
        this.callback.select(a);
    }
    
    public void refreshData(int i) {
        this.refreshData(i, (com.navdy.hud.app.ui.component.vlist.VerticalList$Model)null);
    }
    
    public void refreshData(int i, com.navdy.hud.app.ui.component.vlist.VerticalList$Model a) {
        this.refreshData(i, a, false);
    }
    
    public void refreshData(int i, com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, boolean b) {
        if (this.firstEntryBlank) {
            i = i + 1;
        }
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a0 = this.adapter.getModel(i);
        if (a0 != null) {
            if (a != null) {
                a.needsRebind = true;
                this.adapter.updateModel(i, a);
            } else {
                a0.needsRebind = true;
                a0.dontStartFluctuator = b;
            }
            this.adapter.notifyItemChanged(i);
        } else {
            sLogger.i(new StringBuilder().append("refreshData: invalid model pos:").append(i).toString());
        }
    }
    
    public void refreshData(int i, com.navdy.hud.app.ui.component.vlist.VerticalList$Model[] a) {
        if (this.firstEntryBlank) {
            i = i + 1;
        }
        int i0 = 0;
        int i1 = 0;
        while(i0 < a.length) {
            int i2 = i + i0;
            if (this.adapter.getModel(i2) != null) {
                i1 = i1 + 1;
                this.adapter.updateModel(i2, a[i0]);
            } else {
                sLogger.i(new StringBuilder().append("refreshData(s): invalid model pos:").append(i2).toString());
            }
            i0 = i0 + 1;
        }
        this.adapter.setInitialState(false);
        this.adapter.notifyItemRangeChanged(i, i1);
        sLogger.i(new StringBuilder().append("refreshData(s) pos=").append(i).append(" len=").append(i1).toString());
    }
    
    public void scrollToPosition(int i) {
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a = this.adapter.getModel(i);
        if (a != null) {
            if (this.isLocked()) {
                sLogger.v(new StringBuilder().append("scrollToPos list locked:").append(i).toString());
            } else if (this.isScrolling) {
                sLogger.v(new StringBuilder().append("scrollToPos wait for scroll idle:").append(i).toString());
                this.scrollPendingPos = i;
            } else {
                sLogger.v(new StringBuilder().append("scrollToPos:").append(i).append(" , ").append(a.title).toString());
                this.currentMiddlePosition = i;
                int i0 = this.currentMiddlePosition - 1;
                if (i0 < 0) {
                    i0 = 0;
                }
                this.actualScrollY = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight * i0;
                this.adapter.setInitialState(false);
                this.layoutManager.scrollToPositionWithOffset(i0, 0);
                sLogger.v(new StringBuilder().append("scrollToPos scroll to ").append(i0).append(" scrollPos=").append(this.actualScrollY).append(" middle=").append(this.getCurrentPosition()).append(" raw=").append(this.getRawPosition()).toString());
                this.scrollListener.onScrollStateChanged((android.support.v7.widget.RecyclerView)this.recyclerView, 0);
            }
        }
    }
    
    public void select() {
        this.select(false);
    }
    
    public void setBindCallbacks(boolean b) {
        this.bindCallbacks = b;
    }
    
    public void setScrollIdleEvent(boolean b) {
        this.sendScrollIdleEvent = b;
    }
    
    public void setViewHolderState(int i, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State a, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType a0, int i0) {
        com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder a1 = this.getViewHolder(i);
        if (a1 != null && a1.getState() != a) {
            sLogger.v(new StringBuilder().append("setViewHolderState:").append(i).append(":").append(a).toString());
            a1.setState(a, a0, i0);
        }
    }
    
    public void targetFound(int i) {
        boolean b = this.hasScrollableElement;
        label0: {
            label2: {
                if (!b) {
                    break label2;
                }
                if (this.isScrollPosInScrollItem()) {
                    break label0;
                }
            }
            int i0 = this.getPositionFromScrollY(this.actualScrollY + i);
            boolean b0 = sLogger.isLoggable(2);
            label1: {
                if (i != 0) {
                    break label1;
                }
                if (!this.waitForTarget) {
                    break label1;
                }
                if (i0 != this.targetPos) {
                    break label0;
                }
            }
            this.waitForTarget = false;
            this.targetFound = false;
            this.targetPos = -1;
            com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder a = (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder)this.recyclerView.findViewHolderForAdapterPosition(i0);
            if (a != null) {
                this.targetFound = true;
                this.currentMiddlePosition = i0;
                java.util.HashSet a0 = this.selectedList;
                this.selectedList = new java.util.HashSet();
                if (b0) {
                    sLogger.v(new StringBuilder().append("targetFound selected list size = ").append(a0.size()).toString());
                }
                if (a0.size() > 0) {
                    Object a1 = a0.iterator();
                    while(((java.util.Iterator)a1).hasNext()) {
                        int i1 = ((Integer)((java.util.Iterator)a1).next()).intValue();
                        com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder a2 = (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder)this.recyclerView.findViewHolderForAdapterPosition(i1);
                        if (a2 != null && a2.getState() == com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.SELECTED) {
                            a2.setState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.UNSELECTED, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.MOVE, 230);
                        }
                    }
                }
                if (a != null && a.getState() == com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.UNSELECTED && !this.isCloseMenuVisible()) {
                    a.setState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.SELECTED, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.MOVE, 230);
                }
            } else {
                this.waitForTarget = true;
                this.targetPos = i0;
            }
        }
    }
    
    public void unlock() {
        this.unlock(true);
    }
    
    public void unlock(boolean b) {
        this.lockList = false;
        com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder a = (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder)this.recyclerView.findViewHolderForAdapterPosition(this.getRawPosition());
        if (a != null) {
            if (b) {
                a.startFluctuator();
            } else {
                a.clearAnimation();
            }
        }
    }
    
    public com.navdy.hud.app.ui.component.vlist.VerticalList$KeyHandlerState up() {
        com.navdy.hud.app.ui.component.vlist.VerticalList$KeyHandlerState a = null;
        this.keyHandlerState.clear();
        label0: if (this.isLocked()) {
            sLogger.v("up locked");
            a = this.keyHandlerState;
        } else if (this.switchingScrollBoundary) {
            this.keyHandlerState.keyHandled = true;
            a = this.keyHandlerState;
        } else {
            int i = 0;
            boolean b = this.reCalcScrollPos;
            label3: {
                if (!b) {
                    break label3;
                }
                if (!this.isScrolling) {
                    break label3;
                }
                this.keyHandlerState.keyHandled = true;
                a = this.keyHandlerState;
                break label0;
            }
            boolean b0 = this.hasScrollableElement;
            label1: {
                label2: {
                    if (!b0) {
                        break label2;
                    }
                    if (!this.isScrollPosInScrollItem()) {
                        break label2;
                    }
                    i = this.canScrollUp();
                    if (i != -1) {
                        break label1;
                    }
                }
                if (this.isTop()) {
                    if (this.isOverrideKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent.LEFT)) {
                        com.navdy.hud.app.audio.SoundUtils.playSound(com.navdy.hud.app.audio.SoundUtils$Sound.MENU_MOVE);
                        this.keyHandlerState.keyHandled = true;
                        this.keyHandlerState.listMoved = false;
                        a = this.keyHandlerState;
                        break label0;
                    } else {
                        if (this.isScrollBy) {
                            this.scrollByUpReceived = this.scrollByUpReceived + 1;
                            this.keyHandlerState.keyHandled = true;
                        }
                        if (this.isScrolling) {
                            this.keyHandlerState.keyHandled = true;
                        }
                        sLogger.v(new StringBuilder().append("cannot go up:").append(this.currentMiddlePosition).append(" isScrolling:").append(this.isScrolling).toString());
                        a = this.keyHandlerState;
                        break label0;
                    }
                } else {
                    this.keyHandlerState.keyHandled = true;
                    if (this.isOverrideKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent.LEFT)) {
                        com.navdy.hud.app.audio.SoundUtils.playSound(com.navdy.hud.app.audio.SoundUtils$Sound.MENU_MOVE);
                        this.keyHandlerState.listMoved = false;
                        a = this.keyHandlerState;
                        break label0;
                    } else {
                        this.keyHandlerState.listMoved = true;
                        this.up(this.currentMiddlePosition, this.currentMiddlePosition - 1);
                        a = this.keyHandlerState;
                        break label0;
                    }
                }
            }
            if (this.isScrolling) {
                this.currentScrollBy = this.currentScrollBy + 0.5f;
                if (this.currentScrollBy > 2f) {
                    this.currentScrollBy = 2f;
                }
            } else {
                this.currentScrollBy = 1f;
            }
            float f = (float)com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.scrollDistance * this.currentScrollBy;
            if (!((float)i > f)) {
                f = (float)i;
            }
            this.isScrolling = true;
            this.recyclerView.smoothScrollBy(0, (int)(-f));
            com.navdy.hud.app.audio.SoundUtils.playSound(com.navdy.hud.app.audio.SoundUtils$Sound.MENU_MOVE);
            this.keyHandlerState.keyHandled = true;
            a = this.keyHandlerState;
        }
        return a;
    }
    
    public void updateView(java.util.List a, int i, boolean b) {
        this.updateView(a, i, b, false, -1);
    }
    
    public void updateViewWithScrollableContent(java.util.List a, int i, boolean b) {
        java.util.Iterator a0 = a.iterator();
        Object a1 = a;
        int i0 = 0;
        int i1 = -1;
        int i2 = 0;
        Object a2 = a0;
        while(((java.util.Iterator)a2).hasNext()) {
            if (((com.navdy.hud.app.ui.component.vlist.VerticalList$Model)((java.util.Iterator)a2).next()).type == com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.SCROLL_CONTENT) {
                i0 = i0 + 1;
                i1 = i2;
            }
            i2 = i2 + 1;
        }
        if (((java.util.List)a1).size() != 1 && i0 != 0 && i0 <= 1) {
            this.updateView((java.util.List)a1, i, b, true, i1);
            return;
        }
        throw new RuntimeException(new StringBuilder().append("invalid scroll item model size=").append(((java.util.List)a1).size()).append(" count=").append(i0).toString());
    }
}
