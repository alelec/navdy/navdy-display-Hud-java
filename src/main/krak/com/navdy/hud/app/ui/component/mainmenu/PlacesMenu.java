package com.navdy.hud.app.ui.component.mainmenu;
import com.navdy.hud.app.R;

public class PlacesMenu implements com.navdy.hud.app.ui.component.mainmenu.IMenu {
    final public static int CANCEL_POSITION = 1;
    private static java.util.List CONFIRMATION_CHOICES;
    final public static int NAVIGATE_POSITION = 0;
    final private static int SELECTION_ANIMATION_DELAY = 1000;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model back;
    final private static int backColor;
    final public static int bkColorUnselected;
    final private static int contactColor;
    final private static int favColor;
    final private static int gasColor;
    final private static int homeColor;
    final public static String places;
    final private static int placesColor;
    final private static int recentColor;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model recentPlaces;
    final private static android.content.res.Resources resources;
    final private static com.navdy.service.library.log.Logger sLogger;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model search;
    final public static String suggested;
    final public static int suggestedColor;
    final private static int workColor;
    private int backSelection;
    private int backSelectionId;
    private com.squareup.otto.Bus bus;
    private java.util.List cachedList;
    private java.util.HashSet latlngSet;
    private com.navdy.hud.app.ui.component.mainmenu.NearbyPlacesMenu nearbyPlacesMenu;
    private com.navdy.hud.app.ui.component.mainmenu.IMenu parent;
    private com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter presenter;
    private com.navdy.hud.app.ui.component.mainmenu.RecentPlacesMenu recentPlacesMenu;
    private java.util.List returnToCacheList;
    private com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.mainmenu.PlacesMenu.class);
        CONFIRMATION_CHOICES = (java.util.List)new java.util.ArrayList();
        resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        bkColorUnselected = resources.getColor(R.color.icon_bk_color_unselected);
        recentColor = resources.getColor(R.color.mm_recent_places);
        favColor = resources.getColor(R.color.mm_place_favorite);
        homeColor = resources.getColor(R.color.mm_place_home);
        workColor = resources.getColor(R.color.mm_place_work);
        contactColor = resources.getColor(R.color.mm_place_contact);
        backColor = resources.getColor(R.color.mm_back);
        suggestedColor = resources.getColor(R.color.mm_place_suggested);
        placesColor = resources.getColor(R.color.mm_places);
        gasColor = resources.getColor(R.color.mm_place_gas);
        suggested = resources.getString(R.string.suggested);
        places = resources.getString(R.string.carousel_menu_map_places);
        String s = resources.getString(R.string.back);
        int i = backColor;
        back = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, i, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, i, s, (String)null);
        String s0 = resources.getString(R.string.carousel_menu_recent_place);
        int i0 = resources.getColor(R.color.mm_recent_places);
        recentPlaces = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.places_menu_recent, R.drawable.icon_place_recent_2, i0, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, i0, s0, (String)null);
        String s1 = resources.getString(R.string.carousel_menu_search_title);
        int i1 = resources.getColor(R.color.mm_search);
        search = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.main_menu_search, R.drawable.icon_mm_search_2, i1, bkColorUnselected, i1, s1, (String)null);
        CONFIRMATION_CHOICES.add(resources.getString(R.string.destination_start_navigation_yes));
        CONFIRMATION_CHOICES.add(resources.getString(R.string.destination_start_navigation_cancel));
    }
    
    public PlacesMenu(com.squareup.otto.Bus a, com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a0, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter a1, com.navdy.hud.app.ui.component.mainmenu.IMenu a2) {
        this.latlngSet = new java.util.HashSet();
        this.bus = a;
        this.vscrollComponent = a0;
        this.presenter = a1;
        this.parent = a2;
    }
    
    static com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter access$000(com.navdy.hud.app.ui.component.mainmenu.PlacesMenu a) {
        return a.presenter;
    }
    
    static com.navdy.service.library.log.Logger access$100() {
        return sLogger;
    }
    
    static com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent access$200(com.navdy.hud.app.ui.component.mainmenu.PlacesMenu a) {
        return a.vscrollComponent;
    }
    
    public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model buildModel(com.navdy.hud.app.framework.destinations.Destination a, int i) {
        return com.navdy.hud.app.ui.component.mainmenu.PlacesMenu.getPlaceModel(a, i, a.destinationTitle, (a.recentTimeLabel == null) ? a.destinationSubtitle : a.recentTimeLabel);
    }
    
    public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model getPlaceModel(com.navdy.hud.app.framework.destinations.Destination a, int i, String s, String s0) {
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a0 = null;
        if (a != null) {
            switch(com.navdy.hud.app.ui.component.mainmenu.PlacesMenu$4.$SwitchMap$com$navdy$hud$app$framework$destinations$Destination$FavoriteDestinationType[a.favoriteDestinationType.ordinal()]) {
                case 3: {
                    a0 = com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder.buildModel(i, R.drawable.icon_user_bg_1, 0, contactColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, s, s0);
                    a0.extras = new java.util.HashMap();
                    a0.extras.put("INITIAL", a.initials);
                    break;
                }
                case 2: {
                    a0 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(i, R.drawable.icon_place_work_2, workColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, workColor, s, s0);
                    break;
                }
                case 1: {
                    a0 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(i, R.drawable.icon_place_home_2, homeColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, homeColor, s, s0);
                    break;
                }
                default: {
                    if (a.placeCategory != com.navdy.hud.app.framework.destinations.Destination$PlaceCategory.SUGGESTED_RECENT) {
                        switch(com.navdy.hud.app.ui.component.mainmenu.PlacesMenu$4.$SwitchMap$com$navdy$hud$app$framework$destinations$Destination$FavoriteDestinationType[a.favoriteDestinationType.ordinal()]) {
                            case 5: {
                                a0 = com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder.buildModel(i, R.drawable.icon_place_calendar, R.drawable.icon_place_calendar_sm, homeColor, -1, s, s0);
                                break;
                            }
                            case 4: {
                                a0 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(i, R.drawable.icon_place_favorite_2, favColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, favColor, s, s0);
                                break;
                            }
                            default: {
                                if (a.destinationType != com.navdy.hud.app.framework.destinations.Destination$DestinationType.FIND_GAS) {
                                    boolean b = a.recommendation;
                                    label0: {
                                        label1: {
                                            if (b) {
                                                break label1;
                                            }
                                            if (a.placeCategory == com.navdy.hud.app.framework.destinations.Destination$PlaceCategory.RECENT) {
                                                break label0;
                                            }
                                        }
                                        a0 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(i, R.drawable.icon_place_favorite_2, favColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, favColor, s, s0);
                                        break;
                                    }
                                    a0 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(i, R.drawable.icon_place_recent_2, recentColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, recentColor, s, s0);
                                } else {
                                    a0 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(i, R.drawable.icon_places_gas_2, gasColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, gasColor, s, s0);
                                }
                            }
                        }
                    } else {
                        a0 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(i, R.drawable.icon_place_recent_2, recentColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, recentColor, s, s0);
                    }
                }
            }
        } else {
            a0 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(i, R.drawable.icon_mm_places_2, placesColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, placesColor, s, s0);
        }
        return a0;
    }
    
    public com.navdy.hud.app.ui.component.mainmenu.IMenu getChildMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu a, String s, String s0) {
        return null;
    }
    
    public int getInitialSelection() {
        int i = 0;
        if (this.cachedList != null) {
            i = this.cachedList.indexOf(recentPlaces);
            if (i == -1) {
                i = (this.cachedList.size() < 3) ? 1 : 2;
            }
        } else {
            i = 0;
        }
        return i;
    }
    
    public java.util.List getItems() {
        Object a = null;
        if (this.cachedList == null) {
            int i = 0;
            java.util.ArrayList a0 = new java.util.ArrayList();
            this.returnToCacheList = (java.util.List)new java.util.ArrayList();
            ((java.util.List)a0).add(back);
            com.navdy.service.library.events.DeviceInfo a1 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getRemoteDeviceInfo();
            boolean b = !com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile();
            if (b && a1 == null) {
                b = false;
            }
            sLogger.v(new StringBuilder().append("isConnected:").append(b).toString());
            if (b && com.navdy.hud.app.util.RemoteCapabilitiesUtil.supportsPlaceSearch()) {
                ((java.util.List)a0).add(search);
            }
            ((java.util.List)a0).add(recentPlaces);
            int i0 = ((java.util.List)a0).size();
            com.navdy.hud.app.framework.destinations.DestinationsManager a2 = com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance();
            this.latlngSet.clear();
            java.util.List a3 = a2.getSuggestedDestinations();
            label5: {
                label3: {
                    label4: {
                        if (a3 == null) {
                            break label4;
                        }
                        if (a3.size() > 0) {
                            break label3;
                        }
                    }
                    sLogger.v("no suggested destinations");
                    i = 0;
                    break label5;
                }
                sLogger.v(new StringBuilder().append("suggested destinations:").append(a3.size()).toString());
                java.util.Iterator a4 = a3.iterator();
                i = 0;
                Object a5 = a4;
                while(((java.util.Iterator)a5).hasNext()) {
                    com.navdy.hud.app.framework.destinations.Destination a6 = (com.navdy.hud.app.framework.destinations.Destination)((java.util.Iterator)a5).next();
                    com.navdy.hud.app.ui.component.vlist.VerticalList$Model a7 = com.navdy.hud.app.ui.component.mainmenu.PlacesMenu.buildModel(a6, i);
                    a7.subTitle = suggested;
                    a7.state = a6;
                    a7.extras = new java.util.HashMap();
                    a7.extras.put("SUBTITLE_COLOR", String.valueOf(suggestedColor));
                    ((java.util.List)a0).add(a7);
                    this.returnToCacheList.add(a7);
                    String s = new StringBuilder().append(a6.displayPositionLatitude).append(",").append(a6.displayPositionLongitude).append(",").append(a6.navigationPositionLatitude).append(",").append(a6.navigationPositionLongitude).toString();
                    this.latlngSet.add(s);
                    i = i + 1;
                }
            }
            java.util.List a8 = a2.getFavoriteDestinations();
            label2: {
                label0: {
                    label1: {
                        if (a8 == null) {
                            break label1;
                        }
                        if (a8.size() > 0) {
                            break label0;
                        }
                    }
                    sLogger.v("no favorite destinations");
                    break label2;
                }
                sLogger.v(new StringBuilder().append("favorite destinations:").append(a8.size()).toString());
                Object a9 = a8.iterator();
                while(((java.util.Iterator)a9).hasNext()) {
                    com.navdy.hud.app.framework.destinations.Destination a10 = (com.navdy.hud.app.framework.destinations.Destination)((java.util.Iterator)a9).next();
                    String s0 = new StringBuilder().append(a10.displayPositionLatitude).append(",").append(a10.displayPositionLongitude).append(",").append(a10.navigationPositionLatitude).append(",").append(a10.navigationPositionLongitude).toString();
                    if (this.latlngSet.contains(s0)) {
                        sLogger.v(new StringBuilder().append("latlng already seen in suggested:").append(a10.destinationTitle).toString());
                    } else {
                        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a11 = com.navdy.hud.app.ui.component.mainmenu.PlacesMenu.buildModel(a10, i);
                        a11.state = a10;
                        ((java.util.List)a0).add(a11);
                        this.returnToCacheList.add(a11);
                        i = i + 1;
                    }
                }
            }
            com.navdy.hud.app.framework.destinations.DestinationsManager$GasDestination a12 = com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance().getGasDestination();
            if (a12 != null) {
                com.navdy.hud.app.ui.component.vlist.VerticalList$Model a13 = com.navdy.hud.app.ui.component.mainmenu.PlacesMenu.buildModel(a12.destination, i);
                a13.subTitle = suggested;
                a13.state = a12.destination;
                a13.extras = new java.util.HashMap();
                a13.extras.put("SUBTITLE_COLOR", String.valueOf(suggestedColor));
                if (a12.showFirst) {
                    ((java.util.List)a0).add(i0, a13);
                } else {
                    ((java.util.List)a0).add(a13);
                }
                this.returnToCacheList.add(a13);
            }
            this.cachedList = (java.util.List)a0;
            a = a0;
        } else {
            a = this.cachedList;
        }
        return (java.util.List)a;
    }
    
    public com.navdy.hud.app.ui.component.vlist.VerticalList$Model getModelfromPos(int i) {
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a = null;
        java.util.List a0 = this.cachedList;
        label2: {
            label0: {
                label1: {
                    if (a0 == null) {
                        break label1;
                    }
                    if (this.cachedList.size() > i) {
                        break label0;
                    }
                }
                a = null;
                break label2;
            }
            a = (com.navdy.hud.app.ui.component.vlist.VerticalList$Model)this.cachedList.get(i);
        }
        return a;
    }
    
    public com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex getScrollIndex() {
        return null;
    }
    
    public com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu getType() {
        return com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.PLACES;
    }
    
    public boolean isBindCallsEnabled() {
        return false;
    }
    
    public boolean isFirstItemEmpty() {
        return true;
    }
    
    public boolean isItemClickable(int i, int i0) {
        return true;
    }
    
    void launchDestination(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a) {
        label2: if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
            com.navdy.hud.app.ui.component.vlist.VerticalList$Model a0 = new com.navdy.hud.app.ui.component.vlist.VerticalList$Model(a);
            com.navdy.hud.app.maps.here.HereNavigationManager a1 = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
            boolean b = a1.isNavigationModeOn();
            label0: {
                label1: {
                    if (!b) {
                        break label1;
                    }
                    if (!a1.hasArrived()) {
                        break label0;
                    }
                }
                sLogger.v("called requestNavigation");
                this.presenter.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.mainmenu.PlacesMenu$2(this, a0), 1000);
                break label2;
            }
            com.navdy.hud.app.framework.destinations.Destination a2 = (com.navdy.hud.app.framework.destinations.Destination)a0.state;
            com.navdy.hud.app.ui.component.ConfirmationLayout a3 = this.presenter.getConfirmationLayout();
            if (a3 != null) {
                com.navdy.hud.app.ui.component.destination.DestinationConfirmationHelper.configure(a3, a0, a2);
                a3.setChoices(CONFIRMATION_CHOICES, 0, (com.navdy.hud.app.ui.component.ChoiceLayout$IListener)new com.navdy.hud.app.ui.component.mainmenu.PlacesMenu$3(this, a2));
                a3.setVisibility(0);
            } else {
                sLogger.v("confirmation layout not found");
            }
        } else {
            sLogger.w("Here maps engine not initialized, exit");
            this.presenter.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.mainmenu.PlacesMenu$1(this));
        }
    }
    
    public void onBindToView(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, android.view.View a0, int i, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState a1) {
    }
    
    public void onFastScrollEnd() {
    }
    
    public void onFastScrollStart() {
    }
    
    public void onItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
    }
    
    public void onScrollIdle() {
    }
    
    public void onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel a) {
        if (com.navdy.hud.app.ui.component.mainmenu.PlacesMenu$4.$SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$MenuLevel[a.ordinal()] != 0) {
            if (this.returnToCacheList != null) {
                sLogger.v("pm:unload add to cache");
                com.navdy.hud.app.ui.component.vlist.VerticalModelCache.addToCache(this.returnToCacheList);
                this.returnToCacheList = null;
            }
            if (this.recentPlacesMenu != null) {
                this.recentPlacesMenu.onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.CLOSE);
            }
        }
    }
    
    public boolean selectItem(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
        sLogger.v(new StringBuilder().append("select id:").append(a.id).append(" pos:").append(a.pos).toString());
        switch(a.id) {
            case R.id.places_menu_recent: {
                sLogger.v("recent places");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("recent_places");
                if (this.recentPlacesMenu == null) {
                    this.recentPlacesMenu = new com.navdy.hud.app.ui.component.mainmenu.RecentPlacesMenu(this.bus, this.vscrollComponent, this.presenter, (com.navdy.hud.app.ui.component.mainmenu.IMenu)this);
                }
                this.presenter.loadMenu((com.navdy.hud.app.ui.component.mainmenu.IMenu)this.recentPlacesMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.SUB_LEVEL, a.pos, 0);
                break;
            }
            case R.id.menu_back: {
                sLogger.v("back");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("back");
                this.presenter.loadMenu(this.parent, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.BACK_TO_PARENT, this.backSelection, this.backSelectionId);
                this.backSelectionId = 0;
                break;
            }
            case R.id.main_menu_search: {
                sLogger.v("search");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("search");
                if (this.nearbyPlacesMenu == null) {
                    this.nearbyPlacesMenu = new com.navdy.hud.app.ui.component.mainmenu.NearbyPlacesMenu(this.bus, this.vscrollComponent, this.presenter, (com.navdy.hud.app.ui.component.mainmenu.IMenu)this);
                }
                this.presenter.loadMenu((com.navdy.hud.app.ui.component.mainmenu.IMenu)this.nearbyPlacesMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.SUB_LEVEL, a.pos, 0);
                break;
            }
            default: {
                this.launchDestination((com.navdy.hud.app.ui.component.vlist.VerticalList$Model)this.cachedList.get(a.pos));
            }
        }
        return true;
    }
    
    public void setBackSelectionId(int i) {
        this.backSelectionId = i;
    }
    
    public void setBackSelectionPos(int i) {
        this.backSelection = i;
    }
    
    public void setSelectedIcon() {
        this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_mm_places_2, placesColor, (android.graphics.Shader)null, 1f);
        this.vscrollComponent.selectedText.setText((CharSequence)places);
    }
    
    public void showToolTip() {
    }
}
