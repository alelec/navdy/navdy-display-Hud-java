package com.navdy.hud.app.ui.activity;

final public class MainActivity$$InjectAdapter extends dagger.internal.Binding implements javax.inject.Provider, dagger.MembersInjector {
    private dagger.internal.Binding mainPresenter;
    private dagger.internal.Binding pairingManager;
    private dagger.internal.Binding powerManager;
    private dagger.internal.Binding supertype;
    
    public MainActivity$$InjectAdapter() {
        super("com.navdy.hud.app.ui.activity.MainActivity", "members/com.navdy.hud.app.ui.activity.MainActivity", false, com.navdy.hud.app.ui.activity.MainActivity.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.mainPresenter = a.requestBinding("com.navdy.hud.app.ui.activity.Main$Presenter", com.navdy.hud.app.ui.activity.MainActivity.class, (this).getClass().getClassLoader());
        this.pairingManager = a.requestBinding("com.navdy.hud.app.manager.PairingManager", com.navdy.hud.app.ui.activity.MainActivity.class, (this).getClass().getClassLoader());
        this.powerManager = a.requestBinding("com.navdy.hud.app.device.PowerManager", com.navdy.hud.app.ui.activity.MainActivity.class, (this).getClass().getClassLoader());
        this.supertype = a.requestBinding("members/com.navdy.hud.app.ui.activity.HudBaseActivity", com.navdy.hud.app.ui.activity.MainActivity.class, (this).getClass().getClassLoader(), false, true);
    }
    
    public com.navdy.hud.app.ui.activity.MainActivity get() {
        com.navdy.hud.app.ui.activity.MainActivity a = new com.navdy.hud.app.ui.activity.MainActivity();
        this.injectMembers(a);
        return a;
    }
    
    public Object get() {
        return this.get();
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.mainPresenter);
        a0.add(this.pairingManager);
        a0.add(this.powerManager);
        a0.add(this.supertype);
    }
    
    public void injectMembers(com.navdy.hud.app.ui.activity.MainActivity a) {
        a.mainPresenter = (com.navdy.hud.app.ui.activity.Main$Presenter)this.mainPresenter.get();
        a.pairingManager = (com.navdy.hud.app.manager.PairingManager)this.pairingManager.get();
        a.powerManager = (com.navdy.hud.app.device.PowerManager)this.powerManager.get();
        this.supertype.injectMembers(a);
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.ui.activity.MainActivity)a);
    }
}
