package com.navdy.hud.app.ui.component;

final public class SwitchHaloView extends com.navdy.hud.app.ui.component.HaloView {
    private java.util.HashMap _$_findViewCache;
    private float maxScalingFactorX;
    private float maxScalingFactorY;
    
    public SwitchHaloView(android.content.Context a) {
        super(a);
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "context");
    }
    
    public SwitchHaloView(android.content.Context a, android.util.AttributeSet a0) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "context");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a0, "attrs");
        super(a, a0);
    }
    
    public SwitchHaloView(android.content.Context a, android.util.AttributeSet a0, int i) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "context");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a0, "attrs");
        super(a, a0, i);
    }
    
    public void _$_clearFindViewByIdCache() {
        if (this._$_findViewCache != null) {
            this._$_findViewCache.clear();
        }
    }
    
    public android.view.View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new java.util.HashMap();
        }
        android.view.View a = (android.view.View)this._$_findViewCache.get(Integer.valueOf(i));
        if (a == null) {
            a = this.findViewById(i);
            this._$_findViewCache.put(Integer.valueOf(i), a);
        }
        return a;
    }
    
    final public float getMaxScalingFactorX() {
        return this.maxScalingFactorX;
    }
    
    final public float getMaxScalingFactorY() {
        return this.maxScalingFactorY;
    }
    
    public void onAnimationUpdateInternal(android.animation.ValueAnimator a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "animation");
        super.onAnimationUpdateInternal(a);
        Object a0 = a.getAnimatedValue();
        if (a0 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.Float");
        }
        float f = (((Float)a0).floatValue() - this.startRadius) / (this.endRadius - this.startRadius) * (float)2;
        this.maxScalingFactorX = (this.endRadius - this.startRadius) / (float)this.getWidth();
        this.maxScalingFactorY = (this.endRadius - this.startRadius) / (float)this.getHeight();
        this.setScaleX((float)1 + this.maxScalingFactorX * f);
        this.setScaleY((float)1 + this.maxScalingFactorY * f);
        this.setPivotX((float)(this.getWidth() / 2));
        this.setPivotY((float)(this.getHeight() / 2));
        this.invalidate();
    }
    
    public void onDrawInternal(android.graphics.Canvas a) {
    }
    
    final public void setMaxScalingFactorX(float f) {
        this.maxScalingFactorX = f;
    }
    
    final public void setMaxScalingFactorY(float f) {
        this.maxScalingFactorY = f;
    }
    
    public void setStrokeColor(int i) {
        super.setStrokeColor(i);
        android.graphics.drawable.Drawable a = this.getBackground();
        if (a == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.graphics.drawable.GradientDrawable");
        }
        ((android.graphics.drawable.GradientDrawable)a).setColor(i);
    }
}
