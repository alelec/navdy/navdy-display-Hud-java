package com.navdy.hud.app.ui.component.destination;

public class DestinationPickerView extends android.widget.RelativeLayout implements com.navdy.hud.app.manager.InputManager$IInputHandler {
    final private static com.navdy.service.library.log.Logger sLogger;
    private com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent$Callback callback;
    @InjectView(R.id.confirmationLayout)
    com.navdy.hud.app.ui.component.ConfirmationLayout confirmationLayout;
    @Inject
    public com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter presenter;
    @InjectView(R.id.rightBackground)
    android.view.View rightBackground;
    com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vmenuComponent;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.destination.DestinationPickerView.class);
    }
    
    public DestinationPickerView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public DestinationPickerView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public DestinationPickerView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.callback = (com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent$Callback)new com.navdy.hud.app.ui.component.destination.DestinationPickerView$1(this);
        if (this.isInEditMode()) {
            com.navdy.hud.app.HudApplication.setContext(a);
        } else {
            mortar.Mortar.inject(a, this);
        }
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        return com.navdy.hud.app.manager.InputManager.nextContainingHandler((android.view.View)this);
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.vmenuComponent.clear();
        if (this.presenter != null) {
            this.presenter.dropView((android.view.View)this);
        }
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View)this);
        this.vmenuComponent = new com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent((android.view.ViewGroup)this, this.callback, false);
        this.vmenuComponent.leftContainer.setAlpha(0.0f);
        this.vmenuComponent.rightContainer.setAlpha(0.0f);
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        return this.confirmationLayout.getVisibility() != 0 && this.vmenuComponent.handleGesture(a);
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        return (this.confirmationLayout.getVisibility() != 0) ? this.vmenuComponent.handleKey(a) : this.confirmationLayout.handleKey(a);
    }
}
