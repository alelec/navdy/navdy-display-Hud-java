package com.navdy.hud.app.ui.component.vmenu;

public class VerticalAnimationUtils$ActionRunnable implements Runnable {
    private android.os.Bundle args;
    private com.squareup.otto.Bus bus;
    private boolean ignoreAnimation;
    private com.navdy.service.library.events.ui.Screen screen;
    
    public VerticalAnimationUtils$ActionRunnable(com.squareup.otto.Bus a, com.navdy.service.library.events.ui.Screen a0) {
        this(a, a0, (android.os.Bundle)null, false);
    }
    
    public VerticalAnimationUtils$ActionRunnable(com.squareup.otto.Bus a, com.navdy.service.library.events.ui.Screen a0, android.os.Bundle a1, boolean b) {
        this.bus = a;
        this.screen = a0;
        this.args = a1;
        this.ignoreAnimation = b;
    }
    
    public void run() {
        if (this.args != null) {
            this.bus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(this.screen, this.args, this.ignoreAnimation));
        } else {
            this.bus.post(new com.navdy.service.library.events.ui.ShowScreen$Builder().screen(this.screen).build());
        }
    }
}
