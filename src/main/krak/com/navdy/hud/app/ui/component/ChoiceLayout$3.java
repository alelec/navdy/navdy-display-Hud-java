package com.navdy.hud.app.ui.component;

class ChoiceLayout$3 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
    final com.navdy.hud.app.ui.component.ChoiceLayout this$0;
    
    ChoiceLayout$3(com.navdy.hud.app.ui.component.ChoiceLayout a) {
        super();
        this.this$0 = a;
    }
    
    public void onAnimationEnd(android.animation.Animator a) {
        if (com.navdy.hud.app.ui.component.ChoiceLayout.access$100(this.this$0) != null) {
            if (com.navdy.hud.app.ui.component.ChoiceLayout.access$100(this.this$0).getTag() == null) {
                if (!this.this$0.isHighlightPersistent()) {
                    this.this$0.highlightView.setVisibility(4);
                }
                this.this$0.highlightView.setAlpha(1f);
                ((android.view.ViewGroup$MarginLayoutParams)this.this$0.highlightView.getLayoutParams()).topMargin = com.navdy.hud.app.ui.component.ChoiceLayout.access$300();
                com.navdy.hud.app.ui.component.ChoiceLayout.access$702(this.this$0, false);
                com.navdy.hud.app.ui.component.ChoiceLayout.access$602(this.this$0, (android.animation.AnimatorSet)null);
                this.this$0.clearOperationQueue();
                if (com.navdy.hud.app.ui.component.ChoiceLayout.access$800(this.this$0) != -1) {
                    com.navdy.hud.app.ui.component.ChoiceLayout.access$900(this.this$0, com.navdy.hud.app.ui.component.ChoiceLayout.access$800(this.this$0));
                }
                if (com.navdy.hud.app.ui.component.ChoiceLayout.access$100(this.this$0) instanceof android.widget.TextView) {
                    ((android.widget.TextView)com.navdy.hud.app.ui.component.ChoiceLayout.access$1100(this.this$0)).setTextColor(com.navdy.hud.app.ui.component.ChoiceLayout.access$1000());
                } else if (com.navdy.hud.app.ui.component.ChoiceLayout.access$100(this.this$0) instanceof android.widget.ImageView) {
                    ((android.widget.ImageView)com.navdy.hud.app.ui.component.ChoiceLayout.access$1100(this.this$0)).setColorFilter(com.navdy.hud.app.ui.component.ChoiceLayout.access$1000());
                }
            } else {
                com.navdy.hud.app.ui.component.ChoiceLayout.access$100(this.this$0).setTag(null);
                if (com.navdy.hud.app.ui.component.ChoiceLayout.access$100(this.this$0) instanceof android.widget.TextView) {
                    ((android.widget.TextView)com.navdy.hud.app.ui.component.ChoiceLayout.access$100(this.this$0)).setTextColor(com.navdy.hud.app.ui.component.ChoiceLayout.access$200());
                } else if (com.navdy.hud.app.ui.component.ChoiceLayout.access$100(this.this$0) instanceof android.widget.ImageView) {
                    ((android.widget.ImageView)com.navdy.hud.app.ui.component.ChoiceLayout.access$100(this.this$0)).setColorFilter(com.navdy.hud.app.ui.component.ChoiceLayout.access$200());
                }
                int[] a0 = new int[2];
                a0[0] = 0;
                a0[1] = com.navdy.hud.app.ui.component.ChoiceLayout.access$300();
                android.animation.ValueAnimator a1 = android.animation.ValueAnimator.ofInt(a0);
                a1.addUpdateListener(com.navdy.hud.app.ui.component.ChoiceLayout.access$400(this.this$0));
                a1.addListener((android.animation.Animator$AnimatorListener)com.navdy.hud.app.ui.component.ChoiceLayout.access$500(this.this$0));
                android.view.View a2 = this.this$0.highlightView;
                float[] a3 = new float[2];
                a3[0] = 1f;
                a3[1] = 0.5f;
                android.animation.ObjectAnimator a4 = android.animation.ObjectAnimator.ofFloat(a2, "alpha", a3);
                com.navdy.hud.app.ui.component.ChoiceLayout.access$602(this.this$0, new android.animation.AnimatorSet());
                com.navdy.hud.app.ui.component.ChoiceLayout.access$600(this.this$0).setDuration(200L);
                android.animation.AnimatorSet a5 = com.navdy.hud.app.ui.component.ChoiceLayout.access$600(this.this$0);
                android.animation.Animator[] a6 = new android.animation.Animator[2];
                a6[0] = a1;
                a6[1] = a4;
                a5.playTogether(a6);
                com.navdy.hud.app.ui.component.ChoiceLayout.access$600(this.this$0).start();
            }
        }
    }
}
