package com.navdy.hud.app.ui.component.vlist.viewholder;

class IconBaseViewHolder$3 implements Runnable {
    final com.navdy.hud.app.ui.component.vlist.viewholder.IconBaseViewHolder this$0;
    final com.navdy.hud.app.ui.component.vlist.VerticalList$Model val$model;
    final int val$pos;
    
    IconBaseViewHolder$3(com.navdy.hud.app.ui.component.vlist.viewholder.IconBaseViewHolder a, com.navdy.hud.app.ui.component.vlist.VerticalList$Model a0, int i) {
        super();
        this.this$0 = a;
        this.val$model = a0;
        this.val$pos = i;
    }
    
    public void run() {
        com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a = this.this$0.vlist.getItemSelectionState();
        a.set(this.val$model, this.val$model.id, this.val$pos, -1, -1);
        this.this$0.vlist.performSelectAction(a);
    }
}
