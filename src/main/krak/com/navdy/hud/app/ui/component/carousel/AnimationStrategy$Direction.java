package com.navdy.hud.app.ui.component.carousel;


public enum AnimationStrategy$Direction {
    LEFT(0),
    RIGHT(1);

    private int value;
    AnimationStrategy$Direction(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class AnimationStrategy$Direction extends Enum {
//    final private static com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction[] $VALUES;
//    final public static com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction LEFT;
//    final public static com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction RIGHT;
//    
//    static {
//        LEFT = new com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction("LEFT", 0);
//        RIGHT = new com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction("RIGHT", 1);
//        com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction[] a = new com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction[2];
//        a[0] = LEFT;
//        a[1] = RIGHT;
//        $VALUES = a;
//    }
//    
//    private AnimationStrategy$Direction(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction valueOf(String s) {
//        return (com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction)Enum.valueOf(com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction.class, s);
//    }
//    
//    public static com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction[] values() {
//        return $VALUES.clone();
//    }
//}
//