package com.navdy.hud.app.ui.component.homescreen;

public class OpenRoadView extends android.widget.RelativeLayout {
    private com.squareup.otto.Bus bus;
    private com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView;
    private com.navdy.service.library.log.Logger logger;
    @InjectView(R.id.openMapRoadInfo)
    android.widget.TextView openMapRoadInfo;
    
    public OpenRoadView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public OpenRoadView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public OpenRoadView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
    }
    
    private void setRoad(String s) {
        if (s == null) {
            s = "";
        }
        this.openMapRoadInfo.setText((CharSequence)s);
    }
    
    public void getCustomAnimator(com.navdy.hud.app.view.MainView$CustomAnimationMode a, android.animation.AnimatorSet$Builder a0) {
        switch(com.navdy.hud.app.ui.component.homescreen.OpenRoadView$1.$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode[a.ordinal()]) {
            case 2: {
                a0.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getMarginAnimator((android.view.View)this.openMapRoadInfo, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.openMapRoadInfoShrinkLeft_L_Margin, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.openMapRoadInfoShrinkLeft_R_Margin));
                break;
            }
            case 1: {
                a0.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getMarginAnimator((android.view.View)this.openMapRoadInfo, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.openMapRoadInfoMargin, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.openMapRoadInfoMargin));
                break;
            }
        }
    }
    
    public void init(com.navdy.hud.app.ui.component.homescreen.HomeScreenView a) {
        this.homeScreenView = a;
        this.bus.register(this);
    }
    
    protected void onFinishInflate() {
        this.logger = com.navdy.hud.app.ui.component.homescreen.HomeScreenView.sLogger;
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View)this);
        this.bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
    }
    
    public void onManeuverDisplay(com.navdy.hud.app.maps.MapEvents$ManeuverDisplay a) {
        this.setRoad(a.currentRoad);
    }
    
    public void setRoad() {
        this.setRoad(com.navdy.hud.app.maps.here.HereMapUtil.getCurrentRoadName());
    }
    
    public void setView(com.navdy.hud.app.view.MainView$CustomAnimationMode a) {
        android.view.ViewGroup$MarginLayoutParams a0 = (android.view.ViewGroup$MarginLayoutParams)this.openMapRoadInfo.getLayoutParams();
        switch(com.navdy.hud.app.ui.component.homescreen.OpenRoadView$1.$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode[a.ordinal()]) {
            case 2: {
                a0.leftMargin = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.openMapRoadInfoShrinkLeft_L_Margin;
                a0.rightMargin = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.openMapRoadInfoShrinkLeft_R_Margin;
                break;
            }
            case 1: {
                a0.leftMargin = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.openMapRoadInfoMargin;
                a0.rightMargin = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.openMapRoadInfoMargin;
                break;
            }
        }
    }
}
