package com.navdy.hud.app.ui.component.mainmenu;

class MainOptionsMenu$7 {
    final static int[] $SwitchMap$com$navdy$hud$app$ui$component$mainmenu$MainOptionsMenu$Mode;
    
    static {
        $SwitchMap$com$navdy$hud$app$ui$component$mainmenu$MainOptionsMenu$Mode = new int[com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu$Mode.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$ui$component$mainmenu$MainOptionsMenu$Mode;
        com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu$Mode a0 = com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu$Mode.MAP;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$mainmenu$MainOptionsMenu$Mode[com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu$Mode.DASH.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
    }
}
