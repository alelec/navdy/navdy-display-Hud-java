package com.navdy.hud.app.ui.component.vlist;

class VerticalList$2 implements Runnable {
    final com.navdy.hud.app.ui.component.vlist.VerticalList this$0;
    
    VerticalList$2(com.navdy.hud.app.ui.component.vlist.VerticalList a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        if (com.navdy.hud.app.ui.component.vlist.VerticalList.access$200(this.this$0)) {
            com.navdy.hud.app.ui.component.vlist.VerticalList.access$500().v("initial pos changed");
        } else {
            int i = com.navdy.hud.app.ui.component.vlist.VerticalList.access$300(this.this$0).findFirstCompletelyVisibleItemPosition();
            int i0 = this.this$0.getRawPosition();
            label1: {
                label0: {
                    if (i != 0) {
                        break label0;
                    }
                    if (this.this$0.adapter.getItemCount() <= 3) {
                        break label0;
                    }
                    com.navdy.hud.app.ui.component.vlist.VerticalList.access$500().w(new StringBuilder().append("initial scroll did not work, firstV=").append(i).append(" raw=").append(i0).append(" scrollY=").append(com.navdy.hud.app.ui.component.vlist.VerticalList.access$400(this.this$0)).toString());
                    break label1;
                }
                com.navdy.hud.app.ui.component.vlist.VerticalList.access$500().w(new StringBuilder().append("initial scroll worked firstV=").append(i).append(" raw=").append(i0).append(" scrollY=").append(com.navdy.hud.app.ui.component.vlist.VerticalList.access$400(this.this$0)).toString());
            }
            com.navdy.hud.app.ui.component.vlist.VerticalList.access$600(this.this$0);
        }
    }
}
