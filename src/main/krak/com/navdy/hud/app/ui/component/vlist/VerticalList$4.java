package com.navdy.hud.app.ui.component.vlist;

class VerticalList$4 {
    final static int[] $SwitchMap$com$navdy$hud$app$ui$component$vlist$VerticalList$FontSize;
    
    static {
        $SwitchMap$com$navdy$hud$app$ui$component$vlist$VerticalList$FontSize = new int[com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$ui$component$vlist$VerticalList$FontSize;
        com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize a0 = com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_26;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$vlist$VerticalList$FontSize[com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_22.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$vlist$VerticalList$FontSize[com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_22_2.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$vlist$VerticalList$FontSize[com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_18.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$vlist$VerticalList$FontSize[com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_18_2.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException3) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$vlist$VerticalList$FontSize[com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_16.ordinal()] = 6;
        } catch(NoSuchFieldError ignoredException4) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$vlist$VerticalList$FontSize[com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_16_2.ordinal()] = 7;
        } catch(NoSuchFieldError ignoredException5) {
        }
    }
}
