package com.navdy.hud.app.ui.component.mainmenu;

class MainMenuScreen2$Presenter$3 implements Runnable {
    final com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter this$0;
    final boolean val$hasScrollModel;
    final int val$itemSelection;
    final com.navdy.hud.app.ui.component.mainmenu.IMenu val$menu;
    final java.util.List val$parentList;
    
    MainMenuScreen2$Presenter$3(com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter a, com.navdy.hud.app.ui.component.mainmenu.IMenu a0, java.util.List a1, int i, boolean b) {
        super();
        this.this$0 = a;
        this.val$menu = a0;
        this.val$parentList = a1;
        this.val$itemSelection = i;
        this.val$hasScrollModel = b;
    }
    
    public void run() {
        com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 a = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2)com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter.access$300(this.this$0);
        if (a != null) {
            com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().v(new StringBuilder().append("Loading menu:").append(this.val$menu.getType()).toString());
            if (com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter.access$200(this.this$0) != null) {
                com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter.access$200(this.this$0).onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.BACK_TO_PARENT);
            }
            com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter.access$202(this.this$0, this.val$menu);
            com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter.access$200(this.this$0).setSelectedIcon();
            com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().v(new StringBuilder().append("performBackAnimation:").append(com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter.access$200(this.this$0).getType()).toString());
            a.vmenuComponent.unlock(false);
            a.vmenuComponent.verticalList.setBindCallbacks(com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter.access$200(this.this$0).isBindCallsEnabled());
            a.vmenuComponent.updateView(this.val$parentList, this.val$itemSelection, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter.access$200(this.this$0).isFirstItemEmpty(), this.val$hasScrollModel, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter.access$200(this.this$0).getScrollIndex());
        }
    }
}
