package com.navdy.hud.app.ui.component.homescreen;

public class TimeView extends android.widget.RelativeLayout {
    final public static int CLOCK_UPDATE_INTERVAL = 30000;
    @InjectView(R.id.txt_ampm)
    android.widget.TextView ampmTextView;
    private com.squareup.otto.Bus bus;
    @InjectView(R.id.txt_day)
    android.widget.TextView dayTextView;
    private android.os.Handler handler;
    private com.navdy.service.library.log.Logger logger;
    private Runnable runnable;
    private StringBuilder stringBuilder;
    private com.navdy.hud.app.common.TimeHelper timeHelper;
    @InjectView(R.id.txt_time)
    android.widget.TextView timeTextView;
    
    public TimeView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public TimeView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public TimeView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.stringBuilder = new StringBuilder();
    }
    
    static com.navdy.service.library.log.Logger access$000(com.navdy.hud.app.ui.component.homescreen.TimeView a) {
        return a.logger;
    }
    
    static com.navdy.hud.app.common.TimeHelper access$100(com.navdy.hud.app.ui.component.homescreen.TimeView a) {
        return a.timeHelper;
    }
    
    static void access$200(com.navdy.hud.app.ui.component.homescreen.TimeView a) {
        a.updateTime();
    }
    
    static Runnable access$300(com.navdy.hud.app.ui.component.homescreen.TimeView a) {
        return a.runnable;
    }
    
    private void updateTime() {
        this.dayTextView.setText((CharSequence)this.timeHelper.getDay());
        String s = this.timeHelper.formatTime(new java.util.Date(), this.stringBuilder);
        this.timeTextView.setText((CharSequence)s);
        this.ampmTextView.setText((CharSequence)this.stringBuilder.toString());
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.handler != null) {
            this.handler.removeCallbacks(this.runnable);
        }
    }
    
    protected void onFinishInflate() {
        this.logger = com.navdy.hud.app.ui.component.homescreen.HomeScreenView.sLogger;
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View)this);
        if (!this.isInEditMode()) {
            this.timeHelper = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getTimeHelper();
            this.updateTime();
            android.content.IntentFilter a = new android.content.IntentFilter();
            a.addAction("android.intent.action.TIMEZONE_CHANGED");
            com.navdy.hud.app.HudApplication.getAppContext().registerReceiver((android.content.BroadcastReceiver)new com.navdy.hud.app.ui.component.homescreen.TimeView$1(this), a);
            this.runnable = (Runnable)new com.navdy.hud.app.ui.component.homescreen.TimeView$2(this);
            this.postDelayed(this.runnable, 30000L);
            this.bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
            this.bus.register(this);
        }
    }
    
    public void onTimeSettingsChange(com.navdy.hud.app.common.TimeHelper$UpdateClock a) {
        this.updateTime();
    }
    
    public void setView(com.navdy.hud.app.view.MainView$CustomAnimationMode a) {
        switch(com.navdy.hud.app.ui.component.homescreen.TimeView$3.$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode[a.ordinal()]) {
            case 2: {
                this.setX((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.timeShrinkLeftX);
                break;
            }
            case 1: {
                this.setX((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.timeX);
                break;
            }
        }
    }
}
