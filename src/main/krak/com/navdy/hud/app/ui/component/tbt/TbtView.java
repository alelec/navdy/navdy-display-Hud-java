package com.navdy.hud.app.ui.component.tbt;
import com.navdy.hud.app.R;

final public class TbtView extends android.widget.FrameLayout {
    final public static com.navdy.hud.app.ui.component.tbt.TbtView$Companion Companion;
    final private static com.navdy.service.library.log.Logger logger;
    final private static int paddingFull;
    final private static int paddingMedium;
    final private static android.content.res.Resources resources;
    final private static float shrinkLeftX;
    private java.util.HashMap _$_findViewCache;
    private com.navdy.hud.app.view.MainView$CustomAnimationMode currentMode;
    private com.navdy.hud.app.ui.component.tbt.TbtDirectionView directionView;
    private com.navdy.hud.app.ui.component.tbt.TbtDistanceView distanceView;
    private android.support.constraint.ConstraintLayout groupContainer;
    private com.navdy.hud.app.ui.component.tbt.TbtNextManeuverView tbtNextManeuverView;
    private com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView textInstructionView;
    
    static {
        Companion = new com.navdy.hud.app.ui.component.tbt.TbtView$Companion((kotlin.jvm.internal.DefaultConstructorMarker)null);
        logger = new com.navdy.service.library.log.Logger("TbtView");
        android.content.res.Resources a = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(a, "HudApplication.getAppContext().resources");
        resources = a;
        paddingFull = com.navdy.hud.app.ui.component.tbt.TbtView$Companion.access$getResources$p(Companion).getDimensionPixelSize(R.dimen.tbt_padding_full);
        paddingMedium = com.navdy.hud.app.ui.component.tbt.TbtView$Companion.access$getResources$p(Companion).getDimensionPixelSize(R.dimen.tbt_padding_medium);
        shrinkLeftX = com.navdy.hud.app.ui.component.tbt.TbtView$Companion.access$getResources$p(Companion).getDimension(R.dimen.tbt_shrinkleft_x);
    }
    
    public TbtView(android.content.Context a) {
        super(a);
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "context");
    }
    
    public TbtView(android.content.Context a, android.util.AttributeSet a0) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "context");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a0, "attrs");
        super(a, a0);
    }
    
    public TbtView(android.content.Context a, android.util.AttributeSet a0, int i) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "context");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a0, "attrs");
        super(a, a0, i);
    }
    
    final public static com.navdy.service.library.log.Logger access$getLogger$cp() {
        return logger;
    }
    
    final public static int access$getPaddingFull$cp() {
        return paddingFull;
    }
    
    final public static int access$getPaddingMedium$cp() {
        return paddingMedium;
    }
    
    final public static android.content.res.Resources access$getResources$cp() {
        return resources;
    }
    
    final public static float access$getShrinkLeftX$cp() {
        return shrinkLeftX;
    }
    
    final private void setConstraints(com.navdy.hud.app.view.MainView$CustomAnimationMode a) {
        if (a != null) {
            com.navdy.hud.app.ui.component.tbt.TbtNextManeuverView a0 = this.tbtNextManeuverView;
            Integer a1 = (a0 == null) ? null : Integer.valueOf(a0.getVisibility());
            boolean b = kotlin.jvm.internal.Intrinsics.areEqual(a1, Integer.valueOf(0));
            int i = (com.navdy.hud.app.ui.component.tbt.TbtView$WhenMappings.$EnumSwitchMapping$2[a.ordinal()] != 0) ? b ? Companion.getPaddingMedium() : Companion.getPaddingFull() : Companion.getPaddingMedium();
            android.view.ViewGroup$LayoutParams a2 = ((com.navdy.hud.app.ui.component.tbt.TbtDirectionView)super.findViewById(com.navdy.hud.app.R$id.tbtDirectionView)).getLayoutParams();
            if (a2 == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            }
            ((android.view.ViewGroup$MarginLayoutParams)a2).leftMargin = i;
            android.view.ViewGroup$LayoutParams a3 = ((com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView)super.findViewById(com.navdy.hud.app.R$id.tbtTextInstructionView)).getLayoutParams();
            if (a3 == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            }
            ((android.view.ViewGroup$MarginLayoutParams)a3).leftMargin = i;
            if (b) {
                com.navdy.hud.app.ui.component.tbt.TbtNextManeuverView a4 = this.tbtNextManeuverView;
                android.view.ViewGroup$LayoutParams a5 = (a4 == null) ? null : a4.getLayoutParams();
                if (a5 == null) {
                    throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
                }
                ((android.view.ViewGroup$MarginLayoutParams)a5).leftMargin = Companion.getPaddingMedium() * 2;
            }
            android.support.constraint.ConstraintSet a6 = new android.support.constraint.ConstraintSet();
            a6.clone(this.groupContainer);
            com.navdy.hud.app.ui.component.tbt.TbtDirectionView a7 = this.directionView;
            int i0 = (a7 == null) ? 0 : a7.getId();
            com.navdy.hud.app.ui.component.tbt.TbtDistanceView a8 = this.distanceView;
            a6.connect(i0, 1, (a8 == null) ? 0 : a8.getId(), 2, 0);
            com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView a9 = this.textInstructionView;
            int i1 = (a9 == null) ? 0 : a9.getId();
            com.navdy.hud.app.ui.component.tbt.TbtDirectionView a10 = this.directionView;
            a6.connect(i1, 1, (a10 == null) ? 0 : a10.getId(), 2, 0);
            if (b) {
                com.navdy.hud.app.ui.component.tbt.TbtNextManeuverView a11 = this.tbtNextManeuverView;
                int i2 = (a11 == null) ? 0 : a11.getId();
                com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView a12 = this.textInstructionView;
                a6.connect(i2, 1, (a12 == null) ? 0 : a12.getId(), 2, 0);
            }
            a6.applyTo(this.groupContainer);
            this.invalidate();
            this.requestLayout();
        }
    }
    
    final private void setViewAttrs(com.navdy.hud.app.view.MainView$CustomAnimationMode a) {
        com.navdy.hud.app.ui.component.tbt.TbtDistanceView a0 = this.distanceView;
        if (a0 != null) {
            a0.setMode(a);
        }
        com.navdy.hud.app.ui.component.tbt.TbtDirectionView a1 = this.directionView;
        if (a1 != null) {
            a1.setMode(a);
        }
        com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView a2 = this.textInstructionView;
        if (a2 != null) {
            a2.setMode(a);
        }
        com.navdy.hud.app.ui.component.tbt.TbtNextManeuverView a3 = this.tbtNextManeuverView;
        if (a3 != null) {
            a3.setMode(a);
        }
        this.setConstraints(a);
        this.currentMode = a;
    }
    
    public void _$_clearFindViewByIdCache() {
        if (this._$_findViewCache != null) {
            this._$_findViewCache.clear();
        }
    }
    
    public android.view.View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new java.util.HashMap();
        }
        android.view.View a = (android.view.View)this._$_findViewCache.get(Integer.valueOf(i));
        if (a == null) {
            a = this.findViewById(i);
            this._$_findViewCache.put(Integer.valueOf(i), a);
        }
        return a;
    }
    
    final public void clear() {
        com.navdy.hud.app.ui.component.tbt.TbtDistanceView a = this.distanceView;
        if (a != null) {
            a.clear();
        }
        com.navdy.hud.app.ui.component.tbt.TbtDirectionView a0 = this.directionView;
        if (a0 != null) {
            a0.clear();
        }
        com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView a1 = this.textInstructionView;
        if (a1 != null) {
            a1.clear();
        }
        com.navdy.hud.app.ui.component.tbt.TbtNextManeuverView a2 = this.tbtNextManeuverView;
        if (a2 != null) {
            a2.clear();
        }
    }
    
    final public void getCustomAnimator(com.navdy.hud.app.view.MainView$CustomAnimationMode a, android.animation.AnimatorSet$Builder a0) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "mode");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a0, "mainBuilder");
        this.setViewAttrs(a);
        switch(com.navdy.hud.app.ui.component.tbt.TbtView$WhenMappings.$EnumSwitchMapping$1[a.ordinal()]) {
            case 2: {
                a0.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator((android.view.View)this, Companion.getShrinkLeftX()));
                break;
            }
            case 1: {
                a0.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator((android.view.View)this, 0.0f));
                break;
            }
        }
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        android.view.View a = this.findViewById(R.id.groupContainer);
        if (a == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.support.constraint.ConstraintLayout");
        }
        this.groupContainer = (android.support.constraint.ConstraintLayout)a;
        android.view.View a0 = this.findViewById(R.id.tbtDistanceView);
        if (a0 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type com.navdy.hud.app.ui.component.tbt.TbtDistanceView");
        }
        this.distanceView = (com.navdy.hud.app.ui.component.tbt.TbtDistanceView)a0;
        android.view.View a1 = this.findViewById(R.id.tbtDirectionView);
        if (a1 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type com.navdy.hud.app.ui.component.tbt.TbtDirectionView");
        }
        this.directionView = (com.navdy.hud.app.ui.component.tbt.TbtDirectionView)a1;
        android.view.View a2 = this.findViewById(R.id.tbtTextInstructionView);
        if (a2 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView");
        }
        this.textInstructionView = (com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView)a2;
        android.view.View a3 = this.findViewById(R.id.tbtNextManeuverView);
        if (a3 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type com.navdy.hud.app.ui.component.tbt.TbtNextManeuverView");
        }
        this.tbtNextManeuverView = (com.navdy.hud.app.ui.component.tbt.TbtNextManeuverView)a3;
    }
    
    final public void setView(com.navdy.hud.app.view.MainView$CustomAnimationMode a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "mode");
        this.setViewAttrs(a);
        switch(com.navdy.hud.app.ui.component.tbt.TbtView$WhenMappings.$EnumSwitchMapping$0[a.ordinal()]) {
            case 2: {
                this.setX(Companion.getShrinkLeftX());
                break;
            }
            case 1: {
                this.setX(0.0f);
                break;
            }
        }
    }
    
    final public void updateDisplay(com.navdy.hud.app.maps.MapEvents$ManeuverDisplay a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "event");
        com.navdy.hud.app.ui.component.tbt.TbtDistanceView a0 = this.distanceView;
        if (a0 != null) {
            a0.updateDisplay(a);
        }
        com.navdy.hud.app.ui.component.tbt.TbtDirectionView a1 = this.directionView;
        if (a1 != null) {
            a1.updateDisplay(a);
        }
        com.navdy.hud.app.ui.component.tbt.TbtNextManeuverView a2 = this.tbtNextManeuverView;
        Integer a3 = (a2 == null) ? null : Integer.valueOf(a2.getVisibility());
        boolean b = kotlin.jvm.internal.Intrinsics.areEqual(a3, Integer.valueOf(0));
        com.navdy.hud.app.ui.component.tbt.TbtNextManeuverView a4 = this.tbtNextManeuverView;
        if (a4 != null) {
            a4.updateDisplay(a);
        }
        com.navdy.hud.app.ui.component.tbt.TbtNextManeuverView a5 = this.tbtNextManeuverView;
        Integer a6 = (a5 == null) ? null : Integer.valueOf(a5.getVisibility());
        boolean b0 = kotlin.jvm.internal.Intrinsics.areEqual(a6, Integer.valueOf(0));
        if (b == b0) {
            com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView a7 = this.textInstructionView;
            if (a7 != null) {
                com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView.updateDisplay$default(a7, a, false, false, 6, null);
            }
        } else {
            com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView a8 = this.textInstructionView;
            if (a8 != null) {
                a8.updateDisplay(a, true, b0);
            }
            this.setConstraints(this.currentMode);
        }
    }
}
