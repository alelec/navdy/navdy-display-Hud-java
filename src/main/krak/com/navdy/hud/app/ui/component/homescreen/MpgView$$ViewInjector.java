package com.navdy.hud.app.ui.component.homescreen;
import com.navdy.hud.app.R;

public class MpgView$$ViewInjector {
    public MpgView$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.ui.component.homescreen.MpgView a0, Object a1) {
        a0.mpgTextView = (android.widget.TextView)a.findRequiredView(a1, R.id.txt_mpg, "field 'mpgTextView'");
        a0.mpgLabelTextView = (android.widget.TextView)a.findRequiredView(a1, R.id.txt_mpg_label, "field 'mpgLabelTextView'");
    }
    
    public static void reset(com.navdy.hud.app.ui.component.homescreen.MpgView a) {
        a.mpgTextView = null;
        a.mpgLabelTextView = null;
    }
}
