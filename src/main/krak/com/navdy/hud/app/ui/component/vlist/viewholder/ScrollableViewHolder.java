package com.navdy.hud.app.ui.component.vlist.viewholder;
import com.navdy.hud.app.R;

public class ScrollableViewHolder extends com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder {
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.vlist.viewholder.ScrollableViewHolder.class);
    }
    
    public ScrollableViewHolder(android.view.ViewGroup a, com.navdy.hud.app.ui.component.vlist.VerticalList a0, android.os.Handler a1) {
        super(a, a0, a1);
    }
    
    public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model buildModel(int i) {
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a = new com.navdy.hud.app.ui.component.vlist.VerticalList$Model();
        a.type = com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.SCROLL_CONTENT;
        a.id = R.id.vlist_scroll_item;
        a.scrollItemLayoutId = i;
        return a;
    }
    
    public static com.navdy.hud.app.ui.component.vlist.viewholder.ScrollableViewHolder buildViewHolder(android.view.ViewGroup a, com.navdy.hud.app.ui.component.vlist.VerticalList a0, android.os.Handler a1) {
        return new com.navdy.hud.app.ui.component.vlist.viewholder.ScrollableViewHolder((android.view.ViewGroup)android.view.LayoutInflater.from(a.getContext()).inflate(R.layout.vlist_scroll_item, a, false), a0, a1);
    }
    
    public void bind(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState a0) {
    }
    
    public void clearAnimation() {
    }
    
    public void copyAndPosition(android.widget.ImageView a, android.widget.TextView a0, android.widget.TextView a1, android.widget.TextView a2, boolean b) {
    }
    
    public com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType getModelType() {
        return com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.SCROLL_CONTENT;
    }
    
    public void preBind(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState a0) {
        if (this.layout.getChildCount() == 0) {
            android.view.ViewGroup a1 = (android.view.ViewGroup)android.view.LayoutInflater.from(this.layout.getContext()).inflate(a.scrollItemLayoutId, this.layout, false);
            android.widget.FrameLayout$LayoutParams a2 = new android.widget.FrameLayout$LayoutParams(-1, -2);
            this.layout.addView((android.view.View)a1, (android.view.ViewGroup$LayoutParams)a2);
        }
    }
    
    public void select(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, int i, int i0) {
    }
    
    public void setItemState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State a, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType a0, int i, boolean b) {
    }
}
