package com.navdy.hud.app.ui.component.tbt;

final public class TbtNextManeuverView$Companion {
    private TbtNextManeuverView$Companion() {
    }
    
    public TbtNextManeuverView$Companion(kotlin.jvm.internal.DefaultConstructorMarker a) {
        this();
    }
    
    final private com.navdy.service.library.log.Logger getLogger() {
        return com.navdy.hud.app.ui.component.tbt.TbtNextManeuverView.access$getLogger$cp();
    }
}
