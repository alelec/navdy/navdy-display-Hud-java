package com.navdy.hud.app.ui.component.mainmenu;

class MusicMenu2$1 implements Runnable {
    final com.navdy.hud.app.ui.component.mainmenu.MusicMenu2 this$0;
    final com.navdy.service.library.events.audio.MusicCollectionRequest val$musicCollectionRequest;
    
    MusicMenu2$1(com.navdy.hud.app.ui.component.mainmenu.MusicMenu2 a, com.navdy.service.library.events.audio.MusicCollectionRequest a0) {
        super();
        this.this$0 = a;
        this.val$musicCollectionRequest = a0;
    }
    
    public void run() {
        String s = com.navdy.hud.app.ExtensionsKt.cacheKey(this.val$musicCollectionRequest);
        com.navdy.service.library.events.audio.MusicCollectionResponse a = (com.navdy.service.library.events.audio.MusicCollectionResponse)this.this$0.musicCollectionResponseMessageCache.get(s);
        if (a == null) {
            com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.access$400().d(new StringBuilder().append("Cache miss for MusicCollectionRequest ").append(this.val$musicCollectionRequest).append(", Key :").append(s).toString());
            com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.access$500(this.this$0).post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)this.val$musicCollectionRequest));
        } else {
            com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.access$400().d(new StringBuilder().append("Cache hit for MusicCollectionRequest ").append(this.val$musicCollectionRequest).append(", Key :").append(s).toString());
            com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.access$500(this.this$0).post(a);
        }
    }
}
