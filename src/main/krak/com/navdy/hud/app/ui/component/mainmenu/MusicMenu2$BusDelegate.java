package com.navdy.hud.app.ui.component.mainmenu;
import com.squareup.otto.Subscribe;

class MusicMenu2$BusDelegate implements com.navdy.hud.app.manager.MusicManager$MusicUpdateListener {
    com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter presenter;
    
    MusicMenu2$BusDelegate(com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter a) {
        this.presenter = a;
    }
    
    public void onAlbumArtUpdate(okio.ByteString a, boolean b) {
    }
    
    @Subscribe
    public void onMusicArtworkResponse(com.navdy.service.library.events.audio.MusicArtworkResponse a) {
        com.navdy.hud.app.ui.component.mainmenu.IMenu a0 = this.presenter.getCurrentMenu();
        if (a0 != null && a0.getType() == com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.MUSIC) {
            com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.access$200((com.navdy.hud.app.ui.component.mainmenu.MusicMenu2)a0, a);
        }
    }
    
    @Subscribe
    public void onMusicCollectionResponse(com.navdy.service.library.events.audio.MusicCollectionResponse a) {
        com.navdy.hud.app.ui.component.mainmenu.IMenu a0 = this.presenter.getCurrentMenu();
        if (a0 != null && a0.getType() == com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.MUSIC) {
            com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.access$100((com.navdy.hud.app.ui.component.mainmenu.MusicMenu2)a0, a);
        }
    }
    
    public void onTrackUpdated(com.navdy.service.library.events.audio.MusicTrackInfo a, java.util.Set a0, boolean b) {
        com.navdy.hud.app.ui.component.mainmenu.IMenu a1 = this.presenter.getCurrentMenu();
        if (a1 != null && a1.getType() == com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.MUSIC) {
            com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.access$300((com.navdy.hud.app.ui.component.mainmenu.MusicMenu2)a1, a);
        }
    }
}
