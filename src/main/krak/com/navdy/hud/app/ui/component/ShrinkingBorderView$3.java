package com.navdy.hud.app.ui.component;

class ShrinkingBorderView$3 implements android.animation.ValueAnimator$AnimatorUpdateListener {
    final com.navdy.hud.app.ui.component.ShrinkingBorderView this$0;
    
    ShrinkingBorderView$3(com.navdy.hud.app.ui.component.ShrinkingBorderView a) {
        super();
        this.this$0 = a;
    }
    
    public void onAnimationUpdate(android.animation.ValueAnimator a) {
        android.view.ViewGroup$MarginLayoutParams a0 = (android.view.ViewGroup$MarginLayoutParams)this.this$0.getLayoutParams();
        int i = ((Integer)a.getAnimatedValue()).intValue();
        if (a0.height != i) {
            a0.height = i;
            this.this$0.requestLayout();
        }
    }
}
