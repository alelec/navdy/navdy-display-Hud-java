package com.navdy.hud.app.ui.component.homescreen;
import com.navdy.hud.app.R;

public class NoLocationView$$ViewInjector {
    public NoLocationView$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.ui.component.homescreen.NoLocationView a0, Object a1) {
        a0.noLocationImage = (android.widget.ImageView)a.findRequiredView(a1, R.id.noLocationImage, "field 'noLocationImage'");
        a0.noLocationTextContainer = (android.widget.LinearLayout)a.findRequiredView(a1, R.id.noLocationTextContainer, "field 'noLocationTextContainer'");
    }
    
    public static void reset(com.navdy.hud.app.ui.component.homescreen.NoLocationView a) {
        a.noLocationImage = null;
        a.noLocationTextContainer = null;
    }
}
