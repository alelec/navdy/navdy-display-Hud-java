package com.navdy.hud.app.ui.component.destination;

abstract public interface IDestinationPicker {
    abstract public void onDestinationPickerClosed();
    
    
    abstract public boolean onItemClicked(int arg, int arg0, com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$DestinationPickerState arg1);
    
    
    abstract public boolean onItemSelected(int arg, int arg0, com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$DestinationPickerState arg1);
}
