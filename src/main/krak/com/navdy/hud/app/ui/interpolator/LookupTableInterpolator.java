package com.navdy.hud.app.ui.interpolator;

abstract class LookupTableInterpolator implements android.view.animation.Interpolator {
    final private float mStepSize;
    final private float[] mValues;
    
    public LookupTableInterpolator(float[] a) {
        this.mValues = a;
        this.mStepSize = 1f / (float)(this.mValues.length - 1);
    }
    
    public float getInterpolation(float f) {
        float f0 = 0.0f;
        if (f >= 1f) {
            f0 = 1f;
        } else if (f <= 0.0f) {
            f0 = 0.0f;
        } else {
            int i = Math.min((int)((float)(this.mValues.length - 1) * f), this.mValues.length - 2);
            float f1 = (f - (float)i * this.mStepSize) / this.mStepSize;
            f0 = this.mValues[i] + (this.mValues[i + 1] - this.mValues[i]) * f1;
        }
        return f0;
    }
}
