package com.navdy.hud.app.ui.component.carousel;

class CarouselLayout$3 implements com.navdy.hud.app.gesture.MultipleClickGestureDetector$IMultipleClickKeyGesture {
    final com.navdy.hud.app.ui.component.carousel.CarouselLayout this$0;
    
    CarouselLayout$3(com.navdy.hud.app.ui.component.carousel.CarouselLayout a) {
        super();
        this.this$0 = a;
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        return null;
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        return false;
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        return com.navdy.hud.app.ui.component.carousel.CarouselLayout.access$400(this.this$0, a);
    }
    
    public void onMultipleClick(int i) {
        if (this.this$0.itemChangeListener != null) {
            this.this$0.itemChangeListener.onExit();
        }
    }
}
