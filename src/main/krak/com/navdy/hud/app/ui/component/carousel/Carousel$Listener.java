package com.navdy.hud.app.ui.component.carousel;

abstract public interface Carousel$Listener {
    abstract public void onCurrentItemChanged(int arg, int arg0);
    
    
    abstract public void onCurrentItemChanging(int arg, int arg0, int arg1);
    
    
    abstract public void onExecuteItem(int arg, int arg0);
    
    
    abstract public void onExit();
}
