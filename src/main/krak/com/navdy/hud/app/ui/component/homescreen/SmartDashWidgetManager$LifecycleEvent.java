package com.navdy.hud.app.ui.component.homescreen;


public enum SmartDashWidgetManager$LifecycleEvent {
    PAUSE(0),
    RESUME(1);

    private int value;
    SmartDashWidgetManager$LifecycleEvent(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class SmartDashWidgetManager$LifecycleEvent extends Enum {
//    final private static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$LifecycleEvent[] $VALUES;
//    final public static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$LifecycleEvent PAUSE;
//    final public static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$LifecycleEvent RESUME;
//    
//    static {
//        PAUSE = new com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$LifecycleEvent("PAUSE", 0);
//        RESUME = new com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$LifecycleEvent("RESUME", 1);
//        com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$LifecycleEvent[] a = new com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$LifecycleEvent[2];
//        a[0] = PAUSE;
//        a[1] = RESUME;
//        $VALUES = a;
//    }
//    
//    private SmartDashWidgetManager$LifecycleEvent(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$LifecycleEvent valueOf(String s) {
//        return (com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$LifecycleEvent)Enum.valueOf(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$LifecycleEvent.class, s);
//    }
//    
//    public static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$LifecycleEvent[] values() {
//        return $VALUES.clone();
//    }
//}
//