package com.navdy.hud.app.ui.component.homescreen;
import com.navdy.hud.app.R;

public class HomeScreenView$$ViewInjector {
    public HomeScreenView$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.ui.component.homescreen.HomeScreenView a0, Object a1) {
        a0.mapContainer = (com.navdy.hud.app.ui.component.homescreen.NavigationView)a.findRequiredView(a1, R.id.map_container, "field 'mapContainer'");
        a0.smartDashContainer = (com.navdy.hud.app.ui.component.homescreen.BaseSmartDashView)a.findRequiredView(a1, R.id.smartDashContainer, "field 'smartDashContainer'");
        a0.openMapRoadInfoContainer = (com.navdy.hud.app.ui.component.homescreen.OpenRoadView)a.findRequiredView(a1, R.id.openMapRoadInfoContainer, "field 'openMapRoadInfoContainer'");
        a0.tbtView = (com.navdy.hud.app.ui.component.tbt.TbtViewContainer)a.findRequiredView(a1, R.id.activeMapRoadInfoContainer, "field 'tbtView'");
        a0.recalcRouteContainer = (com.navdy.hud.app.ui.component.homescreen.RecalculatingView)a.findRequiredView(a1, R.id.recalcRouteContainer, "field 'recalcRouteContainer'");
        a0.timeContainer = (com.navdy.hud.app.ui.component.homescreen.TimeView)a.findRequiredView(a1, R.id.time, "field 'timeContainer'");
        a0.activeEtaContainer = (com.navdy.hud.app.ui.component.homescreen.EtaView)a.findRequiredView(a1, R.id.activeEtaContainer, "field 'activeEtaContainer'");
        a0.navigationViewsContainer = (android.widget.RelativeLayout)a.findRequiredView(a1, R.id.navigation_views_container, "field 'navigationViewsContainer'");
        a0.laneGuidanceView = (com.navdy.hud.app.ui.component.homescreen.LaneGuidanceView)a.findRequiredView(a1, R.id.laneGuidance, "field 'laneGuidanceView'");
        a0.laneGuidanceIconIndicator = (android.widget.ImageView)a.findRequiredView(a1, R.id.lane_guidance_map_icon_indicator, "field 'laneGuidanceIconIndicator'");
        a0.mainscreenRightSection = (com.navdy.hud.app.ui.component.homescreen.HomeScreenRightSectionView)a.findRequiredView(a1, R.id.mainscreenRightSection, "field 'mainscreenRightSection'");
        a0.mapViewSpeedContainer = (android.view.ViewGroup)a.findRequiredView(a1, R.id.map_view_speed_container, "field 'mapViewSpeedContainer'");
        a0.speedView = (com.navdy.hud.app.ui.component.homescreen.SpeedView)a.findRequiredView(a1, R.id.speedContainer, "field 'speedView'");
        a0.speedLimitSignView = (com.navdy.hud.app.view.SpeedLimitSignView)a.findRequiredView(a1, R.id.home_screen_speed_limit_sign, "field 'speedLimitSignView'");
        a0.dashboardWidgetView = (com.navdy.hud.app.view.DashboardWidgetView)a.findRequiredView(a1, R.id.drive_score_events, "field 'dashboardWidgetView'");
        a0.mapMask = (android.widget.ImageView)a.findRequiredView(a1, R.id.map_mask, "field 'mapMask'");
    }
    
    public static void reset(com.navdy.hud.app.ui.component.homescreen.HomeScreenView a) {
        a.mapContainer = null;
        a.smartDashContainer = null;
        a.openMapRoadInfoContainer = null;
        a.tbtView = null;
        a.recalcRouteContainer = null;
        a.timeContainer = null;
        a.activeEtaContainer = null;
        a.navigationViewsContainer = null;
        a.laneGuidanceView = null;
        a.laneGuidanceIconIndicator = null;
        a.mainscreenRightSection = null;
        a.mapViewSpeedContainer = null;
        a.speedView = null;
        a.speedLimitSignView = null;
        a.dashboardWidgetView = null;
        a.mapMask = null;
    }
}
