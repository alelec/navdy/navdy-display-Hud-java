package com.navdy.hud.app.ui.component;

class SystemTrayView$1 {
    final static int[] $SwitchMap$com$navdy$hud$app$ui$component$SystemTrayView$Device;
    final static int[] $SwitchMap$com$navdy$hud$app$ui$component$SystemTrayView$State;
    
    static {
        $SwitchMap$com$navdy$hud$app$ui$component$SystemTrayView$State = new int[com.navdy.hud.app.ui.component.SystemTrayView$State.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$ui$component$SystemTrayView$State;
        com.navdy.hud.app.ui.component.SystemTrayView$State a0 = com.navdy.hud.app.ui.component.SystemTrayView$State.CONNECTED;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$SystemTrayView$State[com.navdy.hud.app.ui.component.SystemTrayView$State.DISCONNECTED.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$SystemTrayView$State[com.navdy.hud.app.ui.component.SystemTrayView$State.OK_BATTERY.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$SystemTrayView$State[com.navdy.hud.app.ui.component.SystemTrayView$State.LOW_BATTERY.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$SystemTrayView$State[com.navdy.hud.app.ui.component.SystemTrayView$State.EXTREMELY_LOW_BATTERY.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException3) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$SystemTrayView$State[com.navdy.hud.app.ui.component.SystemTrayView$State.PHONE_NETWORK_AVAILABLE.ordinal()] = 6;
        } catch(NoSuchFieldError ignoredException4) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$SystemTrayView$State[com.navdy.hud.app.ui.component.SystemTrayView$State.NO_PHONE_NETWORK.ordinal()] = 7;
        } catch(NoSuchFieldError ignoredException5) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$SystemTrayView$State[com.navdy.hud.app.ui.component.SystemTrayView$State.LOCATION_LOST.ordinal()] = 8;
        } catch(NoSuchFieldError ignoredException6) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$SystemTrayView$State[com.navdy.hud.app.ui.component.SystemTrayView$State.LOCATION_AVAILABLE.ordinal()] = 9;
        } catch(NoSuchFieldError ignoredException7) {
        }
        $SwitchMap$com$navdy$hud$app$ui$component$SystemTrayView$Device = new int[com.navdy.hud.app.ui.component.SystemTrayView$Device.values().length];
        int[] a1 = $SwitchMap$com$navdy$hud$app$ui$component$SystemTrayView$Device;
        com.navdy.hud.app.ui.component.SystemTrayView$Device a2 = com.navdy.hud.app.ui.component.SystemTrayView$Device.Phone;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException8) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$SystemTrayView$Device[com.navdy.hud.app.ui.component.SystemTrayView$Device.Dial.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException9) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$SystemTrayView$Device[com.navdy.hud.app.ui.component.SystemTrayView$Device.Gps.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException10) {
        }
    }
}
