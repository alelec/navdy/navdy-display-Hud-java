package com.navdy.hud.app.ui.component.carousel;

class FastScrollAnimator$1 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
    final com.navdy.hud.app.ui.component.carousel.FastScrollAnimator this$0;
    final android.animation.Animator$AnimatorListener val$listener;
    final int val$newPos;
    
    FastScrollAnimator$1(com.navdy.hud.app.ui.component.carousel.FastScrollAnimator a, android.animation.Animator$AnimatorListener a0, int i) {
        super();
        this.this$0 = a;
        this.val$listener = a0;
        this.val$newPos = i;
    }
    
    public void onAnimationEnd(android.animation.Animator a) {
        com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$302(this.this$0, android.os.SystemClock.elapsedRealtime());
        if (com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).isAnimationPending()) {
            com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).currentItem = this.val$newPos;
            android.view.View a0 = com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$100(this.this$0);
            com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$102(this.this$0, com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).middleLeftView);
            com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$100(this.this$0).setVisibility(4);
            com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$100(this.this$0).setAlpha(0.0f);
            com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).middleLeftView = a0;
            if (this.val$listener != null) {
                this.val$listener.onAnimationEnd(a);
            }
            com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).selectedItemView = com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).middleLeftView;
            if (com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).carouselIndicator != null) {
                com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).carouselIndicator.setCurrentItem(com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).currentItem);
            }
            if (com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).itemChangeListener != null) {
                com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).itemChangeListener.onCurrentItemChanged(com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).currentItem, ((com.navdy.hud.app.ui.component.carousel.Carousel$Model)com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).model.get(com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).currentItem)).id);
            }
            com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).runQueuedOperation();
        } else {
            this.this$0.endAnimation(this.val$listener, this.val$newPos, false);
        }
    }
    
    public void onAnimationStart(android.animation.Animator a) {
        com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$002(this.this$0, true);
        if (this.val$listener != null) {
            this.val$listener.onAnimationStart(a);
        }
        com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$100(this.this$0).setVisibility(0);
        com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).leftView.setAlpha(0.0f);
        com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).rightView.setAlpha(0.0f);
        com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).middleRightView.setAlpha(0.0f);
        if (!com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).isAnimationPending()) {
            com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).carouselAdapter.getView(this.val$newPos - 1, com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).leftView, com.navdy.hud.app.ui.component.carousel.Carousel$ViewType.SIDE, com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).imageLytResourceId, com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).sideImageSize);
            com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).carouselAdapter.getView(this.val$newPos + 1, com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).rightView, com.navdy.hud.app.ui.component.carousel.Carousel$ViewType.SIDE, com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).imageLytResourceId, com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).sideImageSize);
            com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).carouselAdapter.getView(this.val$newPos, com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).middleRightView, com.navdy.hud.app.ui.component.carousel.Carousel$ViewType.MIDDLE_RIGHT, com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).imageLytResourceId, com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).sideImageSize);
        }
    }
}
