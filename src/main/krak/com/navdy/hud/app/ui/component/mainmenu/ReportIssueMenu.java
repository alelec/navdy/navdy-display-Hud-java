package com.navdy.hud.app.ui.component.mainmenu;
import com.navdy.hud.app.R;

final public class ReportIssueMenu implements com.navdy.hud.app.ui.component.mainmenu.IMenu {
    final public static com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion Companion;
    final private static String NAV_ISSUE_SENT_TOAST_ID = "nav-issue-sent";
    final private static int TOAST_TIMEOUT = 1000;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model back;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model driveScore;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model etaInaccurate;
    final private static java.util.HashMap idToTitleMap;
    final private static com.navdy.service.library.log.Logger logger;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model maps;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model navigation;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model notFastestRoute;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model permanentClosure;
    final private static String reportIssue;
    final private static int reportIssueColor;
    final private static java.util.ArrayList reportIssueItems;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model roadBlocked;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model smartDash;
    final private static java.util.ArrayList takeSnapShotItems;
    final private static String takeSnapshot;
    final private static int takeSnapshotColor;
    final private static String toastSentSuccessfully;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model wrongDirection;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model wrongRoadName;
    private int backSelection;
    private int backSelectionId;
    private java.util.List currentItems;
    final private com.navdy.hud.app.ui.component.mainmenu.IMenu parent;
    final private com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter presenter;
    final private com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$ReportIssueMenuType type;
    final private com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent;
    
    static {
        Companion = new com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion((kotlin.jvm.internal.DefaultConstructorMarker)null);
        logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.class);
        kotlin.Pair[] a = new kotlin.Pair[4];
        a[0] = kotlin.TuplesKt.to(Integer.valueOf(R.id.take_snapshot_maps), "Maps");
        a[1] = kotlin.TuplesKt.to(Integer.valueOf(R.id.take_snapshot_navigation), "Navigation");
        a[2] = kotlin.TuplesKt.to(Integer.valueOf(R.id.take_snapshot_smart_dash), "Smart_Dash");
        a[3] = kotlin.TuplesKt.to(Integer.valueOf(R.id.take_snapshot_drive_score), "Drive_Score");
        idToTitleMap = kotlin.collections.MapsKt.hashMapOf(a);
        TOAST_TIMEOUT = 1000;
        NAV_ISSUE_SENT_TOAST_ID = "nav-issue-sent";
        reportIssueItems = new java.util.ArrayList();
        takeSnapShotItems = new java.util.ArrayList();
        android.content.res.Resources a0 = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        String s = a0.getString(R.string.issue_successfully_sent);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(s, "resources.getString(R.st\u2026.issue_successfully_sent)");
        toastSentSuccessfully = s;
        String s0 = a0.getString(R.string.report_navigation_issue);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(s0, "resources.getString(R.st\u2026.report_navigation_issue)");
        reportIssue = s0;
        String s1 = a0.getString(R.string.take_snapshot);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(s1, "resources.getString(R.string.take_snapshot)");
        takeSnapshot = s1;
        reportIssueColor = a0.getColor(R.color.mm_options_report_issue);
        takeSnapshotColor = a0.getColor(R.color.options_dash_purple);
        int i = a0.getColor(R.color.icon_bk_color_unselected);
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a1 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getReportIssueColor$p(Companion), com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getReportIssueColor$p(Companion), a0.getString(R.string.back), (String)null);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(a1, "IconBkColorViewHolder.bu\u2026back), null\n            )");
        back = a1;
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a2 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.report_issue_menu_eta_inaccurate, R.drawable.icon_eta, com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getReportIssueColor$p(Companion), i, com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getReportIssueColor$p(Companion), a0.getString(com.navdy.hud.app.util.ReportIssueService$IssueType.INEFFICIENT_ROUTE_ETA_TRAFFIC.getTitleStringResource()), (String)null);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(a2, "IconBkColorViewHolder.bu\u2026urce), null\n            )");
        etaInaccurate = a2;
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a3 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.report_issue_menu_not_fastest_route, R.drawable.icon_bad_route, com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getReportIssueColor$p(Companion), i, com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getReportIssueColor$p(Companion), a0.getString(com.navdy.hud.app.util.ReportIssueService$IssueType.INEFFICIENT_ROUTE_SELECTED.getTitleStringResource()), (String)null);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(a3, "IconBkColorViewHolder.bu\u2026urce), null\n            )");
        notFastestRoute = a3;
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a4 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.report_issue_menu_road_blocked, R.drawable.icon_road_closed_temp, com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getReportIssueColor$p(Companion), i, com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getReportIssueColor$p(Companion), a0.getString(com.navdy.hud.app.util.ReportIssueService$IssueType.ROAD_CLOSED.getTitleStringResource()), a0.getString(com.navdy.hud.app.util.ReportIssueService$IssueType.ROAD_CLOSED.getMessageStringResource()));
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(a4, "IconBkColorViewHolder.bu\u2026ngResource)\n            )");
        roadBlocked = a4;
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a5 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.report_issue_menu_wrong_direction, R.drawable.icon_bad_map_info, com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getReportIssueColor$p(Companion), i, com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getReportIssueColor$p(Companion), a0.getString(com.navdy.hud.app.util.ReportIssueService$IssueType.WRONG_DIRECTION.getTitleStringResource()), (String)null);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(a5, "IconBkColorViewHolder.bu\u2026urce), null\n            )");
        wrongDirection = a5;
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a6 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.report_issue_menu_wrong_road_name, R.drawable.icon_wrong_road_name, com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getReportIssueColor$p(Companion), i, com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getReportIssueColor$p(Companion), a0.getString(com.navdy.hud.app.util.ReportIssueService$IssueType.ROAD_NAME.getTitleStringResource()), (String)null);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(a6, "IconBkColorViewHolder.bu\u2026urce), null\n            )");
        wrongRoadName = a6;
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a7 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.report_issue_menu_permanent_closure, R.drawable.icon_road_closed_perm, com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getReportIssueColor$p(Companion), i, com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getReportIssueColor$p(Companion), a0.getString(com.navdy.hud.app.util.ReportIssueService$IssueType.ROAD_CLOSED_PERMANENT.getTitleStringResource()), (String)null);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(a7, "IconBkColorViewHolder.bu\u2026urce), null\n            )");
        permanentClosure = a7;
        com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getReportIssueItems$p(Companion).add(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getBack$p(Companion));
        com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getReportIssueItems$p(Companion).add(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getEtaInaccurate$p(Companion));
        com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getReportIssueItems$p(Companion).add(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getNotFastestRoute$p(Companion));
        com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getReportIssueItems$p(Companion).add(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getRoadBlocked$p(Companion));
        com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getReportIssueItems$p(Companion).add(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getWrongDirection$p(Companion));
        com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getReportIssueItems$p(Companion).add(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getWrongRoadName$p(Companion));
        com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getReportIssueItems$p(Companion).add(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getPermanentClosure$p(Companion));
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a8 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.take_snapshot_maps, R.drawable.icon_options_report_issue_2, com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getTakeSnapshotColor$p(Companion), i, com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getTakeSnapshotColor$p(Companion), a0.getString(R.string.snapshot_maps), (String)null);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(a8, "IconBkColorViewHolder.bu\u2026ing.snapshot_maps), null)");
        maps = a8;
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a9 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.take_snapshot_navigation, R.drawable.icon_options_report_issue_2, com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getTakeSnapshotColor$p(Companion), i, com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getTakeSnapshotColor$p(Companion), a0.getString(R.string.snapshot_navigation), (String)null);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(a9, "IconBkColorViewHolder.bu\u2026apshot_navigation), null)");
        navigation = a9;
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a10 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.take_snapshot_smart_dash, R.drawable.icon_options_report_issue_2, com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getTakeSnapshotColor$p(Companion), i, com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getTakeSnapshotColor$p(Companion), a0.getString(R.string.snapshot_smart_dash), (String)null);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(a10, "IconBkColorViewHolder.bu\u2026apshot_smart_dash), null)");
        driveScore = a10;
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a11 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.take_snapshot_drive_score, R.drawable.icon_options_report_issue_2, com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getTakeSnapshotColor$p(Companion), i, com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getTakeSnapshotColor$p(Companion), a0.getString(R.string.snapshot_drive_score), (String)null);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(a11, "IconBkColorViewHolder.bu\u2026pshot_drive_score), null)");
        smartDash = a11;
        com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getTakeSnapShotItems$p(Companion).add(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getMaps$p(Companion));
        com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getTakeSnapShotItems$p(Companion).add(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getNavigation$p(Companion));
        com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getTakeSnapShotItems$p(Companion).add(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getDriveScore$p(Companion));
        com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getTakeSnapShotItems$p(Companion).add(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getSmartDash$p(Companion));
    }
    
    public ReportIssueMenu(com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter a0, com.navdy.hud.app.ui.component.mainmenu.IMenu a1) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "vscrollComponent");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a0, "presenter");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a1, "parent");
        this(a, a0, a1, com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$ReportIssueMenuType.NAVIGATION_ISSUES);
    }
    
    public ReportIssueMenu(com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter a0, com.navdy.hud.app.ui.component.mainmenu.IMenu a1, com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$ReportIssueMenuType a2) {
        super();
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "vscrollComponent");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a0, "presenter");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a2, "type");
        this.vscrollComponent = a;
        this.presenter = a0;
        this.parent = a1;
        this.type = a2;
    }
    
    public ReportIssueMenu(com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter a0, com.navdy.hud.app.ui.component.mainmenu.IMenu a1, com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$ReportIssueMenuType a2, int i, kotlin.jvm.internal.DefaultConstructorMarker a3) {
        if ((i & 8) != 0) {
            a2 = com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$ReportIssueMenuType.NAVIGATION_ISSUES;
        }
        this(a, a0, a1, a2);
    }
    
    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model access$getBack$cp() {
        return back;
    }
    
    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model access$getDriveScore$cp() {
        return driveScore;
    }
    
    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model access$getEtaInaccurate$cp() {
        return etaInaccurate;
    }
    
    final public static java.util.HashMap access$getIdToTitleMap$cp() {
        return idToTitleMap;
    }
    
    final public static com.navdy.service.library.log.Logger access$getLogger$cp() {
        return logger;
    }
    
    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model access$getMaps$cp() {
        return maps;
    }
    
    final public static String access$getNAV_ISSUE_SENT_TOAST_ID$cp() {
        return NAV_ISSUE_SENT_TOAST_ID;
    }
    
    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model access$getNavigation$cp() {
        return navigation;
    }
    
    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model access$getNotFastestRoute$cp() {
        return notFastestRoute;
    }
    
    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model access$getPermanentClosure$cp() {
        return permanentClosure;
    }
    
    final public static com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter access$getPresenter$p(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu a) {
        return a.presenter;
    }
    
    final public static String access$getReportIssue$cp() {
        return reportIssue;
    }
    
    final public static int access$getReportIssueColor$cp() {
        return reportIssueColor;
    }
    
    final public static java.util.ArrayList access$getReportIssueItems$cp() {
        return reportIssueItems;
    }
    
    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model access$getRoadBlocked$cp() {
        return roadBlocked;
    }
    
    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model access$getSmartDash$cp() {
        return smartDash;
    }
    
    final public static int access$getTOAST_TIMEOUT$cp() {
        return TOAST_TIMEOUT;
    }
    
    final public static java.util.ArrayList access$getTakeSnapShotItems$cp() {
        return takeSnapShotItems;
    }
    
    final public static String access$getTakeSnapshot$cp() {
        return takeSnapshot;
    }
    
    final public static int access$getTakeSnapshotColor$cp() {
        return takeSnapshotColor;
    }
    
    final public static String access$getToastSentSuccessfully$cp() {
        return toastSentSuccessfully;
    }
    
    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model access$getWrongDirection$cp() {
        return wrongDirection;
    }
    
    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model access$getWrongRoadName$cp() {
        return wrongRoadName;
    }
    
    final public static void access$showSentToast(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu a) {
        a.showSentToast();
    }
    
    final private void reportIssue(com.navdy.hud.app.util.ReportIssueService$IssueType a) {
        this.presenter.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$reportIssue$1(this, a));
    }
    
    final private void showSentToast() {
        android.os.Bundle a = new android.os.Bundle();
        a.putInt("13", com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getTOAST_TIMEOUT$p(Companion));
        a.putInt("8", R.drawable.icon_report_issue);
        a.putString("4", com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getToastSentSuccessfully$p(Companion));
        a.putInt("5", R.style.Glances_1);
        a.putString("17", com.navdy.hud.app.framework.voice.TTSUtils.TTS_NAV_STOPPED);
        a.putBoolean("21", true);
        a.putString("18", com.navdy.service.library.events.ui.Screen.SCREEN_BACK.name());
        com.navdy.hud.app.framework.toast.ToastManager.getInstance().addToast(new com.navdy.hud.app.framework.toast.ToastManager$ToastParams(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getNAV_ISSUE_SENT_TOAST_ID$p(Companion), a, (com.navdy.hud.app.framework.toast.IToastCallback)null, true, false));
    }
    
    final private void takeSnapshot(int i) {
        this.presenter.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$takeSnapshot$1(this, i));
    }
    
    public com.navdy.hud.app.ui.component.mainmenu.IMenu getChildMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu a, String s, String s0) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "parent");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(s, "args");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(s0, "path");
        return null;
    }
    
    public int getInitialSelection() {
        int i = 0;
        com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$ReportIssueMenuType a = this.type;
        switch(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$WhenMappings.$EnumSwitchMapping$1[a.ordinal()]) {
            case 2: {
                i = 0;
                break;
            }
            case 1: {
                i = 1;
                break;
            }
            default: {
                throw new kotlin.NoWhenBranchMatchedException();
            }
        }
        return i;
    }
    
    public java.util.List getItems() {
        java.util.List a = null;
        com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$ReportIssueMenuType a0 = this.type;
        int i = com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$WhenMappings.$EnumSwitchMapping$0[a0.ordinal()];
        label0: {
            switch(i) {
                case 2: {
                    this.currentItems = (java.util.List)com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getTakeSnapShotItems$p(Companion);
                    break;
                }
                case 1: {
                    this.currentItems = (java.util.List)com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getReportIssueItems$p(Companion);
                    break;
                }
                default: {
                    a = null;
                    break label0;
                }
            }
            a = this.currentItems;
        }
        return a;
    }
    
    public com.navdy.hud.app.ui.component.vlist.VerticalList$Model getModelfromPos(int i) {
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a = null;
        java.util.List a0 = this.currentItems;
        label2: {
            label0: {
                label1: {
                    if (a0 == null) {
                        break label1;
                    }
                    java.util.List a1 = this.currentItems;
                    if (a1 == null) {
                        kotlin.jvm.internal.Intrinsics.throwNpe();
                    }
                    if (a1.size() > i) {
                        break label0;
                    }
                }
                a = null;
                break label2;
            }
            java.util.List a2 = this.currentItems;
            if (a2 == null) {
                kotlin.jvm.internal.Intrinsics.throwNpe();
            }
            a = (com.navdy.hud.app.ui.component.vlist.VerticalList$Model)a2.get(i);
        }
        return a;
    }
    
    public com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex getScrollIndex() {
        return null;
    }
    
    public com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu getType() {
        return com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.REPORT_ISSUE;
    }
    
    public boolean isBindCallsEnabled() {
        return false;
    }
    
    public boolean isFirstItemEmpty() {
        return true;
    }
    
    public boolean isItemClickable(int i, int i0) {
        return true;
    }
    
    public void onBindToView(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, android.view.View a0, int i, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState a1) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "model");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a0, "view");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a1, "state");
    }
    
    public void onFastScrollEnd() {
    }
    
    public void onFastScrollStart() {
    }
    
    public void onItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "selection");
    }
    
    public void onScrollIdle() {
    }
    
    public void onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "level");
    }
    
    public boolean selectItem(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "selection");
        com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getLogger$p(Companion).v(new StringBuilder().append("select id:").append(a.id).append(" pos:").append(a.pos).toString());
        switch(a.id) {
            case R.id.take_snapshot_drive_score: case R.id.take_snapshot_maps: case R.id.take_snapshot_navigation: case R.id.take_snapshot_smart_dash: {
                this.takeSnapshot(a.id);
                break;
            }
            case R.id.report_issue_menu_wrong_road_name: {
                com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getLogger$p(Companion).v("Wrong road name");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("report-issue-wrong-road-name");
                this.reportIssue(com.navdy.hud.app.util.ReportIssueService$IssueType.ROAD_NAME);
                break;
            }
            case R.id.report_issue_menu_wrong_direction: {
                com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getLogger$p(Companion).v("Wrong direction");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("report-issue-wrong-direction");
                this.reportIssue(com.navdy.hud.app.util.ReportIssueService$IssueType.WRONG_DIRECTION);
                break;
            }
            case R.id.report_issue_menu_road_blocked: {
                com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getLogger$p(Companion).v("Road blocked");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("report-issue-road-blocked");
                this.reportIssue(com.navdy.hud.app.util.ReportIssueService$IssueType.ROAD_CLOSED);
                break;
            }
            case R.id.report_issue_menu_permanent_closure: {
                com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getLogger$p(Companion).v("Permanent closure");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("report-issue-permanent-closure");
                this.reportIssue(com.navdy.hud.app.util.ReportIssueService$IssueType.ROAD_CLOSED_PERMANENT);
                break;
            }
            case R.id.report_issue_menu_not_fastest_route: {
                com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getLogger$p(Companion).v("Not the fastest route");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("report-issue-not-fastest-route");
                this.reportIssue(com.navdy.hud.app.util.ReportIssueService$IssueType.INEFFICIENT_ROUTE_SELECTED);
                break;
            }
            case R.id.report_issue_menu_eta_inaccurate: {
                com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getLogger$p(Companion).v("ETA inaccurate");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("report-issue-eta-inaccurate");
                this.reportIssue(com.navdy.hud.app.util.ReportIssueService$IssueType.INEFFICIENT_ROUTE_ETA_TRAFFIC);
                break;
            }
            case R.id.menu_back: {
                com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getLogger$p(Companion).v("back");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("report-issue-back");
                this.presenter.loadMenu(this.parent, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.BACK_TO_PARENT, this.backSelection, this.backSelectionId, true);
                break;
            }
        }
        return false;
    }
    
    public void setBackSelectionId(int i) {
        this.backSelectionId = i;
    }
    
    public void setBackSelectionPos(int i) {
        this.backSelection = i;
    }
    
    public void setSelectedIcon() {
        com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$ReportIssueMenuType a = this.type;
        switch(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$WhenMappings.$EnumSwitchMapping$2[a.ordinal()]) {
            case 2: {
                this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_options_report_issue_2, com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getTakeSnapshotColor$p(Companion), (android.graphics.Shader)null, 1f);
                this.vscrollComponent.selectedText.setText((CharSequence)com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getTakeSnapshot$p(Companion));
                break;
            }
            case 1: {
                this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_options_report_issue_2, com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getReportIssueColor$p(Companion), (android.graphics.Shader)null, 1f);
                this.vscrollComponent.selectedText.setText((CharSequence)com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion.access$getReportIssue$p(Companion));
                break;
            }
        }
    }
    
    public void showToolTip() {
    }
}
