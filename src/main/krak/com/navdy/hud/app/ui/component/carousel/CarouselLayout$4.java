package com.navdy.hud.app.ui.component.carousel;

class CarouselLayout$4 implements Runnable {
    final com.navdy.hud.app.ui.component.carousel.CarouselLayout this$0;
    final boolean val$animated;
    final java.util.List val$newModel;
    final int val$position;
    
    CarouselLayout$4(com.navdy.hud.app.ui.component.carousel.CarouselLayout a, java.util.List a0, int i, boolean b) {
        super();
        this.this$0 = a;
        this.val$newModel = a0;
        this.val$position = i;
        this.val$animated = b;
    }
    
    public void run() {
        com.navdy.hud.app.ui.component.carousel.CarouselLayout.access$500(this.this$0, this.val$newModel, this.val$position, this.val$animated);
    }
}
