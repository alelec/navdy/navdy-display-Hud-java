package com.navdy.hud.app.ui.component.mainmenu;

class MainMenu$4 implements Runnable {
    final com.navdy.hud.app.ui.component.mainmenu.MainMenu this$0;
    
    MainMenu$4(com.navdy.hud.app.ui.component.mainmenu.MainMenu a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        com.navdy.hud.app.framework.notifications.NotificationManager a = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
        com.navdy.hud.app.framework.voice.VoiceSearchNotification a0 = (com.navdy.hud.app.framework.voice.VoiceSearchNotification)a.getNotification("navdy#voicesearch#notif");
        if (a0 == null) {
            a0 = new com.navdy.hud.app.framework.voice.VoiceSearchNotification();
        }
        a.addNotification((com.navdy.hud.app.framework.notifications.INotification)a0);
    }
}
