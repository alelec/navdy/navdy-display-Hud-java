package com.navdy.hud.app.ui.component.homescreen;

class SmartDashView$2 {
    final com.navdy.hud.app.ui.component.homescreen.SmartDashView this$0;
    final com.navdy.hud.app.ui.component.homescreen.HomeScreenView val$homeScreenView;
    
    SmartDashView$2(com.navdy.hud.app.ui.component.homescreen.SmartDashView a, com.navdy.hud.app.ui.component.homescreen.HomeScreenView a0) {
        super();
        this.this$0 = a;
        this.val$homeScreenView = a0;
    }
    
    public void ObdStateChangeEvent(com.navdy.hud.app.obd.ObdManager$ObdConnectionStatusEvent a) {
        com.navdy.hud.app.obd.ObdManager a0 = com.navdy.hud.app.obd.ObdManager.getInstance();
        if (this.this$0.tachometerPresenter != null) {
            int i = a0.getEngineRpm();
            if (i < 0) {
                i = 0;
            }
            this.this$0.tachometerPresenter.setRPM(i);
        }
        if (com.navdy.hud.app.ui.component.homescreen.SmartDashView.access$200(this.this$0) != null) {
            com.navdy.hud.app.ui.component.homescreen.SmartDashView.access$200(this.this$0).reLoadAvailableWidgets(false);
        }
    }
    
    public void onDashboardPreferences(com.navdy.service.library.events.preferences.DashboardPreferences a) {
        if (a != null) {
            if (a.middleGauge != null) {
                switch(com.navdy.hud.app.ui.component.homescreen.SmartDashView$8.$SwitchMap$com$navdy$service$library$events$preferences$MiddleGauge[a.middleGauge.ordinal()]) {
                    case 2: {
                        com.navdy.hud.app.ui.component.homescreen.SmartDashView.access$500(this.this$0, "PREFERENCE_MIDDLE_GAUGE", Integer.valueOf(0));
                        break;
                    }
                    case 1: {
                        com.navdy.hud.app.ui.component.homescreen.SmartDashView.access$500(this.this$0, "PREFERENCE_MIDDLE_GAUGE", Integer.valueOf(1));
                        break;
                    }
                }
            }
            if (a.scrollableSide != null) {
                switch(com.navdy.hud.app.ui.component.homescreen.SmartDashView$8.$SwitchMap$com$navdy$service$library$events$preferences$ScrollableSide[a.scrollableSide.ordinal()]) {
                    case 2: {
                        com.navdy.hud.app.ui.component.homescreen.SmartDashView.access$500(this.this$0, "PREFERENCE_SCROLLABLE_SIDE", Integer.valueOf(1));
                        break;
                    }
                    case 1: {
                        com.navdy.hud.app.ui.component.homescreen.SmartDashView.access$500(this.this$0, "PREFERENCE_SCROLLABLE_SIDE", Integer.valueOf(0));
                        break;
                    }
                }
            }
            if (com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.isValidGaugeId(a.leftGaugeId)) {
                com.navdy.hud.app.ui.component.homescreen.SmartDashView.access$500(this.this$0, "PREFERENCE_LEFT_GAUGE", a.leftGaugeId);
            }
            if (com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.isValidGaugeId(a.rightGaugeId)) {
                com.navdy.hud.app.ui.component.homescreen.SmartDashView.access$500(this.this$0, "PREFERENCE_RIGHT_GAUGE", a.rightGaugeId);
            }
            com.navdy.hud.app.ui.component.homescreen.SmartDashView.access$600(this.this$0);
        }
    }
    
    public void onDriverProfileChanged(com.navdy.hud.app.event.DriverProfileChanged a) {
        this.val$homeScreenView.updateDriverPrefs();
        if (com.navdy.hud.app.ui.component.homescreen.SmartDashView.access$200(this.this$0) != null) {
            com.navdy.hud.app.ui.component.homescreen.SmartDashView.access$200(this.this$0).reLoadAvailableWidgets(false);
        }
    }
    
    public void onGpsLocationChanged(android.location.Location a) {
        double d = (double)a.getBearing();
        com.navdy.hud.app.ui.component.homescreen.SmartDashView.access$000(this.this$0).setHeading(d);
        double d0 = com.navdy.hud.app.ui.component.homescreen.SmartDashView.access$000(this.this$0).getHeading();
        if (com.navdy.hud.app.ui.component.homescreen.SmartDashView.access$100().isLoggable(2)) {
            com.navdy.hud.app.ui.component.homescreen.SmartDashView.access$100().v(new StringBuilder().append("SmartDash New: Heading :").append(d0).append(", ").append(com.navdy.hud.app.device.gps.GpsUtils.getHeadingDirection(d0)).append(", Provider :").append(a.getProvider()).toString());
        }
        com.navdy.hud.app.ui.component.homescreen.SmartDashView.access$200(this.this$0).updateWidget("COMPASS_WIDGET", Double.valueOf(d0));
    }
    
    public void onLocationFixChangeEvent(com.navdy.hud.app.maps.MapEvents$LocationFix a) {
        if (!a.locationAvailable) {
            com.navdy.hud.app.ui.component.homescreen.SmartDashView.access$000(this.this$0).reset();
            com.navdy.hud.app.ui.component.homescreen.SmartDashView.access$200(this.this$0).updateWidget("COMPASS_WIDGET", Float.valueOf(0.0f));
        }
    }
    
    public void onSupportedPidEventsChange(com.navdy.hud.app.obd.ObdManager$ObdSupportedPidsChangedEvent a) {
        com.navdy.hud.app.ui.component.homescreen.SmartDashView.access$200(this.this$0).reLoadAvailableWidgets(false);
    }
    
    public void onUserGaugePreferencesChanged(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$UserPreferenceChanged a) {
        com.navdy.hud.app.ui.component.homescreen.SmartDashView.access$200(this.this$0).reLoadAvailableWidgets(false);
    }
    
    public void updateETA(com.navdy.hud.app.maps.MapEvents$ManeuverDisplay a) {
        if (this.val$homeScreenView.isNavigationActive()) {
            if (a.etaDate != null) {
                if (!android.text.TextUtils.equals((CharSequence)a.eta, (CharSequence)com.navdy.hud.app.ui.component.homescreen.SmartDashView.access$300(this.this$0))) {
                    com.navdy.hud.app.ui.component.homescreen.SmartDashView.access$302(this.this$0, a.eta);
                    this.this$0.etaText.setText((CharSequence)com.navdy.hud.app.ui.component.homescreen.SmartDashView.access$300(this.this$0));
                }
                if (!android.text.TextUtils.equals((CharSequence)a.etaAmPm, (CharSequence)com.navdy.hud.app.ui.component.homescreen.SmartDashView.access$400(this.this$0))) {
                    com.navdy.hud.app.ui.component.homescreen.SmartDashView.access$402(this.this$0, a.etaAmPm);
                    this.this$0.etaAmPm.setText((CharSequence)com.navdy.hud.app.ui.component.homescreen.SmartDashView.access$400(this.this$0));
                }
            } else {
                com.navdy.hud.app.ui.component.homescreen.SmartDashView.access$100().w("etaDate is not set");
            }
        }
    }
}
