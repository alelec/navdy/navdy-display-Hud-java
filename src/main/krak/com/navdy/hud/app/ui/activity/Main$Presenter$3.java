package com.navdy.hud.app.ui.activity;

class Main$Presenter$3 implements com.navdy.hud.app.ui.framework.IScreenAnimationListener {
    final com.navdy.hud.app.ui.activity.Main$Presenter this$0;
    
    Main$Presenter$3(com.navdy.hud.app.ui.activity.Main$Presenter a) {
        super();
        this.this$0 = a;
    }
    
    public void onStart(com.navdy.hud.app.screen.BaseScreen a, com.navdy.hud.app.screen.BaseScreen a0) {
        com.navdy.hud.app.ui.activity.Main.access$200().v(new StringBuilder().append("screen anim start in[").append(a).append("]  out[").append(a0).append("]").toString());
        this.this$0.clearInputFocus();
        com.navdy.hud.app.ui.framework.UIStateManager dummy = this.this$0.uiStateManager;
        if (!com.navdy.hud.app.ui.framework.UIStateManager.isFullscreenMode(a.getScreen())) {
            this.this$0.setNotificationColorVisibility(4);
            this.this$0.setSystemTrayVisibility(4);
        }
    }
    
    public void onStop(com.navdy.hud.app.screen.BaseScreen a, com.navdy.hud.app.screen.BaseScreen a0) {
        com.navdy.hud.app.ui.activity.Main.access$200().v(new StringBuilder().append("screen anim stop in[").append(a).append("]  out[").append(a0).append("]").toString());
        com.navdy.hud.app.ui.activity.Main$Presenter.access$400(this.this$0, com.navdy.hud.app.ui.activity.Main$ScreenCategory.SCREEN, a.getScreen());
    }
}
