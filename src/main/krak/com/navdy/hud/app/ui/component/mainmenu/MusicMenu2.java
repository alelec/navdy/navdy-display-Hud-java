package com.navdy.hud.app.ui.component.mainmenu;
import com.navdy.hud.app.R;

public class MusicMenu2 implements com.navdy.hud.app.ui.component.mainmenu.IMenu {
    private static java.util.Map analyticsTypeStringMap;
    private static int androidBgColor;
    private static int appleMusicBgColor;
    private static int applePodcastsBgColor;
    final private static Object artworkRequestLock;
    private static java.util.Stack artworkRequestQueue;
    private static int artworkSize;
    private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model back;
    private static int backColor;
    private static int bgColorUnselected;
    private static com.navdy.hud.app.ui.component.mainmenu.MusicMenu2$BusDelegate busDelegate;
    private static com.navdy.service.library.events.audio.MusicCollectionInfo infoForCurrentArtworkRequest;
    private static java.util.Map lastPlayedMusicMenuData;
    final private static com.navdy.service.library.log.Logger logger;
    private static java.util.Map menuSourceIconMap;
    private static java.util.Map menuSourceStringMap;
    private static java.util.Map menuTypeIconMap;
    private static java.util.Map menuTypeStringMap;
    private static int mmMusicColor;
    private static android.content.res.Resources resources;
    private static String shuffle;
    private static String shuffleOff;
    private static String shuffleOn;
    private static String shufflePlay;
    private static int sourceBgColor;
    private static int transparentColor;
    private boolean anyPlayersPermitted;
    private long artWorkRequestTime;
    private int backSelection;
    private int backSelectionId;
    private int bgColorSelected;
    private com.squareup.otto.Bus bus;
    private java.util.List cachedList;
    private com.navdy.service.library.events.audio.MusicCollectionRequest currentMusicCollectionRequest;
    private long currentMusicCollectionRequestTime;
    private com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex fastScrollIndex;
    private android.os.Handler handler;
    private int highlightedItem;
    private boolean initialized;
    private boolean isRequestingMusicCollection;
    private boolean isShuffling;
    final private com.navdy.hud.app.ui.component.mainmenu.MusicMenu2$IndexOffset loadingOffset;
    private com.navdy.hud.app.ui.component.mainmenu.MusicMenu2$MusicMenuData menuData;
    private android.graphics.drawable.AnimationDrawable musicAnimation;
    @Inject
    com.navdy.hud.app.util.MusicArtworkCache musicArtworkCache;
    com.navdy.hud.app.storage.cache.MessageCache musicCollectionResponseMessageCache;
    private boolean musicCollectionSyncComplete;
    final private com.navdy.hud.app.manager.MusicManager musicManager;
    private int nextRequestLimit;
    private int nextRequestOffset;
    private int nowPlayingTrackPosition;
    private com.navdy.hud.app.ui.component.mainmenu.IMenu parentMenu;
    private boolean partialRefresh;
    private String path;
    private com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter presenter;
    private int requestMusicOffset;
    private java.util.Map requestedArtworkModelPositions;
    private java.util.List returnToCacheList;
    private java.util.Map trackIdToPositionMap;
    private com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vmenuComponent;
    
    static {
        logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.class);
        lastPlayedMusicMenuData = (java.util.Map)new java.util.HashMap();
        menuSourceStringMap = (java.util.Map)new java.util.HashMap();
        menuSourceIconMap = (java.util.Map)new java.util.HashMap();
        menuTypeStringMap = (java.util.Map)new java.util.HashMap();
        menuTypeIconMap = (java.util.Map)new java.util.HashMap();
        artworkRequestQueue = new java.util.Stack();
        infoForCurrentArtworkRequest = null;
        artworkRequestLock = new Object();
        analyticsTypeStringMap = (java.util.Map)new java.util.HashMap();
        resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        menuSourceStringMap.put(com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL, resources.getString(R.string.local_music));
        menuSourceStringMap.put(com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_IOS_MEDIA_PLAYER, resources.getString(R.string.music_library));
        menuSourceIconMap.put(com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL, Integer.valueOf(R.drawable.icon_google_pm));
        menuSourceIconMap.put(com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_IOS_MEDIA_PLAYER, Integer.valueOf(R.drawable.icon_apple_music));
        menuTypeStringMap.put(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ALBUMS, resources.getString(R.string.albums));
        menuTypeStringMap.put(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ARTISTS, resources.getString(R.string.artists));
        menuTypeStringMap.put(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PLAYLISTS, resources.getString(R.string.playlists));
        menuTypeStringMap.put(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PODCASTS, resources.getString(R.string.podcasts));
        menuTypeIconMap.put(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ALBUMS, Integer.valueOf(R.drawable.icon_album));
        menuTypeIconMap.put(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ARTISTS, Integer.valueOf(R.drawable.icon_artist));
        menuTypeIconMap.put(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PLAYLISTS, Integer.valueOf(R.drawable.icon_playlist));
        menuTypeIconMap.put(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PODCASTS, Integer.valueOf(R.drawable.icon_podcasts));
        mmMusicColor = resources.getColor(R.color.mm_music);
        sourceBgColor = resources.getColor(17170443);
        bgColorUnselected = resources.getColor(R.color.grey_4a);
        androidBgColor = resources.getColor(R.color.music_android_local);
        appleMusicBgColor = resources.getColor(R.color.music_apple_music);
        applePodcastsBgColor = resources.getColor(R.color.music_apple_podcasts);
        transparentColor = resources.getColor(17170445);
        artworkSize = resources.getDimensionPixelSize(R.dimen.vmenu_selected_image);
        backColor = resources.getColor(R.color.mm_back);
        back = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, backColor, bgColorUnselected, backColor, resources.getString(R.string.back), (String)null);
        shuffle = resources.getString(R.string.shuffle);
        shufflePlay = resources.getString(R.string.shuffle_play);
        shuffleOn = resources.getString(R.string.shuffle_on);
        shuffleOff = resources.getString(R.string.shuffle_off);
        analyticsTypeStringMap.put(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ALBUMS, "Album");
        analyticsTypeStringMap.put(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ARTISTS, "Artist");
        analyticsTypeStringMap.put(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PLAYLISTS, "Playlist");
        analyticsTypeStringMap.put(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PODCASTS, "Podcast");
    }
    
    MusicMenu2(com.squareup.otto.Bus a, com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a0, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter a1, com.navdy.hud.app.ui.component.mainmenu.IMenu a2, com.navdy.hud.app.ui.component.mainmenu.MusicMenu2$MusicMenuData a3) {
        this.path = null;
        this.highlightedItem = 1;
        this.isRequestingMusicCollection = false;
        this.musicCollectionSyncComplete = false;
        this.nextRequestOffset = -1;
        this.nextRequestLimit = -1;
        this.anyPlayersPermitted = true;
        this.requestMusicOffset = -1;
        this.loadingOffset = new com.navdy.hud.app.ui.component.mainmenu.MusicMenu2$IndexOffset((com.navdy.hud.app.ui.component.mainmenu.MusicMenu2$1)null);
        this.cachedList = null;
        this.returnToCacheList = null;
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.requestedArtworkModelPositions = (java.util.Map)new java.util.HashMap();
        this.artWorkRequestTime = 0L;
        this.trackIdToPositionMap = null;
        this.initialized = false;
        this.isShuffling = false;
        this.bus = a;
        this.vmenuComponent = a0;
        this.presenter = a1;
        this.parentMenu = a2;
        if (a3 == null) {
            this.menuData = new com.navdy.hud.app.ui.component.mainmenu.MusicMenu2$MusicMenuData(this);
        } else {
            this.menuData = a3;
        }
        mortar.Mortar.inject(com.navdy.hud.app.HudApplication.getAppContext(), this);
        logger.d(new StringBuilder().append("MusicMenu ").append(this.menuData.musicCollectionInfo).toString());
        this.musicManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getMusicManager();
    }
    
    static void access$100(com.navdy.hud.app.ui.component.mainmenu.MusicMenu2 a, com.navdy.service.library.events.audio.MusicCollectionResponse a0) {
        a.onMusicCollectionResponse(a0);
    }
    
    static com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter access$1000(com.navdy.hud.app.ui.component.mainmenu.MusicMenu2 a) {
        return a.presenter;
    }
    
    static com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent access$1100(com.navdy.hud.app.ui.component.mainmenu.MusicMenu2 a) {
        return a.vmenuComponent;
    }
    
    static com.navdy.hud.app.manager.MusicManager access$1200(com.navdy.hud.app.ui.component.mainmenu.MusicMenu2 a) {
        return a.musicManager;
    }
    
    static android.os.Handler access$1300(com.navdy.hud.app.ui.component.mainmenu.MusicMenu2 a) {
        return a.handler;
    }
    
    static java.util.List access$1400(com.navdy.hud.app.ui.component.mainmenu.MusicMenu2 a) {
        return a.cachedList;
    }
    
    static void access$200(com.navdy.hud.app.ui.component.mainmenu.MusicMenu2 a, com.navdy.service.library.events.audio.MusicArtworkResponse a0) {
        a.onMusicArtworkResponse(a0);
    }
    
    static void access$300(com.navdy.hud.app.ui.component.mainmenu.MusicMenu2 a, com.navdy.service.library.events.audio.MusicTrackInfo a0) {
        a.onTrackUpdated(a0);
    }
    
    static com.navdy.service.library.log.Logger access$400() {
        return logger;
    }
    
    static com.squareup.otto.Bus access$500(com.navdy.hud.app.ui.component.mainmenu.MusicMenu2 a) {
        return a.bus;
    }
    
    static com.navdy.hud.app.ui.component.mainmenu.MusicMenu2$MusicMenuData access$600(com.navdy.hud.app.ui.component.mainmenu.MusicMenu2 a) {
        return a.menuData;
    }
    
    static String access$700(com.navdy.hud.app.ui.component.mainmenu.MusicMenu2 a, com.navdy.service.library.events.audio.MusicCollectionInfo a0) {
        return a.collectionIdString(a0);
    }
    
    static void access$800(com.navdy.hud.app.ui.component.mainmenu.MusicMenu2 a, byte[] a0, Integer a1, String s, boolean b) {
        a.handleArtwork(a0, a1, s, b);
    }
    
    static void access$900(com.navdy.hud.app.ui.component.mainmenu.MusicMenu2 a, com.navdy.service.library.events.audio.MusicArtworkRequest a0, com.navdy.service.library.events.audio.MusicCollectionInfo a1) {
        a.postOrQueueArtworkRequest(a0, a1);
    }
    
    private com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex buildIndexFromCharacterMap(java.util.List a) {
        com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex a0 = null;
        try {
            com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex$Builder a1 = new com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex$Builder();
            Object a2 = a.iterator();
            while(((java.util.Iterator)a2).hasNext()) {
                com.navdy.service.library.events.audio.MusicCharacterMap a3 = (com.navdy.service.library.events.audio.MusicCharacterMap)((java.util.Iterator)a2).next();
                int i = a3.character.charAt(0);
                a1.setEntry((char)i, a3.offset.intValue());
            }
            a1.positionOffset(1);
            a0 = a1.build();
        } catch(Throwable a4) {
            logger.e("buildIndexFromCharacterMap", a4);
            a0 = null;
        }
        return a0;
    }
    
    private com.navdy.hud.app.ui.component.vlist.VerticalList$Model buildModelfromCollectionInfo(int i, com.navdy.service.library.events.audio.MusicCollectionInfo a) {
        String s = null;
        if (a.subtitle == null) {
            int i0 = (a.collectionType != com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PODCASTS) ? R.plurals.songs_count : R.plurals.episodes_count;
            android.content.res.Resources a0 = resources;
            int i1 = a.size.intValue();
            Object[] a1 = new Object[1];
            a1[0] = a.size;
            s = a0.getQuantityString(i0, i1, a1);
        } else {
            s = a.subtitle;
        }
        com.navdy.hud.app.ui.component.image.IconColorImageView$IconShape a2 = (a.collectionType != com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ARTISTS) ? com.navdy.hud.app.ui.component.image.IconColorImageView$IconShape.SQUARE : com.navdy.hud.app.ui.component.image.IconColorImageView$IconShape.CIRCLE;
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a3 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(i, ((Integer)menuTypeIconMap.get(a.collectionType)).intValue(), this.bgColorSelected, bgColorUnselected, transparentColor, a.name, s, (String)null, a2);
        a3.state = a;
        return a3;
    }
    
    private void calculateOffset(int i, int i0, boolean b) {
        int i1 = i / i0 * i0;
        int i2 = Math.min((b ? this.menuData.musicCollections.size() : this.menuData.musicTracks.size()) - 1, i1 + i0);
        this.loadingOffset.clear();
        this.loadingOffset.offset = i1;
        this.loadingOffset.limit = i2 - i1 + 1;
    }
    
    private com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex checkCharacterMap(java.util.List a, int i, int i0, boolean b) {
        com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex a0 = null;
        boolean b0 = a != null;
        int i1 = b0 ? a.size() : -1;
        label3: {
            label0: {
                label2: {
                    if (!b0) {
                        break label2;
                    }
                    if (!b) {
                        break label0;
                    }
                    label1: {
                        if (i <= 40) {
                            break label1;
                        }
                        if (i1 >= 2) {
                            break label0;
                        }
                    }
                    logger.v(new StringBuilder().append("checkCharacterMap index-no created len[").append(i).append("] index-len[").append(i1).append("]").toString());
                }
                a0 = null;
                break label3;
            }
            a0 = this.buildIndexFromCharacterMap(a);
            if (a0 == null) {
                logger.v(new StringBuilder().append("checkCharacterMap index-error len[").append(i).append("] map[").append(i1).append("]").toString());
            } else {
                logger.v(new StringBuilder().append("checkCharacterMap index-yes len[").append(i).append("] map[").append(i1).append("] index[").append(a0.getEntryCount()).append("]").toString());
            }
        }
        return a0;
    }
    
    public static void clearMenuData() {
        lastPlayedMusicMenuData.clear();
    }
    
    private String collectionIdString(com.navdy.service.library.events.audio.MusicArtworkResponse a) {
        return new StringBuilder().append((String)menuSourceStringMap.get(a.collectionSource)).append(" - ").append((String)menuTypeStringMap.get(a.collectionType)).append(" - ").append(a.collectionId).toString();
    }
    
    private String collectionIdString(com.navdy.service.library.events.audio.MusicCollectionInfo a) {
        String s = null;
        if (a != null) {
            com.navdy.service.library.events.audio.MusicCollectionInfo a0 = (com.navdy.service.library.events.audio.MusicCollectionInfo)com.navdy.service.library.events.MessageStore.removeNulls((com.squareup.wire.Message)a);
            s = new StringBuilder().append((String)menuSourceStringMap.get(a0.collectionSource)).append(" - ").append((String)menuTypeStringMap.get(a0.collectionType)).append(" - ").append(a0.collectionId).toString();
        } else {
            s = null;
        }
        return s;
    }
    
    private com.navdy.hud.app.ui.component.mainmenu.MusicMenu2 getMusicPlayerMenu(int i, int i0) {
        com.navdy.hud.app.ui.component.mainmenu.MusicMenu2$MusicMenuData a = null;
        logger.d(new StringBuilder().append("getMusicPlayerMenu ").append(i).append(", ").append(i0).toString());
        String s = this.getSubPath(i, i0);
        if (lastPlayedMusicMenuData.containsKey(s)) {
            a = (com.navdy.hud.app.ui.component.mainmenu.MusicMenu2$MusicMenuData)lastPlayedMusicMenuData.get(s);
        } else {
            com.navdy.service.library.events.audio.MusicCollectionInfo a0 = null;
            java.util.ArrayList a1 = null;
            if (this.menuData.musicCollectionInfo.collectionSource != null) {
                if (this.menuData.musicCollectionInfo.collectionType != null) {
                    int i1 = this.menuData.musicCollections.size();
                    a0 = null;
                    a1 = null;
                    if (i1 > 0) {
                        a0 = (com.navdy.service.library.events.audio.MusicCollectionInfo)this.menuData.musicCollections.get(i);
                        logger.i(new StringBuilder().append("fetching collection ").append(a0.collectionId).toString());
                        a1 = null;
                    }
                } else {
                    a0 = new com.navdy.service.library.events.audio.MusicCollectionInfo$Builder().collectionSource(this.menuData.musicCollectionInfo.collectionSource).collectionType(((com.navdy.service.library.events.audio.MusicCollectionInfo)this.menuData.musicCollections.get(i)).collectionType).build();
                    a1 = null;
                }
            } else if (i != R.id.apple_podcasts) {
                com.navdy.service.library.events.audio.MusicCollectionInfo a2 = (com.navdy.service.library.events.audio.MusicCollectionInfo)this.menuData.musicCollections.get(i);
                a0 = new com.navdy.service.library.events.audio.MusicCollectionInfo$Builder().collectionSource(a2.collectionSource).build();
                java.util.List a3 = this.musicManager.getCollectionTypesForSource(a2.collectionSource);
                a1 = new java.util.ArrayList(a3.size());
                Object a4 = a3.iterator();
                while(((java.util.Iterator)a4).hasNext()) {
                    com.navdy.service.library.events.audio.MusicCollectionType a5 = (com.navdy.service.library.events.audio.MusicCollectionType)((java.util.Iterator)a4).next();
                    ((java.util.List)a1).add(new com.navdy.service.library.events.audio.MusicCollectionInfo$Builder().collectionSource(a2.collectionSource).collectionType(a5).build());
                }
            } else {
                a0 = new com.navdy.service.library.events.audio.MusicCollectionInfo$Builder().collectionSource(com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_IOS_MEDIA_PLAYER).collectionType(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PODCASTS).build();
                a1 = null;
            }
            a = new com.navdy.hud.app.ui.component.mainmenu.MusicMenu2$MusicMenuData(this, a0, (java.util.List)a1);
        }
        com.navdy.hud.app.ui.component.mainmenu.MusicMenu2 a6 = new com.navdy.hud.app.ui.component.mainmenu.MusicMenu2(this.bus, this.vmenuComponent, this.presenter, (com.navdy.hud.app.ui.component.mainmenu.IMenu)this, a);
        a6.setBackSelectionPos(i0);
        a6.setBackSelectionId(i);
        return a6;
    }
    
    private com.navdy.hud.app.ui.component.vlist.VerticalList$Model getShuffleModel() {
        String s = null;
        String s0 = null;
        if (this.isThisQueuePlaying()) {
            s = shuffle;
            if (this.musicManager.isShuffling()) {
                s0 = shuffleOn;
                this.isShuffling = true;
            } else {
                s0 = shuffleOff;
            }
        } else {
            s = shufflePlay;
            s0 = null;
        }
        return com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.music_browser_shuffle, R.drawable.icon_shuffle, this.bgColorSelected, bgColorUnselected, this.bgColorSelected, s, s0);
    }
    
    private String getSubPath(int i, int i0) {
        return new StringBuilder().append(this.getPath()).append("/").append(i).append(":").append(i0).toString();
    }
    
    private void handleArtwork(byte[] a, Integer a0, String s, boolean b) {
        int i = artworkSize;
        android.graphics.Bitmap a1 = com.navdy.service.library.util.ScalingUtilities.decodeByteArray(a, i, i, com.navdy.service.library.util.ScalingUtilities$ScalingLogic.FIT);
        if (com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ARTISTS == this.menuData.musicCollectionInfo.collectionType) {
            a1 = new com.makeramen.RoundedTransformationBuilder().oval(true).build().transform(a1);
        }
        this.handler.post((Runnable)new com.navdy.hud.app.ui.component.mainmenu.MusicMenu2$7(this, s, a1, a0, b, a));
    }
    
    private boolean hasShuffleEntry() {
        boolean b = false;
        java.util.List a = this.cachedList;
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (this.cachedList.size() < 2) {
                        break label1;
                    }
                    if (((com.navdy.hud.app.ui.component.vlist.VerticalList$Model)this.cachedList.get(1)).id == R.id.music_browser_shuffle) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    private boolean isIndexSupported(com.navdy.service.library.events.audio.MusicCollectionInfo a) {
        boolean b = false;
        label0: {
            label1: {
                if (a == null) {
                    break label1;
                }
                if (a.collectionType == null) {
                    break label1;
                }
                switch(com.navdy.hud.app.ui.component.mainmenu.MusicMenu2$8.$SwitchMap$com$navdy$service$library$events$audio$MusicCollectionType[a.collectionType.ordinal()]) {
                    case 1: case 2: case 3: {
                        b = true;
                        break label0;
                    }
                }
            }
            b = false;
        }
        return b;
    }
    
    private boolean isThisQueuePlaying() {
        return android.text.TextUtils.equals((CharSequence)this.getPath(), (CharSequence)this.musicManager.getMusicMenuPath());
    }
    
    private void onMusicArtworkResponse(com.navdy.service.library.events.audio.MusicArtworkResponse a) {
        logger.d(new StringBuilder().append("REQUESTQUEUE onMusicArtworkResponse ").append(a).toString());
        String s = this.collectionIdString(a);
        Integer a0 = (Integer)this.requestedArtworkModelPositions.get(s);
        label1: {
            label0: {
                if (a0 != null) {
                    break label0;
                }
                if (android.text.TextUtils.equals((CharSequence)s, (CharSequence)this.collectionIdString(this.menuData.musicCollectionInfo))) {
                    break label0;
                }
                logger.w("Wrong menu");
                break label1;
            }
            long j = android.os.SystemClock.elapsedRealtime();
            long j0 = this.artWorkRequestTime;
            if (a.photo == null) {
                logger.w("No photo in artwork response");
            } else {
                logger.d(new StringBuilder().append("[Timing] MusicArtworkRequest, response received in ").append(j - j0).append(" MS, Size : ").append(a.photo.size()).toString());
                com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.ui.component.mainmenu.MusicMenu2$6(this, a, a0, s), 1);
            }
            synchronized(artworkRequestLock) {
                java.util.Stack a2 = artworkRequestQueue;
                if (a2.isEmpty()) {
                    logger.d("REQUESTQUEUE onMusicArtworkResponse queue empty");
                    infoForCurrentArtworkRequest = null;
                } else {
                    android.util.Pair a3 = (android.util.Pair)artworkRequestQueue.pop();
                    com.navdy.service.library.events.audio.MusicArtworkRequest a4 = (com.navdy.service.library.events.audio.MusicArtworkRequest)a3.first;
                    logger.d(new StringBuilder().append("REQUESTQUEUE onMusicArtworkResponse posting: ").append(a4).toString());
                    infoForCurrentArtworkRequest = (com.navdy.service.library.events.audio.MusicCollectionInfo)a3.second;
                    this.artWorkRequestTime = android.os.SystemClock.elapsedRealtime();
                    logger.d(new StringBuilder().append("[Timing] Making MusicArtWorkRequest ").append(a4).toString());
                    this.bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)a4));
                }
                /*monexit(a1)*/;
            }
        }
    }
    
    private void onMusicCollectionResponse(com.navdy.service.library.events.audio.MusicCollectionResponse a) {
        if (android.text.TextUtils.equals((CharSequence)this.collectionIdString(a.collectionInfo), (CharSequence)this.collectionIdString(this.menuData.musicCollectionInfo))) {
            if (this.requestMusicOffset != -1) {
                boolean b = false;
                com.navdy.hud.app.ui.component.vlist.VerticalList$Model[] a0 = null;
                long j = android.os.SystemClock.elapsedRealtime() - this.currentMusicCollectionRequestTime;
                if (this.musicManager.isMusicLibraryCachingEnabled()) {
                    String s = com.navdy.hud.app.ExtensionsKt.cacheKey(this.currentMusicCollectionRequest);
                    this.musicCollectionResponseMessageCache.put(s, (com.squareup.wire.Message)a);
                }
                if (this.menuData.musicCollectionInfo != null) {
                    com.navdy.service.library.events.audio.MusicCollectionInfo a1 = this.menuData.musicCollectionInfo;
                    com.navdy.service.library.events.audio.MusicCollectionInfo$Builder a2 = new com.navdy.service.library.events.audio.MusicCollectionInfo$Builder(a.collectionInfo);
                    if (android.text.TextUtils.isEmpty((CharSequence)a.collectionInfo.name)) {
                        a2.name(a1.name);
                    }
                    if (android.text.TextUtils.isEmpty((CharSequence)a.collectionInfo.subtitle)) {
                        a2.subtitle(a1.subtitle);
                    }
                    this.menuData.musicCollectionInfo = a2.build();
                } else {
                    this.menuData.musicCollectionInfo = a.collectionInfo;
                }
                java.util.List a3 = a.musicCollections;
                label5: {
                    label6: {
                        if (a3 == null) {
                            break label6;
                        }
                        if (a.musicCollections.size() <= 0) {
                            break label6;
                        }
                        if (this.menuData.musicCollections != null) {
                            b = false;
                        } else {
                            this.menuData.musicCollections = (java.util.List)new java.util.ArrayList();
                            b = true;
                        }
                        logger.d(new StringBuilder().append("[Timing] MusicCollectionRequest , received response  in : ").append(j).append(" MS, Collections : ").append(a.musicCollections.size()).toString());
                        if (b) {
                            this.menuData.musicCollections.addAll((java.util.Collection)a.musicCollections);
                            int i = this.menuData.musicCollectionInfo.size.intValue();
                            int i0 = a.musicCollections.size();
                            this.fastScrollIndex = this.checkCharacterMap(a.characterMap, i, i0, true);
                            if (this.fastScrollIndex != null) {
                                this.menuData.musicIndex = a.characterMap;
                            }
                            a0 = null;
                            if (i == i0) {
                                break label5;
                            }
                            int i1 = i - i0;
                            int i2 = 0;
                            while(i2 < i1) {
                                this.menuData.musicCollections.add(null);
                                i2 = i2 + 1;
                            }
                            logger.v(new StringBuilder().append("1-resp added[").append(i1).append("]").toString());
                            a0 = null;
                            break label5;
                        } else {
                            int i3 = this.menuData.musicCollectionInfo.size.intValue();
                            int i4 = a.musicCollections.size();
                            a0 = new com.navdy.hud.app.ui.component.vlist.VerticalList$Model[i4];
                            logger.v(new StringBuilder().append("2-resp offset[").append(this.requestMusicOffset).append("] total[").append(i3).append("] got[").append(i4).append("]").toString());
                            int i5 = 0;
                            while(i5 < i4) {
                                com.navdy.service.library.events.audio.MusicCollectionInfo a4 = (com.navdy.service.library.events.audio.MusicCollectionInfo)a.musicCollections.get(i5);
                                this.menuData.musicCollections.set(this.requestMusicOffset + i5, a4);
                                a0[i5] = this.buildModelfromCollectionInfo(this.requestMusicOffset + i5, a4);
                                i5 = i5 + 1;
                            }
                            break label5;
                        }
                    }
                    java.util.List a5 = a.musicTracks;
                    label3: {
                        label4: {
                            if (a5 == null) {
                                break label4;
                            }
                            if (a.musicTracks.size() > 0) {
                                break label3;
                            }
                        }
                        logger.w("No collections or tracks in this collection...");
                        this.musicCollectionSyncComplete = true;
                        a0 = null;
                        b = true;
                        break label5;
                    }
                    if (this.menuData.musicTracks != null) {
                        b = false;
                    } else {
                        this.menuData.musicTracks = (java.util.List)new java.util.ArrayList();
                        b = true;
                    }
                    logger.d(new StringBuilder().append("[Timing] MusicCollectionRequest , received response  in : ").append(j).append(" MS, Tracks : ").append(a.musicTracks.size()).toString());
                    if (b) {
                        this.menuData.musicTracks.addAll((java.util.Collection)a.musicTracks);
                        int i6 = this.menuData.musicCollectionInfo.size.intValue();
                        int i7 = this.menuData.musicTracks.size();
                        this.fastScrollIndex = this.checkCharacterMap(a.characterMap, i6, i7, true);
                        if (this.fastScrollIndex != null) {
                            this.menuData.musicIndex = a.characterMap;
                        }
                        a0 = null;
                        if (i6 != i7) {
                            int i8 = i6 - i7;
                            int i9 = 0;
                            while(i9 < i8) {
                                this.menuData.musicTracks.add(null);
                                i9 = i9 + 1;
                            }
                            logger.v(new StringBuilder().append("1-resp-track added[").append(i8).append("]").toString());
                            a0 = null;
                        }
                    } else {
                        int i10 = this.menuData.musicCollectionInfo.size.intValue();
                        int i11 = a.musicTracks.size();
                        a0 = new com.navdy.hud.app.ui.component.vlist.VerticalList$Model[i11];
                        logger.v(new StringBuilder().append("2-resp-track offset[").append(this.requestMusicOffset).append("] total[").append(i10).append("]  got[").append(i11).append("]").toString());
                        int i12 = 0;
                        while(i12 < i11) {
                            com.navdy.service.library.events.audio.MusicTrackInfo a6 = (com.navdy.service.library.events.audio.MusicTrackInfo)a.musicTracks.get(i12);
                            this.menuData.musicTracks.set(this.requestMusicOffset + i12, a6);
                            com.navdy.hud.app.ui.component.vlist.VerticalList$Model a7 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(this.requestMusicOffset + i12, R.drawable.icon_song, this.bgColorSelected, bgColorUnselected, this.bgColorSelected, a6.name, a6.author);
                            a7.state = a6;
                            a0[i12] = a7;
                            i12 = i12 + 1;
                        }
                    }
                }
                label2: {
                    label0: {
                        label1: {
                            if (b) {
                                break label1;
                            }
                            if (a0 != null) {
                                break label0;
                            }
                        }
                        this.presenter.cancelLoadingAnimation(1);
                        this.highlightedItem = this.vmenuComponent.verticalList.getCurrentPosition();
                        this.presenter.updateCurrentMenu((com.navdy.hud.app.ui.component.mainmenu.IMenu)this);
                        break label2;
                    }
                    int i13 = this.requestMusicOffset + 1;
                    if (this.hasShuffleEntry()) {
                        i13 = i13 + 1;
                    }
                    logger.v(new StringBuilder().append("updatemodels pos=").append(i13).append(" len=").append(a0.length).toString());
                    this.partialRefresh = true;
                    if (this.cachedList != null) {
                        int i14 = this.cachedList.size();
                        int i15 = 0;
                        while(i15 < a0.length) {
                            int i16 = i13 + i15;
                            {
                                if (i16 < i14) {
                                    this.cachedList.set(i16, a0[i15]);
                                    i15 = i15 + 1;
                                    continue;
                                }
                                logger.v(new StringBuilder().append("invalid index:").append(i16).append(" size:").append(i14).toString());
                                break;
                            }
                        }
                    }
                    this.presenter.refreshData(i13, a0, (com.navdy.hud.app.ui.component.mainmenu.IMenu)this);
                }
                this.requestMusicOffset = -1;
                this.isRequestingMusicCollection = false;
                if (this.nextRequestOffset != -1) {
                    if ((this.menuData.musicCollections == null) ? this.menuData.musicTracks != null && this.menuData.musicTracks.get(this.nextRequestOffset) == null : this.menuData.musicCollections.get(this.nextRequestOffset) == null) {
                        this.requestMusicCollection(this.nextRequestOffset, this.nextRequestLimit, false);
                        logger.v(new StringBuilder().append("pending request:").append(this.nextRequestLimit).append(" limit=").append(this.nextRequestLimit).toString());
                    }
                    this.nextRequestOffset = -1;
                    this.nextRequestLimit = -1;
                }
            } else {
                logger.w("no outstanding request");
            }
        } else {
            logger.w("wrong collection");
        }
    }
    
    private void onTrackUpdated(com.navdy.service.library.events.audio.MusicTrackInfo a) {
        if (this.isThisQueuePlaying()) {
            String s = a.trackId;
            if (this.trackIdToPositionMap != null && s != null && this.trackIdToPositionMap.containsKey(s)) {
                Integer a0 = (Integer)this.trackIdToPositionMap.get(s);
                if (this.nowPlayingTrackPosition > 0 && this.nowPlayingTrackPosition != a0.intValue()) {
                    this.stopAudioAnimation();
                    this.presenter.refreshDataforPos(this.nowPlayingTrackPosition);
                }
                this.presenter.refreshDataforPos(a0.intValue());
                this.nowPlayingTrackPosition = a0.intValue();
            }
            boolean b = this.musicManager.isShuffling();
            if (this.isShuffling != b) {
                this.isShuffling = b;
                this.vmenuComponent.verticalList.refreshData(1, this.getShuffleModel());
            }
        } else {
            logger.w("This queue isn't playing");
        }
    }
    
    private void postOrQueueArtworkRequest(com.navdy.service.library.events.audio.MusicArtworkRequest a, com.navdy.service.library.events.audio.MusicCollectionInfo a0) {
        synchronized(artworkRequestLock) {
            java.util.Stack a2 = artworkRequestQueue;
            boolean b = a2.isEmpty();
            label1: {
                label0: {
                    if (!b) {
                        break label0;
                    }
                    if (infoForCurrentArtworkRequest != null) {
                        break label0;
                    }
                    logger.d(new StringBuilder().append("REQUESTQUEUE postOrQueueArtworkRequest posting: ").append(a).append(" title:").append(a0.name).toString());
                    infoForCurrentArtworkRequest = a0;
                    this.artWorkRequestTime = android.os.SystemClock.elapsedRealtime();
                    logger.d(new StringBuilder().append("[Timing] Making MusicArtWorkRequest ").append(a).toString());
                    this.bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)a));
                    break label1;
                }
                logger.d(new StringBuilder().append("REQUESTQUEUE postOrQueueArtworkRequest queueing: ").append(a).append(" title:").append(a0.name).toString());
                artworkRequestQueue.push(new android.util.Pair(a, a0));
            }
            /*monexit(a1)*/;
        }
    }
    
    private void purgeArtworkQueue() {
        synchronized(artworkRequestLock) {
            java.util.Stack a0 = artworkRequestQueue;
            int i = a0.size();
            artworkRequestQueue.clear();
            logger.d(new StringBuilder().append("REQUESTQUEUE purged: ").append(i).toString());
            /*monexit(a)*/;
        }
    }
    
    private void requestMusicCollection(int i, int i0, boolean b) {
        logger.d(new StringBuilder().append("requestMusicCollection Offset : ").append(i).append(", Limit : ").append(i0).append(", AFI : ").append(b).toString());
        if (this.musicCollectionSyncComplete) {
            logger.i("sync is complete");
        } else if (this.isRequestingMusicCollection) {
            logger.i(new StringBuilder().append("Request already in process next=").append(i).append(" limit:").append(i0).toString());
            this.nextRequestOffset = i;
            this.nextRequestLimit = i0;
        } else {
            this.nextRequestOffset = -1;
            this.nextRequestLimit = -1;
            this.isRequestingMusicCollection = true;
            this.requestMusicOffset = i;
            if (i0 == -1) {
                i0 = 25;
            }
            logger.i(new StringBuilder().append("request music =").append(i).append(" limit:").append(i0).toString());
            if (this.menuData.musicCollections == null && this.menuData.musicTracks == null) {
                i0 = 50;
            }
            com.navdy.service.library.events.audio.MusicCollectionRequest$Builder a = new com.navdy.service.library.events.audio.MusicCollectionRequest$Builder().collectionSource(this.menuData.musicCollectionInfo.collectionSource).collectionType(this.menuData.musicCollectionInfo.collectionType).collectionId(this.menuData.musicCollectionInfo.collectionId).offset(Integer.valueOf(i)).limit(Integer.valueOf(i0));
            if (this.menuData.musicCollectionInfo.collectionType == com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ARTISTS) {
                a.groupBy(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ALBUMS);
            }
            if (b) {
                a.includeCharacterMap(Boolean.valueOf(true));
            }
            com.navdy.service.library.events.audio.MusicCollectionRequest a0 = a.build();
            this.currentMusicCollectionRequest = a0;
            this.currentMusicCollectionRequestTime = android.os.SystemClock.elapsedRealtime();
            logger.i(new StringBuilder().append("got-asked offset=").append(i).append(" limit=").append(i0).toString());
            logger.d(new StringBuilder().append("[Timing] MusicCollectionRequest ").append(a0).toString());
            if (this.musicManager.isMusicLibraryCachingEnabled()) {
                com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.ui.component.mainmenu.MusicMenu2$1(this, a0), 1);
            } else {
                this.bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)a0));
            }
        }
    }
    
    private void setupIconBgColor() {
        if (this.menuData.musicCollectionInfo != null) {
            if (this.menuData.musicCollectionInfo.collectionSource != com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL) {
                if (this.menuData.musicCollectionInfo.collectionType != com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PODCASTS) {
                    this.bgColorSelected = appleMusicBgColor;
                } else {
                    this.bgColorSelected = applePodcastsBgColor;
                }
            } else {
                this.bgColorSelected = androidBgColor;
            }
        }
    }
    
    private void setupMenu() {
        if (busDelegate == null) {
            logger.v("bus-register ctor");
            busDelegate = new com.navdy.hud.app.ui.component.mainmenu.MusicMenu2$BusDelegate(this.presenter);
            this.bus.register(busDelegate);
            this.presenter.setScrollIdleEvents(true);
            this.musicManager.addMusicUpdateListener((com.navdy.hud.app.manager.MusicManager$MusicUpdateListener)busDelegate);
        }
        if (!this.initialized) {
            this.initialized = true;
            if (this.musicManager.hasMusicCapabilities()) {
                label2: if (this.menuData.musicCollectionInfo == null) {
                    java.util.List a = this.musicManager.getMusicCapabilities().capabilities;
                    this.anyPlayersPermitted = false;
                    java.util.ArrayList a0 = new java.util.ArrayList(a.size());
                    java.util.Iterator a1 = a.iterator();
                    Object a2 = a;
                    Object a3 = a1;
                    while(((java.util.Iterator)a3).hasNext()) {
                        com.navdy.service.library.events.audio.MusicCapability a4 = (com.navdy.service.library.events.audio.MusicCapability)((java.util.Iterator)a3).next();
                        if (a4.authorizationStatus == com.navdy.service.library.events.audio.MusicCapability$MusicAuthorizationStatus.MUSIC_AUTHORIZATION_AUTHORIZED) {
                            this.anyPlayersPermitted = true;
                            ((java.util.List)a0).add(a4.collectionSource);
                        }
                    }
                    int i = ((java.util.List)a2).size();
                    label0: {
                        label1: {
                            if (i != 1) {
                                break label1;
                            }
                            if (((com.navdy.service.library.events.audio.MusicCapability)((java.util.List)a2).get(0)).collectionSource == com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL) {
                                break label0;
                            }
                        }
                        this.menuData.musicCollectionInfo = new com.navdy.service.library.events.audio.MusicCollectionInfo$Builder().build();
                        this.menuData.musicCollections = (java.util.List)new java.util.ArrayList(((java.util.List)a0).size());
                        Object a5 = ((java.util.List)a0).iterator();
                        while(((java.util.Iterator)a5).hasNext()) {
                            com.navdy.service.library.events.audio.MusicCollectionSource a6 = (com.navdy.service.library.events.audio.MusicCollectionSource)((java.util.Iterator)a5).next();
                            this.menuData.musicCollections.add(new com.navdy.service.library.events.audio.MusicCollectionInfo$Builder().collectionSource(a6).build());
                        }
                        break label2;
                    }
                    this.menuData.musicCollectionInfo = new com.navdy.service.library.events.audio.MusicCollectionInfo$Builder().collectionSource(com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL).build();
                    java.util.List a7 = this.musicManager.getCollectionTypesForSource(com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL);
                    this.menuData.musicCollections = (java.util.List)new java.util.ArrayList(a7.size());
                    Object a8 = a7.iterator();
                    while(((java.util.Iterator)a8).hasNext()) {
                        com.navdy.service.library.events.audio.MusicCollectionType a9 = (com.navdy.service.library.events.audio.MusicCollectionType)((java.util.Iterator)a8).next();
                        this.menuData.musicCollections.add(new com.navdy.service.library.events.audio.MusicCollectionInfo$Builder().collectionSource(com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL).collectionType(a9).build());
                    }
                }
                this.setupIconBgColor();
            } else {
                logger.e("Navigated to Music menu with no music capabilities");
            }
        }
    }
    
    private void showMusicPlayer() {
        logger.i("showMusicPlayer");
        com.navdy.hud.app.ui.framework.UIStateManager a = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager();
        a.addScreenAnimationListener((com.navdy.hud.app.ui.framework.IScreenAnimationListener)new com.navdy.hud.app.ui.component.mainmenu.MusicMenu2$4(this, a));
        this.presenter.close();
        com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.clearMenuData();
        this.storeMenuData();
        this.musicManager.setMusicMenuPath(this.getPath());
    }
    
    private void startAudioAnimation(com.navdy.hud.app.ui.component.image.IconColorImageView a) {
        a.animate().cancel();
        a.setImageResource(R.drawable.audio_loop);
        android.graphics.drawable.AnimationDrawable a0 = (android.graphics.drawable.AnimationDrawable)a.getDrawable();
        a0.start();
        this.musicAnimation = a0;
        logger.v("startAudioAnimation");
    }
    
    private void stopAudioAnimation() {
        if (this.musicAnimation != null && this.musicAnimation.isRunning()) {
            this.musicAnimation.stop();
            logger.v("stopAudioAnimation");
        }
        this.musicAnimation = null;
    }
    
    private void storeMenuData() {
        if (this.parentMenu.getType() == com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.MUSIC) {
            ((com.navdy.hud.app.ui.component.mainmenu.MusicMenu2)this.parentMenu).storeMenuData();
        }
        lastPlayedMusicMenuData.put(this.getPath(), this.menuData);
    }
    
    public com.navdy.hud.app.ui.component.mainmenu.IMenu getChildMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu a, String s, String s0) {
        Object a0 = null;
        logger.v(new StringBuilder().append("getChildMenu:").append(s).append(",").append(s0).toString());
        if (this.musicManager.hasMusicCapabilities()) {
            String[] a1 = s.split(":");
            com.navdy.hud.app.ui.component.mainmenu.MusicMenu2 a2 = this.getMusicPlayerMenu(Integer.valueOf(a1[0]).intValue(), Integer.valueOf(a1[1]).intValue());
            String s1 = null;
            if (s0 != null) {
                if (s0.indexOf("/") != 0) {
                    s0 = null;
                    s1 = null;
                } else {
                    s1 = s0.substring(1);
                    int i = s1.indexOf("/");
                    if (i < 0) {
                        s0 = null;
                    } else {
                        String s2 = s0.substring(i + 1);
                        s1 = s1.substring(0, i);
                        s0 = s2;
                    }
                }
            }
            a0 = (android.text.TextUtils.isEmpty((CharSequence)s1)) ? a2 : a2.getChildMenu((com.navdy.hud.app.ui.component.mainmenu.IMenu)this, s1, s0);
        } else {
            logger.e("No music capabilities...");
            a0 = null;
        }
        return (com.navdy.hud.app.ui.component.mainmenu.IMenu)a0;
    }
    
    public int getInitialSelection() {
        logger.i(new StringBuilder().append("getInitialSelection ").append(this.highlightedItem).toString());
        return this.highlightedItem;
    }
    
    public java.util.List getItems() {
        logger.v(new StringBuilder().append("getItems ").append(this.getPath()).toString());
        this.setupMenu();
        java.util.ArrayList a = new java.util.ArrayList();
        this.returnToCacheList = (java.util.List)new java.util.ArrayList();
        label8: if (this.musicManager.hasMusicCapabilities()) {
            if (this.anyPlayersPermitted) {
                Integer a0 = this.menuData.musicCollectionInfo.size;
                label7: {
                    if (a0 == null) {
                        break label7;
                    }
                    if (this.menuData.musicCollectionInfo.size.intValue() != 0) {
                        break label7;
                    }
                    android.content.res.Resources a1 = resources;
                    Object[] a2 = new Object[1];
                    a2[0] = menuTypeStringMap.get(this.menuData.musicCollectionInfo.collectionType);
                    String s = a1.getString(R.string.empty_queue, a2);
                    com.navdy.hud.app.ui.component.vlist.VerticalList$Model a3 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, backColor, bgColorUnselected, backColor, resources.getString(R.string.back), s);
                    ((java.util.List)a).add(a3);
                    this.returnToCacheList.add(a3);
                    break label8;
                }
                ((java.util.List)a).add(back);
                label4: if (this.menuData.musicCollectionInfo.collectionSource != null) {
                    if (this.menuData.musicCollectionInfo.collectionType != null) {
                        int i = 0;
                        java.util.List a4 = this.menuData.musicCollections;
                        label6: {
                            if (a4 != null) {
                                break label6;
                            }
                            if (this.menuData.musicTracks != null) {
                                break label6;
                            }
                            ((java.util.List)a).add(com.navdy.hud.app.ui.component.vlist.viewholder.LoadingViewHolder.buildModel());
                            this.requestMusicCollection(0, -1, this.isIndexSupported(this.menuData.musicCollectionInfo));
                            break label4;
                        }
                        java.util.List a5 = this.menuData.musicCollections;
                        label5: {
                            if (a5 == null) {
                                break label5;
                            }
                            if (this.menuData.musicCollections.size() <= 0) {
                                break label5;
                            }
                            logger.i(new StringBuilder().append("Loading music collections for type ").append(this.backSelection).toString());
                            if (this.menuData.musicIndex != null && this.fastScrollIndex == null) {
                                this.fastScrollIndex = this.checkCharacterMap(this.menuData.musicIndex, 0, 0, false);
                            }
                            com.navdy.service.library.events.audio.MusicCollectionInfo a6 = this.menuData.musicCollectionInfo;
                            Integer a7 = null;
                            if (a6 != null) {
                                com.navdy.service.library.events.audio.MusicCollectionType a8 = this.menuData.musicCollectionInfo.collectionType;
                                a7 = null;
                                if (a8 != null) {
                                    a7 = (Integer)menuTypeIconMap.get(this.menuData.musicCollectionInfo.collectionType);
                                }
                            }
                            if (a7 == null) {
                                a7 = Integer.valueOf(0);
                            }
                            if (this.menuData.musicCollectionInfo.collectionType == com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ARTISTS && !android.text.TextUtils.isEmpty((CharSequence)this.menuData.musicCollectionInfo.collectionId) && ((Boolean)com.squareup.wire.Wire.get(this.menuData.musicCollectionInfo.canShuffle, Boolean.valueOf(false))).booleanValue()) {
                                ((java.util.List)a).add(this.getShuffleModel());
                                this.highlightedItem = 2;
                            }
                            int i0 = 0;
                            while(i0 < this.menuData.musicCollections.size()) {
                                com.navdy.service.library.events.audio.MusicCollectionInfo a9 = (com.navdy.service.library.events.audio.MusicCollectionInfo)this.menuData.musicCollections.get(i0);
                                if (a9 != null) {
                                    com.navdy.hud.app.ui.component.vlist.VerticalList$Model a10 = this.buildModelfromCollectionInfo(i0, a9);
                                    ((java.util.List)a).add(a10);
                                    this.returnToCacheList.add(a10);
                                } else {
                                    com.navdy.hud.app.ui.component.vlist.VerticalList$Model a11 = com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder.buildModel(a7.intValue(), this.bgColorSelected, bgColorUnselected);
                                    ((java.util.List)a).add(a11);
                                    this.returnToCacheList.add(a11);
                                }
                                i0 = i0 + 1;
                            }
                            break label4;
                        }
                        java.util.List a12 = this.menuData.musicTracks;
                        label2: {
                            label3: {
                                if (a12 == null) {
                                    break label3;
                                }
                                if (this.menuData.musicTracks.size() > 0) {
                                    break label2;
                                }
                            }
                            logger.e("No collections or tracks in this collection...");
                            break label4;
                        }
                        if (this.trackIdToPositionMap == null) {
                            this.trackIdToPositionMap = (java.util.Map)new java.util.HashMap();
                        }
                        if (this.menuData.musicCollectionInfo.collectionType != com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PODCASTS) {
                            if (this.menuData.musicTracks.size() <= 1) {
                                i = R.drawable.icon_song;
                            } else {
                                ((java.util.List)a).add(this.getShuffleModel());
                                if (this.menuData.musicCollectionInfo.collectionType == com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PLAYLISTS) {
                                    i = R.drawable.icon_song;
                                } else {
                                    this.highlightedItem = 2;
                                    i = R.drawable.icon_song;
                                }
                            }
                        } else {
                            i = R.drawable.icon_play_music;
                        }
                        int i1 = 0;
                        while(i1 < this.menuData.musicTracks.size()) {
                            com.navdy.service.library.events.audio.MusicTrackInfo a13 = (com.navdy.service.library.events.audio.MusicTrackInfo)this.menuData.musicTracks.get(i1);
                            if (a13 != null) {
                                com.navdy.hud.app.ui.component.vlist.VerticalList$Model a14 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(i1, i, this.bgColorSelected, bgColorUnselected, this.bgColorSelected, a13.name, a13.author);
                                a14.state = a13;
                                ((java.util.List)a).add(a14);
                                this.returnToCacheList.add(a14);
                                this.trackIdToPositionMap.put(a13.trackId, Integer.valueOf(((java.util.List)a).size() - 1));
                            } else {
                                com.navdy.hud.app.ui.component.vlist.VerticalList$Model a15 = com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder.buildModel(i, this.bgColorSelected, bgColorUnselected);
                                ((java.util.List)a).add(a15);
                                this.returnToCacheList.add(a15);
                            }
                            i1 = i1 + 1;
                        }
                        if (this.menuData.musicTracks.size() < this.menuData.musicCollectionInfo.size.intValue()) {
                            ((java.util.List)a).add(com.navdy.hud.app.ui.component.vlist.viewholder.LoadingViewHolder.buildModel());
                        }
                    } else {
                        int i2 = 0;
                        while(i2 < this.menuData.musicCollections.size()) {
                            com.navdy.service.library.events.audio.MusicCollectionType a16 = ((com.navdy.service.library.events.audio.MusicCollectionInfo)this.menuData.musicCollections.get(i2)).collectionType;
                            com.navdy.service.library.events.audio.MusicCollectionSource a17 = this.menuData.musicCollectionInfo.collectionSource;
                            com.navdy.service.library.events.audio.MusicCollectionSource a18 = com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_IOS_MEDIA_PLAYER;
                            label0: {
                                label1: {
                                    if (a17 != a18) {
                                        break label1;
                                    }
                                    if (a16 == com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PODCASTS) {
                                        break label0;
                                    }
                                }
                                com.navdy.hud.app.ui.component.vlist.VerticalList$Model a19 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(i2, ((Integer)menuTypeIconMap.get(a16)).intValue(), this.bgColorSelected, bgColorUnselected, this.bgColorSelected, (String)menuTypeStringMap.get(a16), (String)null);
                                a19.state = a16;
                                ((java.util.List)a).add(a19);
                                this.returnToCacheList.add(a19);
                            }
                            i2 = i2 + 1;
                        }
                    }
                } else {
                    if (busDelegate == null && this.parentMenu.getType() == com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.MAIN) {
                        logger.v("bus-register getItems");
                        busDelegate = new com.navdy.hud.app.ui.component.mainmenu.MusicMenu2$BusDelegate(this.presenter);
                        this.bus.register(busDelegate);
                        this.presenter.setScrollIdleEvents(true);
                    }
                    int i3 = 0;
                    while(i3 < this.menuData.musicCollections.size()) {
                        com.navdy.service.library.events.audio.MusicCollectionInfo a20 = (com.navdy.service.library.events.audio.MusicCollectionInfo)this.menuData.musicCollections.get(i3);
                        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a21 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(i3, ((Integer)menuSourceIconMap.get(a20.collectionSource)).intValue(), sourceBgColor, bgColorUnselected, sourceBgColor, (String)menuSourceStringMap.get(a20.collectionSource), (String)null);
                        a21.state = a20;
                        ((java.util.List)a).add(a21);
                        this.returnToCacheList.add(a21);
                        java.util.List a22 = this.musicManager.getCollectionTypesForSource(a20.collectionSource);
                        if (a20.collectionSource == com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_IOS_MEDIA_PLAYER && a22.contains(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PODCASTS)) {
                            com.navdy.hud.app.ui.component.vlist.VerticalList$Model a23 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.apple_podcasts, ((Integer)menuTypeIconMap.get(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PODCASTS)).intValue(), applePodcastsBgColor, bgColorUnselected, applePodcastsBgColor, resources.getString(R.string.apple_podcasts), (String)null);
                            a23.state = com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PODCASTS;
                            ((java.util.List)a).add(a23);
                            this.returnToCacheList.add(a23);
                        }
                        i3 = i3 + 1;
                    }
                }
                this.cachedList = (java.util.List)a;
            } else {
                String s0 = null;
                String s1 = null;
                if (com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getRemoteDevicePlatform() != com.navdy.service.library.events.DeviceInfo$Platform.PLATFORM_iOS) {
                    s0 = resources.getString(R.string.music_enable_permissions_title_android);
                    s1 = resources.getString(R.string.music_enable_permissions_subtitle_android);
                } else {
                    s0 = resources.getString(R.string.music_enable_permissions_title_ios);
                    s1 = resources.getString(R.string.music_enable_permissions_subtitle_ios);
                }
                com.navdy.hud.app.ui.component.vlist.VerticalList$Model a24 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, backColor, bgColorUnselected, backColor, s0, s1);
                ((java.util.List)a).add(a24);
                this.returnToCacheList.add(a24);
            }
        } else {
            logger.e("Navigated to Music menu with no music capabilities");
            ((java.util.List)a).add(back);
        }
        return (java.util.List)a;
    }
    
    public com.navdy.hud.app.ui.component.vlist.VerticalList$Model getModelfromPos(int i) {
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a = null;
        java.util.List a0 = this.cachedList;
        label2: {
            label0: {
                label1: {
                    if (a0 == null) {
                        break label1;
                    }
                    if (this.cachedList.size() > i) {
                        break label0;
                    }
                }
                a = null;
                break label2;
            }
            a = (com.navdy.hud.app.ui.component.vlist.VerticalList$Model)this.cachedList.get(i);
        }
        return a;
    }
    
    public String getPath() {
        if (this.path == null) {
            if (this.parentMenu.getType() != com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.MUSIC) {
                this.path = new StringBuilder().append("/").append(com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.MUSIC.name()).toString();
            } else {
                this.path = new StringBuilder().append(((com.navdy.hud.app.ui.component.mainmenu.MusicMenu2)this.parentMenu).getPath()).append("/").append(this.backSelectionId).append(":").append(this.backSelection).toString();
            }
        }
        return this.path;
    }
    
    public com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex getScrollIndex() {
        return this.fastScrollIndex;
    }
    
    public com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu getType() {
        return com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.MUSIC;
    }
    
    public boolean isBindCallsEnabled() {
        return true;
    }
    
    public boolean isFirstItemEmpty() {
        return true;
    }
    
    public boolean isItemClickable(int i, int i0) {
        return true;
    }
    
    public void onBindToView(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, android.view.View a0, int i, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState a1) {
        if (this.partialRefresh && i == this.vmenuComponent.verticalList.getRawPosition()) {
            this.partialRefresh = false;
            logger.v(new StringBuilder().append("partial refresh:").append(i).toString());
            this.vmenuComponent.verticalList.setViewHolderState(i, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.SELECTED, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.NONE, 0);
        }
        logger.v(new StringBuilder().append("onBindToView:").append(i).append(" current=").append(this.vmenuComponent.verticalList.getCurrentPosition()).toString());
        label2: if (a.state instanceof com.navdy.service.library.events.audio.MusicTrackInfo) {
            com.navdy.hud.app.ui.component.image.IconColorImageView a2 = (com.navdy.hud.app.ui.component.image.IconColorImageView)com.navdy.hud.app.ui.component.vlist.VerticalList.findImageView(a0);
            com.navdy.hud.app.ui.component.image.IconColorImageView a3 = (com.navdy.hud.app.ui.component.image.IconColorImageView)com.navdy.hud.app.ui.component.vlist.VerticalList.findSmallImageView(a0);
            com.navdy.service.library.events.audio.MusicTrackInfo a4 = (com.navdy.service.library.events.audio.MusicTrackInfo)a.state;
            com.navdy.service.library.events.audio.MusicTrackInfo a5 = this.musicManager.getCurrentTrack();
            label0: {
                label1: {
                    if (a5 == null) {
                        break label1;
                    }
                    if (android.text.TextUtils.equals((CharSequence)a5.trackId, (CharSequence)a4.trackId)) {
                        break label0;
                    }
                }
                a1.updateImage = true;
                a1.updateSmallImage = true;
                break label2;
            }
            a1.updateImage = false;
            a1.updateSmallImage = false;
            if (com.navdy.hud.app.manager.MusicManager.tryingToPlay(a5.playbackState)) {
                this.startAudioAnimation(a2);
            } else {
                a2.setImageResource(R.drawable.audio_seq_32_sm);
                this.stopAudioAnimation();
            }
            a3.setImageResource(R.drawable.audio_seq_32_sm);
        } else if (a.state instanceof com.navdy.service.library.events.audio.MusicCollectionInfo) {
            com.navdy.service.library.events.audio.MusicCollectionInfo a6 = (com.navdy.service.library.events.audio.MusicCollectionInfo)a.state;
            if (!android.text.TextUtils.isEmpty((CharSequence)a6.collectionId)) {
                com.navdy.hud.app.ui.component.image.IconColorImageView a7 = (com.navdy.hud.app.ui.component.image.IconColorImageView)com.navdy.hud.app.ui.component.vlist.VerticalList.findImageView(a0);
                com.navdy.hud.app.ui.component.image.IconColorImageView a8 = (com.navdy.hud.app.ui.component.image.IconColorImageView)com.navdy.hud.app.ui.component.vlist.VerticalList.findSmallImageView(a0);
                a7.setTag(null);
                String s = this.collectionIdString(a6);
                android.graphics.Bitmap a9 = com.navdy.hud.app.util.picasso.PicassoUtil.getBitmapfromCache(s);
                if (a9 == null) {
                    a7.setImageBitmap((android.graphics.Bitmap)null);
                    a8.setImageBitmap((android.graphics.Bitmap)null);
                    if (this.requestedArtworkModelPositions.get(s) == null) {
                        this.requestedArtworkModelPositions.put(s, Integer.valueOf(i));
                        logger.i(new StringBuilder().append("Checking cache for artwork for collection: ").append(a6).toString());
                        this.musicArtworkCache.getArtwork(a6, (com.navdy.hud.app.util.MusicArtworkCache$Callback)new com.navdy.hud.app.ui.component.mainmenu.MusicMenu2$5(this, i, a6));
                    } else {
                        logger.i(new StringBuilder().append("Already requested artwork for collection: ").append(s).toString());
                    }
                } else {
                    a7.setImageBitmap(a9);
                    a8.setImageBitmap(a9);
                    a1.updateImage = false;
                    a1.updateSmallImage = false;
                }
            }
        } else {
            a1.updateImage = true;
            a1.updateSmallImage = true;
        }
    }
    
    public void onFastScrollEnd() {
        logger.v("REQUESTQUEUE onFastScrollEnd");
        this.purgeArtworkQueue();
    }
    
    public void onFastScrollStart() {
    }
    
    public void onItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
    }
    
    public void onScrollIdle() {
        label0: if (!this.musicCollectionSyncComplete) {
            boolean b = false;
            boolean b0 = false;
            java.util.List a = this.menuData.musicCollections;
            label7: {
                label5: {
                    label6: {
                        if (a == null) {
                            break label6;
                        }
                        if (this.menuData.musicCollections.size() > 0) {
                            break label5;
                        }
                    }
                    b = false;
                    break label7;
                }
                b = true;
            }
            java.util.List a0 = this.menuData.musicTracks;
            label4: {
                label2: {
                    label3: {
                        if (a0 == null) {
                            break label3;
                        }
                        if (this.menuData.musicTracks.size() > 0) {
                            break label2;
                        }
                    }
                    b0 = false;
                    break label4;
                }
                b0 = true;
            }
            if (this.menuData.musicCollectionInfo != null && this.menuData.musicCollectionInfo.collectionType != null && this.cachedList != null) {
                label1: {
                    if (b) {
                        break label1;
                    }
                    if (!b0) {
                        break label0;
                    }
                }
                int i = this.vmenuComponent.verticalList.getCurrentPosition() - 1;
                if (this.hasShuffleEntry()) {
                    i = i - 1;
                }
                if (i < 0) {
                    i = 0;
                }
                int i0 = b ? this.menuData.musicCollections.size() : this.menuData.musicTracks.size();
                if (i < i0) {
                    com.squareup.wire.Message a1 = b ? (com.squareup.wire.Message)this.menuData.musicCollections.get(i) : (com.squareup.wire.Message)this.menuData.musicTracks.get(i);
                    if (a1 != null) {
                        int i1 = i - 1;
                        if (i1 >= 0) {
                            a1 = b ? (com.squareup.wire.Message)this.menuData.musicCollections.get(i1) : (com.squareup.wire.Message)this.menuData.musicTracks.get(i1);
                            if (a1 == null) {
                                i = i - 1;
                            }
                        }
                        if (a1 != null) {
                            int i2 = i + 1;
                            if (i2 <= i0 - 1) {
                                a1 = b ? (com.squareup.wire.Message)this.menuData.musicCollections.get(i2) : (com.squareup.wire.Message)this.menuData.musicTracks.get(i2);
                            }
                            if (a1 == null) {
                                i = i + 1;
                            }
                        }
                    }
                    if (a1 == null) {
                        this.calculateOffset(i, 25, b);
                        logger.v(new StringBuilder().append("onScrollIdle newOffset:").append(this.loadingOffset.offset).append(" count=").append(this.loadingOffset.limit).toString());
                        this.requestMusicCollection(this.loadingOffset.offset, this.loadingOffset.limit, false);
                    }
                } else {
                    logger.v(new StringBuilder().append("onScrollIdle pos(").append(i).append(") >= size(").append(i0).append(")").toString());
                }
            }
        }
    }
    
    public void onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel a) {
        switch(com.navdy.hud.app.ui.component.mainmenu.MusicMenu2$8.$SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$MenuLevel[a.ordinal()]) {
            case 2: {
                if (busDelegate != null) {
                    this.bus.unregister(busDelegate);
                    this.presenter.setScrollIdleEvents(false);
                    this.purgeArtworkQueue();
                    infoForCurrentArtworkRequest = null;
                    this.isRequestingMusicCollection = false;
                    this.musicManager.removeMusicUpdateListener((com.navdy.hud.app.manager.MusicManager$MusicUpdateListener)busDelegate);
                    this.stopAudioAnimation();
                    busDelegate = null;
                    logger.v("onUnload-close bus-unregister");
                }
                if (this.returnToCacheList != null) {
                    logger.v("onUnload add to cache");
                    com.navdy.hud.app.ui.component.vlist.VerticalModelCache.addToCache(this.returnToCacheList);
                    this.returnToCacheList = null;
                }
                if (this.parentMenu == null) {
                    break;
                }
                if (this.parentMenu.getType() != com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.MUSIC) {
                    break;
                }
                this.parentMenu.onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.CLOSE);
                break;
            }
            case 1: {
                if (busDelegate != null && this.parentMenu.getType() == com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.MAIN) {
                    this.bus.unregister(busDelegate);
                    this.presenter.setScrollIdleEvents(false);
                    this.purgeArtworkQueue();
                    infoForCurrentArtworkRequest = null;
                    this.isRequestingMusicCollection = false;
                    this.musicManager.removeMusicUpdateListener((com.navdy.hud.app.manager.MusicManager$MusicUpdateListener)busDelegate);
                    this.stopAudioAnimation();
                    busDelegate = null;
                    logger.v("onUnload-back bus-unregister");
                }
                if (this.returnToCacheList == null) {
                    break;
                }
                logger.v("onUnload add to cache");
                com.navdy.hud.app.ui.component.vlist.VerticalModelCache.addToCache(this.returnToCacheList);
                this.returnToCacheList = null;
                break;
            }
        }
    }
    
    public boolean selectItem(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
        logger.i(new StringBuilder().append("onItemClicked ").append(a.id).append(", ").append(a.pos).toString());
        label2: if (a.id != R.id.menu_back) {
            if (a.id != R.id.music_browser_shuffle) {
                java.util.List a0 = this.menuData.musicTracks;
                label0: {
                    label1: {
                        if (a0 == null) {
                            break label1;
                        }
                        if (this.menuData.musicTracks.size() > 0) {
                            break label0;
                        }
                    }
                    com.navdy.hud.app.ui.component.mainmenu.MusicMenu2 a1 = this.getMusicPlayerMenu(a.id, a.pos);
                    this.presenter.loadMenu((com.navdy.hud.app.ui.component.mainmenu.IMenu)a1, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.SUB_LEVEL, a.pos, 0);
                    break label2;
                }
                java.util.List a2 = this.cachedList;
                com.navdy.hud.app.ui.component.vlist.VerticalList$Model a3 = null;
                if (a2 != null) {
                    int i = a.pos;
                    a3 = null;
                    if (i >= 0) {
                        int i0 = a.pos;
                        int i1 = this.cachedList.size();
                        a3 = null;
                        if (i0 < i1) {
                            a3 = (com.navdy.hud.app.ui.component.vlist.VerticalList$Model)this.cachedList.get(a.pos);
                        }
                    }
                }
                logger.v(new StringBuilder().append("onItemClicked track:").append((a3 == null) ? "null" : a3.title).toString());
                com.navdy.service.library.events.audio.MusicCollectionInfo a4 = this.menuData.musicCollectionInfo;
                this.bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)new com.navdy.service.library.events.audio.MusicEvent$Builder().collectionSource(a4.collectionSource).collectionType(a4.collectionType).collectionId(a4.collectionId).index(Integer.valueOf(a.id)).action(com.navdy.service.library.events.audio.MusicEvent$Action.MUSIC_ACTION_PLAY).shuffleMode(com.navdy.service.library.events.audio.MusicShuffleMode.MUSIC_SHUFFLE_MODE_OFF).build()));
                this.showMusicPlayer();
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMusicBrowsePlayAction((String)analyticsTypeStringMap.get(a4.collectionType));
            } else if (this.isThisQueuePlaying()) {
                com.navdy.service.library.events.audio.MusicShuffleMode a5 = (this.musicManager.isShuffling()) ? com.navdy.service.library.events.audio.MusicShuffleMode.MUSIC_SHUFFLE_MODE_OFF : com.navdy.service.library.events.audio.MusicShuffleMode.MUSIC_SHUFFLE_MODE_SONGS;
                this.bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)new com.navdy.service.library.events.audio.MusicEvent$Builder().action(com.navdy.service.library.events.audio.MusicEvent$Action.MUSIC_ACTION_MODE_CHANGE).shuffleMode(a5).build()));
                this.handler.post((Runnable)new com.navdy.hud.app.ui.component.mainmenu.MusicMenu2$3(this));
            } else {
                com.navdy.service.library.events.audio.MusicCollectionInfo a6 = this.menuData.musicCollectionInfo;
                this.bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)new com.navdy.service.library.events.audio.MusicEvent$Builder().collectionSource(a6.collectionSource).collectionType(a6.collectionType).collectionId(a6.collectionId).action(com.navdy.service.library.events.audio.MusicEvent$Action.MUSIC_ACTION_PLAY).shuffleMode(com.navdy.service.library.events.audio.MusicShuffleMode.MUSIC_SHUFFLE_MODE_SONGS).build()));
                this.showMusicPlayer();
            }
        } else {
            this.presenter.loadMenu(this.parentMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.BACK_TO_PARENT, this.backSelection, this.backSelectionId);
            this.backSelection = 0;
            this.backSelectionId = 0;
        }
        return true;
    }
    
    public void setBackSelectionId(int i) {
        logger.i(new StringBuilder().append("setBackSelectionId: ").append(i).toString());
        this.backSelectionId = i;
    }
    
    public void setBackSelectionPos(int i) {
        this.backSelection = i;
    }
    
    public void setSelectedIcon() {
        logger.i("setSelectedIcon");
        this.setupMenu();
        if (this.parentMenu.getType() != com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.MAIN) {
            if (this.menuData.musicCollectionInfo.collectionType != null) {
                if (android.text.TextUtils.isEmpty((CharSequence)this.menuData.musicCollectionInfo.collectionId)) {
                    this.vmenuComponent.setSelectedIconColorImage(((Integer)menuTypeIconMap.get(this.menuData.musicCollectionInfo.collectionType)).intValue(), this.bgColorSelected, (android.graphics.Shader)null, 1f);
                    this.vmenuComponent.selectedText.setText((CharSequence)menuTypeStringMap.get(this.menuData.musicCollectionInfo.collectionType));
                } else {
                    android.graphics.Bitmap a = com.navdy.hud.app.util.picasso.PicassoUtil.getBitmapfromCache(this.collectionIdString(this.menuData.musicCollectionInfo));
                    if (a == null) {
                        com.navdy.hud.app.ui.component.image.IconColorImageView$IconShape a0 = (this.menuData.musicCollectionInfo.collectionType != com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ARTISTS) ? com.navdy.hud.app.ui.component.image.IconColorImageView$IconShape.SQUARE : com.navdy.hud.app.ui.component.image.IconColorImageView$IconShape.CIRCLE;
                        this.vmenuComponent.setSelectedIconColorImage(((Integer)menuTypeIconMap.get(this.menuData.musicCollectionInfo.collectionType)).intValue(), this.bgColorSelected, (android.graphics.Shader)null, 1f, a0);
                        this.musicArtworkCache.getArtwork(this.menuData.musicCollectionInfo, (com.navdy.hud.app.util.MusicArtworkCache$Callback)new com.navdy.hud.app.ui.component.mainmenu.MusicMenu2$2(this));
                    } else {
                        this.vmenuComponent.setSelectedIconImage(a);
                    }
                    this.vmenuComponent.selectedText.setText((CharSequence)this.menuData.musicCollectionInfo.name);
                }
            } else {
                this.vmenuComponent.setSelectedIconColorImage(((Integer)menuSourceIconMap.get(this.menuData.musicCollectionInfo.collectionSource)).intValue(), sourceBgColor, (android.graphics.Shader)null, 1f);
                this.vmenuComponent.selectedText.setText((CharSequence)menuSourceStringMap.get(this.menuData.musicCollectionInfo.collectionSource));
            }
        } else {
            this.vmenuComponent.setSelectedIconColorImage(R.drawable.icon_mm_music_2, mmMusicColor, (android.graphics.Shader)null, 1f);
            this.vmenuComponent.selectedText.setText(R.string.music);
        }
    }
    
    public void showToolTip() {
    }
}
