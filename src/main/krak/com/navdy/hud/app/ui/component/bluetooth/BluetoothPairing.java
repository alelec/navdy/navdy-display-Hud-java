package com.navdy.hud.app.ui.component.bluetooth;
import com.navdy.hud.app.R;

public class BluetoothPairing implements com.navdy.hud.app.framework.toast.IToastCallback {
    final public static String ARG_AUTO_ACCEPT = "auto";
    final public static String ARG_DEVICE = "device";
    final public static String ARG_PIN = "pin";
    final public static String ARG_VARIANT = "variant";
    final private static String BLUETOOTH_PAIRING_TOAST_ID = "bt-pairing";
    final private static com.navdy.service.library.log.Logger sLogger;
    private boolean acceptDone;
    private boolean autoAccept;
    private com.squareup.otto.Bus bus;
    private java.util.ArrayList choices;
    private boolean confirmationRequired;
    private android.bluetooth.BluetoothDevice device;
    private boolean initialized;
    private int pin;
    private int variant;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.bluetooth.BluetoothPairing.class);
    }
    
    BluetoothPairing(android.bluetooth.BluetoothDevice a, int i, int i0, boolean b, com.squareup.otto.Bus a0) {
        this.choices = new java.util.ArrayList(2);
        this.device = a;
        this.pin = i;
        this.variant = i0;
        this.autoAccept = b;
        this.bus = a0;
    }
    
    private void confirm() {
        sLogger.v("confirm()");
        com.navdy.hud.app.util.BluetoothUtil.confirmPairing(this.device, this.variant, this.pin);
        this.finish();
    }
    
    private void finish() {
        sLogger.v("finish()");
        com.navdy.hud.app.framework.toast.ToastManager.getInstance().dismissCurrentToast();
    }
    
    private String formatPin(int i, int i0) {
        String s = null;
        if (i0 != 5) {
            Object[] a = new Object[1];
            a[0] = Integer.valueOf(i);
            s = String.format("%06d", a);
        } else {
            Object[] a0 = new Object[1];
            a0[0] = Integer.valueOf(i);
            s = String.format("%04d", a0);
        }
        return s;
    }
    
    public static void showBluetoothPairingToast(android.os.Bundle a) {
        sLogger.v("showBluetoothPairingToast");
        android.bluetooth.BluetoothDevice a0 = (android.bluetooth.BluetoothDevice)a.getParcelable("device");
        int i = a.getInt("pin");
        int i0 = a.getInt("variant");
        boolean b = a.getBoolean("auto");
        if (!Boolean.TRUE.equals(Boolean.valueOf(com.navdy.hud.app.device.dial.DialManager.getInstance().isDialConnected()))) {
            b = true;
        }
        com.navdy.hud.app.ui.component.bluetooth.BluetoothPairing a1 = new com.navdy.hud.app.ui.component.bluetooth.BluetoothPairing(a0, i, i0, b, com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus());
        sLogger.v("show()");
        a1.show();
    }
    
    public void executeChoiceItem(int i, int i0) {
        sLogger.v(new StringBuilder().append("execute:").append(i).toString());
        if (this.confirmationRequired) {
            if (i != 0) {
                this.finish();
            } else {
                this.confirm();
            }
        } else {
            this.finish();
        }
    }
    
    public void onBondStateChange(com.navdy.hud.app.ui.component.bluetooth.BluetoothPairing$BondStateChange a) {
        sLogger.v("onBondStateChange");
        label2: if (a.device.equals(this.device)) {
            int i = a.newState;
            label0: {
                label1: {
                    if (i == 10) {
                        break label1;
                    }
                    if (a.newState != 12) {
                        break label0;
                    }
                }
                sLogger.i("ending pairing dialog because of bond state change");
                this.finish();
                break label2;
            }
            sLogger.i(new StringBuilder().append("Ignoring bond state change for ").append(a.device.getAddress()).append(" state: ").append(a.newState).append(" dialog device:").append(this.device.getAddress()).toString());
        }
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        return false;
    }
    
    public void onPairingCancelled(com.navdy.hud.app.ui.component.bluetooth.BluetoothPairing$PairingCancelled a) {
        sLogger.v("onPairingCancelled");
        this.finish();
    }
    
    public void onStart(com.navdy.hud.app.view.ToastView a) {
        sLogger.v("registerBus");
        this.bus.register(this);
        if (!this.acceptDone && this.autoAccept) {
            this.acceptDone = true;
            sLogger.v("auto-accept");
            com.navdy.hud.app.util.BluetoothUtil.confirmPairing(this.device, this.variant, this.pin);
        }
    }
    
    public void onStop() {
        sLogger.v("unregisterBus");
        this.bus.unregister(this);
    }
    
    void show() {
        android.content.res.Resources a = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        android.os.Bundle a0 = new android.os.Bundle();
        a0.putInt("8", R.drawable.icon_bluetooth_connecting);
        a0.putString("1", a.getString(R.string.bluetooth_pairing_title));
        a0.putString("2", a.getString(R.string.bluetooth_pairing_code));
        android.bluetooth.BluetoothDevice a1 = this.device;
        String s = null;
        if (a1 != null) {
            s = this.device.getName();
            if (android.text.TextUtils.isEmpty((CharSequence)s)) {
                s = this.device.getAddress();
            }
        }
        if (s == null) {
            s = a.getString(R.string.unknown_device);
        }
        Object[] a2 = new Object[1];
        a2[0] = s;
        a0.putString("6", a.getString(R.string.bluetooth_device, a2));
        a0.putString("4", this.formatPin(this.pin, this.variant));
        boolean b = !this.autoAccept;
        boolean b0 = this.confirmationRequired;
        label0: {
            label1: {
                if (b != b0) {
                    break label1;
                }
                if (this.initialized) {
                    break label0;
                }
            }
            this.initialized = true;
            this.confirmationRequired = b;
            if (b) {
                this.choices.add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(a.getString(R.string.bluetooth_confirm), 0));
                this.choices.add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(a.getString(R.string.cancel), 0));
            } else {
                this.choices.add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(a.getString(R.string.cancel), 0));
            }
            a0.putParcelableArrayList("20", this.choices);
        }
        com.navdy.hud.app.framework.toast.ToastManager a3 = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
        a3.dismissCurrentToast();
        a3.clearAllPendingToast();
        a3.addToast(new com.navdy.hud.app.framework.toast.ToastManager$ToastParams("bt-pairing", a0, (com.navdy.hud.app.framework.toast.IToastCallback)this, false, true, true));
    }
}
