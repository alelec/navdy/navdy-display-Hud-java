package com.navdy.hud.app.ui.framework;

public class BasePresenter extends mortar.ViewPresenter {
    protected boolean active;
    
    public BasePresenter() {
    }
    
    public void dropView(android.view.View a) {
        android.view.View a0 = (android.view.View)this.getView();
        if (a0 != null) {
            if (a == a0) {
                this.active = false;
                this.onUnload();
            }
            super.dropView(a0);
        }
    }
    
    public void dropView(Object a) {
        this.dropView((android.view.View)a);
    }
    
    public boolean isActive() {
        return this.active;
    }
    
    public void onLoad(android.os.Bundle a) {
        this.active = true;
        super.onLoad(a);
    }
    
    protected void onUnload() {
    }
}
