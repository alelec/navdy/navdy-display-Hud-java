package com.navdy.hud.app.ui.component.tbt;

final public class TbtTextInstructionView$Companion {
    private TbtTextInstructionView$Companion() {
    }
    
    public TbtTextInstructionView$Companion(kotlin.jvm.internal.DefaultConstructorMarker a) {
        this();
    }
    
    final public static int access$getFullWidth$p(com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$Companion a) {
        return a.getFullWidth();
    }
    
    final public static int access$getMediumWidth$p(com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$Companion a) {
        return a.getMediumWidth();
    }
    
    final public static android.content.res.Resources access$getResources$p(com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$Companion a) {
        return a.getResources();
    }
    
    final public static int access$getSingleLineMinWidth$p(com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$Companion a) {
        return a.getSingleLineMinWidth();
    }
    
    final public static float access$getSize18$p(com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$Companion a) {
        return a.getSize18();
    }
    
    final public static float access$getSize20$p(com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$Companion a) {
        return a.getSize20();
    }
    
    final public static float access$getSize22$p(com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$Companion a) {
        return a.getSize22();
    }
    
    final public static float access$getSize24$p(com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$Companion a) {
        return a.getSize24();
    }
    
    final public static float access$getSize26$p(com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$Companion a) {
        return a.getSize26();
    }
    
    final public static int access$getSmallWidth$p(com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$Companion a) {
        return a.getSmallWidth();
    }
    
    final private int getFullWidth() {
        return com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView.access$getFullWidth$cp();
    }
    
    final private com.navdy.service.library.log.Logger getLogger() {
        return com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView.access$getLogger$cp();
    }
    
    final private int getMediumWidth() {
        return com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView.access$getMediumWidth$cp();
    }
    
    final private android.content.res.Resources getResources() {
        return com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView.access$getResources$cp();
    }
    
    final private int getSingleLineMinWidth() {
        return com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView.access$getSingleLineMinWidth$cp();
    }
    
    final private float getSize18() {
        return com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView.access$getSize18$cp();
    }
    
    final private float getSize20() {
        return com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView.access$getSize20$cp();
    }
    
    final private float getSize22() {
        return com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView.access$getSize22$cp();
    }
    
    final private float getSize24() {
        return com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView.access$getSize24$cp();
    }
    
    final private float getSize26() {
        return com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView.access$getSize26$cp();
    }
    
    final private int getSmallWidth() {
        return com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView.access$getSmallWidth$cp();
    }
}
