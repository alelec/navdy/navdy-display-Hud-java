package com.navdy.hud.app.ui.component.homescreen;

class SmartDashView$6 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
    final com.navdy.hud.app.ui.component.homescreen.SmartDashView this$0;
    
    SmartDashView$6(com.navdy.hud.app.ui.component.homescreen.SmartDashView a) {
        super();
        this.this$0 = a;
    }
    
    public void onAnimationEnd(android.animation.Animator a) {
        this.this$0.mLeftGaugeViewPager.setVisibility(0);
        this.this$0.mRightGaugeViewPager.setVisibility(0);
        if (this.this$0.homeScreenView.isNavigationActive()) {
            com.navdy.hud.app.ui.component.homescreen.SmartDashView.access$900(this.this$0, true);
        }
    }
}
