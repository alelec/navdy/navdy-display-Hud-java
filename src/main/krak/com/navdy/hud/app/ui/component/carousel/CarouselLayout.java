package com.navdy.hud.app.ui.component.carousel;
import com.navdy.hud.app.R;

public class CarouselLayout extends android.widget.FrameLayout implements com.navdy.hud.app.manager.InputManager$IInputHandler, com.navdy.hud.app.gesture.GestureDetector$GestureListener {
    final static boolean $assertionsDisabled;
    final private static int DEFAULT_ANIM_DURATION = 250;
    final private static com.navdy.service.library.log.Logger sLogger;
    private int animationDuration;
    boolean animationRunning;
    private com.navdy.hud.app.ui.component.carousel.AnimationStrategy animator;
    com.navdy.hud.app.ui.component.carousel.CarouselAdapter carouselAdapter;
    com.navdy.hud.app.ui.component.carousel.CarouselIndicator carouselIndicator;
    int currentItem;
    protected com.navdy.hud.app.gesture.GestureDetector detector;
    private boolean exitOnDoubleClick;
    private boolean fastScrollAnimation;
    private android.os.Handler handler;
    int imageLytResourceId;
    android.view.LayoutInflater inflater;
    int infoLayoutResourceId;
    android.view.animation.Interpolator interpolator;
    com.navdy.hud.app.ui.component.carousel.Carousel$Listener itemChangeListener;
    com.navdy.hud.app.ui.component.carousel.CarouselLayout$Operation lastScrollAnimationOperation;
    private int lastTouchX;
    private int lastTouchY;
    android.view.View leftView;
    int mainImageSize;
    int mainLeftPadding;
    int mainRightPadding;
    int mainViewDividerPadding;
    android.view.View middleLeftView;
    android.view.View middleRightView;
    java.util.List model;
    private com.navdy.hud.app.gesture.MultipleClickGestureDetector multipleClickGestureDetector;
    android.view.View newLeftView;
    android.view.View newMiddleLeftView;
    android.view.View newMiddleRightView;
    android.view.View newRightView;
    private java.util.Queue operationQueue;
    int rightImageStart;
    int rightSectionHeight;
    int rightSectionWidth;
    android.view.View rightView;
    android.view.View rootContainer;
    android.view.View selectedItemView;
    int sideImageSize;
    private com.navdy.hud.app.ui.component.carousel.Carousel$ViewCacheManager viewCacheManager;
    int viewPadding;
    com.navdy.hud.app.ui.component.carousel.Carousel$ViewProcessor viewProcessor;
    boolean viewsScaled;
    
    static {
        $assertionsDisabled = !com.navdy.hud.app.ui.component.carousel.CarouselLayout.class.desiredAssertionStatus();
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.carousel.CarouselLayout.class);
    }
    
    public CarouselLayout(android.content.Context a) {
        super(a, (android.util.AttributeSet)null);
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.operationQueue = (java.util.Queue)new java.util.LinkedList();
        this.currentItem = -1;
        this.viewCacheManager = new com.navdy.hud.app.ui.component.carousel.Carousel$ViewCacheManager(3);
        this.detector = new com.navdy.hud.app.gesture.GestureDetector((com.navdy.hud.app.gesture.GestureDetector$GestureListener)this);
    }
    
    public CarouselLayout(android.content.Context a, android.util.AttributeSet a0) {
        super(a, a0);
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.operationQueue = (java.util.Queue)new java.util.LinkedList();
        this.currentItem = -1;
        this.viewCacheManager = new com.navdy.hud.app.ui.component.carousel.Carousel$ViewCacheManager(3);
        this.detector = new com.navdy.hud.app.gesture.GestureDetector((com.navdy.hud.app.gesture.GestureDetector$GestureListener)this);
        this.inflater = android.view.LayoutInflater.from(a);
        this.initFromAttributes(a, a0);
        this.setWillNotDraw(false);
        if (!com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            this.setOnTouchListener((android.view.View$OnTouchListener)new com.navdy.hud.app.ui.component.carousel.CarouselLayout$1(this));
            this.setOnClickListener((android.view.View$OnClickListener)new com.navdy.hud.app.ui.component.carousel.CarouselLayout$2(this));
        }
    }
    
    static int access$000(com.navdy.hud.app.ui.component.carousel.CarouselLayout a) {
        return a.lastTouchX;
    }
    
    static int access$002(com.navdy.hud.app.ui.component.carousel.CarouselLayout a, int i) {
        a.lastTouchX = i;
        return i;
    }
    
    static int access$100(com.navdy.hud.app.ui.component.carousel.CarouselLayout a) {
        return a.lastTouchY;
    }
    
    static int access$102(com.navdy.hud.app.ui.component.carousel.CarouselLayout a, int i) {
        a.lastTouchY = i;
        return i;
    }
    
    static int access$200(com.navdy.hud.app.ui.component.carousel.CarouselLayout a) {
        return a.animationDuration;
    }
    
    static void access$300(com.navdy.hud.app.ui.component.carousel.CarouselLayout a, android.animation.Animator$AnimatorListener a0, boolean b, boolean b0, boolean b1, int i) {
        a.move(a0, b, b0, b1, i);
    }
    
    static boolean access$400(com.navdy.hud.app.ui.component.carousel.CarouselLayout a, com.navdy.hud.app.manager.InputManager$CustomKeyEvent a0) {
        return a.handleKey(a0);
    }
    
    static void access$500(com.navdy.hud.app.ui.component.carousel.CarouselLayout a, java.util.List a0, int i, boolean b) {
        a.setModelInternal(a0, i, b);
    }
    
    static void access$600(com.navdy.hud.app.ui.component.carousel.CarouselLayout a, int i, boolean b) {
        a.setCurrentItem(i, b);
    }
    
    static void access$700(com.navdy.hud.app.ui.component.carousel.CarouselLayout a, android.animation.Animator$AnimatorListener a0, int i, com.navdy.hud.app.ui.component.carousel.Carousel$Model a1, boolean b) {
        a.doInsert(a0, i, a1, b);
    }
    
    static void access$800(com.navdy.hud.app.ui.component.carousel.CarouselLayout a, boolean b, boolean b0) {
        a.changeSelectedItem(b, b0);
    }
    
    static void access$900(com.navdy.hud.app.ui.component.carousel.CarouselLayout a, com.navdy.hud.app.ui.component.carousel.Carousel$ViewType a0, android.view.View a1) {
        a.recycleView(a0, a1);
    }
    
    private android.animation.AnimatorSet buildLayoutAnimation(android.animation.Animator$AnimatorListener a, int i, int i0) {
        android.animation.AnimatorSet a0 = new android.animation.AnimatorSet();
        boolean b = i0 > i;
        com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction a1 = b ? com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction.LEFT : com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction.RIGHT;
        int i1 = this.mainViewDividerPadding + this.sideImageSize;
        if (b) {
            this.newMiddleRightView = this.addMiddleRightView(i0, (int)this.rightView.getX() + i1, false);
            this.newRightView = this.addHiddenView(i0 + 1, com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction.RIGHT, i0 + 1 < this.model.size());
        } else {
            this.newLeftView = this.addHiddenView(i0 - 1, com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction.LEFT, i0 > 0);
            this.newMiddleRightView = this.addMiddleRightView(i0, (int)this.leftView.getX() + i1, false);
            this.newMiddleRightView.setAlpha(0.0f);
        }
        android.animation.AnimatorSet$Builder a2 = a0.play(this.animator.createViewOutAnimation(this, a1));
        a2.with((android.animation.Animator)this.animator.createMiddleLeftViewAnimation(this, a1));
        a2.with((android.animation.Animator)this.animator.createMiddleRightViewAnimation(this, a1));
        a2.with((android.animation.Animator)this.animator.createSideViewToMiddleAnimation(this, a1));
        a2.with((android.animation.Animator)this.animator.createNewMiddleRightViewAnimation(this, a1));
        a2.with((android.animation.Animator)this.animator.createHiddenViewAnimation(this, a1));
        if (this.carouselIndicator != null) {
            android.animation.AnimatorSet a3 = this.carouselIndicator.getItemMoveAnimator(i0, -1);
            if (a3 != null) {
                a2.with((android.animation.Animator)a3);
            }
        }
        a0.setInterpolator((android.animation.TimeInterpolator)this.interpolator);
        a0.addListener(this.buildListener(a, b, this.currentItem, i0));
        return a0;
    }
    
    private android.animation.Animator$AnimatorListener buildListener(android.animation.Animator$AnimatorListener a, boolean b, int i, int i0) {
        return (android.animation.Animator$AnimatorListener)new com.navdy.hud.app.ui.component.carousel.CarouselLayout$8(this, a, i, i0, b);
    }
    
    private void changeSelectedItem(boolean b, boolean b0) {
        if (this.selectedItemView != null) {
            if (b) {
                this.selectedItemView = this.rightView;
            } else {
                this.selectedItemView = this.leftView;
            }
        }
    }
    
    private void doInsert(android.animation.Animator$AnimatorListener a, int i, com.navdy.hud.app.ui.component.carousel.Carousel$Model a0, boolean b) {
        this.model.add(i, a0);
        android.animation.AnimatorSet a1 = new android.animation.AnimatorSet();
        java.util.ArrayList a2 = new java.util.ArrayList();
        int i0 = this.currentItem;
        com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction a3 = com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction.RIGHT;
        int i1 = this.viewPadding;
        int i2 = this.viewPadding;
        int i3 = this.sideImageSize;
        int i4 = this.rightImageStart;
        int i5 = this.viewPadding;
        int i6 = this.sideImageSize;
        int i7 = this.mainLeftPadding;
        int i8 = this.viewPadding;
        int i9 = this.sideImageSize;
        int i10 = this.mainLeftPadding;
        int i11 = this.mainImageSize;
        int i12 = this.currentItem + 1;
        label1: {
            label2: {
                label5: {
                    if (i <= i12) {
                        break label5;
                    }
                    break label2;
                }
                int i13 = this.currentItem + 1;
                label4: {
                    if (i != i13) {
                        break label4;
                    }
                    this.newRightView = this.addSideView(i, i2 + i3 + i4, false);
                    a3 = com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction.RIGHT;
                    ((java.util.Collection)a2).add(this.animator.createViewOutAnimation(this, a3));
                    break label2;
                }
                int i14 = this.currentItem;
                label3: {
                    if (i != i14) {
                        break label3;
                    }
                    if (b) {
                        this.newLeftView = this.addSideView(i, i1, false);
                        a3 = com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction.LEFT;
                        ((java.util.Collection)a2).add(this.animator.createViewOutAnimation(this, a3));
                        this.currentItem = this.currentItem + 1;
                        break label2;
                    } else {
                        this.newMiddleLeftView = this.addMiddleLeftView(i, i5 + i6 + i7, false);
                        this.newMiddleRightView = this.addMiddleRightView(i, i8 + i9 + i10 + i11, false);
                        a3 = com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction.RIGHT;
                        if (this.rightView != null) {
                            ((java.util.Collection)a2).add(this.animator.createViewOutAnimation(this, a3));
                        }
                        ((java.util.Collection)a2).add(this.animator.createMiddleLeftViewAnimation(this, a3));
                        ((java.util.Collection)a2).add(this.animator.createMiddleRightViewAnimation(this, a3));
                        break label2;
                    }
                }
                if (i > this.currentItem - 1) {
                    break label2;
                }
                label0: {
                    if (b) {
                        break label0;
                    }
                    this.move(a, true, true, false, this.animationDuration);
                    break label1;
                }
                this.currentItem = this.currentItem + 1;
            }
            a1.addListener(this.buildListener(a, a3 == com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction.LEFT, i0, this.currentItem));
            a1.playTogether((java.util.Collection)a2);
            this.animationRunning = true;
            a1.start();
        }
    }
    
    private boolean handleKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        boolean b = false;
        if (this.currentItem != -1) {
            switch(com.navdy.hud.app.ui.component.carousel.CarouselLayout$10.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[a.ordinal()]) {
                case 3: {
                    this.selectItem();
                    b = true;
                    break;
                }
                case 2: {
                    this.move((android.animation.Animator$AnimatorListener)null, true, true, false, this.animationDuration);
                    b = true;
                    break;
                }
                case 1: {
                    this.move((android.animation.Animator$AnimatorListener)null, false, true, false, this.animationDuration);
                    b = true;
                    break;
                }
                default: {
                    b = false;
                }
            }
        } else {
            b = true;
        }
        return b;
    }
    
    private void initFromAttributes(android.content.Context a, android.util.AttributeSet a0) {
        android.content.res.TypedArray a1 = a.getTheme().obtainStyledAttributes(a0, com.navdy.hud.app.R$styleable.Carousel, 0, 0);
        if (!$assertionsDisabled && a1 == null) {
            throw new AssertionError();
        }
        android.content.res.Resources a2 = this.getResources();
        try {
            this.animationDuration = a1.getInt(0, 250);
            this.interpolator = android.view.animation.AnimationUtils.loadInterpolator(this.getContext(), a1.getResourceId(1, 17432580));
            this.viewPadding = (int)a1.getDimension(2, a2.getDimension(R.dimen.carousel_side_margin));
            this.sideImageSize = (int)a1.getDimension(3, a2.getDimension(R.dimen.carousel_side_image_size));
            this.mainImageSize = (int)a1.getDimension(4, a2.getDimension(R.dimen.carousel_main_image_size));
            this.rightImageStart = (int)a1.getDimension(7, a2.getDimension(R.dimen.carousel_main_right_section_start));
            this.mainLeftPadding = (int)a1.getDimension(5, a2.getDimension(R.dimen.carousel_main_left_padding));
            this.mainRightPadding = (int)a1.getDimension(6, a2.getDimension(R.dimen.carousel_main_right_padding));
            this.rightSectionWidth = (int)a1.getDimension(9, a2.getDimension(R.dimen.carousel_main_right_section_width));
            this.viewsScaled = this.mainImageSize != this.sideImageSize;
            sLogger.v(new StringBuilder().append("view scaled=").append(this.viewsScaled).toString());
        } catch(Throwable a3) {
            a1.recycle();
            throw a3;
        }
        a1.recycle();
        this.rightSectionHeight = (int)this.getResources().getDimension(R.dimen.dashboard_box_height);
    }
    
    private void move(android.animation.Animator$AnimatorListener a, boolean b, boolean b0, boolean b1, int i) {
        com.navdy.hud.app.ui.component.carousel.CarouselLayout$Operation a0 = b ? com.navdy.hud.app.ui.component.carousel.CarouselLayout$Operation.FORWARD : com.navdy.hud.app.ui.component.carousel.CarouselLayout$Operation.BACK;
        label2: {
            label7: {
                com.navdy.hud.app.ui.component.carousel.CarouselLayout$OperationInfo a1 = null;
                if (b1) {
                    break label7;
                }
                if (!this.animationRunning) {
                    break label7;
                }
                boolean b2 = this.fastScrollAnimation;
                label6: {
                    if (b2) {
                        break label6;
                    }
                    if (this.operationQueue.size() >= 3) {
                        break label2;
                    }
                }
                boolean b3 = this.fastScrollAnimation;
                label3: {
                    label4: {
                        if (!b3) {
                            break label4;
                        }
                        int i0 = this.operationQueue.size();
                        label5: {
                            if (i0 > 0) {
                                break label5;
                            }
                            if (this.lastScrollAnimationOperation != a0) {
                                break label4;
                            }
                            i = 50;
                            break label4;
                        }
                        a1 = (com.navdy.hud.app.ui.component.carousel.CarouselLayout$OperationInfo)this.operationQueue.peek();
                        if (a1.type == a0) {
                            break label3;
                        }
                    }
                    com.navdy.hud.app.ui.component.carousel.CarouselLayout$OperationInfo a2 = new com.navdy.hud.app.ui.component.carousel.CarouselLayout$OperationInfo(a0, (Runnable)null, i);
                    a2.runnable = (Runnable)new com.navdy.hud.app.ui.component.carousel.CarouselLayout$6(this, a, b, b0, a2);
                    this.operationQueue.add(a2);
                    break label2;
                }
                a1.count = a1.count + 1;
                break label2;
            }
            int i1 = this.currentItem + (b ? 1 : -1);
            int i2 = this.currentItem;
            label0: {
                label1: {
                    if (i2 < 0) {
                        break label1;
                    }
                    if (i1 < 0) {
                        break label1;
                    }
                    if (i1 < this.getCount()) {
                        break label0;
                    }
                }
                if (sLogger.isLoggable(2)) {
                    sLogger.v(new StringBuilder().append("cannot go ").append(b ? "next" : "prev").toString());
                }
                this.runQueuedOperation();
                break label2;
            }
            this.lastScrollAnimationOperation = a0;
            if (this.animator instanceof com.navdy.hud.app.ui.component.carousel.FastScrollAnimator) {
                android.animation.AnimatorSet a3 = this.animator.buildLayoutAnimation(a, this, this.currentItem, i1);
                this.animationRunning = true;
                if (sLogger.isLoggable(2)) {
                    sLogger.v(new StringBuilder().append("move:").append(a0.name()).append(" duration =").append(a3.getDuration()).toString());
                }
                a3.start();
            } else {
                android.animation.AnimatorSet a4 = this.buildLayoutAnimation(a, this.currentItem, i1);
                a4.setDuration((long)i);
                this.animationRunning = true;
                if (sLogger.isLoggable(2)) {
                    sLogger.v(new StringBuilder().append("move:").append(a0.name()).append(" duration =").append(a4.getDuration()).toString());
                }
                a4.start();
            }
            com.navdy.hud.app.audio.SoundUtils.playSound(com.navdy.hud.app.audio.SoundUtils$Sound.MENU_MOVE);
        }
    }
    
    private void recycleView(com.navdy.hud.app.ui.component.carousel.Carousel$ViewType a, android.view.View a0) {
        if (a0 != null) {
            this.removeView(a0);
            a0.setScaleX(1f);
            a0.setScaleY(1f);
            a0.setVisibility(0);
            a0.setTranslationX(0.0f);
            a0.setTranslationY(0.0f);
            a0.setAlpha(1f);
            if (a0 instanceof com.navdy.hud.app.ui.component.image.CrossFadeImageView) {
                com.navdy.hud.app.ui.component.image.CrossFadeImageView a1 = (com.navdy.hud.app.ui.component.image.CrossFadeImageView)a0;
                android.view.View a2 = a1;
                android.widget.ImageView a3 = (android.widget.ImageView)a1.getBig();
                a3.setImageResource(0);
                a3.setPivotX(0.0f);
                a3.setPivotY(0.0f);
                if (this.viewsScaled && a3 instanceof com.navdy.hud.app.ui.component.image.InitialsImageView) {
                    ((com.navdy.hud.app.ui.component.image.InitialsImageView)a3).setScaled(true);
                }
                android.widget.ImageView a4 = (android.widget.ImageView)a1.getSmall();
                a4.setImageResource(0);
                a4.setPivotX(0.0f);
                a4.setPivotY(0.0f);
                if (this.viewsScaled) {
                    if (a4 instanceof com.navdy.hud.app.ui.component.image.InitialsImageView) {
                        ((com.navdy.hud.app.ui.component.image.InitialsImageView)a4).setScaled(true);
                        a0 = a2;
                    } else {
                        a0 = a2;
                    }
                } else {
                    a0 = a2;
                }
            }
            this.viewCacheManager.putView(a, a0);
        }
    }
    
    private void recycleViews() {
        this.recycleView(com.navdy.hud.app.ui.component.carousel.Carousel$ViewType.SIDE, this.leftView);
        this.leftView = null;
        this.recycleView(com.navdy.hud.app.ui.component.carousel.Carousel$ViewType.SIDE, this.rightView);
        this.rightView = null;
        this.recycleView(com.navdy.hud.app.ui.component.carousel.Carousel$ViewType.MIDDLE_LEFT, this.middleLeftView);
        this.middleLeftView = null;
        this.recycleView(com.navdy.hud.app.ui.component.carousel.Carousel$ViewType.MIDDLE_RIGHT, this.middleRightView);
        this.middleRightView = null;
    }
    
    private void setCurrentItem(int i, boolean b) {
        if (i >= 0 && i < this.model.size()) {
            if (this.currentItem != -1) {
                this.recycleViews();
            }
            this.leftView = this.addSideView(i - 1, this.viewPadding, i > 0);
            this.middleLeftView = this.addMiddleLeftView(i, this.viewPadding + this.sideImageSize + this.mainLeftPadding, true);
            this.middleRightView = this.addMiddleRightView(i, this.viewPadding + this.sideImageSize + this.mainLeftPadding + this.mainImageSize, true);
            this.rightView = this.addSideView(i + 1, this.viewPadding + this.sideImageSize + this.rightImageStart, i + 1 < this.model.size());
            if (b && this.itemChangeListener != null) {
                this.itemChangeListener.onCurrentItemChanging(this.currentItem, i, ((com.navdy.hud.app.ui.component.carousel.Carousel$Model)this.model.get(i)).id);
            }
            this.currentItem = i;
            if (this.carouselIndicator != null) {
                this.carouselIndicator.setCurrentItem(this.currentItem);
            }
            if (this.itemChangeListener != null) {
                this.itemChangeListener.onCurrentItemChanged(this.currentItem, ((com.navdy.hud.app.ui.component.carousel.Carousel$Model)this.model.get(i)).id);
            }
            if (this.middleLeftView != null && this.middleLeftView.getVisibility() == 0) {
                this.selectedItemView = this.middleLeftView;
            }
        }
    }
    
    private void setModelInternal(java.util.List a, int i, boolean b) {
        int i0 = this.currentItem;
        label2: {
            label0: {
                label1: {
                    if (i0 == -1) {
                        break label1;
                    }
                    if (b) {
                        break label0;
                    }
                }
                this.model = a;
                this.setCurrentItem(i, false);
                this.runQueuedOperation();
                break label2;
            }
            android.animation.AnimatorSet a0 = new android.animation.AnimatorSet();
            android.view.View a1 = this.middleLeftView;
            android.util.Property a2 = android.view.View.ALPHA;
            float[] a3 = new float[1];
            a3[0] = 0.0f;
            android.animation.AnimatorSet$Builder a4 = a0.play((android.animation.Animator)android.animation.ObjectAnimator.ofFloat(a1, a2, a3));
            if (this.middleRightView != null) {
                android.view.View a5 = this.middleRightView;
                android.util.Property a6 = android.view.View.ALPHA;
                float[] a7 = new float[1];
                a7[0] = 0.0f;
                a4.with((android.animation.Animator)android.animation.ObjectAnimator.ofFloat(a5, a6, a7));
            }
            if (this.leftView != null) {
                android.view.View a8 = this.leftView;
                android.util.Property a9 = android.view.View.ALPHA;
                float[] a10 = new float[1];
                a10[0] = 0.0f;
                a4.with((android.animation.Animator)android.animation.ObjectAnimator.ofFloat(a8, a9, a10));
            }
            if (this.rightView != null) {
                android.view.View a11 = this.rightView;
                android.util.Property a12 = android.view.View.ALPHA;
                float[] a13 = new float[1];
                a13[0] = 0.0f;
                a4.with((android.animation.Animator)android.animation.ObjectAnimator.ofFloat(a11, a12, a13));
            }
            a0.setDuration(250L);
            a0.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.ui.component.carousel.CarouselLayout$5(this, a, i));
            this.animationRunning = true;
            a0.start();
        }
    }
    
    android.view.View addHiddenView(int i, com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction a, boolean b) {
        return this.addSideView(i, (int)((a != com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction.RIGHT) ? -(this.leftView.getX() + (float)this.leftView.getMeasuredWidth()) : this.rightView.getX() + (float)this.mainImageSize + (float)this.mainViewDividerPadding + (float)this.rightSectionWidth + (float)this.mainRightPadding), b);
    }
    
    android.view.View addMiddleLeftView(int i, int i0, boolean b) {
        return this.buildView(i, i0, com.navdy.hud.app.ui.component.carousel.Carousel$ViewType.MIDDLE_LEFT, b);
    }
    
    android.view.View addMiddleRightView(int i, int i0, boolean b) {
        return this.buildView(i, i0, com.navdy.hud.app.ui.component.carousel.Carousel$ViewType.MIDDLE_RIGHT, b);
    }
    
    android.view.View addSideView(int i, int i0, boolean b) {
        return this.buildView(i, i0, com.navdy.hud.app.ui.component.carousel.Carousel$ViewType.SIDE, b);
    }
    
    android.view.View buildView(int i, int i0, com.navdy.hud.app.ui.component.carousel.Carousel$ViewType a, boolean b) {
        int i1 = 0;
        int i2 = 0;
        int i3 = 0;
        switch(com.navdy.hud.app.ui.component.carousel.CarouselLayout$10.$SwitchMap$com$navdy$hud$app$ui$component$carousel$Carousel$ViewType[a.ordinal()]) {
            case 3: {
                i1 = this.rightSectionHeight;
                i2 = this.rightSectionWidth;
                i3 = this.infoLayoutResourceId;
                break;
            }
            case 2: {
                i1 = this.mainImageSize;
                i2 = this.mainImageSize;
                i3 = this.imageLytResourceId;
                break;
            }
            case 1: {
                i1 = this.sideImageSize;
                i2 = this.sideImageSize;
                i3 = this.imageLytResourceId;
                break;
            }
            default: {
                i2 = 0;
                i1 = 0;
                i3 = 0;
            }
        }
        android.widget.FrameLayout$LayoutParams a0 = new android.widget.FrameLayout$LayoutParams(i2, i1);
        a0.gravity = 16;
        android.view.View a1 = this.viewCacheManager.getView(a);
        android.view.View a2 = this.carouselAdapter.getView(i, a1, a, i3, i2);
        if (a1 == null && this.viewsScaled && a == com.navdy.hud.app.ui.component.carousel.Carousel$ViewType.SIDE && a2 instanceof com.navdy.hud.app.ui.component.image.CrossFadeImageView) {
            com.navdy.hud.app.ui.component.image.CrossFadeImageView a3 = (com.navdy.hud.app.ui.component.image.CrossFadeImageView)a2;
            a2 = a3;
            android.view.View a4 = a3.getBig();
            if (a4 instanceof com.navdy.hud.app.ui.component.image.InitialsImageView) {
                com.navdy.hud.app.ui.component.image.InitialsImageView a5 = (com.navdy.hud.app.ui.component.image.InitialsImageView)a4;
                a4 = a5;
                a5.setScaled(true);
            }
            if (a3.getSmall() instanceof com.navdy.hud.app.ui.component.image.InitialsImageView) {
                ((com.navdy.hud.app.ui.component.image.InitialsImageView)a4).setScaled(true);
            }
        }
        a2.setX((float)i0);
        a2.setVisibility(b ? 0 : 4);
        this.addView(a2, (android.view.ViewGroup$LayoutParams)a0);
        return a2;
    }
    
    public void clearOperationQueue() {
        this.operationQueue.clear();
    }
    
    public int getCount() {
        return (this.model != null) ? this.model.size() : 0;
    }
    
    public int getCurrentItem() {
        return this.currentItem;
    }
    
    public com.navdy.hud.app.ui.component.carousel.Carousel$Model getCurrentModel() {
        return (com.navdy.hud.app.ui.component.carousel.Carousel$Model)this.model.get(this.currentItem);
    }
    
    public com.navdy.hud.app.ui.component.carousel.Carousel$Listener getListener() {
        return this.itemChangeListener;
    }
    
    public com.navdy.hud.app.ui.component.carousel.Carousel$Model getModel(int i) {
        com.navdy.hud.app.ui.component.carousel.Carousel$Model a = null;
        label2: {
            label0: {
                label1: {
                    if (i < 0) {
                        break label1;
                    }
                    if (i < this.model.size()) {
                        break label0;
                    }
                }
                a = null;
                break label2;
            }
            a = (com.navdy.hud.app.ui.component.carousel.Carousel$Model)this.model.get(i);
        }
        return a;
    }
    
    public void init(com.navdy.hud.app.ui.component.carousel.Carousel$InitParams a) {
        if (this.model != null) {
            throw new IllegalStateException();
        }
        if (a.model != null && a.rootContainer != null && a.infoLayoutResourceId != 0 && a.imageLytResourceId != 0) {
            this.model = a.model;
            this.infoLayoutResourceId = a.infoLayoutResourceId;
            this.viewProcessor = a.viewProcessor;
            this.rootContainer = a.rootContainer;
            this.carouselIndicator = a.carouselIndicator;
            this.carouselAdapter = new com.navdy.hud.app.ui.component.carousel.CarouselAdapter(this);
            this.imageLytResourceId = a.imageLytResourceId;
            this.fastScrollAnimation = a.fastScrollAnimation;
            this.exitOnDoubleClick = a.exitOnDoubleClick;
            Object a0 = (a.animator == null) ? (this.fastScrollAnimation) ? new com.navdy.hud.app.ui.component.carousel.FastScrollAnimator(this) : new com.navdy.hud.app.ui.component.carousel.CarouselAnimator() : a.animator;
            this.animator = (com.navdy.hud.app.ui.component.carousel.AnimationStrategy)a0;
            if (this.exitOnDoubleClick) {
                this.multipleClickGestureDetector = new com.navdy.hud.app.gesture.MultipleClickGestureDetector(2, (com.navdy.hud.app.gesture.MultipleClickGestureDetector$IMultipleClickKeyGesture)new com.navdy.hud.app.ui.component.carousel.CarouselLayout$3(this));
            }
            return;
        }
        throw new IllegalArgumentException();
    }
    
    public void insert(android.animation.Animator$AnimatorListener a, int i, com.navdy.hud.app.ui.component.carousel.Carousel$Model a0, boolean b) {
        if (this.animationRunning) {
            com.navdy.hud.app.ui.component.carousel.CarouselLayout$7 a1 = new com.navdy.hud.app.ui.component.carousel.CarouselLayout$7(this, a, i, a0, b);
            this.operationQueue.add(new com.navdy.hud.app.ui.component.carousel.CarouselLayout$OperationInfo(com.navdy.hud.app.ui.component.carousel.CarouselLayout$Operation.OTHER, (Runnable)a1));
        } else {
            this.doInsert(a, i, a0, b);
        }
    }
    
    public boolean isAnimationEndPending() {
        return this.animator instanceof com.navdy.hud.app.ui.component.carousel.FastScrollAnimator && ((com.navdy.hud.app.ui.component.carousel.FastScrollAnimator)this.animator).isEndPending();
    }
    
    boolean isAnimationPending() {
        return this.operationQueue.size() > 0 && ((com.navdy.hud.app.ui.component.carousel.CarouselLayout$OperationInfo)this.operationQueue.peek()).type == this.lastScrollAnimationOperation;
    }
    
    public void moveNext(android.animation.Animator$AnimatorListener a) {
        this.move(a, true, false, false, this.animationDuration);
    }
    
    public void movePrevious(android.animation.Animator$AnimatorListener a) {
        this.move(a, false, false, false, this.animationDuration);
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        return null;
    }
    
    public void notifyDatasetChanged(java.util.List a, int i) {
        this.model = a;
        this.setCurrentItem(i, true);
    }
    
    public void onClick() {
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        boolean b = false;
        if (this.currentItem != -1) {
            this.detector.onGesture(a);
            com.navdy.service.library.events.input.Gesture a0 = a.gesture;
            int dummy = com.navdy.hud.app.ui.component.carousel.CarouselLayout$10.$SwitchMap$com$navdy$service$library$events$input$Gesture[a0.ordinal()];
            b = true;
        } else {
            b = false;
        }
        return b;
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        return (this.multipleClickGestureDetector == null) ? this.handleKey(a) : this.multipleClickGestureDetector.onKey(a);
    }
    
    public void onTrackHand(float f) {
    }
    
    public void reload() {
        this.setCurrentItem(this.currentItem, true);
    }
    
    void runQueuedOperation() {
        label2: if (this.operationQueue.size() <= 0) {
            com.navdy.hud.app.ui.component.carousel.FastScrollAnimator a = null;
            boolean b = this.animator instanceof com.navdy.hud.app.ui.component.carousel.FastScrollAnimator;
            label0: {
                label1: {
                    if (!b) {
                        break label1;
                    }
                    a = (com.navdy.hud.app.ui.component.carousel.FastScrollAnimator)this.animator;
                    if (a.isEndPending()) {
                        break label0;
                    }
                }
                this.lastScrollAnimationOperation = null;
                this.animationRunning = false;
                break label2;
            }
            a.endAnimation();
        } else {
            com.navdy.hud.app.ui.component.carousel.CarouselLayout$OperationInfo a0 = (com.navdy.hud.app.ui.component.carousel.CarouselLayout$OperationInfo)this.operationQueue.peek();
            if (a0.count != 1) {
                a0.count = a0.count - 1;
            } else {
                this.operationQueue.remove();
            }
            this.handler.post(a0.runnable);
        }
    }
    
    public void selectItem() {
        if (this.animationRunning) {
            com.navdy.hud.app.ui.component.carousel.CarouselLayout$9 a = new com.navdy.hud.app.ui.component.carousel.CarouselLayout$9(this);
            sLogger.w("queueing up select event");
            this.operationQueue.add(new com.navdy.hud.app.ui.component.carousel.CarouselLayout$OperationInfo(com.navdy.hud.app.ui.component.carousel.CarouselLayout$Operation.SELECT, (Runnable)a));
        } else {
            this.clearOperationQueue();
            if (this.selectedItemView != null) {
                if (this.selectedItemView != this.middleLeftView) {
                    sLogger.v("no match");
                    this.runQueuedOperation();
                } else {
                    int i = ((Integer)this.selectedItemView.getTag(R.id.item_id)).intValue();
                    com.navdy.hud.app.audio.SoundUtils.playSound(com.navdy.hud.app.audio.SoundUtils$Sound.MENU_SELECT);
                    if (this.itemChangeListener == null) {
                        sLogger.v("no carousel listener");
                        this.runQueuedOperation();
                    } else {
                        sLogger.v(new StringBuilder().append("execute item:").append(i).toString());
                        this.itemChangeListener.onExecuteItem(i, this.currentItem);
                    }
                }
            } else {
                sLogger.v("no item selected");
                this.runQueuedOperation();
            }
        }
    }
    
    public void setCurrentItem(int i) {
        this.setCurrentItem(i, true);
    }
    
    public void setListener(com.navdy.hud.app.ui.component.carousel.Carousel$Listener a) {
        this.itemChangeListener = a;
    }
    
    public void setModel(java.util.List a, int i, boolean b) {
        if (this.animationRunning) {
            this.clearOperationQueue();
            com.navdy.hud.app.ui.component.carousel.CarouselLayout$4 a0 = new com.navdy.hud.app.ui.component.carousel.CarouselLayout$4(this, a, i, b);
            this.operationQueue.add(new com.navdy.hud.app.ui.component.carousel.CarouselLayout$OperationInfo(com.navdy.hud.app.ui.component.carousel.CarouselLayout$Operation.OTHER, (Runnable)a0));
        } else {
            this.setModelInternal(a, i, b);
        }
    }
}
