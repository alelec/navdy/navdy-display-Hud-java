package com.navdy.hud.app.ui.component.destination;

public class DestinationPickerScreen$PlaceTypeResourceHolder {
    final public int colorRes;
    final public int destinationIcon;
    final public int iconRes;
    final public int titleRes;
    
    private DestinationPickerScreen$PlaceTypeResourceHolder(int i, int i0, int i1, int i2) {
        this.iconRes = i;
        this.titleRes = i0;
        this.colorRes = i1;
        this.destinationIcon = i2;
    }
    
    DestinationPickerScreen$PlaceTypeResourceHolder(int i, int i0, int i1, int i2, com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$1 a) {
        this(i, i0, i1, i2);
    }
}
