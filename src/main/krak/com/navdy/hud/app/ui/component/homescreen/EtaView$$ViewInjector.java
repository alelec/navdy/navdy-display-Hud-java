package com.navdy.hud.app.ui.component.homescreen;
import com.navdy.hud.app.R;

public class EtaView$$ViewInjector {
    public EtaView$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.ui.component.homescreen.EtaView a0, Object a1) {
        a0.etaTextView = (android.widget.TextView)a.findRequiredView(a1, R.id.etaTextView, "field 'etaTextView'");
        a0.etaTimeLeft = (android.widget.TextView)a.findRequiredView(a1, R.id.etaTimeLeft, "field 'etaTimeLeft'");
        a0.etaTimeAmPm = (android.widget.TextView)a.findRequiredView(a1, R.id.etaTimeAmPm, "field 'etaTimeAmPm'");
    }
    
    public static void reset(com.navdy.hud.app.ui.component.homescreen.EtaView a) {
        a.etaTextView = null;
        a.etaTimeLeft = null;
        a.etaTimeAmPm = null;
    }
}
