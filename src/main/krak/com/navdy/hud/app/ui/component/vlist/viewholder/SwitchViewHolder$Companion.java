package com.navdy.hud.app.ui.component.vlist.viewholder;
import com.navdy.hud.app.R;

final public class SwitchViewHolder$Companion {
    private SwitchViewHolder$Companion() {
    }
    
    public SwitchViewHolder$Companion(kotlin.jvm.internal.DefaultConstructorMarker a) {
        this();
    }
    
    public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model buildModel$default(com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder$Companion a, int i, int i0, String s, String s0, boolean b, boolean b0, int i1, Object a0) {
        if ((i1 & 16) != 0) {
            b = false;
        }
        if ((i1 & 32) != 0) {
            b0 = false;
        }
        return a.buildModel(i, i0, s, s0, b, b0);
    }
    
    final public com.navdy.hud.app.ui.component.vlist.VerticalList$Model buildModel(int i, int i0, String s, String s0, boolean b, boolean b0) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(s, "title");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(s0, "subTitle");
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a = com.navdy.hud.app.ui.component.vlist.VerticalModelCache.getFromCache(com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.SWITCH);
        if (a == null) {
            a = new com.navdy.hud.app.ui.component.vlist.VerticalList$Model();
        }
        a.type = com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.SWITCH;
        a.id = i;
        a.iconFluctuatorColor = i0;
        a.title = s;
        a.subTitle = s0;
        a.isOn = b;
        a.isEnabled = b0;
        return a;
    }
    
    final public com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder buildViewHolder(android.view.ViewGroup a, com.navdy.hud.app.ui.component.vlist.VerticalList a0, android.os.Handler a1) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "parent");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a0, "vlist");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a1, "handler");
        return new com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder(this.getLayout(a, R.layout.vlist_toggle_switch), a0, a1);
    }
    
    final public android.view.ViewGroup getLayout(android.view.ViewGroup a, int i) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "parent");
        android.view.View a0 = android.view.LayoutInflater.from(a.getContext()).inflate(i, a, false);
        if (a0 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.ViewGroup");
        }
        return (android.view.ViewGroup)a0;
    }
    
    final public com.navdy.service.library.log.Logger getLogger() {
        return com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder.access$getLogger$cp();
    }
}
