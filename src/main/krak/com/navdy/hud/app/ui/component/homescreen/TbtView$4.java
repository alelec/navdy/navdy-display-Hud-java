package com.navdy.hud.app.ui.component.homescreen;

class TbtView$4 implements android.animation.Animator$AnimatorListener {
    final com.navdy.hud.app.ui.component.homescreen.TbtView this$0;
    final android.text.SpannableStringBuilder val$spannableStringBuilder;
    
    TbtView$4(com.navdy.hud.app.ui.component.homescreen.TbtView a, android.text.SpannableStringBuilder a0) {
        super();
        this.this$0 = a;
        this.val$spannableStringBuilder = a0;
    }
    
    public void onAnimationCancel(android.animation.Animator a) {
    }
    
    public void onAnimationEnd(android.animation.Animator a) {
        this.this$0.activeMapInstruction.setText((CharSequence)this.val$spannableStringBuilder);
        this.this$0.activeMapInstruction.setTranslationY(0.0f);
        this.this$0.nextMapInstructionContainer.setTranslationY(0.0f);
        this.this$0.nextMapInstructionContainer.setVisibility(8);
    }
    
    public void onAnimationRepeat(android.animation.Animator a) {
    }
    
    public void onAnimationStart(android.animation.Animator a) {
    }
}
