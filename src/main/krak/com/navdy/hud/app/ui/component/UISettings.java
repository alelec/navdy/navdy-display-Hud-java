package com.navdy.hud.app.ui.component;

public class UISettings {
    final private static String ADVANCED_GPS_STATS = "persist.sys.gps.stats";
    final private static String DIAL_LONG_PRESS_ACTION_PLACE_SEARCH = "persist.sys.dlpress_search";
    final private static boolean dialLongPressPlaceSearchAction;
    
    static {
        dialLongPressPlaceSearchAction = com.navdy.hud.app.util.os.SystemProperties.getBoolean("persist.sys.dlpress_search", false);
    }
    
    public UISettings() {
    }
    
    public static boolean advancedGpsStatsEnabled() {
        return com.navdy.hud.app.util.os.SystemProperties.getBoolean("persist.sys.gps.stats", false);
    }
    
    public static boolean isLongPressActionPlaceSearch() {
        return dialLongPressPlaceSearchAction;
    }
    
    public static boolean isMusicBrowsingEnabled() {
        return true;
    }
    
    public static boolean isVerticalListNoCloseTimeout() {
        return true;
    }
    
    public static boolean supportsIosSms() {
        return true;
    }
}
