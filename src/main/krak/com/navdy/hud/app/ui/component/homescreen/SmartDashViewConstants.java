package com.navdy.hud.app.ui.component.homescreen;

public class SmartDashViewConstants {
    final public static int DEFAULT_SPEED_LIMIT_THRESHOLD_KMPH = 13;
    final public static int DEFAULT_SPEED_LIMIT_THRESHOLD_MPH = 8;
    final public static String PREFERENCE_SPEED_LIMIT_THRESHOLD = "PREFERENCE_SPEED_LIMIT_THRESHOLD";
    
    public SmartDashViewConstants() {
    }
}
