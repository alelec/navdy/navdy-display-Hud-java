package com.navdy.hud.app.ui.component;

class FluctuatorAnimatorView$3 implements android.animation.ValueAnimator$AnimatorUpdateListener {
    final com.navdy.hud.app.ui.component.FluctuatorAnimatorView this$0;
    
    FluctuatorAnimatorView$3(com.navdy.hud.app.ui.component.FluctuatorAnimatorView a) {
        super();
        this.this$0 = a;
    }
    
    public void onAnimationUpdate(android.animation.ValueAnimator a) {
        float f = ((Float)a.getAnimatedValue()).floatValue();
        this.this$0.currentCircle = f;
        this.this$0.invalidate();
    }
}
