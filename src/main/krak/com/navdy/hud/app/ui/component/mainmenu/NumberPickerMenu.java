package com.navdy.hud.app.ui.component.mainmenu;
import com.navdy.hud.app.R;

final public class NumberPickerMenu implements com.navdy.hud.app.ui.component.mainmenu.IMenu {
    final public static com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Companion Companion;
    final private static int SELECTION_ANIMATION_DELAY = 1000;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model back;
    final private static int baseMessageId = 100;
    final private static android.content.Context context;
    final private static int fluctuatorColor;
    final private static com.navdy.service.library.log.Logger logger;
    final private static android.content.res.Resources resources;
    private int backSelection;
    private int backSelectionId;
    private java.util.List cachedList;
    final private com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Callback callback;
    final private java.util.List contacts;
    final private int iconColor;
    final private int iconResource;
    private com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu messagePickerMenu;
    final private com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Mode mode;
    final private String notificationId;
    final private com.navdy.hud.app.ui.component.mainmenu.IMenu parent;
    final private com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter presenter;
    final private com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent;
    
    static {
        Companion = new com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Companion((kotlin.jvm.internal.DefaultConstructorMarker)null);
        logger = new com.navdy.service.library.log.Logger((Companion).getClass().getSimpleName());
        SELECTION_ANIMATION_DELAY = 1000;
        context = com.navdy.hud.app.HudApplication.getAppContext();
        resources = com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Companion.access$getContext$p(Companion).getResources();
        fluctuatorColor = android.support.v4.content.ContextCompat.getColor(com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Companion.access$getContext$p(Companion), R.color.mm_back);
        back = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Companion.access$getFluctuatorColor$p(Companion), com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Companion.access$getFluctuatorColor$p(Companion), com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Companion.access$getResources$p(Companion).getString(R.string.back), (String)null);
        baseMessageId = 100;
    }
    
    public NumberPickerMenu(com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter a0, com.navdy.hud.app.ui.component.mainmenu.IMenu a1, com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Mode a2, java.util.List a3, String s, int i, int i0, com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Callback a4) {
        super();
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "vscrollComponent");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a0, "presenter");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a1, "parent");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a2, "mode");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a3, "contacts");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a4, "callback");
        this.vscrollComponent = a;
        this.presenter = a0;
        this.parent = a1;
        this.mode = a2;
        this.contacts = a3;
        this.notificationId = s;
        this.iconResource = i;
        this.iconColor = i0;
        this.callback = a4;
    }
    
    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model access$getBack$cp() {
        return back;
    }
    
    final public static int access$getBaseMessageId$cp() {
        return baseMessageId;
    }
    
    final public static com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Callback access$getCallback$p(com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu a) {
        return a.callback;
    }
    
    final public static java.util.List access$getContacts$p(com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu a) {
        return a.contacts;
    }
    
    final public static android.content.Context access$getContext$cp() {
        return context;
    }
    
    final public static int access$getFluctuatorColor$cp() {
        return fluctuatorColor;
    }
    
    final public static com.navdy.service.library.log.Logger access$getLogger$cp() {
        return logger;
    }
    
    final public static android.content.res.Resources access$getResources$cp() {
        return resources;
    }
    
    final public static int access$getSELECTION_ANIMATION_DELAY$cp() {
        return SELECTION_ANIMATION_DELAY;
    }
    
    public com.navdy.hud.app.ui.component.mainmenu.IMenu getChildMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu a, String s, String s0) {
        return null;
    }
    
    public int getInitialSelection() {
        return 1;
    }
    
    public java.util.List getItems() {
        Object a = null;
        if (this.cachedList == null) {
            java.util.ArrayList a0 = new java.util.ArrayList();
            a0.add(com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Companion.access$getBack$p(Companion));
            java.util.Iterator a1 = this.contacts.iterator();
            int i = 0;
            Object a2 = a1;
            while(((java.util.Iterator)a2).hasNext()) {
                com.navdy.hud.app.framework.contacts.Contact a3 = (com.navdy.hud.app.framework.contacts.Contact)((java.util.Iterator)a2).next();
                String s = (a3.numberTypeStr == null) ? a3.formattedNumber : a3.numberTypeStr;
                String s0 = (a3.numberTypeStr == null) ? null : a3.formattedNumber;
                a0.add(com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Companion.access$getBaseMessageId$p(Companion) + i, this.iconResource, this.iconColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, this.iconColor, s, s0));
                i = i + 1;
            }
            this.cachedList = (java.util.List)a0;
            a = a0;
        } else {
            java.util.List a4 = this.cachedList;
            if (a4 == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.collections.MutableList<com.navdy.hud.app.ui.component.vlist.VerticalList.Model>");
            }
            a = kotlin.jvm.internal.TypeIntrinsics.asMutableList(a4);
        }
        return (java.util.List)a;
    }
    
    public com.navdy.hud.app.ui.component.vlist.VerticalList$Model getModelfromPos(int i) {
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a = null;
        java.util.List a0 = this.cachedList;
        label2: {
            label0: {
                label1: {
                    if (a0 == null) {
                        break label1;
                    }
                    if (a0.size() > i) {
                        break label0;
                    }
                }
                a = null;
                break label2;
            }
            a = (com.navdy.hud.app.ui.component.vlist.VerticalList$Model)a0.get(i);
        }
        return a;
    }
    
    public com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex getScrollIndex() {
        return null;
    }
    
    public com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu getType() {
        return com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.NUMBER_PICKER;
    }
    
    public boolean isBindCallsEnabled() {
        return false;
    }
    
    public boolean isFirstItemEmpty() {
        return false;
    }
    
    public boolean isItemClickable(int i, int i0) {
        return true;
    }
    
    public void onBindToView(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, android.view.View a0, int i, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState a1) {
    }
    
    public void onFastScrollEnd() {
    }
    
    public void onFastScrollStart() {
    }
    
    public void onItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
    }
    
    public void onScrollIdle() {
    }
    
    public void onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel a) {
    }
    
    public boolean selectItem(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "selection");
        com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Companion.access$getLogger$p(Companion).v(new StringBuilder().append("select id:").append(a.id).append(" pos:").append(a.pos).toString());
        if (a.id == R.id.menu_back) {
            com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("back");
            this.presenter.loadMenu(this.parent, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.BACK_TO_PARENT, this.backSelection, this.backSelectionId);
            this.backSelectionId = 0;
        } else {
            int i = a.id - com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Companion.access$getBaseMessageId$p(Companion);
            com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Mode a0 = this.mode;
            if (com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$WhenMappings.$EnumSwitchMapping$0[a0.ordinal()] != 0) {
                if (this.messagePickerMenu == null) {
                    com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a1 = this.vscrollComponent;
                    com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter a2 = this.presenter;
                    java.util.List a3 = com.navdy.hud.app.framework.glance.GlanceConstants.getCannedMessages();
                    kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(a3, "GlanceConstants.getCannedMessages()");
                    this.messagePickerMenu = new com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu(a1, a2, (com.navdy.hud.app.ui.component.mainmenu.IMenu)this, a3, (com.navdy.hud.app.framework.contacts.Contact)this.contacts.get(i), this.notificationId);
                }
                this.presenter.loadMenu((com.navdy.hud.app.ui.component.mainmenu.IMenu)this.messagePickerMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.SUB_LEVEL, a.pos, 0);
            } else {
                this.presenter.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$selectItem$1(this, i), com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Companion.access$getSELECTION_ANIMATION_DELAY$p(Companion));
            }
        }
        return true;
    }
    
    public void setBackSelectionId(int i) {
        this.backSelectionId = i;
    }
    
    public void setBackSelectionPos(int i) {
        this.backSelection = i;
    }
    
    public void setSelectedIcon() {
        this.vscrollComponent.setSelectedIconColorImage(this.iconResource, this.iconColor, (android.graphics.Shader)null, 1f);
        this.vscrollComponent.selectedText.setText((CharSequence)com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Companion.access$getResources$p(Companion).getString(R.string.which_number));
    }
    
    public void showToolTip() {
    }
}
