package com.navdy.hud.app.ui.component.mainmenu;

final public class MessagePickerMenu$Companion {
    private MessagePickerMenu$Companion() {
    }
    
    public MessagePickerMenu$Companion(kotlin.jvm.internal.DefaultConstructorMarker a) {
        this();
    }
    
    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model access$getBack$p(com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu$Companion a) {
        return a.getBack();
    }
    
    final public static int access$getBaseMessageId$p(com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu$Companion a) {
        return a.getBaseMessageId();
    }
    
    final public static android.content.Context access$getContext$p(com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu$Companion a) {
        return a.getContext();
    }
    
    final public static int access$getFluctuatorColor$p(com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu$Companion a) {
        return a.getFluctuatorColor();
    }
    
    final public static com.navdy.service.library.log.Logger access$getLogger$p(com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu$Companion a) {
        return a.getLogger();
    }
    
    final public static int access$getMessageColor$p(com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu$Companion a) {
        return a.getMessageColor();
    }
    
    final public static android.content.res.Resources access$getResources$p(com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu$Companion a) {
        return a.getResources();
    }
    
    final public static int access$getSELECTION_ANIMATION_DELAY$p(com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu$Companion a) {
        return a.getSELECTION_ANIMATION_DELAY();
    }
    
    final private com.navdy.hud.app.ui.component.vlist.VerticalList$Model getBack() {
        return com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu.access$getBack$cp();
    }
    
    final private int getBaseMessageId() {
        return com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu.access$getBaseMessageId$cp();
    }
    
    final private android.content.Context getContext() {
        return com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu.access$getContext$cp();
    }
    
    final private int getFluctuatorColor() {
        return com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu.access$getFluctuatorColor$cp();
    }
    
    final private com.navdy.service.library.log.Logger getLogger() {
        return com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu.access$getLogger$cp();
    }
    
    final private int getMessageColor() {
        return com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu.access$getMessageColor$cp();
    }
    
    final private android.content.res.Resources getResources() {
        return com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu.access$getResources$cp();
    }
    
    final private int getSELECTION_ANIMATION_DELAY() {
        return com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu.access$getSELECTION_ANIMATION_DELAY$cp();
    }
}
