package com.navdy.hud.app.ui.component.homescreen;


    public enum TbtShrunkDistanceView$Mode {
        PROGRESS_BAR(0),
    DISTANCE_NUMBER(1);

        private int value;
        TbtShrunkDistanceView$Mode(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class TbtShrunkDistanceView$Mode extends Enum {
//    final private static com.navdy.hud.app.ui.component.homescreen.TbtShrunkDistanceView$Mode[] $VALUES;
//    final public static com.navdy.hud.app.ui.component.homescreen.TbtShrunkDistanceView$Mode DISTANCE_NUMBER;
//    final public static com.navdy.hud.app.ui.component.homescreen.TbtShrunkDistanceView$Mode PROGRESS_BAR;
//    
//    static {
//        PROGRESS_BAR = new com.navdy.hud.app.ui.component.homescreen.TbtShrunkDistanceView$Mode("PROGRESS_BAR", 0);
//        DISTANCE_NUMBER = new com.navdy.hud.app.ui.component.homescreen.TbtShrunkDistanceView$Mode("DISTANCE_NUMBER", 1);
//        com.navdy.hud.app.ui.component.homescreen.TbtShrunkDistanceView$Mode[] a = new com.navdy.hud.app.ui.component.homescreen.TbtShrunkDistanceView$Mode[2];
//        a[0] = PROGRESS_BAR;
//        a[1] = DISTANCE_NUMBER;
//        $VALUES = a;
//    }
//    
//    private TbtShrunkDistanceView$Mode(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.ui.component.homescreen.TbtShrunkDistanceView$Mode valueOf(String s) {
//        return (com.navdy.hud.app.ui.component.homescreen.TbtShrunkDistanceView$Mode)Enum.valueOf(com.navdy.hud.app.ui.component.homescreen.TbtShrunkDistanceView$Mode.class, s);
//    }
//    
//    public static com.navdy.hud.app.ui.component.homescreen.TbtShrunkDistanceView$Mode[] values() {
//        return $VALUES.clone();
//    }
//}
//