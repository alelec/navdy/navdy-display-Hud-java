package com.navdy.hud.app.ui.component.vlist;

public class VerticalList$ModelState {
    public boolean updateImage;
    public boolean updateSmallImage;
    public boolean updateSubTitle;
    public boolean updateSubTitle2;
    public boolean updateTitle;
    
    VerticalList$ModelState() {
        this.reset();
    }
    
    void reset() {
        this.updateImage = true;
        this.updateSmallImage = true;
        this.updateTitle = true;
        this.updateSubTitle = true;
        this.updateSubTitle2 = true;
    }
}
