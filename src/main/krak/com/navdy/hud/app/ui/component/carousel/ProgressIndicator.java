package com.navdy.hud.app.ui.component.carousel;
import com.navdy.hud.app.R;

public class ProgressIndicator extends android.view.View implements com.navdy.hud.app.ui.component.carousel.IProgressIndicator {
    private boolean animating;
    private int animatingColor;
    private float animatingPos;
    private int backgroundColor;
    private int barParentSize;
    private int barSize;
    private int blackColor;
    private int currentItem;
    private int currentItemColor;
    private int currentItemPaddingRadius;
    private int defaultColor;
    private boolean fullBackground;
    private int greyColor;
    private int itemCount;
    private int itemPadding;
    private int itemRadius;
    private com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation orientation;
    private android.graphics.Paint paint;
    private int roundRadius;
    private int viewPadding;
    
    public ProgressIndicator(android.content.Context a) {
        super(a);
        this.barSize = -1;
        this.barParentSize = -1;
        this.currentItem = -1;
        this.currentItemColor = -1;
        this.init(a);
    }
    
    public ProgressIndicator(android.content.Context a, android.util.AttributeSet a0) {
        super(a, a0);
        this.barSize = -1;
        this.barParentSize = -1;
        this.currentItem = -1;
        this.currentItemColor = -1;
        this.init(a);
    }
    
    static float access$002(com.navdy.hud.app.ui.component.carousel.ProgressIndicator a, float f) {
        a.animatingPos = f;
        return f;
    }
    
    private void drawItem(android.graphics.Canvas a, float f, float f0, float f1, int i, int i0) {
        this.paint.setColor(this.blackColor);
        if (this.orientation != com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation.HORIZONTAL) {
            a.drawCircle(f / 2f, f1, (float)i, this.paint);
        } else {
            a.drawCircle(f1, f0 / 2f, (float)i, this.paint);
        }
        this.paint.setColor(i0);
        if (this.orientation != com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation.HORIZONTAL) {
            a.drawCircle(f / 2f, f1, (float)this.itemRadius, this.paint);
        } else {
            a.drawCircle(f1, f0 / 2f, (float)this.itemRadius, this.paint);
        }
    }
    
    private float getItemTargetPos(float f, int i) {
        float f0 = 0.0f;
        if (this.fullBackground && this.viewPadding > 0) {
            f = f - (float)(this.viewPadding * 2);
        }
        if (this.itemCount > 0) {
            if (this.itemCount != 1) {
                f0 = (float)(this.itemRadius + this.currentItemPaddingRadius);
                float f1 = f - (float)(this.itemRadius + this.currentItemPaddingRadius);
                float f2 = (i != 0) ? (i != this.itemCount - 1) ? (float)i * (f / (float)(this.itemCount - 1)) : f - (float)(this.itemRadius + this.currentItemPaddingRadius) : (float)(this.itemRadius + this.currentItemPaddingRadius);
                if (!(f2 < f0)) {
                    f0 = (f2 > f1) ? f1 : f2;
                }
            } else {
                f0 = f - (float)(this.itemRadius + this.currentItemPaddingRadius);
            }
        } else {
            f0 = 0.0f;
        }
        return f0;
    }
    
    private void init(android.content.Context a) {
        android.content.res.Resources a0 = this.getResources();
        this.blackColor = a0.getColor(17170444);
        this.greyColor = a0.getColor(R.color.grey_4a);
        this.paint = new android.graphics.Paint();
        this.paint.setStrokeWidth(0.0f);
        this.paint.setAntiAlias(true);
    }
    
    public int getCurrentItem() {
        return this.currentItem;
    }
    
    public android.animation.AnimatorSet getItemMoveAnimator(int i, int i0) {
        float f = (float)this.getWidth();
        float f0 = (float)this.getHeight();
        float f1 = this.getItemTargetPos((this.orientation != com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation.HORIZONTAL) ? f0 : f, this.currentItem);
        float f2 = this.getItemTargetPos((this.orientation != com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation.HORIZONTAL) ? f0 : f, i);
        if (this.fullBackground) {
            if (this.orientation != com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation.HORIZONTAL) {
                f = f0;
            }
            int i1 = (int)this.getItemTargetPos(f, this.itemCount - 1);
            int i2 = (int)f2 + this.viewPadding;
            f2 = (i2 >= i1 - this.viewPadding) ? (float)(i1 - this.viewPadding) : (float)i2;
        }
        float[] a = new float[2];
        a[0] = f1;
        a[1] = f2;
        android.animation.ValueAnimator a0 = android.animation.ValueAnimator.ofFloat(a);
        a0.addUpdateListener((android.animation.ValueAnimator$AnimatorUpdateListener)new com.navdy.hud.app.ui.component.carousel.ProgressIndicator$1(this));
        a0.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.ui.component.carousel.ProgressIndicator$2(this));
        android.animation.AnimatorSet a1 = new android.animation.AnimatorSet();
        a1.play((android.animation.Animator)a0);
        this.animatingPos = f1;
        this.animatingColor = i0;
        this.animating = true;
        return a1;
    }
    
    public android.graphics.RectF getItemPos(int i) {
        android.graphics.RectF a = null;
        label2: {
            float f = 0.0f;
            label0: {
                label1: {
                    if (i < 0) {
                        break label1;
                    }
                    if (i < this.itemCount) {
                        break label0;
                    }
                }
                a = null;
                break label2;
            }
            float f0 = (float)this.getWidth();
            float f1 = (float)this.getHeight();
            if (this.orientation != com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation.HORIZONTAL) {
                f0 = f1;
            }
            float f2 = this.getItemTargetPos(f0, this.currentItem);
            if (this.orientation != com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation.HORIZONTAL) {
                f = f2;
                f2 = 0.0f;
            } else {
                f = 0.0f;
            }
            a = new android.graphics.RectF(f2, f, 0.0f, 0.0f);
        }
        return a;
    }
    
    protected void onDraw(android.graphics.Canvas a) {
        float f = 0.0f;
        float f0 = 0.0f;
        int i = 0;
        int i0 = 0;
        super.onDraw(a);
        float f1 = (float)this.getWidth();
        float f2 = (float)this.getHeight();
        if (this.barSize == -1) {
            f = f2;
            f0 = f1;
            i = 0;
            i0 = 0;
        } else if (this.orientation != com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation.VERTICAL) {
            i0 = (int)(f1 - (float)this.barSize) / 2;
            f = (float)this.barSize;
            f0 = f1;
            i = 0;
        } else {
            i = (int)(f1 - (float)this.barSize) / 2;
            f0 = (float)this.barSize;
            f = f2;
            i0 = 0;
        }
        this.paint.setColor(this.backgroundColor);
        if (this.itemCount != 0) {
            if (this.itemCount != 1) {
                if (this.fullBackground) {
                    a.drawRoundRect(new android.graphics.RectF((float)i, (float)i0, (float)i + f0, (float)i0 + f), (float)this.roundRadius, (float)this.roundRadius, this.paint);
                } else {
                    float f3 = 0.0f;
                    float f4 = 0.0f;
                    android.graphics.RectF a0 = null;
                    if (this.orientation != com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation.HORIZONTAL) {
                        f3 = f1 / 2f;
                        f4 = (float)this.itemRadius;
                        a0 = new android.graphics.RectF(0.0f, (float)(this.itemRadius * 2 + this.itemPadding), f1, f2);
                    } else {
                        f3 = (float)this.itemRadius;
                        f4 = f2 / 2f;
                        a0 = new android.graphics.RectF((float)(this.itemRadius * 2 + this.itemPadding), 0.0f, f1, f2);
                    }
                    a.drawCircle(f3, f4, (float)this.itemRadius, this.paint);
                    a.drawRoundRect(a0, (float)this.roundRadius, (float)this.roundRadius, this.paint);
                }
            } else {
                a.drawRoundRect(new android.graphics.RectF((float)i, (float)i0, (float)i + f0, (float)i0 + f), (float)this.roundRadius, (float)this.roundRadius, this.paint);
            }
            int i1 = this.currentItemPaddingRadius + this.itemRadius;
            if (this.animating) {
                this.drawItem(a, f1, f2, this.animatingPos, i1, this.animatingColor);
            } else if (this.currentItem != -1) {
                float f5 = this.getItemTargetPos((this.orientation != com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation.HORIZONTAL) ? f2 : f1, this.currentItem);
                if (this.fullBackground && this.viewPadding > 0) {
                    if (this.currentItem != 0) {
                        if (this.currentItem != this.itemCount - 1) {
                            int i2 = this.getHeight() - this.viewPadding;
                            int i3 = (int)f5 + this.viewPadding;
                            f5 = (i3 > i2) ? (float)i2 : (float)i3;
                        } else {
                            f5 = (float)(this.getHeight() - this.viewPadding);
                        }
                    } else {
                        f5 = (float)this.viewPadding;
                    }
                }
                int i4 = this.defaultColor;
                if (this.currentItemColor != -1) {
                    i4 = this.currentItemColor;
                }
                this.drawItem(a, f1, f2, f5, i1, i4);
            }
        } else {
            a.drawRoundRect(new android.graphics.RectF((float)i, (float)i0, (float)i + f0, (float)i0 + f), (float)this.roundRadius, (float)this.roundRadius, this.paint);
        }
    }
    
    protected void onMeasure(int i, int i0) {
        super.onMeasure(i, i0);
        if (this.barSize != -1) {
            int i1 = this.getMeasuredWidth();
            int i2 = this.getMeasuredHeight();
            if (this.orientation != com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation.VERTICAL) {
                i2 = this.barParentSize;
            } else {
                i1 = this.barParentSize;
            }
            this.setMeasuredDimension(i1, i2);
        }
    }
    
    public void setBackgroundColor(int i) {
        this.backgroundColor = i;
    }
    
    public void setCurrentItem(int i) {
        this.setCurrentItem(i, -1);
    }
    
    public void setCurrentItem(int i, int i0) {
        if (i >= 0 && i <= this.itemCount - 1) {
            this.currentItem = i;
            this.currentItemColor = i0;
            this.animating = false;
            this.animatingPos = -1f;
            this.animatingColor = -1;
            this.invalidate();
        }
    }
    
    public void setItemCount(int i) {
        if (i <= 0) {
            throw new IllegalArgumentException();
        }
        this.currentItem = -1;
        this.itemCount = i;
    }
    
    public void setOrientation(com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation a) {
        this.orientation = a;
    }
    
    public void setProperties(int i, int i0, int i1, int i2, int i3, int i4, boolean b, int i5, int i6, int i7) {
        this.roundRadius = i;
        this.itemRadius = i0;
        this.itemPadding = i1;
        this.viewPadding = i5;
        this.currentItemPaddingRadius = i2;
        this.defaultColor = i3;
        this.barSize = i6;
        this.barParentSize = i7;
        if (i4 == -1) {
            this.backgroundColor = this.greyColor;
        } else {
            this.backgroundColor = i4;
        }
        this.fullBackground = b;
        if (this.fullBackground && i5 > 0) {
            this.viewPadding = this.viewPadding + i0;
        }
    }
}
