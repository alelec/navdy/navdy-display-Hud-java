package com.navdy.hud.app.ui.component.tbt;
import com.navdy.hud.app.R;

final public class TbtDirectionView extends android.widget.ImageView {
    final public static com.navdy.hud.app.ui.component.tbt.TbtDirectionView$Companion Companion;
    final private static int fullWidth;
    final private static com.navdy.service.library.log.Logger logger;
    final private static int mediumWidth;
    final private static android.content.res.Resources resources;
    private java.util.HashMap _$_findViewCache;
    private com.navdy.hud.app.view.MainView$CustomAnimationMode currentMode;
    private com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState lastManeuverState;
    private int lastTurnIconId;
    
    static {
        Companion = new com.navdy.hud.app.ui.component.tbt.TbtDirectionView$Companion((kotlin.jvm.internal.DefaultConstructorMarker)null);
        logger = new com.navdy.service.library.log.Logger("TbtDirectionView");
        android.content.res.Resources a = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(a, "HudApplication.getAppContext().resources");
        resources = a;
        fullWidth = com.navdy.hud.app.ui.component.tbt.TbtDirectionView$Companion.access$getResources$p(Companion).getDimensionPixelSize(R.dimen.tbt_direction_full_w);
        mediumWidth = com.navdy.hud.app.ui.component.tbt.TbtDirectionView$Companion.access$getResources$p(Companion).getDimensionPixelSize(R.dimen.tbt_direction_medium_w);
    }
    
    public TbtDirectionView(android.content.Context a) {
        super(a);
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "context");
        this.lastTurnIconId = -1;
    }
    
    public TbtDirectionView(android.content.Context a, android.util.AttributeSet a0) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "context");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a0, "attrs");
        super(a, a0);
        this.lastTurnIconId = -1;
    }
    
    public TbtDirectionView(android.content.Context a, android.util.AttributeSet a0, int i) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "context");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a0, "attrs");
        super(a, a0, i);
        this.lastTurnIconId = -1;
    }
    
    final public static int access$getFullWidth$cp() {
        return fullWidth;
    }
    
    final public static com.navdy.service.library.log.Logger access$getLogger$cp() {
        return logger;
    }
    
    final public static int access$getMediumWidth$cp() {
        return mediumWidth;
    }
    
    final public static android.content.res.Resources access$getResources$cp() {
        return resources;
    }
    
    public void _$_clearFindViewByIdCache() {
        if (this._$_findViewCache != null) {
            this._$_findViewCache.clear();
        }
    }
    
    public android.view.View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new java.util.HashMap();
        }
        android.view.View a = (android.view.View)this._$_findViewCache.get(Integer.valueOf(i));
        if (a == null) {
            a = this.findViewById(i);
            this._$_findViewCache.put(Integer.valueOf(i), a);
        }
        return a;
    }
    
    final public void clear() {
        this.lastTurnIconId = -1;
        this.setImageDrawable((android.graphics.drawable.Drawable)null);
    }
    
    final public void getCustomAnimator(com.navdy.hud.app.view.MainView$CustomAnimationMode a, android.animation.AnimatorSet$Builder a0) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "mode");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a0, "mainBuilder");
    }
    
    final public void setMode(com.navdy.hud.app.view.MainView$CustomAnimationMode a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "mode");
        if (!kotlin.jvm.internal.Intrinsics.areEqual(this.currentMode, a)) {
            android.view.ViewGroup$LayoutParams a0 = this.getLayoutParams();
            if (a0 == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            }
            android.view.ViewGroup$MarginLayoutParams a1 = (android.view.ViewGroup$MarginLayoutParams)a0;
            if (kotlin.jvm.internal.Intrinsics.areEqual(a, com.navdy.hud.app.view.MainView$CustomAnimationMode.EXPAND)) {
                a1.width = com.navdy.hud.app.ui.component.tbt.TbtDirectionView$Companion.access$getFullWidth$p(Companion);
                a1.height = com.navdy.hud.app.ui.component.tbt.TbtDirectionView$Companion.access$getFullWidth$p(Companion);
            } else {
                a1.width = com.navdy.hud.app.ui.component.tbt.TbtDirectionView$Companion.access$getMediumWidth$p(Companion);
                a1.height = com.navdy.hud.app.ui.component.tbt.TbtDirectionView$Companion.access$getMediumWidth$p(Companion);
            }
            this.requestLayout();
            this.currentMode = a;
        }
    }
    
    final public void updateDisplay(com.navdy.hud.app.maps.MapEvents$ManeuverDisplay a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "event");
        if (a.turnIconId == -1) {
            this.setImageDrawable((android.graphics.drawable.Drawable)null);
        } else {
            this.setImageResource(a.turnIconId);
        }
        this.lastManeuverState = a.maneuverState;
        this.lastTurnIconId = a.turnIconId;
    }
}
