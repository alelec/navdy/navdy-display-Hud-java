package com.navdy.hud.app.ui.component.tbt;
import com.navdy.hud.app.R;

final public class TbtDistanceView extends android.support.constraint.ConstraintLayout {
    final public static com.navdy.hud.app.ui.component.tbt.TbtDistanceView$Companion Companion;
    final private static int fullWidth;
    final private static com.navdy.service.library.log.Logger logger;
    final private static int mediumWidth;
    final private static int nowHeightFull;
    final private static int nowHeightMedium;
    final private static int nowWidthFull;
    final private static int nowWidthMedium;
    final private static int progressHeightFull;
    final private static int progressHeightMedium;
    final private static android.content.res.Resources resources;
    final private static float size18;
    final private static float size22;
    private java.util.HashMap _$_findViewCache;
    private com.navdy.hud.app.view.MainView$CustomAnimationMode currentMode;
    private android.widget.TextView distanceView;
    private int initialProgressBarDistance;
    private String lastDistance;
    private com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState lastManeuverState;
    private android.widget.ImageView nowView;
    private android.animation.ValueAnimator progressAnimator;
    private android.widget.ProgressBar progressView;
    
    static {
        Companion = new com.navdy.hud.app.ui.component.tbt.TbtDistanceView$Companion((kotlin.jvm.internal.DefaultConstructorMarker)null);
        logger = new com.navdy.service.library.log.Logger("TbtDistanceView");
        android.content.res.Resources a = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(a, "HudApplication.getAppContext().resources");
        resources = a;
        fullWidth = com.navdy.hud.app.ui.component.tbt.TbtDistanceView$Companion.access$getResources$p(Companion).getDimensionPixelSize(R.dimen.tbt_distance_full_w);
        mediumWidth = com.navdy.hud.app.ui.component.tbt.TbtDistanceView$Companion.access$getResources$p(Companion).getDimensionPixelSize(R.dimen.tbt_distance_medium_w);
        size22 = com.navdy.hud.app.ui.component.tbt.TbtDistanceView$Companion.access$getResources$p(Companion).getDimension(R.dimen.tbt_instruction_22);
        size18 = com.navdy.hud.app.ui.component.tbt.TbtDistanceView$Companion.access$getResources$p(Companion).getDimension(R.dimen.tbt_instruction_18);
        progressHeightFull = com.navdy.hud.app.ui.component.tbt.TbtDistanceView$Companion.access$getResources$p(Companion).getDimensionPixelSize(R.dimen.tbt_distance_progress_h_full);
        progressHeightMedium = com.navdy.hud.app.ui.component.tbt.TbtDistanceView$Companion.access$getResources$p(Companion).getDimensionPixelSize(R.dimen.tbt_distance_progress_h_medium);
        nowWidthFull = com.navdy.hud.app.ui.component.tbt.TbtDistanceView$Companion.access$getResources$p(Companion).getDimensionPixelSize(R.dimen.tbt_distance_now_w_full);
        nowHeightFull = com.navdy.hud.app.ui.component.tbt.TbtDistanceView$Companion.access$getResources$p(Companion).getDimensionPixelSize(R.dimen.tbt_distance_now_h_full);
        nowWidthMedium = com.navdy.hud.app.ui.component.tbt.TbtDistanceView$Companion.access$getResources$p(Companion).getDimensionPixelSize(R.dimen.tbt_distance_now_w_medium);
        nowHeightMedium = com.navdy.hud.app.ui.component.tbt.TbtDistanceView$Companion.access$getResources$p(Companion).getDimensionPixelSize(R.dimen.tbt_distance_now_h_medium);
    }
    
    public TbtDistanceView(android.content.Context a) {
        super(a);
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "context");
    }
    
    public TbtDistanceView(android.content.Context a, android.util.AttributeSet a0) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "context");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a0, "attrs");
        super(a, a0);
    }
    
    public TbtDistanceView(android.content.Context a, android.util.AttributeSet a0, int i) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "context");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a0, "attrs");
        super(a, a0, i);
    }
    
    final public static int access$getFullWidth$cp() {
        return fullWidth;
    }
    
    final public static com.navdy.service.library.log.Logger access$getLogger$cp() {
        return logger;
    }
    
    final public static int access$getMediumWidth$cp() {
        return mediumWidth;
    }
    
    final public static int access$getNowHeightFull$cp() {
        return nowHeightFull;
    }
    
    final public static int access$getNowHeightMedium$cp() {
        return nowHeightMedium;
    }
    
    final public static int access$getNowWidthFull$cp() {
        return nowWidthFull;
    }
    
    final public static int access$getNowWidthMedium$cp() {
        return nowWidthMedium;
    }
    
    final public static int access$getProgressHeightFull$cp() {
        return progressHeightFull;
    }
    
    final public static int access$getProgressHeightMedium$cp() {
        return progressHeightMedium;
    }
    
    final public static android.content.res.Resources access$getResources$cp() {
        return resources;
    }
    
    final public static float access$getSize18$cp() {
        return size18;
    }
    
    final public static float access$getSize22$cp() {
        return size22;
    }
    
    final private void cancelProgressAnimator() {
        android.animation.ValueAnimator a = this.progressAnimator;
        if (a != null && a.isRunning()) {
            android.animation.ValueAnimator a0 = this.progressAnimator;
            if (a0 != null) {
                a0.removeAllListeners();
            }
            android.animation.ValueAnimator a1 = this.progressAnimator;
            if (a1 != null) {
                a1.cancel();
            }
            this.progressAnimator = null;
        }
    }
    
    final private void setDistance(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState a, String s, long j) {
        boolean b = android.text.TextUtils.equals((CharSequence)s, (CharSequence)this.lastDistance);
        label12: {
            label13: {
                if (!b) {
                    break label13;
                }
                if (kotlin.jvm.internal.Intrinsics.areEqual(this.lastManeuverState, a)) {
                    break label12;
                }
            }
            if (a != null) {
                boolean b0 = false;
                boolean b1 = false;
                boolean b2 = false;
                boolean b3 = false;
                this.lastDistance = s;
                android.widget.TextView a0 = this.distanceView;
                if (a0 != null) {
                    if (s == null) {
                        s = "";
                    }
                    a0.setText((CharSequence)s);
                }
                boolean b4 = kotlin.jvm.internal.Intrinsics.areEqual(this.lastManeuverState, com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.NOW) ^ true;
                label11: {
                    label9: {
                        label10: {
                            if (!b4) {
                                break label10;
                            }
                            if (kotlin.jvm.internal.Intrinsics.areEqual(a, com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.NOW)) {
                                break label9;
                            }
                        }
                        b0 = false;
                        break label11;
                    }
                    b0 = true;
                }
                boolean b5 = kotlin.jvm.internal.Intrinsics.areEqual(this.lastManeuverState, com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.NOW);
                label8: {
                    label6: {
                        label7: {
                            if (!b5) {
                                break label7;
                            }
                            if (kotlin.jvm.internal.Intrinsics.areEqual(a, com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.NOW) ^ true) {
                                break label6;
                            }
                        }
                        b1 = false;
                        break label8;
                    }
                    b1 = true;
                }
                if (b0) {
                    android.widget.ImageView a1 = this.nowView;
                    if (a1 != null) {
                        a1.setAlpha(0.0f);
                    }
                    android.widget.ImageView a2 = this.nowView;
                    if (a2 != null) {
                        a2.setVisibility(0);
                    }
                    android.animation.AnimatorSet a3 = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getFadeInAndScaleUpAnimator((android.view.View)this.nowView);
                    this.cancelProgressAnimator();
                    android.widget.ProgressBar a4 = this.progressView;
                    if (a4 != null) {
                        a4.setVisibility(8);
                    }
                    android.widget.TextView a5 = this.distanceView;
                    if (a5 != null) {
                        a5.setAlpha(0.0f);
                    }
                    android.animation.AnimatorSet a6 = new android.animation.AnimatorSet();
                    a6.play((android.animation.Animator)a3);
                    a6.setDuration(com.navdy.hud.app.ui.component.tbt.TbtViewContainer.Companion.getMANEUVER_PROGRESS_ANIMATION_DURATION()).start();
                } else if (b1) {
                    android.widget.ImageView a7 = this.nowView;
                    if (a7 != null) {
                        a7.setVisibility(8);
                    }
                    android.widget.ImageView a8 = this.nowView;
                    if (a8 != null) {
                        a8.setAlpha(0.0f);
                    }
                    android.animation.ObjectAnimator a9 = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getAlphaAnimator((android.view.View)this.distanceView, 1);
                    android.animation.AnimatorSet a10 = new android.animation.AnimatorSet();
                    a10.play((android.animation.Animator)a9);
                    android.animation.AnimatorSet a11 = a10.setDuration(com.navdy.hud.app.ui.component.tbt.TbtViewContainer.Companion.getMANEUVER_PROGRESS_ANIMATION_DURATION());
                    if (a11 != null) {
                        a11.start();
                    }
                } else {
                    android.widget.ImageView a12 = this.nowView;
                    Integer a13 = (a12 == null) ? null : Integer.valueOf(a12.getVisibility());
                    if (kotlin.jvm.internal.Intrinsics.areEqual(a13, Integer.valueOf(0)) ^ true) {
                        android.widget.TextView a14 = this.distanceView;
                        if (a14 != null) {
                            a14.setAlpha(1f);
                        }
                    }
                }
                boolean b6 = kotlin.jvm.internal.Intrinsics.areEqual(this.lastManeuverState, com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.SOON) ^ true;
                label5: {
                    label3: {
                        label4: {
                            if (!b6) {
                                break label4;
                            }
                            if (kotlin.jvm.internal.Intrinsics.areEqual(a, com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.SOON)) {
                                break label3;
                            }
                        }
                        b2 = false;
                        break label5;
                    }
                    b2 = true;
                }
                boolean b7 = kotlin.jvm.internal.Intrinsics.areEqual(this.lastManeuverState, com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.SOON);
                label2: {
                    label0: {
                        label1: {
                            if (!b7) {
                                break label1;
                            }
                            if (kotlin.jvm.internal.Intrinsics.areEqual(a, com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.SOON) ^ true) {
                                break label0;
                            }
                        }
                        b3 = false;
                        break label2;
                    }
                    b3 = true;
                }
                if (b3) {
                    android.widget.ProgressBar a15 = this.progressView;
                    if (a15 != null) {
                        a15.setVisibility(8);
                    }
                    this.cancelProgressAnimator();
                    this.initialProgressBarDistance = 0;
                } else if (b2) {
                    this.cancelProgressAnimator();
                    this.initialProgressBarDistance = (int)j;
                    android.widget.ProgressBar a16 = this.progressView;
                    if (a16 != null) {
                        a16.setProgress(0);
                    }
                    android.widget.ProgressBar a17 = this.progressView;
                    if (a17 != null) {
                        a17.setVisibility(0);
                    }
                    android.widget.ProgressBar a18 = this.progressView;
                    if (a18 != null) {
                        a18.requestLayout();
                    }
                } else if (kotlin.jvm.internal.Intrinsics.areEqual(a, com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.SOON)) {
                    double d = (double)1;
                    double d0 = (double)j / (double)this.initialProgressBarDistance;
                    com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getProgressBarAnimator(this.progressView, (int)((double)100 * (d - d0))).setDuration(com.navdy.hud.app.ui.component.tbt.TbtViewContainer.Companion.getMANEUVER_PROGRESS_ANIMATION_DURATION()).start();
                }
                this.setVisibility(0);
            } else {
                this.cancelProgressAnimator();
                android.widget.ProgressBar a19 = this.progressView;
                if (a19 != null) {
                    a19.setVisibility(8);
                }
                android.widget.TextView a20 = this.distanceView;
                if (a20 != null) {
                    a20.setText((CharSequence)"");
                }
                android.widget.ImageView a21 = this.nowView;
                if (a21 != null) {
                    a21.setAlpha(0.0f);
                }
                android.widget.ImageView a22 = this.nowView;
                if (a22 != null) {
                    a22.setScaleX(0.9f);
                }
                android.widget.ImageView a23 = this.nowView;
                if (a23 != null) {
                    a23.setScaleY(0.9f);
                }
                android.widget.ImageView a24 = this.nowView;
                if (a24 != null) {
                    a24.setVisibility(8);
                }
                this.setVisibility(8);
            }
        }
    }
    
    public void _$_clearFindViewByIdCache() {
        if (this._$_findViewCache != null) {
            this._$_findViewCache.clear();
        }
    }
    
    public android.view.View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new java.util.HashMap();
        }
        android.view.View a = (android.view.View)this._$_findViewCache.get(Integer.valueOf(i));
        if (a == null) {
            a = this.findViewById(i);
            this._$_findViewCache.put(Integer.valueOf(i), a);
        }
        return a;
    }
    
    final public void clear() {
        this.lastDistance = null;
        android.widget.TextView a = this.distanceView;
        if (a != null) {
            a.setText((CharSequence)"");
        }
        this.cancelProgressAnimator();
        android.widget.ProgressBar a0 = this.progressView;
        if (a0 != null) {
            a0.setVisibility(8);
        }
    }
    
    final public void getCustomAnimator(com.navdy.hud.app.view.MainView$CustomAnimationMode a, android.animation.AnimatorSet$Builder a0) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "mode");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a0, "mainBuilder");
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        android.view.View a = this.findViewById(R.id.distance);
        if (a == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.widget.TextView");
        }
        this.distanceView = (android.widget.TextView)a;
        android.view.View a0 = this.findViewById(R.id.progress);
        if (a0 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.widget.ProgressBar");
        }
        this.progressView = (android.widget.ProgressBar)a0;
        android.view.View a1 = this.findViewById(R.id.now);
        if (a1 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.widget.ImageView");
        }
        this.nowView = (android.widget.ImageView)a1;
    }
    
    final public void setMode(com.navdy.hud.app.view.MainView$CustomAnimationMode a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "mode");
        if (!kotlin.jvm.internal.Intrinsics.areEqual(this.currentMode, a)) {
            int i = 0;
            float f = 0.0f;
            int i0 = 0;
            int i1 = 0;
            int i2 = 0;
            switch(com.navdy.hud.app.ui.component.tbt.TbtDistanceView$WhenMappings.$EnumSwitchMapping$0[a.ordinal()]) {
                case 2: {
                    i = com.navdy.hud.app.ui.component.tbt.TbtDistanceView$Companion.access$getMediumWidth$p(Companion);
                    f = com.navdy.hud.app.ui.component.tbt.TbtDistanceView$Companion.access$getSize18$p(Companion);
                    i0 = com.navdy.hud.app.ui.component.tbt.TbtDistanceView$Companion.access$getProgressHeightMedium$p(Companion);
                    i1 = com.navdy.hud.app.ui.component.tbt.TbtDistanceView$Companion.access$getNowWidthMedium$p(Companion);
                    i2 = com.navdy.hud.app.ui.component.tbt.TbtDistanceView$Companion.access$getNowHeightMedium$p(Companion);
                    break;
                }
                case 1: {
                    i = com.navdy.hud.app.ui.component.tbt.TbtDistanceView$Companion.access$getFullWidth$p(Companion);
                    f = com.navdy.hud.app.ui.component.tbt.TbtDistanceView$Companion.access$getSize22$p(Companion);
                    i0 = com.navdy.hud.app.ui.component.tbt.TbtDistanceView$Companion.access$getProgressHeightFull$p(Companion);
                    i1 = com.navdy.hud.app.ui.component.tbt.TbtDistanceView$Companion.access$getNowWidthFull$p(Companion);
                    i2 = com.navdy.hud.app.ui.component.tbt.TbtDistanceView$Companion.access$getNowHeightFull$p(Companion);
                    break;
                }
                default: {
                    i = 0;
                    f = 0.0f;
                    i0 = 0;
                    i1 = 0;
                    i2 = 0;
                }
            }
            android.view.ViewGroup$LayoutParams a0 = this.getLayoutParams();
            if (a0 == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            }
            ((android.view.ViewGroup$MarginLayoutParams)a0).width = i;
            android.widget.TextView a1 = this.distanceView;
            if (a1 != null) {
                a1.setTextSize(f);
            }
            android.widget.ProgressBar a2 = this.progressView;
            android.view.ViewGroup$LayoutParams a3 = (a2 == null) ? null : a2.getLayoutParams();
            if (a3 == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            }
            ((android.view.ViewGroup$MarginLayoutParams)a3).height = i0;
            android.widget.ImageView a4 = this.nowView;
            android.view.ViewGroup$LayoutParams a5 = (a4 == null) ? null : a4.getLayoutParams();
            if (a5 == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            }
            android.view.ViewGroup$MarginLayoutParams a6 = (android.view.ViewGroup$MarginLayoutParams)a5;
            a6.width = i1;
            a6.height = i2;
            this.invalidate();
            this.requestLayout();
            this.currentMode = a;
            com.navdy.hud.app.ui.component.tbt.TbtDistanceView$Companion.access$getLogger$p(Companion).v(new StringBuilder().append("view width = ").append(a6.width).toString());
        }
    }
    
    final public void updateDisplay(com.navdy.hud.app.maps.MapEvents$ManeuverDisplay a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "event");
        this.setDistance(a.maneuverState, a.distanceToPendingRoadText, a.distanceInMeters);
        this.lastManeuverState = a.maneuverState;
    }
}
