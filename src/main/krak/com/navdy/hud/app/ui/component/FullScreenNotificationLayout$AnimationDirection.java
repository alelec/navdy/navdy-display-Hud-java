package com.navdy.hud.app.ui.component;


    public enum FullScreenNotificationLayout$AnimationDirection {
        UP(0),
        DOWN(1);

        private int value;
        FullScreenNotificationLayout$AnimationDirection(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class FullScreenNotificationLayout$AnimationDirection extends Enum {
//    final private static com.navdy.hud.app.ui.component.FullScreenNotificationLayout$AnimationDirection[] $VALUES;
//    final public static com.navdy.hud.app.ui.component.FullScreenNotificationLayout$AnimationDirection DOWN;
//    final public static com.navdy.hud.app.ui.component.FullScreenNotificationLayout$AnimationDirection UP;
//    
//    static {
//        UP = new com.navdy.hud.app.ui.component.FullScreenNotificationLayout$AnimationDirection("UP", 0);
//        DOWN = new com.navdy.hud.app.ui.component.FullScreenNotificationLayout$AnimationDirection("DOWN", 1);
//        com.navdy.hud.app.ui.component.FullScreenNotificationLayout$AnimationDirection[] a = new com.navdy.hud.app.ui.component.FullScreenNotificationLayout$AnimationDirection[2];
//        a[0] = UP;
//        a[1] = DOWN;
//        $VALUES = a;
//    }
//    
//    private FullScreenNotificationLayout$AnimationDirection(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.ui.component.FullScreenNotificationLayout$AnimationDirection valueOf(String s) {
//        return (com.navdy.hud.app.ui.component.FullScreenNotificationLayout$AnimationDirection)Enum.valueOf(com.navdy.hud.app.ui.component.FullScreenNotificationLayout$AnimationDirection.class, s);
//    }
//    
//    public static com.navdy.hud.app.ui.component.FullScreenNotificationLayout$AnimationDirection[] values() {
//        return $VALUES.clone();
//    }
//}
//