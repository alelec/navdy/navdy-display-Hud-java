package com.navdy.hud.app.ui.component.homescreen;

class SmartDashView$1 implements com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$IWidgetFilter {
    final com.navdy.hud.app.ui.component.homescreen.SmartDashView this$0;
    final com.navdy.hud.app.ui.component.homescreen.HomeScreenView val$homeScreenView;
    
    SmartDashView$1(com.navdy.hud.app.ui.component.homescreen.SmartDashView a, com.navdy.hud.app.ui.component.homescreen.HomeScreenView a0) {
        super();
        this.this$0 = a;
        this.val$homeScreenView = a0;
    }
    
    public boolean filter(String s) {
        boolean b = false;
        android.content.SharedPreferences a = this.val$homeScreenView.getDriverPreferences();
        String s0 = a.getString("PREFERENCE_LEFT_GAUGE", "ANALOG_CLOCK_WIDGET");
        String s1 = a.getString("PREFERENCE_RIGHT_GAUGE", "COMPASS_WIDGET");
        boolean b0 = android.text.TextUtils.isEmpty((CharSequence)s);
        label4: {
            int i = 0;
            label3: {
                if (b0) {
                    break label3;
                }
                if (s.equals(s0)) {
                    b = false;
                    break label4;
                }
                if (!s.equals(s1)) {
                    break label3;
                }
                b = false;
                break label4;
            }
            switch(s.hashCode()) {
                case 1872543020: {
                    i = (s.equals("TRAFFIC_INCIDENT_GAUGE_ID")) ? 0 : -1;
                    break;
                }
                case 1168400042: {
                    i = (s.equals("FUEL_GAUGE_ID")) ? 1 : -1;
                    break;
                }
                case -131933527: {
                    i = (s.equals("ENGINE_TEMPERATURE_GAUGE_ID")) ? 2 : -1;
                    break;
                }
                case -1158963060: {
                    i = (s.equals("MPG_AVG_WIDGET")) ? 3 : -1;
                    break;
                }
                default: {
                    i = -1;
                }
            }
            switch(i) {
                case 3: {
                    com.navdy.obd.PidSet a0 = com.navdy.hud.app.obd.ObdManager.getInstance().getSupportedPids();
                    label2: {
                        if (a0 == null) {
                            break label2;
                        }
                        if (a0.contains(256)) {
                            b = false;
                            break;
                        }
                    }
                    b = true;
                    break;
                }
                case 2: {
                    com.navdy.obd.PidSet a1 = com.navdy.hud.app.obd.ObdManager.getInstance().getSupportedPids();
                    label1: {
                        if (a1 == null) {
                            break label1;
                        }
                        if (a1.contains(5)) {
                            b = false;
                            break;
                        }
                    }
                    b = true;
                    break;
                }
                case 1: {
                    com.navdy.obd.PidSet a2 = com.navdy.hud.app.obd.ObdManager.getInstance().getSupportedPids();
                    label0: {
                        if (a2 == null) {
                            break label0;
                        }
                        if (a2.contains(47)) {
                            b = false;
                            break;
                        }
                    }
                    b = true;
                    break;
                }
                case 0: {
                    b = !com.navdy.hud.app.maps.MapSettings.isTrafficDashWidgetsEnabled();
                    break;
                }
                default: {
                    b = false;
                }
            }
        }
        return b;
    }
}
