package com.navdy.hud.app.ui.component.homescreen;
import com.navdy.hud.app.R;

class SmartDashView$GaugeViewPagerAdapter implements com.navdy.hud.app.ui.component.dashboard.GaugeViewPager$Adapter {
    private int mExcludedPosition;
    private boolean mLeft;
    private com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager mSmartDashWidgetManager;
    private com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$SmartDashWidgetCache mWidgetCache;
    final com.navdy.hud.app.ui.component.homescreen.SmartDashView this$0;
    
    public SmartDashView$GaugeViewPagerAdapter(com.navdy.hud.app.ui.component.homescreen.SmartDashView a, com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager a0, boolean b) {
        super();
        this.this$0 = a;
        this.mExcludedPosition = -1;
        this.mSmartDashWidgetManager = a0;
        this.mLeft = b;
        this.mWidgetCache = this.mSmartDashWidgetManager.buildSmartDashWidgetCache((!this.mLeft) ? 1 : 0);
        this.mSmartDashWidgetManager.registerForChanges(this);
    }
    
    public int getCount() {
        return this.mWidgetCache.getWidgetsCount();
    }
    
    public int getExcludedPosition() {
        return this.mExcludedPosition;
    }
    
    public com.navdy.hud.app.view.DashboardWidgetPresenter getPresenter(int i) {
        return this.mWidgetCache.getWidgetPresenter(i);
    }
    
    public android.view.View getView(int i, android.view.View a, android.view.ViewGroup a0, boolean b) {
        com.navdy.hud.app.view.DashboardWidgetView a1 = null;
        if (a != null) {
            a1 = (com.navdy.hud.app.view.DashboardWidgetView)a;
        } else {
            a1 = new com.navdy.hud.app.view.DashboardWidgetView(a0.getContext());
            a1.setLayoutParams(new android.view.ViewGroup$LayoutParams(a0.getResources().getDimensionPixelSize(R.dimen.gauge_frame_width), a0.getResources().getDimensionPixelSize(R.dimen.gauge_frame_height)));
        }
        com.navdy.hud.app.view.DashboardWidgetPresenter a2 = (com.navdy.hud.app.view.DashboardWidgetPresenter)a1.getTag();
        com.navdy.hud.app.view.DashboardWidgetPresenter a3 = this.mWidgetCache.getWidgetPresenter(i);
        if (a2 != null && a2 != a3 && a2.getWidgetView() == a1) {
            a2.setView((com.navdy.hud.app.view.DashboardWidgetView)null, (android.os.Bundle)null);
        }
        if (a3 != null) {
            android.os.Bundle a4 = new android.os.Bundle();
            a4.putInt("EXTRA_GRAVITY", (this.mLeft) ? 0 : 2);
            a4.putBoolean("EXTRA_IS_ACTIVE", b);
            if (b) {
                boolean b0 = false;
                if (this.mLeft) {
                    com.navdy.hud.app.ui.component.homescreen.SmartDashView.access$702(this.this$0, a3.getWidgetIdentifier());
                } else {
                    com.navdy.hud.app.ui.component.homescreen.SmartDashView.access$802(this.this$0, a3.getWidgetIdentifier());
                }
                com.navdy.hud.app.ui.component.homescreen.SmartDashView a5 = this.this$0;
                boolean b1 = "DRIVE_SCORE_GAUGE_ID".equals(com.navdy.hud.app.ui.component.homescreen.SmartDashView.access$700(this.this$0));
                label0: {
                    label1: {
                        if (b1) {
                            break label1;
                        }
                        if (!"DRIVE_SCORE_GAUGE_ID".equals(com.navdy.hud.app.ui.component.homescreen.SmartDashView.access$800(this.this$0))) {
                            b0 = false;
                            break label0;
                        }
                    }
                    b0 = true;
                }
                a5.isShowingDriveScoreGauge = b0;
                a3.setWidgetVisibleToUser(true);
            } else {
                a3.setWidgetVisibleToUser(false);
            }
            a3.setView(a1, a4);
        }
        a1.setTag(a3);
        return a1;
    }
    
    public void onWidgetsReload(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$Reload a) {
        if (a == com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$Reload.RELOAD_CACHE) {
            this.mWidgetCache = this.mSmartDashWidgetManager.buildSmartDashWidgetCache((!this.mLeft) ? 1 : 0);
        }
    }
    
    public void setExcludedPosition(int i) {
        if (this.mSmartDashWidgetManager.getIndexForWidgetIdentifier("EMPTY_WIDGET") != i) {
            this.mExcludedPosition = i;
        } else {
            this.mExcludedPosition = -1;
        }
    }
}
