package com.navdy.hud.app.ui.component.mainmenu;


    public enum NumberPickerMenu$Mode {
        MESSAGE(0),
        CALL(1),
        SHARE_TRIP(2);

        private int value;
        NumberPickerMenu$Mode(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class NumberPickerMenu$Mode extends Enum {
//    final private static com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Mode[] $VALUES;
//    final public static com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Mode CALL;
//    final public static com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Mode MESSAGE;
//    final public static com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Mode SHARE_TRIP;
//    
//    static {
//        com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Mode a = new com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Mode("MESSAGE", 0);
//        MESSAGE = a;
//        com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Mode a0 = new com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Mode("CALL", 1);
//        CALL = a0;
//        com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Mode a1 = new com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Mode("SHARE_TRIP", 2);
//        SHARE_TRIP = a1;
//        com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Mode[] a2 = new com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Mode[3];
//        a2[0] = a;
//        a2[1] = a0;
//        a2[2] = a1;
//        $VALUES = a2;
//    }
//    
//    protected NumberPickerMenu$Mode(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Mode valueOf(String s) {
//        return (com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Mode)Enum.valueOf(com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Mode.class, s);
//    }
//    
//    public static com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Mode[] values() {
//        return $VALUES.clone();
//    }
//}
//