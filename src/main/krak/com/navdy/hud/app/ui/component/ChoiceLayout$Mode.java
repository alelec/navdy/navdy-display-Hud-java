package com.navdy.hud.app.ui.component;


public enum ChoiceLayout$Mode {
    LABEL(0),
    ICON(1);

    private int value;
    ChoiceLayout$Mode(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class ChoiceLayout$Mode extends Enum {
//    final private static com.navdy.hud.app.ui.component.ChoiceLayout$Mode[] $VALUES;
//    final public static com.navdy.hud.app.ui.component.ChoiceLayout$Mode ICON;
//    final public static com.navdy.hud.app.ui.component.ChoiceLayout$Mode LABEL;
//    
//    static {
//        LABEL = new com.navdy.hud.app.ui.component.ChoiceLayout$Mode("LABEL", 0);
//        ICON = new com.navdy.hud.app.ui.component.ChoiceLayout$Mode("ICON", 1);
//        com.navdy.hud.app.ui.component.ChoiceLayout$Mode[] a = new com.navdy.hud.app.ui.component.ChoiceLayout$Mode[2];
//        a[0] = LABEL;
//        a[1] = ICON;
//        $VALUES = a;
//    }
//    
//    private ChoiceLayout$Mode(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.ui.component.ChoiceLayout$Mode valueOf(String s) {
//        return (com.navdy.hud.app.ui.component.ChoiceLayout$Mode)Enum.valueOf(com.navdy.hud.app.ui.component.ChoiceLayout$Mode.class, s);
//    }
//    
//    public static com.navdy.hud.app.ui.component.ChoiceLayout$Mode[] values() {
//        return $VALUES.clone();
//    }
//}
//