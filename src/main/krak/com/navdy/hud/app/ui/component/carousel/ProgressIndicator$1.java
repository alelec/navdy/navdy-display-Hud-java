package com.navdy.hud.app.ui.component.carousel;

class ProgressIndicator$1 implements android.animation.ValueAnimator$AnimatorUpdateListener {
    final com.navdy.hud.app.ui.component.carousel.ProgressIndicator this$0;
    
    ProgressIndicator$1(com.navdy.hud.app.ui.component.carousel.ProgressIndicator a) {
        super();
        this.this$0 = a;
    }
    
    public void onAnimationUpdate(android.animation.ValueAnimator a) {
        com.navdy.hud.app.ui.component.carousel.ProgressIndicator.access$002(this.this$0, ((Float)a.getAnimatedValue()).floatValue());
        this.this$0.invalidate();
    }
}
