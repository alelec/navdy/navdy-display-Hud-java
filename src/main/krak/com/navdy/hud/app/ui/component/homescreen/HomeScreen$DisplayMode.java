package com.navdy.hud.app.ui.component.homescreen;


public enum HomeScreen$DisplayMode {
    MAP(0),
    SMART_DASH(1);

    private int value;
    HomeScreen$DisplayMode(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class HomeScreen$DisplayMode extends Enum {
//    final private static com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode[] $VALUES;
//    final public static com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode MAP;
//    final public static com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode SMART_DASH;
//    
//    static {
//        MAP = new com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode("MAP", 0);
//        SMART_DASH = new com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode("SMART_DASH", 1);
//        com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode[] a = new com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode[2];
//        a[0] = MAP;
//        a[1] = SMART_DASH;
//        $VALUES = a;
//    }
//    
//    private HomeScreen$DisplayMode(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode valueOf(String s) {
//        return (com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode)Enum.valueOf(com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.class, s);
//    }
//    
//    public static com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode[] values() {
//        return $VALUES.clone();
//    }
//}
//