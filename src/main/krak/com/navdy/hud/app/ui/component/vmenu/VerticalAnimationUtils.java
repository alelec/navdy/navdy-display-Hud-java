package com.navdy.hud.app.ui.component.vmenu;

public class VerticalAnimationUtils {
    public VerticalAnimationUtils() {
    }
    
    public static android.animation.Animator animateDimension(android.view.View a, int i) {
        android.view.ViewGroup$MarginLayoutParams a0 = (android.view.ViewGroup$MarginLayoutParams)a.getLayoutParams();
        int[] a1 = new int[2];
        a1[0] = a0.width;
        a1[1] = i;
        android.animation.ValueAnimator a2 = android.animation.ValueAnimator.ofInt(a1);
        a2.addUpdateListener((android.animation.ValueAnimator$AnimatorUpdateListener)new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils$4(a0, a));
        return a2;
    }
    
    public static android.animation.Animator animateMargin(android.view.View a, int i) {
        android.view.ViewGroup$MarginLayoutParams a0 = (android.view.ViewGroup$MarginLayoutParams)a.getLayoutParams();
        int[] a1 = new int[2];
        a1[0] = a0.topMargin;
        a1[1] = i;
        android.animation.ValueAnimator a2 = android.animation.ValueAnimator.ofInt(a1);
        a2.addUpdateListener((android.animation.ValueAnimator$AnimatorUpdateListener)new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils$3(a0, a));
        return a2;
    }
    
    public static void copyImage(android.widget.ImageView a, android.widget.ImageView a0) {
        android.graphics.Bitmap a1 = com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.getBitmapFromImageView(a);
        if (a1 != null) {
            a0.setImageBitmap(a1);
        }
    }
    
    public static android.graphics.Bitmap getBitmapFromImageView(android.widget.ImageView a) {
        android.graphics.Bitmap a0 = null;
        android.graphics.drawable.Drawable a1 = a.getDrawable();
        boolean b = a1 instanceof android.graphics.drawable.BitmapDrawable;
        android.graphics.drawable.BitmapDrawable a2 = null;
        if (b) {
            a2 = (android.graphics.drawable.BitmapDrawable)a1;
        }
        boolean b0 = a instanceof com.navdy.hud.app.ui.component.image.IconColorImageView;
        label1: {
            label0: {
                if (b0) {
                    break label0;
                }
                if (a2 == null) {
                    break label0;
                }
                a0 = a2.getBitmap();
                break label1;
            }
            a0 = android.graphics.Bitmap.createBitmap(a.getWidth(), a.getHeight(), android.graphics.Bitmap.Config.ARGB_8888);
            a.draw(new android.graphics.Canvas(a0));
        }
        return a0;
    }
    
    public static void performClick(android.view.View a, int i, Runnable a0) {
        com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.performClickDown(a, i / 2, a0, true);
    }
    
    public static void performClickDown(android.view.View a, int i, Runnable a0, boolean b) {
        a.animate().scaleX(0.8f).scaleY(0.8f).setDuration((long)i).withEndAction((Runnable)new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils$1(b, a, i, a0));
    }
    
    public static void performClickUp(android.view.View a, int i, Runnable a0) {
        a.animate().scaleX(1f).scaleY(1f).setDuration((long)i).withEndAction((Runnable)new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils$2(a0)).start();
    }
}
