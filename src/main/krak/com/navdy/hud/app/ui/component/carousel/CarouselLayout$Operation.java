package com.navdy.hud.app.ui.component.carousel;


    public enum CarouselLayout$Operation {
        FORWARD(0),
        BACK(1),
        SELECT(2),
        OTHER(3);

        private int value;
        CarouselLayout$Operation(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class CarouselLayout$Operation extends Enum {
//    final private static com.navdy.hud.app.ui.component.carousel.CarouselLayout$Operation[] $VALUES;
//    final public static com.navdy.hud.app.ui.component.carousel.CarouselLayout$Operation BACK;
//    final public static com.navdy.hud.app.ui.component.carousel.CarouselLayout$Operation FORWARD;
//    final public static com.navdy.hud.app.ui.component.carousel.CarouselLayout$Operation OTHER;
//    final public static com.navdy.hud.app.ui.component.carousel.CarouselLayout$Operation SELECT;
//    
//    static {
//        FORWARD = new com.navdy.hud.app.ui.component.carousel.CarouselLayout$Operation("FORWARD", 0);
//        BACK = new com.navdy.hud.app.ui.component.carousel.CarouselLayout$Operation("BACK", 1);
//        SELECT = new com.navdy.hud.app.ui.component.carousel.CarouselLayout$Operation("SELECT", 2);
//        OTHER = new com.navdy.hud.app.ui.component.carousel.CarouselLayout$Operation("OTHER", 3);
//        com.navdy.hud.app.ui.component.carousel.CarouselLayout$Operation[] a = new com.navdy.hud.app.ui.component.carousel.CarouselLayout$Operation[4];
//        a[0] = FORWARD;
//        a[1] = BACK;
//        a[2] = SELECT;
//        a[3] = OTHER;
//        $VALUES = a;
//    }
//    
//    private CarouselLayout$Operation(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.ui.component.carousel.CarouselLayout$Operation valueOf(String s) {
//        return (com.navdy.hud.app.ui.component.carousel.CarouselLayout$Operation)Enum.valueOf(com.navdy.hud.app.ui.component.carousel.CarouselLayout$Operation.class, s);
//    }
//    
//    public static com.navdy.hud.app.ui.component.carousel.CarouselLayout$Operation[] values() {
//        return $VALUES.clone();
//    }
//}
//