package com.navdy.hud.app.ui.activity;
import com.navdy.hud.app.R;

public class MainActivity$$ViewInjector {
    public MainActivity$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.ui.activity.MainActivity a0, Object a1) {
        a0.container = (com.navdy.hud.app.view.ContainerView)a.findRequiredView(a1, R.id.container, "field 'container'");
    }
    
    public static void reset(com.navdy.hud.app.ui.activity.MainActivity a) {
        a.container = null;
    }
}
