package com.navdy.hud.app.ui.activity;

class Main$Presenter$8 implements com.navdy.hud.app.framework.toast.IToastCallback {
    android.animation.ObjectAnimator animator;
    final com.navdy.hud.app.ui.activity.Main$Presenter this$0;
    
    Main$Presenter$8(com.navdy.hud.app.ui.activity.Main$Presenter a) {
        super();
        this.this$0 = a;
    }
    
    public void executeChoiceItem(int i, int i0) {
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        return false;
    }
    
    public void onStart(com.navdy.hud.app.view.ToastView a) {
        com.navdy.hud.app.ui.component.ConfirmationLayout a0 = a.getView();
        a0.title1.setMaxLines(2);
        a0.title1.setSingleLine(false);
        a0.title2.setMaxLines(2);
        a0.title2.setSingleLine(false);
        if (this.animator == null) {
            android.widget.ImageView a1 = a0.sideImage;
            android.util.Property a2 = android.view.View.ROTATION;
            float[] a3 = new float[1];
            a3[0] = 360f;
            this.animator = android.animation.ObjectAnimator.ofFloat(a1, a2, a3);
            this.animator.setDuration(500L);
            this.animator.setInterpolator((android.animation.TimeInterpolator)new android.view.animation.AccelerateDecelerateInterpolator());
            this.animator.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.ui.activity.Main$Presenter$8$1(this));
        }
        if (!this.animator.isRunning()) {
            this.animator.start();
        }
    }
    
    public void onStop() {
        if (this.animator != null) {
            this.animator.removeAllListeners();
            this.animator.cancel();
        }
    }
}
