package com.navdy.hud.app.ui.component.homescreen;
import com.navdy.hud.app.R;

public class SmartDashView$$ViewInjector {
    public SmartDashView$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.ui.component.homescreen.SmartDashView a0, Object a1) {
        a0.mLeftGaugeViewPager = (com.navdy.hud.app.ui.component.dashboard.GaugeViewPager)a.findRequiredView(a1, R.id.left_gauge, "field 'mLeftGaugeViewPager'");
        a0.mRightGaugeViewPager = (com.navdy.hud.app.ui.component.dashboard.GaugeViewPager)a.findRequiredView(a1, R.id.right_gauge, "field 'mRightGaugeViewPager'");
        a0.mMiddleGaugeView = (com.navdy.hud.app.view.GaugeView)a.findRequiredView(a1, R.id.middle_gauge, "field 'mMiddleGaugeView'");
        a0.mEtaLayout = (android.view.ViewGroup)a.findRequiredView(a1, R.id.eta_layout, "field 'mEtaLayout'");
        a0.etaText = (android.widget.TextView)a.findRequiredView(a1, R.id.eta_time, "field 'etaText'");
        a0.etaAmPm = (android.widget.TextView)a.findRequiredView(a1, R.id.eta_time_amPm, "field 'etaAmPm'");
    }
    
    public static void reset(com.navdy.hud.app.ui.component.homescreen.SmartDashView a) {
        a.mLeftGaugeViewPager = null;
        a.mRightGaugeViewPager = null;
        a.mMiddleGaugeView = null;
        a.mEtaLayout = null;
        a.etaText = null;
        a.etaAmPm = null;
    }
}
