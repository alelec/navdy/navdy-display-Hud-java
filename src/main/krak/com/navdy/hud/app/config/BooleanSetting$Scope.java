package com.navdy.hud.app.config;


public enum BooleanSetting$Scope {
    NEVER(0),
    ENG(1),
    BETA(2),
    ALWAYS(3),
    CUSTOM(4);

    private int value;
    BooleanSetting$Scope(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class BooleanSetting$Scope extends Enum {
//    final private static com.navdy.hud.app.config.BooleanSetting$Scope[] $VALUES;
//    final public static com.navdy.hud.app.config.BooleanSetting$Scope ALWAYS;
//    final public static com.navdy.hud.app.config.BooleanSetting$Scope BETA;
//    final public static com.navdy.hud.app.config.BooleanSetting$Scope CUSTOM;
//    final public static com.navdy.hud.app.config.BooleanSetting$Scope ENG;
//    final public static com.navdy.hud.app.config.BooleanSetting$Scope NEVER;
//    
//    static {
//        NEVER = new com.navdy.hud.app.config.BooleanSetting$Scope("NEVER", 0);
//        ENG = new com.navdy.hud.app.config.BooleanSetting$Scope("ENG", 1);
//        BETA = new com.navdy.hud.app.config.BooleanSetting$Scope("BETA", 2);
//        ALWAYS = new com.navdy.hud.app.config.BooleanSetting$Scope("ALWAYS", 3);
//        CUSTOM = new com.navdy.hud.app.config.BooleanSetting$Scope("CUSTOM", 4);
//        com.navdy.hud.app.config.BooleanSetting$Scope[] a = new com.navdy.hud.app.config.BooleanSetting$Scope[5];
//        a[0] = NEVER;
//        a[1] = ENG;
//        a[2] = BETA;
//        a[3] = ALWAYS;
//        a[4] = CUSTOM;
//        $VALUES = a;
//    }
//    
//    private BooleanSetting$Scope(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.config.BooleanSetting$Scope valueOf(String s) {
//        return (com.navdy.hud.app.config.BooleanSetting$Scope)Enum.valueOf(com.navdy.hud.app.config.BooleanSetting$Scope.class, s);
//    }
//    
//    public static com.navdy.hud.app.config.BooleanSetting$Scope[] values() {
//        return $VALUES.clone();
//    }
//}
//