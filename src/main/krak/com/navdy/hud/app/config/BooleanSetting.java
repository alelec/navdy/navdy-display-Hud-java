package com.navdy.hud.app.config;

public class BooleanSetting extends com.navdy.hud.app.config.Setting {
    private boolean changed;
    private boolean defaultValue;
    private boolean enabled;
    private com.navdy.hud.app.config.BooleanSetting$Scope scope;
    
    public BooleanSetting(String s, com.navdy.hud.app.config.BooleanSetting$Scope a, String s0, String s1) {
        this(s, a, false, s0, s1);
    }
    
    public BooleanSetting(String s, com.navdy.hud.app.config.BooleanSetting$Scope a, boolean b, String s0, String s1) {
        super(s, s0, s1);
        this.scope = a;
        this.defaultValue = b;
        this.load();
    }
    
    public boolean isEnabled() {
        boolean b = false;
        switch(com.navdy.hud.app.config.BooleanSetting$1.$SwitchMap$com$navdy$hud$app$config$BooleanSetting$Scope[this.scope.ordinal()]) {
            case 5: {
                b = true;
                break;
            }
            case 3: {
                b = !com.navdy.hud.app.util.DeviceUtil.isUserBuild();
                break;
            }
            case 1: {
                b = this.enabled;
                break;
            }
            default: {
                b = false;
                break;
            }
            case 2: case 4: {
                b = false;
            }
        }
        return b;
    }
    
    public void load() {
        String s = this.getProperty();
        if (s != null) {
            String s0 = com.navdy.hud.app.util.os.SystemProperties.get(s);
            if (android.text.TextUtils.isEmpty((CharSequence)s0)) {
                this.enabled = this.defaultValue;
            } else {
                boolean b = false;
                this.scope = com.navdy.hud.app.config.BooleanSetting$Scope.CUSTOM;
                boolean b0 = Boolean.parseBoolean(s0);
                label2: {
                    label0: {
                        label1: {
                            if (b0) {
                                break label1;
                            }
                            if (s0.equalsIgnoreCase("1")) {
                                break label1;
                            }
                            if (!s0.equalsIgnoreCase("yes")) {
                                break label0;
                            }
                        }
                        b = true;
                        break label2;
                    }
                    b = false;
                }
                this.enabled = b;
            }
        }
    }
    
    public void save() {
        if (this.changed) {
            this.changed = false;
            String s = this.getProperty();
            if (s != null) {
                com.navdy.hud.app.util.os.SystemProperties.set(s, Boolean.toString(this.enabled));
            }
        }
    }
    
    public void setEnabled(boolean b) {
        if (this.scope != com.navdy.hud.app.config.BooleanSetting$Scope.CUSTOM) {
            this.scope = com.navdy.hud.app.config.BooleanSetting$Scope.CUSTOM;
        }
        if (this.enabled != b) {
            this.enabled = b;
            this.changed = true;
            this.changed();
            this.save();
        }
    }
    
    public String toString() {
        StringBuilder a = new StringBuilder("BooleanSetting{");
        a.append("name=").append(this.getName());
        a.append(",");
        a.append("enabled=").append(this.enabled);
        a.append((char)125);
        return a.toString();
    }
}
