package com.navdy.hud.app.config;

public class SettingsManager implements com.navdy.hud.app.config.Setting$IObserver {
    final private static com.navdy.service.library.log.Logger sLogger;
    final private com.squareup.otto.Bus bus;
    final private android.content.SharedPreferences preferences;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.config.SettingsManager.class);
    }
    
    public SettingsManager(com.squareup.otto.Bus a, android.content.SharedPreferences a0) {
        this.bus = a;
        this.preferences = a0;
    }
    
    public void addSetting(com.navdy.hud.app.config.Setting a) {
        sLogger.i(new StringBuilder().append("Adding setting ").append(a).toString());
        a.setObserver((com.navdy.hud.app.config.Setting$IObserver)this);
    }
    
    public void onChanged(com.navdy.hud.app.config.Setting a) {
        sLogger.i(new StringBuilder().append("Setting changed:").append(a).toString());
        this.bus.post(new com.navdy.hud.app.config.SettingsManager$SettingsChanged(a.getPath(), a));
    }
}
