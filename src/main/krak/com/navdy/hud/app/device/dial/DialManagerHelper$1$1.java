package com.navdy.hud.app.device.dial;

class DialManagerHelper$1$1 implements Runnable {
    final com.navdy.hud.app.device.dial.DialManagerHelper$1 this$0;
    final android.bluetooth.BluetoothProfile val$proxy;
    
    DialManagerHelper$1$1(com.navdy.hud.app.device.dial.DialManagerHelper$1 a, android.bluetooth.BluetoothProfile a0) {
        super();
        this.this$0 = a;
        this.val$proxy = a0;
    }
    
    public void run() {
        try {
            int i = this.val$proxy.getConnectionState(this.this$0.val$device);
            com.navdy.hud.app.device.dial.DialManagerHelper.access$000().v(new StringBuilder().append("[Dial]Device ").append(this.this$0.val$device.getName()).append(" state is ").append(com.navdy.hud.app.bluetooth.utils.BluetoothUtils.getConnectedState(i)).toString());
            if (i != 2) {
                this.this$0.val$callBack.onStatus(false);
            } else {
                com.navdy.hud.app.device.dial.DialManagerHelper.access$000().v("[Dial]already connected");
                this.this$0.val$callBack.onStatus(true);
            }
        } catch(Throwable a) {
            com.navdy.hud.app.device.dial.DialManagerHelper.access$000().e("[Dial]", a);
            this.this$0.val$callBack.onStatus(false);
        }
    }
}
