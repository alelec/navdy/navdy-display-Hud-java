package com.navdy.hud.app.device.dial;

final class DialManagerHelper$4 implements android.bluetooth.BluetoothProfile$ServiceListener {
    boolean eventSent;
    final com.navdy.hud.app.device.dial.DialManagerHelper$IDialConnection val$callBack;
    final android.bluetooth.BluetoothDevice val$device;
    
    DialManagerHelper$4(android.bluetooth.BluetoothDevice a, com.navdy.hud.app.device.dial.DialManagerHelper$IDialConnection a0) {
        super();
        this.val$device = a;
        this.val$callBack = a0;
        this.eventSent = false;
    }
    
    public void onServiceConnected(int i, android.bluetooth.BluetoothProfile a) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.device.dial.DialManagerHelper$4$1(this, a), 1);
    }
    
    public void onServiceDisconnected(int i) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.device.dial.DialManagerHelper$4$2(this), 1);
    }
}
