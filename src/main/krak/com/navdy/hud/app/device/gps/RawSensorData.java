package com.navdy.hud.app.device.gps;

public class RawSensorData {
    final private static float ACCEL_SCALE_FACTOR;
    final public static int ACCEL_X = 16;
    final public static int ACCEL_Y = 17;
    final public static int ACCEL_Z = 18;
    final private static float GYRO_SCALE_FACTOR;
    final public static int GYRO_TEMP = 12;
    final public static int GYRO_X = 13;
    final public static int GYRO_Y = 14;
    final public static int GYRO_Z = 5;
    final public static float MAX_ACCEL = 2f;
    final public float x;
    final public float y;
    final public float z;
    
    static {
        ACCEL_SCALE_FACTOR = (float)Math.pow(2.0, -10.0);
        GYRO_SCALE_FACTOR = (float)Math.pow(2.0, -12.0);
    }
    
    public RawSensorData(byte[] a, int i) {
        java.nio.ByteBuffer a0 = java.nio.ByteBuffer.wrap(a, i + 4, a.length - i - 4);
        a0.order(java.nio.ByteOrder.LITTLE_ENDIAN);
        int i0 = a0.getShort();
        int i1 = (i0 - 4) / 8;
        a0.mark();
        this.x = this.clamp(this.average(a0, i1, 16, ACCEL_SCALE_FACTOR) / 9.8f, -2f, 2f);
        this.y = this.clamp(this.average(a0, i1, 17, ACCEL_SCALE_FACTOR) / 9.8f, -2f, 2f);
        this.z = this.clamp(this.average(a0, i1, 18, ACCEL_SCALE_FACTOR) / 9.8f, -2f, 2f);
    }
    
    private float average(java.nio.ByteBuffer a, int i, int i0, float f) {
        a.reset();
        a.getInt();
        int i1 = 0;
        float f0 = 0.0f;
        int i2 = 0;
        while(true) {
            if (i2 < i && a.position() < a.limit()) {
                int i3 = a.getInt();
                a.getInt();
                if (i3 >> 24 == i0) {
                    int i4 = i3 & 16777215;
                    if ((8388608 & i4) != 0) {
                        i4 = i4 | -16777216;
                    }
                    f0 = f0 + (float)i4 * f;
                    i1 = i1 + 1;
                }
                i2 = i2 + 1;
                continue;
            }
            return (i1 <= 0) ? 0.0f : f0 / (float)i1;
        }
    }
    
    private float clamp(float f, float f0, float f1) {
        if (f < f0) {
            f = f0;
        } else if (f > f1) {
            f = f1;
        }
        return f;
    }
}
