package com.navdy.hud.app.device.gps;
import com.navdy.hud.app.R;

public class GpsDeadReckoningManager implements Runnable {
    final private static int ALIGNMENT_DONE = 3;
    final private static int ALIGNMENT_PACKET_LENGTH = 24;
    final private static int ALIGNMENT_PACKET_PAYLOAD_LENGTH = 16;
    final private static byte[] ALIGNMENT_PATTERN;
    final private static long ALIGNMENT_POLL_FREQUENCY = 30000L;
    final private static String ALIGNMENT_TIME = "alignment_time";
    final private static byte[] AUTO_ALIGNMENT;
    final private static byte[] CFG_ESF_RAW;
    final private static byte[] CFG_ESF_RAW_OFF;
    final private static byte[] CFG_RST_COLD;
    final private static byte[] CFG_RST_WARM;
    final private static byte DEAD_RECKONING_ONLY = (byte)1;
    final private static int ESF_PACKET_LENGTH = 19;
    final private static byte[] ESF_RAW_PATTERN;
    final private static byte[] ESF_STATUS_PATTERN;
    final private static int FAILURE_RETRY_INTERVAL = 10000;
    final private static byte FIX_2D = (byte)2;
    final private static byte FIX_3D = (byte)3;
    final private static int FUSION_DONE = 1;
    final private static byte[] GET_ALIGNMENT;
    final private static byte[] GET_ESF_STATUS;
    final private static String GPS = "gps";
    final private static byte GPS_DEAD_RECKONING_COMBINED = (byte)4;
    final private static String GPS_LOG = "gps.log";
    final private static char[] HEX;
    final private static byte[] INJECT_OBD_SPEED;
    final private static int MSG_ALIGNMENT_VALUE = 1;
    final private static byte[] NAV_STATUS_PATTERN;
    final private static byte NO_FIX = (byte)0;
    final private static byte[] PASSPHRASE;
    final private static String PITCH = "pitch";
    final private static long POLL_FREQUENCY = 10000L;
    final private static byte[] READ_BUF;
    final private static String ROLL = "roll";
    final private static int SPEED_INJECTION_FREQUENCY = 100;
    final private static int SPEED_TIME_TAG_COUNTER = 100;
    final private static String TCP_SERVER_HOST = "127.0.0.1";
    final private static int TCP_SERVER_PORT = 42434;
    final private static byte[] TEMP_BUF;
    final private static java.util.concurrent.atomic.AtomicInteger THREAD_COUNTER;
    final private static byte TIME_ONLY = (byte)5;
    final private static String YAW = "yaw";
    final private static com.navdy.hud.app.device.gps.GpsDeadReckoningManager sInstance;
    final private static com.navdy.service.library.log.Logger sLogger;
    private volatile boolean alignmentChecked;
    private com.navdy.hud.app.device.gps.GpsDeadReckoningManager$Alignment alignmentInfo;
    final private com.squareup.otto.Bus bus;
    private volatile boolean deadReckoningInjectionStarted;
    private boolean deadReckoningOn;
    private byte deadReckoningType;
    private Runnable enableEsfRunnable;
    final private Runnable getAlignmentRunnable;
    final private Runnable getAlignmentRunnableRetry;
    final private Runnable getFusionStatusRunnable;
    final private Runnable getFusionStatusRunnableRetry;
    private android.os.Handler handler;
    private android.os.HandlerThread handlerThread;
    final private Runnable injectRunnable;
    final private com.navdy.hud.app.device.gps.GpsDeadReckoningManager$CommandWriter injectSpeed;
    private java.io.InputStream inputStream;
    private long lastConnectionFailure;
    private java.io.OutputStream outputStream;
    private Runnable resetRunnable;
    private org.json.JSONObject rootInfo;
    private volatile boolean runThread;
    private com.navdy.hud.app.util.GForceRawSensorDataProcessor sensorDataProcessor;
    private java.net.Socket socket;
    final private com.navdy.hud.app.manager.SpeedManager speedManager;
    private Thread thread;
    private int timeStampCounter;
    private boolean waitForAutoAlignment;
    private boolean waitForFusionStatus;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.device.gps.GpsDeadReckoningManager.class);
        sInstance = new com.navdy.hud.app.device.gps.GpsDeadReckoningManager();
        char[] a = new char[16];
        a[0] = (char)48;
        a[1] = (char)49;
        a[2] = (char)50;
        a[3] = (char)51;
        a[4] = (char)52;
        a[5] = (char)53;
        a[6] = (char)54;
        a[7] = (char)55;
        a[8] = (char)56;
        a[9] = (char)57;
        a[10] = (char)65;
        a[11] = (char)66;
        a[12] = (char)67;
        a[13] = (char)68;
        a[14] = (char)69;
        a[15] = (char)70;
        HEX = a;
        THREAD_COUNTER = new java.util.concurrent.atomic.AtomicInteger(1);
        byte[] a0 = new byte[20];
        a0[0] = (byte)(-75);
        a0[1] = (byte)98;
        a0[2] = (byte)16;
        a0[3] = (byte)2;
        a0[4] = (byte)12;
        a0[5] = (byte)0;
        a0[6] = (byte)0;
        a0[7] = (byte)0;
        a0[8] = (byte)0;
        a0[9] = (byte)0;
        a0[10] = (byte)0;
        a0[11] = (byte)0;
        a0[12] = (byte)0;
        a0[13] = (byte)0;
        a0[14] = (byte)0;
        a0[15] = (byte)0;
        a0[16] = (byte)0;
        a0[17] = (byte)11;
        a0[18] = (byte)0;
        a0[19] = (byte)0;
        INJECT_OBD_SPEED = a0;
        byte[] a1 = new byte[12];
        a1[0] = (byte)(-75);
        a1[1] = (byte)98;
        a1[2] = (byte)6;
        a1[3] = (byte)4;
        a1[4] = (byte)4;
        a1[5] = (byte)0;
        a1[6] = (byte)1;
        a1[7] = (byte)0;
        a1[8] = (byte)2;
        a1[9] = (byte)0;
        a1[10] = (byte)17;
        a1[11] = (byte)108;
        CFG_RST_WARM = a1;
        byte[] a2 = new byte[11];
        a2[0] = (byte)(-75);
        a2[1] = (byte)98;
        a2[2] = (byte)6;
        a2[3] = (byte)1;
        a2[4] = (byte)3;
        a2[5] = (byte)0;
        a2[6] = (byte)16;
        a2[7] = (byte)3;
        a2[8] = (byte)1;
        a2[9] = (byte)0;
        a2[10] = (byte)0;
        CFG_ESF_RAW = a2;
        byte[] a3 = new byte[11];
        a3[0] = (byte)(-75);
        a3[1] = (byte)98;
        a3[2] = (byte)6;
        a3[3] = (byte)1;
        a3[4] = (byte)3;
        a3[5] = (byte)0;
        a3[6] = (byte)16;
        a3[7] = (byte)3;
        a3[8] = (byte)0;
        a3[9] = (byte)0;
        a3[10] = (byte)0;
        CFG_ESF_RAW_OFF = a3;
        com.navdy.hud.app.device.gps.GpsDeadReckoningManager.calculateChecksum(CFG_ESF_RAW);
        com.navdy.hud.app.device.gps.GpsDeadReckoningManager.calculateChecksum(CFG_ESF_RAW_OFF);
        byte[] a4 = new byte[12];
        a4[0] = (byte)(-75);
        a4[1] = (byte)98;
        a4[2] = (byte)6;
        a4[3] = (byte)4;
        a4[4] = (byte)4;
        a4[5] = (byte)0;
        a4[6] = (byte)(-1);
        a4[7] = (byte)(-1);
        a4[8] = (byte)2;
        a4[9] = (byte)0;
        a4[10] = (byte)14;
        a4[11] = (byte)97;
        CFG_RST_COLD = a4;
        byte[] a5 = new byte[4];
        a5[0] = (byte)(-75);
        a5[1] = (byte)98;
        a5[2] = (byte)1;
        a5[3] = (byte)3;
        NAV_STATUS_PATTERN = a5;
        byte[] a6 = new byte[4];
        a6[0] = (byte)(-75);
        a6[1] = (byte)98;
        a6[2] = (byte)16;
        a6[3] = (byte)3;
        ESF_RAW_PATTERN = a6;
        byte[] a7 = new byte[4];
        a7[0] = (byte)(-75);
        a7[1] = (byte)98;
        a7[2] = (byte)16;
        a7[3] = (byte)20;
        ALIGNMENT_PATTERN = a7;
        byte[] a8 = new byte[8];
        a8[0] = (byte)(-75);
        a8[1] = (byte)98;
        a8[2] = (byte)16;
        a8[3] = (byte)20;
        a8[4] = (byte)0;
        a8[5] = (byte)0;
        a8[6] = (byte)36;
        a8[7] = (byte)124;
        GET_ALIGNMENT = a8;
        byte[] a9 = new byte[20];
        a9[0] = (byte)(-75);
        a9[1] = (byte)98;
        a9[2] = (byte)6;
        a9[3] = (byte)86;
        a9[4] = (byte)12;
        a9[5] = (byte)0;
        a9[6] = (byte)0;
        a9[7] = (byte)1;
        a9[8] = (byte)0;
        a9[9] = (byte)0;
        a9[10] = (byte)0;
        a9[11] = (byte)0;
        a9[12] = (byte)0;
        a9[13] = (byte)0;
        a9[14] = (byte)0;
        a9[15] = (byte)0;
        a9[16] = (byte)0;
        a9[17] = (byte)0;
        a9[18] = (byte)105;
        a9[19] = (byte)29;
        AUTO_ALIGNMENT = a9;
        byte[] a10 = new byte[8];
        a10[0] = (byte)(-75);
        a10[1] = (byte)98;
        a10[2] = (byte)16;
        a10[3] = (byte)16;
        a10[4] = (byte)0;
        a10[5] = (byte)0;
        a10[6] = (byte)32;
        a10[7] = (byte)112;
        GET_ESF_STATUS = a10;
        byte[] a11 = new byte[4];
        a11[0] = (byte)(-75);
        a11[1] = (byte)98;
        a11[2] = (byte)16;
        a11[3] = (byte)16;
        ESF_STATUS_PATTERN = a11;
        TEMP_BUF = new byte[128];
        READ_BUF = new byte[8192];
        byte[] a12 = new byte[5];
        a12[0] = (byte)110;
        a12[1] = (byte)97;
        a12[2] = (byte)118;
        a12[3] = (byte)100;
        a12[4] = (byte)121;
        PASSPHRASE = a12;
    }
    
    private GpsDeadReckoningManager() {
        this.speedManager = com.navdy.hud.app.manager.SpeedManager.getInstance();
        this.timeStampCounter = 0;
        this.deadReckoningType = (byte)(-1);
        this.injectSpeed = (com.navdy.hud.app.device.gps.GpsDeadReckoningManager$CommandWriter)new com.navdy.hud.app.device.gps.GpsDeadReckoningManager$1(this);
        this.injectRunnable = (Runnable)new com.navdy.hud.app.device.gps.GpsDeadReckoningManager$2(this);
        this.getFusionStatusRunnable = (Runnable)new com.navdy.hud.app.device.gps.GpsDeadReckoningManager$3(this);
        this.getFusionStatusRunnableRetry = (Runnable)new com.navdy.hud.app.device.gps.GpsDeadReckoningManager$4(this);
        this.getAlignmentRunnable = (Runnable)new com.navdy.hud.app.device.gps.GpsDeadReckoningManager$5(this);
        this.getAlignmentRunnableRetry = (Runnable)new com.navdy.hud.app.device.gps.GpsDeadReckoningManager$6(this);
        this.enableEsfRunnable = (Runnable)new com.navdy.hud.app.device.gps.GpsDeadReckoningManager$9(this);
        this.resetRunnable = (Runnable)new com.navdy.hud.app.device.gps.GpsDeadReckoningManager$10(this);
        sLogger.v("ctor()");
        this.sensorDataProcessor = new com.navdy.hud.app.util.GForceRawSensorDataProcessor();
        this.handlerThread = new android.os.HandlerThread("gpsDeadReckoningHandler");
        this.handlerThread.start();
        this.handler = new android.os.Handler(this.handlerThread.getLooper(), (android.os.Handler$Callback)new com.navdy.hud.app.device.gps.GpsDeadReckoningManager$7(this));
        com.navdy.hud.app.obd.ObdManager a = com.navdy.hud.app.obd.ObdManager.getInstance();
        if (a.isConnected()) {
            sLogger.v("ctor() obd is connected");
            if (a.isSpeedPidAvailable()) {
                sLogger.v("ctor() speed pid is available");
                this.checkAlignment();
                this.startDeadReckoning();
            } else {
                sLogger.v("ctor() speed pid is not available");
            }
        } else {
            sLogger.v("ctor() obd is not connected, waiting for obd to connect");
        }
        this.bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
        this.bus.register(this);
        sLogger.v("ctor() initialized");
        this.handler.post(this.enableEsfRunnable);
    }
    
    static void access$000(com.navdy.hud.app.device.gps.GpsDeadReckoningManager a) {
        a.sendObdSpeed();
    }
    
    static com.navdy.hud.app.device.gps.GpsDeadReckoningManager$CommandWriter access$100(com.navdy.hud.app.device.gps.GpsDeadReckoningManager a) {
        return a.injectSpeed;
    }
    
    static com.navdy.service.library.log.Logger access$1000() {
        return sLogger;
    }
    
    static Runnable access$1100(com.navdy.hud.app.device.gps.GpsDeadReckoningManager a) {
        return a.getFusionStatusRunnable;
    }
    
    static byte[] access$1200() {
        return GET_ALIGNMENT;
    }
    
    static Runnable access$1300(com.navdy.hud.app.device.gps.GpsDeadReckoningManager a) {
        return a.getAlignmentRunnableRetry;
    }
    
    static void access$1400(com.navdy.hud.app.device.gps.GpsDeadReckoningManager a, boolean b) {
        a.postAlignmentRunnable(b);
    }
    
    static Runnable access$1500(com.navdy.hud.app.device.gps.GpsDeadReckoningManager a) {
        return a.getAlignmentRunnable;
    }
    
    static void access$1600(com.navdy.hud.app.device.gps.GpsDeadReckoningManager a, com.navdy.hud.app.device.gps.GpsDeadReckoningManager$Alignment a0) {
        a.handleAutoAlignmentResult(a0);
    }
    
    static boolean access$1700(com.navdy.hud.app.device.gps.GpsDeadReckoningManager a) {
        return a.waitForAutoAlignment;
    }
    
    static boolean access$1800(com.navdy.hud.app.device.gps.GpsDeadReckoningManager a) {
        return a.waitForFusionStatus;
    }
    
    static java.io.OutputStream access$1900(com.navdy.hud.app.device.gps.GpsDeadReckoningManager a) {
        return a.outputStream;
    }
    
    static boolean access$200(com.navdy.hud.app.device.gps.GpsDeadReckoningManager a, com.navdy.hud.app.device.gps.GpsDeadReckoningManager$CommandWriter a0) {
        return a.invokeUblox(a0);
    }
    
    static String access$2000(byte[] a, int i, int i0) {
        return com.navdy.hud.app.device.gps.GpsDeadReckoningManager.bytesToHex(a, i, i0);
    }
    
    static byte[] access$2100() {
        return CFG_ESF_RAW;
    }
    
    static com.navdy.hud.app.util.GForceRawSensorDataProcessor access$2200(com.navdy.hud.app.device.gps.GpsDeadReckoningManager a) {
        return a.sensorDataProcessor;
    }
    
    static com.squareup.otto.Bus access$2300(com.navdy.hud.app.device.gps.GpsDeadReckoningManager a) {
        return a.bus;
    }
    
    static byte[] access$2400() {
        return CFG_RST_WARM;
    }
    
    static void access$2500(com.navdy.hud.app.device.gps.GpsDeadReckoningManager a) {
        a.sendAutoAlignment();
    }
    
    static boolean access$300(com.navdy.hud.app.device.gps.GpsDeadReckoningManager a) {
        return a.deadReckoningInjectionStarted;
    }
    
    static long access$400(com.navdy.hud.app.device.gps.GpsDeadReckoningManager a) {
        return a.lastConnectionFailure;
    }
    
    static android.os.Handler access$500(com.navdy.hud.app.device.gps.GpsDeadReckoningManager a) {
        return a.handler;
    }
    
    static byte[] access$600() {
        return GET_ESF_STATUS;
    }
    
    static boolean access$700(com.navdy.hud.app.device.gps.GpsDeadReckoningManager a, byte[] a0, String s) {
        return a.invokeUblox(a0, s);
    }
    
    static Runnable access$800(com.navdy.hud.app.device.gps.GpsDeadReckoningManager a) {
        return a.getFusionStatusRunnableRetry;
    }
    
    static void access$900(com.navdy.hud.app.device.gps.GpsDeadReckoningManager a, boolean b) {
        a.postFusionRunnable(b);
    }
    
    private static String bytesToHex(byte[] a, int i, int i0) {
        char[] a0 = new char[i0 * 2];
        int i1 = 0;
        while(i1 < i0) {
            int i2 = a[i1 + i];
            int i3 = i2 & 255;
            int i4 = i1 * 2;
            int i5 = (char)(boolean)HEX[i3 >>> 4];
            a0[i4] = (char)i5;
            int i6 = i1 * 2 + 1;
            int i7 = (char)(boolean)HEX[i3 & 15];
            a0[i6] = (char)i7;
            i1 = i1 + 1;
        }
        return new String(a0);
    }
    
    private static void calculateChecksum(byte[] a) {
        int i = a.length;
        int i0 = 0;
        int i1 = 0;
        int i2 = 2;
        while(i2 < i - 2) {
            int i3 = a[i2];
            i0 = (byte)(i3 + i0);
            i1 = (byte)(i1 + i0);
            i2 = i2 + 1;
        }
        int i4 = i - 2;
        int i5 = (byte)i0;
        a[i4] = (byte)i5;
        int i6 = i - 1;
        int i7 = (byte)i1;
        a[i6] = (byte)i7;
    }
    
    private void checkAlignment() {
        if (this.alignmentChecked) {
            sLogger.v("alignment already checked");
        } else {
            sLogger.v("checking alignment");
            this.postAlignmentRunnable(false);
        }
    }
    
    private void closeSocket() {
        this.runThread = false;
        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)this.inputStream);
        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)this.outputStream);
        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)this.socket);
        this.inputStream = null;
        this.outputStream = null;
        this.socket = null;
        Thread a = this.thread;
        label1: {
            Throwable a0 = null;
            if (a == null) {
                break label1;
            }
            label0: {
                try {
                    if (this.thread.isAlive()) {
                        this.thread.interrupt();
                        sLogger.v("waiting for thread");
                        this.thread.join();
                        sLogger.v("waited");
                    } else {
                        sLogger.v("thread is not alive");
                    }
                } catch(Throwable a1) {
                    a0 = a1;
                    break label0;
                }
                this.thread = null;
                break label1;
            }
            try {
                sLogger.e(a0);
            } catch(Throwable a2) {
                this.thread = null;
                throw a2;
            }
            this.thread = null;
        }
        this.deadReckoningOn = false;
        this.deadReckoningType = (byte)(-1);
    }
    
    private boolean connectSocket() {
        boolean b = false;
        try {
            if (this.socket != null) {
                b = true;
            } else {
                this.socket = new java.net.Socket("127.0.0.1", 42434);
                this.inputStream = this.socket.getInputStream();
                this.outputStream = this.socket.getOutputStream();
                sLogger.v("connected to 42434");
                this.outputStream.write(PASSPHRASE);
                sLogger.v("sent passphrase");
                this.thread = new Thread((Runnable)this);
                this.thread.setName(new StringBuilder().append("GpsDeadReckoningManager-").append(THREAD_COUNTER.getAndIncrement()).toString());
                this.runThread = true;
                this.thread.start();
                com.navdy.hud.app.util.GenericUtil.sleep(2000);
                b = true;
            }
        } catch(Throwable a) {
            sLogger.e(a);
            this.closeSocket();
            b = false;
        }
        return b;
    }
    
    private static String getDRType(byte a) {
        String s = null;
        switch(a) {
            case 5: {
                s = "TIME_ONLY";
                break;
            }
            case 4: {
                s = "GPS_DEAD_RECKONING_COMBINED";
                break;
            }
            case 3: {
                s = "FIX_3D";
                break;
            }
            case 2: {
                s = "FIX_2D";
                break;
            }
            case 1: {
                s = "DEAD_RECKONING_ONLY";
                break;
            }
            case 0: {
                s = "NO_FIX";
                break;
            }
            default: {
                s = "UNKNOWN";
            }
        }
        return s;
    }
    
    public static com.navdy.hud.app.device.gps.GpsDeadReckoningManager getInstance() {
        return sInstance;
    }
    
    private void handleAlignment(int i, int i0) {
        if (i + 24 > i0) {
            this.postAlignmentRunnable(true);
        } else {
            String s = com.navdy.hud.app.device.gps.GpsDeadReckoningManager.bytesToHex(READ_BUF, i, 24);
            sLogger.v(new StringBuilder().append("alignment raw data[").append(s).append("]").toString());
            java.nio.ByteBuffer a = java.nio.ByteBuffer.wrap(READ_BUF, i, 24);
            a.order(java.nio.ByteOrder.LITTLE_ENDIAN);
            a.get(TEMP_BUF, 0, ALIGNMENT_PATTERN.length);
            int i1 = a.getShort();
            if (i1 != 16) {
                throw new RuntimeException(new StringBuilder().append("len[").append(i1).append("] expected [").append(16).append("]").toString());
            }
            a.get(TEMP_BUF, 0, 4);
            int i2 = a.get();
            int i3 = a.get();
            int i4 = (i3 & 14) >> 1;
            boolean b = (i3 & 1) == 1;
            boolean b0 = i4 == 3;
            a.get();
            a.get();
            float f = (float)a.getInt() / 100f;
            int i5 = a.getShort();
            float f0 = (float)i5 / 100f;
            int i6 = a.getShort();
            float f1 = (float)i6 / 100f;
            android.os.Message a0 = android.os.Message.obtain();
            a0.what = 1;
            com.navdy.hud.app.device.gps.GpsDeadReckoningManager$Alignment a1 = new com.navdy.hud.app.device.gps.GpsDeadReckoningManager$Alignment(f, f0, f1, b0);
            a0.obj = a1;
            sLogger.v(new StringBuilder().append("Alignment bitField[").append(i3).append("] alignment[").append(i4).append("] autoAlignment[").append(b).append("] version[").append(i2).append("] ").append(a1).toString());
            this.handler.removeCallbacks(this.getAlignmentRunnableRetry);
            this.handler.sendMessage(a0);
        }
    }
    
    private void handleAutoAlignmentResult(com.navdy.hud.app.device.gps.GpsDeadReckoningManager$Alignment a) {
        if (this.waitForAutoAlignment) {
            if (a.done) {
                this.alignmentInfo = a;
                com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("GPS_Calibration_IMU_Done", new String[0]);
                com.navdy.hud.app.framework.voice.TTSUtils.debugShowGpsImuCalibrationDone();
                this.waitForAutoAlignment = false;
                this.waitForFusionStatus = true;
                this.postFusionRunnable(false);
                sLogger.v("got alignment, wait for fusion status");
            } else {
                sLogger.v("alignment not done yet, try again");
                this.postAlignmentRunnable(true);
            }
        } else {
            boolean b = false;
            sLogger.v("waiting for align:false");
            String s = com.navdy.hud.app.obd.ObdManager.getInstance().getVin();
            if (s == null) {
                s = "UNKNOWN_VIN";
            }
            sLogger.v(new StringBuilder().append("Vin is ").append(s).toString());
            String s0 = com.navdy.hud.app.storage.db.helper.VinInformationHelper.getVinInfo(s);
            boolean b0 = android.text.TextUtils.isEmpty((CharSequence)s0);
            label4: {
                Throwable a0 = null;
                if (b0) {
                    sLogger.v("no info found for vin,  need auto alignment");
                    b = true;
                    break label4;
                } else {
                    try {
                        com.navdy.service.library.log.Logger a1 = sLogger;
                        b = false;
                        a1.v(new StringBuilder().append("VinInfo is ").append(s0).toString());
                        float f = a.yaw;
                        int i = (f > 0.0f) ? 1 : (f == 0.0f) ? 0 : -1;
                        label2: {
                            label3: {
                                if (i != 0) {
                                    break label3;
                                }
                                b = false;
                                if (a.pitch != 0.0f) {
                                    break label3;
                                }
                                b = false;
                                if (a.roll == 0.0f) {
                                    break label2;
                                }
                            }
                            b = false;
                            this.rootInfo = new org.json.JSONObject(s0);
                            long j = Long.parseLong(this.rootInfo.getJSONObject("gps").getString("alignment_time"));
                            long j0 = System.currentTimeMillis();
                            sLogger.v(new StringBuilder().append("elapsed=").append(j0 - j).toString());
                            b = false;
                            break label4;
                        }
                        com.navdy.service.library.log.Logger a2 = sLogger;
                        b = false;
                        a2.w("VinInfo exists but u-blox alignment is lost, trigger alignment");
                        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("GPS_Calibration_Lost", new String[0]);
                        b = true;
                        com.navdy.hud.app.storage.db.helper.VinInformationHelper.deleteVinInfo(s);
                        b = true;
                        break label4;
                    } catch(Throwable a3) {
                        a0 = a3;
                    }
                }
                sLogger.e(a0);
            }
            String s1 = com.navdy.hud.app.storage.db.helper.VinInformationHelper.getVinPreference().getString("vin", (String)null);
            sLogger.v(new StringBuilder().append("last vin:").append(s1).append(" current vin:").append(s).toString());
            boolean b1 = android.text.TextUtils.isEmpty((CharSequence)s1);
            label1: {
                label0: {
                    if (b1) {
                        break label0;
                    }
                    if (android.text.TextUtils.equals((CharSequence)s1, (CharSequence)s)) {
                        break label0;
                    }
                    sLogger.v("vin has switched: trigger auto alignment");
                    com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("GPS_Calibration_VinSwitch", new String[0]);
                    b = true;
                    break label1;
                }
                sLogger.v("vin switch not detected");
            }
            if (b) {
                sLogger.v("alignment required");
                com.navdy.hud.app.storage.db.helper.VinInformationHelper.getVinPreference().edit().remove("vin").commit();
                sLogger.v("last pref removed");
                if (!this.invokeUblox((com.navdy.hud.app.device.gps.GpsDeadReckoningManager$CommandWriter)new com.navdy.hud.app.device.gps.GpsDeadReckoningManager$11(this))) {
                    this.postAlignmentRunnable(true);
                }
            } else {
                this.alignmentChecked = true;
                sLogger.v("alignment not reqd");
                com.navdy.hud.app.framework.voice.TTSUtils.debugShowGpsSensorCalibrationNotNeeded();
            }
        }
    }
    
    private void handleEsfRaw(int i, int i0) {
        com.navdy.hud.app.device.gps.RawSensorData a = new com.navdy.hud.app.device.gps.RawSensorData(READ_BUF, i);
        this.sensorDataProcessor.onRawData(a);
        if (this.sensorDataProcessor.isCalibrated()) {
            this.bus.post(new com.navdy.hud.app.device.gps.CalibratedGForceData(this.sensorDataProcessor.xAccel, this.sensorDataProcessor.yAccel, this.sensorDataProcessor.zAccel));
        }
    }
    
    private void handleFusion(int i, int i0) {
        if (i + 19 > i0) {
            this.postFusionRunnable(true);
        } else {
            String s = com.navdy.hud.app.device.gps.GpsDeadReckoningManager.bytesToHex(READ_BUF, i, 19);
            sLogger.v(new StringBuilder().append("esf raw data[").append(s).append("]").toString());
            java.nio.ByteBuffer a = java.nio.ByteBuffer.wrap(READ_BUF, i, 19);
            a.order(java.nio.ByteOrder.LITTLE_ENDIAN);
            a.get(TEMP_BUF, 0, ALIGNMENT_PATTERN.length);
            int i1 = a.getShort();
            a.get(TEMP_BUF, 0, 4);
            int i2 = a.get();
            int i3 = a.get();
            int i4 = a.get();
            a.get(TEMP_BUF, 0, 5);
            int i5 = a.get();
            this.handler.removeCallbacks(this.getFusionStatusRunnableRetry);
            if (i5 == 1) {
                sLogger.v(new StringBuilder().append("Fusion Done len[").append(i1).append("] fusionMode[").append(i5).append("] initStatus1[").append(i3).append("] initStatus2[").append(i4).append("] version[").append(i2).append("]").toString());
                this.storeVinInfoInDb();
            } else {
                sLogger.v(new StringBuilder().append("Fusion not done retry len[").append(i1).append("] fusionMode[").append(i5).append("] initStatus1[").append(i3).append("] initStatus2[").append(i4).append("] version[").append(i2).append("]").toString());
                this.postFusionRunnable(true);
            }
        }
    }
    
    private void handleNavStatus(int i, int i0) {
        int i1 = i + 10;
        if (i1 <= i0 - 1) {
            int i2 = READ_BUF[i1];
            switch(i2) {
                case 1: case 4: {
                    if (this.deadReckoningOn) {
                        int i3 = this.deadReckoningType;
                        if (i3 == i2) {
                            break;
                        }
                        String s = com.navdy.hud.app.device.gps.GpsDeadReckoningManager.getDRType((byte)i2);
                        com.navdy.service.library.log.Logger a = sLogger;
                        StringBuilder a0 = new StringBuilder().append("dead reckoning type changed from [");
                        int i4 = this.deadReckoningType;
                        a.v(a0.append(i4).append("] to [").append(i2).append("] ").append(s).toString());
                        this.deadReckoningType = (byte)i2;
                        com.navdy.hud.app.framework.voice.TTSUtils.debugShowDRStarted(s);
                        android.os.Bundle a1 = new android.os.Bundle();
                        a1.putString("drtype", (i2 != 1) ? "GPS_DEAD_RECKONING_COMBINED" : "DEAD_RECKONING_ONLY");
                        com.navdy.hud.app.device.gps.GpsUtils.sendEventBroadcast("GPS_DR_STARTED", a1);
                        break;
                    } else {
                        this.deadReckoningOn = true;
                        this.deadReckoningType = (byte)i2;
                        String s0 = com.navdy.hud.app.device.gps.GpsDeadReckoningManager.getDRType((byte)i2);
                        com.navdy.hud.app.framework.voice.TTSUtils.debugShowDRStarted(s0);
                        sLogger.v(new StringBuilder().append("dead reckoning on[").append(i2).append("] ").append(s0).toString());
                        android.os.Bundle a2 = new android.os.Bundle();
                        a2.putString("drtype", (i2 != 1) ? "GPS_DEAD_RECKONING_COMBINED" : "DEAD_RECKONING_ONLY");
                        com.navdy.hud.app.device.gps.GpsUtils.sendEventBroadcast("GPS_DR_STARTED", a2);
                        break;
                    }
                }
                default: {
                    if (this.deadReckoningOn) {
                        this.deadReckoningOn = false;
                        this.deadReckoningType = (byte)(-1);
                        com.navdy.hud.app.framework.voice.TTSUtils.debugShowDREnded();
                        sLogger.v(new StringBuilder().append("dead reckoning stopped:").append(i2).toString());
                        com.navdy.hud.app.device.gps.GpsUtils.sendEventBroadcast("GPS_DR_STOPPED", (android.os.Bundle)null);
                    }
                }
            }
        }
    }
    
    private void initDeadReckoning() {
        if (com.navdy.hud.app.obd.ObdManager.getInstance().isSpeedPidAvailable()) {
            sLogger.v("speed pid is available");
            this.checkAlignment();
            this.startDeadReckoning();
        } else {
            sLogger.v("speed pid is not available");
        }
    }
    
    private boolean invokeUblox(com.navdy.hud.app.device.gps.GpsDeadReckoningManager$CommandWriter a) {
        boolean b = false;
        long j = this.lastConnectionFailure;
        int i = (j < 0L) ? -1 : (j == 0L) ? 0 : 1;
        label1: {
            label0: {
                try {
                    long j0 = 0L;
                    label2: {
                        label3: {
                            if (i <= 0) {
                                break label3;
                            }
                            j0 = android.os.SystemClock.elapsedRealtime() - this.lastConnectionFailure;
                            if (j0 < 10000L) {
                                break label2;
                            }
                        }
                        if (this.connectSocket()) {
                            this.lastConnectionFailure = 0L;
                            a.run();
                            b = true;
                            break label1;
                        } else {
                            this.lastConnectionFailure = android.os.SystemClock.elapsedRealtime();
                            b = false;
                            break label1;
                        }
                    }
                    if (!sLogger.isLoggable(2)) {
                        break label0;
                    }
                    sLogger.v(new StringBuilder().append("would retry time[").append(j0).append("]").toString());
                    break label0;
                } catch(java.io.IOException ignoredException) {
                    sLogger.e("Failed to ");
                    this.closeSocket();
                    this.lastConnectionFailure = android.os.SystemClock.elapsedRealtime();
                    b = false;
                }
                break label1;
            }
            b = false;
        }
        return b;
    }
    
    private boolean invokeUblox(byte[] a, String s) {
        return this.invokeUblox((com.navdy.hud.app.device.gps.GpsDeadReckoningManager$CommandWriter)new com.navdy.hud.app.device.gps.GpsDeadReckoningManager$8(this, s, a));
    }
    
    private void postAlignmentRunnable(boolean b) {
        this.handler.removeCallbacks(this.getAlignmentRunnable);
        this.handler.removeCallbacks(this.getAlignmentRunnableRetry);
        if (b) {
            this.handler.postDelayed(this.getAlignmentRunnable, 30000L);
        } else {
            this.handler.post(this.getAlignmentRunnable);
        }
    }
    
    private void postFusionRunnable(boolean b) {
        this.handler.removeCallbacks(this.getFusionStatusRunnable);
        this.handler.removeCallbacks(this.getFusionStatusRunnableRetry);
        if (b) {
            this.handler.postDelayed(this.getFusionStatusRunnable, 10000L);
        } else {
            this.handler.post(this.getFusionStatusRunnable);
        }
    }
    
    private void sendAutoAlignment() {
        if (com.navdy.hud.app.obd.ObdManager.getInstance().isConnected()) {
            sLogger.v(new StringBuilder().append("send auto alignment [").append(com.navdy.hud.app.device.gps.GpsDeadReckoningManager.bytesToHex(AUTO_ALIGNMENT, 0, AUTO_ALIGNMENT.length)).append("]").toString());
            this.waitForAutoAlignment = true;
            this.outputStream.write(AUTO_ALIGNMENT);
            this.postAlignmentRunnable(true);
            com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("GPS_Calibration_Start", new String[0]);
            com.navdy.hud.app.framework.voice.TTSUtils.debugShowGpsCalibrationStarted();
        } else {
            sLogger.v("not connected to obd manager any more");
        }
    }
    
    private void sendObdSpeed() {
        boolean b = sLogger.isLoggable(2);
        long j = (long)this.speedManager.getRawObdSpeed();
        if (j >= 0L) {
            this.timeStampCounter = this.timeStampCounter + 100;
            if (this.timeStampCounter < 0) {
                this.timeStampCounter = 100;
            }
            byte[] a = INJECT_OBD_SPEED;
            int i = (byte)(this.timeStampCounter >> 0);
            int i0 = (byte)i;
            a[6] = (byte)i0;
            byte[] a0 = INJECT_OBD_SPEED;
            int i1 = (byte)(this.timeStampCounter >> 8);
            int i2 = (byte)i1;
            a0[7] = (byte)i2;
            byte[] a1 = INJECT_OBD_SPEED;
            int i3 = (byte)(this.timeStampCounter >> 16);
            int i4 = (byte)i3;
            a1[8] = (byte)i4;
            byte[] a2 = INJECT_OBD_SPEED;
            int i5 = (byte)(this.timeStampCounter >> 24);
            int i6 = (byte)i5;
            a2[9] = (byte)i6;
            long j0 = (long)(int)(com.navdy.hud.app.manager.SpeedManager.convertWithPrecision((double)j, com.navdy.hud.app.manager.SpeedManager$SpeedUnit.KILOMETERS_PER_HOUR, com.navdy.hud.app.manager.SpeedManager$SpeedUnit.METERS_PER_SECOND) * 1000f);
            byte[] a3 = INJECT_OBD_SPEED;
            int i7 = (byte)(int)(j0 >> 0);
            int i8 = (byte)i7;
            a3[14] = (byte)i8;
            byte[] a4 = INJECT_OBD_SPEED;
            int i9 = (byte)(int)(j0 >> 8);
            int i10 = (byte)i9;
            a4[15] = (byte)i10;
            byte[] a5 = INJECT_OBD_SPEED;
            int i11 = (byte)(int)(j0 >> 16);
            int i12 = (byte)i11;
            a5[16] = (byte)i12;
            com.navdy.hud.app.device.gps.GpsDeadReckoningManager.calculateChecksum(INJECT_OBD_SPEED);
            if (b) {
                sLogger.v(new StringBuilder().append("inject obd speed [").append(com.navdy.hud.app.device.gps.GpsDeadReckoningManager.bytesToHex(INJECT_OBD_SPEED, 0, INJECT_OBD_SPEED.length)).append("]").toString());
            }
            this.outputStream.write(INJECT_OBD_SPEED);
        } else if (b) {
            sLogger.i(new StringBuilder().append("invalid obd speed:").append(j).toString());
        }
    }
    
    private void startDeadReckoning() {
        if (!this.deadReckoningInjectionStarted) {
            sLogger.v("starting dead reckoning injection");
            this.deadReckoningInjectionStarted = true;
            this.handler.removeCallbacks(this.injectRunnable);
            this.handler.postDelayed(this.injectRunnable, 100L);
        }
    }
    
    private void stopDeadReckoning() {
        if (this.deadReckoningInjectionStarted) {
            this.waitForAutoAlignment = false;
            this.waitForFusionStatus = false;
            this.alignmentInfo = null;
            this.rootInfo = null;
            this.alignmentChecked = false;
            sLogger.v("stopping dead rekoning injection");
            this.deadReckoningInjectionStarted = false;
            this.handler.removeCallbacks(this.injectRunnable);
            this.handler.removeCallbacks(this.getAlignmentRunnable);
            this.handler.removeCallbacks(this.getAlignmentRunnableRetry);
            this.handler.removeCallbacks(this.getFusionStatusRunnable);
            this.handler.removeCallbacks(this.getFusionStatusRunnableRetry);
        }
    }
    
    private void storeVinInfoInDb() {
        try {
            org.json.JSONObject a = null;
            String s = com.navdy.hud.app.obd.ObdManager.getInstance().getVin();
            if (s == null) {
                s = "UNKNOWN_VIN";
            }
            sLogger.v(new StringBuilder().append("store alignment info in db for vin[").append(s).append("]").toString());
            if (this.rootInfo != null) {
                a = this.rootInfo.getJSONObject("gps");
            } else {
                this.rootInfo = new org.json.JSONObject();
                a = new org.json.JSONObject();
                this.rootInfo.put("gps", a);
            }
            a.put("yaw", String.valueOf(this.alignmentInfo.yaw));
            a.put("pitch", String.valueOf(this.alignmentInfo.pitch));
            a.put("roll", String.valueOf(this.alignmentInfo.roll));
            a.put("alignment_time", String.valueOf(System.currentTimeMillis()));
            com.navdy.hud.app.storage.db.helper.VinInformationHelper.storeVinInfo(s, this.rootInfo.toString());
            com.navdy.hud.app.storage.db.helper.VinInformationHelper.getVinPreference().edit().putString("vin", s).commit();
            sLogger.v(new StringBuilder().append("store last vin pref [").append(s).append("]").toString());
            this.waitForAutoAlignment = false;
            this.waitForFusionStatus = false;
            this.alignmentChecked = true;
            com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("GPS_Calibration_Sensor_Done", new String[0]);
            com.navdy.hud.app.framework.voice.TTSUtils.debugShowGpsSensorCalibrationDone();
        } catch(Throwable a0) {
            sLogger.e(a0);
        }
    }
    
    public void ObdConnectionStatusEvent(com.navdy.hud.app.obd.ObdManager$ObdConnectionStatusEvent a) {
        if (a.connected) {
            sLogger.v("got obd connected");
            this.initDeadReckoning();
        } else {
            sLogger.v("got obd dis-connected");
            this.stopDeadReckoning();
        }
    }
    
    public void dumpGpsInfo(String s) {
        label3: {
            java.io.FileOutputStream a = null;
            Throwable a0 = null;
            label1: {
                label2: {
                    try {
                        a = new java.io.FileOutputStream(new StringBuilder().append(s).append(java.io.File.separator).append("gps.log").toString());
                        break label2;
                    } catch(Throwable a1) {
                        a0 = a1;
                    }
                    a = null;
                    break label1;
                }
                label0: {
                    try {
                        java.io.PrintWriter a2 = new java.io.PrintWriter((java.io.Writer)new java.io.OutputStreamWriter((java.io.OutputStream)a));
                        com.navdy.hud.app.obd.ObdManager a3 = com.navdy.hud.app.obd.ObdManager.getInstance();
                        a2.write(new StringBuilder().append("obd_connected=").append(a3.isConnected()).append("\n").toString());
                        if (this.deadReckoningInjectionStarted) {
                            String s0 = a3.getVin();
                            if (s0 == null) {
                                s0 = "UNKNOWN_VIN";
                            }
                            a2.write(new StringBuilder().append("vin=").append(s0).append("\n").toString());
                            a2.write(new StringBuilder().append("calibrated=").append((this.rootInfo != null) ? this.rootInfo.toString() : "no").toString());
                            a2.write("\n");
                        }
                        a2.flush();
                        sLogger.i(new StringBuilder().append("gps log written:").append(s).toString());
                        break label0;
                    } catch(Throwable a4) {
                        a0 = a4;
                    }
                    break label1;
                }
                com.navdy.service.library.util.IOUtils.fileSync(a);
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a);
                break label3;
            }
            try {
                sLogger.e(a0);
            } catch(Throwable a5) {
                com.navdy.service.library.util.IOUtils.fileSync(a);
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a);
                throw a5;
            }
            com.navdy.service.library.util.IOUtils.fileSync(a);
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a);
        }
    }
    
    public void enableEsfRaw() {
        this.handler.post(this.enableEsfRunnable);
    }
    
    public String getDeadReckoningStatus() {
        android.content.res.Resources a = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        com.navdy.hud.app.obd.ObdManager a0 = com.navdy.hud.app.obd.ObdManager.getInstance();
        return (a0.isConnected()) ? (a0.isSpeedPidAvailable()) ? (this.deadReckoningInjectionStarted) ? (this.rootInfo == null) ? (this.waitForAutoAlignment) ? a.getString(R.string.gps_calibrating_sensor) : (this.waitForFusionStatus) ? a.getString(R.string.gps_calibrating_fusion) : a.getString(R.string.gps_calibration_unknown) : new StringBuilder().append(a.getString(R.string.gps_calibrated)).append(" ").append(this.rootInfo.toString()).toString() : a.getString(R.string.gps_calibration_unknown) : a.getString(R.string.obd_no_speed_pid) : a.getString(R.string.obd_not_connected);
    }
    
    public void onDrivingStateChange(com.navdy.hud.app.event.DrivingStateChange a) {
        if (!a.driving) {
            this.sensorDataProcessor.setCalibrated(false);
        }
    }
    
    public void onSupportedPidEventsChange(com.navdy.hud.app.obd.ObdManager$ObdSupportedPidsChangedEvent a) {
        this.initDeadReckoning();
    }
    
    public void run() {
        sLogger.v("start thread");
        while(this.runThread) {
            Throwable a = null;
            label0: {
                try {
                    Throwable a0 = null;
                    int i = this.inputStream.read(READ_BUF);
                    if (i != -1) {
                        if (i <= 0) {
                            continue;
                        }
                        try {
                            Throwable a1 = null;
                            int i0 = com.navdy.hud.app.util.GenericUtil.indexOf(READ_BUF, ESF_RAW_PATTERN, 0, i - 1);
                            label1: {
                                Throwable a2 = null;
                                if (i0 == -1) {
                                    int i1 = com.navdy.hud.app.util.GenericUtil.indexOf(READ_BUF, NAV_STATUS_PATTERN, 0, i - 1);
                                    if (i1 == -1) {
                                        if (!this.alignmentChecked) {
                                            int i2 = com.navdy.hud.app.util.GenericUtil.indexOf(READ_BUF, ALIGNMENT_PATTERN, 0, i - 1);
                                            if (i2 != -1) {
                                                try {
                                                    this.handleAlignment(i2, i);
                                                    continue;
                                                } catch(Throwable a3) {
                                                    a1 = a3;
                                                    break label1;
                                                }
                                            }
                                        }
                                        if (!this.waitForFusionStatus) {
                                            continue;
                                        }
                                        int i3 = com.navdy.hud.app.util.GenericUtil.indexOf(READ_BUF, ESF_STATUS_PATTERN, 0, i - 1);
                                        if (i3 == -1) {
                                            continue;
                                        }
                                        try {
                                            this.handleFusion(i3, i);
                                            continue;
                                        } catch(Throwable a4) {
                                            a2 = a4;
                                        }
                                    } else {
                                        this.handleNavStatus(i1, i);
                                        continue;
                                    }
                                } else {
                                    this.handleEsfRaw(i0, i);
                                    continue;
                                }
                                sLogger.e(a2);
                                this.postFusionRunnable(true);
                                continue;
                            }
                            sLogger.e(a1);
                            this.postAlignmentRunnable(true);
                            continue;
                        } catch(Throwable a5) {
                            a0 = a5;
                        }
                    } else {
                        sLogger.v("eof");
                        this.runThread = false;
                        continue;
                    }
                    sLogger.e(a0);
                } catch(Throwable a6) {
                    a = a6;
                    break label0;
                }
                continue;
            }
            if (!(a instanceof InterruptedException)) {
                sLogger.e(a);
            }
            this.runThread = false;
        }
        sLogger.v("end thread");
    }
    
    public void sendWarmReset() {
        this.handler.postAtFrontOfQueue(this.resetRunnable);
        this.handler.postDelayed(this.enableEsfRunnable, 2000L);
    }
}
