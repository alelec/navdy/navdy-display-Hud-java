package com.navdy.hud.app.device.light;

public class LED$Settings extends com.navdy.hud.app.device.light.LightSettings {
    private boolean activityBlink;
    private com.navdy.hud.app.device.light.LED$BlinkFrequency blinkFrequency;
    private boolean blinkInfinite;
    private int blinkPulseCount;
    private String name;
    
    public LED$Settings(int i, boolean b, boolean b0, com.navdy.hud.app.device.light.LED$BlinkFrequency a, boolean b1, int i0, boolean b2, String s) {
        super(i, b, b0);
        this.blinkFrequency = a;
        this.blinkInfinite = b1;
        this.blinkPulseCount = i0;
        this.activityBlink = b2;
        this.name = s;
    }
    
    public boolean getActivityBlink() {
        return this.activityBlink;
    }
    
    public com.navdy.hud.app.device.light.LED$BlinkFrequency getBlinkFrequency() {
        return this.blinkFrequency;
    }
    
    public int getBlinkPulseCount() {
        return this.blinkPulseCount;
    }
    
    public boolean isBlinkInfinite() {
        return this.blinkInfinite;
    }
}
