package com.navdy.hud.app.device.dial;

class DialManager$23$1 implements Runnable {
    final com.navdy.hud.app.device.dial.DialManager$23 this$1;
    
    DialManager$23$1(com.navdy.hud.app.device.dial.DialManager$23 a) {
        super();
        this.this$1 = a;
    }
    
    public void run() {
        this.this$1.this$0.forgetDial(this.this$1.val$device);
        com.navdy.hud.app.device.dial.DialManager.sLogger.e(new StringBuilder().append("[DialEvent-encryptfail] bond removed :-/ connected[").append(com.navdy.hud.app.device.dial.DialManager.access$300(this.this$1.this$0)).append("] event[").append(this.this$1.val$device).append("]").toString());
        if (this.this$1.val$device.equals(com.navdy.hud.app.device.dial.DialManager.access$300(this.this$1.this$0))) {
            com.navdy.hud.app.device.dial.DialManager.access$1602(this.this$1.this$0, com.navdy.hud.app.device.dial.DialManager.access$300(this.this$1.this$0).getName());
            com.navdy.hud.app.device.dial.DialManager.access$1700(this.this$1.this$0);
            com.navdy.hud.app.device.dial.DialManager.access$1300().dialName = com.navdy.hud.app.device.dial.DialManager.access$1600(this.this$1.this$0);
            com.navdy.hud.app.device.dial.DialManager.access$200(this.this$1.this$0).post(com.navdy.hud.app.device.dial.DialManager.access$1300());
            com.navdy.hud.app.device.dial.DialManager.sLogger.e("[DialEvent-encryptfail] current dial is disconnected");
        }
        com.navdy.hud.app.device.dial.DialManagerHelper.sendLocalyticsEvent(com.navdy.hud.app.device.dial.DialManager.access$400(this.this$1.this$0), false, false, 0, false, "Encryption failed, deleting bond");
    }
}
