package com.navdy.hud.app.device.dial;

class DialManager$6$1 implements com.navdy.hud.app.device.dial.DialManagerHelper$IDialConnection {
    final com.navdy.hud.app.device.dial.DialManager$6 this$1;
    final android.bluetooth.BluetoothDevice val$device;
    
    DialManager$6$1(com.navdy.hud.app.device.dial.DialManager$6 a, android.bluetooth.BluetoothDevice a0) {
        super();
        this.this$1 = a;
        this.val$device = a0;
    }
    
    public void onAttemptConnection(boolean b) {
        com.navdy.hud.app.device.dial.DialManager.sLogger.v(new StringBuilder().append("btConnRecvr: attempt connection [").append(this.val$device.getName()).append(" ] success[").append(b).append("]").toString());
    }
    
    public void onAttemptDisconnection(boolean b) {
    }
}
