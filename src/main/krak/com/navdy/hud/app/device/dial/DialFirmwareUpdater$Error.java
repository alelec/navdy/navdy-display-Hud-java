package com.navdy.hud.app.device.dial;


public enum DialFirmwareUpdater$Error {
    NONE(0),
    INVALID_IMAGE_SIZE(1),
    INVALID_COMMAND(2),
    INVALID_RESPONSE(3),
    CANCELLED(4),
    TIMEDOUT(5);

    private int value;
    DialFirmwareUpdater$Error(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class DialFirmwareUpdater$Error extends Enum {
//    final private static com.navdy.hud.app.device.dial.DialFirmwareUpdater$Error[] $VALUES;
//    final public static com.navdy.hud.app.device.dial.DialFirmwareUpdater$Error CANCELLED;
//    final public static com.navdy.hud.app.device.dial.DialFirmwareUpdater$Error INVALID_COMMAND;
//    final public static com.navdy.hud.app.device.dial.DialFirmwareUpdater$Error INVALID_IMAGE_SIZE;
//    final public static com.navdy.hud.app.device.dial.DialFirmwareUpdater$Error INVALID_RESPONSE;
//    final public static com.navdy.hud.app.device.dial.DialFirmwareUpdater$Error NONE;
//    final public static com.navdy.hud.app.device.dial.DialFirmwareUpdater$Error TIMEDOUT;
//    
//    static {
//        NONE = new com.navdy.hud.app.device.dial.DialFirmwareUpdater$Error("NONE", 0);
//        INVALID_IMAGE_SIZE = new com.navdy.hud.app.device.dial.DialFirmwareUpdater$Error("INVALID_IMAGE_SIZE", 1);
//        INVALID_COMMAND = new com.navdy.hud.app.device.dial.DialFirmwareUpdater$Error("INVALID_COMMAND", 2);
//        INVALID_RESPONSE = new com.navdy.hud.app.device.dial.DialFirmwareUpdater$Error("INVALID_RESPONSE", 3);
//        CANCELLED = new com.navdy.hud.app.device.dial.DialFirmwareUpdater$Error("CANCELLED", 4);
//        TIMEDOUT = new com.navdy.hud.app.device.dial.DialFirmwareUpdater$Error("TIMEDOUT", 5);
//        com.navdy.hud.app.device.dial.DialFirmwareUpdater$Error[] a = new com.navdy.hud.app.device.dial.DialFirmwareUpdater$Error[6];
//        a[0] = NONE;
//        a[1] = INVALID_IMAGE_SIZE;
//        a[2] = INVALID_COMMAND;
//        a[3] = INVALID_RESPONSE;
//        a[4] = CANCELLED;
//        a[5] = TIMEDOUT;
//        $VALUES = a;
//    }
//    
//    private DialFirmwareUpdater$Error(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.device.dial.DialFirmwareUpdater$Error valueOf(String s) {
//        return (com.navdy.hud.app.device.dial.DialFirmwareUpdater$Error)Enum.valueOf(com.navdy.hud.app.device.dial.DialFirmwareUpdater$Error.class, s);
//    }
//    
//    public static com.navdy.hud.app.device.dial.DialFirmwareUpdater$Error[] values() {
//        return $VALUES.clone();
//    }
//}
//