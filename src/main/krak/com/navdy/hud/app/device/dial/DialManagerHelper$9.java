package com.navdy.hud.app.device.dial;

final class DialManagerHelper$9 implements Runnable {
    final com.navdy.hud.app.device.dial.DialManagerHelper$IDialConnection val$callBack;
    
    DialManagerHelper$9(com.navdy.hud.app.device.dial.DialManagerHelper$IDialConnection a) {
        super();
        this.val$callBack = a;
    }
    
    public void run() {
        this.val$callBack.onAttemptDisconnection(false);
    }
}
