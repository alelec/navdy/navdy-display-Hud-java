package com.navdy.hud.app.device.dial;

class DialManager$CharacteristicCommandProcessor {
    private android.bluetooth.BluetoothGatt bluetoothGatt;
    private java.util.LinkedList commandsQueue;
    private android.os.Handler handler;
    
    public DialManager$CharacteristicCommandProcessor(android.bluetooth.BluetoothGatt a, android.os.Handler a0) {
        this.bluetoothGatt = a;
        this.handler = a0;
        this.commandsQueue = new java.util.LinkedList();
    }
    
    static android.bluetooth.BluetoothGatt access$2500(com.navdy.hud.app.device.dial.DialManager$CharacteristicCommandProcessor a) {
        return a.bluetoothGatt;
    }
    
    public void commandFinished() {
        synchronized(this) {
            if (this.commandsQueue.size() > 0) {
                com.navdy.hud.app.device.dial.DialManager$CharacteristicCommandProcessor$Command a = (com.navdy.hud.app.device.dial.DialManager$CharacteristicCommandProcessor$Command)this.commandsQueue.poll();
                this.submitNext();
            }
        }
        /*monexit(this)*/;
    }
    
    public void process(com.navdy.hud.app.device.dial.DialManager$CharacteristicCommandProcessor$Command a) {
        synchronized(this) {
            this.commandsQueue.add(a);
            if (this.commandsQueue.size() == 1) {
                this.submitNext();
            }
        }
        /*monexit(this)*/;
    }
    
    public void release() {
        this.commandsQueue.clear();
        this.handler = null;
        this.bluetoothGatt = null;
    }
    
    public void submitNext() {
        synchronized(this) {
            com.navdy.hud.app.device.dial.DialManager$CharacteristicCommandProcessor$Command a = (com.navdy.hud.app.device.dial.DialManager$CharacteristicCommandProcessor$Command)this.commandsQueue.peek();
            if (a != null) {
                this.handler.post((Runnable)new com.navdy.hud.app.device.dial.DialManager$CharacteristicCommandProcessor$1(this, a));
            }
        }
        /*monexit(this)*/;
    }
}
