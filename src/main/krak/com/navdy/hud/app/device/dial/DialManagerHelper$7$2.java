package com.navdy.hud.app.device.dial;

class DialManagerHelper$7$2 implements Runnable {
    final com.navdy.hud.app.device.dial.DialManagerHelper$7 this$0;
    
    DialManagerHelper$7$2(com.navdy.hud.app.device.dial.DialManagerHelper$7 a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        try {
            com.navdy.hud.app.device.dial.DialManagerHelper.access$000().v("[Dial]disconnectFromDial onServiceDisconnected: could not connect proxy");
            if (!this.this$0.eventSent) {
                this.this$0.eventSent = true;
                this.this$0.val$callBack.onAttemptDisconnection(false);
            }
        } catch(Throwable a) {
            com.navdy.hud.app.device.dial.DialManagerHelper.access$000().e("[Dial]", a);
        }
    }
}
