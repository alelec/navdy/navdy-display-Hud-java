package com.navdy.hud.app.device.gps;

public class GpsConstants {
    final public static int ACCURACY_REPORTING_INTERVAL = 300000;
    final public static String DEAD_RECKONING_ONLY = "DEAD_RECKONING_ONLY";
    final public static String DEBUG_TTS_PHONE_SWITCH = "Switched to Phone Gps";
    final public static String DEBUG_TTS_UBLOX_SWITCH = "Switched to Ublox Gps";
    final public static String DRIVE_LOGS_FOLDER = "drive_logs";
    final public static String EMPTY = "";
    final public static String EXTRAPOLATION = "EXTRAPOLATION";
    final public static int EXTRAPOLATION_EXPIRY_INTERVAL = 5000;
    final public static String EXTRAPOLATION_FLAG = "EXTRAPOLATION_FLAG";
    final public static String GPS_COLLECT_LOGS = "GPS_COLLECT_LOGS";
    final public static String GPS_DEAD_RECKONING_COMBINED = "GPS_DEAD_RECKONING_COMBINED";
    final public static String GPS_EVENT_ACCURACY = "accuracy";
    final public static String GPS_EVENT_ACCURACY_AVERAGE = "Average_Accuracy";
    final public static String GPS_EVENT_ACCURACY_MAX = "Max_Accuracy";
    final public static String GPS_EVENT_ACCURACY_MIN = "Min_Accuracy";
    final public static String GPS_EVENT_DEAD_RECKONING_STARTED = "GPS_DR_STARTED";
    final public static String GPS_EVENT_DEAD_RECKONING_STOPPED = "GPS_DR_STOPPED";
    final public static String GPS_EVENT_DRIVING_STARTED = "driving_started";
    final public static String GPS_EVENT_DRIVING_STOPPED = "driving_stopped";
    final public static String GPS_EVENT_ENABLE_ESF_RAW = "GPS_ENABLE_ESF_RAW";
    final public static String GPS_EVENT_SATELLITE_DATA = "satellite_data";
    final public static String GPS_EVENT_SWITCH = "GPS_Switch";
    final public static String GPS_EVENT_TIME = "time";
    final public static String GPS_EVENT_WARM_RESET_UBLOX = "GPS_WARM_RESET_UBLOX";
    final public static String GPS_EXTRA_DR_TYPE = "drtype";
    final public static String GPS_EXTRA_LOG_PATH = "logPath";
    final public static String GPS_FIX_TYPE = "SAT_FIX_TYPE";
    final public static String GPS_PHONE_MARKER = "P";
    final public static String GPS_SATELLITE_DB = "SAT_DB_";
    final public static String GPS_SATELLITE_ID = "SAT_ID_";
    final public static String GPS_SATELLITE_MAX_DB = "SAT_MAX_DB";
    final public static String GPS_SATELLITE_PROVIDER = "SAT_PROVIDER_";
    final public static String GPS_SATELLITE_SEEN = "SAT_SEEN";
    final public static String GPS_SATELLITE_STATUS = "GPS_SATELLITE_STATUS";
    final public static String GPS_SATELLITE_USED = "SAT_USED";
    final public static String GPS_UBLOX_MARKER = "U";
    final public static String INFO = "info";
    final public static int LOCATION_FIX_CHECK_INTERVAL = 1000;
    final public static int LOCATION_FIX_THRESHOLD = 3000;
    final public static String NAVDY_GPS_PROVIDER = "NAVDY_GPS_PROVIDER";
    final public static int NO_LOCATION_FIX_CHECK_INTERVAL = 5000;
    final public static String SPACE = " ";
    final public static String TITLE = "title";
    final public static String USING_PHONE_LOCATION = "phone";
    final public static String USING_UBLOX_LOCATION = "ublox";
    
    public GpsConstants() {
    }
}
