package com.navdy.hud.app.device.gps;


    public enum GpsManager$LocationSource {
        UBLOX(0),
        PHONE(1);

        private int value;
        GpsManager$LocationSource(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class GpsManager$LocationSource extends Enum {
//    final private static com.navdy.hud.app.device.gps.GpsManager$LocationSource[] $VALUES;
//    final public static com.navdy.hud.app.device.gps.GpsManager$LocationSource PHONE;
//    final public static com.navdy.hud.app.device.gps.GpsManager$LocationSource UBLOX;
//    
//    static {
//        UBLOX = new com.navdy.hud.app.device.gps.GpsManager$LocationSource("UBLOX", 0);
//        PHONE = new com.navdy.hud.app.device.gps.GpsManager$LocationSource("PHONE", 1);
//        com.navdy.hud.app.device.gps.GpsManager$LocationSource[] a = new com.navdy.hud.app.device.gps.GpsManager$LocationSource[2];
//        a[0] = UBLOX;
//        a[1] = PHONE;
//        $VALUES = a;
//    }
//    
//    private GpsManager$LocationSource(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.device.gps.GpsManager$LocationSource valueOf(String s) {
//        return (com.navdy.hud.app.device.gps.GpsManager$LocationSource)Enum.valueOf(com.navdy.hud.app.device.gps.GpsManager$LocationSource.class, s);
//    }
//    
//    public static com.navdy.hud.app.device.gps.GpsManager$LocationSource[] values() {
//        return $VALUES.clone();
//    }
//}
//