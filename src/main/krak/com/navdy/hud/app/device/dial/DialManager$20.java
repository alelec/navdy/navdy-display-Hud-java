package com.navdy.hud.app.device.dial;

class DialManager$20 implements com.navdy.hud.app.device.dial.DialManagerHelper$IDialForgotten {
    final com.navdy.hud.app.device.dial.DialManager this$0;
    final com.navdy.hud.app.device.dial.DialManagerHelper$IDialForgotten val$callback;
    final java.util.concurrent.atomic.AtomicInteger val$counter;
    
    DialManager$20(com.navdy.hud.app.device.dial.DialManager a, java.util.concurrent.atomic.AtomicInteger a0, com.navdy.hud.app.device.dial.DialManagerHelper$IDialForgotten a1) {
        super();
        this.this$0 = a;
        this.val$counter = a0;
        this.val$callback = a1;
    }
    
    public void onForgotten() {
        int i = this.val$counter.decrementAndGet();
        com.navdy.hud.app.device.dial.DialManager.sLogger.v(new StringBuilder().append("forgetAllDials counter:").append(i).toString());
        if (i == 0) {
            com.navdy.hud.app.device.dial.DialManager.access$100().dialName = null;
            com.navdy.hud.app.device.dial.DialManager.access$200(this.this$0).post(com.navdy.hud.app.device.dial.DialManager.access$100());
            if (this.val$callback != null) {
                this.val$callback.onForgotten();
            }
        }
    }
}
