package com.navdy.hud.app.device.gps;

class GpsManager$9 implements android.os.Handler$Callback {
    final com.navdy.hud.app.device.gps.GpsManager this$0;
    
    GpsManager$9(com.navdy.hud.app.device.gps.GpsManager a) {
        super();
        this.this$0 = a;
    }
    
    public boolean handleMessage(android.os.Message a) {
        try {
            switch(a.what) {
                case 2: {
                    if (android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.device.gps.GpsManager.access$2700(this.this$0) <= 15000L) {
                        break;
                    }
                    android.os.Bundle a0 = (android.os.Bundle)a.obj;
                    android.os.Bundle a1 = new android.os.Bundle();
                    a1.putBundle("satellite_data", a0);
                    com.navdy.hud.app.device.gps.GpsManager.access$2300(this.this$0, "GPS_SATELLITE_STATUS", a1);
                    break;
                }
                case 1: {
                    com.navdy.hud.app.device.gps.GpsManager.access$2600(this.this$0).parseNmeaMessage((String)a.obj);
                    break;
                }
            }
        } catch(Throwable a2) {
            com.navdy.hud.app.device.gps.GpsManager.access$000().e(a2);
        }
        return true;
    }
}
