package com.navdy.hud.app.device.dial;

class DialManager$12 implements Runnable {
    final com.navdy.hud.app.device.dial.DialManager this$0;
    
    DialManager$12(com.navdy.hud.app.device.dial.DialManager a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        try {
            if (com.navdy.hud.app.device.dial.DialManager.access$2400(this.this$0) == null) {
                com.navdy.hud.app.device.dial.DialManager.sLogger.v("not queuing hid host ready characteristic: null");
            } else {
                com.navdy.hud.app.device.dial.DialManager.sLogger.v("queuing hid host ready characteristic");
                this.this$0.queueRead(com.navdy.hud.app.device.dial.DialManager.access$2400(this.this$0));
            }
        } catch(Throwable a) {
            com.navdy.hud.app.device.dial.DialManager.sLogger.e("[Dial]", a);
        }
    }
}
