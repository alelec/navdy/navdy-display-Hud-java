package com.navdy.hud.app.device.gps;

class GpsManager$1 implements android.location.GpsStatus$NmeaListener {
    final com.navdy.hud.app.device.gps.GpsManager this$0;
    
    GpsManager$1(com.navdy.hud.app.device.gps.GpsManager a) {
        super();
        this.this$0 = a;
    }
    
    public void onNmeaReceived(long j, String s) {
        try {
            if (com.navdy.hud.app.device.gps.GpsManager.access$000().isLoggable(2)) {
                com.navdy.hud.app.device.gps.GpsManager.access$000().v(new StringBuilder().append("[Gps-nmea] ").append(s).toString());
            }
            com.navdy.hud.app.device.gps.GpsManager.access$100(this.this$0, s);
        } catch(Throwable a) {
            com.navdy.hud.app.device.gps.GpsManager.access$000().e(a);
        }
    }
}
