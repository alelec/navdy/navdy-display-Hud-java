package com.navdy.hud.app.device.gps;

class GpsManager$5 implements Runnable {
    final com.navdy.hud.app.device.gps.GpsManager this$0;
    
    GpsManager$5(com.navdy.hud.app.device.gps.GpsManager a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        label0: {
            Throwable a = null;
            label1: {
                boolean b = false;
                try {
                    if (com.navdy.hud.app.device.gps.GpsManager.access$300(this.this$0) != com.navdy.hud.app.device.gps.GpsManager$LocationSource.UBLOX) {
                        com.navdy.hud.app.device.gps.GpsManager.access$1600(this.this$0, (android.location.Location)null);
                        com.navdy.hud.app.device.gps.GpsManager.access$2100(this.this$0);
                        b = true;
                    } else {
                        com.navdy.hud.app.device.gps.GpsManager.access$000().i("[Gps-loc] noPhoneLocationFixRunnable: has location fix now");
                        b = false;
                    }
                } catch(Throwable a0) {
                    a = a0;
                    break label1;
                }
                if (!b) {
                    break label0;
                }
                com.navdy.hud.app.device.gps.GpsManager.access$500(this.this$0).postDelayed(com.navdy.hud.app.device.gps.GpsManager.access$400(this.this$0), 5000L);
                break label0;
            }
            try {
                com.navdy.hud.app.device.gps.GpsManager.access$000().e(a);
            } catch(Throwable a1) {
                com.navdy.hud.app.device.gps.GpsManager.access$500(this.this$0).postDelayed(com.navdy.hud.app.device.gps.GpsManager.access$400(this.this$0), 5000L);
                throw a1;
            }
            com.navdy.hud.app.device.gps.GpsManager.access$500(this.this$0).postDelayed(com.navdy.hud.app.device.gps.GpsManager.access$400(this.this$0), 5000L);
        }
    }
}
