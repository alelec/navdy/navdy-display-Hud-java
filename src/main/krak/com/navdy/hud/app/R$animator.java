package com.navdy.hud.app;
import com.navdy.hud.app.R;

final public class R$animator {
    final public static int listening_feedback_animation = R.animator.listening_feedback_animation;
    final public static int rotate = R.animator.rotate;
    final public static int rotate_x = R.animator.rotate_x;
    final public static int rotate_y = R.animator.rotate_y;
    
    public R$animator() {
    }
}
