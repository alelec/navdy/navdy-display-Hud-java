package com.navdy.hud.app;

abstract public interface IEventSource extends android.os.IInterface {
    abstract public void addEventListener(com.navdy.hud.app.IEventListener arg);
    
    
    abstract public void postEvent(byte[] arg);
    
    
    abstract public void postRemoteEvent(String arg, byte[] arg0);
    
    
    abstract public void removeEventListener(com.navdy.hud.app.IEventListener arg);
}
