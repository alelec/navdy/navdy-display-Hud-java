package com.navdy.hud.app;

public class HudApplication extends android.support.multidex.MultiDexApplication {
    final public static String NOT_A_CRASH = "NAVDY_NOT_A_CRASH";
    final private static String TAG = "HudApplication";
    private static android.content.Context sAppContext;
    private static com.navdy.hud.app.HudApplication sApplication;
    final private static com.navdy.service.library.log.Logger sLogger;
    @Inject
    public com.squareup.otto.Bus bus;
    private android.os.Handler handler;
    private boolean initialized;
    private com.navdy.hud.app.event.Shutdown$Reason lastSeenReason;
    private mortar.MortarScope rootScope;
    private com.navdy.hud.app.manager.UpdateReminderManager updateReminderManager;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.HudApplication.class);
    }
    
    public HudApplication() {
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.lastSeenReason = com.navdy.hud.app.event.Shutdown$Reason.UNKNOWN;
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    public static android.content.Context getAppContext() {
        return sAppContext;
    }
    
    public static com.navdy.hud.app.HudApplication getApplication() {
        return sApplication;
    }
    
    private void initConnectionService(String s) {
        sLogger.v(new StringBuilder().append(s).append(":initializing taskMgr").toString());
        com.navdy.service.library.task.TaskManager a = com.navdy.service.library.task.TaskManager.getInstance();
        a.addTaskQueue(1, 3);
        a.addTaskQueue(5, 1);
        a.addTaskQueue(9, 1);
        a.init();
        sLogger.v(new StringBuilder().append(s).append(":initializing gps manager").toString());
        com.navdy.hud.app.device.gps.GpsManager.getInstance();
        android.content.IntentFilter a0 = new android.content.IntentFilter("com.navdy.service.library.log.action.RELOAD");
        this.registerReceiver((android.content.BroadcastReceiver)new com.navdy.hud.app.receiver.LogLevelReceiver(), a0);
    }
    
    private static void initLogger(boolean b) {
        com.navdy.service.library.log.LogAppender[] a = new com.navdy.service.library.log.LogAppender[1];
        a[0] = (com.navdy.service.library.log.LogAppender)new com.navdy.service.library.log.LogcatAppender();
        com.navdy.service.library.log.Logger.init(a);
    }
    
    private void initTaskManager(String s) {
        sLogger.v(new StringBuilder().append(s).append(":initializing taskMgr").toString());
        com.navdy.service.library.task.TaskManager a = com.navdy.service.library.task.TaskManager.getInstance();
        try {
            a.addTaskQueue(1, 3);
            a.addTaskQueue(5, 1);
            a.addTaskQueue(8, 1);
            a.addTaskQueue(6, 1);
            a.addTaskQueue(7, 1);
            a.addTaskQueue(9, 1);
            a.addTaskQueue(10, 1);
            a.addTaskQueue(11, 1);
            a.addTaskQueue(12, 1);
            a.addTaskQueue(13, 1);
            a.addTaskQueue(14, 1);
            a.addTaskQueue(22, 1);
            a.addTaskQueue(15, 1);
            a.addTaskQueue(16, 1);
            a.addTaskQueue(17, 1);
            a.addTaskQueue(18, 1);
            a.addTaskQueue(2, 5);
            a.addTaskQueue(3, 1);
            a.addTaskQueue(4, 1);
            a.addTaskQueue(19, 1);
            a.addTaskQueue(20, 1);
            a.addTaskQueue(21, 1);
            a.addTaskQueue(23, 1);
            a.init();
        } catch(IllegalStateException a0) {
            if (!a0.getMessage().equals("already initialized")) {
                throw a0;
            }
        }
    }
    
    public static boolean isDeveloperBuild() {
        return false;
    }
    
    public static void setContext(android.content.Context a) {
        sAppContext = a;
    }
    
    protected void attachBaseContext(android.content.Context a) {
        super.attachBaseContext(com.navdy.hud.app.profile.HudLocale.onAttach(a));
    }
    
    public com.squareup.otto.Bus getBus() {
        return this.bus;
    }
    
    public mortar.MortarScope getRootScope() {
        return this.rootScope;
    }
    
    public Object getSystemService(String s) {
        Object a = (mortar.Mortar.isScopeSystemService(s)) ? this.rootScope : super.getSystemService(s);
        return a;
    }
    
    public void initHudApp() {
        if (!this.initialized) {
            this.initialized = true;
            String s = com.navdy.service.library.util.SystemUtils.getProcessName(sAppContext, android.os.Process.myPid());
            com.navdy.hud.app.util.NavdyNativeLibWrapper.loadlibrary();
            if (com.navdy.hud.app.util.CrashReporter.isEnabled()) {
                sLogger.v(new StringBuilder().append(s).append(":initializing crash reporter").toString());
                com.navdy.hud.app.util.CrashReporter.getInstance().installCrashHandler(sAppContext);
            } else {
                sLogger.v(new StringBuilder().append(s).append(":crash reporter not installed").toString());
            }
            if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
                com.navdy.hud.app.util.os.PropsFileUpdater.run();
                com.navdy.hud.app.device.ProjectorBrightness.init();
            }
            sLogger.v(new StringBuilder().append(s).append(":updating logging setup").toString());
            com.navdy.hud.app.HudApplication.initLogger(true);
            android.content.IntentFilter a = new android.content.IntentFilter();
            a.addAction("com.navdy.hud.app.service.OTA_DOWNLOAD");
            android.content.SharedPreferences a0 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getSharedPreferences();
            if (a0 != null) {
                this.registerReceiver((android.content.BroadcastReceiver)new com.navdy.hud.app.util.OTAUpdateService$OTADownloadIntentsReceiver(a0), a);
            } else {
                sLogger.e("unable to get SharedPreferences");
            }
            com.navdy.hud.app.util.picasso.PicassoUtil.initPicasso(sAppContext);
            com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getSharedPreferences().edit();
            com.navdy.service.library.log.Logger a1 = sLogger;
            label1: {
                Throwable a2 = null;
                label0: try {
                    a1.v("dbase:opening...");
                    long j = android.os.SystemClock.elapsedRealtime();
                    com.navdy.hud.app.storage.db.HudDatabase.getInstance().getWritableDatabase();
                    sLogger.v(new StringBuilder().append("dbase: opened[").append(android.os.SystemClock.elapsedRealtime() - j).append("]").toString());
                    break label1;
                } catch(Throwable a3) {
                    sLogger.e("dbase: error opening, deleting it", a3);
                    com.navdy.hud.app.storage.db.HudDatabase.deleteDatabaseFile();
                    sLogger.e("dbase: deletinged");
                    com.navdy.service.library.log.Logger a4 = sLogger;
                    try {
                        a4.v("dbase:re-opening");
                        long j0 = android.os.SystemClock.elapsedRealtime();
                        com.navdy.hud.app.storage.db.HudDatabase.getInstance().getWritableDatabase();
                        sLogger.v(new StringBuilder().append("dbase: opened[").append(android.os.SystemClock.elapsedRealtime() - j0).append("]").toString());
                    } catch(Throwable ignoredException) {
                        a2 = a3;
                        break label0;
                    }
                    break label1;
                }
                sLogger.e("dbase: error opening", a2);
            }
            com.navdy.hud.app.analytics.AnalyticsSupport.analyticsApplicationInit((android.app.Application)this);
            com.navdy.hud.app.debug.DriveRecorder a5 = (com.navdy.hud.app.debug.DriveRecorder)this.rootScope.getObjectGraph().get(com.navdy.hud.app.debug.DriveRecorder.class);
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.HudApplication$1(this, a5), 1);
            sLogger.v(new StringBuilder().append(s).append(":initializing here maps engine").toString());
            com.navdy.hud.app.maps.here.HereMapsManager.getInstance();
            sLogger.v(new StringBuilder().append(s).append(":called initialized").toString());
            String dummy = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.speedMph;
            com.here.android.mpa.common.ViewRect dummy0 = com.navdy.hud.app.ui.component.homescreen.HomeScreenConstants.routeOverviewRect;
            int dummy1 = com.navdy.hud.app.ui.component.homescreen.SmartDashViewResourceValues.middleGaugeShrinkLeftX;
            String dummy2 = com.navdy.hud.app.framework.voice.TTSUtils.TTS_DIAL_BATTERY_EXTREMELY_LOW;
            int dummy3 = com.navdy.hud.app.framework.glance.GlanceConstants.colorFacebook;
            com.navdy.hud.app.framework.glance.GlanceHelper.initMessageAttributes();
            if (com.navdy.hud.app.util.CrashReporter.isEnabled()) {
                this.handler.post((Runnable)new com.navdy.hud.app.HudApplication$2(this));
                this.handler.postDelayed((Runnable)new com.navdy.hud.app.HudApplication$3(this), 45000L);
            }
            com.navdy.hud.app.device.dial.DialManager.getInstance();
            com.navdy.hud.app.framework.recentcall.RecentCallManager.getInstance();
            com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.getInstance();
            com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance();
            com.navdy.hud.app.framework.contacts.FavoriteContactsManager.getInstance();
            com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance();
            com.navdy.hud.app.framework.message.MessageManager.getInstance();
            com.navdy.hud.app.framework.glance.GlanceHandler.getInstance();
            com.navdy.hud.app.util.ReportIssueService.initialize();
            com.navdy.hud.app.service.ShutdownMonitor.getInstance();
            if (!com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
                com.navdy.hud.app.audio.SoundUtils.init();
            }
            com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getFeatureUtil();
            if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
                sLogger.v("start dead reckoning mgr");
                com.navdy.hud.app.device.gps.GpsDeadReckoningManager.getInstance();
            }
            this.updateReminderManager = new com.navdy.hud.app.manager.UpdateReminderManager((android.content.Context)this);
            sLogger.i("init network b/w controller");
            com.navdy.hud.app.framework.network.NetworkBandwidthController.getInstance();
            com.navdy.hud.app.util.os.CpuProfiler.getInstance().start();
            this.handler.post((Runnable)new com.navdy.hud.app.HudApplication$4(this));
            sLogger.v(new StringBuilder().append(s).append(":background init done").toString());
        }
    }
    
    public void onCreate() {
        android.util.Log.e("", new StringBuilder().append("::onCreate locale:").append(this.getResources().getConfiguration().locale).toString());
        sApplication = this;
        sAppContext = sApplication;
        super.onCreate();
        String s = com.navdy.service.library.util.SystemUtils.getProcessName(sAppContext, android.os.Process.myPid());
        String s0 = this.getPackageName();
        com.navdy.hud.app.HudApplication.initLogger(false);
        sLogger.d(new StringBuilder().append(s).append(" starting  on ").append(android.os.Build.BRAND).append(",").append(android.os.Build.HARDWARE).append(",").append(android.os.Build.MODEL).toString());
        if (s.equalsIgnoreCase(new StringBuilder().append(s0).append(":connectionService").toString())) {
            sLogger.v(new StringBuilder().append("startup:").append(s).append(" :connection service").toString());
            this.initConnectionService(s);
        } else if (s.equalsIgnoreCase(s0)) {
            java.util.Locale a = java.util.Locale.getDefault();
            sLogger.v(new StringBuilder().append("startup:").append(s).append(" :hud app locale=").append(a.toString()).toString());
            sLogger.v(new StringBuilder().append(s).append(":initializing Mortar").toString());
            long j = android.os.SystemClock.elapsedRealtime();
            Object[] a0 = new Object[1];
            a0[0] = new com.navdy.hud.app.common.ProdModule((android.content.Context)this);
            this.rootScope = mortar.Mortar.createRootScope(false, dagger.ObjectGraph.create(a0));
            this.rootScope.getObjectGraph().inject(this);
            long j0 = android.os.SystemClock.elapsedRealtime();
            sLogger.v(new StringBuilder().append(s).append(":mortar init took:").append(j0 - j).toString());
            this.initTaskManager(s);
            sLogger.i("init network state manager");
            com.navdy.hud.app.framework.network.NetworkStateManager.getInstance();
            sLogger.i("*** starting hud sticky-service ***");
            android.content.Intent a1 = new android.content.Intent();
            a1.setClass((android.content.Context)this, com.navdy.hud.app.service.StickyService.class);
            this.startService(a1);
            sLogger.i("*** started hud sticky-service ***");
            android.content.IntentFilter a2 = new android.content.IntentFilter();
            a2.addAction("GPS_Switch");
            a2.addAction("GPS_WARM_RESET_UBLOX");
            a2.addAction("GPS_SATELLITE_STATUS");
            a2.addAction("GPS_COLLECT_LOGS");
            a2.addAction("driving_started");
            a2.addAction("driving_stopped");
            a2.addAction("GPS_ENABLE_ESF_RAW");
            this.registerReceiver((android.content.BroadcastReceiver)new com.navdy.hud.app.maps.GpsEventsReceiver(), a2);
            sLogger.v("registered GpsEventsReceiver");
            android.content.IntentFilter a3 = new android.content.IntentFilter();
            a3.addAction("com.navdy.hud.app.analytics.AnalyticsEvent");
            this.registerReceiver((android.content.BroadcastReceiver)new com.navdy.hud.app.analytics.AnalyticsSupport$AnalyticsIntentsReceiver(), a3);
            sLogger.v("registered AnalyticsIntentsReceiver");
            android.content.Intent a4 = new android.content.Intent((android.content.Context)this, com.navdy.hud.app.ui.activity.MainActivity.class);
            a4.addFlags(268435456);
            this.startActivity(a4);
        } else {
            sLogger.v(new StringBuilder().append("startup:").append(s).append(" :no-op").toString());
        }
        sLogger.v(new StringBuilder().append("startup:").append(s).append(" :initialization done").toString());
    }
    
    public void setShutdownReason(com.navdy.hud.app.event.Shutdown$Reason a) {
        this.lastSeenReason = a;
    }
    
    public void shutdown() {
        sLogger.i("shutting down HUD");
        com.navdy.hud.app.util.CrashReporter.getInstance().stopCrashReporting(true);
        android.content.Intent a = new android.content.Intent();
        a.setClass((android.content.Context)this, com.navdy.hud.app.service.StickyService.class);
        this.stopService(a);
        if (this.bus != null) {
            sLogger.i(new StringBuilder().append("shutting down HUD - reason: ").append(this.lastSeenReason).toString());
            this.bus.post(new com.navdy.hud.app.event.Shutdown(this.lastSeenReason, com.navdy.hud.app.event.Shutdown$State.SHUTTING_DOWN));
        }
        com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLocationFixManager().shutdown();
    }
}
