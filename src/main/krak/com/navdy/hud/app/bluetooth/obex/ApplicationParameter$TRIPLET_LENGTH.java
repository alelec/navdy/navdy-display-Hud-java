package com.navdy.hud.app.bluetooth.obex;

public class ApplicationParameter$TRIPLET_LENGTH {
    final public static byte FILTER_LENGTH = (byte)8;
    final public static byte FORMAT_LENGTH = (byte)1;
    final public static byte LISTSTARTOFFSET_LENGTH = (byte)2;
    final public static byte MAXLISTCOUNT_LENGTH = (byte)2;
    final public static byte NEWMISSEDCALLS_LENGTH = (byte)1;
    final public static byte ORDER_LENGTH = (byte)1;
    final public static byte PHONEBOOKSIZE_LENGTH = (byte)2;
    final public static byte SEARCH_ATTRIBUTE_LENGTH = (byte)1;
    
    public ApplicationParameter$TRIPLET_LENGTH() {
    }
}
