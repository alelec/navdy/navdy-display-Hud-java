package com.navdy.hud.app.bluetooth.vcard;

public class VCardEntry$PhotoData implements com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryElement {
    final private byte[] mBytes;
    final private String mFormat;
    private Integer mHashCode;
    final private boolean mIsPrimary;
    
    public VCardEntry$PhotoData(String s, byte[] a, boolean b) {
        this.mHashCode = null;
        this.mFormat = s;
        this.mBytes = a;
        this.mIsPrimary = b;
    }
    
    public void constructInsertOperation(java.util.List a, int i) {
        android.content.ContentProviderOperation$Builder a0 = android.content.ContentProviderOperation.newInsert(android.provider.ContactsContract$Data.CONTENT_URI);
        a0.withValueBackReference("raw_contact_id", i);
        a0.withValue("mimetype", "vnd.android.cursor.item/photo");
        a0.withValue("data15", this.mBytes);
        if (this.mIsPrimary) {
            a0.withValue("is_primary", Integer.valueOf(1));
        }
        a.add(a0.build());
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (this != a) {
            if (a instanceof com.navdy.hud.app.bluetooth.vcard.VCardEntry$PhotoData) {
                com.navdy.hud.app.bluetooth.vcard.VCardEntry$PhotoData a0 = (com.navdy.hud.app.bluetooth.vcard.VCardEntry$PhotoData)a;
                boolean b0 = android.text.TextUtils.equals((CharSequence)this.mFormat, (CharSequence)a0.mFormat);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!java.util.Arrays.equals(this.mBytes, a0.mBytes)) {
                        break label1;
                    }
                    if (this.mIsPrimary == a0.mIsPrimary) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public byte[] getBytes() {
        return this.mBytes;
    }
    
    final public com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel getEntryLabel() {
        return com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel.PHOTO;
    }
    
    public String getFormat() {
        return this.mFormat;
    }
    
    public int hashCode() {
        int i = 0;
        if (this.mHashCode == null) {
            int i0 = ((this.mFormat == null) ? 0 : this.mFormat.hashCode()) * 31;
            if (this.mBytes != null) {
                byte[] a = this.mBytes;
                int i1 = a.length;
                int i2 = 0;
                while(i2 < i1) {
                    int i3 = a[i2];
                    i0 = i0 + i3;
                    i2 = i2 + 1;
                }
            }
            i = i0 * 31 + ((this.mIsPrimary) ? 1231 : 1237);
            this.mHashCode = Integer.valueOf(i);
        } else {
            i = this.mHashCode.intValue();
        }
        return i;
    }
    
    public boolean isEmpty() {
        boolean b = false;
        byte[] a = this.mBytes;
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (this.mBytes.length != 0) {
                        break label0;
                    }
                }
                b = true;
                break label2;
            }
            b = false;
        }
        return b;
    }
    
    public boolean isPrimary() {
        return this.mIsPrimary;
    }
    
    public String toString() {
        Object[] a = new Object[3];
        a[0] = this.mFormat;
        a[1] = Integer.valueOf(this.mBytes.length);
        a[2] = Boolean.valueOf(this.mIsPrimary);
        return String.format("format: %s: size: %d, isPrimary: %s", a);
    }
}
