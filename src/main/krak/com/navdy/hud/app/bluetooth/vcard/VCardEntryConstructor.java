package com.navdy.hud.app.bluetooth.vcard;

public class VCardEntryConstructor implements com.navdy.hud.app.bluetooth.vcard.VCardInterpreter {
    private static String LOG_TAG;
    final private android.accounts.Account mAccount;
    private com.navdy.hud.app.bluetooth.vcard.VCardEntry mCurrentEntry;
    final private java.util.List mEntryHandlers;
    final private java.util.List mEntryStack;
    final private int mVCardType;
    
    static {
        LOG_TAG = "vCard";
    }
    
    public VCardEntryConstructor() {
        this(-1073741824, (android.accounts.Account)null, (String)null);
    }
    
    public VCardEntryConstructor(int i) {
        this(i, (android.accounts.Account)null, (String)null);
    }
    
    public VCardEntryConstructor(int i, android.accounts.Account a) {
        this(i, a, (String)null);
    }
    
    public VCardEntryConstructor(int i, android.accounts.Account a, String s) {
        this.mEntryStack = (java.util.List)new java.util.ArrayList();
        this.mEntryHandlers = (java.util.List)new java.util.ArrayList();
        this.mVCardType = i;
        this.mAccount = a;
    }
    
    public void addEntryHandler(com.navdy.hud.app.bluetooth.vcard.VCardEntryHandler a) {
        this.mEntryHandlers.add(a);
    }
    
    public void clear() {
        this.mCurrentEntry = null;
        this.mEntryStack.clear();
    }
    
    public void onEntryEnded() {
        this.mCurrentEntry.consolidateFields();
        Object a = this.mEntryHandlers.iterator();
        while(((java.util.Iterator)a).hasNext()) {
            ((com.navdy.hud.app.bluetooth.vcard.VCardEntryHandler)((java.util.Iterator)a).next()).onEntryCreated(this.mCurrentEntry);
        }
        int i = this.mEntryStack.size();
        if (i <= 1) {
            this.mCurrentEntry = null;
        } else {
            com.navdy.hud.app.bluetooth.vcard.VCardEntry a0 = (com.navdy.hud.app.bluetooth.vcard.VCardEntry)this.mEntryStack.get(i - 2);
            a0.addChild(this.mCurrentEntry);
            this.mCurrentEntry = a0;
        }
        this.mEntryStack.remove(i - 1);
    }
    
    public void onEntryStarted() {
        this.mCurrentEntry = new com.navdy.hud.app.bluetooth.vcard.VCardEntry(this.mVCardType, this.mAccount);
        this.mEntryStack.add(this.mCurrentEntry);
    }
    
    public void onPropertyCreated(com.navdy.hud.app.bluetooth.vcard.VCardProperty a) {
        this.mCurrentEntry.addProperty(a);
    }
    
    public void onVCardEnded() {
        Object a = this.mEntryHandlers.iterator();
        while(((java.util.Iterator)a).hasNext()) {
            ((com.navdy.hud.app.bluetooth.vcard.VCardEntryHandler)((java.util.Iterator)a).next()).onEnd();
        }
    }
    
    public void onVCardStarted() {
        Object a = this.mEntryHandlers.iterator();
        while(((java.util.Iterator)a).hasNext()) {
            ((com.navdy.hud.app.bluetooth.vcard.VCardEntryHandler)((java.util.Iterator)a).next()).onStart();
        }
    }
}
