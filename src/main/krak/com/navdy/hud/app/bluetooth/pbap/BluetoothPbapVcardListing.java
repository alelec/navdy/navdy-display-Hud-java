package com.navdy.hud.app.bluetooth.pbap;

class BluetoothPbapVcardListing {
    final private static String TAG = "BTPbapVcardList";
    java.util.ArrayList mCards;
    
    public BluetoothPbapVcardListing(java.io.InputStream a) {
        this.mCards = new java.util.ArrayList();
        this.parse(a);
    }
    
    private void parse(java.io.InputStream a) {
        org.xmlpull.v1.XmlPullParser a0 = android.util.Xml.newPullParser();
        try {
            a0.setInput(a, "UTF-8");
            int i = a0.getEventType();
            Object a1 = a0;
            while(i != 1) {
                if (i == 2 && ((org.xmlpull.v1.XmlPullParser)a1).getName().equals("card")) {
                    com.navdy.hud.app.bluetooth.pbap.BluetoothPbapCard a2 = new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapCard(((org.xmlpull.v1.XmlPullParser)a1).getAttributeValue((String)null, "handle"), ((org.xmlpull.v1.XmlPullParser)a1).getAttributeValue((String)null, "name"));
                    this.mCards.add(a2);
                }
                i = ((org.xmlpull.v1.XmlPullParser)a1).next();
            }
        } catch(org.xmlpull.v1.XmlPullParserException a3) {
            android.util.Log.e("BTPbapVcardList", "XML parser error when parsing XML", (Throwable)a3);
        }
    }
    
    public java.util.ArrayList getList() {
        return this.mCards;
    }
}
