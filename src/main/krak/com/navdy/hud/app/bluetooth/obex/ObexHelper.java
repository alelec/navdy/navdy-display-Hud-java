package com.navdy.hud.app.bluetooth.obex;

final public class ObexHelper {
    final public static int BASE_PACKET_LENGTH = 3;
    final public static int LOWER_LIMIT_MAX_PACKET_SIZE = 255;
    final public static int MAX_CLIENT_PACKET_SIZE = 64512;
    final public static int MAX_PACKET_SIZE_INT = 65534;
    final public static int OBEX_AUTH_REALM_CHARSET_ASCII = 0;
    final public static int OBEX_AUTH_REALM_CHARSET_ISO_8859_1 = 1;
    final public static int OBEX_AUTH_REALM_CHARSET_ISO_8859_2 = 2;
    final public static int OBEX_AUTH_REALM_CHARSET_ISO_8859_3 = 3;
    final public static int OBEX_AUTH_REALM_CHARSET_ISO_8859_4 = 4;
    final public static int OBEX_AUTH_REALM_CHARSET_ISO_8859_5 = 5;
    final public static int OBEX_AUTH_REALM_CHARSET_ISO_8859_6 = 6;
    final public static int OBEX_AUTH_REALM_CHARSET_ISO_8859_7 = 7;
    final public static int OBEX_AUTH_REALM_CHARSET_ISO_8859_8 = 8;
    final public static int OBEX_AUTH_REALM_CHARSET_ISO_8859_9 = 9;
    final public static int OBEX_AUTH_REALM_CHARSET_UNICODE = 255;
    final public static int OBEX_OPCODE_ABORT = 255;
    final public static int OBEX_OPCODE_CONNECT = 128;
    final public static int OBEX_OPCODE_DISCONNECT = 129;
    final public static int OBEX_OPCODE_FINAL_BIT_MASK = 128;
    final public static int OBEX_OPCODE_GET = 3;
    final public static int OBEX_OPCODE_GET_FINAL = 131;
    final public static int OBEX_OPCODE_PUT = 2;
    final public static int OBEX_OPCODE_PUT_FINAL = 130;
    final public static int OBEX_OPCODE_RESERVED = 4;
    final public static int OBEX_OPCODE_RESERVED_FINAL = 132;
    final public static int OBEX_OPCODE_SETPATH = 133;
    final public static byte OBEX_SRMP_WAIT = (byte)1;
    final public static byte OBEX_SRM_DISABLE = (byte)0;
    final public static byte OBEX_SRM_ENABLE = (byte)1;
    final public static byte OBEX_SRM_SUPPORT = (byte)2;
    final private static String TAG = "ObexHelper";
    final public static boolean VDBG = false;
    
    private ObexHelper() {
    }
    
    public static byte[] computeAuthenticationChallenge(byte[] a, String s, boolean b, boolean b0) {
        byte[] a0 = null;
        if (a.length != 16) {
            throw new IllegalArgumentException("Nonce must be 16 bytes long");
        }
        if (s != null) {
            if (s.length() >= 255) {
                throw new IllegalArgumentException("Realm must be less then 255 bytes");
            }
            a0 = new byte[s.length() + 24];
            a0[21] = (byte)2;
            int i = (byte)(s.length() + 1);
            int i0 = (byte)i;
            a0[22] = (byte)i0;
            a0[23] = (byte)1;
            System.arraycopy(s.getBytes("ISO8859_1"), 0, a0, 24, s.length());
        } else {
            a0 = new byte[21];
        }
        a0[0] = (byte)0;
        a0[1] = (byte)16;
        System.arraycopy(a, 0, a0, 2, 16);
        a0[18] = (byte)1;
        a0[19] = (byte)1;
        a0[20] = (byte)0;
        if (!b) {
            int i1 = a0[20];
            int i2 = (byte)(i1 | 2);
            int i3 = (byte)i2;
            a0[20] = (byte)i3;
        }
        if (b0) {
            int i4 = a0[20];
            int i5 = (byte)(i4 | 1);
            int i6 = (byte)i5;
            a0[20] = (byte)i6;
        }
        return a0;
    }
    
    public static byte[] computeMd5Hash(byte[] a) {
        try {
            return java.security.MessageDigest.getInstance("MD5").digest(a);
        } catch(java.security.NoSuchAlgorithmException a0) {
            throw new RuntimeException((Throwable)a0);
        }
    }
    
    public static byte[] convertToByteArray(long j) {
        byte[] a = new byte[4];
        int i = (byte)(int)(j >> 24 & 255L);
        a[0] = (byte)i;
        int i0 = (byte)(int)(j >> 16 & 255L);
        a[1] = (byte)i0;
        int i1 = (byte)(int)(j >> 8 & 255L);
        a[2] = (byte)i1;
        int i2 = (byte)(int)(255L & j);
        a[3] = (byte)i2;
        return a;
    }
    
    public static long convertToLong(byte[] a) {
        int i = a.length - 1;
        long j = 0L;
        long j0 = 0L;
        while(i >= 0) {
            int i0 = a[i];
            long j1 = (long)i0;
            if (j1 < 0L) {
                j1 = j1 + 256L;
            }
            j = j | j1 << (int)j0;
            j0 = j0 + 8L;
            i = i + -1;
        }
        return j;
    }
    
    public static String convertToUnicode(byte[] a, boolean b) {
        String s = null;
        label4: {
            label2: {
                label3: {
                    if (a == null) {
                        break label3;
                    }
                    if (a.length != 0) {
                        break label2;
                    }
                }
                s = null;
                break label4;
            }
            int i = a.length;
            if (i % 2 != 0) {
                throw new IllegalArgumentException("Byte array not of a valid form");
            }
            int i0 = i >> 1;
            if (b) {
                i0 = i0 - 1;
            }
            char[] a0 = new char[i0];
            int i1 = 0;
            while(true) {
                if (i1 >= i0) {
                    s = new String(a0);
                    break;
                } else {
                    int i2 = a[i1 * 2];
                    int i3 = a[i1 * 2 + 1];
                    if (i2 < 0) {
                        i2 = i2 + 256;
                    }
                    if (i3 < 0) {
                        i3 = i3 + 256;
                    }
                    label0: {
                        label1: {
                            if (i2 != 0) {
                                break label1;
                            }
                            if (i3 == 0) {
                                break label0;
                            }
                        }
                        int i4 = (char)(i2 << 8 | i3);
                        int i5 = (char)i4;
                        a0[i1] = (char)i5;
                        i1 = i1 + 1;
                        continue;
                    }
                    s = new String(a0, 0, i1);
                    break;
                }
            }
        }
        return s;
    }
    
    public static byte[] convertToUnicodeByteArray(String s) {
        byte[] a = null;
        if (s != null) {
            char[] a0 = s.toCharArray();
            a = new byte[a0.length * 2 + 2];
            int i = 0;
            while(i < a0.length) {
                int i0 = i * 2;
                int i1 = a0[i];
                int i2 = (byte)(i1 >> 8);
                int i3 = (byte)i2;
                a[i0] = (byte)i3;
                int i4 = i * 2 + 1;
                int i5 = (byte)(boolean)a0[i];
                int i6 = (byte)i5;
                a[i4] = (byte)i6;
                i = i + 1;
            }
            a[a.length - 2] = (byte)0;
            a[a.length - 1] = (byte)0;
        } else {
            a = null;
        }
        return a;
    }
    
    public static byte[] createHeader(com.navdy.hud.app.bluetooth.obex.HeaderSet a, boolean b) {
        Throwable a0 = null;
        byte[] a1 = new byte[2];
        java.io.ByteArrayOutputStream a2 = new java.io.ByteArrayOutputStream();
        label1: {
            byte[] a3 = null;
            label0: {
                label2: {
                    label3: try {
                        byte[] a4 = a.mConnectionID;
                        {
                            {
                                java.io.UnsupportedEncodingException a5 = null;
                                label4: {
                                    try {
                                        if (a4 != null && a.getHeader(70) == null) {
                                            a2.write(-53);
                                            a2.write(a.mConnectionID);
                                        }
                                        Long a6 = (Long)a.getHeader(192);
                                        if (a6 != null) {
                                            a2.write(-64);
                                            a2.write(com.navdy.hud.app.bluetooth.obex.ObexHelper.convertToByteArray(a6.longValue()));
                                            if (b) {
                                                a.setHeader(192, null);
                                            }
                                        }
                                        String s = (String)a.getHeader(1);
                                        if (s == null) {
                                            if (a.getEmptyNameHeader()) {
                                                a2.write(1);
                                                a1[0] = (byte)0;
                                                a1[1] = (byte)3;
                                                a2.write(a1);
                                            }
                                        } else {
                                            a2.write(1);
                                            byte[] a7 = com.navdy.hud.app.bluetooth.obex.ObexHelper.convertToUnicodeByteArray(s);
                                            int i = a7.length + 3;
                                            int i0 = (byte)(i >> 8 & 255);
                                            int i1 = (byte)i0;
                                            a1[0] = (byte)i1;
                                            int i2 = (byte)(i & 255);
                                            int i3 = (byte)i2;
                                            a1[1] = (byte)i3;
                                            a2.write(a1);
                                            a2.write(a7);
                                            if (b) {
                                                a.setHeader(1, null);
                                            }
                                        }
                                        String s0 = (String)a.getHeader(66);
                                        label5: {
                                            java.io.UnsupportedEncodingException a8 = null;
                                            if (s0 == null) {
                                                break label5;
                                            }
                                            a2.write(66);
                                            label6: {
                                                byte[] a9 = null;
                                                try {
                                                    a9 = s0.getBytes("ISO8859_1");
                                                } catch(java.io.UnsupportedEncodingException a10) {
                                                    a8 = a10;
                                                    break label6;
                                                }
                                                int i4 = a9.length + 4;
                                                int i5 = (byte)(i4 >> 8 & 255);
                                                int i6 = (byte)i5;
                                                a1[0] = (byte)i6;
                                                int i7 = (byte)(i4 & 255);
                                                int i8 = (byte)i7;
                                                a1[1] = (byte)i8;
                                                a2.write(a1);
                                                a2.write(a9);
                                                a2.write(0);
                                                if (!b) {
                                                    break label5;
                                                }
                                                a.setHeader(66, null);
                                                break label5;
                                            }
                                            throw a8;
                                        }
                                        Long a11 = (Long)a.getHeader(195);
                                        if (a11 != null) {
                                            a2.write(-61);
                                            a2.write(com.navdy.hud.app.bluetooth.obex.ObexHelper.convertToByteArray(a11.longValue()));
                                            if (b) {
                                                a.setHeader(195, null);
                                            }
                                        }
                                        java.util.Calendar a12 = (java.util.Calendar)a.getHeader(68);
                                        if (a12 != null) {
                                            StringBuffer a13 = new StringBuffer();
                                            try {
                                                try {
                                                    byte[] a14 = null;
                                                    int i9 = a12.get(1);
                                                    int i10 = i9;
                                                    while(i10 < 1000) {
                                                        a13.append("0");
                                                        i10 = i10 * 10;
                                                    }
                                                    a13.append(i9);
                                                    int i11 = a12.get(2);
                                                    if (i11 < 10) {
                                                        a13.append("0");
                                                    }
                                                    a13.append(i11);
                                                    int i12 = a12.get(5);
                                                    if (i12 < 10) {
                                                        a13.append("0");
                                                    }
                                                    a13.append(i12);
                                                    a13.append("T");
                                                    int i13 = a12.get(11);
                                                    if (i13 < 10) {
                                                        a13.append("0");
                                                    }
                                                    a13.append(i13);
                                                    int i14 = a12.get(12);
                                                    if (i14 < 10) {
                                                        a13.append("0");
                                                    }
                                                    a13.append(i14);
                                                    int i15 = a12.get(13);
                                                    if (i15 < 10) {
                                                        a13.append("0");
                                                    }
                                                    a13.append(i15);
                                                    if (a12.getTimeZone().getID().equals("UTC")) {
                                                        a13.append("Z");
                                                    }
                                                    try {
                                                        a14 = a13.toString().getBytes("ISO8859_1");
                                                    } catch(java.io.UnsupportedEncodingException a15) {
                                                        a5 = a15;
                                                        break label4;
                                                    }
                                                    int i16 = a14.length + 3;
                                                    int i17 = (byte)(i16 >> 8 & 255);
                                                    int i18 = (byte)i17;
                                                    a1[0] = (byte)i18;
                                                    int i19 = (byte)(i16 & 255);
                                                    int i20 = (byte)i19;
                                                    a1[1] = (byte)i20;
                                                    a2.write(68);
                                                    a2.write(a1);
                                                    a2.write(a14);
                                                    if (b) {
                                                        a.setHeader(68, null);
                                                    }
                                                } catch(java.io.IOException ignoredException) {
                                                    break label3;
                                                }
                                            } catch(Throwable a16) {
                                                a0 = a16;
                                                break label1;
                                            }
                                        }
                                        java.util.Calendar a17 = (java.util.Calendar)a.getHeader(196);
                                        if (a17 != null) {
                                            a2.write(196);
                                            a2.write(com.navdy.hud.app.bluetooth.obex.ObexHelper.convertToByteArray(a17.getTime().getTime() / 1000L));
                                            if (b) {
                                                a.setHeader(196, null);
                                            }
                                        }
                                        String s1 = (String)a.getHeader(5);
                                        if (s1 != null) {
                                            a2.write(5);
                                            byte[] a18 = com.navdy.hud.app.bluetooth.obex.ObexHelper.convertToUnicodeByteArray(s1);
                                            int i21 = a18.length + 3;
                                            int i22 = (byte)(i21 >> 8 & 255);
                                            int i23 = (byte)i22;
                                            a1[0] = (byte)i23;
                                            int i24 = (byte)(i21 & 255);
                                            int i25 = (byte)i24;
                                            a1[1] = (byte)i25;
                                            a2.write(a1);
                                            a2.write(a18);
                                            if (b) {
                                                a.setHeader(5, null);
                                            }
                                        }
                                        byte[] a19 = (byte[])a.getHeader(70);
                                        if (a19 != null) {
                                            a2.write(70);
                                            int i26 = a19.length + 3;
                                            int i27 = (byte)(i26 >> 8 & 255);
                                            int i28 = (byte)i27;
                                            a1[0] = (byte)i28;
                                            int i29 = (byte)(i26 & 255);
                                            int i30 = (byte)i29;
                                            a1[1] = (byte)i30;
                                            a2.write(a1);
                                            a2.write(a19);
                                            if (b) {
                                                a.setHeader(70, null);
                                            }
                                        }
                                        byte[] a20 = (byte[])a.getHeader(71);
                                        if (a20 != null) {
                                            a2.write(71);
                                            int i31 = a20.length + 3;
                                            int i32 = (byte)(i31 >> 8 & 255);
                                            int i33 = (byte)i32;
                                            a1[0] = (byte)i33;
                                            int i34 = (byte)(i31 & 255);
                                            int i35 = (byte)i34;
                                            a1[1] = (byte)i35;
                                            a2.write(a1);
                                            a2.write(a20);
                                            if (b) {
                                                a.setHeader(71, null);
                                            }
                                        }
                                        byte[] a21 = (byte[])a.getHeader(74);
                                        if (a21 != null) {
                                            a2.write(74);
                                            int i36 = a21.length + 3;
                                            int i37 = (byte)(i36 >> 8 & 255);
                                            int i38 = (byte)i37;
                                            a1[0] = (byte)i38;
                                            int i39 = (byte)(i36 & 255);
                                            int i40 = (byte)i39;
                                            a1[1] = (byte)i40;
                                            a2.write(a1);
                                            a2.write(a21);
                                            if (b) {
                                                a.setHeader(74, null);
                                            }
                                        }
                                        byte[] a22 = (byte[])a.getHeader(76);
                                        if (a22 != null) {
                                            a2.write(76);
                                            int i41 = a22.length + 3;
                                            int i42 = (byte)(i41 >> 8 & 255);
                                            int i43 = (byte)i42;
                                            a1[0] = (byte)i43;
                                            int i44 = (byte)(i41 & 255);
                                            int i45 = (byte)i44;
                                            a1[1] = (byte)i45;
                                            a2.write(a1);
                                            a2.write(a22);
                                            if (b) {
                                                a.setHeader(76, null);
                                            }
                                        }
                                        byte[] a23 = (byte[])a.getHeader(79);
                                        if (a23 != null) {
                                            a2.write(79);
                                            int i46 = a23.length + 3;
                                            int i47 = (byte)(i46 >> 8 & 255);
                                            int i48 = (byte)i47;
                                            a1[0] = (byte)i48;
                                            int i49 = (byte)(i46 & 255);
                                            int i50 = (byte)i49;
                                            a1[1] = (byte)i50;
                                            a2.write(a1);
                                            a2.write(a23);
                                            if (b) {
                                                a.setHeader(79, null);
                                            }
                                        }
                                        int i51 = 0;
                                        while(i51 < 16) {
                                            String s2 = (String)a.getHeader(i51 + 48);
                                            if (s2 != null) {
                                                int i52 = (byte)i51;
                                                a2.write(i52 + 48);
                                                byte[] a24 = com.navdy.hud.app.bluetooth.obex.ObexHelper.convertToUnicodeByteArray(s2);
                                                int i53 = a24.length + 3;
                                                int i54 = (byte)(i53 >> 8 & 255);
                                                int i55 = (byte)i54;
                                                a1[0] = (byte)i55;
                                                int i56 = (byte)(i53 & 255);
                                                int i57 = (byte)i56;
                                                a1[1] = (byte)i57;
                                                a2.write(a1);
                                                a2.write(a24);
                                                if (b) {
                                                    a.setHeader(i51 + 48, null);
                                                }
                                            }
                                            byte[] a25 = (byte[])a.getHeader(i51 + 112);
                                            if (a25 != null) {
                                                int i58 = (byte)i51;
                                                a2.write(i58 + 112);
                                                int i59 = a25.length + 3;
                                                int i60 = (byte)(i59 >> 8 & 255);
                                                int i61 = (byte)i60;
                                                a1[0] = (byte)i61;
                                                int i62 = (byte)(i59 & 255);
                                                int i63 = (byte)i62;
                                                a1[1] = (byte)i63;
                                                a2.write(a1);
                                                a2.write(a25);
                                                if (b) {
                                                    a.setHeader(i51 + 112, null);
                                                }
                                            }
                                            Byte a26 = (Byte)a.getHeader(i51 + 176);
                                            if (a26 != null) {
                                                int i64 = (byte)i51;
                                                a2.write(i64 + 176);
                                                int i65 = a26.byteValue();
                                                a2.write(i65);
                                                if (b) {
                                                    a.setHeader(i51 + 176, null);
                                                }
                                            }
                                            Long a27 = (Long)a.getHeader(i51 + 240);
                                            if (a27 != null) {
                                                int i66 = (byte)i51;
                                                a2.write(i66 + 240);
                                                a2.write(com.navdy.hud.app.bluetooth.obex.ObexHelper.convertToByteArray(a27.longValue()));
                                                if (b) {
                                                    a.setHeader(i51 + 240, null);
                                                }
                                            }
                                            i51 = i51 + 1;
                                        }
                                        if (a.mAuthChall != null) {
                                            a2.write(77);
                                            int i67 = a.mAuthChall.length + 3;
                                            int i68 = (byte)(i67 >> 8 & 255);
                                            int i69 = (byte)i68;
                                            a1[0] = (byte)i69;
                                            int i70 = (byte)(i67 & 255);
                                            int i71 = (byte)i70;
                                            a1[1] = (byte)i71;
                                            a2.write(a1);
                                            a2.write(a.mAuthChall);
                                            if (b) {
                                                a.mAuthChall = null;
                                            }
                                        }
                                        if (a.mAuthResp != null) {
                                            a2.write(78);
                                            int i72 = a.mAuthResp.length + 3;
                                            int i73 = (byte)(i72 >> 8 & 255);
                                            int i74 = (byte)i73;
                                            a1[0] = (byte)i74;
                                            int i75 = (byte)(i72 & 255);
                                            int i76 = (byte)i75;
                                            a1[1] = (byte)i76;
                                            a2.write(a1);
                                            a2.write(a.mAuthResp);
                                            if (b) {
                                                a.mAuthResp = null;
                                            }
                                        }
                                        Byte a28 = (Byte)a.getHeader(151);
                                        if (a28 != null) {
                                            a2.write(-105);
                                            int i77 = a28.byteValue();
                                            a2.write(i77);
                                            if (b) {
                                                a.setHeader(151, null);
                                            }
                                        }
                                        Byte a29 = (Byte)a.getHeader(152);
                                        if (a29 == null) {
                                            break label2;
                                        }
                                        a2.write(-104);
                                        int i78 = a29.byteValue();
                                        a2.write(i78);
                                        if (!b) {
                                            break label2;
                                        }
                                        a.setHeader(152, null);
                                        break label2;
                                    } catch(java.io.IOException ignoredException0) {
                                    }
                                    break label3;
                                }
                                try {
                                    throw a5;
                                } catch(java.io.UnsupportedEncodingException ignoredException1) {
                                }
                            }
                            break label3;
                        }
                    } catch(Throwable a30) {
                        a0 = a30;
                        break label1;
                    }
                    a3 = a2.toByteArray();
                    try {
                        a2.close();
                        break label0;
                    } catch(Exception ignoredException2) {
                        break label0;
                    }
                }
                a3 = a2.toByteArray();
                try {
                    a2.close();
                } catch(Exception ignoredException3) {
                }
            }
            return a3;
        }
        a2.toByteArray();
        try {
            a2.close();
        } catch(Exception ignoredException4) {
        }
        throw a0;
    }
    
    public static int findHeaderEnd(byte[] a, int i, int i0) {
        int i1 = 0;
        int i2 = -1;
        int i3 = i;
        while(true) {
            if (i1 < i0 && i3 < a.length) {
                int i4 = 0;
                int i5 = a[i3];
                i2 = i1;
                if (i5 >= 0) {
                    i4 = a[i3];
                    i2 = i1;
                } else {
                    int i6 = a[i3];
                    i4 = i6 + 256;
                }
                int i7 = i4 & 192;
                i1 = i2;
                switch(i7) {
                    case 192: {
                        i3 = i3 + 5;
                        i1 = i2 + 5;
                        continue;
                    }
                    case 128: {
                        i3 = i3 + 1 + 1;
                        i1 = i2 + 2;
                        continue;
                    }
                    case 0: case 64: {
                        int i8 = 0;
                        int i9 = 0;
                        int i10 = i3 + 1;
                        int i11 = a[i10];
                        if (i11 >= 0) {
                            i8 = a[i10];
                        } else {
                            int i12 = a[i10];
                            i8 = i12 + 256;
                        }
                        int i13 = i10 + 1;
                        int i14 = a[i13];
                        if (i14 >= 0) {
                            i9 = a[i13];
                        } else {
                            int i15 = a[i13];
                            i9 = i15 + 256;
                        }
                        int i16 = (i8 << 8) + i9 - 3;
                        i3 = i13 + 1 + i16;
                        i1 = i2 + (i16 + 3);
                        continue;
                    }
                    default: {
                        continue;
                    }
                }
            }
            return (i2 != 0) ? i2 + i : (i1 >= i0) ? -1 : a.length;
        }
    }
    
    public static int findTag(byte a, byte[] a0) {
        int i = 0;
        int i0 = a;
        if (a0 != null) {
            i = 0;
            while(i < a0.length) {
                int i1 = a0[i];
                if (i1 == i0) {
                    break;
                }
                int i2 = a0[i + 1];
                i = i + ((i2 & 255) + 2);
            }
            if (i >= a0.length) {
                i = -1;
            }
        } else {
            i = -1;
        }
        return i;
    }
    
    public static int getMaxRxPacketSize(com.navdy.hud.app.bluetooth.obex.ObexTransport a) {
        return com.navdy.hud.app.bluetooth.obex.ObexHelper.validateMaxPacketSize(a.getMaxReceivePacketSize());
    }
    
    public static int getMaxTxPacketSize(com.navdy.hud.app.bluetooth.obex.ObexTransport a) {
        return com.navdy.hud.app.bluetooth.obex.ObexHelper.validateMaxPacketSize(a.getMaxTransmitPacketSize());
    }
    
    public static byte[] getTagValue(byte a, byte[] a0) {
        byte[] a1 = null;
        int i = com.navdy.hud.app.bluetooth.obex.ObexHelper.findTag(a, a0);
        if (i != -1) {
            int i0 = i + 1;
            int i1 = a0[i0];
            int i2 = i1 & 255;
            a1 = new byte[i2];
            System.arraycopy(a0, i0 + 1, a1, 0, i2);
        } else {
            a1 = null;
        }
        return a1;
    }
    
    public static byte[] updateHeaderSet(com.navdy.hud.app.bluetooth.obex.HeaderSet a, byte[] a0) {
        int i = 0;
        byte[] a1 = null;
        try {
            while(true) {
                java.io.UnsupportedEncodingException a2 = null;
                if (i >= a0.length) {
                    return a1;
                }
                int i0 = a0[i];
                int i1 = i0 & 255;
                int i2 = i1 & 192;
                label0: {
                    java.io.UnsupportedEncodingException a3 = null;
                    label1: {
                        Exception a4 = null;
                        switch(i2) {
                            case 192: {
                                int i3 = i + 1;
                                byte[] a5 = new byte[4];
                                System.arraycopy(a0, i3, a5, 0, 4);
                                try {
                                    if (i1 == 196) {
                                        java.util.Calendar a6 = java.util.Calendar.getInstance();
                                        a6.setTime(new java.util.Date(com.navdy.hud.app.bluetooth.obex.ObexHelper.convertToLong(a5) * 1000L));
                                        a.setHeader(196, a6);
                                    } else if (i1 != 203) {
                                        a.setHeader(i1, Long.valueOf(com.navdy.hud.app.bluetooth.obex.ObexHelper.convertToLong(a5)));
                                    } else {
                                        a.mConnectionID = new byte[4];
                                        System.arraycopy(a5, 0, a.mConnectionID, 0, 4);
                                    }
                                    i = i3 + 4;
                                    continue;
                                } catch(Exception a7) {
                                    a4 = a7;
                                    break;
                                }
                            }
                            case 128: {
                                int i4 = 0;
                                try {
                                    i4 = i + 1;
                                    int i5 = a0[i4];
                                    a.setHeader(i1, Byte.valueOf((byte)i5));
                                } catch(Exception ignoredException) {
                                }
                                i = i4 + 1;
                                continue;
                            }
                            case 0: case 64: {
                                boolean b = false;
                                int i6 = i + 1;
                                int i7 = a0[i6];
                                int i8 = i6 + 1;
                                int i9 = (i7 & 255) << 8;
                                int i10 = a0[i8];
                                int i11 = i9 + (i10 & 255) - 3;
                                int i12 = i8 + 1;
                                byte[] a8 = new byte[i11];
                                System.arraycopy(a0, i12, a8, 0, i11);
                                label2: {
                                    label3: {
                                        if (i11 == 0) {
                                            break label3;
                                        }
                                        if (i11 <= 0) {
                                            b = true;
                                            break label2;
                                        }
                                        int i13 = a8[i11 - 1];
                                        if (i13 == 0) {
                                            b = true;
                                            break label2;
                                        }
                                    }
                                    b = false;
                                }
                                switch(i1) {
                                    case 78: {
                                        a.mAuthResp = new byte[i11];
                                        System.arraycopy(a0, i12, a.mAuthResp, 0, i11);
                                        break;
                                    }
                                    case 77: {
                                        a.mAuthChall = new byte[i11];
                                        System.arraycopy(a0, i12, a.mAuthChall, 0, i11);
                                        break;
                                    }
                                    case 72: case 73: {
                                        a1 = new byte[i11 + 1];
                                        int i14 = (byte)i1;
                                        int i15 = (byte)i14;
                                        a1[0] = (byte)i15;
                                        System.arraycopy(a0, i12, a1, 1, i11);
                                        break;
                                    }
                                    case 68: {
                                        String s = null;
                                        java.util.Calendar a9 = null;
                                        int i16 = 0;
                                        try {
                                            s = new String(a8, "ISO8859_1");
                                            a9 = java.util.Calendar.getInstance();
                                            i16 = s.length();
                                        } catch(java.io.UnsupportedEncodingException a10) {
                                            a3 = a10;
                                            break label1;
                                        }
                                        try {
                                            if (i16 == 16) {
                                                int i17 = s.charAt(15);
                                                if (i17 == 90) {
                                                    a9.setTimeZone(java.util.TimeZone.getTimeZone("UTC"));
                                                }
                                            }
                                            a9.set(1, Integer.parseInt(s.substring(0, 4)));
                                            a9.set(2, Integer.parseInt(s.substring(4, 6)));
                                            a9.set(5, Integer.parseInt(s.substring(6, 8)));
                                            a9.set(11, Integer.parseInt(s.substring(9, 11)));
                                            a9.set(12, Integer.parseInt(s.substring(11, 13)));
                                            a9.set(13, Integer.parseInt(s.substring(13, 15)));
                                            a.setHeader(68, a9);
                                        } catch(java.io.UnsupportedEncodingException a11) {
                                            a3 = a11;
                                            break label1;
                                        }
                                        break;
                                    }
                                    case 66: {
                                        if (b) {
                                            try {
                                                a.setHeader(i1, new String(a8, 0, a8.length - 1, "ISO8859_1"));
                                            } catch(java.io.UnsupportedEncodingException a12) {
                                                a2 = a12;
                                                break label0;
                                            }
                                            break;
                                        } else {
                                            try {
                                                a.setHeader(i1, new String(a8, 0, a8.length, "ISO8859_1"));
                                            } catch(java.io.UnsupportedEncodingException a13) {
                                                a2 = a13;
                                                break label0;
                                            }
                                            break;
                                        }
                                    }
                                    default: {
                                        if ((i1 & 192) != 0) {
                                            a.setHeader(i1, a8);
                                        } else {
                                            a.setHeader(i1, com.navdy.hud.app.bluetooth.obex.ObexHelper.convertToUnicode(a8, true));
                                        }
                                    }
                                }
                                i = i12 + i11;
                                continue;
                            }
                            default: {
                                continue;
                            }
                        }
                        throw new java.io.IOException("Header was not formatted properly", (Throwable)a4);
                    }
                    throw a3;
                }
                throw a2;
            }
        } catch(java.io.IOException a14) {
            throw new java.io.IOException("Header was not formatted properly", (Throwable)a14);
        }
    }
    
    private static int validateMaxPacketSize(int i) {
        if (i == -1) {
            i = 65534;
        } else if (i < 255) {
            throw new IllegalArgumentException(new StringBuilder().append(i).append(" is less that the lower limit: ").append(255).toString());
        }
        return i;
    }
}
