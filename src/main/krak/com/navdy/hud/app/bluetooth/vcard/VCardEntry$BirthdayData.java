package com.navdy.hud.app.bluetooth.vcard;

public class VCardEntry$BirthdayData implements com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryElement {
    final private String mBirthday;
    
    public VCardEntry$BirthdayData(String s) {
        this.mBirthday = s;
    }
    
    static String access$1800(com.navdy.hud.app.bluetooth.vcard.VCardEntry$BirthdayData a) {
        return a.mBirthday;
    }
    
    public void constructInsertOperation(java.util.List a, int i) {
        android.content.ContentProviderOperation$Builder a0 = android.content.ContentProviderOperation.newInsert(android.provider.ContactsContract$Data.CONTENT_URI);
        a0.withValueBackReference("raw_contact_id", i);
        a0.withValue("mimetype", "vnd.android.cursor.item/contact_event");
        a0.withValue("data1", this.mBirthday);
        a0.withValue("data2", Integer.valueOf(3));
        a.add(a0.build());
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        if (this != a) {
            if (a instanceof com.navdy.hud.app.bluetooth.vcard.VCardEntry$BirthdayData) {
                com.navdy.hud.app.bluetooth.vcard.VCardEntry$BirthdayData a0 = (com.navdy.hud.app.bluetooth.vcard.VCardEntry$BirthdayData)a;
                b = android.text.TextUtils.equals((CharSequence)this.mBirthday, (CharSequence)a0.mBirthday);
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public String getBirthday() {
        return this.mBirthday;
    }
    
    public com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel getEntryLabel() {
        return com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel.BIRTHDAY;
    }
    
    public int hashCode() {
        return (this.mBirthday == null) ? 0 : this.mBirthday.hashCode();
    }
    
    public boolean isEmpty() {
        return android.text.TextUtils.isEmpty((CharSequence)this.mBirthday);
    }
    
    public String toString() {
        return new StringBuilder().append("birthday: ").append(this.mBirthday).toString();
    }
}
