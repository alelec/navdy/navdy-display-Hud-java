package com.navdy.hud.app.bluetooth.vcard;

public class VCardEntry$OrganizationData implements com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryElement {
    private String mDepartmentName;
    private boolean mIsPrimary;
    private String mOrganizationName;
    final private String mPhoneticName;
    private String mTitle;
    final private int mType;
    
    public VCardEntry$OrganizationData(String s, String s0, String s1, String s2, int i, boolean b) {
        this.mType = i;
        this.mOrganizationName = s;
        this.mDepartmentName = s0;
        this.mTitle = s1;
        this.mPhoneticName = s2;
        this.mIsPrimary = b;
    }
    
    static String access$100(com.navdy.hud.app.bluetooth.vcard.VCardEntry$OrganizationData a) {
        return a.mOrganizationName;
    }
    
    static String access$102(com.navdy.hud.app.bluetooth.vcard.VCardEntry$OrganizationData a, String s) {
        a.mOrganizationName = s;
        return s;
    }
    
    static String access$200(com.navdy.hud.app.bluetooth.vcard.VCardEntry$OrganizationData a) {
        return a.mDepartmentName;
    }
    
    static String access$202(com.navdy.hud.app.bluetooth.vcard.VCardEntry$OrganizationData a, String s) {
        a.mDepartmentName = s;
        return s;
    }
    
    static boolean access$302(com.navdy.hud.app.bluetooth.vcard.VCardEntry$OrganizationData a, boolean b) {
        a.mIsPrimary = b;
        return b;
    }
    
    static String access$400(com.navdy.hud.app.bluetooth.vcard.VCardEntry$OrganizationData a) {
        return a.mTitle;
    }
    
    static String access$402(com.navdy.hud.app.bluetooth.vcard.VCardEntry$OrganizationData a, String s) {
        a.mTitle = s;
        return s;
    }
    
    public void constructInsertOperation(java.util.List a, int i) {
        android.content.ContentProviderOperation$Builder a0 = android.content.ContentProviderOperation.newInsert(android.provider.ContactsContract$Data.CONTENT_URI);
        a0.withValueBackReference("raw_contact_id", i);
        a0.withValue("mimetype", "vnd.android.cursor.item/organization");
        a0.withValue("data2", Integer.valueOf(this.mType));
        if (this.mOrganizationName != null) {
            a0.withValue("data1", this.mOrganizationName);
        }
        if (this.mDepartmentName != null) {
            a0.withValue("data5", this.mDepartmentName);
        }
        if (this.mTitle != null) {
            a0.withValue("data4", this.mTitle);
        }
        if (this.mPhoneticName != null) {
            a0.withValue("data8", this.mPhoneticName);
        }
        if (this.mIsPrimary) {
            a0.withValue("is_primary", Integer.valueOf(1));
        }
        a.add(a0.build());
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (this != a) {
            if (a instanceof com.navdy.hud.app.bluetooth.vcard.VCardEntry$OrganizationData) {
                com.navdy.hud.app.bluetooth.vcard.VCardEntry$OrganizationData a0 = (com.navdy.hud.app.bluetooth.vcard.VCardEntry$OrganizationData)a;
                int i = this.mType;
                int i0 = a0.mType;
                label1: {
                    if (i != i0) {
                        break label1;
                    }
                    if (!android.text.TextUtils.equals((CharSequence)this.mOrganizationName, (CharSequence)a0.mOrganizationName)) {
                        break label1;
                    }
                    if (!android.text.TextUtils.equals((CharSequence)this.mDepartmentName, (CharSequence)a0.mDepartmentName)) {
                        break label1;
                    }
                    if (!android.text.TextUtils.equals((CharSequence)this.mTitle, (CharSequence)a0.mTitle)) {
                        break label1;
                    }
                    if (this.mIsPrimary == a0.mIsPrimary) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public String getDepartmentName() {
        return this.mDepartmentName;
    }
    
    final public com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel getEntryLabel() {
        return com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel.ORGANIZATION;
    }
    
    public String getFormattedString() {
        StringBuilder a = new StringBuilder();
        if (!android.text.TextUtils.isEmpty((CharSequence)this.mOrganizationName)) {
            a.append(this.mOrganizationName);
        }
        if (!android.text.TextUtils.isEmpty((CharSequence)this.mDepartmentName)) {
            if (a.length() > 0) {
                a.append(", ");
            }
            a.append(this.mDepartmentName);
        }
        if (!android.text.TextUtils.isEmpty((CharSequence)this.mTitle)) {
            if (a.length() > 0) {
                a.append(", ");
            }
            a.append(this.mTitle);
        }
        return a.toString();
    }
    
    public String getOrganizationName() {
        return this.mOrganizationName;
    }
    
    public String getPhoneticName() {
        return this.mPhoneticName;
    }
    
    public String getTitle() {
        return this.mTitle;
    }
    
    public int getType() {
        return this.mType;
    }
    
    public int hashCode() {
        return (((this.mType * 31 + ((this.mOrganizationName == null) ? 0 : this.mOrganizationName.hashCode())) * 31 + ((this.mDepartmentName == null) ? 0 : this.mDepartmentName.hashCode())) * 31 + ((this.mTitle == null) ? 0 : this.mTitle.hashCode())) * 31 + ((this.mIsPrimary) ? 1231 : 1237);
    }
    
    public boolean isEmpty() {
        boolean b = false;
        boolean b0 = android.text.TextUtils.isEmpty((CharSequence)this.mOrganizationName);
        label2: {
            label0: {
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)this.mDepartmentName)) {
                        break label1;
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)this.mTitle)) {
                        break label1;
                    }
                    if (android.text.TextUtils.isEmpty((CharSequence)this.mPhoneticName)) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    public boolean isPrimary() {
        return this.mIsPrimary;
    }
    
    public String toString() {
        Object[] a = new Object[5];
        a[0] = Integer.valueOf(this.mType);
        a[1] = this.mOrganizationName;
        a[2] = this.mDepartmentName;
        a[3] = this.mTitle;
        a[4] = Boolean.valueOf(this.mIsPrimary);
        return String.format("type: %d, organization: %s, department: %s, title: %s, isPrimary: %s", a);
    }
}
