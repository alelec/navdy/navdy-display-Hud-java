package com.navdy.hud.app.bluetooth.vcard.exception;

public class VCardAgentNotSupportedException extends com.navdy.hud.app.bluetooth.vcard.exception.VCardNotSupportedException {
    public VCardAgentNotSupportedException() {
    }
    
    public VCardAgentNotSupportedException(String s) {
        super(s);
    }
}
