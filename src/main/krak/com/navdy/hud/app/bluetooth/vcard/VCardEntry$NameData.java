package com.navdy.hud.app.bluetooth.vcard;

public class VCardEntry$NameData implements com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryElement {
    public String displayName;
    private String mFamily;
    private String mFormatted;
    private String mGiven;
    private String mMiddle;
    private String mPhoneticFamily;
    private String mPhoneticGiven;
    private String mPhoneticMiddle;
    private String mPrefix;
    private String mSortString;
    private String mSuffix;
    
    public VCardEntry$NameData() {
    }
    
    static String access$1000(com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData a) {
        return a.mMiddle;
    }
    
    static String access$1002(com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData a, String s) {
        a.mMiddle = s;
        return s;
    }
    
    static String access$1100(com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData a) {
        return a.mGiven;
    }
    
    static String access$1102(com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData a, String s) {
        a.mGiven = s;
        return s;
    }
    
    static String access$1200(com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData a) {
        return a.mFamily;
    }
    
    static String access$1202(com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData a, String s) {
        a.mFamily = s;
        return s;
    }
    
    static String access$1300(com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData a) {
        return a.mFormatted;
    }
    
    static String access$1302(com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData a, String s) {
        a.mFormatted = s;
        return s;
    }
    
    static String access$1402(com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData a, String s) {
        a.mSortString = s;
        return s;
    }
    
    static String access$500(com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData a) {
        return a.mPhoneticFamily;
    }
    
    static String access$502(com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData a, String s) {
        a.mPhoneticFamily = s;
        return s;
    }
    
    static String access$600(com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData a) {
        return a.mPhoneticMiddle;
    }
    
    static String access$602(com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData a, String s) {
        a.mPhoneticMiddle = s;
        return s;
    }
    
    static String access$700(com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData a) {
        return a.mPhoneticGiven;
    }
    
    static String access$702(com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData a, String s) {
        a.mPhoneticGiven = s;
        return s;
    }
    
    static String access$800(com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData a) {
        return a.mSuffix;
    }
    
    static String access$802(com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData a, String s) {
        a.mSuffix = s;
        return s;
    }
    
    static String access$900(com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData a) {
        return a.mPrefix;
    }
    
    static String access$902(com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData a, String s) {
        a.mPrefix = s;
        return s;
    }
    
    public void constructInsertOperation(java.util.List a, int i) {
        boolean b = false;
        android.content.ContentProviderOperation$Builder a0 = android.content.ContentProviderOperation.newInsert(android.provider.ContactsContract$Data.CONTENT_URI);
        a0.withValueBackReference("raw_contact_id", i);
        a0.withValue("mimetype", "vnd.android.cursor.item/name");
        if (!android.text.TextUtils.isEmpty((CharSequence)this.mGiven)) {
            a0.withValue("data2", this.mGiven);
        }
        if (!android.text.TextUtils.isEmpty((CharSequence)this.mFamily)) {
            a0.withValue("data3", this.mFamily);
        }
        if (!android.text.TextUtils.isEmpty((CharSequence)this.mMiddle)) {
            a0.withValue("data5", this.mMiddle);
        }
        if (!android.text.TextUtils.isEmpty((CharSequence)this.mPrefix)) {
            a0.withValue("data4", this.mPrefix);
        }
        if (!android.text.TextUtils.isEmpty((CharSequence)this.mSuffix)) {
            a0.withValue("data6", this.mSuffix);
        }
        if (android.text.TextUtils.isEmpty((CharSequence)this.mPhoneticGiven)) {
            b = false;
        } else {
            a0.withValue("data7", this.mPhoneticGiven);
            b = true;
        }
        if (!android.text.TextUtils.isEmpty((CharSequence)this.mPhoneticFamily)) {
            a0.withValue("data9", this.mPhoneticFamily);
            b = true;
        }
        if (!android.text.TextUtils.isEmpty((CharSequence)this.mPhoneticMiddle)) {
            a0.withValue("data8", this.mPhoneticMiddle);
            b = true;
        }
        if (!b) {
            a0.withValue("data7", this.mSortString);
        }
        a0.withValue("data1", this.displayName);
        a.add(a0.build());
    }
    
    public boolean emptyPhoneticStructuredName() {
        boolean b = false;
        boolean b0 = android.text.TextUtils.isEmpty((CharSequence)this.mPhoneticFamily);
        label2: {
            label0: {
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)this.mPhoneticGiven)) {
                        break label1;
                    }
                    if (android.text.TextUtils.isEmpty((CharSequence)this.mPhoneticMiddle)) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    public boolean emptyStructuredName() {
        boolean b = false;
        boolean b0 = android.text.TextUtils.isEmpty((CharSequence)this.mFamily);
        label2: {
            label0: {
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)this.mGiven)) {
                        break label1;
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)this.mMiddle)) {
                        break label1;
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)this.mPrefix)) {
                        break label1;
                    }
                    if (android.text.TextUtils.isEmpty((CharSequence)this.mSuffix)) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (this != a) {
            if (a instanceof com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData) {
                com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData a0 = (com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData)a;
                boolean b0 = android.text.TextUtils.equals((CharSequence)this.mFamily, (CharSequence)a0.mFamily);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!android.text.TextUtils.equals((CharSequence)this.mMiddle, (CharSequence)a0.mMiddle)) {
                        break label1;
                    }
                    if (!android.text.TextUtils.equals((CharSequence)this.mGiven, (CharSequence)a0.mGiven)) {
                        break label1;
                    }
                    if (!android.text.TextUtils.equals((CharSequence)this.mPrefix, (CharSequence)a0.mPrefix)) {
                        break label1;
                    }
                    if (!android.text.TextUtils.equals((CharSequence)this.mSuffix, (CharSequence)a0.mSuffix)) {
                        break label1;
                    }
                    if (!android.text.TextUtils.equals((CharSequence)this.mFormatted, (CharSequence)a0.mFormatted)) {
                        break label1;
                    }
                    if (!android.text.TextUtils.equals((CharSequence)this.mPhoneticFamily, (CharSequence)a0.mPhoneticFamily)) {
                        break label1;
                    }
                    if (!android.text.TextUtils.equals((CharSequence)this.mPhoneticMiddle, (CharSequence)a0.mPhoneticMiddle)) {
                        break label1;
                    }
                    if (!android.text.TextUtils.equals((CharSequence)this.mPhoneticGiven, (CharSequence)a0.mPhoneticGiven)) {
                        break label1;
                    }
                    if (android.text.TextUtils.equals((CharSequence)this.mSortString, (CharSequence)a0.mSortString)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    final public com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel getEntryLabel() {
        return com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel.NAME;
    }
    
    public String getFamily() {
        return this.mFamily;
    }
    
    public String getFormatted() {
        return this.mFormatted;
    }
    
    public String getGiven() {
        return this.mGiven;
    }
    
    public String getMiddle() {
        return this.mMiddle;
    }
    
    public String getPrefix() {
        return this.mPrefix;
    }
    
    public String getSortString() {
        return this.mSortString;
    }
    
    public String getSuffix() {
        return this.mSuffix;
    }
    
    public int hashCode() {
        String[] a = new String[10];
        a[0] = this.mFamily;
        a[1] = this.mMiddle;
        a[2] = this.mGiven;
        a[3] = this.mPrefix;
        a[4] = this.mSuffix;
        a[5] = this.mFormatted;
        a[6] = this.mPhoneticFamily;
        a[7] = this.mPhoneticMiddle;
        a[8] = this.mPhoneticGiven;
        a[9] = this.mSortString;
        int i = a.length;
        int i0 = 0;
        int i1 = 0;
        while(i1 < i) {
            String s = a[i1];
            i0 = i0 * 31 + ((s == null) ? 0 : s.hashCode());
            i1 = i1 + 1;
        }
        return i0;
    }
    
    public boolean isEmpty() {
        boolean b = false;
        boolean b0 = android.text.TextUtils.isEmpty((CharSequence)this.mFamily);
        label2: {
            label0: {
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)this.mMiddle)) {
                        break label1;
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)this.mGiven)) {
                        break label1;
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)this.mPrefix)) {
                        break label1;
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)this.mSuffix)) {
                        break label1;
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)this.mFormatted)) {
                        break label1;
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)this.mPhoneticFamily)) {
                        break label1;
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)this.mPhoneticMiddle)) {
                        break label1;
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)this.mPhoneticGiven)) {
                        break label1;
                    }
                    if (android.text.TextUtils.isEmpty((CharSequence)this.mSortString)) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    public void setFamily(String s) {
        this.mFamily = s;
    }
    
    public void setGiven(String s) {
        this.mGiven = s;
    }
    
    public void setMiddle(String s) {
        this.mMiddle = s;
    }
    
    public void setPrefix(String s) {
        this.mPrefix = s;
    }
    
    public void setSuffix(String s) {
        this.mSuffix = s;
    }
    
    public String toString() {
        Object[] a = new Object[5];
        a[0] = this.mFamily;
        a[1] = this.mGiven;
        a[2] = this.mMiddle;
        a[3] = this.mPrefix;
        a[4] = this.mSuffix;
        return String.format("family: %s, given: %s, middle: %s, prefix: %s, suffix: %s", a);
    }
}
