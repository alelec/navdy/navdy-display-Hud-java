package com.navdy.hud.app.bluetooth.pbap;

public class BluetoothPbapClient {
    final public static String CCH_PATH = "telecom/cch.vcf";
    final public static int EVENT_PULL_PHONE_BOOK_DONE = 2;
    final public static int EVENT_PULL_PHONE_BOOK_ERROR = 102;
    final public static int EVENT_PULL_PHONE_BOOK_SIZE_DONE = 5;
    final public static int EVENT_PULL_PHONE_BOOK_SIZE_ERROR = 105;
    final public static int EVENT_PULL_VCARD_ENTRY_DONE = 4;
    final public static int EVENT_PULL_VCARD_ENTRY_ERROR = 104;
    final public static int EVENT_PULL_VCARD_LISTING_DONE = 3;
    final public static int EVENT_PULL_VCARD_LISTING_ERROR = 103;
    final public static int EVENT_PULL_VCARD_LISTING_SIZE_DONE = 6;
    final public static int EVENT_PULL_VCARD_LISTING_SIZE_ERROR = 106;
    final public static int EVENT_SESSION_AUTH_REQUESTED = 203;
    final public static int EVENT_SESSION_AUTH_TIMEOUT = 204;
    final public static int EVENT_SESSION_CONNECTED = 201;
    final public static int EVENT_SESSION_DISCONNECTED = 202;
    final public static int EVENT_SET_PHONE_BOOK_DONE = 1;
    final public static int EVENT_SET_PHONE_BOOK_ERROR = 101;
    final public static String ICH_PATH = "telecom/ich.vcf";
    final public static short MAX_LIST_COUNT = (short)-1;
    final public static String MCH_PATH = "telecom/mch.vcf";
    final public static String OCH_PATH = "telecom/och.vcf";
    final public static byte ORDER_BY_ALPHABETICAL = (byte)1;
    final public static byte ORDER_BY_DEFAULT = (byte)-1;
    final public static byte ORDER_BY_INDEXED = (byte)0;
    final public static byte ORDER_BY_PHONETIC = (byte)2;
    final public static String PB_PATH = "telecom/pb.vcf";
    final public static byte SEARCH_ATTR_NAME = (byte)0;
    final public static byte SEARCH_ATTR_NUMBER = (byte)1;
    final public static byte SEARCH_ATTR_SOUND = (byte)2;
    final public static String SIM_CCH_PATH = "SIM1/telecom/cch.vcf";
    final public static String SIM_ICH_PATH = "SIM1/telecom/ich.vcf";
    final public static String SIM_MCH_PATH = "SIM1/telecom/mch.vcf";
    final public static String SIM_OCH_PATH = "SIM1/telecom/och.vcf";
    final public static String SIM_PB_PATH = "SIM1/telecom/pb.vcf";
    final private static String TAG;
    final public static long VCARD_ATTR_ADDR = 32L;
    final public static long VCARD_ATTR_AGENT = 32768L;
    final public static long VCARD_ATTR_BDAY = 16L;
    final public static long VCARD_ATTR_CATEGORIES = 16777216L;
    final public static long VCARD_ATTR_CLASS = 67108864L;
    final public static long VCARD_ATTR_EMAIL = 256L;
    final public static long VCARD_ATTR_FN = 2L;
    final public static long VCARD_ATTR_GEO = 2048L;
    final public static long VCARD_ATTR_KEY = 4194304L;
    final public static long VCARD_ATTR_LABEL = 64L;
    final public static long VCARD_ATTR_LOGO = 16384L;
    final public static long VCARD_ATTR_MAILER = 512L;
    final public static long VCARD_ATTR_N = 4L;
    final public static long VCARD_ATTR_NICKNAME = 8388608L;
    final public static long VCARD_ATTR_NOTE = 131072L;
    final public static long VCARD_ATTR_ORG = 65536L;
    final public static long VCARD_ATTR_PHOTO = 8L;
    final public static long VCARD_ATTR_PROID = 33554432L;
    final public static long VCARD_ATTR_REV = 262144L;
    final public static long VCARD_ATTR_ROLE = 8192L;
    final public static long VCARD_ATTR_SORT_STRING = 134217728L;
    final public static long VCARD_ATTR_SOUND = 524288L;
    final public static long VCARD_ATTR_TEL = 128L;
    final public static long VCARD_ATTR_TITLE = 4096L;
    final public static long VCARD_ATTR_TZ = 1024L;
    final public static long VCARD_ATTR_UID = 2097152L;
    final public static long VCARD_ATTR_URL = 1048576L;
    final public static long VCARD_ATTR_VERSION = 1L;
    final public static long VCARD_ATTR_X_IRMC_CALL_DATETIME = 268435456L;
    final public static byte VCARD_TYPE_21 = (byte)0;
    final public static byte VCARD_TYPE_30 = (byte)1;
    final private android.os.Handler mClientHandler;
    private com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient$ConnectionState mConnectionState;
    final private com.navdy.hud.app.bluetooth.pbap.BluetoothPbapSession mSession;
    private com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient$SessionHandler mSessionHandler;
    
    static {
        TAG = com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.class.getSimpleName();
    }
    
    public BluetoothPbapClient(android.bluetooth.BluetoothDevice a, android.os.Handler a0) {
        this.mConnectionState = com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient$ConnectionState.DISCONNECTED;
        if (a == null) {
            throw new NullPointerException("BluetothDevice is null");
        }
        this.mClientHandler = a0;
        this.mSessionHandler = new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient$SessionHandler(this);
        this.mSession = new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapSession(a, (android.os.Handler)this.mSessionHandler);
    }
    
    static String access$000() {
        return TAG;
    }
    
    static void access$100(com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient a, int i) {
        a.sendToClient(i);
    }
    
    static void access$200(com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient a, int i, int i0) {
        a.sendToClient(i, i0);
    }
    
    static void access$300(com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient a, int i, int i0, Object a0) {
        a.sendToClient(i, i0, a0);
    }
    
    static void access$400(com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient a, int i, Object a0) {
        a.sendToClient(i, a0);
    }
    
    static com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient$ConnectionState access$502(com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient a, com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient$ConnectionState a0) {
        a.mConnectionState = a0;
        return a0;
    }
    
    private void sendToClient(int i) {
        this.sendToClient(i, 0, null);
    }
    
    private void sendToClient(int i, int i0) {
        this.sendToClient(i, i0, null);
    }
    
    private void sendToClient(int i, int i0, Object a) {
        this.mClientHandler.obtainMessage(i, i0, 0, a).sendToTarget();
    }
    
    private void sendToClient(int i, Object a) {
        this.sendToClient(i, 0, a);
    }
    
    public void abort() {
        this.mSession.abort();
    }
    
    public void connect() {
        this.mSession.start();
    }
    
    public void disconnect() {
        this.mSession.stop();
    }
    
    public void finalize() {
        if (this.mSession != null) {
            this.mSession.stop();
        }
    }
    
    public com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient$ConnectionState getState() {
        return this.mConnectionState;
    }
    
    public boolean pullPhoneBook(String s) {
        return this.pullPhoneBook(s, 0L, (byte)0, 0, 0);
    }
    
    public boolean pullPhoneBook(String s, int i, int i0) {
        return this.pullPhoneBook(s, 0L, (byte)0, i, i0);
    }
    
    public boolean pullPhoneBook(String s, long j, byte a) {
        return this.pullPhoneBook(s, j, a, 0, 0);
    }
    
    public boolean pullPhoneBook(String s, long j, byte a, int i, int i0) {
        com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullPhoneBook a0 = new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullPhoneBook(s, j, a, i, i0);
        return this.mSession.makeRequest((com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequest)a0);
    }
    
    public boolean pullPhoneBookSize(String s) {
        com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullPhoneBookSize a = new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullPhoneBookSize(s);
        return this.mSession.makeRequest((com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequest)a);
    }
    
    public boolean pullVcardEntry(String s) {
        return this.pullVcardEntry(s, 0L, (byte)0);
    }
    
    public boolean pullVcardEntry(String s, long j, byte a) {
        com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullVcardEntry a0 = new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullVcardEntry(s, j, a);
        return this.mSession.makeRequest((com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequest)a0);
    }
    
    public boolean pullVcardListing(String s) {
        return this.pullVcardListing(s, (byte)(-1), (byte)0, (String)null, 0, 0);
    }
    
    public boolean pullVcardListing(String s, byte a) {
        return this.pullVcardListing(s, a, (byte)0, (String)null, 0, 0);
    }
    
    public boolean pullVcardListing(String s, byte a, byte a0, String s0, int i, int i0) {
        com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullVcardListing a1 = new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullVcardListing(s, a, a0, s0, i, i0);
        return this.mSession.makeRequest((com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequest)a1);
    }
    
    public boolean pullVcardListing(String s, byte a, int i, int i0) {
        return this.pullVcardListing(s, a, (byte)0, (String)null, i, i0);
    }
    
    public boolean pullVcardListing(String s, byte a, String s0) {
        return this.pullVcardListing(s, (byte)(-1), a, s0, 0, 0);
    }
    
    public boolean pullVcardListing(String s, int i, int i0) {
        return this.pullVcardListing(s, (byte)(-1), (byte)0, (String)null, i, i0);
    }
    
    public boolean pullVcardListingSize(String s) {
        com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullVcardListingSize a = new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullVcardListingSize(s);
        return this.mSession.makeRequest((com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequest)a);
    }
    
    public boolean setAuthResponse(String s) {
        android.util.Log.d(TAG, new StringBuilder().append(" setAuthResponse key=").append(s).toString());
        return this.mSession.setAuthResponse(s);
    }
    
    public boolean setPhoneBookFolderDown(String s) {
        com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestSetPath a = new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestSetPath(s);
        return this.mSession.makeRequest((com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequest)a);
    }
    
    public boolean setPhoneBookFolderRoot() {
        com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestSetPath a = new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestSetPath(false);
        return this.mSession.makeRequest((com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequest)a);
    }
    
    public boolean setPhoneBookFolderUp() {
        com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestSetPath a = new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestSetPath(true);
        return this.mSession.makeRequest((com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequest)a);
    }
}
