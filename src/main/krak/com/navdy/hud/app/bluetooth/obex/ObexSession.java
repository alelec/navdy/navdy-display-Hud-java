package com.navdy.hud.app.bluetooth.obex;

public class ObexSession {
    final private static String TAG = "ObexSession";
    final private static boolean V = false;
    protected com.navdy.hud.app.bluetooth.obex.Authenticator mAuthenticator;
    protected byte[] mChallengeDigest;
    
    public ObexSession() {
    }
    
    public boolean handleAuthChall(com.navdy.hud.app.bluetooth.obex.HeaderSet a) {
        boolean b = false;
        com.navdy.hud.app.bluetooth.obex.Authenticator a0 = this.mAuthenticator;
        label0: {
            label2: {
                label1: if (a0 != null) {
                    boolean b0 = false;
                    boolean b1 = false;
                    com.navdy.hud.app.bluetooth.obex.PasswordAuthentication a1 = null;
                    byte[] a2 = com.navdy.hud.app.bluetooth.obex.ObexHelper.getTagValue((byte)0, a.mAuthChall);
                    byte[] a3 = com.navdy.hud.app.bluetooth.obex.ObexHelper.getTagValue((byte)1, a.mAuthChall);
                    byte[] a4 = com.navdy.hud.app.bluetooth.obex.ObexHelper.getTagValue((byte)2, a.mAuthChall);
                    String s = null;
                    if (a4 != null) {
                        byte[] a5 = new byte[a4.length - 1];
                        System.arraycopy(a4, 1, a5, 0, a5.length);
                        int i = a4[0];
                        switch(i & 255) {
                            case 255: {
                                s = com.navdy.hud.app.bluetooth.obex.ObexHelper.convertToUnicode(a5, false);
                                break;
                            }
                            case 0: case 1: {
                                try {
                                    s = new String(a5, "ISO8859_1");
                                    break;
                                } catch(Exception ignoredException) {
                                    break label2;
                                }
                            }
                            default: {
                                throw new java.io.IOException("Unsupported Encoding Scheme");
                            }
                        }
                    }
                    if (a3 == null) {
                        b0 = false;
                        b1 = true;
                    } else {
                        int i0 = a3[0];
                        b0 = (i0 & 1) != 0;
                        int i1 = a3[0];
                        b1 = (i1 & 2) == 0;
                    }
                    a.mAuthChall = null;
                    try {
                        a1 = this.mAuthenticator.onAuthenticationChallenge(s, b0, b1);
                    } catch(Exception ignoredException0) {
                        break label1;
                    }
                    if (a1 != null) {
                        byte[] a6 = a1.getPassword();
                        if (a6 != null) {
                            byte[] a7 = a1.getUserName();
                            if (a7 == null) {
                                a.mAuthResp = new byte[36];
                            } else {
                                a.mAuthResp = new byte[a7.length + 38];
                                a.mAuthResp[36] = (byte)1;
                                byte[] a8 = a.mAuthResp;
                                int i2 = (byte)a7.length;
                                int i3 = (byte)i2;
                                a8[37] = (byte)i3;
                                System.arraycopy(a7, 0, a.mAuthResp, 38, a7.length);
                            }
                            byte[] a9 = new byte[a2.length + a6.length + 1];
                            System.arraycopy(a2, 0, a9, 0, a2.length);
                            a9[a2.length] = (byte)58;
                            System.arraycopy(a6, 0, a9, a2.length + 1, a6.length);
                            a.mAuthResp[0] = (byte)0;
                            a.mAuthResp[1] = (byte)16;
                            System.arraycopy(com.navdy.hud.app.bluetooth.obex.ObexHelper.computeMd5Hash(a9), 0, a.mAuthResp, 2, 16);
                            a.mAuthResp[18] = (byte)2;
                            a.mAuthResp[19] = (byte)16;
                            System.arraycopy(a2, 0, a.mAuthResp, 20, 16);
                            b = true;
                            break label0;
                        } else {
                            b = false;
                            break label0;
                        }
                    } else {
                        b = false;
                        break label0;
                    }
                } else {
                    b = false;
                    break label0;
                }
                b = false;
                break label0;
            }
            throw new java.io.IOException("Unsupported Encoding Scheme");
        }
        return b;
    }
    
    public boolean handleAuthResp(byte[] a) {
        boolean b = false;
        if (this.mAuthenticator != null) {
            byte[] a0 = this.mAuthenticator.onAuthenticationResponse(com.navdy.hud.app.bluetooth.obex.ObexHelper.getTagValue((byte)1, a));
            if (a0 == null) {
                b = false;
            } else {
                byte[] a1 = new byte[a0.length + 16];
                System.arraycopy(this.mChallengeDigest, 0, a1, 0, 16);
                System.arraycopy(a0, 0, a1, 16, a0.length);
                byte[] a2 = com.navdy.hud.app.bluetooth.obex.ObexHelper.computeMd5Hash(a1);
                byte[] a3 = com.navdy.hud.app.bluetooth.obex.ObexHelper.getTagValue((byte)0, a);
                int i = 0;
                while(true) {
                    if (i >= 16) {
                        b = true;
                        break;
                    } else {
                        int i0 = a2[i];
                        int i1 = a3[i];
                        if (i0 != i1) {
                            b = false;
                            break;
                        } else {
                            i = i + 1;
                        }
                    }
                }
            }
        } else {
            b = false;
        }
        return b;
    }
}
