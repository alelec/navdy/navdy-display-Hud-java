package com.navdy.hud.app.bluetooth.vcard;


public enum VCardEntry$EntryLabel {
    NAME(0),
    PHONE(1),
    EMAIL(2),
    POSTAL_ADDRESS(3),
    ORGANIZATION(4),
    IM(5),
    PHOTO(6),
    WEBSITE(7),
    SIP(8),
    NICKNAME(9),
    NOTE(10),
    BIRTHDAY(11),
    ANNIVERSARY(12),
    ANDROID_CUSTOM(13);

    private int value;
    VCardEntry$EntryLabel(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class VCardEntry$EntryLabel extends Enum {
//    final private static com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel[] $VALUES;
//    final public static com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel ANDROID_CUSTOM;
//    final public static com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel ANNIVERSARY;
//    final public static com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel BIRTHDAY;
//    final public static com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel EMAIL;
//    final public static com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel IM;
//    final public static com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel NAME;
//    final public static com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel NICKNAME;
//    final public static com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel NOTE;
//    final public static com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel ORGANIZATION;
//    final public static com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel PHONE;
//    final public static com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel PHOTO;
//    final public static com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel POSTAL_ADDRESS;
//    final public static com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel SIP;
//    final public static com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel WEBSITE;
//    
//    static {
//        NAME = new com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel("NAME", 0);
//        PHONE = new com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel("PHONE", 1);
//        EMAIL = new com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel("EMAIL", 2);
//        POSTAL_ADDRESS = new com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel("POSTAL_ADDRESS", 3);
//        ORGANIZATION = new com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel("ORGANIZATION", 4);
//        IM = new com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel("IM", 5);
//        PHOTO = new com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel("PHOTO", 6);
//        WEBSITE = new com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel("WEBSITE", 7);
//        SIP = new com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel("SIP", 8);
//        NICKNAME = new com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel("NICKNAME", 9);
//        NOTE = new com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel("NOTE", 10);
//        BIRTHDAY = new com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel("BIRTHDAY", 11);
//        ANNIVERSARY = new com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel("ANNIVERSARY", 12);
//        ANDROID_CUSTOM = new com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel("ANDROID_CUSTOM", 13);
//        com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel[] a = new com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel[14];
//        a[0] = NAME;
//        a[1] = PHONE;
//        a[2] = EMAIL;
//        a[3] = POSTAL_ADDRESS;
//        a[4] = ORGANIZATION;
//        a[5] = IM;
//        a[6] = PHOTO;
//        a[7] = WEBSITE;
//        a[8] = SIP;
//        a[9] = NICKNAME;
//        a[10] = NOTE;
//        a[11] = BIRTHDAY;
//        a[12] = ANNIVERSARY;
//        a[13] = ANDROID_CUSTOM;
//        $VALUES = a;
//    }
//    
//    private VCardEntry$EntryLabel(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel valueOf(String s) {
//        return (com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel)Enum.valueOf(com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel.class, s);
//    }
//    
//    public static com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel[] values() {
//        return $VALUES.clone();
//    }
//}
//