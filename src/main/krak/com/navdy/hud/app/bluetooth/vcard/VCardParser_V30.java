package com.navdy.hud.app.bluetooth.vcard;

public class VCardParser_V30 extends com.navdy.hud.app.bluetooth.vcard.VCardParser {
    final static java.util.Set sAcceptableEncoding;
    final static java.util.Set sKnownPropertyNameSet;
    final private com.navdy.hud.app.bluetooth.vcard.VCardParserImpl_V30 mVCardParserImpl;
    
    static {
        String[] a = new String[30];
        a[0] = "BEGIN";
        a[1] = "END";
        a[2] = "LOGO";
        a[3] = "PHOTO";
        a[4] = "LABEL";
        a[5] = "FN";
        a[6] = "TITLE";
        a[7] = "SOUND";
        a[8] = "VERSION";
        a[9] = "TEL";
        a[10] = "EMAIL";
        a[11] = "TZ";
        a[12] = "GEO";
        a[13] = "NOTE";
        a[14] = "URL";
        a[15] = "BDAY";
        a[16] = "ROLE";
        a[17] = "REV";
        a[18] = "UID";
        a[19] = "KEY";
        a[20] = "MAILER";
        a[21] = "NAME";
        a[22] = "PROFILE";
        a[23] = "SOURCE";
        a[24] = "NICKNAME";
        a[25] = "CLASS";
        a[26] = "SORT-STRING";
        a[27] = "CATEGORIES";
        a[28] = "PRODID";
        a[29] = "IMPP";
        sKnownPropertyNameSet = java.util.Collections.unmodifiableSet((java.util.Set)new java.util.HashSet((java.util.Collection)java.util.Arrays.asList((Object[])a)));
        String[] a0 = new String[4];
        a0[0] = "7BIT";
        a0[1] = "8BIT";
        a0[2] = "BASE64";
        a0[3] = "B";
        sAcceptableEncoding = java.util.Collections.unmodifiableSet((java.util.Set)new java.util.HashSet((java.util.Collection)java.util.Arrays.asList((Object[])a0)));
    }
    
    public VCardParser_V30() {
        this.mVCardParserImpl = new com.navdy.hud.app.bluetooth.vcard.VCardParserImpl_V30();
    }
    
    public VCardParser_V30(int i) {
        this.mVCardParserImpl = new com.navdy.hud.app.bluetooth.vcard.VCardParserImpl_V30(i);
    }
    
    public void addInterpreter(com.navdy.hud.app.bluetooth.vcard.VCardInterpreter a) {
        this.mVCardParserImpl.addInterpreter(a);
    }
    
    public void cancel() {
        this.mVCardParserImpl.cancel();
    }
    
    public void parse(java.io.InputStream a) {
        this.mVCardParserImpl.parse(a);
    }
    
    public void parseOne(java.io.InputStream a) {
        this.mVCardParserImpl.parseOne(a);
    }
}
