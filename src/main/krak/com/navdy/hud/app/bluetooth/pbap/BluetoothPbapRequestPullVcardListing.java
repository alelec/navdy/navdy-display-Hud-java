package com.navdy.hud.app.bluetooth.pbap;

final class BluetoothPbapRequestPullVcardListing extends com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequest {
    final private static String TAG = "BTPbapReqPullVcardL";
    final private static String TYPE = "x-bt/vcard-listing";
    private int mNewMissedCalls;
    private com.navdy.hud.app.bluetooth.pbap.BluetoothPbapVcardListing mResponse;
    
    public BluetoothPbapRequestPullVcardListing(String s, byte a, byte a0, String s0, int i, int i0) {
        this.mResponse = null;
        this.mNewMissedCalls = -1;
        int i1 = a;
        int i2 = a0;
        if (i >= 0 && i <= 65535) {
            if (i0 >= 0 && i0 <= 65535) {
                if (s == null) {
                    s = "";
                }
                this.mHeaderSet.setHeader(1, s);
                this.mHeaderSet.setHeader(66, "x-bt/vcard-listing");
                com.navdy.hud.app.bluetooth.pbap.utils.ObexAppParameters a1 = new com.navdy.hud.app.bluetooth.pbap.utils.ObexAppParameters();
                if (i1 >= 0) {
                    a1.add((byte)1, (byte)i1);
                }
                if (s0 != null) {
                    a1.add((byte)3, (byte)i2);
                    a1.add((byte)2, s0);
                }
                if (i > 0) {
                    int i3 = (short)i;
                    a1.add((byte)4, (short)i3);
                }
                if (i0 > 0) {
                    int i4 = (short)i0;
                    a1.add((byte)5, (short)i4);
                }
                a1.addToHeaderSet(this.mHeaderSet);
                return;
            }
            throw new IllegalArgumentException("listStartOffset should be [0..65535]");
        }
        throw new IllegalArgumentException("maxListCount should be [0..65535]");
    }
    
    public java.util.ArrayList getList() {
        return this.mResponse.getList();
    }
    
    public int getNewMissedCalls() {
        return this.mNewMissedCalls;
    }
    
    protected void readResponse(java.io.InputStream a) {
        android.util.Log.v("BTPbapReqPullVcardL", "readResponse");
        this.mResponse = new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapVcardListing(a);
    }
    
    protected void readResponseHeaders(com.navdy.hud.app.bluetooth.obex.HeaderSet a) {
        android.util.Log.v("BTPbapReqPullVcardL", "readResponseHeaders");
        com.navdy.hud.app.bluetooth.pbap.utils.ObexAppParameters a0 = com.navdy.hud.app.bluetooth.pbap.utils.ObexAppParameters.fromHeaderSet(a);
        if (a0.exists((byte)9)) {
            int i = a0.getByte((byte)9);
            this.mNewMissedCalls = i;
        }
    }
}
