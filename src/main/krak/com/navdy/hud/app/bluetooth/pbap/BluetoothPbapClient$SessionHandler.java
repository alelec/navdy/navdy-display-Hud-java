package com.navdy.hud.app.bluetooth.pbap;

class BluetoothPbapClient$SessionHandler extends android.os.Handler {
    final private java.lang.ref.WeakReference mClient;
    
    BluetoothPbapClient$SessionHandler(com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient a) {
        this.mClient = new java.lang.ref.WeakReference(a);
    }
    
    public void handleMessage(android.os.Message a) {
        android.util.Log.d(com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.access$000(), new StringBuilder().append("handleMessage: what=").append(a.what).toString());
        com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient a0 = (com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient)this.mClient.get();
        if (a0 != null) {
            switch(a.what) {
                case 9: {
                    com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.access$100(a0, 204);
                    break;
                }
                case 8: {
                    com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.access$100(a0, 203);
                    break;
                }
                case 7: {
                    com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.access$502(a0, com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient$ConnectionState.DISCONNECTED);
                    com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.access$100(a0, 202);
                    break;
                }
                case 6: {
                    com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.access$502(a0, com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient$ConnectionState.CONNECTED);
                    com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.access$100(a0, 201);
                    break;
                }
                case 5: {
                    com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.access$502(a0, com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient$ConnectionState.CONNECTING);
                    break;
                }
                case 4: {
                    com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequest a1 = (com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequest)a.obj;
                    if (a1 instanceof com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullPhoneBookSize) {
                        com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.access$100(a0, 105);
                        break;
                    } else if (a1 instanceof com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullVcardListingSize) {
                        com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.access$100(a0, 106);
                        break;
                    } else if (a1 instanceof com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullPhoneBook) {
                        com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.access$100(a0, 102);
                        break;
                    } else if (a1 instanceof com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullVcardListing) {
                        com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.access$100(a0, 103);
                        break;
                    } else if (a1 instanceof com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullVcardEntry) {
                        com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.access$100(a0, 104);
                        break;
                    } else {
                        if (!(a1 instanceof com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestSetPath)) {
                            break;
                        }
                        com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.access$100(a0, 101);
                        break;
                    }
                }
                case 3: {
                    com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequest a2 = (com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequest)a.obj;
                    if (a2 instanceof com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullPhoneBookSize) {
                        com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.access$200(a0, 5, ((com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullPhoneBookSize)a2).getSize());
                        break;
                    } else if (a2 instanceof com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullVcardListingSize) {
                        com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.access$200(a0, 6, ((com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullVcardListingSize)a2).getSize());
                        break;
                    } else if (a2 instanceof com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullPhoneBook) {
                        com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullPhoneBook a3 = (com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullPhoneBook)a2;
                        com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.access$300(a0, 2, a3.getNewMissedCalls(), a3.getList());
                        break;
                    } else if (a2 instanceof com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullVcardListing) {
                        com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullVcardListing a4 = (com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullVcardListing)a2;
                        com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.access$300(a0, 3, a4.getNewMissedCalls(), a4.getList());
                        break;
                    } else if (a2 instanceof com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullVcardEntry) {
                        com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.access$400(a0, 4, ((com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestPullVcardEntry)a2).getVcard());
                        break;
                    } else {
                        if (!(a2 instanceof com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestSetPath)) {
                            break;
                        }
                        com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient.access$100(a0, 1);
                        break;
                    }
                }
            }
        }
    }
}
