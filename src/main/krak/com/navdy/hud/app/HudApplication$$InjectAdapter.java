package com.navdy.hud.app;

final public class HudApplication$$InjectAdapter extends dagger.internal.Binding implements javax.inject.Provider, dagger.MembersInjector {
    private dagger.internal.Binding bus;
    
    public HudApplication$$InjectAdapter() {
        super("com.navdy.hud.app.HudApplication", "members/com.navdy.hud.app.HudApplication", false, com.navdy.hud.app.HudApplication.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.bus = a.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.HudApplication.class, (this).getClass().getClassLoader());
    }
    
    public com.navdy.hud.app.HudApplication get() {
        com.navdy.hud.app.HudApplication a = new com.navdy.hud.app.HudApplication();
        this.injectMembers(a);
        return a;
    }
    
    public Object get() {
        return this.get();
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.bus);
    }
    
    public void injectMembers(com.navdy.hud.app.HudApplication a) {
        a.bus = (com.squareup.otto.Bus)this.bus.get();
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.HudApplication)a);
    }
}
