package com.navdy.hud.app.analytics;

class AnalyticsSupport$DeferredWakeup {
    String batteryDrain;
    String batteryLevel;
    String maxBattery;
    public com.navdy.hud.app.analytics.AnalyticsSupport$WakeupReason reason;
    String sleepTime;
    
    AnalyticsSupport$DeferredWakeup(com.navdy.hud.app.analytics.AnalyticsSupport$WakeupReason a, String s, String s0, String s1, String s2) {
        this.reason = a;
        this.batteryLevel = s;
        this.batteryDrain = s0;
        this.sleepTime = s1;
        this.maxBattery = s2;
    }
}
