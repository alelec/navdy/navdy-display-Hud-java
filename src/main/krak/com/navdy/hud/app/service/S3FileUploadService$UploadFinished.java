package com.navdy.hud.app.service;

public class S3FileUploadService$UploadFinished {
    final public String filePath;
    final public boolean succeeded;
    final public String userTag;
    
    S3FileUploadService$UploadFinished(boolean b, String s, String s0) {
        this.succeeded = b;
        this.filePath = s;
        this.userTag = s0;
    }
}
