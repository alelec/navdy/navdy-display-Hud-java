package com.navdy.hud.app.service;

class FileTransferHandler$4 implements Runnable {
    final com.navdy.hud.app.service.FileTransferHandler this$0;
    final com.navdy.service.library.events.file.FileTransferStatus val$fileTransferStatus;
    
    FileTransferHandler$4(com.navdy.hud.app.service.FileTransferHandler a, com.navdy.service.library.events.file.FileTransferStatus a0) {
        super();
        this.this$0 = a;
        this.val$fileTransferStatus = a0;
    }
    
    public void run() {
        com.navdy.service.library.events.file.FileTransferStatus a = this.val$fileTransferStatus;
        label0: {
            Throwable a0 = null;
            if (a == null) {
                break label0;
            }
            boolean b = this.val$fileTransferStatus.success.booleanValue();
            label1: {
                label2: {
                    label3: {
                        if (!b) {
                            break label3;
                        }
                        if (this.val$fileTransferStatus.transferComplete.booleanValue()) {
                            break label2;
                        }
                    }
                    try {
                        if (this.this$0.remoteDevice.getLinkBandwidthLevel() > 0) {
                            if (this.this$0.fileTransferManager.handleFileTransferStatus(this.val$fileTransferStatus)) {
                                if (com.navdy.hud.app.service.FileTransferHandler.access$000(this.this$0, this.val$fileTransferStatus.transferId.intValue()) == null) {
                                    com.navdy.hud.app.service.FileTransferHandler.sLogger.e("Failed to get the data for the transfer session");
                                    break label0;
                                } else {
                                    com.navdy.hud.app.service.FileTransferHandler.sLogger.d("Sent chunk");
                                    break label0;
                                }
                            } else {
                                this.this$0.fileTransferManager.endFileTransferSession(this.val$fileTransferStatus.transferId.intValue(), true);
                                break label0;
                            }
                        } else {
                            com.navdy.hud.app.service.FileTransferHandler.sLogger.d("Link bandwidth is low, Not sending the next chunk. Ending the file transfer");
                            this.this$0.fileTransferManager.endFileTransferSession(this.val$fileTransferStatus.transferId.intValue(), true);
                            break label0;
                        }
                    } catch(Throwable a1) {
                        a0 = a1;
                        break label1;
                    }
                }
                com.navdy.hud.app.service.FileTransferHandler.sLogger.d(new StringBuilder().append("File Transfer complete ").append(this.val$fileTransferStatus.transferId).toString());
                com.navdy.service.library.events.file.FileType a2 = this.this$0.fileTransferManager.getFileType(this.val$fileTransferStatus.transferId.intValue());
                this.this$0.fileTransferManager.endFileTransferSession(this.val$fileTransferStatus.transferId.intValue(), true);
                if (a2 == null) {
                    com.navdy.hud.app.service.FileTransferHandler.sLogger.e(new StringBuilder().append("Cannot find FileType associated with transfer ID :").append(this.val$fileTransferStatus.transferId).toString());
                    break label0;
                } else {
                    this.this$0.fileTransferAuthority.onFileSent(a2);
                    break label0;
                }
            }
            com.navdy.hud.app.service.FileTransferHandler.sLogger.d("Exception while reading the data and sending it across", a0);
        }
    }
}
