package com.navdy.hud.app.service;


    public enum ShutdownMonitor$MonitorState {
        STATE_BOOT(0),
    STATE_OBD_ACTIVE(1),
    STATE_ACTIVE_USE(2),
    STATE_PENDING_SHUTDOWN(3),
    STATE_SHUTDOWN_PROMPT(4),
    STATE_QUIET_MODE(5);

        private int value;
        ShutdownMonitor$MonitorState(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class ShutdownMonitor$MonitorState extends Enum {
//    final private static com.navdy.hud.app.service.ShutdownMonitor$MonitorState[] $VALUES;
//    final public static com.navdy.hud.app.service.ShutdownMonitor$MonitorState STATE_ACTIVE_USE;
//    final public static com.navdy.hud.app.service.ShutdownMonitor$MonitorState STATE_BOOT;
//    final public static com.navdy.hud.app.service.ShutdownMonitor$MonitorState STATE_OBD_ACTIVE;
//    final public static com.navdy.hud.app.service.ShutdownMonitor$MonitorState STATE_PENDING_SHUTDOWN;
//    final public static com.navdy.hud.app.service.ShutdownMonitor$MonitorState STATE_QUIET_MODE;
//    final public static com.navdy.hud.app.service.ShutdownMonitor$MonitorState STATE_SHUTDOWN_PROMPT;
//    
//    static {
//        STATE_BOOT = new com.navdy.hud.app.service.ShutdownMonitor$MonitorState("STATE_BOOT", 0);
//        STATE_OBD_ACTIVE = new com.navdy.hud.app.service.ShutdownMonitor$MonitorState("STATE_OBD_ACTIVE", 1);
//        STATE_ACTIVE_USE = new com.navdy.hud.app.service.ShutdownMonitor$MonitorState("STATE_ACTIVE_USE", 2);
//        STATE_PENDING_SHUTDOWN = new com.navdy.hud.app.service.ShutdownMonitor$MonitorState("STATE_PENDING_SHUTDOWN", 3);
//        STATE_SHUTDOWN_PROMPT = new com.navdy.hud.app.service.ShutdownMonitor$MonitorState("STATE_SHUTDOWN_PROMPT", 4);
//        STATE_QUIET_MODE = new com.navdy.hud.app.service.ShutdownMonitor$MonitorState("STATE_QUIET_MODE", 5);
//        com.navdy.hud.app.service.ShutdownMonitor$MonitorState[] a = new com.navdy.hud.app.service.ShutdownMonitor$MonitorState[6];
//        a[0] = STATE_BOOT;
//        a[1] = STATE_OBD_ACTIVE;
//        a[2] = STATE_ACTIVE_USE;
//        a[3] = STATE_PENDING_SHUTDOWN;
//        a[4] = STATE_SHUTDOWN_PROMPT;
//        a[5] = STATE_QUIET_MODE;
//        $VALUES = a;
//    }
//    
//    private ShutdownMonitor$MonitorState(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.service.ShutdownMonitor$MonitorState valueOf(String s) {
//        return (com.navdy.hud.app.service.ShutdownMonitor$MonitorState)Enum.valueOf(com.navdy.hud.app.service.ShutdownMonitor$MonitorState.class, s);
//    }
//    
//    public static com.navdy.hud.app.service.ShutdownMonitor$MonitorState[] values() {
//        return $VALUES.clone();
//    }
//}
//