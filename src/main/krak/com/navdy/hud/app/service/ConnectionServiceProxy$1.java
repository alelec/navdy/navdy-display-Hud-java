package com.navdy.hud.app.service;

class ConnectionServiceProxy$1 implements Runnable {
    final com.navdy.hud.app.service.ConnectionServiceProxy this$0;
    final com.navdy.service.library.device.NavdyDeviceId val$deviceId;
    final com.navdy.service.library.events.NavdyEvent val$event;
    
    ConnectionServiceProxy$1(com.navdy.hud.app.service.ConnectionServiceProxy a, com.navdy.service.library.events.NavdyEvent a0, com.navdy.service.library.device.NavdyDeviceId a1) {
        super();
        this.this$0 = a;
        this.val$event = a0;
        this.val$deviceId = a1;
    }
    
    public void run() {
        com.navdy.hud.app.IEventSource a = this.this$0.mEventSource;
        label2: {
            Throwable a0 = null;
            label0: {
                int i = 0;
                android.os.TransactionTooLargeException a1 = null;
                label1: {
                    android.os.DeadObjectException a2 = null;
                    if (a == null) {
                        com.navdy.hud.app.service.ConnectionServiceProxy.access$000().e(new StringBuilder().append("Failed to send event - service connection broken").append(this.val$event).toString());
                        break label2;
                    } else {
                        try {
                            try {
                                try {
                                    i = -1;
                                    long j = android.os.SystemClock.elapsedRealtime();
                                    byte[] a3 = this.val$event.toByteArray();
                                    long j0 = android.os.SystemClock.elapsedRealtime();
                                    i = a3.length;
                                    if (com.navdy.hud.app.service.ConnectionServiceProxy.access$000().isLoggable(2)) {
                                        com.navdy.hud.app.service.ConnectionServiceProxy.access$000().v(new StringBuilder().append("NAVDY-PACKET [H2P-Outgoing-Event] ").append(this.val$event).toString());
                                    }
                                    long j1 = j0 - j;
                                    if (j1 >= 100L) {
                                        com.navdy.hud.app.service.ConnectionServiceProxy.access$000().v(new StringBuilder().append("NAVDY-PACKET [H2P-Outgoing-Event]").append(this.val$event.type.name()).append(" took: ").append(j1).append(" len:").append(i).toString());
                                    }
                                    this.this$0.mEventSource.postRemoteEvent(this.val$deviceId.toString(), a3);
                                    break label2;
                                } catch(android.os.DeadObjectException a4) {
                                    a2 = a4;
                                }
                            } catch(android.os.TransactionTooLargeException a5) {
                                a1 = a5;
                                break label1;
                            }
                        } catch(Throwable a6) {
                            a0 = a6;
                            break label0;
                        }
                    }
                    com.navdy.hud.app.service.ConnectionServiceProxy.access$000().e("Exception posting remote event, server died", (Throwable)a2);
                    break label2;
                }
                com.navdy.hud.app.service.ConnectionServiceProxy.access$000().e(new StringBuilder().append("Exception posting remote event, large payload type[").append(this.val$event.type).append("] size[").append(i).append("]").toString(), (Throwable)a1);
                break label2;
            }
            com.navdy.hud.app.service.ConnectionServiceProxy.access$000().e("Exception posting remote event", a0);
        }
    }
}
