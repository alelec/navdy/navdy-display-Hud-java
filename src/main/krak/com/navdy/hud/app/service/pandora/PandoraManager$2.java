package com.navdy.hud.app.service.pandora;

class PandoraManager$2 {
    final static int[] $SwitchMap$com$navdy$hud$app$service$pandora$messages$UpdateStatus$Value;
    final static int[] $SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState;
    
    static {
        $SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState = new int[com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState.values().length];
        int[] a = $SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState;
        com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState a0 = com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState.CONNECTION_VERIFIED;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState[com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState.CONNECTION_DISCONNECTED.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        $SwitchMap$com$navdy$hud$app$service$pandora$messages$UpdateStatus$Value = new int[com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value.values().length];
        int[] a1 = $SwitchMap$com$navdy$hud$app$service$pandora$messages$UpdateStatus$Value;
        com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value a2 = com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value.PLAYING;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$service$pandora$messages$UpdateStatus$Value[com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value.PAUSED.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException2) {
        }
    }
}
