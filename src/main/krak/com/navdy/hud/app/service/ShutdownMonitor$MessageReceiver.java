package com.navdy.hud.app.service;

class ShutdownMonitor$MessageReceiver extends Thread {
    final com.navdy.hud.app.service.ShutdownMonitor this$0;
    
    public ShutdownMonitor$MessageReceiver(com.navdy.hud.app.service.ShutdownMonitor a) {
        this.this$0 = a;
        super("ShutdownMonitor MessageReceiver thread");
    }
    
    public void run() {
        byte[] a = new byte[100];
        java.nio.charset.Charset a0 = java.nio.charset.Charset.forName("US-ASCII");
        int i = 0;
        String s = "";
        while(true) {
            int i0 = 0;
            Exception a1 = null;
            try {
                i0 = -1;
                java.io.InputStream a2 = com.navdy.hud.app.service.ShutdownMonitor.access$000(this.this$0);
                if (a2 == null) {
                    i0 = -1;
                } else {
                    i0 = -1;
                    i = 0;
                    i0 = a2.read(a);
                    if (i0 >= 0) {
                        i = 0;
                    } else {
                        i = 0;
                        com.navdy.hud.app.service.ShutdownMonitor.access$100().e("end of file reading shutdownd socket");
                        com.navdy.hud.app.service.ShutdownMonitor.access$200(this.this$0);
                        i = 0;
                    }
                }
            } catch(Exception a3) {
                com.navdy.hud.app.service.ShutdownMonitor.access$100().e("exception reading shutdownd socket", (Throwable)a3);
                com.navdy.hud.app.service.ShutdownMonitor.access$200(this.this$0);
            }
            label0: {
                if (i0 >= 0) {
                    try {
                        s = new StringBuilder().append(s).append(new String(a, 0, i0, a0)).toString();
                    } catch(Exception a4) {
                        a1 = a4;
                        break label0;
                    }
                    while(true) {
                        String[] a5 = null;
                        int i1 = 0;
                        try {
                            if (!s.contains((CharSequence)"\n")) {
                                break;
                            }
                            a5 = s.split("\n", 2);
                        } catch(Exception a6) {
                            a1 = a6;
                            break label0;
                        }
                        String s0 = a5[0];
                        try {
                            i1 = a5.length;
                        } catch(NullPointerException a7) {
                            a1 = a7;
                            break label0;
                        }
                        s = (i1 <= 1) ? "" : a5[1];
                        try {
                            com.navdy.hud.app.service.ShutdownMonitor.access$100().v(new StringBuilder().append("got command[").append(s0).append("]").toString());
                            if ("powerclick".equals(s0)) {
                                this.this$0.mInputManager.injectKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent.POWER_BUTTON_CLICK);
                            } else if ("powerdoubleclick".equals(s0)) {
                                this.this$0.mInputManager.injectKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent.POWER_BUTTON_DOUBLE_CLICK);
                            } else if ("powerlongpress".equals(s0)) {
                                this.this$0.mInputManager.injectKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent.POWER_BUTTON_LONG_PRESS);
                            } else if (s0.startsWith("powerstate")) {
                                String s1 = s0.substring("powerstate".length() + 1).trim();
                                com.navdy.hud.app.service.ShutdownMonitor.access$100().d(new StringBuilder().append("State : ").append(s1).toString());
                                int i2 = s1.charAt(0);
                                boolean b = i2 != 48;
                                int i3 = s1.charAt(1);
                                boolean b0 = i3 != 48;
                                com.navdy.hud.app.service.ShutdownMonitor.access$100().d(new StringBuilder().append("USB on : ").append(b).append(" , Main on : ").append(b0).toString());
                                label1: {
                                    label3: {
                                        label4: {
                                            if (b) {
                                                break label4;
                                            }
                                            if (!b0) {
                                                break label3;
                                            }
                                        }
                                        if (com.navdy.hud.app.service.ShutdownMonitor.access$300(this.this$0)) {
                                            break label1;
                                        }
                                        if (!com.navdy.hud.app.service.ShutdownMonitor.access$400(this.this$0) && com.navdy.hud.app.service.ShutdownMonitor.access$500(this.this$0) != null) {
                                            com.navdy.hud.app.service.ShutdownMonitor.access$700(this.this$0).removeCallbacks(com.navdy.hud.app.service.ShutdownMonitor.access$500(this.this$0));
                                            com.navdy.hud.app.service.ShutdownMonitor.access$502(this.this$0, (Runnable)null);
                                        }
                                        com.navdy.hud.app.service.ShutdownMonitor.access$800(this.this$0).updateListener();
                                        break label1;
                                    }
                                    boolean b1 = com.navdy.hud.app.service.ShutdownMonitor.access$300(this.this$0);
                                    label2: {
                                        if (b1) {
                                            break label2;
                                        }
                                        if (!com.navdy.hud.app.service.ShutdownMonitor.access$400(this.this$0)) {
                                            break label1;
                                        }
                                    }
                                    com.navdy.hud.app.service.ShutdownMonitor.access$502(this.this$0, (Runnable)new com.navdy.hud.app.service.ShutdownMonitor$MessageReceiver$1(this));
                                    com.navdy.hud.app.service.ShutdownMonitor.access$700(this.this$0).postDelayed(com.navdy.hud.app.service.ShutdownMonitor.access$500(this.this$0), com.navdy.hud.app.service.ShutdownMonitor.access$600());
                                }
                                com.navdy.hud.app.service.ShutdownMonitor.access$302(this.this$0, b0);
                                com.navdy.hud.app.service.ShutdownMonitor.access$402(this.this$0, b);
                            } else {
                                com.navdy.hud.app.service.ShutdownMonitor.access$100().e(new StringBuilder().append("invalid command from shutdownd socket: ").append(s0).toString());
                            }
                        } catch(Exception a8) {
                            a1 = a8;
                            break label0;
                        }
                    }
                } else {
                    String s2 = null;
                    i = i + 1;
                    if (i > 3) {
                        com.navdy.hud.app.service.ShutdownMonitor.access$100().e("MessageReceiver thread terminating");
                        return;
                    }
                    try {
                        long j = (long)(i * 10000);
                        s2 = s;
                        Thread.sleep(j);
                    } catch(InterruptedException ignoredException) {
                        s = s2;
                    }
                }
                continue;
            }
            com.navdy.hud.app.service.ShutdownMonitor.access$100().e("exception while processing data from shutdownd", (Throwable)a1);
            s = "";
        }
    }
}
