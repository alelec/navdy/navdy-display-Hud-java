package com.navdy.hud.app.service;

class ShutdownMonitor$2$1 implements Runnable {
    final com.navdy.hud.app.service.ShutdownMonitor$2 this$1;
    final android.location.Location val$copy;
    
    ShutdownMonitor$2$1(com.navdy.hud.app.service.ShutdownMonitor$2 a, android.location.Location a0) {
        super();
        this.this$1 = a;
        this.val$copy = a0;
    }
    
    public void run() {
        String s = this.val$copy.getProvider();
        if (s == null) {
            s = "unknown";
        }
        android.location.Location a = (android.location.Location)com.navdy.hud.app.service.ShutdownMonitor.access$2300(this.this$1.this$0).get(s);
        label0: if (a != null) {
            if (a.distanceTo(this.val$copy) > 60f) {
                com.navdy.hud.app.service.ShutdownMonitor.access$2300(this.this$1.this$0).put(s, this.val$copy);
                com.navdy.hud.app.service.ShutdownMonitor.access$2200(this.this$1.this$0);
                long j = com.navdy.hud.app.service.ShutdownMonitor.access$1700(this.this$1.this$0);
                long j0 = com.navdy.hud.app.service.ShutdownMonitor.access$2400(this.this$1.this$0);
                int i = (j0 < 0L) ? -1 : (j0 == 0L) ? 0 : 1;
                label1: {
                    if (i == 0) {
                        break label1;
                    }
                    if (j - com.navdy.hud.app.service.ShutdownMonitor.access$2400(this.this$1.this$0) < com.navdy.hud.app.service.ShutdownMonitor.access$2500()) {
                        break label0;
                    }
                }
                com.navdy.hud.app.service.ShutdownMonitor.access$2402(this.this$1.this$0, j);
                com.navdy.hud.app.service.ShutdownMonitor.access$100().i(new StringBuilder().append("Moved significant distance - based on ").append(s).append(" provider").toString());
            }
        } else {
            com.navdy.hud.app.service.ShutdownMonitor.access$2300(this.this$1.this$0).put(s, this.val$copy);
            com.navdy.hud.app.service.ShutdownMonitor.access$2200(this.this$1.this$0);
        }
    }
}
