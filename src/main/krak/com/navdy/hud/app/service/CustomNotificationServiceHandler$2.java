package com.navdy.hud.app.service;

class CustomNotificationServiceHandler$2 implements com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnNearestGasStationCallback {
    final com.navdy.hud.app.service.CustomNotificationServiceHandler this$0;
    final long val$t1;
    
    CustomNotificationServiceHandler$2(com.navdy.hud.app.service.CustomNotificationServiceHandler a, long j) {
        super();
        this.this$0 = a;
        this.val$t1 = j;
    }
    
    public void onComplete(com.navdy.service.library.events.navigation.NavigationRouteResult a) {
        long j = android.os.SystemClock.elapsedRealtime();
        com.navdy.hud.app.service.CustomNotificationServiceHandler.access$000().v(new StringBuilder().append("FIND_ROUTE_TO_CLOSEST_GAS_STATION: success: time  [").append(j - this.val$t1).append("]").toString());
        com.navdy.hud.app.service.CustomNotificationServiceHandler.access$000().v(new StringBuilder().append("FIND_ROUTE_TO_CLOSEST_GAS_STATION route id[").append(a.routeId).append("] label[").append(a.label).append("] via[").append(a.via).append("] address[").append((a.address == null) ? "null" : a.address.replace((CharSequence)"\n", (CharSequence)"")).append("] length[").append(a.length).append("] duration[").append(a.duration_traffic).append("] freeFlowDuration[").append(a.duration).append("]").toString());
    }
    
    public void onError(com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback$Error a) {
        long j = android.os.SystemClock.elapsedRealtime();
        com.navdy.hud.app.service.CustomNotificationServiceHandler.access$000().v(new StringBuilder().append("FIND_ROUTE_TO_CLOSEST_GAS_STATION: failed:").append(a).append(" time to error [").append(j - this.val$t1).append("]").toString());
    }
}
