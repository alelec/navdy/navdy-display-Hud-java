package com.navdy.hud.app.service;

class FileTransferHandler$3 implements Runnable {
    final com.navdy.hud.app.service.FileTransferHandler this$0;
    final com.navdy.service.library.events.file.FileTransferData val$request;
    
    FileTransferHandler$3(com.navdy.hud.app.service.FileTransferHandler a, com.navdy.service.library.events.file.FileTransferData a0) {
        super();
        this.this$0 = a;
        this.val$request = a0;
    }
    
    public void run() {
        com.navdy.hud.app.service.FileTransferHandler.access$100(this.this$0).v("onFileTransferData");
        if (this.this$0.remoteDevice.getLinkBandwidthLevel() > 0) {
            String s = this.this$0.fileTransferManager.absolutePathForTransferId(this.val$request.transferId.intValue());
            com.navdy.service.library.events.file.FileTransferStatus a = this.this$0.fileTransferManager.handleFileTransferData(this.val$request);
            this.this$0.remoteDevice.postEvent((com.squareup.wire.Message)a);
            com.navdy.hud.app.service.FileTransferHandler.access$200(this.this$0, a.success.booleanValue() && !a.transferComplete.booleanValue());
            if (s != null && !s.equals("") && a.transferComplete.booleanValue()) {
                com.navdy.hud.app.service.FileTransferHandler.access$300(this.this$0, s);
            }
        } else {
            com.navdy.service.library.events.file.FileTransferStatus a0 = new com.navdy.service.library.events.file.FileTransferStatus$Builder().success(Boolean.valueOf(false)).transferComplete(Boolean.valueOf(false)).error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_HOST_BUSY).build();
            this.this$0.remoteDevice.postEvent((com.squareup.wire.Message)a0);
        }
    }
}
