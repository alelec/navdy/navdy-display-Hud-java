package com.navdy.hud.app.service.pandora.messages;

final class UpdateStatus$1 extends java.util.HashMap {
    UpdateStatus$1() {
        this.put(Byte.valueOf((byte)1), new com.navdy.hud.app.service.pandora.messages.UpdateStatus(com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value.PLAYING, (com.navdy.hud.app.service.pandora.messages.UpdateStatus$1)null));
        this.put(Byte.valueOf((byte)2), new com.navdy.hud.app.service.pandora.messages.UpdateStatus(com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value.PAUSED, (com.navdy.hud.app.service.pandora.messages.UpdateStatus$1)null));
        this.put(Byte.valueOf((byte)3), new com.navdy.hud.app.service.pandora.messages.UpdateStatus(com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value.INCOMPATIBLE_API_VERSION, (com.navdy.hud.app.service.pandora.messages.UpdateStatus$1)null));
        this.put(Byte.valueOf((byte)4), new com.navdy.hud.app.service.pandora.messages.UpdateStatus(com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value.UNKNOWN_ERROR, (com.navdy.hud.app.service.pandora.messages.UpdateStatus$1)null));
        this.put(Byte.valueOf((byte)5), new com.navdy.hud.app.service.pandora.messages.UpdateStatus(com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value.NO_STATIONS, (com.navdy.hud.app.service.pandora.messages.UpdateStatus$1)null));
        this.put(Byte.valueOf((byte)6), new com.navdy.hud.app.service.pandora.messages.UpdateStatus(com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value.NO_STATION_ACTIVE, (com.navdy.hud.app.service.pandora.messages.UpdateStatus$1)null));
        this.put(Byte.valueOf((byte)7), new com.navdy.hud.app.service.pandora.messages.UpdateStatus(com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value.INSUFFICIENT_CONNECTIVITY, (com.navdy.hud.app.service.pandora.messages.UpdateStatus$1)null));
        this.put(Byte.valueOf((byte)8), new com.navdy.hud.app.service.pandora.messages.UpdateStatus(com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value.LISTENING_RESTRICTIONS, (com.navdy.hud.app.service.pandora.messages.UpdateStatus$1)null));
        this.put(Byte.valueOf((byte)9), new com.navdy.hud.app.service.pandora.messages.UpdateStatus(com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value.INVALID_LOGIN, (com.navdy.hud.app.service.pandora.messages.UpdateStatus$1)null));
    }
}
