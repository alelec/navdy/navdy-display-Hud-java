package com.navdy.hud.app.service.pandora;

class PandoraManager$ConnectRunnable implements Runnable {
    final private static int MAX_CONNECTING_RETRIES = 10;
    final private static int PAUSE_BETWEEN_CONNECTING_RETRIES = 2000;
    private boolean isStartMusicOn;
    final com.navdy.hud.app.service.pandora.PandoraManager this$0;
    
    public PandoraManager$ConnectRunnable(com.navdy.hud.app.service.pandora.PandoraManager a, boolean b) {
        super();
        this.this$0 = a;
        this.isStartMusicOn = false;
        this.isStartMusicOn = b;
    }
    
    private void safeSleepPause() {
        try {
            Thread.sleep(2000L);
        } catch(InterruptedException ignoredException) {
            com.navdy.hud.app.service.pandora.PandoraManager.sLogger.e("Interrupted while pausing between Pandora connection retries");
        }
    }
    
    public void run() {
        try {
            if (com.navdy.hud.app.service.pandora.PandoraManager.access$000(this.this$0)) {
                this.this$0.terminateAndClose();
            }
            String s = new com.navdy.service.library.device.NavdyDeviceId(com.navdy.hud.app.service.pandora.PandoraManager.access$100(this.this$0)).getBluetoothAddress();
            android.bluetooth.BluetoothDevice a = android.bluetooth.BluetoothAdapter.getDefaultAdapter().getRemoteDevice(s);
            android.bluetooth.BluetoothSocket a0 = null;
            int i = 0;
            while(i < 10) {
                try {
                    a0 = a.createRfcommSocketToServiceRecord(com.navdy.hud.app.service.pandora.PandoraManager.access$200());
                    a0.connect();
                    break;
                } catch(java.io.IOException ignoredException) {
                }
                com.navdy.hud.app.service.pandora.PandoraManager.sLogger.e(new StringBuilder().append("Cannot connect to Pandora on remote device: ").append(com.navdy.hud.app.service.pandora.PandoraManager.access$100(this.this$0)).toString());
                com.navdy.service.library.util.IOUtils.closeObject((java.io.Closeable)a0);
                this.safeSleepPause();
                i = i + 1;
            }
            com.navdy.hud.app.service.pandora.PandoraManager.access$300(this.this$0, a0);
            if (com.navdy.hud.app.service.pandora.PandoraManager.access$000(this.this$0)) {
                this.this$0.sendOrQueueMessage((com.navdy.hud.app.service.pandora.messages.BaseOutgoingMessage)com.navdy.hud.app.service.pandora.messages.SessionStart.INSTANCE);
                this.this$0.sendOrQueueMessage((com.navdy.hud.app.service.pandora.messages.BaseOutgoingMessage)com.navdy.hud.app.service.pandora.messages.GetTrackInfoExtended.INSTANCE);
                this.this$0.sendOrQueueMessage((com.navdy.hud.app.service.pandora.messages.BaseOutgoingMessage)com.navdy.hud.app.service.pandora.messages.SetTrackElapsedPolling.ENABLED);
                if (this.isStartMusicOn) {
                    this.this$0.sendOrQueueMessage((com.navdy.hud.app.service.pandora.messages.BaseOutgoingMessage)com.navdy.hud.app.service.pandora.messages.EventTrackPlay.INSTANCE);
                }
                Thread a1 = new Thread((Runnable)new com.navdy.hud.app.service.pandora.ReadMessagesThread(this.this$0));
                a1.setName(new StringBuilder().append("Pandora-Read-").append(com.navdy.hud.app.service.pandora.PandoraManager.access$400().getAndIncrement()).toString());
                a1.start();
            }
            com.navdy.hud.app.service.pandora.PandoraManager.access$500(this.this$0).set(false);
        } catch(Throwable a2) {
            com.navdy.hud.app.service.pandora.PandoraManager.sLogger.e(Thread.currentThread().getName(), a2);
        }
        com.navdy.hud.app.service.pandora.PandoraManager.sLogger.v(new StringBuilder().append("exiting thread:").append(Thread.currentThread().getName()).toString());
    }
}
