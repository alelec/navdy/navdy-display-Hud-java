package com.navdy.hud.app.service;

public class ConnectionHandler {
    static com.navdy.service.library.events.Capabilities CAPABILITIES;
    final private static com.navdy.hud.app.service.ConnectionHandler$DeviceSyncEvent DEVICE_FULL_SYNC_EVENT;
    final private static com.navdy.hud.app.service.ConnectionHandler$DeviceSyncEvent DEVICE_MINIMAL_SYNC_EVENT;
    final public static int IOS_APP_LAUNCH_TIMEOUT = 5000;
    private static java.util.List LEGACY_CAPABILITIES;
    final private static int LINK_LOST_TIME_OUT_AFTER_DISCONNECTED = 1000;
    final private static int REDOWNLOAD_THRESHOLD = 30000;
    final private static com.navdy.service.library.log.Logger sLogger;
    private com.squareup.otto.Bus bus;
    protected android.content.Context context;
    protected boolean disconnecting;
    private volatile boolean forceFullUpdate;
    private volatile boolean isNetworkLinkReady;
    private String lastConnectedDeviceId;
    protected com.navdy.service.library.events.DeviceInfo lastConnectedDeviceInfo;
    private long lastDisconnectTime;
    private boolean mAppClosed;
    private boolean mAppLaunchAttempted;
    private Runnable mAppLaunchAttemptedRunnable;
    protected com.navdy.hud.app.service.CustomNotificationServiceHandler mCustomNotificationServiceHandler;
    protected Runnable mDisconnectRunnable;
    protected com.navdy.hud.app.profile.DriverProfileManager mDriverProfileManager;
    protected android.os.Handler mHandler;
    private com.navdy.hud.app.device.PowerManager mPowerManager;
    protected Runnable mReconnectTimedOutRunnable;
    protected com.navdy.hud.app.service.RemoteDeviceProxy mRemoteDevice;
    protected com.navdy.hud.app.service.ConnectionServiceProxy proxy;
    private com.navdy.hud.app.common.TimeHelper timeHelper;
    private volatile boolean triggerDownload;
    private com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.service.ConnectionHandler.class);
        LEGACY_CAPABILITIES = (java.util.List)new java.util.ArrayList();
        LEGACY_CAPABILITIES.add(com.navdy.service.library.events.LegacyCapability.CAPABILITY_VOICE_SEARCH);
        LEGACY_CAPABILITIES.add(com.navdy.service.library.events.LegacyCapability.CAPABILITY_COMPACT_UI);
        LEGACY_CAPABILITIES.add(com.navdy.service.library.events.LegacyCapability.CAPABILITY_LOCAL_MUSIC_BROWSER);
        CAPABILITIES = new com.navdy.service.library.events.Capabilities$Builder().compactUi(Boolean.valueOf(true)).localMusicBrowser(Boolean.valueOf(true)).voiceSearch(Boolean.valueOf(true)).voiceSearchNewIOSPauseBehaviors(Boolean.valueOf(true)).musicArtworkCache(Boolean.valueOf(true)).searchResultList(Boolean.valueOf(true)).cannedResponseToSms(Boolean.valueOf(com.navdy.hud.app.ui.component.UISettings.supportsIosSms())).customDialLongPress(Boolean.valueOf(true)).build();
        DEVICE_FULL_SYNC_EVENT = new com.navdy.hud.app.service.ConnectionHandler$DeviceSyncEvent(1);
        DEVICE_MINIMAL_SYNC_EVENT = new com.navdy.hud.app.service.ConnectionHandler$DeviceSyncEvent(0);
    }
    
    public ConnectionHandler(android.content.Context a, com.navdy.hud.app.service.ConnectionServiceProxy a0, com.navdy.hud.app.device.PowerManager a1, com.navdy.hud.app.profile.DriverProfileManager a2, com.navdy.hud.app.ui.framework.UIStateManager a3, com.navdy.hud.app.common.TimeHelper a4) {
        this.forceFullUpdate = false;
        this.isNetworkLinkReady = false;
        this.triggerDownload = true;
        this.mAppClosed = true;
        this.mAppLaunchAttempted = false;
        this.context = a;
        this.proxy = a0;
        this.mPowerManager = a1;
        this.bus = this.proxy.getBus();
        this.bus.register(this);
        this.mCustomNotificationServiceHandler = new com.navdy.hud.app.service.CustomNotificationServiceHandler(this.bus);
        this.mCustomNotificationServiceHandler.start();
        this.mDriverProfileManager = a2;
        this.uiStateManager = a3;
        this.timeHelper = a4;
        sLogger.i(new StringBuilder().append("Creating connectionHandler:").append(this).toString());
        this.mHandler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.mDisconnectRunnable = (Runnable)new com.navdy.hud.app.service.ConnectionHandler$1(this);
        this.mReconnectTimedOutRunnable = (Runnable)new com.navdy.hud.app.service.ConnectionHandler$2(this);
        this.mAppLaunchAttemptedRunnable = (Runnable)new com.navdy.hud.app.service.ConnectionHandler$3(this);
    }
    
    static void access$000(com.navdy.hud.app.service.ConnectionHandler a) {
        a.handleDisconnectWithoutLinkLoss();
    }
    
    static com.navdy.service.library.log.Logger access$100() {
        return sLogger;
    }
    
    private String getSystemVersion() {
        return (String.valueOf(3049).equals(android.os.Build.VERSION.INCREMENTAL)) ? android.os.Build.VERSION.INCREMENTAL : "1.3.3051-corona";
    }
    
    private void handleDisconnectWithoutLinkLoss() {
        com.navdy.service.library.events.DeviceInfo$Platform a = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getRemoteDevicePlatform();
        if (a != null && a.equals(com.navdy.service.library.events.DeviceInfo$Platform.PLATFORM_iOS)) {
            this.sendConnectionNotification();
        }
    }
    
    private void onConnect(com.navdy.service.library.device.NavdyDeviceId a) {
        if (this.mRemoteDevice == null) {
            this.mRemoteDevice = new com.navdy.hud.app.service.RemoteDeviceProxy(this.proxy, this.context, a);
        }
        this.mDriverProfileManager.loadProfileForId(a);
        com.navdy.service.library.device.NavdyDeviceId a0 = com.navdy.service.library.device.NavdyDeviceId.getThisDevice(this.context);
        com.navdy.service.library.events.DeviceInfo a1 = new com.navdy.service.library.events.DeviceInfo$Builder().deviceId(a0.toString()).clientVersion("1.3.3051-corona").protocolVersion(com.navdy.service.library.Version.PROTOCOL_VERSION.toString()).deviceName(a0.getDeviceName()).systemVersion(this.getSystemVersion()).model(android.os.Build.MODEL).deviceUuid(android.os.Build.SERIAL).systemApiLevel(Integer.valueOf(android.os.Build.VERSION.SDK_INT)).kernelVersion(System.getProperty("os.version")).platform(com.navdy.service.library.events.DeviceInfo$Platform.PLATFORM_Android).buildType(android.os.Build.TYPE).deviceMake("Navdy").forceFullUpdate(Boolean.valueOf(this.forceFullUpdate)).legacyCapabilities(LEGACY_CAPABILITIES).capabilities(CAPABILITIES).build();
        sLogger.d(new StringBuilder().append("onConnect - sending device info: ").append(a1).toString());
        this.mRemoteDevice.postEvent((com.squareup.wire.Message)a1);
    }
    
    private void onDisconnect(com.navdy.service.library.device.NavdyDeviceId a) {
        synchronized(this) {
            com.navdy.service.library.log.Logger a0 = sLogger;
            a0.v(new StringBuilder().append("onDisconnect:").append(a).toString());
            this.isNetworkLinkReady = false;
            com.navdy.hud.app.framework.network.NetworkStateManager.getInstance().networkNotAvailable();
            this.mDriverProfileManager.setCurrentProfile((com.navdy.hud.app.profile.DriverProfile)null);
            this.mHandler.postDelayed(this.mDisconnectRunnable, 1000L);
        }
        /*monexit(this)*/;
    }
    
    private void onLinkEstablished(com.navdy.service.library.device.NavdyDeviceId a) {
        this.mPowerManager.wakeUp(com.navdy.hud.app.analytics.AnalyticsSupport$WakeupReason.PHONE);
        com.navdy.hud.app.service.RemoteDeviceProxy dummy = this.mRemoteDevice;
        this.mRemoteDevice = new com.navdy.hud.app.service.RemoteDeviceProxy(this.proxy, this.context, a);
        this.mHandler.removeCallbacks(this.mReconnectTimedOutRunnable);
    }
    
    private void onLinkLost(com.navdy.service.library.device.NavdyDeviceId a) {
        synchronized(this) {
            com.navdy.service.library.log.Logger a0 = sLogger;
            a0.v(new StringBuilder().append("onLinkLost:").append(a).toString());
            this.isNetworkLinkReady = false;
            this.mHandler.removeCallbacks(this.mDisconnectRunnable);
            this.mHandler.removeCallbacks(this.mReconnectTimedOutRunnable);
            boolean b = this.mRemoteDevice != null;
            this.mRemoteDevice = null;
            com.navdy.hud.app.util.picasso.PicassoUtil.clearCache();
            if (b) {
                com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().handleDisconnect(true);
                if (!this.disconnecting) {
                    this.mHandler.postDelayed(this.mReconnectTimedOutRunnable, 15000L);
                }
            }
            this.disconnecting = false;
        }
        /*monexit(this)*/;
    }
    
    private void requestIAPUpdateBasedOnPreference(com.navdy.service.library.events.preferences.NotificationPreferences a) {
        if (a != null) {
            if (Boolean.TRUE.equals(a.enabled)) {
                this.bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)new com.navdy.service.library.events.callcontrol.CallStateUpdateRequest(Boolean.valueOf(true))));
                this.bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)new com.navdy.service.library.events.audio.NowPlayingUpdateRequest(Boolean.valueOf(true))));
            } else {
                this.bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)new com.navdy.service.library.events.callcontrol.CallStateUpdateRequest(Boolean.valueOf(false))));
                this.bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)new com.navdy.service.library.events.audio.NowPlayingUpdateRequest(Boolean.valueOf(false))));
            }
        }
    }
    
    public void connect() {
        this.proxy.connect();
    }
    
    public void connectToDevice(com.navdy.service.library.device.NavdyDeviceId a) {
        com.navdy.service.library.events.connection.ConnectionRequest$Action a0 = com.navdy.service.library.events.connection.ConnectionRequest$Action.CONNECTION_SELECT;
        String s = (a == null) ? null : a.toString();
        this.sendLocalMessage((com.squareup.wire.Message)new com.navdy.service.library.events.connection.ConnectionRequest(a0, s));
    }
    
    public void disconnect() {
        if (this.mRemoteDevice != null) {
            this.lastConnectedDeviceInfo = null;
            this.disconnecting = true;
            this.connectToDevice((com.navdy.service.library.device.NavdyDeviceId)null);
        }
    }
    
    public com.navdy.service.library.device.NavdyDeviceId getConnectedDevice() {
        com.navdy.service.library.device.NavdyDeviceId a = (this.mRemoteDevice == null) ? null : this.mRemoteDevice.getDeviceId();
        return a;
    }
    
    public com.navdy.service.library.events.debug.StartDriveRecordingEvent getDriverRecordingEvent() {
        com.navdy.service.library.events.debug.StartDriveRecordingEvent a = (this.proxy == null) ? null : this.proxy.getDriverRecordingEvent();
        return a;
    }
    
    public com.navdy.service.library.events.DeviceInfo getLastConnectedDeviceInfo() {
        return this.lastConnectedDeviceInfo;
    }
    
    public com.navdy.service.library.device.RemoteDevice getRemoteDevice() {
        return this.mRemoteDevice;
    }
    
    public boolean isAppClosed() {
        return this.mAppClosed;
    }
    
    public boolean isAppLaunchAttempted() {
        return this.mAppLaunchAttempted;
    }
    
    public boolean isNetworkLinkReady() {
        return this.isNetworkLinkReady;
    }
    
    public void onApplicationLaunchAttempted(com.navdy.hud.app.service.ConnectionHandler$ApplicationLaunchAttempted a) {
        sLogger.d("Attempting to launch the App, Will wait and see");
        if (this.mAppClosed) {
            this.mAppLaunchAttempted = true;
            this.mHandler.removeCallbacks(this.mAppLaunchAttemptedRunnable);
            this.mHandler.postDelayed(this.mAppLaunchAttemptedRunnable, 5000L);
        }
    }
    
    public void onConnectionStateChange(com.navdy.service.library.events.connection.ConnectionStateChange a) {
        sLogger.d(new StringBuilder().append("ConnectionStateChange - ").append(a.state).toString());
        String s = a.remoteDeviceId;
        com.navdy.service.library.device.NavdyDeviceId a0 = (android.text.TextUtils.isEmpty((CharSequence)s)) ? com.navdy.service.library.device.NavdyDeviceId.UNKNOWN_ID : new com.navdy.service.library.device.NavdyDeviceId(s);
        this.mAppLaunchAttempted = false;
        this.mHandler.removeCallbacks(this.mAppLaunchAttemptedRunnable);
        switch(com.navdy.hud.app.service.ConnectionHandler$4.$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState[a.state.ordinal()]) {
            case 4: {
                this.lastDisconnectTime = android.os.SystemClock.elapsedRealtime();
                this.mAppClosed = true;
                this.onLinkLost(a0);
                break;
            }
            case 3: {
                this.lastDisconnectTime = android.os.SystemClock.elapsedRealtime();
                this.mAppClosed = true;
                this.onDisconnect(a0);
                break;
            }
            case 2: {
                this.onConnect(a0);
                this.mAppClosed = false;
                break;
            }
            case 1: {
                this.mAppClosed = true;
                this.onLinkEstablished(a0);
                break;
            }
        }
    }
    
    public void onDateTimeConfiguration(com.navdy.service.library.events.settings.DateTimeConfiguration a) {
        sLogger.i(new StringBuilder().append("Received timestamp from the client: ").append(a.timestamp).append(" ").append(a.timezone).toString());
        try {
            if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
                android.app.AlarmManager a0 = (android.app.AlarmManager)this.context.getSystemService("alarm");
                sLogger.v(new StringBuilder().append("setting date timezone[").append(a.timezone).append("] time[").append(a.timestamp).append("]").toString());
                if (a.timezone == null) {
                    sLogger.w("timezone not provided");
                } else if (java.util.TimeZone.getDefault().getID().equalsIgnoreCase(a.timezone)) {
                    sLogger.v(new StringBuilder().append("timezone already set to ").append(a.timezone).toString());
                } else {
                    java.util.TimeZone a1 = java.util.TimeZone.getTimeZone(a.timezone);
                    if (a1.getID().equalsIgnoreCase(a.timezone)) {
                        sLogger.v(new StringBuilder().append("timezone found on HUD:").append(a1.getID()).toString());
                        a0.setTimeZone(a.timezone);
                    } else {
                        sLogger.e(new StringBuilder().append("timezone not found on HUD:").append(a.timezone).toString());
                    }
                }
                sLogger.v(new StringBuilder().append("setting time [").append(a.timestamp).append("]").toString());
                a0.setTime(a.timestamp.longValue());
                sLogger.v(new StringBuilder().append("set time [").append(a.timestamp).append("]").toString());
                this.bus.post(com.navdy.hud.app.common.TimeHelper.DATE_TIME_AVAILABLE_EVENT);
                sLogger.v("post date complete");
            }
            com.navdy.hud.app.profile.DriverProfileManager a2 = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager();
            a2.getSessionPreferences().setClockConfiguration(a);
            a2.updateLocalPreferences(new com.navdy.service.library.events.preferences.LocalPreferences$Builder(a2.getCurrentProfile().getLocalPreferences()).clockFormat(a.format).build());
        } catch(Throwable a3) {
            sLogger.e("Cannot update time on device", a3);
        }
    }
    
    public void onDeviceInfo(com.navdy.service.library.events.DeviceInfo a) {
        sLogger.i(new StringBuilder().append("Received deviceInfo:").append(a).toString());
        if (this.mRemoteDevice != null) {
            this.triggerDownload = true;
            if (this.lastConnectedDeviceId == null) {
                sLogger.v("no last device");
                this.lastConnectedDeviceId = a.deviceId;
            } else {
                long j = android.os.SystemClock.elapsedRealtime() - this.lastDisconnectTime;
                if (j >= 30000L) {
                    sLogger.v(new StringBuilder().append("threshold not met=").append(j).toString());
                    this.lastConnectedDeviceId = a.deviceId;
                } else {
                    sLogger.v(new StringBuilder().append("threshold met=").append(j).toString());
                    if (android.text.TextUtils.equals((CharSequence)this.lastConnectedDeviceId, (CharSequence)a.deviceId)) {
                        this.triggerDownload = false;
                        sLogger.v(new StringBuilder().append("same device connected:").append(a.deviceId).toString());
                    } else {
                        sLogger.v(new StringBuilder().append("different device connected last[").append(this.lastConnectedDeviceId).append("] current[").append(a.deviceId).append("]").toString());
                        this.lastConnectedDeviceId = a.deviceId;
                    }
                }
            }
            String s = this.mRemoteDevice.getDeviceId().toString();
            com.navdy.service.library.events.DeviceInfo a0 = new com.navdy.service.library.events.DeviceInfo$Builder(a).deviceId(s).build();
            String s0 = (this.lastConnectedDeviceInfo == null) ? null : this.lastConnectedDeviceInfo.deviceId;
            boolean b = !s.equals(s0);
            this.lastConnectedDeviceInfo = a0;
            this.mRemoteDevice.setDeviceInfo(a0);
            this.bus.post(new com.navdy.hud.app.event.DeviceInfoAvailable(a0));
            if (this.triggerDownload) {
                sLogger.v("trigger device sync");
                this.bus.post(DEVICE_FULL_SYNC_EVENT);
            } else if (a0.platform != com.navdy.service.library.events.DeviceInfo$Platform.PLATFORM_iOS) {
                sLogger.v("don't trigger device sync");
            } else {
                sLogger.v("triggering minimal sync");
                this.bus.post(DEVICE_MINIMAL_SYNC_EVENT);
            }
            if (b) {
                this.sendConnectionNotification();
            } else {
                com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().handleConnect();
                com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().hideConnectionToast(true);
                long j0 = android.os.SystemClock.elapsedRealtime() - this.lastDisconnectTime;
                sLogger.i(new StringBuilder().append("Tracking a reconnect that took ").append(j0).append("ms").toString());
                com.navdy.hud.app.analytics.AnalyticsSupport.recordPhoneDisconnect(true, String.valueOf(j0));
            }
        }
    }
    
    public void onDisconnect(com.navdy.hud.app.event.Disconnect a) {
        this.disconnect();
    }
    
    public void onDriverProfileUpdated(com.navdy.hud.app.event.DriverProfileChanged a) {
        com.navdy.hud.app.profile.DriverProfile a0 = this.mDriverProfileManager.getCurrentProfile();
        com.navdy.service.library.events.preferences.NotificationPreferences a1 = (a0 == null) ? null : a0.getNotificationPreferences();
        this.requestIAPUpdateBasedOnPreference(a1);
    }
    
    public void onIncrementalOTAFailureDetected(com.navdy.hud.app.util.OTAUpdateService$IncrementalOTAFailureDetected a) {
        sLogger.d("Incremental OTA failure detected, indicate the remote device to force full update");
        this.forceFullUpdate = true;
    }
    
    public void onLinkPropertiesChanged(com.navdy.service.library.events.connection.LinkPropertiesChanged a) {
        if (a != null && a.bandwidthLevel != null && this.mRemoteDevice != null) {
            this.mRemoteDevice.setLinkBandwidthLevel(a.bandwidthLevel.intValue());
        }
    }
    
    public void onLocalSpeechRequest(com.navdy.hud.app.event.LocalSpeechRequest a) {
        if (!com.navdy.hud.app.framework.phonecall.CallUtils.isPhoneCallInProgress()) {
            if (sLogger.isLoggable(2)) {
                sLogger.v(new StringBuilder().append("[tts-outbound] [").append(a.speechRequest.category.name()).append("] [").append(a.speechRequest.words).append("]").toString());
            }
            this.onRemoteEvent(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)a.speechRequest));
        }
    }
    
    public void onNetworkLinkReady(com.navdy.service.library.events.connection.NetworkLinkReady a) {
        sLogger.v("DummyNetNetworkFactory:onConnect");
        this.isNetworkLinkReady = true;
        com.navdy.hud.app.framework.network.NetworkStateManager.getInstance().networkAvailable();
        com.navdy.hud.app.service.GestureVideosSyncService.scheduleWithDelay(30000L);
        com.navdy.hud.app.service.ObdCANBusDataUploadService.scheduleWithDelay(45000L);
    }
    
    public void onNotificationPreference(com.navdy.service.library.events.preferences.NotificationPreferences a) {
        this.requestIAPUpdateBasedOnPreference(a);
    }
    
    public void onRemoteEvent(com.navdy.hud.app.event.RemoteEvent a) {
        if (this.mRemoteDevice != null) {
            this.mRemoteDevice.postEvent(a.getMessage());
        }
    }
    
    public void onShutdown(com.navdy.hud.app.event.Shutdown a) {
        if (a.state == com.navdy.hud.app.event.Shutdown$State.SHUTTING_DOWN) {
            this.shutdown();
        }
    }
    
    public void onWakeup(com.navdy.hud.app.event.Wakeup a) {
        if (com.navdy.hud.app.device.dial.DialManager.getInstance().getBondedDialCount() != 0) {
            if (this.mRemoteDevice != null) {
                this.bus.post(new com.navdy.service.library.events.ui.ShowScreen$Builder().screen(this.uiStateManager.getDefaultMainActiveScreen()).build());
            } else {
                sLogger.v("go to welcome screen");
                android.os.Bundle a0 = new android.os.Bundle();
                a0.putString(com.navdy.hud.app.screen.WelcomeScreen.ARG_ACTION, com.navdy.hud.app.screen.WelcomeScreen.ACTION_RECONNECT);
                this.bus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen.SCREEN_WELCOME, a0, false));
            }
        } else {
            sLogger.v("go to dial pairing screen");
            this.bus.post(new com.navdy.service.library.events.ui.ShowScreen$Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_PAIRING).build());
        }
    }
    
    public void searchForDevices() {
        this.sendLocalMessage((com.squareup.wire.Message)new com.navdy.service.library.events.connection.ConnectionRequest(com.navdy.service.library.events.connection.ConnectionRequest$Action.CONNECTION_START_SEARCH, (String)null));
    }
    
    public void sendConnectionNotification() {
        com.navdy.hud.app.framework.notifications.NotificationManager a = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
        if (com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().isRemoteDeviceConnected()) {
            if (this.isAppClosed()) {
                a.handleDisconnect(false);
                a.showHideToast(false);
            } else {
                a.handleConnect();
                a.showHideToast(true);
            }
        } else {
            a.handleDisconnect(true);
            a.showHideToast(false);
        }
    }
    
    public void sendLocalMessage(com.squareup.wire.Message a) {
        try {
            this.proxy.postRemoteEvent(com.navdy.service.library.device.NavdyDeviceId.getThisDevice(this.context), com.navdy.service.library.events.NavdyEventUtil.eventFromMessage(a));
        } catch(Exception a0) {
            sLogger.e("Failed to send local event", (Throwable)a0);
        }
    }
    
    public boolean serviceConnected() {
        return this.proxy.connected();
    }
    
    public void shutdown() {
        this.disconnect();
        this.proxy.disconnect();
    }
    
    public void stopSearch() {
        this.sendLocalMessage((com.squareup.wire.Message)new com.navdy.service.library.events.connection.ConnectionRequest(com.navdy.service.library.events.connection.ConnectionRequest$Action.CONNECTION_STOP_SEARCH, (String)null));
    }
}
