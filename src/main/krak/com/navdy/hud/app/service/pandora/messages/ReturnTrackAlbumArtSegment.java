package com.navdy.hud.app.service.pandora.messages;

public class ReturnTrackAlbumArtSegment extends com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage {
    public byte[] data;
    public byte segmentIndex;
    public byte totalSegments;
    public int trackToken;
    
    public ReturnTrackAlbumArtSegment() {
    }
    
    protected static com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage innerBuildFromPayload(byte[] a) {
        java.nio.ByteBuffer a0 = java.nio.ByteBuffer.wrap(a);
        a0.get();
        com.navdy.hud.app.service.pandora.messages.ReturnTrackAlbumArtSegment a1 = new com.navdy.hud.app.service.pandora.messages.ReturnTrackAlbumArtSegment();
        a1.trackToken = a0.getInt();
        int i = a0.get();
        a1.segmentIndex = (byte)i;
        int i0 = a0.get();
        a1.totalSegments = (byte)i0;
        a1.data = new byte[a0.remaining()];
        a0.get(a1.data);
        return a1;
    }
    
    public String toString() {
        StringBuilder a = new StringBuilder().append("Got artwork's segment ");
        int i = this.segmentIndex;
        StringBuilder a0 = a.append(i + 1).append("/");
        int i0 = this.totalSegments;
        return a0.append(i0).append(" for track with token ").append(this.trackToken).toString();
    }
}
