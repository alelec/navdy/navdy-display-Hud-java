package com.navdy.hud.app.service;

public class CustomNotificationServiceHandler {
    final private static com.navdy.service.library.log.Logger sLogger;
    com.squareup.otto.Bus bus;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.service.CustomNotificationServiceHandler.class);
    }
    
    public CustomNotificationServiceHandler(com.squareup.otto.Bus a) {
        this.bus = a;
        a.register(this);
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    public void onShowCustomNotification(com.navdy.service.library.events.notification.ShowCustomNotification a) {
        int i = 0;
        sLogger.v(new StringBuilder().append("onShowCustomNotification: ").append(a.id).toString());
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
        String s = a.id;
        switch(s.hashCode()) {
            case 1487737514: {
                i = (s.equals("DIAL_DISCONNECTED")) ? 11 : -1;
                break;
            }
            case 1483760395: {
                i = (s.equals("TEXT_NOTIFICAION_WITH_REPLY_999")) ? 9 : -1;
                break;
            }
            case 1483755310: {
                i = (s.equals("TEXT_NOTIFICAION_WITH_REPLY_408")) ? 7 : -1;
                break;
            }
            case 1456922895: {
                i = (s.equals("CLEAR_CURRENT_TOAST")) ? 32 : -1;
                break;
            }
            case 1427731674: {
                i = (s.equals("DIAL_CONNECTED")) ? 10 : -1;
                break;
            }
            case 1336078449: {
                i = (s.equals("PHONE_BATTERY_LOW")) ? 4 : -1;
                break;
            }
            case 1314336394: {
                i = (s.equals("PHONE_APP_DISCONNECTED")) ? 31 : -1;
                break;
            }
            case 1225163157: {
                i = (s.equals("APPLE_CALENDAR_1")) ? 36 : -1;
                break;
            }
            case 1133254737: {
                i = (s.equals("BRIGHTNESS")) ? 20 : -1;
                break;
            }
            case 1033071277: {
                i = (s.equals("DIAL_BATTERY_EXTREMELY_LOW")) ? 15 : -1;
                break;
            }
            case 998768680: {
                i = (s.equals("END_PHONE_CALL_323")) ? 3 : -1;
                break;
            }
            case 993806900: {
                i = (s.equals("LOW_FUEL_LEVEL_CLEAR")) ? 39 : -1;
                break;
            }
            case 634551002: {
                i = (s.equals("TEXT_NOTIFICAION_WITH_NO_REPLY_510")) ? 8 : -1;
                break;
            }
            case 531351226: {
                i = (s.equals("FUEL_ADDED_TEST")) ? 42 : -1;
                break;
            }
            case 518374548: {
                i = (s.equals("TRAFFIC_JAM")) ? 23 : -1;
                break;
            }
            case 361103604: {
                i = (s.equals("TRAFFIC_INCIDENT")) ? 25 : -1;
                break;
            }
            case 333419062: {
                i = (s.equals("VOICE_ASSIST")) ? 19 : -1;
                break;
            }
            case 204001803: {
                i = (s.equals("PHONE_BATTERY_EXTREMELY_LOW")) ? 5 : -1;
                break;
            }
            case 83953026: {
                i = (s.equals("CLEAR_TRAFFIC_JAM")) ? 24 : -1;
                break;
            }
            case 73725445: {
                i = (s.equals("MUSIC")) ? 18 : -1;
                break;
            }
            case 60514076: {
                i = (s.equals("DIAL_FORGOTTEN_SINGLE")) ? 12 : -1;
                break;
            }
            case 43099391: {
                i = (s.equals("PHONE_BATTERY_OK")) ? 6 : -1;
                break;
            }
            case -5849545: {
                i = (s.equals("CLEAR_ALL_TOAST")) ? 33 : -1;
                break;
            }
            case -11797496: {
                i = (s.equals("FIND_GAS_STATION")) ? 43 : -1;
                break;
            }
            case -63686527: {
                i = (s.equals("TRAFFIC_DELAY")) ? 27 : -1;
                break;
            }
            case -342876771: {
                i = (s.equals("DIAL_BATTERY_OK")) ? 17 : -1;
                break;
            }
            case -406264442: {
                i = (s.equals("LOW_FUEL_LEVEL")) ? 37 : -1;
                break;
            }
            case -638027464: {
                i = (s.equals("PHONE_CONNECTED")) ? 30 : -1;
                break;
            }
            case -767590304: {
                i = (s.equals("FIND_ROUTE_TO_CLOSEST_GAS_STATION")) ? 44 : -1;
                break;
            }
            case -930941457: {
                i = (s.equals("CLEAR_TRAFFIC_DELAY")) ? 28 : -1;
                break;
            }
            case -1163820707: {
                i = (s.equals("INCOMING_PHONE_CALL_323")) ? 2 : -1;
                break;
            }
            case -1221193814: {
                i = (s.equals("GENERIC_2")) ? 1 : -1;
                break;
            }
            case -1221193815: {
                i = (s.equals("GENERIC_1")) ? 0 : -1;
                break;
            }
            case -1251325556: {
                i = (s.equals("PHONE_DISCONNECTED")) ? 29 : -1;
                break;
            }
            case -1284131530: {
                i = (s.equals("GOOGLE_CALENDAR_1")) ? 35 : -1;
                break;
            }
            case -1338282068: {
                i = (s.equals("DIAL_BATTERY_VERY_LOW")) ? 16 : -1;
                break;
            }
            case -1472431602: {
                i = (s.equals("LOW_FUEL_LEVEL_NOCHECK")) ? 38 : -1;
                break;
            }
            case -1481455415: {
                i = (s.equals("CLEAR_ALL_TOAST_AND_CURRENT")) ? 34 : -1;
                break;
            }
            case -1527103628: {
                i = (s.equals("TRAFFIC_REROUTE")) ? 21 : -1;
                break;
            }
            case -1614526796: {
                i = (s.equals("OBD_LOW_FUEL_LEVEL")) ? 40 : -1;
                break;
            }
            case -1735435934: {
                i = (s.equals("CLEAR_TRAFFIC_REROUTE")) ? 22 : -1;
                break;
            }
            case -1802230586: {
                i = (s.equals("CLEAR_TRAFFIC_INCIDENT")) ? 26 : -1;
                break;
            }
            case -1814512122: {
                i = (s.equals("CLEAR_OBD_LOW_FUEL_LEVEL")) ? 41 : -1;
                break;
            }
            case -1827661852: {
                i = (s.equals("DIAL_FORGOTTEN_MULTIPLE")) ? 13 : -1;
                break;
            }
            case -2039247981: {
                i = (s.equals("DIAL_BATTERY_LOW")) ? 14 : -1;
                break;
            }
            default: {
                i = -1;
            }
        }
        switch(i) {
            case 44: {
                Throwable a0 = null;
                label1: {
                    boolean b = false;
                    try {
                        b = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized();
                    } catch(Throwable a1) {
                        a0 = a1;
                        break label1;
                    }
                    try {
                        if (b) {
                            long j = android.os.SystemClock.elapsedRealtime();
                            com.navdy.hud.app.framework.fuel.FuelRoutingManager.getInstance().findNearestGasStation((com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnNearestGasStationCallback)new com.navdy.hud.app.service.CustomNotificationServiceHandler$2(this, j));
                            break;
                        } else {
                            sLogger.v("FIND_ROUTE_TO_CLOSEST_GAS_STATION: here maps not initialized");
                            break;
                        }
                    } catch(Throwable a2) {
                        a0 = a2;
                    }
                }
                sLogger.e("FIND_ROUTE_TO_CLOSEST_GAS_STATION", a0);
                break;
            }
            case 43: {
                Throwable a3 = null;
                label0: {
                    boolean b0 = false;
                    try {
                        b0 = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized();
                    } catch(Throwable a4) {
                        a3 = a4;
                        break label0;
                    }
                    try {
                        if (b0) {
                            long j0 = android.os.SystemClock.elapsedRealtime();
                            com.navdy.hud.app.maps.here.HerePlacesManager.handleCategoriesRequest(com.navdy.hud.app.framework.fuel.FuelRoutingManager.GAS_CATEGORY, 3, (com.navdy.hud.app.maps.here.HerePlacesManager$OnCategoriesSearchListener)new com.navdy.hud.app.service.CustomNotificationServiceHandler$1(this, j0));
                            break;
                        } else {
                            sLogger.v("FIND_GAS_STATION: here maps not initialized");
                            break;
                        }
                    } catch(Throwable a5) {
                        a3 = a5;
                    }
                }
                sLogger.e("FIND_GAS_STATION", a3);
                break;
            }
            case 42: {
                this.bus.post(new com.navdy.hud.app.framework.fuel.FuelRoutingManager$FuelAddedTestEvent());
                break;
            }
            case 41: {
                this.bus.post(new com.navdy.hud.app.framework.fuel.FuelRoutingManager$ClearTestObdLowFuelLevel());
                break;
            }
            case 40: {
                this.bus.post(new com.navdy.hud.app.framework.fuel.FuelRoutingManager$TestObdLowFuelLevel());
                break;
            }
            case 39: {
                com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(com.navdy.hud.app.framework.fuel.FuelRoutingManager.LOW_FUEL_ID);
                break;
            }
            case 38: {
                java.util.ArrayList a6 = new java.util.ArrayList();
                ((java.util.List)a6).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.FuelConstants.FUEL_LEVEL.name(), "14"));
                com.navdy.service.library.events.glances.GlanceEvent a7 = new com.navdy.service.library.events.glances.GlanceEvent$Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent$GlanceType.GLANCE_TYPE_FUEL).id(java.util.UUID.randomUUID().toString()).postTime(Long.valueOf(System.currentTimeMillis())).provider("com.navdy.fuel").glanceData((java.util.List)a6).build();
                this.bus.post(a7);
                break;
            }
            case 37: {
                if (!com.navdy.hud.app.framework.glance.GlanceHelper.isFuelNotificationEnabled()) {
                    break;
                }
                java.util.ArrayList a8 = new java.util.ArrayList();
                ((java.util.List)a8).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.FuelConstants.FUEL_LEVEL.name(), "14"));
                com.navdy.service.library.events.glances.GlanceEvent a9 = new com.navdy.service.library.events.glances.GlanceEvent$Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent$GlanceType.GLANCE_TYPE_FUEL).id(java.util.UUID.randomUUID().toString()).postTime(Long.valueOf(System.currentTimeMillis())).provider("com.navdy.fuel").glanceData((java.util.List)a8).build();
                this.bus.post(a9);
                break;
            }
            case 36: {
                long j1 = System.currentTimeMillis();
                java.util.ArrayList a10 = new java.util.ArrayList();
                ((java.util.List)a10).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TITLE.name(), "Huddle"));
                ((java.util.List)a10).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TIME.name(), String.valueOf(j1)));
                ((java.util.List)a10).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TIME_STR.name(), "1:30 pm - 1:45 pm"));
                com.navdy.service.library.events.glances.GlanceEvent a11 = new com.navdy.service.library.events.glances.GlanceEvent$Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent$GlanceType.GLANCE_TYPE_CALENDAR).id(java.util.UUID.randomUUID().toString()).postTime(Long.valueOf(j1)).provider("com.apple.mobilecal").glanceData((java.util.List)a10).build();
                this.bus.post(a11);
                break;
            }
            case 35: {
                long j2 = System.currentTimeMillis();
                long j3 = java.util.concurrent.TimeUnit.MINUTES.toMillis(10L);
                java.util.ArrayList a12 = new java.util.ArrayList();
                ((java.util.List)a12).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TITLE.name(), "Meeting with Obama"));
                ((java.util.List)a12).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TIME_STR.name(), "\u200e\u202a12:00 \u2013 12:10 PM\u202c\u200e"));
                ((java.util.List)a12).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_LOCATION.name(), "575 7th Street, San Francisco, CA 94103"));
                com.navdy.service.library.events.glances.GlanceEvent a13 = new com.navdy.service.library.events.glances.GlanceEvent$Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent$GlanceType.GLANCE_TYPE_CALENDAR).id(java.util.UUID.randomUUID().toString()).postTime(Long.valueOf(j2 + j3)).provider("com.google.android.calendar").glanceData((java.util.List)a12).build();
                this.bus.post(a13);
                break;
            }
            case 34: {
                com.navdy.hud.app.framework.toast.ToastManager.getInstance().clearAllPendingToast();
                com.navdy.hud.app.framework.toast.ToastManager.getInstance().dismissCurrentToast();
                break;
            }
            case 33: {
                com.navdy.hud.app.framework.toast.ToastManager.getInstance().clearAllPendingToast();
                break;
            }
            case 32: {
                com.navdy.hud.app.framework.toast.ToastManager.getInstance().dismissCurrentToast();
                break;
            }
            case 31: {
                com.navdy.hud.app.framework.connection.ConnectionNotification.showDisconnectedToast(true);
                break;
            }
            case 30: {
                com.navdy.hud.app.framework.connection.ConnectionNotification.showConnectedToast();
                break;
            }
            case 29: {
                com.navdy.hud.app.framework.connection.ConnectionNotification.showDisconnectedToast(false);
                break;
            }
            case 28: {
                this.bus.post(new com.navdy.hud.app.maps.MapEvents$TrafficDelayDismissEvent());
                break;
            }
            case 27: {
                this.bus.post(new com.navdy.hud.app.maps.MapEvents$TrafficDelayEvent(420L));
                break;
            }
            case 26: {
                this.bus.post(new com.navdy.hud.app.maps.MapEvents$LiveTrafficDismissEvent());
                break;
            }
            case 25: {
                this.bus.post(new com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent(com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent$Type.INCIDENT, com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent$Severity.HIGH));
                break;
            }
            case 24: {
                this.bus.post(new com.navdy.hud.app.maps.MapEvents$TrafficJamDismissEvent());
                break;
            }
            case 23: {
                this.bus.post(new com.navdy.hud.app.maps.MapEvents$TrafficJamProgressEvent(900));
                break;
            }
            case 22: {
                this.bus.post(new com.navdy.hud.app.maps.MapEvents$TrafficRerouteDismissEvent());
                break;
            }
            case 21: {
                this.bus.post(new com.navdy.hud.app.maps.MapEvents$TrafficRerouteEvent("Heaven", "Heaven, Hell", 1500L, System.currentTimeMillis() + 300000L, 10000L, 0L));
                break;
            }
            case 20: {
                this.bus.post(new com.navdy.service.library.events.ui.ShowScreen$Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_BRIGHTNESS).build());
                break;
            }
            case 19: {
                this.bus.post(new com.navdy.service.library.events.ui.ShowScreen$Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_VOICE_CONTROL).build());
                break;
            }
            case 18: {
                this.bus.post(new com.navdy.service.library.events.ui.ShowScreen$Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_MUSIC).build());
                break;
            }
            case 17: {
                com.navdy.hud.app.device.dial.DialNotification.dismissAllBatteryToasts();
                break;
            }
            case 16: {
                com.navdy.hud.app.device.dial.DialNotification.showVeryLowBatteryToast();
                break;
            }
            case 15: {
                com.navdy.hud.app.device.dial.DialNotification.showExtremelyLowBatteryToast();
                break;
            }
            case 14: {
                com.navdy.hud.app.device.dial.DialNotification.showLowBatteryToast();
                break;
            }
            case 13: {
                com.navdy.hud.app.device.dial.DialNotification.showForgottenToast(true, "Navdy Dial (AAAA), Navdy Dial (BBBB)");
                break;
            }
            case 12: {
                com.navdy.hud.app.device.dial.DialNotification.showForgottenToast(false, "Navdy Dial (test)");
                break;
            }
            case 11: {
                com.navdy.hud.app.device.dial.DialNotification.showDisconnectedToast("Navdy Dial (test)");
                break;
            }
            case 10: {
                com.navdy.hud.app.device.dial.DialNotification.showConnectedToast();
                break;
            }
            case 9: {
                java.util.ArrayList a14 = new java.util.ArrayList(1);
                ((java.util.List)a14).add(com.navdy.service.library.events.glances.GlanceEvent$GlanceActions.REPLY);
                java.util.ArrayList a15 = new java.util.ArrayList();
                ((java.util.List)a15).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM.name(), "Barack Obama"));
                ((java.util.List)a15).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM_NUMBER.name(), "9999999999"));
                ((java.util.List)a15).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_BODY.name(), "Hi how are u doing"));
                ((java.util.List)a15).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_IS_SMS.name(), ""));
                com.navdy.service.library.events.glances.GlanceEvent a16 = new com.navdy.service.library.events.glances.GlanceEvent$Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent$GlanceType.GLANCE_TYPE_MESSAGE).id(java.util.UUID.randomUUID().toString()).postTime(Long.valueOf(System.currentTimeMillis())).provider("com.navdy.sms").actions((java.util.List)a14).glanceData((java.util.List)a15).build();
                this.bus.post(a16);
                break;
            }
            case 8: {
                java.util.ArrayList a17 = new java.util.ArrayList();
                ((java.util.List)a17).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM.name(), "Santa Singh"));
                ((java.util.List)a17).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM_NUMBER.name(), "999-999-0999"));
                ((java.util.List)a17).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_BODY.name(), "Hi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doinghow are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doinghow are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing"));
                ((java.util.List)a17).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_IS_SMS.name(), ""));
                com.navdy.service.library.events.glances.GlanceEvent a18 = new com.navdy.service.library.events.glances.GlanceEvent$Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent$GlanceType.GLANCE_TYPE_MESSAGE).id(java.util.UUID.randomUUID().toString()).postTime(Long.valueOf(System.currentTimeMillis())).provider("com.navdy.sms").glanceData((java.util.List)a17).build();
                this.bus.post(a18);
                break;
            }
            case 7: {
                java.util.ArrayList a19 = new java.util.ArrayList(1);
                ((java.util.List)a19).add(com.navdy.service.library.events.glances.GlanceEvent$GlanceActions.REPLY);
                java.util.ArrayList a20 = new java.util.ArrayList();
                ((java.util.List)a20).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM.name(), "John Doe"));
                ((java.util.List)a20).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM_NUMBER.name(), "4081111111"));
                ((java.util.List)a20).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_BODY.name(), "Hi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing"));
                ((java.util.List)a20).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_IS_SMS.name(), ""));
                com.navdy.service.library.events.glances.GlanceEvent a21 = new com.navdy.service.library.events.glances.GlanceEvent$Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent$GlanceType.GLANCE_TYPE_MESSAGE).id(java.util.UUID.randomUUID().toString()).postTime(Long.valueOf(System.currentTimeMillis())).provider("com.navdy.sms").actions((java.util.List)a19).glanceData((java.util.List)a20).build();
                this.bus.post(a21);
                break;
            }
            case 6: {
                this.bus.post(new com.navdy.service.library.events.callcontrol.PhoneBatteryStatus(com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$BatteryStatus.BATTERY_OK, Integer.valueOf(36), Boolean.valueOf(false)));
                break;
            }
            case 5: {
                this.bus.post(new com.navdy.service.library.events.callcontrol.PhoneBatteryStatus(com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$BatteryStatus.BATTERY_EXTREMELY_LOW, Integer.valueOf(4), Boolean.valueOf(false)));
                break;
            }
            case 4: {
                this.bus.post(new com.navdy.service.library.events.callcontrol.PhoneBatteryStatus(com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$BatteryStatus.BATTERY_LOW, Integer.valueOf(12), Boolean.valueOf(false)));
                break;
            }
            case 3: {
                this.bus.post(new com.navdy.service.library.events.callcontrol.PhoneEvent$Builder().number("323-222-1111").status(com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_IDLE).contact_name("Al Jazeera").build());
                break;
            }
            case 2: {
                this.bus.post(new com.navdy.service.library.events.callcontrol.PhoneEvent$Builder().number("323-222-1111").status(com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_RINGING).contact_name("Al Jazeera Al Bin Al Salaad").build());
                break;
            }
            case 1: {
                java.util.ArrayList a22 = new java.util.ArrayList();
                ((java.util.List)a22).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.GenericConstants.GENERIC_TITLE.name(), "Jone Doe"));
                ((java.util.List)a22).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.GenericConstants.GENERIC_MESSAGE.name(), "Message Test"));
                com.navdy.service.library.events.glances.GlanceEvent a23 = new com.navdy.service.library.events.glances.GlanceEvent$Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent$GlanceType.GLANCE_TYPE_GENERIC).id(java.util.UUID.randomUUID().toString()).postTime(Long.valueOf(System.currentTimeMillis())).provider("blah").glanceData((java.util.List)a22).build();
                this.bus.post(a23);
                break;
            }
            case 0: {
                java.util.ArrayList a24 = new java.util.ArrayList();
                ((java.util.List)a24).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.GenericConstants.GENERIC_TITLE.name(), "John Doe"));
                ((java.util.List)a24).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.GenericConstants.GENERIC_MESSAGE.name(), "Message Test"));
                ((java.util.List)a24).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.GenericConstants.GENERIC_MAIN_ICON.name(), com.navdy.service.library.events.glances.GlanceIconConstants.GLANCE_ICON_NAVDY_MAIN.name()));
                ((java.util.List)a24).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.GenericConstants.GENERIC_SIDE_ICON.name(), com.navdy.service.library.events.glances.GlanceIconConstants.GLANCE_ICON_MESSAGE_SIDE_BLUE.name()));
                com.navdy.service.library.events.glances.GlanceEvent a25 = new com.navdy.service.library.events.glances.GlanceEvent$Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent$GlanceType.GLANCE_TYPE_GENERIC).id(java.util.UUID.randomUUID().toString()).postTime(Long.valueOf(System.currentTimeMillis())).provider("blah").glanceData((java.util.List)a24).build();
                this.bus.post(a25);
                break;
            }
        }
    }
    
    public void start() {
    }
}
