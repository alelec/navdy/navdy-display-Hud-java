package com.navdy.hud.app.framework.places;

class NearbyPlaceSearchNotification$3$1 implements Runnable {
    final com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$3 this$1;
    final java.util.List val$destinations;
    
    NearbyPlaceSearchNotification$3$1(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$3 a, java.util.List a0) {
        super();
        this.this$1 = a;
        this.val$destinations = a0;
    }
    
    public void run() {
        if (com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.access$000(this.this$1.this$0) != null) {
            com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.access$700(this.this$1.this$0).removeCallbacks(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.access$600(this.this$1.this$0));
            com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.access$100(this.this$1.this$0);
            if (this.val$destinations.size() <= 0) {
                com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.access$900(this.this$1.this$0);
            } else {
                com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.access$800(this.this$1.this$0, this.val$destinations);
            }
        }
    }
}
