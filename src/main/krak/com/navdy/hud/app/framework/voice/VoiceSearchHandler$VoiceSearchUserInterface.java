package com.navdy.hud.app.framework.voice;

abstract public interface VoiceSearchHandler$VoiceSearchUserInterface {
    abstract public void close();
    
    
    abstract public void onVoiceSearchResponse(com.navdy.service.library.events.audio.VoiceSearchResponse arg);
}
