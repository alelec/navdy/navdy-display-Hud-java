package com.navdy.hud.app.framework.glance;
import com.navdy.hud.app.R;

class GlanceNotification$1 implements Runnable {
    final com.navdy.hud.app.framework.glance.GlanceNotification this$0;
    
    GlanceNotification$1(com.navdy.hud.app.framework.glance.GlanceNotification a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        com.navdy.hud.app.framework.notifications.INotificationController a = com.navdy.hud.app.framework.glance.GlanceNotification.access$000(this.this$0);
        label5: {
            Throwable a0 = null;
            if (a == null) {
                break label5;
            }
            label0: {
                label4: {
                    label1: {
                        label3: try {
                            boolean b = false;
                            if (com.navdy.hud.app.framework.glance.GlanceNotification.access$100(this.this$0) == null) {
                                break label4;
                            }
                            android.widget.TextView a1 = (android.widget.TextView)com.navdy.hud.app.framework.glance.GlanceNotification.access$100(this.this$0).findViewById(R.id.title);
                            switch(com.navdy.hud.app.framework.glance.GlanceNotification$5.$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceNotification.access$200(this.this$0).ordinal()]) {
                                case 9: case 10: case 11: case 12: case 13: case 14: case 15: case 16: case 17: case 18: case 19: {
                                    a1.setText((CharSequence)com.navdy.hud.app.framework.glance.GlanceHelper.getTimeStr(System.currentTimeMillis(), com.navdy.hud.app.framework.glance.GlanceNotification.access$800(this.this$0), com.navdy.hud.app.framework.glance.GlanceNotification.access$900(this.this$0)));
                                    break label3;
                                }
                                case 8: {
                                    b = com.navdy.hud.app.framework.glance.GlanceNotification.access$400(this.this$0);
                                    break;
                                }
                                case 1: case 2: case 3: {
                                    com.navdy.hud.app.framework.glance.GlanceNotification.access$300(this.this$0);
                                    break label3;
                                }
                                default: {
                                    break label3;
                                }
                            }
                            label2: {
                                if (!b) {
                                    break label2;
                                }
                                int i = com.navdy.hud.app.obd.ObdManager.getInstance().getFuelLevel();
                                if (i > Integer.parseInt((String)com.navdy.hud.app.framework.glance.GlanceNotification.access$500(this.this$0).get(com.navdy.service.library.events.glances.FuelConstants.FUEL_LEVEL.name()))) {
                                    break label1;
                                }
                                com.navdy.hud.app.framework.glance.GlanceNotification.access$500(this.this$0).put(com.navdy.service.library.events.glances.FuelConstants.FUEL_LEVEL.name(), String.valueOf(i));
                            }
                            com.navdy.hud.app.framework.fuel.FuelRoutingManager a2 = com.navdy.hud.app.framework.fuel.FuelRoutingManager.getInstance();
                            if (a2 != null && a2.getCurrentGasStation() != null) {
                                double d = (double)Math.round(a2.getCurrentGasStation().getLocation().getCoordinate().distanceTo(com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLastGeoPosition().getCoordinate()) * 10.0 / 1609.34) / 10.0;
                                com.navdy.hud.app.framework.glance.GlanceNotification.access$500(this.this$0).put(com.navdy.service.library.events.glances.FuelConstants.GAS_STATION_DISTANCE.name(), String.valueOf(d));
                            }
                            com.navdy.hud.app.framework.glance.GlanceNotification.access$600(this.this$0);
                            com.navdy.hud.app.framework.glance.GlanceNotification.access$700(this.this$0, (android.view.View)com.navdy.hud.app.framework.glance.GlanceNotification.access$100(this.this$0));
                        } catch(Throwable a3) {
                            a0 = a3;
                            break label0;
                        }
                        com.navdy.hud.app.framework.glance.GlanceNotification.access$1200().postDelayed(com.navdy.hud.app.framework.glance.GlanceNotification.access$1100(this.this$0), 30000L);
                        break label5;
                    }
                    com.navdy.hud.app.framework.glance.GlanceNotification.access$1200().postDelayed(com.navdy.hud.app.framework.glance.GlanceNotification.access$1100(this.this$0), 30000L);
                    break label5;
                }
                com.navdy.hud.app.framework.glance.GlanceNotification.access$1200().postDelayed(com.navdy.hud.app.framework.glance.GlanceNotification.access$1100(this.this$0), 30000L);
                break label5;
            }
            try {
                com.navdy.hud.app.framework.glance.GlanceNotification.access$1000().e(a0);
            } catch(Throwable a4) {
                com.navdy.hud.app.framework.glance.GlanceNotification.access$1200().postDelayed(com.navdy.hud.app.framework.glance.GlanceNotification.access$1100(this.this$0), 30000L);
                throw a4;
            }
            com.navdy.hud.app.framework.glance.GlanceNotification.access$1200().postDelayed(com.navdy.hud.app.framework.glance.GlanceNotification.access$1100(this.this$0), 30000L);
        }
    }
}
