package com.navdy.hud.app.framework.connection;

class ConnectionNotification$3$1 implements Runnable {
    final com.navdy.hud.app.framework.connection.ConnectionNotification$3 this$0;
    
    ConnectionNotification$3$1(com.navdy.hud.app.framework.connection.ConnectionNotification$3 a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        if (this.this$0.val$driverImage.getTag() == this.this$0.val$tag) {
            this.this$0.val$driverImage.setInitials((String)null, com.navdy.hud.app.ui.component.image.InitialsImageView$Style.DEFAULT);
            com.navdy.hud.app.util.picasso.PicassoUtil.getInstance().load(this.this$0.val$imagePath).fit().transform(com.navdy.hud.app.framework.connection.ConnectionNotification.access$200()).into((android.widget.ImageView)this.this$0.val$driverImage);
        }
    }
}
