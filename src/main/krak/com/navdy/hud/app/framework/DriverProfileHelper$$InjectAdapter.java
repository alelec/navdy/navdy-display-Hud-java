package com.navdy.hud.app.framework;

final public class DriverProfileHelper$$InjectAdapter extends dagger.internal.Binding implements dagger.MembersInjector {
    private dagger.internal.Binding driverProfileManager;
    
    public DriverProfileHelper$$InjectAdapter() {
        super((String)null, "members/com.navdy.hud.app.framework.DriverProfileHelper", false, com.navdy.hud.app.framework.DriverProfileHelper.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.driverProfileManager = a.requestBinding("com.navdy.hud.app.profile.DriverProfileManager", com.navdy.hud.app.framework.DriverProfileHelper.class, (this).getClass().getClassLoader());
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.driverProfileManager);
    }
    
    public void injectMembers(com.navdy.hud.app.framework.DriverProfileHelper a) {
        a.driverProfileManager = (com.navdy.hud.app.profile.DriverProfileManager)this.driverProfileManager.get();
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.framework.DriverProfileHelper)a);
    }
}
