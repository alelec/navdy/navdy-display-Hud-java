package com.navdy.hud.app.framework.glance;

class GlanceTracker {
    final private static long NOTIFICATION_SEEN_THRESHOLD;
    final private static com.navdy.service.library.log.Logger sLogger;
    private java.util.HashMap notificationSavedState;
    private java.util.HashMap notificationSeen;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.glance.GlanceTracker.class);
        NOTIFICATION_SEEN_THRESHOLD = java.util.concurrent.TimeUnit.MINUTES.toMillis(60L);
    }
    
    GlanceTracker() {
        this.notificationSeen = new java.util.HashMap();
    }
    
    private boolean isNotificationSame(com.navdy.hud.app.framework.glance.GlanceTracker$GlanceInfo a, com.navdy.hud.app.framework.glance.GlanceApp a0, java.util.Map a1) {
        boolean b = false;
        if (a1.size() == a.data.size()) {
            Object a2 = a.data.entrySet().iterator();
            Object a3 = a1;
            while(true) {
                if (((java.util.Iterator)a2).hasNext()) {
                    Object a4 = ((java.util.Iterator)a2).next();
                    String s = (String)((java.util.Map)a3).get(((java.util.Map.Entry)a4).getKey());
                    if (s != null) {
                        if (android.text.TextUtils.equals((CharSequence)s, (CharSequence)((java.util.Map.Entry)a4).getValue())) {
                            continue;
                        }
                        b = false;
                        break;
                    } else {
                        b = false;
                        break;
                    }
                } else {
                    b = true;
                    break;
                }
            }
        } else {
            b = false;
        }
        return b;
    }
    
    void clearState() {
        synchronized(this) {
            com.navdy.service.library.log.Logger a = sLogger;
            a.v("clearState");
            this.notificationSavedState = null;
        }
        /*monexit(this)*/;
    }
    
    long isNotificationSeen(com.navdy.service.library.events.glances.GlanceEvent a, com.navdy.hud.app.framework.glance.GlanceApp a0, java.util.Map a1) {
        Throwable a2 = null;
        label0: synchronized(this) {
        int[] a3 = com.navdy.hud.app.framework.glance.GlanceTracker$1.$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp;
            int i = 0;
            try {
                i = a3[a0.ordinal()];
            } catch(Throwable a4) {
                a2 = a4;
                break label0;
            }
            long j = 0L;
            switch(i) {
                case 1: case 2: case 3: case 4: case 5: case 6: case 7: {
                    try {
                        Object a5 = this.notificationSeen.get(a0);
                        if (a5 != null) {
                            java.util.Iterator a6 = ((java.util.List)a5).iterator();
                            Object a7 = a1;
                            Object a8 = a6;
                            while(true) {
                                if (((java.util.Iterator)a8).hasNext()) {
                                    com.navdy.hud.app.framework.glance.GlanceTracker$GlanceInfo a9 = (com.navdy.hud.app.framework.glance.GlanceTracker$GlanceInfo)((java.util.Iterator)a8).next();
                                    if (android.os.SystemClock.elapsedRealtime() - a9.time > NOTIFICATION_SEEN_THRESHOLD) {
                                        ((java.util.Iterator)a8).remove();
                                        sLogger.v(new StringBuilder().append("expired notification removed [").append(a0).append("]").toString());
                                        continue;
                                    }
                                    if (!this.isNotificationSame(a9, a0, (java.util.Map)a7)) {
                                        continue;
                                    }
                                    j = a9.time;
                                    a9.time = android.os.SystemClock.elapsedRealtime();
                                    sLogger.v(new StringBuilder().append("notification has been seen [").append(a0).append("] time [").append(j).append("]").toString());
                                    break;
                                } else {
                                    ((java.util.List)a5).add(new com.navdy.hud.app.framework.glance.GlanceTracker$GlanceInfo(android.os.SystemClock.elapsedRealtime(), a, (java.util.Map)a7));
                                    sLogger.v(new StringBuilder().append("new notification [").append(a0).append("]").toString());
                                    j = 0L;
                                    break;
                                }
                            }
                        } else {
                            java.util.ArrayList a10 = new java.util.ArrayList();
                            ((java.util.List)a10).add(new com.navdy.hud.app.framework.glance.GlanceTracker$GlanceInfo(android.os.SystemClock.elapsedRealtime(), a, a1));
                            this.notificationSeen.put(a0, a10);
                            sLogger.v(new StringBuilder().append("first notification[").append(a0).append("]").toString());
                            j = 0L;
                        }
                    } catch(Throwable a11) {
                        a2 = a11;
                        break;
                    }
                }
                default: {
                    /*monexit(this)*/;
                    return j;
                }
            }
        }
        /*monexit(this)*/;
        throw a2;
    }
    
    void restoreState() {
        synchronized(this) {
            com.navdy.service.library.log.Logger a = sLogger;
            a.v("restoreState");
            this.notificationSeen = this.notificationSavedState;
            this.notificationSavedState = null;
            if (this.notificationSeen == null) {
                this.notificationSeen = new java.util.HashMap();
            }
        }
        /*monexit(this)*/;
    }
    
    void saveState() {
        synchronized(this) {
            com.navdy.service.library.log.Logger a = sLogger;
            a.v("saveState");
            this.notificationSavedState = this.notificationSeen;
            this.notificationSeen = new java.util.HashMap();
        }
        /*monexit(this)*/;
    }
}
