package com.navdy.hud.app.framework.music;

@Layout(R.layout.screen_music_details)
public class MusicDetailsScreen extends com.navdy.hud.app.screen.BaseScreen {
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.music.MusicDetailsScreen.class);
    }
    
    public MusicDetailsScreen() {
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    public int getAnimationIn(flow.Flow$Direction a) {
        return -1;
    }
    
    public int getAnimationOut(flow.Flow$Direction a) {
        return -1;
    }
    
    public Object getDaggerModule() {
        return new com.navdy.hud.app.framework.music.MusicDetailsScreen$Module(this);
    }
    
    public String getMortarScopeName() {
        return (this).getClass().getName();
    }
    
    public com.navdy.service.library.events.ui.Screen getScreen() {
        return com.navdy.service.library.events.ui.Screen.SCREEN_MUSIC_DETAILS;
    }
}
