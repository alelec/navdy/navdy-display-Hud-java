package com.navdy.hud.app.framework.glympse;
import com.navdy.hud.app.R;

public class GlympseNotification implements com.navdy.hud.app.framework.notifications.INotification, com.navdy.hud.app.ui.component.ChoiceLayout2$IListener {
    final private static float NOTIFICATION_ALERT_BADGE_SCALE = 0.95f;
    final private static float NOTIFICATION_BADGE_SCALE = 0.5f;
    final private static float NOTIFICATION_DEFAULT_ICON_SCALE = 1f;
    final private static float NOTIFICATION_ICON_SCALE = 1.38f;
    final private static int NOTIFICATION_TIMEOUT = 5000;
    final private static int alertColor;
    final private static java.util.List choicesOnlyDismiss;
    final private static java.util.List choicesRetryAndDismiss;
    final private static android.os.Handler handler;
    final private static com.navdy.service.library.log.Logger logger;
    final private static android.content.res.Resources resources;
    final private static int shareLocationTripColor;
    @InjectView(R.id.badge)
    android.widget.ImageView badge;
    @InjectView(R.id.badge_icon)
    com.navdy.hud.app.ui.component.image.IconColorImageView badgeIcon;
    @InjectView(R.id.choice_layout)
    com.navdy.hud.app.ui.component.ChoiceLayout2 choiceLayout;
    final private com.navdy.hud.app.framework.contacts.Contact contact;
    private com.navdy.hud.app.framework.notifications.INotificationController controller;
    final private String destinationLabel;
    final private double latitude;
    final private double longitude;
    final private String message;
    final private String notifId;
    @InjectView(R.id.notification_icon)
    com.navdy.hud.app.ui.component.image.IconColorImageView notificationIcon;
    @InjectView(R.id.notification_user_image)
    android.widget.ImageView notificationUserImage;
    private boolean retrySendingMessage;
    @InjectView(R.id.subtitle)
    android.widget.TextView subtitle;
    @InjectView(R.id.title)
    android.widget.TextView title;
    final private com.navdy.hud.app.framework.glympse.GlympseNotification$Type type;
    final private String uuid;
    
    static {
        logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.glympse.GlympseNotification.class);
        resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        handler = new android.os.Handler(android.os.Looper.getMainLooper());
        int i = resources.getColor(R.color.glance_dismiss);
        com.navdy.hud.app.ui.component.ChoiceLayout2$Choice a = new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(R.id.dismiss, R.drawable.icon_glances_dismiss, i, R.drawable.icon_glances_dismiss, -16777216, resources.getString(R.string.dismiss), i);
        choicesOnlyDismiss = (java.util.List)new java.util.ArrayList();
        choicesOnlyDismiss.add(a);
        int i0 = resources.getColor(R.color.glance_ok_blue);
        choicesRetryAndDismiss = (java.util.List)new java.util.ArrayList();
        choicesRetryAndDismiss.add(new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(R.id.retry, R.drawable.icon_glances_retry, i0, R.drawable.icon_glances_retry, -16777216, resources.getString(R.string.retry), i0));
        choicesRetryAndDismiss.add(a);
        shareLocationTripColor = resources.getColor(R.color.share_location_trip_color);
        alertColor = resources.getColor(R.color.share_location_trip_alert_color);
    }
    
    GlympseNotification(com.navdy.hud.app.framework.contacts.Contact a, com.navdy.hud.app.framework.glympse.GlympseNotification$Type a0) {
        this(a, a0, (String)null, (String)null, 0.0, 0.0);
    }
    
    public GlympseNotification(com.navdy.hud.app.framework.contacts.Contact a, com.navdy.hud.app.framework.glympse.GlympseNotification$Type a0, String s, String s0, double d, double d0) {
        this.contact = a;
        this.type = a0;
        this.message = s;
        this.destinationLabel = s0;
        this.latitude = d;
        this.longitude = d0;
        this.uuid = java.util.UUID.randomUUID().toString();
        this.notifId = new StringBuilder().append("navdy#glympse#notif#").append(a0.name()).append("#").append(this.uuid).toString();
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return logger;
    }
    
    static com.navdy.hud.app.framework.contacts.Contact access$100(com.navdy.hud.app.framework.glympse.GlympseNotification a) {
        return a.contact;
    }
    
    static String access$200(com.navdy.hud.app.framework.glympse.GlympseNotification a) {
        return a.message;
    }
    
    static String access$300(com.navdy.hud.app.framework.glympse.GlympseNotification a) {
        return a.destinationLabel;
    }
    
    static double access$400(com.navdy.hud.app.framework.glympse.GlympseNotification a) {
        return a.latitude;
    }
    
    static double access$500(com.navdy.hud.app.framework.glympse.GlympseNotification a) {
        return a.longitude;
    }
    
    static void access$600(String s, com.navdy.hud.app.framework.contacts.Contact a, String s0, double d, double d0) {
        com.navdy.hud.app.framework.glympse.GlympseNotification.addGlympseOfflineGlance(s, a, s0, d, d0);
    }
    
    private static void addGlympseOfflineGlance(String s, com.navdy.hud.app.framework.contacts.Contact a, String s0, double d, double d0) {
        com.navdy.hud.app.framework.glympse.GlympseNotification a0 = new com.navdy.hud.app.framework.glympse.GlympseNotification(a, com.navdy.hud.app.framework.glympse.GlympseNotification$Type.OFFLINE, s, s0, d, d0);
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().addNotification((com.navdy.hud.app.framework.notifications.INotification)a0);
    }
    
    private String getContactName() {
        return (android.text.TextUtils.isEmpty((CharSequence)this.contact.name)) ? (android.text.TextUtils.isEmpty((CharSequence)this.contact.formattedNumber)) ? "" : this.contact.formattedNumber : this.contact.name;
    }
    
    private void setOfflineUI() {
        this.title.setText((CharSequence)resources.getString(R.string.sending_failed));
        this.subtitle.setText((CharSequence)resources.getString(R.string.offline));
        this.notificationIcon.setIcon(R.drawable.icon_message, shareLocationTripColor, (android.graphics.Shader)null, 1.38f);
        this.notificationIcon.setVisibility(0);
        this.badgeIcon.setIcon(R.drawable.icon_msg_alert, alertColor, (android.graphics.Shader)null, 0.95f);
        this.badgeIcon.setVisibility(0);
        this.choiceLayout.setChoices(choicesRetryAndDismiss, 0, (com.navdy.hud.app.ui.component.ChoiceLayout2$IListener)this);
    }
    
    private void setReadUI() {
        this.title.setText((CharSequence)this.getContactName());
        boolean b = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized();
        label1: {
            label0: {
                if (!b) {
                    break label0;
                }
                if (!com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().isNavigationModeOn()) {
                    break label0;
                }
                this.subtitle.setText((CharSequence)resources.getString(R.string.viewed_your_trip));
                break label1;
            }
            this.subtitle.setText((CharSequence)resources.getString(R.string.viewed_your_location));
        }
        android.graphics.Bitmap a = com.navdy.hud.app.util.picasso.PicassoUtil.getBitmapfromCache(com.navdy.hud.app.framework.contacts.PhoneImageDownloader.getInstance().getImagePath(this.contact.number, com.navdy.service.library.events.photo.PhotoType.PHOTO_CONTACT));
        if (a == null) {
            com.navdy.hud.app.framework.contacts.ContactImageHelper a0 = com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance();
            this.notificationIcon.setIcon(a0.getResourceId(this.contact.defaultImageIndex), a0.getResourceColor(this.contact.defaultImageIndex), (android.graphics.Shader)null, 1f);
            this.notificationIcon.setVisibility(0);
        } else {
            this.notificationUserImage.setImageBitmap(a);
            this.notificationUserImage.setVisibility(0);
        }
        this.badgeIcon.setIcon(R.drawable.icon_message, shareLocationTripColor, (android.graphics.Shader)null, 0.5f);
        this.badgeIcon.setVisibility(0);
        this.choiceLayout.setChoices(choicesOnlyDismiss, 0, (com.navdy.hud.app.ui.component.ChoiceLayout2$IListener)this);
    }
    
    private void setSentUI() {
        boolean b = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized();
        label1: {
            label0: {
                if (!b) {
                    break label0;
                }
                if (!com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().isNavigationModeOn()) {
                    break label0;
                }
                this.title.setText((CharSequence)resources.getString(R.string.trip_sent));
                break label1;
            }
            this.title.setText((CharSequence)resources.getString(R.string.location_sent));
        }
        android.widget.TextView a = this.subtitle;
        android.content.res.Resources a0 = resources;
        Object[] a1 = new Object[1];
        a1[0] = this.getContactName();
        a.setText((CharSequence)a0.getString(R.string.to_contact_name, a1));
        this.notificationIcon.setIcon(R.drawable.icon_message, shareLocationTripColor, (android.graphics.Shader)null, 1.38f);
        this.notificationIcon.setVisibility(0);
        this.badge.setImageResource(R.drawable.icon_msg_success);
        this.badge.setVisibility(0);
        this.choiceLayout.setChoices(choicesOnlyDismiss, 0, (com.navdy.hud.app.ui.component.ChoiceLayout2$IListener)this);
    }
    
    public boolean canAddToStackIfCurrentExists() {
        return true;
    }
    
    public void executeItem(com.navdy.hud.app.ui.component.ChoiceLayout2$Selection a) {
        if (a.id != R.id.retry) {
            if (a.id == R.id.dismiss) {
                com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(this.notifId);
            }
        } else {
            this.retrySendingMessage = true;
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(this.notifId);
        }
    }
    
    public boolean expandNotification() {
        return false;
    }
    
    public int getColor() {
        return 0;
    }
    
    public android.view.View getExpandedView(android.content.Context a, Object a0) {
        return null;
    }
    
    public int getExpandedViewIndicatorColor() {
        return 0;
    }
    
    public String getId() {
        return this.notifId;
    }
    
    public int getTimeout() {
        return (this.type != com.navdy.hud.app.framework.glympse.GlympseNotification$Type.OFFLINE) ? 5000 : 0;
    }
    
    public com.navdy.hud.app.framework.notifications.NotificationType getType() {
        return com.navdy.hud.app.framework.notifications.NotificationType.GLYMPSE;
    }
    
    public android.view.View getView(android.content.Context a) {
        android.view.View a0 = android.view.LayoutInflater.from(a).inflate(R.layout.notification_glympse, (android.view.ViewGroup)null);
        butterknife.ButterKnife.inject(this, a0);
        switch(com.navdy.hud.app.framework.glympse.GlympseNotification$2.$SwitchMap$com$navdy$hud$app$framework$glympse$GlympseNotification$Type[this.type.ordinal()]) {
            case 3: {
                this.setOfflineUI();
                break;
            }
            case 2: {
                this.setReadUI();
                break;
            }
            case 1: {
                this.setSentUI();
                break;
            }
        }
        return a0;
    }
    
    public android.animation.AnimatorSet getViewSwitchAnimation(boolean b) {
        return null;
    }
    
    public boolean isAlive() {
        return false;
    }
    
    public boolean isPurgeable() {
        return false;
    }
    
    public void itemSelected(com.navdy.hud.app.ui.component.ChoiceLayout2$Selection a) {
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        return null;
    }
    
    public void onClick() {
    }
    
    public void onExpandedNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager$Mode a) {
    }
    
    public void onExpandedNotificationSwitched() {
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        return false;
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        boolean b = false;
        if (this.controller != null) {
            switch(com.navdy.hud.app.framework.glympse.GlympseNotification$2.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[a.ordinal()]) {
                case 3: {
                    this.choiceLayout.executeSelectedItem();
                    b = true;
                    break;
                }
                case 2: {
                    this.choiceLayout.moveSelectionRight();
                    if (this.type != com.navdy.hud.app.framework.glympse.GlympseNotification$Type.OFFLINE) {
                        this.controller.resetTimeout();
                    }
                    b = true;
                    break;
                }
                case 1: {
                    this.choiceLayout.moveSelectionLeft();
                    if (this.type != com.navdy.hud.app.framework.glympse.GlympseNotification$Type.OFFLINE) {
                        this.controller.resetTimeout();
                    }
                    b = true;
                    break;
                }
                default: {
                    b = false;
                }
            }
        } else {
            b = false;
        }
        return b;
    }
    
    public void onNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager$Mode a) {
    }
    
    public void onStart(com.navdy.hud.app.framework.notifications.INotificationController a) {
        this.controller = a;
    }
    
    public void onStop() {
        this.controller = null;
        if (this.retrySendingMessage) {
            handler.post((Runnable)new com.navdy.hud.app.framework.glympse.GlympseNotification$1(this));
        }
    }
    
    public void onTrackHand(float f) {
    }
    
    public void onUpdate() {
    }
    
    public boolean supportScroll() {
        return false;
    }
}
