package com.navdy.hud.app.framework.music;
import com.navdy.hud.app.R;

public class AlbumArtImageView extends android.widget.ImageView {
    final private static int ARTWORK_TRANSITION_DURATION = 1000;
    private static com.navdy.service.library.log.Logger logger;
    final private Object artworkLock;
    private android.graphics.drawable.BitmapDrawable defaultArtwork;
    private android.os.Handler handler;
    private java.util.concurrent.atomic.AtomicBoolean isAnimatingArtwork;
    private boolean mask;
    private android.graphics.Bitmap nextArtwork;
    private String nextArtworkHash;
    private android.graphics.Paint paint;
    private android.graphics.RadialGradient radialGradient;
    
    static {
        logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.music.AlbumArtImageView.class);
    }
    
    public AlbumArtImageView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public AlbumArtImageView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public AlbumArtImageView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.isAnimatingArtwork = new java.util.concurrent.atomic.AtomicBoolean(false);
        this.artworkLock = new Object();
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.init(a0);
    }
    
    static Object access$000(com.navdy.hud.app.framework.music.AlbumArtImageView a) {
        return a.artworkLock;
    }
    
    static String access$100(com.navdy.hud.app.framework.music.AlbumArtImageView a) {
        return a.nextArtworkHash;
    }
    
    static java.util.concurrent.atomic.AtomicBoolean access$200(com.navdy.hud.app.framework.music.AlbumArtImageView a) {
        return a.isAnimatingArtwork;
    }
    
    static void access$300(com.navdy.hud.app.framework.music.AlbumArtImageView a) {
        a.animateArtwork();
    }
    
    private android.graphics.Bitmap addMask(android.graphics.Bitmap a) {
        android.graphics.Bitmap a0 = null;
        if (a != null) {
            int i = a.getWidth();
            int i0 = a.getHeight();
            a0 = android.graphics.Bitmap.createBitmap(i, i0, android.graphics.Bitmap.Config.ARGB_8888);
            android.graphics.Canvas a1 = new android.graphics.Canvas(a0);
            a1.drawBitmap(a, 0.0f, 0.0f, (android.graphics.Paint)null);
            this.paint.setShader((android.graphics.Shader)this.radialGradient);
            this.paint.setXfermode((android.graphics.Xfermode)new android.graphics.PorterDuffXfermode(android.graphics.PorterDuff$Mode.MULTIPLY));
            a1.drawRect(0.0f, 0.0f, (float)i, (float)i0, this.paint);
        } else {
            a0 = null;
        }
        return a0;
    }
    
    private void animateArtwork() {
        String s = null;
        android.graphics.Bitmap a = null;
        synchronized(this.artworkLock) {
            s = this.nextArtworkHash;
            a = this.nextArtwork;
            /*monexit(a0)*/;
        }
        android.graphics.drawable.Drawable[] a5 = new android.graphics.drawable.Drawable[2];
        android.graphics.drawable.Drawable a6 = this.getDrawable();
        if (a6 != null) {
            a5[0] = ((android.graphics.drawable.LayerDrawable)a6).getDrawable(0);
        } else {
            a5[0] = new android.graphics.drawable.ColorDrawable(0);
        }
        if (a == null) {
            logger.i("Bitmap image is null");
            a5[1] = this.defaultArtwork;
        } else {
            a5[1] = new android.graphics.drawable.BitmapDrawable(this.getContext().getResources(), a);
        }
        android.graphics.drawable.TransitionDrawable a7 = new android.graphics.drawable.TransitionDrawable(a5);
        this.setImageDrawable((android.graphics.drawable.Drawable)a7);
        a7.setCrossFadeEnabled(true);
        a7.startTransition(1000);
        this.handler.postDelayed((Runnable)new com.navdy.hud.app.framework.music.AlbumArtImageView$1(this, a5, s), 1000L);
    }
    
    private void drawImmediately() {
        this.isAnimatingArtwork.set(false);
        this.handler.removeCallbacksAndMessages(null);
        android.graphics.drawable.Drawable[] a = new android.graphics.drawable.Drawable[1];
        if (this.nextArtwork == null) {
            logger.i("Bitmap image is null");
            a[0] = this.defaultArtwork;
        } else {
            a[0] = new android.graphics.drawable.BitmapDrawable(this.getContext().getResources(), this.nextArtwork);
        }
        this.setImageDrawable((android.graphics.drawable.Drawable)new android.graphics.drawable.LayerDrawable(a));
    }
    
    private void init(android.util.AttributeSet a) {
        int i = 0;
        if (a == null) {
            i = R.drawable.icon_mm_music_control_blank;
        } else {
            android.content.res.TypedArray a0 = this.getContext().obtainStyledAttributes(a, com.navdy.hud.app.R$styleable.AlbumArtImageView);
            this.mask = a0.getBoolean(0, false);
            i = a0.getResourceId(1, R.drawable.icon_mm_music_control_blank);
            a0.recycle();
        }
        android.graphics.drawable.BitmapDrawable a1 = (android.graphics.drawable.BitmapDrawable)this.getResources().getDrawable(i);
        if (this.mask) {
            this.paint = new android.graphics.Paint();
            int[] a2 = this.getResources().getIntArray(R.array.smart_dash_music_gauge_gradient);
            android.content.Context a3 = this.getContext();
            int[] a4 = new int[1];
            a4[0] = 16842996;
            android.content.res.TypedArray a5 = a3.obtainStyledAttributes(a, a4);
            float f = (float)(a5.getDimensionPixelSize(0, 130) / 2);
            a5.recycle();
            this.radialGradient = new android.graphics.RadialGradient(f, f, f, a2, (float[])null, android.graphics.Shader$TileMode.CLAMP);
            this.defaultArtwork = new android.graphics.drawable.BitmapDrawable(this.getResources(), this.addMask(a1.getBitmap()));
        } else {
            this.defaultArtwork = a1;
        }
        android.graphics.drawable.Drawable[] a6 = new android.graphics.drawable.Drawable[1];
        a6[0] = this.defaultArtwork;
        this.setImageDrawable((android.graphics.drawable.Drawable)new android.graphics.drawable.LayerDrawable(a6));
    }
    
    public void setArtworkBitmap(android.graphics.Bitmap a, boolean b) {
        Throwable a0 = null;
        String s = com.navdy.service.library.util.IOUtils.hashForBitmap(a);
        boolean b0 = android.text.TextUtils.equals((CharSequence)this.nextArtworkHash, (CharSequence)s);
        label0: {
            if (b0) {
                logger.d("Already set this artwork, ignoring");
            } else {
                synchronized(this) {
                    this.nextArtworkHash = s;
                    if (this.mask) {
                        a = this.addMask(a);
                    }
                    this.nextArtwork = a;
                    /*monexit(this)*/;
                }
                if (b) {
                    if (this.isAnimatingArtwork.compareAndSet(false, true)) {
                        this.animateArtwork();
                    } else {
                        logger.d("Already animating");
                    }
                } else {
                    this.drawImmediately();
                }
            }
            return;
        }
        while(true) {
            try {
                /*monexit(this)*/;
            } catch(IllegalMonitorStateException | NullPointerException a2) {
                Throwable a3 = a2;
                a0 = a3;
                continue;
            }
            throw a0;
        }
    }
}
