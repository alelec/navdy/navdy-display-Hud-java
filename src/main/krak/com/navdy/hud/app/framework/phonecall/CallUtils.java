package com.navdy.hud.app.framework.phonecall;

public class CallUtils {
    private static com.navdy.hud.app.framework.phonecall.CallManager callManager;
    
    public CallUtils() {
    }
    
    public static boolean isPhoneCallInProgress() {
        boolean b = false;
        synchronized(com.navdy.hud.app.framework.phonecall.CallUtils.class) {
            com.navdy.hud.app.framework.phonecall.CallManager a = callManager;
            if (a == null) {
                callManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getCallManager();
            }
            b = callManager.isCallInProgress();
        }
        /*monexit(com.navdy.hud.app.framework.phonecall.CallUtils.class)*/;
        return b;
    }
}
