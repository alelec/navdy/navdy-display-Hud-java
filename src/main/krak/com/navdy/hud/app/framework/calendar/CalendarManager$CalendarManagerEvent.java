package com.navdy.hud.app.framework.calendar;


public enum CalendarManager$CalendarManagerEvent {
    UPDATED(0);

    private int value;
    CalendarManager$CalendarManagerEvent(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class CalendarManager$CalendarManagerEvent extends Enum {
//    final private static com.navdy.hud.app.framework.calendar.CalendarManager$CalendarManagerEvent[] $VALUES;
//    final public static com.navdy.hud.app.framework.calendar.CalendarManager$CalendarManagerEvent UPDATED;
//    
//    static {
//        UPDATED = new com.navdy.hud.app.framework.calendar.CalendarManager$CalendarManagerEvent("UPDATED", 0);
//        com.navdy.hud.app.framework.calendar.CalendarManager$CalendarManagerEvent[] a = new com.navdy.hud.app.framework.calendar.CalendarManager$CalendarManagerEvent[1];
//        a[0] = UPDATED;
//        $VALUES = a;
//    }
//    
//    private CalendarManager$CalendarManagerEvent(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.framework.calendar.CalendarManager$CalendarManagerEvent valueOf(String s) {
//        return (com.navdy.hud.app.framework.calendar.CalendarManager$CalendarManagerEvent)Enum.valueOf(com.navdy.hud.app.framework.calendar.CalendarManager$CalendarManagerEvent.class, s);
//    }
//    
//    public static com.navdy.hud.app.framework.calendar.CalendarManager$CalendarManagerEvent[] values() {
//        return $VALUES.clone();
//    }
//}
//