package com.navdy.hud.app.framework.connection;

final class ConnectionNotification$3 implements Runnable {
    final com.navdy.hud.app.ui.component.image.InitialsImageView val$driverImage;
    final java.io.File val$imagePath;
    final com.navdy.hud.app.profile.DriverProfile val$profile;
    final String val$tag;
    
    ConnectionNotification$3(java.io.File a, com.navdy.hud.app.ui.component.image.InitialsImageView a0, String s, com.navdy.hud.app.profile.DriverProfile a1) {
        super();
        this.val$imagePath = a;
        this.val$driverImage = a0;
        this.val$tag = s;
        this.val$profile = a1;
    }
    
    public void run() {
        if (this.val$imagePath.exists()) {
            com.navdy.hud.app.framework.connection.ConnectionNotification.access$300().post((Runnable)new com.navdy.hud.app.framework.connection.ConnectionNotification$3$1(this));
        } else if (this.val$profile != null) {
            java.io.File a = this.val$profile.getDriverImageFile();
            if (a != null && !a.exists()) {
                com.navdy.hud.app.framework.connection.ConnectionNotification.access$400().d("photo not available locally");
                com.navdy.hud.app.framework.contacts.PhoneImageDownloader.getInstance().submitDownload("DriverImage", com.navdy.hud.app.framework.contacts.PhoneImageDownloader$Priority.NORMAL, com.navdy.service.library.events.photo.PhotoType.PHOTO_DRIVER_PROFILE, (String)null);
            }
        }
    }
}
