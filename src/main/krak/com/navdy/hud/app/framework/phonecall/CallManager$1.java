package com.navdy.hud.app.framework.phonecall;

class CallManager$1 implements Runnable {
    final com.navdy.hud.app.framework.phonecall.CallManager this$0;
    
    CallManager$1(com.navdy.hud.app.framework.phonecall.CallManager a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        com.navdy.hud.app.framework.phonecall.CallManager.access$000().v(new StringBuilder().append("no response for call action:").append(this.this$0.lastCallAction).toString());
        if (this.this$0.lastCallAction != null && com.navdy.hud.app.framework.phonecall.CallManager.access$100(this.this$0, this.this$0.lastCallAction)) {
            switch(com.navdy.hud.app.framework.phonecall.CallManager$3.$SwitchMap$com$navdy$service$library$events$callcontrol$CallAction[this.this$0.lastCallAction.ordinal()]) {
                case 3: {
                    this.this$0.state = com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState.FAILED;
                    break;
                }
                case 2: {
                    this.this$0.state = com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState.IDLE;
                    break;
                }
                case 1: {
                    this.this$0.state = com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState.FAILED;
                    break;
                }
            }
            this.this$0.userRejectedCall = false;
            this.this$0.userCancelledCall = false;
            this.this$0.dialSet = false;
            if (com.navdy.hud.app.framework.phonecall.CallManager.access$200(this.this$0)) {
                this.this$0.sendNotification();
            }
        }
    }
}
