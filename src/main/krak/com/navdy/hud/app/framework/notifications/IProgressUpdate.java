package com.navdy.hud.app.framework.notifications;

abstract public interface IProgressUpdate {
    abstract public void onPosChange(int arg);
}
