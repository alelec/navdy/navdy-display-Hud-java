package com.navdy.hud.app.framework.voice;

class VoiceSearchNotification$SinusoidalInterpolator implements android.view.animation.Interpolator {
    VoiceSearchNotification$SinusoidalInterpolator() {
    }
    
    public float getInterpolation(float f) {
        return (float)(Math.sin((double)f * 3.141592653589793 - 1.5707963267948966) * 0.5 + 0.5);
    }
}
