package com.navdy.hud.app.framework.contacts;

class PhoneImageDownloader$4 implements Runnable {
    final com.navdy.hud.app.framework.contacts.PhoneImageDownloader this$0;
    final java.io.File val$file;
    final String val$id;
    final com.navdy.service.library.events.photo.PhotoType val$photoType;
    
    PhoneImageDownloader$4(com.navdy.hud.app.framework.contacts.PhoneImageDownloader a, java.io.File a0, String s, com.navdy.service.library.events.photo.PhotoType a1) {
        super();
        this.this$0 = a;
        this.val$file = a0;
        this.val$id = s;
        this.val$photoType = a1;
    }
    
    public void run() {
        if (com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.framework.contacts.PhoneImageDownloader.access$700(this.this$0), this.val$file.getAbsolutePath())) {
            com.navdy.hud.app.framework.contacts.PhoneImageDownloader.access$100().v(new StringBuilder().append("removed [").append(this.val$id).append("]").toString());
        }
        com.navdy.hud.app.framework.contacts.PhoneImageDownloader.access$500(this.this$0).post(new com.navdy.hud.app.framework.contacts.PhoneImageDownloader$PhotoDownloadStatus(this.val$id, this.val$photoType, false, false));
    }
}
