package com.navdy.hud.app.framework.message;

public class MessageManager {
    final private static String EMPTY = "";
    final private static com.navdy.hud.app.framework.message.MessageManager sInstance;
    final public static com.navdy.service.library.log.Logger sLogger;
    private com.squareup.otto.Bus bus;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.message.MessageManager.class);
        sInstance = new com.navdy.hud.app.framework.message.MessageManager();
    }
    
    private MessageManager() {
        this.bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
        this.bus.register(this);
    }
    
    public static com.navdy.hud.app.framework.message.MessageManager getInstance() {
        return sInstance;
    }
    
    public void onMessage(com.navdy.service.library.events.notification.NotificationEvent a) {
        sLogger.v("old notification --> convert to glance");
        if (a.sourceIdentifier != null) {
            String s = null;
            boolean b = false;
            com.navdy.service.library.events.notification.NotificationCategory a0 = a.category;
            com.navdy.service.library.events.notification.NotificationCategory a1 = com.navdy.service.library.events.notification.NotificationCategory.CATEGORY_SOCIAL;
            com.navdy.hud.app.framework.recentcall.RecentCall a2 = null;
            if (a0 == a1) {
                a2 = new com.navdy.hud.app.framework.recentcall.RecentCall((String)null, com.navdy.hud.app.framework.recentcall.RecentCall$Category.MESSAGE, a.sourceIdentifier, com.navdy.hud.app.framework.contacts.NumberType.OTHER, new java.util.Date(), com.navdy.hud.app.framework.recentcall.RecentCall$CallType.INCOMING, -1, 0L);
                com.navdy.hud.app.framework.recentcall.RecentCallManager.getInstance().handleNewCall(a2);
            }
            String s0 = (android.text.TextUtils.isEmpty((CharSequence)a.title)) ? a.sourceIdentifier : a.title;
            String s1 = a.message;
            boolean b0 = Boolean.TRUE.equals(a.cannotReplyBack);
            if (com.navdy.hud.app.framework.contacts.ContactUtil.isValidNumber(a.sourceIdentifier)) {
                s = a.sourceIdentifier;
                b = true;
            } else {
                s = null;
                if (a2 == null) {
                    b = false;
                } else {
                    boolean b1 = com.navdy.hud.app.framework.contacts.ContactUtil.isValidNumber(a2.number);
                    s = null;
                    if (b1) {
                        s = a2.number;
                        b = true;
                    } else {
                        b = false;
                    }
                }
            }
            if (!b) {
                b0 = true;
            }
            String s2 = com.navdy.hud.app.framework.glance.GlanceHelper.getId();
            java.util.ArrayList a3 = new java.util.ArrayList();
            ((java.util.List)a3).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM.name(), s0));
            java.util.ArrayList a4 = null;
            if (s != null) {
                ((java.util.List)a3).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM_NUMBER.name(), s));
                a4 = null;
                if (!b0) {
                    a4 = new java.util.ArrayList(1);
                    ((java.util.List)a4).add(com.navdy.service.library.events.glances.GlanceEvent$GlanceActions.REPLY);
                    ((java.util.List)a3).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_IS_SMS.name(), s0));
                }
            }
            ((java.util.List)a3).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_BODY.name(), s1));
            com.navdy.service.library.events.glances.GlanceEvent$Builder a5 = new com.navdy.service.library.events.glances.GlanceEvent$Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent$GlanceType.GLANCE_TYPE_MESSAGE).provider("com.navdy.sms").id(s2).postTime(Long.valueOf(System.currentTimeMillis())).glanceData((java.util.List)a3);
            if (a4 != null) {
                a5.actions((java.util.List)a4);
            }
            com.navdy.service.library.events.glances.GlanceEvent a6 = a5.build();
            this.bus.post(a6);
        } else {
            sLogger.w(new StringBuilder().append("Ignoring notification with null sourceIdentifier").append(a).toString());
        }
    }
}
