package com.navdy.hud.app.framework.phonecall;

class CallNotification$3 implements com.navdy.hud.app.framework.toast.IToastCallback {
    com.navdy.hud.app.framework.contacts.ContactUtil$IContactCallback cb;
    final com.navdy.hud.app.framework.phonecall.CallNotification this$0;
    com.navdy.hud.app.ui.component.image.InitialsImageView toastImage;
    com.navdy.hud.app.view.ToastView toastView;
    final String val$caller;
    final String val$number;
    
    CallNotification$3(com.navdy.hud.app.framework.phonecall.CallNotification a, String s, String s0) {
        super();
        this.this$0 = a;
        this.val$caller = s;
        this.val$number = s0;
        this.cb = (com.navdy.hud.app.framework.contacts.ContactUtil$IContactCallback)new com.navdy.hud.app.framework.phonecall.CallNotification$3$1(this);
    }
    
    public void executeChoiceItem(int i, int i0) {
        switch(i0) {
            case 102: {
                if (com.navdy.hud.app.framework.phonecall.CallNotification.access$400(this.this$0).state == com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState.RINGING) {
                    com.navdy.hud.app.framework.phonecall.CallNotification.access$400(this.this$0).sendCallAction(com.navdy.service.library.events.callcontrol.CallAction.CALL_REJECT, (String)null);
                }
                com.navdy.hud.app.framework.toast.ToastManager.getInstance().dismissCurrentToast("incomingcall#toast");
                break;
            }
            case 101: {
                if (com.navdy.hud.app.framework.phonecall.CallNotification.access$400(this.this$0).state == com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState.RINGING) {
                    com.navdy.hud.app.framework.phonecall.CallNotification.access$400(this.this$0).sendCallAction(com.navdy.service.library.events.callcontrol.CallAction.CALL_ACCEPT, (String)null);
                }
                com.navdy.hud.app.framework.toast.ToastManager.getInstance().dismissCurrentToast("incomingcall#toast");
                break;
            }
        }
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        return false;
    }
    
    public void onPhotoDownload(com.navdy.hud.app.framework.contacts.PhoneImageDownloader$PhotoDownloadStatus a) {
        if (this.toastView != null && this.toastImage != null && !a.alreadyDownloaded && a.photoType == com.navdy.service.library.events.photo.PhotoType.PHOTO_CONTACT && com.navdy.hud.app.framework.phonecall.CallNotification.access$400(this.this$0).phoneStatus != com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_IDLE && android.text.TextUtils.equals((CharSequence)this.val$number, (CharSequence)a.sourceIdentifier)) {
            com.navdy.hud.app.framework.contacts.ContactUtil.setContactPhoto(this.val$caller, this.val$number, false, this.toastImage, com.navdy.hud.app.framework.phonecall.CallNotification.access$1500(this.this$0), this.cb);
        }
    }
    
    public void onStart(com.navdy.hud.app.view.ToastView a) {
        this.toastView = a;
        this.toastImage = a.getMainLayout().screenImage;
        this.toastImage.setVisibility(0);
        com.navdy.hud.app.framework.phonecall.CallNotification.access$1400(this.this$0).register(this);
        com.navdy.hud.app.framework.contacts.ContactUtil.setContactPhoto(this.val$caller, this.val$number, true, this.toastImage, com.navdy.hud.app.framework.phonecall.CallNotification.access$1500(this.this$0), this.cb);
    }
    
    public void onStop() {
        com.navdy.hud.app.framework.phonecall.CallNotification.access$1400(this.this$0).unregister(this);
        this.toastView = null;
        this.toastImage = null;
    }
}
