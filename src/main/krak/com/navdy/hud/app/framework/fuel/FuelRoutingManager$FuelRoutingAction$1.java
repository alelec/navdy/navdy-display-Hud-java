package com.navdy.hud.app.framework.fuel;

class FuelRoutingManager$FuelRoutingAction$1 implements com.navdy.hud.app.maps.here.HereRouteCalculator$RouteCalculatorListener {
    final com.navdy.hud.app.framework.fuel.FuelRoutingManager$FuelRoutingAction this$1;
    
    FuelRoutingManager$FuelRoutingAction$1(com.navdy.hud.app.framework.fuel.FuelRoutingManager$FuelRoutingAction a) {
        super();
        this.this$1 = a;
    }
    
    public void error(com.here.android.mpa.routing.RoutingError a, Throwable a0) {
        com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$000().w(new StringBuilder().append("Calculated route to ").append(com.navdy.hud.app.framework.fuel.FuelRoutingManager$FuelRoutingAction.access$1400(this.this$1).getName()).append(" with error ").append(a.name()).toString());
        com.navdy.hud.app.framework.fuel.FuelRoutingManager$FuelRoutingAction.access$1602(this.this$1, com.navdy.hud.app.framework.fuel.FuelRoutingManager$FuelRoutingAction.access$1600(this.this$1) - 1);
        if (com.navdy.hud.app.framework.fuel.FuelRoutingManager$FuelRoutingAction.access$1500(this.this$1).size() != com.navdy.hud.app.framework.fuel.FuelRoutingManager$FuelRoutingAction.access$1600(this.this$1)) {
            com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$1300(this.this$1.this$0).post((Runnable)com.navdy.hud.app.framework.fuel.FuelRoutingManager$FuelRoutingAction.access$1900(this.this$1).poll());
        } else {
            com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$1800(this.this$1.this$0, com.navdy.hud.app.framework.fuel.FuelRoutingManager$FuelRoutingAction.access$1500(this.this$1), com.navdy.hud.app.framework.fuel.FuelRoutingManager$FuelRoutingAction.access$1700(this.this$1));
        }
    }
    
    public void postSuccess(java.util.ArrayList a) {
        if (com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$000().isLoggable(2)) {
            com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$000().v(new StringBuilder().append("Calculated route to ").append(com.navdy.hud.app.framework.fuel.FuelRoutingManager$FuelRoutingAction.access$1400(this.this$1).getName()).append(" with no errors").toString());
        }
        com.navdy.service.library.events.navigation.NavigationRouteResult a0 = (com.navdy.service.library.events.navigation.NavigationRouteResult)a.get(0);
        com.navdy.hud.app.framework.fuel.FuelRoutingManager$FuelRoutingAction.access$1500(this.this$1).add(new com.navdy.hud.app.framework.fuel.FuelRoutingManager$RouteCacheItem(a0, com.navdy.hud.app.framework.fuel.FuelRoutingManager$FuelRoutingAction.access$1400(this.this$1)));
        if (com.navdy.hud.app.framework.fuel.FuelRoutingManager$FuelRoutingAction.access$1500(this.this$1).size() != com.navdy.hud.app.framework.fuel.FuelRoutingManager$FuelRoutingAction.access$1600(this.this$1)) {
            com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$1300(this.this$1.this$0).post((Runnable)com.navdy.hud.app.framework.fuel.FuelRoutingManager$FuelRoutingAction.access$1900(this.this$1).poll());
        } else {
            com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$1800(this.this$1.this$0, com.navdy.hud.app.framework.fuel.FuelRoutingManager$FuelRoutingAction.access$1500(this.this$1), com.navdy.hud.app.framework.fuel.FuelRoutingManager$FuelRoutingAction.access$1700(this.this$1));
        }
    }
    
    public void preSuccess() {
    }
    
    public void progress(int i) {
        if (com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$000().isLoggable(2)) {
            com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$000().v(new StringBuilder().append("Calculating route to ").append(com.navdy.hud.app.framework.fuel.FuelRoutingManager$FuelRoutingAction.access$1400(this.this$1).getName()).append("; progress %: ").append(i).toString());
        }
    }
}
