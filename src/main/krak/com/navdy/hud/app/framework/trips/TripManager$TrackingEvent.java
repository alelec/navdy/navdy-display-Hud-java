package com.navdy.hud.app.framework.trips;

public class TripManager$TrackingEvent {
    final private String chosenDestinationId;
    final private int distanceToDestination;
    final private int estimatedTimeRemaining;
    final private com.here.android.mpa.common.GeoPosition geoPosition;
    
    public TripManager$TrackingEvent(com.here.android.mpa.common.GeoPosition a) {
        this.geoPosition = a;
        this.chosenDestinationId = null;
        this.estimatedTimeRemaining = -1;
        this.distanceToDestination = -1;
    }
    
    public TripManager$TrackingEvent(com.here.android.mpa.common.GeoPosition a, String s, int i, int i0) {
        this.geoPosition = a;
        this.chosenDestinationId = s;
        this.estimatedTimeRemaining = i;
        this.distanceToDestination = i0;
    }
    
    static int access$1700(com.navdy.hud.app.framework.trips.TripManager$TrackingEvent a) {
        return a.distanceToDestination;
    }
    
    static int access$1800(com.navdy.hud.app.framework.trips.TripManager$TrackingEvent a) {
        return a.estimatedTimeRemaining;
    }
    
    static String access$1900(com.navdy.hud.app.framework.trips.TripManager$TrackingEvent a) {
        return a.chosenDestinationId;
    }
    
    static com.here.android.mpa.common.GeoPosition access$200(com.navdy.hud.app.framework.trips.TripManager$TrackingEvent a) {
        return a.geoPosition;
    }
    
    public boolean isRouteTracking() {
        boolean b = false;
        if (com.navdy.hud.app.framework.trips.TripManager.access$300().isLoggable(2)) {
            com.navdy.hud.app.framework.trips.TripManager.access$300().v(new StringBuilder().append("[routeTracking] chosenDestinationId=").append(this.chosenDestinationId).append("; estimatedTimeRemaining=").append(this.estimatedTimeRemaining).append("; distanceToDestination=").append(this.distanceToDestination).toString());
        }
        String s = this.chosenDestinationId;
        label2: {
            label0: {
                label1: {
                    if (s == null) {
                        break label1;
                    }
                    if (this.estimatedTimeRemaining < 0) {
                        break label1;
                    }
                    if (this.distanceToDestination >= 0) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
}
