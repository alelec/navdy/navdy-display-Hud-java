package com.navdy.hud.app.framework;

public class DriverProfileHelper {
    final private static com.navdy.hud.app.framework.DriverProfileHelper sInstance;
    @Inject
    com.navdy.hud.app.profile.DriverProfileManager driverProfileManager;
    
    static {
        sInstance = new com.navdy.hud.app.framework.DriverProfileHelper();
    }
    
    private DriverProfileHelper() {
        mortar.Mortar.inject(com.navdy.hud.app.HudApplication.getAppContext(), this);
    }
    
    public static com.navdy.hud.app.framework.DriverProfileHelper getInstance() {
        return sInstance;
    }
    
    public java.util.Locale getCurrentLocale() {
        return this.driverProfileManager.getCurrentLocale();
    }
    
    public com.navdy.hud.app.profile.DriverProfile getCurrentProfile() {
        return this.driverProfileManager.getCurrentProfile();
    }
    
    public com.navdy.hud.app.profile.DriverProfileManager getDriverProfileManager() {
        return this.driverProfileManager;
    }
}
