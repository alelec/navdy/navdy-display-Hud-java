package com.navdy.hud.app.framework.music;
import com.navdy.hud.app.R;

public class MusicDetailsView extends android.widget.RelativeLayout implements com.navdy.hud.app.manager.InputManager$IInputHandler {
    final private static int musicRightContainerLeftMargin;
    final private static int musicRightContainerSize;
    final private static com.navdy.service.library.log.Logger sLogger;
    android.widget.ImageView albumArt;
    android.view.ViewGroup albumArtContainer;
    private com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent$Callback callback;
    android.widget.TextView counter;
    @Inject
    public com.navdy.hud.app.framework.music.MusicDetailsScreen$Presenter presenter;
    com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vmenuComponent;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.music.MusicDetailsView.class);
        android.content.res.Resources a = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        musicRightContainerSize = a.getDimensionPixelSize(R.dimen.music_details_rightC_w);
        musicRightContainerLeftMargin = a.getDimensionPixelSize(R.dimen.music_details_rightC_left_margin);
    }
    
    public MusicDetailsView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public MusicDetailsView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public MusicDetailsView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.callback = (com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent$Callback)new com.navdy.hud.app.framework.music.MusicDetailsView$1(this);
        if (!this.isInEditMode()) {
            mortar.Mortar.inject(a, this);
        }
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        return com.navdy.hud.app.manager.InputManager.nextContainingHandler((android.view.View)this);
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.vmenuComponent.clear();
        if (this.presenter != null) {
            this.presenter.dropView((android.view.View)this);
        }
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View)this);
        this.vmenuComponent = new com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent((android.view.ViewGroup)this, this.callback, true);
        this.vmenuComponent.leftContainer.setAlpha(0.0f);
        this.vmenuComponent.rightContainer.setAlpha(0.0f);
        android.view.ViewGroup$MarginLayoutParams a = (android.view.ViewGroup$MarginLayoutParams)this.vmenuComponent.rightContainer.getLayoutParams();
        a.width = musicRightContainerSize;
        a.leftMargin = musicRightContainerLeftMargin;
        android.view.LayoutInflater a0 = android.view.LayoutInflater.from(this.getContext());
        this.vmenuComponent.leftContainer.removeAllViews();
        this.albumArtContainer = (android.view.ViewGroup)a0.inflate(R.layout.music_details_album_art, (android.view.ViewGroup)this, false);
        this.albumArt = (android.widget.ImageView)this.albumArtContainer.findViewById(R.id.albumArt);
        this.counter = (android.widget.TextView)this.albumArtContainer.findViewById(R.id.counter);
        this.albumArt.setImageDrawable((android.graphics.drawable.Drawable)new android.graphics.drawable.ColorDrawable(-12303292));
        this.vmenuComponent.leftContainer.addView((android.view.View)this.albumArtContainer);
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        return this.vmenuComponent.handleGesture(a);
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        return this.vmenuComponent.handleKey(a);
    }
    
    void performSelectionAnimation(Runnable a, int i) {
        this.vmenuComponent.performSelectionAnimation(a, i);
    }
}
