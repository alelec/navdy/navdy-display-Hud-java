package com.navdy.hud.app.framework.notifications;

class NotificationManager$10 implements Runnable {
    final com.navdy.hud.app.framework.notifications.NotificationManager this$0;
    final Runnable val$endAction;
    final android.view.View val$large;
    final com.navdy.hud.app.framework.notifications.NotificationManager$Info val$notifInfo;
    final android.view.View val$notifViewOut;
    final android.view.ViewGroup val$notifViewOutParent;
    
    NotificationManager$10(com.navdy.hud.app.framework.notifications.NotificationManager a, com.navdy.hud.app.framework.notifications.NotificationManager$Info a0, android.view.View a1, android.view.ViewGroup a2, android.view.View a3, Runnable a4) {
        super();
        this.this$0 = a;
        this.val$notifInfo = a0;
        this.val$notifViewOut = a1;
        this.val$notifViewOutParent = a2;
        this.val$large = a3;
        this.val$endAction = a4;
    }
    
    public void run() {
        if (this.val$notifInfo != null) {
            android.animation.AnimatorSet a = this.val$notifInfo.notification.getViewSwitchAnimation(true);
            if (a != null) {
                a.setDuration(100L);
                a.start();
            }
        }
        if (this.val$notifViewOut != null && com.navdy.hud.app.framework.glance.GlanceHelper.isDeleteAllView(this.val$notifViewOut)) {
            this.val$notifViewOutParent.removeView(this.val$notifViewOut);
            com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("delete all small view removed");
            this.val$notifViewOut.setTag(null);
            com.navdy.hud.app.framework.notifications.NotificationManager.access$4200(this.this$0, this.val$notifViewOut);
        }
        if (this.val$large == null) {
            this.val$endAction.run();
        }
    }
}
