package com.navdy.hud.app.framework.fuel;

class FuelRoutingManager$2$1 implements com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback {
    final com.navdy.hud.app.framework.fuel.FuelRoutingManager$2 this$1;
    
    FuelRoutingManager$2$1(com.navdy.hud.app.framework.fuel.FuelRoutingManager$2 a) {
        super();
        this.this$1 = a;
    }
    
    public void onError(com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback$Error a) {
        this.this$1.val$callback.onError(com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback$Error.RESPONSE_ERROR);
    }
    
    public void onOptimalRouteCalculationComplete(com.navdy.hud.app.framework.fuel.FuelRoutingManager$RouteCacheItem a, java.util.ArrayList a0, com.navdy.service.library.events.navigation.NavigationRouteRequest a1) {
        com.navdy.service.library.events.navigation.NavigationRouteResult a2 = (com.navdy.service.library.events.navigation.NavigationRouteResult)a0.get(0);
        this.this$1.val$callback.onComplete(a2);
    }
}
