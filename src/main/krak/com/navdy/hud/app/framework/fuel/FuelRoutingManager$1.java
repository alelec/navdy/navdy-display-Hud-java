package com.navdy.hud.app.framework.fuel;

class FuelRoutingManager$1 implements Runnable {
    final com.navdy.hud.app.framework.fuel.FuelRoutingManager this$0;
    
    FuelRoutingManager$1(com.navdy.hud.app.framework.fuel.FuelRoutingManager a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$000().v("routeToGasStation");
        label2: if (com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$100(this.this$0)) {
            java.util.ArrayList a = com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$200(this.this$0);
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$200(this.this$0).size() != 0) {
                        break label0;
                    }
                }
                com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$000().w("outgoing results not available");
                this.this$0.reset();
                break label2;
            }
            if (com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$300(this.this$0).isNavigationModeOn()) {
                com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$000().v("navon: saving current non-gas route to storage");
                long j = (com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$400(this.this$0)) ? 20L : -1L;
                if (com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$300(this.this$0).hasArrived()) {
                    com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$000().v("navon: already arrived, set ignore flag");
                    com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$300(this.this$0).setIgnoreArrived(true);
                }
                com.here.android.mpa.guidance.NavigationManager$Error a0 = com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$300(this.this$0).addNewRoute(com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$200(this.this$0), com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason.NAV_SESSION_FUEL_REROUTE, com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$500(this.this$0), j);
                if (a0 != com.here.android.mpa.guidance.NavigationManager$Error.NONE) {
                    com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$000().e(new StringBuilder().append("navon:arrival route navigation started failed:").append(a0).toString());
                    this.this$0.reset();
                    com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(com.navdy.hud.app.framework.fuel.FuelRoutingManager.LOW_FUEL_ID);
                } else {
                    com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$000().v("navon:gas route navigation started");
                }
            } else {
                com.navdy.service.library.events.navigation.NavigationRouteResult a1 = (com.navdy.service.library.events.navigation.NavigationRouteResult)com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$200(this.this$0).get(0);
                if (com.navdy.hud.app.maps.here.HereRouteCache.getInstance().getRoute(a1.routeId) != null) {
                    com.navdy.service.library.events.navigation.NavigationSessionRequest a2 = new com.navdy.service.library.events.navigation.NavigationSessionRequest(com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STARTED, a1.label, a1.routeId, Integer.valueOf(0), Boolean.valueOf(true));
                    com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$300(this.this$0).handleNavigationSessionRequest(a2);
                    com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(com.navdy.hud.app.framework.fuel.FuelRoutingManager.LOW_FUEL_ID);
                    com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$000().v("navoff:gas route navigation started");
                } else {
                    com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$000().w(new StringBuilder().append("navoff:route not available in cache:").append(a1.routeId).toString());
                    this.this$0.reset();
                }
            }
        } else {
            com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$000().w("Fuel manager not available");
        }
    }
}
