package com.navdy.hud.app.framework.twilio;

abstract public interface TwilioSmsManager$Callback {
    abstract public void result(com.navdy.hud.app.framework.twilio.TwilioSmsManager$ErrorCode arg);
}
