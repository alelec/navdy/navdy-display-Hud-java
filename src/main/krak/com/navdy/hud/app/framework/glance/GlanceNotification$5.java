package com.navdy.hud.app.framework.glance;

class GlanceNotification$5 {
    final static int[] $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp;
    final static int[] $SwitchMap$com$navdy$hud$app$framework$glance$GlanceViewCache$ViewType;
    final static int[] $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent;
    final static int[] $SwitchMap$com$navdy$service$library$events$audio$SpeechRequestStatus$SpeechRequestStatusType;
    
    static {
        $SwitchMap$com$navdy$service$library$events$audio$SpeechRequestStatus$SpeechRequestStatusType = new int[com.navdy.service.library.events.audio.SpeechRequestStatus$SpeechRequestStatusType.values().length];
        int[] a = $SwitchMap$com$navdy$service$library$events$audio$SpeechRequestStatus$SpeechRequestStatusType;
        com.navdy.service.library.events.audio.SpeechRequestStatus$SpeechRequestStatusType a0 = com.navdy.service.library.events.audio.SpeechRequestStatus$SpeechRequestStatusType.SPEECH_REQUEST_STARTING;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$audio$SpeechRequestStatus$SpeechRequestStatusType[com.navdy.service.library.events.audio.SpeechRequestStatus$SpeechRequestStatusType.SPEECH_REQUEST_STOPPED.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        $SwitchMap$com$navdy$hud$app$framework$glance$GlanceViewCache$ViewType = new int[com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType.values().length];
        int[] a1 = $SwitchMap$com$navdy$hud$app$framework$glance$GlanceViewCache$ViewType;
        com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType a2 = com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType.SMALL_GLANCE_MESSAGE;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceViewCache$ViewType[com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType.BIG_GLANCE_MESSAGE.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceViewCache$ViewType[com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType.BIG_GLANCE_MESSAGE_SINGLE.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException3) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceViewCache$ViewType[com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType.BIG_MULTI_TEXT.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException4) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceViewCache$ViewType[com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType.BIG_CALENDAR.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException5) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceViewCache$ViewType[com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType.SMALL_SIGN.ordinal()] = 6;
        } catch(NoSuchFieldError ignoredException6) {
        }
        $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent = new int[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.values().length];
        int[] a3 = $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent;
        com.navdy.hud.app.manager.InputManager$CustomKeyEvent a4 = com.navdy.hud.app.manager.InputManager$CustomKeyEvent.LEFT;
        try {
            a3[a4.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException7) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.RIGHT.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException8) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.SELECT.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException9) {
        }
        $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp = new int[com.navdy.hud.app.framework.glance.GlanceApp.values().length];
        int[] a5 = $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp;
        com.navdy.hud.app.framework.glance.GlanceApp a6 = com.navdy.hud.app.framework.glance.GlanceApp.GOOGLE_CALENDAR;
        try {
            a5[a6.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException10) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.APPLE_CALENDAR.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException11) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.GENERIC_CALENDAR.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException12) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.GOOGLE_MAIL.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException13) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.APPLE_MAIL.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException14) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.GENERIC_MAIL.ordinal()] = 6;
        } catch(NoSuchFieldError ignoredException15) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.GOOGLE_INBOX.ordinal()] = 7;
        } catch(NoSuchFieldError ignoredException16) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.FUEL.ordinal()] = 8;
        } catch(NoSuchFieldError ignoredException17) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.GOOGLE_HANGOUT.ordinal()] = 9;
        } catch(NoSuchFieldError ignoredException18) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.SLACK.ordinal()] = 10;
        } catch(NoSuchFieldError ignoredException19) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.WHATS_APP.ordinal()] = 11;
        } catch(NoSuchFieldError ignoredException20) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.FACEBOOK_MESSENGER.ordinal()] = 12;
        } catch(NoSuchFieldError ignoredException21) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.FACEBOOK.ordinal()] = 13;
        } catch(NoSuchFieldError ignoredException22) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.GENERIC_MESSAGE.ordinal()] = 14;
        } catch(NoSuchFieldError ignoredException23) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.TWITTER.ordinal()] = 15;
        } catch(NoSuchFieldError ignoredException24) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.GENERIC_SOCIAL.ordinal()] = 16;
        } catch(NoSuchFieldError ignoredException25) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.IMESSAGE.ordinal()] = 17;
        } catch(NoSuchFieldError ignoredException26) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.SMS.ordinal()] = 18;
        } catch(NoSuchFieldError ignoredException27) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.GENERIC.ordinal()] = 19;
        } catch(NoSuchFieldError ignoredException28) {
        }
    }
}
