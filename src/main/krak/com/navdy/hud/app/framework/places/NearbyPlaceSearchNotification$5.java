package com.navdy.hud.app.framework.places;

class NearbyPlaceSearchNotification$5 {
    final static int[] $SwitchMap$com$navdy$hud$app$framework$places$NearbyPlaceSearchNotification$PlaceTypeSearchState;
    final static int[] $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent;
    final static int[] $SwitchMap$com$navdy$service$library$events$RequestStatus;
    final static int[] $SwitchMap$com$navdy$service$library$events$input$Gesture;
    final static int[] $SwitchMap$com$navdy$service$library$events$places$PlaceType;
    
    static {
        $SwitchMap$com$navdy$service$library$events$places$PlaceType = new int[com.navdy.service.library.events.places.PlaceType.values().length];
        int[] a = $SwitchMap$com$navdy$service$library$events$places$PlaceType;
        com.navdy.service.library.events.places.PlaceType a0 = com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_GAS;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$places$PlaceType[com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_PARKING.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$places$PlaceType[com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_RESTAURANT.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$places$PlaceType[com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_STORE.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$places$PlaceType[com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_COFFEE.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException3) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$places$PlaceType[com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_ATM.ordinal()] = 6;
        } catch(NoSuchFieldError ignoredException4) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$places$PlaceType[com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_HOSPITAL.ordinal()] = 7;
        } catch(NoSuchFieldError ignoredException5) {
        }
        $SwitchMap$com$navdy$service$library$events$RequestStatus = new int[com.navdy.service.library.events.RequestStatus.values().length];
        int[] a1 = $SwitchMap$com$navdy$service$library$events$RequestStatus;
        com.navdy.service.library.events.RequestStatus a2 = com.navdy.service.library.events.RequestStatus.REQUEST_SERVICE_ERROR;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException6) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$RequestStatus[com.navdy.service.library.events.RequestStatus.REQUEST_NOT_AVAILABLE.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException7) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$RequestStatus[com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException8) {
        }
        $SwitchMap$com$navdy$hud$app$framework$places$NearbyPlaceSearchNotification$PlaceTypeSearchState = new int[com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$PlaceTypeSearchState.values().length];
        int[] a3 = $SwitchMap$com$navdy$hud$app$framework$places$NearbyPlaceSearchNotification$PlaceTypeSearchState;
        com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$PlaceTypeSearchState a4 = com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$PlaceTypeSearchState.SEARCHING;
        try {
            a3[a4.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException9) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$places$NearbyPlaceSearchNotification$PlaceTypeSearchState[com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$PlaceTypeSearchState.ERROR.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException10) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$places$NearbyPlaceSearchNotification$PlaceTypeSearchState[com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$PlaceTypeSearchState.NO_RESULTS.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException11) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$places$NearbyPlaceSearchNotification$PlaceTypeSearchState[com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$PlaceTypeSearchState.SEARCH_COMPLETE.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException12) {
        }
        $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent = new int[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.values().length];
        int[] a5 = $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent;
        com.navdy.hud.app.manager.InputManager$CustomKeyEvent a6 = com.navdy.hud.app.manager.InputManager$CustomKeyEvent.LEFT;
        try {
            a5[a6.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException13) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.RIGHT.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException14) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.SELECT.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException15) {
        }
        $SwitchMap$com$navdy$service$library$events$input$Gesture = new int[com.navdy.service.library.events.input.Gesture.values().length];
        int[] a7 = $SwitchMap$com$navdy$service$library$events$input$Gesture;
        com.navdy.service.library.events.input.Gesture a8 = com.navdy.service.library.events.input.Gesture.GESTURE_SWIPE_LEFT;
        try {
            a7[a8.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException16) {
        }
    }
}
