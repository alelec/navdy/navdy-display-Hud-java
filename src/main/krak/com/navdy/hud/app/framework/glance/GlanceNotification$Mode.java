package com.navdy.hud.app.framework.glance;


public enum GlanceNotification$Mode {
    REPLY(0),
    READ(1);

    private int value;
    GlanceNotification$Mode(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class GlanceNotification$Mode extends Enum {
//    final private static com.navdy.hud.app.framework.glance.GlanceNotification$Mode[] $VALUES;
//    final public static com.navdy.hud.app.framework.glance.GlanceNotification$Mode READ;
//    final public static com.navdy.hud.app.framework.glance.GlanceNotification$Mode REPLY;
//    
//    static {
//        REPLY = new com.navdy.hud.app.framework.glance.GlanceNotification$Mode("REPLY", 0);
//        READ = new com.navdy.hud.app.framework.glance.GlanceNotification$Mode("READ", 1);
//        com.navdy.hud.app.framework.glance.GlanceNotification$Mode[] a = new com.navdy.hud.app.framework.glance.GlanceNotification$Mode[2];
//        a[0] = REPLY;
//        a[1] = READ;
//        $VALUES = a;
//    }
//    
//    private GlanceNotification$Mode(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.framework.glance.GlanceNotification$Mode valueOf(String s) {
//        return (com.navdy.hud.app.framework.glance.GlanceNotification$Mode)Enum.valueOf(com.navdy.hud.app.framework.glance.GlanceNotification$Mode.class, s);
//    }
//    
//    public static com.navdy.hud.app.framework.glance.GlanceNotification$Mode[] values() {
//        return $VALUES.clone();
//    }
//}
//