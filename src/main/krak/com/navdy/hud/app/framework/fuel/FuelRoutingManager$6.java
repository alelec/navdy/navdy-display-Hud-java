package com.navdy.hud.app.framework.fuel;

class FuelRoutingManager$6 implements Runnable {
    final com.navdy.hud.app.framework.fuel.FuelRoutingManager this$0;
    final int val$fuelLevel;
    final boolean val$postNotificationOnError;
    
    FuelRoutingManager$6(com.navdy.hud.app.framework.fuel.FuelRoutingManager a, int i, boolean b) {
        super();
        this.this$0 = a;
        this.val$fuelLevel = i;
        this.val$postNotificationOnError = b;
    }
    
    public void run() {
        if (com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$100(this.this$0)) {
            com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$602(this.this$0, true);
            com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$700(this.this$0, (com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback)new com.navdy.hud.app.framework.fuel.FuelRoutingManager$6$1(this));
        } else {
            com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$000().w("Fuel manager not available");
        }
    }
}
