package com.navdy.hud.app.framework.glympse;

class GlympseManager$InternalSingleton {
    final private static com.navdy.hud.app.framework.glympse.GlympseManager singleton;
    
    static {
        singleton = new com.navdy.hud.app.framework.glympse.GlympseManager((com.navdy.hud.app.framework.glympse.GlympseManager$1)null);
    }
    
    private GlympseManager$InternalSingleton() {
    }
    
    static com.navdy.hud.app.framework.glympse.GlympseManager access$100() {
        return singleton;
    }
}
