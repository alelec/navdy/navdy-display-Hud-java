package com.navdy.hud.app.framework.music;
import com.navdy.hud.app.R;

class MusicNotification$1 implements Runnable {
    final com.navdy.hud.app.framework.music.MusicNotification this$0;
    final boolean val$animate;
    final okio.ByteString val$photo;
    
    MusicNotification$1(com.navdy.hud.app.framework.music.MusicNotification a, okio.ByteString a0, boolean b) {
        super();
        this.this$0 = a;
        this.val$photo = a0;
        this.val$animate = b;
    }
    
    public void run() {
        com.navdy.hud.app.framework.music.MusicNotification.access$000().d(new StringBuilder().append("PhotoUpdate runnable ").append(this.val$photo).toString());
        byte[] a = this.val$photo.toByteArray();
        label0: {
            android.graphics.Bitmap a0 = null;
            android.graphics.Bitmap a1 = null;
            label2: {
                label3: {
                    if (a == null) {
                        break label3;
                    }
                    if (a.length > 0) {
                        break label2;
                    }
                }
                com.navdy.hud.app.framework.music.MusicNotification.access$000().i("Received photo has null or empty byte array");
                com.navdy.hud.app.framework.music.MusicNotification.access$100(this.this$0, true);
                break label0;
            }
            int i = com.navdy.hud.app.HudApplication.getAppContext().getResources().getDimensionPixelSize(R.dimen.music_notification_image_size);
            label1: {
                android.graphics.Bitmap a2 = null;
                android.graphics.Bitmap a3 = null;
                try {
                    Exception a4 = null;
                    try {
                        a2 = null;
                        a3 = null;
                        a0 = null;
                        a1 = null;
                        a0 = com.navdy.service.library.util.ScalingUtilities.decodeByteArray(a, i, i, com.navdy.hud.app.framework.music.MusicNotification.access$200(this.this$0));
                        a2 = a0;
                        a3 = null;
                        a1 = null;
                        a1 = com.navdy.service.library.util.ScalingUtilities.createScaledBitmap(a0, i, i, com.navdy.hud.app.framework.music.MusicNotification.access$200(this.this$0));
                        a2 = a0;
                        a3 = a1;
                        com.navdy.hud.app.framework.music.MusicNotification.access$300(this.this$0, a1, this.val$animate);
                        break label1;
                    } catch(Exception a5) {
                        a4 = a5;
                    }
                    a2 = a0;
                    a3 = a1;
                    com.navdy.hud.app.framework.music.MusicNotification.access$000().e("Error updating the artwork received ", (Throwable)a4);
                    com.navdy.hud.app.framework.music.MusicNotification.access$100(this.this$0, this.val$animate);
                } catch(Throwable a6) {
                    if (a2 != null && a2 != a3) {
                        a2.recycle();
                    }
                    throw a6;
                }
                if (a0 == null) {
                    break label0;
                }
                if (a0 == a1) {
                    break label0;
                }
                a0.recycle();
                break label0;
            }
            if (a0 != null && a0 != a1) {
                a0.recycle();
            }
        }
    }
}
