package com.navdy.hud.app.framework.voice;
import com.navdy.hud.app.R;

class VoiceSearchHandler$2 implements com.navdy.hud.app.ui.component.destination.IDestinationPicker {
    boolean retrySelected;
    final com.navdy.hud.app.framework.voice.VoiceSearchHandler this$0;
    
    VoiceSearchHandler$2(com.navdy.hud.app.framework.voice.VoiceSearchHandler a) {
        super();
        this.this$0 = a;
        this.retrySelected = false;
    }
    
    public void onDestinationPickerClosed() {
        if (this.retrySelected) {
            com.navdy.hud.app.framework.notifications.NotificationManager a = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
            com.navdy.hud.app.framework.voice.VoiceSearchNotification a0 = (com.navdy.hud.app.framework.voice.VoiceSearchNotification)a.getNotification("navdy#voicesearch#notif");
            if (a0 == null) {
                a0 = new com.navdy.hud.app.framework.voice.VoiceSearchNotification();
            }
            a.addNotification((com.navdy.hud.app.framework.notifications.INotification)a0);
        }
    }
    
    public boolean onItemClicked(int i, int i0, com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$DestinationPickerState a) {
        boolean b = false;
        switch(i) {
            case R.id.retry: case R.id.search_again: {
                this.retrySelected = true;
                if (i != R.id.retry) {
                    com.navdy.hud.app.analytics.AnalyticsSupport.recordVoiceSearchListItemSelection(-2);
                    b = true;
                    break;
                } else {
                    com.navdy.hud.app.analytics.AnalyticsSupport.recordVoiceSearchListItemSelection(-1);
                    b = true;
                    break;
                }
            }
            default: {
                com.navdy.hud.app.analytics.AnalyticsSupport.recordVoiceSearchListItemSelection(i0);
                b = false;
            }
        }
        return b;
    }
    
    public boolean onItemSelected(int i, int i0, com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$DestinationPickerState a) {
        return false;
    }
}
