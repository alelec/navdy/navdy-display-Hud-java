package com.navdy.hud.app.framework.calendar;

public class CalendarManager {
    final private static int CALENDAR_EVENTS_LIST_INITIAL_SIZE = 20;
    final private static com.navdy.service.library.log.Logger sLogger;
    public com.squareup.otto.Bus mBus;
    private java.util.List mCalendarEvents;
    private com.navdy.service.library.events.calendars.CalendarEventUpdates mLastCalendarEventUpdate;
    private String profileName;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.calendar.CalendarManager.class);
    }
    
    public CalendarManager(com.squareup.otto.Bus a) {
        this.mBus = a;
        this.mBus.register(this);
        this.mCalendarEvents = (java.util.List)new java.util.ArrayList(20);
    }
    
    public java.util.List getCalendarEvents() {
        return this.mCalendarEvents;
    }
    
    public void onCalendarEventsUpdate(com.navdy.service.library.events.calendars.CalendarEventUpdates a) {
        sLogger.d(new StringBuilder().append("Received Calendar updates :").append(a).toString());
        this.mLastCalendarEventUpdate = a;
        com.navdy.hud.app.profile.DriverProfile a0 = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile();
        if (a0 != null) {
            this.profileName = a0.getProfileName();
        }
        this.mCalendarEvents.clear();
        if (a != null && a.calendar_events != null) {
            sLogger.d(new StringBuilder().append("Calendar events count :").append(a.calendar_events.size()).toString());
            this.mCalendarEvents.addAll((java.util.Collection)a.calendar_events);
        }
        this.mBus.post(com.navdy.hud.app.framework.calendar.CalendarManager$CalendarManagerEvent.UPDATED);
    }
    
    public void onDriverProfileChanged(com.navdy.hud.app.event.DriverProfileChanged a) {
        if (com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile().isDefaultProfile()) {
            sLogger.d("Default profile loaded, not clearing the calendar events");
        } else {
            this.mCalendarEvents.clear();
            this.mBus.post(com.navdy.hud.app.framework.calendar.CalendarManager$CalendarManagerEvent.UPDATED);
        }
    }
}
