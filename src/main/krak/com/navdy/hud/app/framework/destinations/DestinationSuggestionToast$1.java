package com.navdy.hud.app.framework.destinations;
import com.navdy.hud.app.R;

final class DestinationSuggestionToast$1 implements com.navdy.hud.app.framework.toast.IToastCallback {
    com.navdy.hud.app.view.ToastView toastView;
    final com.navdy.service.library.events.destination.Destination val$destination;
    
    DestinationSuggestionToast$1(com.navdy.service.library.events.destination.Destination a) {
        super();
        this.val$destination = a;
    }
    
    public void executeChoiceItem(int i, int i0) {
        com.navdy.hud.app.view.ToastView a = this.toastView;
        label1: {
            boolean b = false;
            Throwable a0 = null;
            if (a == null) {
                break label1;
            }
            switch(i0) {
                case 2: {
                    this.toastView.dismissToast();
                    b = false;
                    break;
                }
                case 1: {
                    this.toastView.dismissToast();
                    com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance().goToSuggestedDestination();
                    b = true;
                    break;
                }
                default: {
                    b = false;
                }
            }
            label0: {
                try {
                    com.navdy.hud.app.analytics.AnalyticsSupport.recordDestinationSuggestion(b, this.val$destination.suggestion_type == com.navdy.service.library.events.destination.Destination$SuggestionType.SUGGESTION_CALENDAR);
                } catch(Throwable a1) {
                    a0 = a1;
                    break label0;
                }
                break label1;
            }
            com.navdy.hud.app.framework.destinations.DestinationSuggestionToast.sLogger.e("Error while recording the destination suggestion", a0);
        }
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        return false;
    }
    
    public void onStart(com.navdy.hud.app.view.ToastView a) {
        this.toastView = a;
        com.navdy.hud.app.ui.component.ConfirmationLayout a0 = a.getConfirmation();
        a0.screenTitle.setTextAppearance(a.getContext(), R.style.mainTitle);
        a0.title3.setTextColor(a.getResources().getColor(17170443));
    }
    
    public void onStop() {
        this.toastView = null;
    }
}
