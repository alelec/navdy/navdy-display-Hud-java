package com.navdy.hud.app.framework.voice;
import com.navdy.hud.app.R;

public class VoiceSearchHandler {
    final private static String searchAgainSubTitle;
    private com.squareup.otto.Bus bus;
    private android.content.Context context;
    private android.os.Handler handler;
    private com.navdy.service.library.events.audio.VoiceSearchResponse response;
    private boolean showVoiceSearchTipsToUser;
    private com.navdy.hud.app.framework.voice.VoiceSearchHandler$VoiceSearchUserInterface voiceSearchUserInterface;
    
    static {
        searchAgainSubTitle = com.navdy.hud.app.HudApplication.getAppContext().getResources().getString(R.string.retry_search);
    }
    
    public VoiceSearchHandler(android.content.Context a, com.squareup.otto.Bus a0, com.navdy.hud.app.util.FeatureUtil a1) {
        this.showVoiceSearchTipsToUser = true;
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.bus = a0;
        this.context = a;
        this.bus.register(this);
    }
    
    static com.squareup.otto.Bus access$000(com.navdy.hud.app.framework.voice.VoiceSearchHandler a) {
        return a.bus;
    }
    
    public void go() {
        if (this.response != null && this.response.state == com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchState.VOICE_SEARCH_SUCCESS && this.response.results != null && this.response.results.size() > 0) {
            com.navdy.service.library.events.destination.Destination a = (com.navdy.service.library.events.destination.Destination)this.response.results.get(0);
            com.navdy.hud.app.framework.destinations.Destination a0 = com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance().transformToInternalDestination(a);
            com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$PlaceTypeResourceHolder a1 = com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.getPlaceTypeHolder(a.place_type);
            int i = (a1 == null) ? 0 : a1.iconRes;
            if (a0.destinationIcon == 0 && i != 0) {
                a0 = new com.navdy.hud.app.framework.destinations.Destination$Builder(a0).destinationIcon(i).destinationIconBkColor(android.support.v4.content.ContextCompat.getColor(this.context, R.color.icon_bk_color_unselected)).build();
            }
            com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance().requestNavigationWithNavLookup(a0);
        }
    }
    
    public void onDriverProfileChanged(com.navdy.hud.app.event.DriverProfileChanged a) {
        this.showVoiceSearchTipsToUser = true;
    }
    
    public void onVoiceSearchResponse(com.navdy.service.library.events.audio.VoiceSearchResponse a) {
        if (this.voiceSearchUserInterface != null) {
            this.response = a;
            this.voiceSearchUserInterface.onVoiceSearchResponse(a);
        }
    }
    
    public void setVoiceSearchUserInterface(com.navdy.hud.app.framework.voice.VoiceSearchHandler$VoiceSearchUserInterface a) {
        this.voiceSearchUserInterface = a;
    }
    
    public boolean shouldShowVoiceSearchTipsToUser() {
        return this.showVoiceSearchTipsToUser;
    }
    
    public void showResults() {
        if (this.response != null && this.response.results != null) {
            String s = null;
            String s0 = null;
            java.util.List a = this.response.results;
            android.os.Bundle a0 = new android.os.Bundle();
            a0.putBoolean("PICKER_SHOW_DESTINATION_MAP", true);
            a0.putInt("PICKER_DESTINATION_ICON", -1);
            a0.putInt("PICKER_INITIAL_SELECTION", 1);
            int i = android.support.v4.content.ContextCompat.getColor(this.context, R.color.route_sel);
            int i0 = android.support.v4.content.ContextCompat.getColor(this.context, R.color.icon_bk_color_unselected);
            if (android.text.TextUtils.isEmpty((CharSequence)this.response.recognizedWords)) {
                s = searchAgainSubTitle;
                s0 = null;
            } else {
                s = new StringBuilder().append("\"").append(this.response.recognizedWords).append("\"").toString();
                s0 = searchAgainSubTitle;
            }
            com.navdy.hud.app.ui.component.destination.DestinationParcelable a1 = new com.navdy.hud.app.ui.component.destination.DestinationParcelable(R.id.search_again, s, s0, false, (String)null, true, (String)null, 0.0, 0.0, 0.0, 0.0, R.drawable.icon_mm_voice_search_2, 0, i, i0, com.navdy.hud.app.ui.component.destination.DestinationParcelable$DestinationType.NONE, (com.navdy.service.library.events.places.PlaceType)null);
            java.util.List a2 = com.navdy.hud.app.maps.util.DestinationUtil.convert(this.context, a, i, i0, true);
            a2.add(0, a1);
            a2.add(new com.navdy.hud.app.ui.component.destination.DestinationParcelable(R.id.retry, searchAgainSubTitle, (String)null, false, (String)null, false, (String)null, 0.0, 0.0, 0.0, 0.0, R.drawable.icon_mm_voice_search_2, 0, i, i0, com.navdy.hud.app.ui.component.destination.DestinationParcelable$DestinationType.NONE, com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_UNKNOWN));
            com.navdy.hud.app.ui.component.destination.DestinationParcelable[] a3 = new com.navdy.hud.app.ui.component.destination.DestinationParcelable[a2.size()];
            a2.toArray((Object[])a3);
            a0.putParcelableArray("PICKER_DESTINATIONS", (android.os.Parcelable[])a3);
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification("navdy#voicesearch#notif", true, com.navdy.service.library.events.ui.Screen.SCREEN_DESTINATION_PICKER, a0, new com.navdy.hud.app.framework.voice.VoiceSearchHandler$2(this));
        }
    }
    
    public void showedVoiceSearchTipsToUser() {
        this.showVoiceSearchTipsToUser = false;
    }
    
    public void startVoiceSearch() {
        com.navdy.service.library.events.DeviceInfo$Platform a = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getRemoteDevicePlatform();
        label2: if (a != null) {
            com.navdy.service.library.events.DeviceInfo$Platform a0 = com.navdy.service.library.events.DeviceInfo$Platform.PLATFORM_iOS;
            label0: {
                label1: {
                    if (a != a0) {
                        break label1;
                    }
                    if (com.navdy.hud.app.util.RemoteCapabilitiesUtil.supportsVoiceSearchNewIOSPauseBehaviors()) {
                        break label0;
                    }
                }
                this.bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)new com.navdy.service.library.events.audio.VoiceSearchRequest(Boolean.valueOf(false))));
                break label2;
            }
            com.navdy.hud.app.manager.MusicManager a1 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getMusicManager();
            if (a1 != null) {
                a1.softPause();
            }
            this.handler.postDelayed((Runnable)new com.navdy.hud.app.framework.voice.VoiceSearchHandler$1(this), 250L);
        }
    }
    
    public void stopVoiceSearch() {
        if (this.response != null && this.response.state != com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchState.VOICE_SEARCH_SUCCESS) {
            this.bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)new com.navdy.service.library.events.audio.VoiceSearchRequest(Boolean.valueOf(true))));
            com.navdy.hud.app.manager.MusicManager a = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getMusicManager();
            if (a != null) {
                a.acceptResumes();
            }
        }
    }
}
