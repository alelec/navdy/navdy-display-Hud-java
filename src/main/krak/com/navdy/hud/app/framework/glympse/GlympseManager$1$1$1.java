package com.navdy.hud.app.framework.glympse;

class GlympseManager$1$1$1 implements Runnable {
    final com.navdy.hud.app.framework.glympse.GlympseManager$1$1 this$2;
    final java.util.Date val$etaDate;
    final com.glympse.android.api.GTrack val$track;
    
    GlympseManager$1$1$1(com.navdy.hud.app.framework.glympse.GlympseManager$1$1 a, java.util.Date a0, com.glympse.android.api.GTrack a1) {
        super();
        this.this$2 = a;
        this.val$etaDate = a0;
        this.val$track = a1;
    }
    
    public void run() {
        long j = System.currentTimeMillis();
        long j0 = this.val$etaDate.getTime() - j;
        if (j0 < 0L) {
            j0 = 0L;
        }
        long j1 = j0 / 60000L;
        com.glympse.android.core.GArray a = com.navdy.hud.app.framework.glympse.GlympseManager.access$300(this.this$2.this$1.this$0).getHistoryManager().getTickets();
        int i = a.length();
        Object a0 = a;
        int i0 = 0;
        while(i0 < i) {
            Object a1 = ((com.glympse.android.core.GArray)a0).at(i0);
            if (((com.glympse.android.api.GTicket)a1).isActive()) {
                if (((com.glympse.android.api.GTicket)a1).getDestination() != null) {
                    com.navdy.hud.app.framework.glympse.GlympseManager.access$200().v(new StringBuilder().append("update ETA for ticket with id[").append(((com.glympse.android.api.GTicket)a1).getId()).append("] durationMillis[").append(j0).append("] ETA[").append(this.val$etaDate).append("] currentTime=").append(j).append(" minutes[").append(j1).append("] expireTime[").append(new java.util.Date(((com.glympse.android.api.GTicket)a1).getExpireTime())).append("]").toString());
                    ((com.glympse.android.api.GTicket)a1).updateEta(j0);
                    ((com.glympse.android.api.GTicket)a1).updateRoute(this.val$track);
                } else {
                    com.navdy.hud.app.framework.glympse.GlympseManager.access$200().v(new StringBuilder().append("update ETA ticket not for trip:").append(((com.glympse.android.api.GTicket)a1).getId()).toString());
                }
            } else {
                com.navdy.hud.app.framework.glympse.GlympseManager.access$200().v(new StringBuilder().append("update ETA ticket not active:").append(((com.glympse.android.api.GTicket)a1).getId()).toString());
            }
            i0 = i0 + 1;
        }
    }
}
