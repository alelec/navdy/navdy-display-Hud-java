package com.navdy.hud.app.debug;

class DriveRecorder$6 implements com.navdy.obd.IPidListener {
    final com.navdy.hud.app.debug.DriveRecorder this$0;
    
    DriveRecorder$6(com.navdy.hud.app.debug.DriveRecorder a) {
        super();
        this.this$0 = a;
    }
    
    public android.os.IBinder asBinder() {
        return null;
    }
    
    public void onConnectionStateChange(int i) {
    }
    
    public void pidsChanged(java.util.List a) {
    }
    
    public void pidsRead(java.util.List a, java.util.List a0) {
        if (a0 != null && com.navdy.hud.app.debug.DriveRecorder.access$000(this.this$0)) {
            if (!com.navdy.hud.app.debug.DriveRecorder.access$100(this.this$0)) {
                com.navdy.hud.app.obd.ObdManager.getInstance().setRecordingPidListener((com.navdy.obd.IPidListener)null);
                try {
                    com.navdy.hud.app.debug.DriveRecorder.access$800(this.this$0);
                } catch(Throwable ignoredException) {
                    com.navdy.hud.app.debug.DriveRecorder.sLogger.d("Error while recording the supported PIDs");
                }
                com.navdy.hud.app.obd.ObdManager.getInstance().setRecordingPidListener(com.navdy.hud.app.debug.DriveRecorder.access$900(this.this$0));
            }
            this.this$0.obdAsyncBufferedFileWriter.write(new StringBuilder().append(System.currentTimeMillis()).append(",").toString());
            java.util.Iterator a1 = a0.iterator();
            Object a2 = a0;
            Object a3 = a1;
            while(((java.util.Iterator)a3).hasNext()) {
                com.navdy.obd.Pid a4 = (com.navdy.obd.Pid)((java.util.Iterator)a3).next();
                this.this$0.obdAsyncBufferedFileWriter.write(new StringBuilder().append(a4.getId()).append(":").append(a4.getValue()).toString());
                if (a4 == ((java.util.List)a2).get(((java.util.List)a2).size() - 1)) {
                    this.this$0.obdAsyncBufferedFileWriter.write("\n");
                } else {
                    this.this$0.obdAsyncBufferedFileWriter.write(",");
                }
            }
        }
    }
}
