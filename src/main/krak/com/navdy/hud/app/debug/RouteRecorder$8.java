package com.navdy.hud.app.debug;

class RouteRecorder$8 implements Runnable {
    final com.navdy.hud.app.debug.RouteRecorder this$0;
    final String val$fileName;
    final boolean val$local;
    
    RouteRecorder$8(com.navdy.hud.app.debug.RouteRecorder a, String s, boolean b) {
        super();
        this.this$0 = a;
        this.val$fileName = s;
        this.val$local = b;
    }
    
    public void run() {
        java.io.File a = com.navdy.hud.app.debug.DriveRecorder.getDriveLogsDir(this.val$fileName);
        if (!a.exists()) {
            a.mkdirs();
        }
        java.io.File a0 = new java.io.File(a, this.val$fileName);
        try {
            if (a0.createNewFile()) {
                com.navdy.hud.app.debug.RouteRecorder.access$1202(this.this$0, new java.io.BufferedWriter((java.io.Writer)new java.io.OutputStreamWriter((java.io.OutputStream)new java.io.FileOutputStream(a0), "utf-8")));
                com.navdy.hud.app.debug.RouteRecorder.sLogger.v(new StringBuilder().append("writing drive log: ").append(this.val$fileName).toString());
                com.navdy.hud.app.debug.RouteRecorder.access$500(this.this$0).post(com.navdy.hud.app.debug.RouteRecorder.access$1300(this.this$0));
                if (!this.val$local) {
                    com.navdy.hud.app.debug.RouteRecorder.access$1100(this.this$0).sendMessage((com.squareup.wire.Message)new com.navdy.service.library.events.debug.StartDriveRecordingResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS));
                }
            }
        } catch(java.io.IOException a1) {
            com.navdy.hud.app.debug.RouteRecorder.sLogger.e((Throwable)a1);
        }
    }
}
