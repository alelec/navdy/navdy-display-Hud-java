package com.navdy.hud.app.debug;
import com.navdy.hud.app.R;

public class SettingsActivity$DeveloperPreferencesFragment extends android.preference.PreferenceFragment {
    public SettingsActivity$DeveloperPreferencesFragment() {
    }
    
    public void onCreate(android.os.Bundle a) {
        super.onCreate(a);
        this.addPreferencesFromResource(R.xml.developer_preferences);
    }
}
