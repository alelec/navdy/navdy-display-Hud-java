package com.navdy.hud.app.debug;

class DriveRecorder$8 implements Runnable {
    final com.navdy.hud.app.debug.DriveRecorder this$0;
    final com.navdy.hud.app.debug.DriveRecorder$Action val$action;
    final boolean val$loop;
    
    DriveRecorder$8(com.navdy.hud.app.debug.DriveRecorder a, com.navdy.hud.app.debug.DriveRecorder$Action a0, boolean b) {
        super();
        this.this$0 = a;
        this.val$action = a0;
        this.val$loop = b;
    }
    
    public void run() {
        if (this.this$0.isDemoAvailable()) {
            switch(com.navdy.hud.app.debug.DriveRecorder$10.$SwitchMap$com$navdy$hud$app$debug$DriveRecorder$Action[this.val$action.ordinal()]) {
                case 6: {
                    com.navdy.hud.app.debug.DriveRecorder.access$1500(this.this$0);
                    break;
                }
                case 5: {
                    com.navdy.hud.app.debug.DriveRecorder.access$1400(this.this$0);
                    break;
                }
                case 4: {
                    com.navdy.hud.app.debug.DriveRecorder.access$1300(this.this$0, this.val$loop);
                    break;
                }
                case 3: {
                    com.navdy.hud.app.debug.DriveRecorder.access$1200(this.this$0);
                    break;
                }
                case 2: {
                    com.navdy.hud.app.debug.DriveRecorder.access$1100(this.this$0);
                    break;
                }
                case 1: {
                    com.navdy.hud.app.debug.DriveRecorder.access$1000(this.this$0, this.val$loop);
                    break;
                }
            }
        }
    }
}
