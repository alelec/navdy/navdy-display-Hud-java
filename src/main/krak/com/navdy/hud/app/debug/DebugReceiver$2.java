package com.navdy.hud.app.debug;

class DebugReceiver$2 extends java.util.TimerTask {
    final com.navdy.hud.app.debug.DebugReceiver this$0;
    final int val$mediaKey;
    
    DebugReceiver$2(com.navdy.hud.app.debug.DebugReceiver a, int i) {
        super();
        this.this$0 = a;
        this.val$mediaKey = i;
    }
    
    public void run() {
        this.this$0.mBus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)new com.navdy.service.library.events.input.MediaRemoteKeyEvent(com.navdy.service.library.events.input.MediaRemoteKey.values()[this.val$mediaKey], com.navdy.service.library.events.input.KeyEvent.KEY_UP)));
    }
}
