package com.navdy.hud.app.debug;


    public enum DriveRecorder$State {
        PLAYING(0),
    PAUSED(1),
    STOPPED(2);

        private int value;
        DriveRecorder$State(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class DriveRecorder$State extends Enum {
//    final private static com.navdy.hud.app.debug.DriveRecorder$State[] $VALUES;
//    final public static com.navdy.hud.app.debug.DriveRecorder$State PAUSED;
//    final public static com.navdy.hud.app.debug.DriveRecorder$State PLAYING;
//    final public static com.navdy.hud.app.debug.DriveRecorder$State STOPPED;
//    
//    static {
//        PLAYING = new com.navdy.hud.app.debug.DriveRecorder$State("PLAYING", 0);
//        PAUSED = new com.navdy.hud.app.debug.DriveRecorder$State("PAUSED", 1);
//        STOPPED = new com.navdy.hud.app.debug.DriveRecorder$State("STOPPED", 2);
//        com.navdy.hud.app.debug.DriveRecorder$State[] a = new com.navdy.hud.app.debug.DriveRecorder$State[3];
//        a[0] = PLAYING;
//        a[1] = PAUSED;
//        a[2] = STOPPED;
//        $VALUES = a;
//    }
//    
//    private DriveRecorder$State(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.debug.DriveRecorder$State valueOf(String s) {
//        return (com.navdy.hud.app.debug.DriveRecorder$State)Enum.valueOf(com.navdy.hud.app.debug.DriveRecorder$State.class, s);
//    }
//    
//    public static com.navdy.hud.app.debug.DriveRecorder$State[] values() {
//        return $VALUES.clone();
//    }
//}
//