package com.navdy.hud.app.debug;
import com.navdy.hud.app.R;

public class GestureEngine extends android.widget.LinearLayout {
    @Inject
    com.navdy.hud.app.gesture.GestureServiceConnector gestureServiceConnector;
    
    public GestureEngine(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public GestureEngine(android.content.Context a, android.util.AttributeSet a0) {
        super(a, a0);
        if (!this.isInEditMode()) {
            mortar.Mortar.inject(a, this);
        }
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View)this);
        ((android.widget.CheckBox)this.findViewById(R.id.enable_gesture)).setChecked(this.gestureServiceConnector.isRunning());
    }
    
    public void onToggleDiscreteMode(boolean b) {
        this.gestureServiceConnector.setDiscreteMode(b);
    }
    
    void onToggleGesture(boolean b) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.debug.GestureEngine$1(this, b), 1);
    }
    
    public void onTogglePreview(boolean b) {
        this.gestureServiceConnector.enablePreview(b);
    }
}
