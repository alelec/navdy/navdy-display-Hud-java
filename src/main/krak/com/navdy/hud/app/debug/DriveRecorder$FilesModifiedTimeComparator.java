package com.navdy.hud.app.debug;

public class DriveRecorder$FilesModifiedTimeComparator implements java.util.Comparator {
    public DriveRecorder$FilesModifiedTimeComparator() {
    }
    
    public int compare(java.io.File a, java.io.File a0) {
        return Long.compare(a.lastModified(), a0.lastModified());
    }
    
    public int compare(Object a, Object a0) {
        return this.compare((java.io.File)a, (java.io.File)a0);
    }
}
