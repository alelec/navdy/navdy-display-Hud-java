package com.navdy.hud.app.debug;


public enum DriveRecorder$Action {
    PRELOAD(0),
    PLAY(1),
    PAUSE(2),
    RESUME(3),
    RESTART(4),
    STOP(5);

    private int value;
    DriveRecorder$Action(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class DriveRecorder$Action extends Enum {
//    final private static com.navdy.hud.app.debug.DriveRecorder$Action[] $VALUES;
//    final public static com.navdy.hud.app.debug.DriveRecorder$Action PAUSE;
//    final public static com.navdy.hud.app.debug.DriveRecorder$Action PLAY;
//    final public static com.navdy.hud.app.debug.DriveRecorder$Action PRELOAD;
//    final public static com.navdy.hud.app.debug.DriveRecorder$Action RESTART;
//    final public static com.navdy.hud.app.debug.DriveRecorder$Action RESUME;
//    final public static com.navdy.hud.app.debug.DriveRecorder$Action STOP;
//    
//    static {
//        PRELOAD = new com.navdy.hud.app.debug.DriveRecorder$Action("PRELOAD", 0);
//        PLAY = new com.navdy.hud.app.debug.DriveRecorder$Action("PLAY", 1);
//        PAUSE = new com.navdy.hud.app.debug.DriveRecorder$Action("PAUSE", 2);
//        RESUME = new com.navdy.hud.app.debug.DriveRecorder$Action("RESUME", 3);
//        RESTART = new com.navdy.hud.app.debug.DriveRecorder$Action("RESTART", 4);
//        STOP = new com.navdy.hud.app.debug.DriveRecorder$Action("STOP", 5);
//        com.navdy.hud.app.debug.DriveRecorder$Action[] a = new com.navdy.hud.app.debug.DriveRecorder$Action[6];
//        a[0] = PRELOAD;
//        a[1] = PLAY;
//        a[2] = PAUSE;
//        a[3] = RESUME;
//        a[4] = RESTART;
//        a[5] = STOP;
//        $VALUES = a;
//    }
//    
//    private DriveRecorder$Action(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.debug.DriveRecorder$Action valueOf(String s) {
//        return (com.navdy.hud.app.debug.DriveRecorder$Action)Enum.valueOf(com.navdy.hud.app.debug.DriveRecorder$Action.class, s);
//    }
//    
//    public static com.navdy.hud.app.debug.DriveRecorder$Action[] values() {
//        return $VALUES.clone();
//    }
//}
//