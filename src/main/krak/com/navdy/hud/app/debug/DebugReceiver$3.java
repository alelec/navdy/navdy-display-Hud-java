package com.navdy.hud.app.debug;

class DebugReceiver$3 implements Runnable {
    final com.navdy.hud.app.debug.DebugReceiver this$0;
    final int val$size;
    
    DebugReceiver$3(com.navdy.hud.app.debug.DebugReceiver a, int i) {
        super();
        this.this$0 = a;
        this.val$size = i;
    }
    
    public void run() {
        label9: {
            label8: {
                label7: {
                    label6: {
                        label2: try {
                            Exception a = null;
                            label0: {
                                java.io.IOException a0 = null;
                                label1: {
                                    java.net.SocketTimeoutException a1 = null;
                                    try {
                                        try {
                                            try {
                                                int i = 0;
                                                java.net.HttpURLConnection a2 = (java.net.HttpURLConnection)new java.net.URL("http://test.mjpmz4phvm.us-west-2.elasticbeanstalk.com/file").openConnection();
                                                a2.setConnectTimeout(30000);
                                                a2.setReadTimeout(30000);
                                                a2.setRequestMethod("POST");
                                                java.io.OutputStream a3 = a2.getOutputStream();
                                                int i0 = this.val$size;
                                                label5: {
                                                    label3: {
                                                        label4: {
                                                            if (i0 < 0) {
                                                                break label4;
                                                            }
                                                            if (this.val$size <= 2000) {
                                                                break label3;
                                                            }
                                                        }
                                                        i = 100000;
                                                        break label5;
                                                    }
                                                    i = this.val$size * 1000;
                                                }
                                                com.navdy.hud.app.debug.DebugReceiver.sLogger.d(new StringBuilder().append("Uploading ").append(android.text.format.Formatter.formatFileSize(com.navdy.hud.app.HudApplication.getAppContext(), (long)i)).toString());
                                                byte[] a4 = new byte[i];
                                                new java.util.Random().nextBytes(a4);
                                                a3.write(a4);
                                                if (a2.getResponseCode() != 200) {
                                                    break label2;
                                                }
                                                com.navdy.hud.app.debug.DebugReceiver.sLogger.d("POST succeeded");
                                                break label2;
                                            } catch(java.net.SocketTimeoutException a5) {
                                                a1 = a5;
                                            }
                                        } catch(java.io.IOException a6) {
                                            a0 = a6;
                                            break label1;
                                        }
                                    } catch(Exception a7) {
                                        a = a7;
                                        break label0;
                                    }
                                    com.navdy.hud.app.debug.DebugReceiver.sLogger.e("Socket timed out Exception ", (Throwable)a1);
                                    break label6;
                                }
                                com.navdy.hud.app.debug.DebugReceiver.sLogger.e("IOException ", (Throwable)a0);
                                break label7;
                            }
                            com.navdy.hud.app.debug.DebugReceiver.sLogger.e("Exception ", (Throwable)a);
                            break label8;
                        } catch(Throwable a8) {
                            double d = (double)(android.os.SystemClock.elapsedRealtime() - 0L);
                            float f = (float)((double)((float)0L / 1000f) / (d / 1000.0));
                            com.navdy.hud.app.debug.DebugReceiver.sLogger.e(new StringBuilder().append("Upload finished, Size : ").append(0L).append(", Time Taken :").append((long)d).append(" milliseconds , RawSpeed :").append(f).append(" kBps").toString());
                            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)null);
                            throw a8;
                        }
                        double d0 = (double)(android.os.SystemClock.elapsedRealtime() - 0L);
                        float f0 = (float)((double)((float)0L / 1000f) / (d0 / 1000.0));
                        com.navdy.hud.app.debug.DebugReceiver.sLogger.e(new StringBuilder().append("Upload finished, Size : ").append(0L).append(", Time Taken :").append((long)d0).append(" milliseconds , RawSpeed :").append(f0).append(" kBps").toString());
                        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)null);
                        break label9;
                    }
                    double d1 = (double)(android.os.SystemClock.elapsedRealtime() - 0L);
                    float f1 = (float)((double)((float)0L / 1000f) / (d1 / 1000.0));
                    com.navdy.hud.app.debug.DebugReceiver.sLogger.e(new StringBuilder().append("Upload finished, Size : ").append(0L).append(", Time Taken :").append((long)d1).append(" milliseconds , RawSpeed :").append(f1).append(" kBps").toString());
                    com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)null);
                    break label9;
                }
                double d2 = (double)(android.os.SystemClock.elapsedRealtime() - 0L);
                float f2 = (float)((double)((float)0L / 1000f) / (d2 / 1000.0));
                com.navdy.hud.app.debug.DebugReceiver.sLogger.e(new StringBuilder().append("Upload finished, Size : ").append(0L).append(", Time Taken :").append((long)d2).append(" milliseconds , RawSpeed :").append(f2).append(" kBps").toString());
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)null);
                break label9;
            }
            double d3 = (double)(android.os.SystemClock.elapsedRealtime() - 0L);
            float f3 = (float)((double)((float)0L / 1000f) / (d3 / 1000.0));
            com.navdy.hud.app.debug.DebugReceiver.sLogger.e(new StringBuilder().append("Upload finished, Size : ").append(0L).append(", Time Taken :").append((long)d3).append(" milliseconds , RawSpeed :").append(f3).append(" kBps").toString());
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)null);
        }
    }
}
