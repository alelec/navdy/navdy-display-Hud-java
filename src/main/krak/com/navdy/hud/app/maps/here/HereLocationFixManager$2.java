package com.navdy.hud.app.maps.here;

class HereLocationFixManager$2 implements android.location.LocationListener {
    final com.navdy.hud.app.maps.here.HereLocationFixManager this$0;
    
    HereLocationFixManager$2(com.navdy.hud.app.maps.here.HereLocationFixManager a) {
        super();
        this.this$0 = a;
    }
    
    public void onLocationChanged(android.location.Location a) {
        if (!com.navdy.hud.app.maps.here.HereLocationFixManager.access$100(this.this$0)) {
            this.this$0.setMaptoLocation(a);
        }
        boolean b = com.navdy.hud.app.device.gps.GpsUtils.isDebugRawGpsPosEnabled();
        label0: {
            Throwable a0 = null;
            if (!b) {
                break label0;
            }
            try {
                if (com.navdy.hud.app.maps.here.HereLocationFixManager.access$800(this.this$0) == null) {
                    break label0;
                }
                if (!com.navdy.hud.app.maps.here.HereLocationFixManager.access$900(this.this$0)) {
                    break label0;
                }
                com.navdy.hud.app.maps.here.HereLocationFixManager.access$800(this.this$0).setCoordinate(new com.here.android.mpa.common.GeoCoordinate(a.getLatitude(), a.getLongitude()));
                break label0;
            } catch(Throwable a1) {
                a0 = a1;
            }
            com.navdy.hud.app.maps.here.HereLocationFixManager.access$200().e(a0);
        }
        if (com.navdy.hud.app.maps.here.HereLocationFixManager.access$1000(this.this$0)) {
            com.navdy.hud.app.maps.here.HereLocationFixManager.access$1100(this.this$0, a);
        }
        com.navdy.hud.app.maps.here.HereLocationFixManager.access$002(this.this$0, android.os.SystemClock.elapsedRealtime());
        com.navdy.hud.app.maps.here.HereLocationFixManager.access$402(this.this$0, a);
        if (com.navdy.hud.app.maps.here.HereLocationFixManager.access$000(this.this$0) - com.navdy.hud.app.maps.here.HereLocationFixManager.access$1200(this.this$0) >= 1000L) {
            if (!android.text.TextUtils.equals((CharSequence)a.getProvider(), (CharSequence)"NAVDY_GPS_PROVIDER")) {
                com.navdy.hud.app.maps.here.HereLocationFixManager.access$1300(this.this$0, a);
            }
            com.navdy.hud.app.maps.here.HereLocationFixManager.access$1202(this.this$0, com.navdy.hud.app.maps.here.HereLocationFixManager.access$000(this.this$0));
        }
    }
    
    public void onProviderDisabled(String s) {
    }
    
    public void onProviderEnabled(String s) {
    }
    
    public void onStatusChanged(String s, int i, android.os.Bundle a) {
    }
}
