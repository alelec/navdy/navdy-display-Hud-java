package com.navdy.hud.app.maps.here;

public class HereManeuverDisplayBuilder$PendingRoadInfo {
    public boolean dontUseSignpostText;
    public boolean enterHighway;
    public boolean hasTo;
    public boolean usePrevManeuverInfo;
    
    public HereManeuverDisplayBuilder$PendingRoadInfo() {
    }
}
