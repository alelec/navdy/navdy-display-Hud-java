package com.navdy.hud.app.maps;

public class MapEvents$RouteCalculationEvent {
    public boolean abortOriginDisplay;
    public Object end;
    public com.navdy.hud.app.framework.destinations.Destination lookupDestination;
    public String pendingNavigationRequestId;
    public int progress;
    public int progressIncrement;
    public com.navdy.service.library.events.navigation.NavigationRouteRequest request;
    public com.navdy.service.library.events.navigation.NavigationRouteResponse response;
    public com.here.android.mpa.routing.RouteOptions routeOptions;
    public Object start;
    public com.navdy.hud.app.maps.MapEvents$RouteCalculationState state;
    public boolean stopped;
    public java.util.List waypoints;
    
    public MapEvents$RouteCalculationEvent() {
    }
}
