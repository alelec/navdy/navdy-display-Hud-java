package com.navdy.hud.app.maps.here;
import com.navdy.hud.app.R;

final class HereRouteManager$5 implements com.navdy.hud.app.maps.here.HereRouteCalculator$RouteCalculatorListener {
    final com.navdy.service.library.events.navigation.NavigationRouteRequest val$request;
    
    HereRouteManager$5(com.navdy.service.library.events.navigation.NavigationRouteRequest a) {
        super();
        this.val$request = a;
    }
    
    public void error(com.here.android.mpa.routing.RoutingError a, Throwable a0) {
        String s = (a == null) ? com.navdy.hud.app.maps.here.HereRouteManager.access$200().getString(R.string.search_error) : a.name();
        synchronized(com.navdy.hud.app.maps.here.HereRouteManager.access$400()) {
            if (com.navdy.hud.app.maps.here.HereRouteManager.access$1500()) {
                com.navdy.hud.app.maps.here.HereRouteManager.access$100().v("user cancelled routing");
                com.navdy.hud.app.maps.here.HereRouteManager.access$300(com.navdy.service.library.events.RequestStatus.REQUEST_CANCELLED, this.val$request, (String)null);
            } else {
                com.navdy.hud.app.maps.here.HereRouteManager.access$300(com.navdy.service.library.events.RequestStatus.REQUEST_SERVICE_ERROR, this.val$request, s);
            }
            /*monexit(a1)*/;
        }
    }
    
    public void postSuccess(java.util.ArrayList a) {
        com.navdy.hud.app.maps.here.HereRouteManager.access$1300(this.val$request, a, false);
    }
    
    public void preSuccess() {
        synchronized(com.navdy.hud.app.maps.here.HereRouteManager.access$400()) {
            com.navdy.hud.app.maps.here.HereRouteManager.access$1702((com.navdy.hud.app.maps.here.HereRouteCalculator)null);
            /*monexit(a)*/;
        }
    }
    
    public void progress(int i) {
    }
}
