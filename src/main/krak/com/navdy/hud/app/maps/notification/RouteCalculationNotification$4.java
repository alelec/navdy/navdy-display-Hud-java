package com.navdy.hud.app.maps.notification;

class RouteCalculationNotification$4 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
    final com.navdy.hud.app.maps.notification.RouteCalculationNotification this$0;
    
    RouteCalculationNotification$4(com.navdy.hud.app.maps.notification.RouteCalculationNotification a) {
        super();
        this.this$0 = a;
    }
    
    public void onAnimationEnd(android.animation.Animator a) {
        if (com.navdy.hud.app.maps.notification.RouteCalculationNotification.access$1000(this.this$0) == null) {
            com.navdy.hud.app.maps.notification.RouteCalculationNotification.access$200().v("abandon loading animation");
        } else {
            com.navdy.hud.app.maps.notification.RouteCalculationNotification.access$1000(this.this$0).setStartDelay(33L);
            com.navdy.hud.app.maps.notification.RouteCalculationNotification.access$1000(this.this$0).start();
        }
    }
}
