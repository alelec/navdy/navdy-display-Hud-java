package com.navdy.hud.app.maps.here;

class HereMapsManager$3$1 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapsManager$3 this$1;
    final com.here.android.mpa.common.GeoPosition val$geoPosition;
    final boolean val$isMapMatched;
    final com.here.android.mpa.common.PositioningManager$LocationMethod val$locationMethod;
    
    HereMapsManager$3$1(com.navdy.hud.app.maps.here.HereMapsManager$3 a, com.here.android.mpa.common.GeoPosition a0, com.here.android.mpa.common.PositioningManager$LocationMethod a1, boolean b) {
        super();
        this.this$1 = a;
        this.val$geoPosition = a0;
        this.val$locationMethod = a1;
        this.val$isMapMatched = b;
    }
    
    public void run() {
        label0: {
            try {
                boolean b = this.val$geoPosition instanceof com.here.android.mpa.common.MatchedGeoPosition;
                label2: {
                    if (b) {
                        break label2;
                    }
                    com.here.android.mpa.common.GeoCoordinate a = this.val$geoPosition.getCoordinate();
                    com.navdy.hud.app.maps.here.HereMapsManager.access$100().v(new StringBuilder().append("position not map-matched lat:").append(a.getLatitude()).append(" lng:").append(a.getLongitude()).append(" speed:").append(this.val$geoPosition.getSpeed()).toString());
                    break label0;
                }
                if (com.navdy.hud.app.maps.here.HereMapsManager.access$2800(this.this$1.this$0)) {
                    if (com.navdy.hud.app.maps.here.HereMapUtil.isInTunnel(com.navdy.hud.app.maps.here.HereMapsManager.access$900(this.this$1.this$0).getRoadElement())) {
                        com.navdy.hud.app.maps.here.HereMapsManager.access$2900(this.this$1.this$0);
                    } else {
                        com.navdy.hud.app.maps.here.HereMapsManager.access$100().i("TUNNEL extrapolation off");
                        com.navdy.hud.app.maps.here.HereMapsManager.access$2802(this.this$1.this$0, false);
                        com.navdy.hud.app.maps.here.HereMapsManager.access$2900(this.this$1.this$0);
                    }
                }
            } catch(Throwable a0) {
                com.navdy.hud.app.maps.here.HereMapsManager.access$100().e(a0);
            }
            com.here.android.mpa.common.GeoPosition a1 = com.navdy.hud.app.maps.here.HereMapsManager.access$3000(this.this$1.this$0);
            label1: {
                if (a1 == null) {
                    break label1;
                }
                long j = android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.maps.here.HereMapsManager.access$3100(this.this$1.this$0);
                if (j >= 500L) {
                    break label1;
                }
                if (!com.navdy.hud.app.maps.here.HereMapUtil.isCoordinateEqual(com.navdy.hud.app.maps.here.HereMapsManager.access$3000(this.this$1.this$0).getCoordinate(), this.val$geoPosition.getCoordinate())) {
                    break label1;
                }
                if (!com.navdy.hud.app.maps.here.HereMapsManager.access$100().isLoggable(2)) {
                    break label0;
                }
                com.navdy.hud.app.maps.here.HereMapsManager.access$100().v(new StringBuilder().append("GEO-Here same pos as last:").append(j).toString());
                break label0;
            }
            com.navdy.hud.app.maps.here.HereMapsManager.access$3002(this.this$1.this$0, this.val$geoPosition);
            com.navdy.hud.app.maps.here.HereMapsManager.access$3102(this.this$1.this$0, android.os.SystemClock.elapsedRealtime());
            com.navdy.hud.app.maps.here.HereMapsManager.access$1100(this.this$1.this$0).setGeoPosition(this.val$geoPosition);
            com.navdy.hud.app.maps.here.HereMapsManager.access$1300(this.this$1.this$0).onHerePositionUpdated(this.val$locationMethod, this.val$geoPosition, this.val$isMapMatched);
            com.navdy.hud.app.maps.here.HereMapCameraManager.getInstance().onGeoPositionChange(this.val$geoPosition);
            if (com.navdy.hud.app.maps.here.HereMapsManager.access$3200(this.this$1.this$0).getObdSpeed() == -1 && android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.maps.here.HereMapsManager.access$1300(this.this$1.this$0).getLastLocationTime() >= 2000L && this.val$geoPosition instanceof com.here.android.mpa.common.MatchedGeoPosition && ((com.here.android.mpa.common.MatchedGeoPosition)this.val$geoPosition).isExtrapolated() && com.navdy.hud.app.maps.here.HereMapsManager.access$3200(this.this$1.this$0).setGpsSpeed((float)this.val$geoPosition.getSpeed(), android.os.SystemClock.elapsedRealtimeNanos() / 1000000L)) {
                this.this$1.this$0.bus.post(new com.navdy.hud.app.maps.MapEvents$GPSSpeedEvent());
            }
            this.this$1.this$0.bus.post(this.val$geoPosition);
            if (com.navdy.hud.app.maps.here.HereMapsManager.access$100().isLoggable(2)) {
                com.here.android.mpa.common.GeoCoordinate a2 = this.val$geoPosition.getCoordinate();
                com.navdy.hud.app.maps.here.HereMapsManager.access$100().v(new StringBuilder().append("GEO-Here speed-mps[").append(this.val$geoPosition.getSpeed()).append("] ").append("] lat=[").append(a2.getLatitude()).append("] lon=[").append(a2.getLongitude()).append("] provider=[").append(this.val$locationMethod.name()).append("] timestamp:[").append(this.val$geoPosition.getTimestamp().getTime()).append("]").toString());
            }
        }
    }
}
