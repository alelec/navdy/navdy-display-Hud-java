package com.navdy.hud.app.maps.here;

public class HereMapImageGenerator {
    final private static com.navdy.service.library.log.Logger sLogger;
    final private static com.navdy.hud.app.maps.here.HereMapImageGenerator singleton;
    private android.os.Handler handler;
    final private com.here.android.mpa.mapping.Map map;
    final private com.here.android.mpa.mapping.MapOffScreenRenderer mapOffScreenRenderer;
    private boolean renderRunning;
    private Object waitForRender;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereMapImageGenerator.class);
        singleton = new com.navdy.hud.app.maps.here.HereMapImageGenerator();
    }
    
    private HereMapImageGenerator() {
        this.waitForRender = new Object();
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.map = new com.here.android.mpa.mapping.Map();
        this.mapOffScreenRenderer = new com.here.android.mpa.mapping.MapOffScreenRenderer(com.navdy.hud.app.HudApplication.getAppContext());
        this.map.setMapScheme("terrain.day");
        this.map.setExtrudedBuildingsVisible(false);
        this.map.setFadingAnimations(false);
        this.map.getPositionIndicator().setVisible(false);
        this.mapOffScreenRenderer.setMap(this.map);
        this.mapOffScreenRenderer.setBlockingRendering(true);
    }
    
    static void access$000(com.navdy.hud.app.maps.here.HereMapImageGenerator a) {
        a.stopOffscreenRenderer();
    }
    
    static com.navdy.service.library.log.Logger access$100() {
        return sLogger;
    }
    
    static void access$200(com.navdy.hud.app.maps.here.HereMapImageGenerator a, com.navdy.hud.app.maps.here.HereMapImageGenerator$MapGeneratorParams a0, android.graphics.Bitmap a1) {
        a.saveFileToDisk(a0, a1);
    }
    
    static android.os.Handler access$300(com.navdy.hud.app.maps.here.HereMapImageGenerator a) {
        return a.handler;
    }
    
    private void generateMapSnapshotInternal(com.navdy.hud.app.maps.here.HereMapImageGenerator$MapGeneratorParams a) {
        boolean b = false;
        try {
            com.navdy.service.library.log.Logger a0 = sLogger;
            b = false;
            a0.e(new StringBuilder().append("generateMapSnapshotInternal lat:").append(a.latitude).append(" lon:").append(a.longitude).toString());
            com.here.android.mpa.common.GeoCoordinate a1 = new com.here.android.mpa.common.GeoCoordinate(a.latitude, a.longitude);
            this.map.setCenter(a1, com.here.android.mpa.mapping.Map$Animation.NONE);
            this.mapOffScreenRenderer.setSize(a.width, a.height);
            b = true;
            this.mapOffScreenRenderer.start();
            b = true;
            this.mapOffScreenRenderer.getScreenCapture((com.here.android.mpa.common.OnScreenCaptureListener)new com.navdy.hud.app.maps.here.HereMapImageGenerator$1(this, a));
        } catch(Throwable a2) {
            sLogger.e("renderMapSnapshot", a2);
            if (b) {
                this.stopOffscreenRenderer();
            }
        }
    }
    
    public static com.navdy.hud.app.maps.here.HereMapImageGenerator getInstance() {
        return singleton;
    }
    
    private void notifyWaitingRenders() {
        synchronized(this.waitForRender) {
            this.renderRunning = false;
            this.waitForRender.notifyAll();
            /*monexit(a)*/;
        }
    }
    
    private void saveFileToDisk(com.navdy.hud.app.maps.here.HereMapImageGenerator$MapGeneratorParams a, android.graphics.Bitmap a0) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereMapImageGenerator$2(this, a0, a), 1);
    }
    
    private void stopOffscreenRenderer() {
        label1: {
            Throwable a = null;
            label0: {
                try {
                    this.mapOffScreenRenderer.stop();
                } catch(Throwable a0) {
                    a = a0;
                    break label0;
                }
                this.notifyWaitingRenders();
                break label1;
            }
            try {
                sLogger.e(a);
            } catch(Throwable a1) {
                this.notifyWaitingRenders();
                throw a1;
            }
            this.notifyWaitingRenders();
        }
    }
    
    public void generateMapSnapshot(com.navdy.hud.app.maps.here.HereMapImageGenerator$MapGeneratorParams a) {
        if (a == null) {
            throw new IllegalArgumentException();
        }
        if (!com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
            sLogger.e("map engine not initialized");
            if (a.callback != null) {
                a.callback.result((android.graphics.Bitmap)null);
            }
        }
    }
    
    public String getMapImageFile(String s) {
        return new StringBuilder().append(com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getPlacesImageDir().getAbsolutePath()).append(java.io.File.separator).append(s).append(".jpg").toString();
    }
}
