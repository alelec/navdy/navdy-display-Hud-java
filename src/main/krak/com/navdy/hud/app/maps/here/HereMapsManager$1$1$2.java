package com.navdy.hud.app.maps.here;

class HereMapsManager$1$1$2 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapsManager$1$1 this$2;
    final long val$t2;
    
    HereMapsManager$1$1$2(com.navdy.hud.app.maps.here.HereMapsManager$1$1 a, long j) {
        super();
        this.this$2 = a;
        this.val$t2 = j;
    }
    
    public void run() {
        com.navdy.hud.app.maps.here.HereMapsManager.access$900(this.this$2.this$1.this$0).start(com.here.android.mpa.common.PositioningManager$LocationMethod.GPS_NETWORK);
        com.navdy.hud.app.maps.here.HereMapsManager.access$100().v("position manager started");
        com.navdy.hud.app.maps.here.HereMapsManager.access$100().v(new StringBuilder().append("MAP-ENGINE-INIT-3 took [").append(android.os.SystemClock.elapsedRealtime() - this.val$t2).append("]").toString());
        long j = android.os.SystemClock.elapsedRealtime();
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereMapsManager$1$1$2$1(this, j), 3);
    }
}
