package com.navdy.hud.app.maps;

public class MapEvents$LocationFix {
    public boolean locationAvailable;
    public boolean usingLocalGpsLocation;
    public boolean usingPhoneLocation;
    
    public MapEvents$LocationFix(boolean b, boolean b0, boolean b1) {
        this.locationAvailable = b;
        this.usingPhoneLocation = b0;
        this.usingLocalGpsLocation = b1;
    }
}
