package com.navdy.hud.app.maps.here;

class HereMapsManager$5 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapsManager this$0;
    
    HereMapsManager$5(com.navdy.hud.app.maps.here.HereMapsManager a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        label0: {
            Throwable a = null;
            label1: {
                boolean b = false;
                try {
                    if (((android.location.LocationManager)com.navdy.hud.app.HudApplication.getAppContext().getSystemService("location")).getProvider("network") != null) {
                        com.navdy.hud.app.maps.here.HereMapsManager.access$100().v("n/w provider found, initialize here");
                        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereMapsManager$5$1(this), 2);
                        b = false;
                    } else {
                        com.navdy.hud.app.maps.here.HereMapsManager.access$100().v("n/w provider not found yet, check again");
                        b = true;
                    }
                } catch(Throwable a0) {
                    a = a0;
                    break label1;
                }
                if (!b) {
                    break label0;
                }
                com.navdy.hud.app.maps.here.HereMapsManager.access$400(this.this$0).postDelayed((Runnable)this, 1000L);
                break label0;
            }
            com.navdy.hud.app.maps.here.HereMapsManager.access$100().e(a);
            com.navdy.hud.app.maps.here.HereMapsManager.access$400(this.this$0).postDelayed((Runnable)this, 1000L);
        }
    }
}
