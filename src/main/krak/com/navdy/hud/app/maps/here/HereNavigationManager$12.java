package com.navdy.hud.app.maps.here;

class HereNavigationManager$12 {
    final static int[] $SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit;
    final static int[] $SwitchMap$com$navdy$hud$app$maps$NavigationMode;
    final static int[] $SwitchMap$com$navdy$hud$app$maps$here$HereMapController$State;
    final static int[] $SwitchMap$com$navdy$service$library$events$navigation$NavigationSessionState;
    
    static {
        $SwitchMap$com$navdy$hud$app$maps$here$HereMapController$State = new int[com.navdy.hud.app.maps.here.HereMapController$State.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$maps$here$HereMapController$State;
        com.navdy.hud.app.maps.here.HereMapController$State a0 = com.navdy.hud.app.maps.here.HereMapController$State.AR_MODE;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        $SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit = new int[com.navdy.hud.app.manager.SpeedManager$SpeedUnit.values().length];
        int[] a1 = $SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit;
        com.navdy.hud.app.manager.SpeedManager$SpeedUnit a2 = com.navdy.hud.app.manager.SpeedManager$SpeedUnit.KILOMETERS_PER_HOUR;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit[com.navdy.hud.app.manager.SpeedManager$SpeedUnit.MILES_PER_HOUR.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException1) {
        }
        $SwitchMap$com$navdy$hud$app$maps$NavigationMode = new int[com.navdy.hud.app.maps.NavigationMode.values().length];
        int[] a3 = $SwitchMap$com$navdy$hud$app$maps$NavigationMode;
        com.navdy.hud.app.maps.NavigationMode a4 = com.navdy.hud.app.maps.NavigationMode.MAP;
        try {
            a3[a4.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$maps$NavigationMode[com.navdy.hud.app.maps.NavigationMode.MAP_ON_ROUTE.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException3) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$maps$NavigationMode[com.navdy.hud.app.maps.NavigationMode.TBT_ON_ROUTE.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException4) {
        }
        $SwitchMap$com$navdy$service$library$events$navigation$NavigationSessionState = new int[com.navdy.service.library.events.navigation.NavigationSessionState.values().length];
        int[] a5 = $SwitchMap$com$navdy$service$library$events$navigation$NavigationSessionState;
        com.navdy.service.library.events.navigation.NavigationSessionState a6 = com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_PAUSED;
        try {
            a5[a6.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException5) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$navigation$NavigationSessionState[com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STOPPED.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException6) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$navigation$NavigationSessionState[com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STARTED.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException7) {
        }
    }
}
