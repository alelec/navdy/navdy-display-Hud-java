package com.navdy.hud.app.maps.here;

class HereSafetySpotListener$3 implements Runnable {
    final com.navdy.hud.app.maps.here.HereSafetySpotListener this$0;
    final boolean val$enabled;
    
    HereSafetySpotListener$3(com.navdy.hud.app.maps.here.HereSafetySpotListener a, boolean b) {
        super();
        this.this$0 = a;
        this.val$enabled = b;
    }
    
    public void run() {
        if (com.navdy.hud.app.maps.here.HereSafetySpotListener.access$100(this.this$0) != this.val$enabled) {
            com.navdy.hud.app.maps.here.HereSafetySpotListener.access$102(this.this$0, this.val$enabled);
            if (com.navdy.hud.app.maps.here.HereSafetySpotListener.access$100(this.this$0)) {
                java.util.List a = com.navdy.hud.app.maps.here.HereSafetySpotListener.access$000(this.this$0);
                com.navdy.hud.app.maps.here.HereSafetySpotListener.access$300().d(new StringBuilder().append("Enabling the safety spots on the Map, Total spots Valid : ").append(com.navdy.hud.app.maps.here.HereSafetySpotListener.access$1100(this.this$0).size()).append(", Invalid : ").append(a).toString());
                java.util.ArrayList a0 = new java.util.ArrayList();
                Object a1 = com.navdy.hud.app.maps.here.HereSafetySpotListener.access$1100(this.this$0).iterator();
                while(((java.util.Iterator)a1).hasNext()) {
                    ((java.util.List)a0).add(com.navdy.hud.app.maps.here.HereSafetySpotListener$SafetySpotContainer.access$1000((com.navdy.hud.app.maps.here.HereSafetySpotListener$SafetySpotContainer)((java.util.Iterator)a1).next()));
                }
                com.navdy.hud.app.maps.here.HereSafetySpotListener.access$1200(this.this$0).addMapObjects((java.util.List)a0);
            } else {
                com.navdy.hud.app.maps.here.HereSafetySpotListener.access$300().d("Remove all the safety spots from the map");
                com.navdy.hud.app.maps.here.HereSafetySpotListener.access$200(this.this$0, com.navdy.hud.app.maps.here.HereSafetySpotListener.access$1100(this.this$0));
            }
        }
    }
}
