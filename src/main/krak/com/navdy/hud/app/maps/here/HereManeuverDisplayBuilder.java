package com.navdy.hud.app.maps.here;
import com.navdy.hud.app.R;

public class HereManeuverDisplayBuilder {
    final public static String ARRIVED;
    final public static String CLOSE_BRACKET = ")";
    final public static boolean COMBINE_HIGHWAY_MANEUVER = true;
    final public static String COMMA = ",";
    final static String DESTINATION_LEFT;
    final private static int DESTINATION_POINT_A_MIN_DISTANCE = 30;
    final static String DESTINATION_RIGHT;
    final private static com.navdy.hud.app.maps.util.DistanceConverter$Distance DISTANCE;
    final public static String EMPTY = "";
    final public static com.navdy.hud.app.maps.MapEvents$ManeuverDisplay EMPTY_MANEUVER_DISPLAY;
    final private static String END_TURN;
    final private static String ENTER_HIGHWAY;
    final private static String EXIT;
    final public static String EXIT_NUMBER;
    final private static String FEET_METER_FORMAT = "%.0f %s";
    final public static float FT_IN_METER = 3.28084f;
    final private static String HYPHEN;
    final public static float KM_DISPLAY_THRESHOLD = 400f;
    final private static String KM_FORMAT = "%.1f %s";
    final private static String KM_FORMAT_SHORT = "%.0f %s";
    final public static float METERS_IN_KM = 1000f;
    final public static float METERS_IN_MI = 1609.34f;
    final public static float MILES_DISPLAY_THRESHOLD = 160.934f;
    final private static String MILES_FORMAT = "%.1f %s";
    final private static String MILES_FORMAT_SHORT = "%.0f %s";
    final private static int MIN_STEP_FEET = 10;
    final private static int MIN_STEP_METERS = 5;
    final private static long NEXT_DISTANCE_THRESHOLD = 6437L;
    final private static long NEXT_DISTANCE_THRESHOLD_HIGHWAY = 16093L;
    final private static long NOW_DISTANCE_THRESHOLD = 30L;
    final private static long NOW_DISTANCE_THRESHOLD_HIGHWAY = 100L;
    final private static String ONTO;
    final public static String OPEN_BRACKET = "(";
    final public static int SHORT_DISTANCE_DISPLAY_THRESHOLD = 10;
    final public static boolean SHOW_DESTINATION_DIRECTION = true;
    final public static String SLASH = "/";
    final public static char SLASH_CHAR = (char)47;
    final private static float SMALL_UNITS_THRESHOLD = 0.1f;
    final public static boolean SMART_MANEUVER_CALCULATION = true;
    final private static long SOON_DISTANCE_THRESHOLD = 1287L;
    final private static long SOON_DISTANCE_THRESHOLD_HIGHWAY = 3218L;
    final public static String SPACE = " ";
    final public static char SPACE_CHAR = (char)32;
    final public static String START_TURN;
    final private static String STAY_ON;
    final private static long THEN_MANEUVER_THRESHOLD = 250L;
    final private static long THEN_MANEUVER_THRESHOLD_HIGHWAY = 1000L;
    final public static String TOWARDS;
    final private static String UNIT_FEET;
    final private static String UNIT_FEET_EXTENDED;
    final private static String UNIT_FEET_EXTENDED_SINGULAR;
    final private static String UNIT_KILOMETERS;
    final private static String UNIT_KILOMETERS_EXTENDED;
    final private static String UNIT_KILOMETERS_EXTENDED_SINGULAR;
    final private static String UNIT_METERS;
    final private static String UNIT_METERS_EXTENDED;
    final private static String UNIT_METERS_EXTENDED_SINGULAR;
    final private static String UNIT_MILES;
    final private static String UNIT_MILES_EXTENDED;
    final private static String UNIT_MILES_EXTENDED_SINGULAR;
    final private static String UTURN;
    final private static java.util.HashSet allowedTurnTextMap;
    private static com.here.android.mpa.routing.Maneuver lastStayManeuver;
    private static String lastStayManeuverRoadName;
    final private static StringBuilder mainSignPostBuilder;
    final private static android.content.res.Resources resources;
    final private static java.util.HashMap sHereIconToNavigationIconMap;
    final private static java.util.HashMap sHereTurnToNavigationTurnMap;
    final private static com.navdy.service.library.log.Logger sLogger;
    final private static java.util.Map sTurnIconImageMap;
    final private static java.util.Map sTurnIconSoonImageMap;
    final private static java.util.Map sTurnIconSoonImageMapGrey;
    final private static com.navdy.hud.app.manager.SpeedManager speedManager;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.class);
        resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        mainSignPostBuilder = new StringBuilder();
        speedManager = com.navdy.hud.app.manager.SpeedManager.getInstance();
        allowedTurnTextMap = new java.util.HashSet();
        EMPTY_MANEUVER_DISPLAY = new com.navdy.hud.app.maps.MapEvents$ManeuverDisplay();
        sHereIconToNavigationIconMap = new java.util.HashMap();
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.UNDEFINED, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_STRAIGHT);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.GO_STRAIGHT, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_STRAIGHT);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.KEEP_MIDDLE, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_STRAIGHT);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.KEEP_RIGHT, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_KEEP_RIGHT);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.LIGHT_RIGHT, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_EASY_RIGHT);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.QUITE_RIGHT, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_RIGHT);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.HEAVY_RIGHT, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_SHARP_RIGHT);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.KEEP_LEFT, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_KEEP_LEFT);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.LIGHT_LEFT, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_EASY_LEFT);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.QUITE_LEFT, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_LEFT);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.HEAVY_LEFT, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_SHARP_LEFT);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.UTURN_RIGHT, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_UTURN_RIGHT);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.UTURN_LEFT, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_UTURN_LEFT);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.ROUNDABOUT_1, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.ROUNDABOUT_2, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.ROUNDABOUT_3, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.ROUNDABOUT_4, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.ROUNDABOUT_5, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.ROUNDABOUT_6, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.ROUNDABOUT_7, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.ROUNDABOUT_8, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.ROUNDABOUT_9, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.ROUNDABOUT_10, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.ROUNDABOUT_11, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.ROUNDABOUT_12, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.ENTER_HIGHWAY_RIGHT_LANE, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_MERGE_LEFT);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.ENTER_HIGHWAY_LEFT_LANE, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_MERGE_RIGHT);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.LEAVE_HIGHWAY_RIGHT_LANE, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_EXIT_RIGHT);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.LEAVE_HIGHWAY_LEFT_LANE, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_EXIT_LEFT);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.HIGHWAY_KEEP_RIGHT, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_KEEP_RIGHT);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.HIGHWAY_KEEP_LEFT, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_KEEP_LEFT);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.ROUNDABOUT_1_LH, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.ROUNDABOUT_2_LH, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.ROUNDABOUT_3_LH, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.ROUNDABOUT_4_LH, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.ROUNDABOUT_5_LH, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.ROUNDABOUT_6_LH, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.ROUNDABOUT_7_LH, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.ROUNDABOUT_8_LH, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.ROUNDABOUT_9_LH, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.ROUNDABOUT_10_LH, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.ROUNDABOUT_11_LH, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.ROUNDABOUT_12_LH, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.START, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_START);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.END, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_END);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.FERRY, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_FERRY);
        sHereIconToNavigationIconMap.put(com.here.android.mpa.routing.Maneuver$Icon.HEAD_TO, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_STRAIGHT);
        ENTER_HIGHWAY = resources.getString(R.string.enter_highway);
        EXIT = resources.getString(R.string.exit);
        UTURN = resources.getString(R.string.uturn);
        EXIT_NUMBER = resources.getString(R.string.exit_number);
        START_TURN = resources.getString(R.string.start_maneuver_turn);
        END_TURN = resources.getString(R.string.end_maneuver_turn);
        TOWARDS = resources.getString(R.string.toward);
        ONTO = resources.getString(R.string.onto);
        STAY_ON = resources.getString(R.string.stay_on);
        HYPHEN = resources.getString(R.string.hyphen);
        DESTINATION_LEFT = resources.getString(R.string.left);
        DESTINATION_RIGHT = resources.getString(R.string.right);
        ARRIVED = resources.getString(R.string.arrived);
        sHereTurnToNavigationTurnMap = new java.util.HashMap();
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver$Turn.UNDEFINED, resources.getString(R.string.undefined_turn));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver$Turn.NO_TURN, resources.getString(R.string.no_turn));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver$Turn.KEEP_MIDDLE, resources.getString(R.string.keep_middle));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver$Turn.KEEP_RIGHT, resources.getString(R.string.keep_right));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver$Turn.LIGHT_RIGHT, resources.getString(R.string.slight_right));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver$Turn.QUITE_RIGHT, resources.getString(R.string.quite_right));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver$Turn.HEAVY_RIGHT, resources.getString(R.string.quite_right));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver$Turn.KEEP_LEFT, resources.getString(R.string.keep_left));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver$Turn.LIGHT_LEFT, resources.getString(R.string.slight_left));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver$Turn.QUITE_LEFT, resources.getString(R.string.quite_left));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver$Turn.HEAVY_LEFT, resources.getString(R.string.quite_left));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver$Turn.RETURN, UTURN);
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver$Turn.ROUNDABOUT_1, resources.getString(R.string.roundabout_1));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver$Turn.ROUNDABOUT_2, resources.getString(R.string.roundabout_2));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver$Turn.ROUNDABOUT_3, resources.getString(R.string.roundabout_3));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver$Turn.ROUNDABOUT_4, resources.getString(R.string.roundabout_4));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver$Turn.ROUNDABOUT_5, resources.getString(R.string.roundabout_5));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver$Turn.ROUNDABOUT_6, resources.getString(R.string.roundabout_6));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver$Turn.ROUNDABOUT_7, resources.getString(R.string.roundabout_7));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver$Turn.ROUNDABOUT_8, resources.getString(R.string.roundabout_8));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver$Turn.ROUNDABOUT_9, resources.getString(R.string.roundabout_9));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver$Turn.ROUNDABOUT_10, resources.getString(R.string.roundabout_10));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver$Turn.ROUNDABOUT_11, resources.getString(R.string.roundabout_11));
        sHereTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver$Turn.ROUNDABOUT_12, resources.getString(R.string.roundabout_12));
        UNIT_MILES = resources.getString(R.string.unit_miles);
        UNIT_FEET = resources.getString(R.string.unit_feet);
        UNIT_METERS = resources.getString(R.string.unit_meters);
        UNIT_KILOMETERS = resources.getString(R.string.unit_kilometers);
        UNIT_MILES_EXTENDED = resources.getString(R.string.unit_miles_ext);
        UNIT_FEET_EXTENDED = resources.getString(R.string.unit_feet_ext);
        UNIT_METERS_EXTENDED = resources.getString(R.string.unit_meters_ext);
        UNIT_KILOMETERS_EXTENDED = resources.getString(R.string.unit_kilometers_ext);
        UNIT_MILES_EXTENDED_SINGULAR = resources.getString(R.string.unit_miles_ext_singular);
        UNIT_FEET_EXTENDED_SINGULAR = resources.getString(R.string.unit_feet_ext_singular);
        UNIT_METERS_EXTENDED_SINGULAR = resources.getString(R.string.unit_meters_ext_singular);
        UNIT_KILOMETERS_EXTENDED_SINGULAR = resources.getString(R.string.unit_kilometers_ext_singular);
        sTurnIconImageMap = (java.util.Map)new java.util.HashMap();
        sTurnIconImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_START, Integer.valueOf(R.drawable.tbt_straight));
        sTurnIconImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_LEFT, Integer.valueOf(R.drawable.tbt_turn_left));
        sTurnIconImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_RIGHT, Integer.valueOf(R.drawable.tbt_turn_right));
        sTurnIconImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_EASY_RIGHT, Integer.valueOf(R.drawable.tbt_easy_right));
        sTurnIconImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_EASY_LEFT, Integer.valueOf(R.drawable.tbt_easy_left));
        sTurnIconImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_SHARP_LEFT, Integer.valueOf(R.drawable.tbt_sharp_left));
        sTurnIconImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_SHARP_RIGHT, Integer.valueOf(R.drawable.tbt_sharp_right));
        sTurnIconImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_EXIT_RIGHT, Integer.valueOf(R.drawable.tbt_exit_right));
        sTurnIconImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_EXIT_LEFT, Integer.valueOf(R.drawable.tbt_exit_left));
        sTurnIconImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_KEEP_LEFT, Integer.valueOf(R.drawable.tbt_stay_left));
        sTurnIconImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_KEEP_RIGHT, Integer.valueOf(R.drawable.tbt_stay_right));
        sTurnIconImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_END, Integer.valueOf(R.drawable.tbt_end));
        sTurnIconImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_STRAIGHT, Integer.valueOf(R.drawable.tbt_go_straight));
        sTurnIconImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_UTURN_LEFT, Integer.valueOf(R.drawable.tbt_u_turn_left));
        sTurnIconImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_UTURN_RIGHT, Integer.valueOf(R.drawable.tbt_u_turn_right));
        sTurnIconImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S, Integer.valueOf(R.drawable.tbt_left_roundabout));
        sTurnIconImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_N, Integer.valueOf(R.drawable.tbt_right_roundabout));
        sTurnIconImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_MERGE_LEFT, Integer.valueOf(R.drawable.tbt_merge_left));
        sTurnIconImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_MERGE_RIGHT, Integer.valueOf(R.drawable.tbt_merge_right));
        sTurnIconSoonImageMap = (java.util.Map)new java.util.HashMap();
        sTurnIconSoonImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_START, Integer.valueOf(R.drawable.tbt_straight_soon));
        sTurnIconSoonImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_LEFT, Integer.valueOf(R.drawable.tbt_turn_left_soon));
        sTurnIconSoonImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_RIGHT, Integer.valueOf(R.drawable.tbt_turn_right_soon));
        sTurnIconSoonImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_EASY_RIGHT, Integer.valueOf(R.drawable.tbt_easy_right_soon));
        sTurnIconSoonImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_EASY_LEFT, Integer.valueOf(R.drawable.tbt_easy_left_soon));
        sTurnIconSoonImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_SHARP_LEFT, Integer.valueOf(R.drawable.tbt_sharp_left_soon));
        sTurnIconSoonImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_SHARP_RIGHT, Integer.valueOf(R.drawable.tbt_sharp_right_soon));
        sTurnIconSoonImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_EXIT_RIGHT, Integer.valueOf(R.drawable.tbt_exit_right_soon));
        sTurnIconSoonImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_EXIT_LEFT, Integer.valueOf(R.drawable.tbt_exit_left_soon));
        sTurnIconSoonImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_KEEP_LEFT, Integer.valueOf(R.drawable.tbt_stay_left_soon));
        sTurnIconSoonImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_KEEP_RIGHT, Integer.valueOf(R.drawable.tbt_stay_right_soon));
        sTurnIconSoonImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_END, Integer.valueOf(R.drawable.tbt_end_soon));
        sTurnIconSoonImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_STRAIGHT, Integer.valueOf(R.drawable.tbt_go_straight_soon));
        sTurnIconSoonImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_UTURN_LEFT, Integer.valueOf(R.drawable.tbt_u_turn_left_soon));
        sTurnIconSoonImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_UTURN_RIGHT, Integer.valueOf(R.drawable.tbt_u_turn_right_soon));
        sTurnIconSoonImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S, Integer.valueOf(R.drawable.tbt_left_roundabout_soon));
        sTurnIconSoonImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_N, Integer.valueOf(R.drawable.tbt_right_roundabout_soon));
        sTurnIconSoonImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_MERGE_LEFT, Integer.valueOf(R.drawable.tbt_merge_left_soon));
        sTurnIconSoonImageMap.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_MERGE_RIGHT, Integer.valueOf(R.drawable.tbt_merge_right_soon));
        sTurnIconSoonImageMapGrey = (java.util.Map)new java.util.HashMap();
        sTurnIconSoonImageMapGrey.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_START, Integer.valueOf(R.drawable.icon_tbt_start_grey));
        sTurnIconSoonImageMapGrey.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_LEFT, Integer.valueOf(R.drawable.icon_tbt_quite_left_grey));
        sTurnIconSoonImageMapGrey.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_RIGHT, Integer.valueOf(R.drawable.icon_tbt_quite_right_grey));
        sTurnIconSoonImageMapGrey.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_EASY_RIGHT, Integer.valueOf(R.drawable.icon_tbt_light_right_grey));
        sTurnIconSoonImageMapGrey.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_EASY_LEFT, Integer.valueOf(R.drawable.icon_tbt_light_left_grey));
        sTurnIconSoonImageMapGrey.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_SHARP_LEFT, Integer.valueOf(R.drawable.icon_tbt_heavy_left_grey));
        sTurnIconSoonImageMapGrey.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_SHARP_RIGHT, Integer.valueOf(R.drawable.icon_tbt_heavy_right_grey));
        sTurnIconSoonImageMapGrey.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_EXIT_RIGHT, Integer.valueOf(R.drawable.icon_tbt_leave_highway_grey));
        sTurnIconSoonImageMapGrey.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_EXIT_LEFT, Integer.valueOf(R.drawable.icon_tbt_leave_highway_left_lane_grey));
        sTurnIconSoonImageMapGrey.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_KEEP_LEFT, Integer.valueOf(R.drawable.icon_tbt_keep_left_grey));
        sTurnIconSoonImageMapGrey.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_KEEP_RIGHT, Integer.valueOf(R.drawable.icon_tbt_keep_right_grey));
        sTurnIconSoonImageMapGrey.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_END, Integer.valueOf(R.drawable.icon_tbt_arrive_grey));
        sTurnIconSoonImageMapGrey.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_STRAIGHT, Integer.valueOf(R.drawable.icon_tbt_go_straight_grey));
        sTurnIconSoonImageMapGrey.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_UTURN_LEFT, Integer.valueOf(R.drawable.tbt_icon_return_grey));
        sTurnIconSoonImageMapGrey.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_UTURN_RIGHT, Integer.valueOf(R.drawable.icon_tbt_uturn_right_grey));
        sTurnIconSoonImageMapGrey.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S, Integer.valueOf(R.drawable.tbt_left_roundabout_soon));
        sTurnIconSoonImageMapGrey.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_N, Integer.valueOf(R.drawable.tbt_right_roundabout_soon));
        sTurnIconSoonImageMapGrey.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_MERGE_LEFT, Integer.valueOf(R.drawable.icon_tbt_merge_left_grey));
        sTurnIconSoonImageMapGrey.put(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_MERGE_RIGHT, Integer.valueOf(R.drawable.icon_tbt_merge_right_grey));
        EMPTY_MANEUVER_DISPLAY.empty = true;
        allowedTurnTextMap.add(EXIT);
        allowedTurnTextMap.add(UTURN);
        allowedTurnTextMap.add(START_TURN);
        allowedTurnTextMap.add(END_TURN);
        allowedTurnTextMap.add(ARRIVED);
        allowedTurnTextMap.add(sHereTurnToNavigationTurnMap.get(com.here.android.mpa.routing.Maneuver$Turn.ROUNDABOUT_1));
        allowedTurnTextMap.add(sHereTurnToNavigationTurnMap.get(com.here.android.mpa.routing.Maneuver$Turn.ROUNDABOUT_2));
        allowedTurnTextMap.add(sHereTurnToNavigationTurnMap.get(com.here.android.mpa.routing.Maneuver$Turn.ROUNDABOUT_3));
        allowedTurnTextMap.add(sHereTurnToNavigationTurnMap.get(com.here.android.mpa.routing.Maneuver$Turn.ROUNDABOUT_4));
        allowedTurnTextMap.add(sHereTurnToNavigationTurnMap.get(com.here.android.mpa.routing.Maneuver$Turn.ROUNDABOUT_5));
        allowedTurnTextMap.add(sHereTurnToNavigationTurnMap.get(com.here.android.mpa.routing.Maneuver$Turn.ROUNDABOUT_6));
        allowedTurnTextMap.add(sHereTurnToNavigationTurnMap.get(com.here.android.mpa.routing.Maneuver$Turn.ROUNDABOUT_7));
        allowedTurnTextMap.add(sHereTurnToNavigationTurnMap.get(com.here.android.mpa.routing.Maneuver$Turn.ROUNDABOUT_8));
        allowedTurnTextMap.add(sHereTurnToNavigationTurnMap.get(com.here.android.mpa.routing.Maneuver$Turn.ROUNDABOUT_9));
        allowedTurnTextMap.add(sHereTurnToNavigationTurnMap.get(com.here.android.mpa.routing.Maneuver$Turn.ROUNDABOUT_10));
        allowedTurnTextMap.add(sHereTurnToNavigationTurnMap.get(com.here.android.mpa.routing.Maneuver$Turn.ROUNDABOUT_11));
        allowedTurnTextMap.add(sHereTurnToNavigationTurnMap.get(com.here.android.mpa.routing.Maneuver$Turn.ROUNDABOUT_12));
        DISTANCE = new com.navdy.hud.app.maps.util.DistanceConverter$Distance();
    }
    
    public HereManeuverDisplayBuilder() {
    }
    
    public static boolean canShowTurnText(String s) {
        boolean b = false;
        boolean b0 = com.navdy.hud.app.maps.MapSettings.doNotShowTurnTextInTBT();
        label2: {
            label0: {
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!allowedTurnTextMap.contains(s)) {
                        break label0;
                    }
                }
                b = true;
                break label2;
            }
            b = false;
        }
        return b;
    }
    
    public static com.navdy.hud.app.maps.MapEvents$ManeuverDisplay getArrivedManeuverDisplay() {
        com.navdy.hud.app.maps.MapEvents$ManeuverDisplay a = new com.navdy.hud.app.maps.MapEvents$ManeuverDisplay();
        a.maneuverId = "arrived";
        a.turnIconId = R.drawable.icon_tbt_arrive;
        a.pendingTurn = ARRIVED;
        com.navdy.hud.app.maps.here.HereNavigationManager a0 = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
        String s = a0.getDestinationLabel();
        if (android.text.TextUtils.isEmpty((CharSequence)s)) {
            s = a0.getDestinationStreetAddress();
            if (android.text.TextUtils.isEmpty((CharSequence)s)) {
                s = "";
            }
        }
        a.pendingRoad = s;
        return a;
    }
    
    public static String getDestinationDirection(com.navdy.hud.app.maps.MapEvents$DestinationDirection a) {
        String s = null;
        switch(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$1.$SwitchMap$com$navdy$hud$app$maps$MapEvents$DestinationDirection[a.ordinal()]) {
            case 2: {
                s = DESTINATION_RIGHT;
                break;
            }
            case 1: {
                s = DESTINATION_LEFT;
                break;
            }
            default: {
                s = null;
            }
        }
        return s;
    }
    
    public static int getDestinationIcon(com.navdy.hud.app.maps.MapEvents$DestinationDirection a) {
        int i = 0;
        switch(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$1.$SwitchMap$com$navdy$hud$app$maps$MapEvents$DestinationDirection[a.ordinal()]) {
            case 2: {
                i = R.drawable.icon_pin_dot_destination_right;
                break;
            }
            case 1: {
                i = R.drawable.icon_pin_dot_destination_left;
                break;
            }
            default: {
                i = R.drawable.icon_pin_dot_destination;
            }
        }
        return i;
    }
    
    private static com.here.android.mpa.common.GeoCoordinate getEstimatedPointAfromManeuver(com.here.android.mpa.routing.Maneuver a, com.here.android.mpa.common.GeoCoordinate a0) {
        com.here.android.mpa.common.GeoCoordinate a1 = null;
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (a0 != null) {
                        break label0;
                    }
                }
                a1 = null;
                break label2;
            }
            java.util.List a2 = a.getManeuverGeometry();
            int i = a2.size() - 1;
            a1 = null;
            Object a3 = a2;
            while(true) {
                if (i < 0) {
                    if (a1 != null) {
                        sLogger.v(new StringBuilder().append("remaining=").append((int)a1.distanceTo(a0)).toString());
                    }
                    break;
                } else {
                    a1 = (com.here.android.mpa.common.GeoCoordinate)((java.util.List)a3).get(i);
                    int i0 = (int)a1.distanceTo(a0);
                    if (i0 < 30) {
                        i = i + -1;
                        continue;
                    }
                    sLogger.v(new StringBuilder().append("remaining=").append(i0).toString());
                    break;
                }
            }
        }
        return a1;
    }
    
    public static String getFormattedDistance(float f, com.navdy.service.library.events.navigation.DistanceUnit a, boolean b) {
        String s = null;
        switch(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$1.$SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit[a.ordinal()]) {
            case 4: {
                float f0 = com.navdy.hud.app.maps.here.HereMapUtil.roundToIntegerStep(5, f);
                String s0 = b ? (f <= 1f) ? UNIT_METERS_EXTENDED_SINGULAR : UNIT_METERS_EXTENDED : UNIT_METERS;
                Object[] a0 = new Object[2];
                a0[0] = Float.valueOf(f0);
                a0[1] = s0;
                s = String.format("%.0f %s", a0);
                break;
            }
            case 3: {
                String s1 = (f < 10f) ? "%.1f %s" : "%.0f %s";
                String s2 = b ? (f <= 1f) ? UNIT_KILOMETERS_EXTENDED_SINGULAR : UNIT_KILOMETERS_EXTENDED : UNIT_KILOMETERS;
                Object[] a1 = new Object[2];
                a1[0] = Float.valueOf(f);
                a1[1] = s2;
                s = String.format(s1, a1);
                break;
            }
            case 2: {
                float f1 = com.navdy.hud.app.maps.here.HereMapUtil.roundToIntegerStep(10, f);
                String s3 = b ? (f <= 1f) ? UNIT_FEET_EXTENDED_SINGULAR : UNIT_FEET_EXTENDED : UNIT_FEET;
                Object[] a2 = new Object[2];
                a2[0] = Float.valueOf(f1);
                a2[1] = s3;
                s = String.format("%.0f %s", a2);
                break;
            }
            case 1: {
                String s4 = (f < 10f) ? "%.1f %s" : "%.0f %s";
                String s5 = b ? (f <= 1f) ? UNIT_MILES_EXTENDED_SINGULAR : UNIT_MILES_EXTENDED : UNIT_MILES;
                Object[] a3 = new Object[2];
                a3[0] = Float.valueOf(f);
                a3[1] = s5;
                s = String.format(s4, a3);
                break;
            }
            default: {
                s = String.valueOf((int)f);
            }
        }
        return s;
    }
    
    public static com.navdy.hud.app.maps.MapEvents$ManeuverDisplay getManeuverDisplay(com.here.android.mpa.routing.Maneuver a, boolean b, long j, String s, com.here.android.mpa.routing.Maneuver a0, com.navdy.service.library.events.navigation.NavigationRouteRequest a1, com.here.android.mpa.routing.Maneuver a2, boolean b0, boolean b1, com.navdy.hud.app.maps.MapEvents$DestinationDirection a3) {
        com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$PendingRoadInfo a4 = new com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$PendingRoadInfo();
        com.navdy.hud.app.maps.MapEvents$ManeuverDisplay a5 = new com.navdy.hud.app.maps.MapEvents$ManeuverDisplay();
        a5.maneuverId = (a == null) ? "unknown" : String.valueOf(System.identityHashCode(a));
        com.here.android.mpa.common.RoadElement a6 = com.navdy.hud.app.maps.here.HereMapUtil.getCurrentRoadElement();
        boolean b2 = com.navdy.hud.app.maps.here.HereMapUtil.isCurrentRoadHighway(a);
        if (!b2) {
            b2 = com.navdy.hud.app.maps.here.HereMapUtil.isCurrentRoadHighway(a6);
        }
        if (b1) {
            if (j >= 2147483647L) {
                j = 0L;
            }
            a5.maneuverState = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.getManeuverState(j, b2);
        } else {
            a5.maneuverState = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.NOW;
        }
        com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.setNavigationTurnIconId(a, j, a5.maneuverState, a5, b0 ? a2 : null, b2, a3);
        com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.setNavigationDistance(j, a5, false, !b1);
        if (a5.maneuverState != com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.STAY) {
            lastStayManeuver = null;
            lastStayManeuverRoadName = null;
            a5.currentRoad = com.navdy.hud.app.maps.here.HereMapUtil.concatText(a.getRoadNumber(), a.getRoadName(), true, b2);
            if (android.text.TextUtils.isEmpty((CharSequence)a5.currentRoad)) {
                a5.currentRoad = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.getPendingRoadText(a, a0, a2, a4);
            }
        } else {
            if (a6 != null) {
                a5.currentRoad = com.navdy.hud.app.maps.here.HereMapUtil.concatText(a6.getRouteName(), a6.getRoadName(), true, b2);
                if (!android.text.TextUtils.isEmpty((CharSequence)a5.currentRoad)) {
                    lastStayManeuverRoadName = a5.currentRoad;
                    lastStayManeuver = a;
                }
            } else {
                a5.currentRoad = null;
            }
            if (android.text.TextUtils.isEmpty((CharSequence)a5.currentRoad)) {
                if (lastStayManeuver != a) {
                    lastStayManeuver = null;
                    lastStayManeuverRoadName = null;
                } else if (lastStayManeuverRoadName != null) {
                    a5.currentRoad = lastStayManeuverRoadName;
                }
            }
            if (android.text.TextUtils.isEmpty((CharSequence)a5.currentRoad)) {
                if (a0 != null) {
                    a5.currentRoad = com.navdy.hud.app.maps.here.HereMapUtil.concatText(a0.getNextRoadNumber(), a0.getNextRoadName(), true, b2);
                }
                if (a5.currentRoad == null) {
                    a5.currentRoad = "";
                }
            }
        }
        a5.currentSpeedLimit = com.navdy.hud.app.maps.here.HereMapUtil.getCurrentSpeedLimit();
        if (b) {
            a5.pendingTurn = END_TURN;
            String s0 = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getDestinationLabel();
            if (android.text.TextUtils.isEmpty((CharSequence)s0)) {
                s0 = (s == null) ? com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getDestinationStreetAddress() : com.navdy.hud.app.maps.here.HereMapUtil.parseStreetAddress(s);
                if (android.text.TextUtils.isEmpty((CharSequence)s0)) {
                    s0 = a.getRoadName();
                }
            }
            com.navdy.hud.app.maps.MapEvents$DestinationDirection a7 = null;
            label6: if (a1 != null) {
                com.navdy.service.library.events.location.Coordinate a8 = a1.destination;
                com.navdy.service.library.events.location.Coordinate a9 = a1.destinationDisplay;
                label4: {
                    label5: {
                        if (a8 == null) {
                            break label5;
                        }
                        if (a9 != null) {
                            break label4;
                        }
                    }
                    a5.direction = com.navdy.hud.app.maps.MapEvents$DestinationDirection.UNKNOWN;
                    sLogger.v("dest(1) cannot calculate direction pos not available");
                    a7 = null;
                    break label6;
                }
                com.here.android.mpa.common.GeoCoordinate a10 = new com.here.android.mpa.common.GeoCoordinate(a1.destinationDisplay.latitude.doubleValue(), a1.destinationDisplay.longitude.doubleValue());
                com.here.android.mpa.common.GeoCoordinate a11 = new com.here.android.mpa.common.GeoCoordinate(a1.destination.latitude.doubleValue(), a1.destination.longitude.doubleValue());
                if (a0 == null) {
                    com.here.android.mpa.routing.Route a12 = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getCurrentRoute();
                    if (a12 != null) {
                        java.util.List a13 = a12.getManeuvers();
                        if (a13 != null && a13.size() == 2) {
                            a0 = (com.here.android.mpa.routing.Maneuver)a13.get(0);
                        }
                    }
                }
                com.here.android.mpa.common.GeoCoordinate a14 = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.getEstimatedPointAfromManeuver(a0, a11);
                if (a14 == null) {
                    a5.direction = com.navdy.hud.app.maps.MapEvents$DestinationDirection.UNKNOWN;
                    sLogger.v("dest(1) cannot calculate direction, A pos not available");
                    a7 = null;
                } else {
                    com.navdy.hud.app.maps.MapEvents$DestinationDirection a15 = null;
                    double d = Math.sin((a11.getLatitude() - a14.getLatitude()) * (a10.getLongitude() - a14.getLongitude()) - (a11.getLongitude() - a14.getLongitude()) * (a10.getLatitude() - a14.getLatitude()));
                    a7 = (d >= 0.0) ? com.navdy.hud.app.maps.MapEvents$DestinationDirection.RIGHT : (d < 0.0) ? com.navdy.hud.app.maps.MapEvents$DestinationDirection.LEFT : com.navdy.hud.app.maps.MapEvents$DestinationDirection.UNKNOWN;
                    sLogger.v(new StringBuilder().append("dest(1) position=").append(d).append(" DIRECTION = ").append(a7).toString());
                    a5.direction = a7;
                    if (a7 != com.navdy.hud.app.maps.MapEvents$DestinationDirection.UNKNOWN) {
                        a5.destinationIconId = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.getDestinationIcon(a7);
                    }
                    double d0 = a11.getHeading(a10);
                    double d1 = a14.getHeading(a11);
                    int i = (int)(d0 - d1);
                    sLogger.v(new StringBuilder().append("dest(2): navToDisplayAngle=").append((int)d0).append(" currentHereAngle=").append((int)d1).append(" diffAngle =").append(i).toString());
                    if (i < 0) {
                        i = i + 360;
                    }
                    label2: {
                        label3: {
                            if (i < 1) {
                                break label3;
                            }
                            if (i > 179) {
                                break label3;
                            }
                            a15 = com.navdy.hud.app.maps.MapEvents$DestinationDirection.RIGHT;
                            break label2;
                        }
                        label0: {
                            label1: {
                                if (i < 181) {
                                    break label1;
                                }
                                if (i <= 359) {
                                    break label0;
                                }
                            }
                            a15 = com.navdy.hud.app.maps.MapEvents$DestinationDirection.UNKNOWN;
                            break label2;
                        }
                        a15 = com.navdy.hud.app.maps.MapEvents$DestinationDirection.LEFT;
                    }
                    sLogger.v(new StringBuilder().append("dest(2): diffAngle=").append(i).append(" direction=").append(a15).toString());
                }
            }
            if (a7 == null) {
                android.content.res.Resources a16 = resources;
                if (android.text.TextUtils.isEmpty((CharSequence)s0)) {
                    s0 = resources.getString(R.string.end_maneuver_direction_no_road);
                }
                Object[] a17 = new Object[1];
                a17[0] = s0;
                a5.pendingRoad = a16.getString(R.string.end_maneuver, a17);
            } else {
                android.content.res.Resources a18 = resources;
                if (android.text.TextUtils.isEmpty((CharSequence)s0)) {
                    s0 = resources.getString(R.string.end_maneuver_direction_no_road);
                }
                Object[] a19 = new Object[2];
                a19[0] = s0;
                a19[1] = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.getDestinationDirection(a7);
                a5.pendingRoad = a18.getString(R.string.end_maneuver_direction, a19);
            }
        } else {
            a5.pendingTurn = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.getTurnText(a, a4, a5.maneuverState, a0, a2);
            if (a5.maneuverState != com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.STAY) {
                String s1 = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.getPendingRoadText(a, a0, a2, a4);
                if (android.text.TextUtils.isEmpty((CharSequence)s1)) {
                    s1 = com.navdy.hud.app.maps.here.HereMapUtil.getRoadName(a.getRoadElements());
                }
                if (android.text.TextUtils.isEmpty((CharSequence)s1)) {
                    a5.pendingRoad = "";
                } else {
                    a5.pendingRoad = s1;
                }
            } else {
                a5.pendingRoad = a5.currentRoad;
            }
        }
        return a5;
    }
    
    static com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState getManeuverState(long j, boolean b) {
        double d = 0.0;
        double d0 = 0.0;
        long j0 = 0L;
        com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState a = null;
        if (b) {
            d = 16093.0;
            d0 = 3218.0;
            j0 = 100L;
        } else {
            d = 6437.0;
            d0 = 1287.0;
            j0 = 30L;
        }
        if (j > j0) {
            if ((double)j <= d0) {
                a = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.SOON;
            } else {
                a = ((double)j <= d) ? com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.NEXT : com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.STAY;
            }
        } else {
            a = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.NOW;
        }
        if (sLogger.isLoggable(2)) {
            sLogger.v(new StringBuilder().append("maneuverState=").append(a).append(" distance=").append(j).append(" isHighway=").append(b).toString());
        }
        return a;
    }
    
    private static com.navdy.service.library.events.navigation.NavigationTurn getNavigationTurn(com.here.android.mpa.routing.Maneuver a) {
        com.here.android.mpa.routing.Maneuver$Icon a0 = a.getIcon();
        return (a0 != null) ? (com.navdy.service.library.events.navigation.NavigationTurn)sHereIconToNavigationIconMap.get(a0) : null;
    }
    
    private static String getPendingRoadText(com.here.android.mpa.routing.Maneuver a, com.here.android.mpa.routing.Maneuver a0, com.here.android.mpa.routing.Maneuver a1, com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$PendingRoadInfo a2) {
        boolean b = false;
        boolean b0 = false;
        String s = null;
        if (a2 != null && a2.usePrevManeuverInfo && a0 != null) {
            a = a0;
            a0 = null;
            a1 = null;
        }
        String s0 = a.getNextRoadNumber();
        String s1 = a.getNextRoadName();
        com.here.android.mpa.routing.Signpost a3 = a.getSignpost();
        com.navdy.hud.app.maps.here.HereMapUtil$SignPostInfo a4 = new com.navdy.hud.app.maps.here.HereMapUtil$SignPostInfo();
        String s2 = com.navdy.hud.app.maps.here.HereMapUtil.getSignpostText(a3, a, a4, (java.util.ArrayList)null);
        label6: if (a2 != null && a2.dontUseSignpostText) {
            boolean b1 = android.text.TextUtils.isEmpty((CharSequence)s0);
            label7: {
                if (!b1) {
                    break label7;
                }
                if (android.text.TextUtils.isEmpty((CharSequence)s1)) {
                    break label6;
                }
            }
            a4.hasNameInSignPost = false;
            a4.hasNumberInSignPost = false;
            s2 = null;
        }
        boolean b2 = com.navdy.hud.app.maps.here.HereMapUtil.hasSameSignpostAndToRoad(a, a0);
        if (b2) {
            b = false;
            b0 = true;
        } else {
            b2 = com.navdy.hud.app.maps.here.HereMapUtil.hasSameSignpostAndToRoad(a, a1);
            if (b2) {
                b = true;
                b0 = false;
            } else {
                b = false;
                b0 = false;
            }
        }
        synchronized(mainSignPostBuilder) {
            StringBuilder a6 = mainSignPostBuilder;
            boolean b3 = false;
            boolean b4 = false;
            boolean b5 = false;
            a6.setLength(0);
            if (!android.text.TextUtils.isEmpty((CharSequence)s0) && !a4.hasNumberInSignPost) {
                mainSignPostBuilder.append(s0);
            }
            if (android.text.TextUtils.isEmpty((CharSequence)s2)) {
                b3 = false;
            } else {
                boolean b6 = false;
                if (b2) {
                    if (b) {
                        b6 = true;
                    } else {
                        s2 = "";
                        b6 = false;
                    }
                } else {
                    b6 = true;
                }
                if (b6) {
                    if (com.navdy.hud.app.maps.here.HereMapUtil.needSeparator(mainSignPostBuilder)) {
                        mainSignPostBuilder.append("/");
                    }
                    mainSignPostBuilder.append(s2);
                    b3 = true;
                } else {
                    b3 = true;
                }
            }
            if (android.text.TextUtils.isEmpty((CharSequence)s1)) {
                b4 = false;
                b5 = false;
            } else {
                String s3 = null;
                if (b3) {
                    boolean b7 = s2.contains((CharSequence)s1);
                    s3 = null;
                    if (b7) {
                        b5 = false;
                        b4 = false;
                    } else {
                        boolean b8 = !b2 || b0;
                        s3 = null;
                        label2: if (b8) {
                            label5: {
                                label4: {
                                    if (a2 == null) {
                                        break label4;
                                    }
                                    if (!a2.hasTo) {
                                        break label4;
                                    }
                                    s3 = mainSignPostBuilder.toString();
                                    mainSignPostBuilder.setLength(0);
                                    break label5;
                                }
                                boolean b9 = com.navdy.hud.app.maps.here.HereMapUtil.needSeparator(mainSignPostBuilder);
                                s3 = null;
                                if (b9) {
                                    mainSignPostBuilder.append(" ");
                                    s3 = null;
                                }
                            }
                            if (b0) {
                                if (com.navdy.hud.app.maps.here.HereMapUtil.needSeparator(mainSignPostBuilder)) {
                                    mainSignPostBuilder.append(" ");
                                    b5 = false;
                                } else {
                                    b5 = false;
                                }
                            } else if (com.navdy.hud.app.maps.MapSettings.isTbtOntoDisabled()) {
                                b5 = false;
                            } else {
                                mainSignPostBuilder.append(ONTO);
                                mainSignPostBuilder.append(" ");
                                b5 = true;
                            }
                            if (android.text.TextUtils.isEmpty((CharSequence)s3)) {
                                if (a2 == null) {
                                    b4 = false;
                                } else if (a2.hasTo) {
                                    a2.hasTo = false;
                                    if (!com.navdy.hud.app.maps.MapSettings.doNotShowTurnTextInTBT()) {
                                        mainSignPostBuilder.append(TOWARDS);
                                        mainSignPostBuilder.append(" ");
                                    }
                                    b4 = true;
                                } else {
                                    b4 = false;
                                }
                            } else {
                                b4 = false;
                            }
                            if (com.navdy.hud.app.maps.MapSettings.isTbtOntoDisabled()) {
                                label3: {
                                    if (a2 == null) {
                                        break label3;
                                    }
                                    if (a2.hasTo) {
                                        break label2;
                                    }
                                }
                                if (android.text.TextUtils.isEmpty((CharSequence)s2)) {
                                    mainSignPostBuilder.append(com.navdy.hud.app.maps.here.HereMapUtil.concatText(s0, s1, true, com.navdy.hud.app.maps.here.HereMapUtil.isCurrentRoadHighway(a)));
                                }
                            } else {
                                mainSignPostBuilder.append(com.navdy.hud.app.maps.here.HereMapUtil.concatText(s0, s1, true, com.navdy.hud.app.maps.here.HereMapUtil.isCurrentRoadHighway(a)));
                            }
                        } else {
                            b5 = false;
                            b4 = false;
                        }
                        a4.hasNameInSignPost = true;
                    }
                } else {
                    b5 = false;
                    b4 = false;
                }
                if (!a4.hasNameInSignPost && (mainSignPostBuilder.length() <= 0 || !com.navdy.hud.app.maps.here.HereMapUtil.isCurrentRoadHighway(a))) {
                    boolean b10 = false;
                    if (com.navdy.hud.app.maps.here.HereMapUtil.needSeparator(mainSignPostBuilder)) {
                        mainSignPostBuilder.append(" ");
                    }
                    if (android.text.TextUtils.isEmpty((CharSequence)s0)) {
                        b10 = false;
                    } else {
                        mainSignPostBuilder.append("(");
                        b10 = true;
                    }
                    mainSignPostBuilder.append(s1);
                    if (b10) {
                        mainSignPostBuilder.append(")");
                    }
                }
                if (android.text.TextUtils.isEmpty((CharSequence)s3)) {
                    if (a2 != null && a2.hasTo) {
                        String s4 = mainSignPostBuilder.toString();
                        mainSignPostBuilder.setLength(0);
                        if (!com.navdy.hud.app.maps.MapSettings.doNotShowTurnTextInTBT()) {
                            mainSignPostBuilder.append(TOWARDS);
                            mainSignPostBuilder.append(" ");
                        }
                        mainSignPostBuilder.append(s4);
                        a2.hasTo = false;
                        b4 = true;
                    }
                } else {
                    String s5 = mainSignPostBuilder.toString();
                    int i = s5.indexOf(new StringBuilder().append(ONTO).append(" ").toString());
                    if (i != -1) {
                        s5 = s5.substring(ONTO.length() + i + " ".length()).trim();
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)s5) && s3.contains((CharSequence)s5)) {
                        mainSignPostBuilder.setLength(0);
                    }
                    if (com.navdy.hud.app.maps.here.HereMapUtil.needSeparator(mainSignPostBuilder)) {
                        mainSignPostBuilder.append(" ");
                    }
                    if (!com.navdy.hud.app.maps.MapSettings.doNotShowTurnTextInTBT()) {
                        mainSignPostBuilder.append(TOWARDS);
                        mainSignPostBuilder.append(" ");
                    }
                    mainSignPostBuilder.append(s3);
                    a2.hasTo = false;
                    b4 = true;
                }
            }
            label1: if (a2 != null) {
                boolean b11 = com.navdy.hud.app.maps.MapSettings.isTbtOntoDisabled();
                label0: {
                    if (b11) {
                        break label0;
                    }
                    if (!a2.enterHighway) {
                        break label0;
                    }
                    if (b5) {
                        break label0;
                    }
                    if (b4) {
                        break label0;
                    }
                    if (mainSignPostBuilder.length() <= 0) {
                        break label0;
                    }
                    String s6 = mainSignPostBuilder.toString();
                    mainSignPostBuilder.setLength(0);
                    mainSignPostBuilder.append(ONTO);
                    mainSignPostBuilder.append(" ");
                    mainSignPostBuilder.append(s6);
                    break label1;
                }
                if (a2.hasTo && mainSignPostBuilder.length() > 0) {
                    String s7 = mainSignPostBuilder.toString();
                    mainSignPostBuilder.setLength(0);
                    if (!com.navdy.hud.app.maps.MapSettings.doNotShowTurnTextInTBT()) {
                        mainSignPostBuilder.append(TOWARDS);
                        mainSignPostBuilder.append(" ");
                    }
                    mainSignPostBuilder.append(s7);
                }
            }
            s = mainSignPostBuilder.toString();
            /*monexit(a5)*/;
        }
        return s;
    }
    
    public static com.navdy.hud.app.maps.MapEvents$ManeuverDisplay getStartManeuverDisplay(com.here.android.mpa.routing.Maneuver a, com.here.android.mpa.routing.Maneuver a0) {
        com.navdy.hud.app.maps.MapEvents$ManeuverDisplay a1 = new com.navdy.hud.app.maps.MapEvents$ManeuverDisplay();
        boolean b = com.navdy.hud.app.maps.here.HereMapUtil.isCurrentRoadHighway(a);
        boolean b0 = com.navdy.hud.app.maps.here.HereMapUtil.isCurrentRoadHighway(a0);
        String s = com.navdy.hud.app.maps.here.HereMapUtil.concatText(a.getRoadNumber(), a.getRoadName(), true, b);
        String s0 = com.navdy.hud.app.maps.here.HereMapUtil.concatText(a0.getRoadNumber(), a0.getRoadName(), true, b0);
        if (android.text.TextUtils.isEmpty((CharSequence)s)) {
            String s1 = com.navdy.hud.app.maps.here.HereMapUtil.concatText(a0.getNextRoadNumber(), a0.getNextRoadName(), b0, b0);
            if (android.text.TextUtils.isEmpty((CharSequence)s1)) {
                s = s0;
            } else {
                s = s0;
                s0 = s1;
            }
        }
        if (android.text.TextUtils.isEmpty((CharSequence)s)) {
            s = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.getPendingRoadText(a, (com.here.android.mpa.routing.Maneuver)null, (com.here.android.mpa.routing.Maneuver)null, (com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$PendingRoadInfo)null);
        }
        a1.turnIconId = ((Integer)sTurnIconImageMap.get(com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_START)).intValue();
        a1.pendingTurn = START_TURN;
        a1.currentRoad = com.navdy.hud.app.maps.here.HereMapUtil.concatText(a.getRoadNumber(), a.getRoadName(), true, b);
        a1.currentSpeedLimit = com.navdy.hud.app.maps.here.HereMapUtil.getCurrentSpeedLimit();
        if (android.text.TextUtils.equals((CharSequence)s, (CharSequence)s0)) {
            if (android.text.TextUtils.isEmpty((CharSequence)s)) {
                a1.pendingRoad = "";
            } else {
                android.content.res.Resources a2 = resources;
                Object[] a3 = new Object[1];
                a3[0] = s;
                a1.pendingRoad = a2.getString(R.string.start_maneuver_no_next_road, a3);
            }
        } else {
            android.content.res.Resources a4 = resources;
            Object[] a5 = new Object[2];
            a5[0] = s;
            a5[1] = s0;
            a1.pendingRoad = a4.getString(R.string.start_maneuver, a5);
        }
        com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.setNavigationDistance(0L, a1, false, false);
        a1.totalDistanceRemaining = a1.totalDistance;
        a1.totalDistanceRemainingUnit = a1.totalDistanceUnit;
        a1.distanceToPendingRoadText = "";
        a1.navigationTurn = com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_START;
        a1.maneuverId = (a == null) ? "start" : String.valueOf(System.identityHashCode(a));
        return a1;
    }
    
    private static String getTurnText(com.here.android.mpa.routing.Maneuver a, com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$PendingRoadInfo a0, com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState a1, com.here.android.mpa.routing.Maneuver a2, com.here.android.mpa.routing.Maneuver a3) {
        String s = null;
        com.here.android.mpa.routing.Maneuver$Action a4 = a.getAction();
        com.here.android.mpa.routing.Maneuver$Turn a5 = a.getTurn();
        label1: if (a1 != com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.STAY) {
            boolean b = com.navdy.hud.app.maps.here.HereMapUtil.isHighwayAction(a);
            label2: {
                label6: {
                    if (!b) {
                        break label6;
                    }
                    com.here.android.mpa.routing.Maneuver$Action a6 = com.here.android.mpa.routing.Maneuver$Action.ENTER_HIGHWAY_FROM_LEFT;
                    label4: {
                        label5: {
                            if (a4 == a6) {
                                break label5;
                            }
                            if (a4 != com.here.android.mpa.routing.Maneuver$Action.ENTER_HIGHWAY_FROM_RIGHT) {
                                break label4;
                            }
                        }
                        s = ENTER_HIGHWAY;
                        break label1;
                    }
                    if (a2 == null) {
                        break label2;
                    }
                    if (a3 == null) {
                        break label2;
                    }
                    if (com.navdy.hud.app.maps.here.HereMapUtil.isHighwayAction(a2)) {
                        a0.enterHighway = true;
                        a0.usePrevManeuverInfo = true;
                        a5 = a2.getTurn();
                        break label2;
                    } else {
                        if (!com.navdy.hud.app.maps.here.HereMapUtil.isHighwayAction(a3)) {
                            break label2;
                        }
                        a0.enterHighway = true;
                        break label2;
                    }
                }
                com.here.android.mpa.routing.Maneuver$Action a7 = com.here.android.mpa.routing.Maneuver$Action.LEAVE_HIGHWAY;
                label3: {
                    if (a4 == a7) {
                        break label3;
                    }
                    if (a4 != com.here.android.mpa.routing.Maneuver$Action.UTURN) {
                        break label2;
                    }
                    s = UTURN;
                    break label1;
                }
                String s0 = com.navdy.hud.app.maps.here.HereMapUtil.getRoadName(a);
                String s1 = com.navdy.hud.app.maps.here.HereMapUtil.getNextRoadName(a);
                boolean b0 = android.text.TextUtils.equals((CharSequence)s0, (CharSequence)s1);
                label0: {
                    if (b0) {
                        break label0;
                    }
                    s = EXIT;
                    break label1;
                }
                sLogger.v(new StringBuilder().append("ignoring action[").append(a4).append("] using[").append(a5).append("] from[").append(s0).append("] to[").append(s1).append("]").toString());
            }
            switch(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$1.$SwitchMap$com$here$android$mpa$routing$Maneuver$Turn[a5.ordinal()]) {
                case 5: case 6: case 7: {
                    a0.hasTo = true;
                    break;
                }
                case 1: case 2: case 3: case 4: {
                    a0.dontUseSignpostText = true;
                    break;
                }
            }
            s = (String)sHereTurnToNavigationTurnMap.get(a5);
        } else {
            s = STAY_ON;
        }
        return s;
    }
    
    public static String getUnitDisplayStr(com.navdy.hud.app.manager.SpeedManager$SpeedUnit a) {
        String s = null;
        switch(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$1.$SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit[a.ordinal()]) {
            case 3: {
                s = UNIT_METERS;
                break;
            }
            case 2: {
                s = UNIT_KILOMETERS;
                break;
            }
            case 1: {
                s = UNIT_MILES;
                break;
            }
            default: {
                s = "";
            }
        }
        return s;
    }
    
    public static void setNavigationDistance(long j, com.navdy.hud.app.maps.MapEvents$ManeuverDisplay a, boolean b, boolean b0) {
        synchronized(DISTANCE) {
            com.navdy.hud.app.manager.SpeedManager a1 = speedManager;
            com.navdy.hud.app.manager.SpeedManager$SpeedUnit a2 = a1.getSpeedUnit();
            if (j >= 2147483647L) {
                j = 0L;
            }
            a.distanceInMeters = j;
            com.navdy.hud.app.maps.util.DistanceConverter.convertToDistance(a2, (float)j, DISTANCE);
            a.distance = DISTANCE.value;
            a.distanceUnit = DISTANCE.unit;
            a.distanceToPendingRoadText = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.getFormattedDistance(a.distance, a.distanceUnit, b);
            if (b0) {
                /*monexit(a0)*/;
            } else {
                float f = 0.0f;
                float f0 = 0.0f;
                com.navdy.hud.app.maps.here.HereNavigationManager a3 = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
                if (a3.isNavigationModeOn()) {
                    com.here.android.mpa.routing.Route a4 = a3.getCurrentRoute();
                    if (a4 == null) {
                        f = -1f;
                        f0 = -1f;
                    } else {
                        f = (float)a3.getNavController().getDestinationDistance();
                        if (f < 0.0f) {
                            f = 0.0f;
                        }
                        f0 = (float)a4.getLength();
                    }
                } else {
                    f = -1f;
                    f0 = -1f;
                }
                if (f0 != -1f) {
                    com.navdy.hud.app.maps.util.DistanceConverter.convertToDistance(a2, f0, DISTANCE);
                    a.totalDistance = DISTANCE.value;
                    a.totalDistanceUnit = DISTANCE.unit;
                    com.navdy.hud.app.maps.util.DistanceConverter.convertToDistance(a2, f, DISTANCE);
                    a.totalDistanceRemaining = DISTANCE.value;
                    a.totalDistanceRemainingUnit = DISTANCE.unit;
                }
                /*monexit(a0)*/;
            }
        }
    }
    
    private static void setNavigationTurnIconId(com.here.android.mpa.routing.Maneuver a, long j, com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState a0, com.navdy.hud.app.maps.MapEvents$ManeuverDisplay a1, com.here.android.mpa.routing.Maneuver a2, boolean b, com.navdy.hud.app.maps.MapEvents$DestinationDirection a3) {
        Integer a4 = null;
        Integer a5 = null;
        Integer a6 = null;
        switch(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$1.$SwitchMap$com$navdy$hud$app$maps$here$HereManeuverDisplayBuilder$ManeuverState[a0.ordinal()]) {
            case 2: case 3: case 4: {
                com.navdy.service.library.events.navigation.NavigationTurn a7 = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.getNavigationTurn(a);
                a1.navigationTurn = a7;
                com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState a8 = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.NOW;
                label2: {
                    label0: {
                        label1: {
                            if (a0 == a8) {
                                break label1;
                            }
                            if (a0 != com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.SOON) {
                                break label0;
                            }
                        }
                        a4 = (Integer)sTurnIconImageMap.get(a7);
                        break label2;
                    }
                    a4 = (Integer)sTurnIconSoonImageMap.get(a7);
                }
                a5 = (Integer)sTurnIconImageMap.get(a7);
                a6 = (Integer)sTurnIconSoonImageMapGrey.get(a7);
                if (a1.navigationTurn == com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_END && a3 != null && a3 != com.navdy.hud.app.maps.MapEvents$DestinationDirection.UNKNOWN) {
                    if (a0 != com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.NOW) {
                        switch(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$1.$SwitchMap$com$navdy$hud$app$maps$MapEvents$DestinationDirection[a3.ordinal()]) {
                            case 2: {
                                a4 = Integer.valueOf(R.drawable.tbt_arrive_right_soon);
                                break;
                            }
                            case 1: {
                                a4 = Integer.valueOf(R.drawable.tbt_arrive_left_soon);
                                break;
                            }
                        }
                    } else {
                        switch(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$1.$SwitchMap$com$navdy$hud$app$maps$MapEvents$DestinationDirection[a3.ordinal()]) {
                            case 2: {
                                a4 = Integer.valueOf(R.drawable.tbt_arrive_right);
                                break;
                            }
                            case 1: {
                                a4 = Integer.valueOf(R.drawable.tbt_arrive_left);
                                break;
                            }
                        }
                    }
                }
                if (a2 == null) {
                    a1.turnIconId = -1;
                    a1.turnIconNowId = 0;
                    a1.turnIconSoonId = 0;
                    break;
                } else if (j <= (b ? 3218L : 1287L)) {
                    long j0 = (long)a2.getDistanceFromPreviousManeuver();
                    if (j0 > (b ? 1000L : 250L)) {
                        break;
                    }
                    com.navdy.service.library.events.navigation.NavigationTurn a9 = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.getNavigationTurn(a2);
                    Integer a10 = (Integer)sTurnIconSoonImageMap.get(a9);
                    if (a4 != null) {
                        a1.nextTurnIconId = a10.intValue();
                    } else {
                        a1.nextTurnIconId = -1;
                    }
                    sLogger.v(new StringBuilder().append("distance is less than threshold ").append(j0).toString());
                    break;
                } else {
                    a1.nextTurnIconId = -1;
                    break;
                }
            }
            case 1: {
                a4 = Integer.valueOf(R.drawable.tbt_go_straight_soon);
                a1.navigationTurn = com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_STRAIGHT;
                a6 = null;
                a5 = null;
                break;
            }
            default: {
                a4 = null;
                a6 = null;
                a5 = null;
            }
        }
        if (a4 != null) {
            a1.turnIconId = a4.intValue();
            a1.turnIconSoonId = (a6 != null) ? a6.intValue() : 0;
            a1.turnIconNowId = (a5 != null) ? a5.intValue() : 0;
        } else {
            a1.turnIconId = -1;
            a1.turnIconNowId = 0;
            a1.turnIconSoonId = 0;
        }
    }
    
    public static boolean shouldShowTurnText(String s) {
        return allowedTurnTextMap.contains(s);
    }
}
