package com.navdy.hud.app.profile;

public class NotificationSettings {
    final public static String APP_ID_CALENDAR = "com.apple.mobilecal";
    final public static String APP_ID_MAIL = "com.apple.mobilemail";
    final public static String APP_ID_PHONE = "com.apple.mobilephone";
    final public static String APP_ID_REMINDERS = "com.apple.reminders";
    final public static String APP_ID_SMS = "com.apple.MobileSMS";
    private static com.navdy.service.library.log.Logger sLogger;
    private java.util.Map enabledApps;
    final private Object lock;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.profile.NotificationSettings.class);
    }
    
    public NotificationSettings(com.navdy.service.library.events.preferences.NotificationPreferences a) {
        this.enabledApps = (java.util.Map)new java.util.HashMap();
        this.lock = new Object();
        this.update(a);
    }
    
    public Boolean enabled(com.navdy.ancs.AppleNotification a) {
        return this.enabled(a.getAppId());
    }
    
    public Boolean enabled(String s) {
        Boolean a = null;
        synchronized(this.lock) {
            a = (Boolean)this.enabledApps.get(s);
            /*monexit(a0)*/;
        }
        return a;
    }
    
    public java.util.List enabledApps() {
        java.util.ArrayList a = new java.util.ArrayList();
        synchronized(this.lock) {
            Object a1 = this.enabledApps.entrySet().iterator();
            while(((java.util.Iterator)a1).hasNext()) {
                Object a2 = ((java.util.Iterator)a1).next();
                if (Boolean.TRUE.equals(((java.util.Map.Entry)a2).getValue())) {
                    ((java.util.List)a).add(((java.util.Map.Entry)a2).getKey());
                }
            }
            /*monexit(a0)*/;
        }
        return (java.util.List)a;
    }
    
    public void update(com.navdy.service.library.events.preferences.NotificationPreferences a) {
        synchronized(this.lock) {
            this.enabledApps.clear();
            if (a != null && a.settings != null) {
                Object a1 = a.settings.iterator();
                while(((java.util.Iterator)a1).hasNext()) {
                    com.navdy.service.library.events.notification.NotificationSetting a2 = (com.navdy.service.library.events.notification.NotificationSetting)((java.util.Iterator)a1).next();
                    this.enabledApps.put(a2.app, a2.enabled);
                }
            }
            /*monexit(a0)*/;
        }
    }
}
