package com.navdy.hud.app.profile;

class DriverProfileManager$2 implements Runnable {
    final com.navdy.hud.app.profile.DriverProfileManager this$0;
    final com.navdy.service.library.device.NavdyDeviceId val$deviceId;
    
    DriverProfileManager$2(com.navdy.hud.app.profile.DriverProfileManager a, com.navdy.service.library.device.NavdyDeviceId a0) {
        super();
        this.this$0 = a;
        this.val$deviceId = a0;
    }
    
    public void run() {
        com.navdy.hud.app.profile.DriverProfileManager.access$000().v(new StringBuilder().append("loading profile:").append(this.val$deviceId).toString());
        com.navdy.hud.app.profile.DriverProfile a = this.this$0.getProfileForId(this.val$deviceId);
        if (a == null) {
            com.navdy.hud.app.profile.DriverProfileManager.access$000().w(new StringBuilder().append("profile not loaded:").append(this.val$deviceId).toString());
            a = this.this$0.createProfileForId(this.val$deviceId);
        }
        this.this$0.setCurrentProfile(a);
    }
}
