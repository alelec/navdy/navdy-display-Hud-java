package com.navdy.hud.app.profile;
import com.navdy.hud.app.R;

final class HudLocale$1 implements com.navdy.hud.app.framework.toast.IToastCallback {
    android.animation.ObjectAnimator animator;
    final android.content.res.Resources val$resources;
    
    HudLocale$1(android.content.res.Resources a) {
        super();
        this.val$resources = a;
    }
    
    public void executeChoiceItem(int i, int i0) {
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        return false;
    }
    
    public void onStart(com.navdy.hud.app.view.ToastView a) {
        com.navdy.hud.app.ui.component.ConfirmationLayout a0 = a.getView();
        android.view.ViewGroup$MarginLayoutParams a1 = (android.view.ViewGroup$MarginLayoutParams)a0.screenImage.getLayoutParams();
        a1.width = this.val$resources.getDimensionPixelSize(R.dimen.locale_change_icon_size);
        a1.height = a1.width;
        a0.title1.setVisibility(8);
        a0.title3.setVisibility(8);
        a0.title4.setVisibility(8);
        ((android.widget.FrameLayout$LayoutParams)a0.screenImage.getLayoutParams()).gravity = 17;
        a0.findViewById(R.id.infoContainer).setPadding(0, 0, 0, 0);
        if (this.animator == null) {
            com.navdy.hud.app.ui.component.image.InitialsImageView a2 = a0.screenImage;
            android.util.Property a3 = android.view.View.ROTATION;
            float[] a4 = new float[1];
            a4[0] = 360f;
            this.animator = android.animation.ObjectAnimator.ofFloat(a2, a3, a4);
            this.animator.setDuration(500L);
            this.animator.setInterpolator((android.animation.TimeInterpolator)new android.view.animation.AccelerateDecelerateInterpolator());
            this.animator.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.profile.HudLocale$1$1(this));
        }
        if (!this.animator.isRunning()) {
            this.animator.start();
        }
    }
    
    public void onStop() {
        if (this.animator != null) {
            this.animator.removeAllListeners();
            this.animator.cancel();
        }
    }
}
