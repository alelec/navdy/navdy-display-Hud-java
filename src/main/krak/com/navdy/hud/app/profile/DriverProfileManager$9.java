package com.navdy.hud.app.profile;

class DriverProfileManager$9 implements Runnable {
    final com.navdy.hud.app.profile.DriverProfileManager this$0;
    final com.navdy.service.library.events.preferences.NotificationPreferencesUpdate val$update;
    
    DriverProfileManager$9(com.navdy.hud.app.profile.DriverProfileManager a, com.navdy.service.library.events.preferences.NotificationPreferencesUpdate a0) {
        super();
        this.this$0 = a;
        this.val$update = a0;
    }
    
    public void run() {
        com.navdy.hud.app.profile.DriverProfileManager.access$500(this.this$0).setNotificationPreferences(this.val$update.preferences);
        com.navdy.hud.app.profile.DriverProfileManager.access$600(this.this$0).setNotificationPreferences(this.val$update.preferences);
        com.navdy.hud.app.analytics.AnalyticsSupport.recordPreferenceChange(com.navdy.hud.app.profile.DriverProfileManager.access$500(this.this$0));
        com.navdy.hud.app.profile.DriverProfileManager.access$200(this.this$0).post(com.navdy.hud.app.profile.DriverProfileManager.access$500(this.this$0).getNotificationPreferences());
    }
}
