package com.navdy.hud.app.manager;

class InitManager$PhaseEmitter implements Runnable {
    private com.navdy.hud.app.event.InitEvents$Phase phase;
    final com.navdy.hud.app.manager.InitManager this$0;
    
    public InitManager$PhaseEmitter(com.navdy.hud.app.manager.InitManager a, com.navdy.hud.app.event.InitEvents$Phase a0) {
        super();
        this.this$0 = a;
        this.phase = a0;
    }
    
    public void run() {
        com.navdy.hud.app.manager.InitManager.access$100().i(new StringBuilder().append("Triggering init phase:").append(this.phase).toString());
        com.navdy.hud.app.manager.InitManager.access$200(this.this$0).post(new com.navdy.hud.app.event.InitEvents$InitPhase(this.phase));
    }
}
