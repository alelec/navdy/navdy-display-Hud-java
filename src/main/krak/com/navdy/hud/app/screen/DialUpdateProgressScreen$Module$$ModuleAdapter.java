package com.navdy.hud.app.screen;

final public class DialUpdateProgressScreen$Module$$ModuleAdapter extends dagger.internal.ModuleAdapter {
    final private static Class[] INCLUDES;
    final private static String[] INJECTS;
    final private static Class[] STATIC_INJECTIONS;
    
    static {
        String[] a = new String[1];
        a[0] = "members/com.navdy.hud.app.view.DialUpdateProgressView";
        INJECTS = a;
        STATIC_INJECTIONS = new Class[0];
        INCLUDES = new Class[0];
    }
    
    public DialUpdateProgressScreen$Module$$ModuleAdapter() {
        super(com.navdy.hud.app.screen.DialUpdateProgressScreen$Module.class, INJECTS, STATIC_INJECTIONS, false, INCLUDES, true, false);
    }
    
    public void getBindings(dagger.internal.BindingsGroup a, com.navdy.hud.app.screen.DialUpdateProgressScreen$Module a0) {
        a.contributeProvidesBinding("com.navdy.hud.app.screen.DialUpdateProgressScreen", (dagger.internal.ProvidesBinding)new com.navdy.hud.app.screen.DialUpdateProgressScreen$Module$$ModuleAdapter$ProvideScreenProvidesAdapter(a0));
    }
    
    public void getBindings(dagger.internal.BindingsGroup a, Object a0) {
        this.getBindings(a, (com.navdy.hud.app.screen.DialUpdateProgressScreen$Module)a0);
    }
}
