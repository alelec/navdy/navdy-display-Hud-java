package com.navdy.hud.app.screen;

final public class GestureLearningScreen$Module$$ModuleAdapter extends dagger.internal.ModuleAdapter {
    final private static Class[] INCLUDES;
    final private static String[] INJECTS;
    final private static Class[] STATIC_INJECTIONS;
    
    static {
        String[] a = new String[4];
        a[0] = "members/com.navdy.hud.app.view.LearnGestureScreenLayout";
        a[1] = "members/com.navdy.hud.app.view.GestureLearningView";
        a[2] = "members/com.navdy.hud.app.view.ScrollableTextPresenterLayout";
        a[3] = "members/com.navdy.hud.app.view.GestureVideoCaptureView";
        INJECTS = a;
        STATIC_INJECTIONS = new Class[0];
        INCLUDES = new Class[0];
    }
    
    public GestureLearningScreen$Module$$ModuleAdapter() {
        super(com.navdy.hud.app.screen.GestureLearningScreen$Module.class, INJECTS, STATIC_INJECTIONS, false, INCLUDES, true, false);
    }
}
