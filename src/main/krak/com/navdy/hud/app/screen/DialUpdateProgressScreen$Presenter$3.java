package com.navdy.hud.app.screen;

class DialUpdateProgressScreen$Presenter$3 implements com.navdy.hud.app.device.dial.DialFirmwareUpdater$UpdateProgressListener {
    final com.navdy.hud.app.screen.DialUpdateProgressScreen$Presenter this$0;
    
    DialUpdateProgressScreen$Presenter$3(com.navdy.hud.app.screen.DialUpdateProgressScreen$Presenter a) {
        super();
        this.this$0 = a;
    }
    
    public void onFinished(com.navdy.hud.app.device.dial.DialFirmwareUpdater$Error a, String s) {
        com.navdy.hud.app.screen.DialUpdateProgressScreen.access$200().v(new StringBuilder().append("onFinished:").append(a).append(" , ").append(s).toString());
        com.navdy.hud.app.screen.DialUpdateProgressScreen.access$000().post((Runnable)new com.navdy.hud.app.screen.DialUpdateProgressScreen$Presenter$3$2(this, a));
    }
    
    public void onProgress(int i) {
        com.navdy.hud.app.screen.DialUpdateProgressScreen.access$000().post((Runnable)new com.navdy.hud.app.screen.DialUpdateProgressScreen$Presenter$3$1(this, i));
    }
}
