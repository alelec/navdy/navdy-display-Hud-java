package com.navdy.hud.app.screen;

class FirstLaunchScreen$Presenter$2 implements Runnable {
    final com.navdy.hud.app.screen.FirstLaunchScreen$Presenter this$0;
    
    FirstLaunchScreen$Presenter$2(com.navdy.hud.app.screen.FirstLaunchScreen$Presenter a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        int i = 1;
        while(true) {
            int i0 = 0;
            Throwable a = null;
            android.media.MediaPlayer a0 = null;
            label1: {
                long j = 0L;
                com.navdy.service.library.log.Logger a1 = null;
                StringBuilder a2 = null;
                label3: {
                    try {
                        i0 = i;
                        j = android.os.SystemClock.elapsedRealtime();
                        a1 = com.navdy.hud.app.screen.FirstLaunchScreen.access$000();
                        a2 = new StringBuilder().append("creating media player counter=");
                        i0 = i + 1;
                        break label3;
                    } catch(Throwable a3) {
                        a = a3;
                    }
                    a0 = null;
                    break label1;
                }
                try {
                    a1.v(a2.append(i).toString());
                    a0 = new android.media.MediaPlayer();
                    label2: {
                        try {
                            a0.release();
                            break label2;
                        } catch(Throwable a4) {
                            a = a4;
                        }
                        break label1;
                    }
                    com.navdy.hud.app.screen.FirstLaunchScreen.access$000().v(new StringBuilder().append("created media player time=").append(android.os.SystemClock.elapsedRealtime() - j).toString());
                    this.this$0.bus.post(new com.navdy.hud.app.screen.FirstLaunchScreen$MediaServerUp((com.navdy.hud.app.screen.FirstLaunchScreen$1)null));
                } catch(Throwable a5) {
                    a0 = null;
                    a = a5;
                    break label1;
                }
                return;
            }
            com.navdy.hud.app.screen.FirstLaunchScreen.access$000().e("checkForMediaService", a);
            label0: {
                Throwable a6 = null;
                if (a0 == null) {
                    i = i0;
                    break label0;
                } else {
                    try {
                        i = i0;
                        a0.release();
                        i = i0;
                        break label0;
                    } catch(Throwable a7) {
                        a6 = a7;
                    }
                }
                com.navdy.hud.app.screen.FirstLaunchScreen.access$000().e(a6);
            }
            com.navdy.hud.app.util.GenericUtil.sleep(1000);
        }
    }
}
