package com.navdy.hud.app.screen;
import javax.inject.Inject;
import com.squareup.otto.Subscribe;

public class ShutDownScreen$Presenter extends com.navdy.hud.app.ui.framework.BasePresenter {
    @Inject
    com.navdy.hud.app.gesture.GestureServiceConnector gestureServiceConnector;
    @Inject
    com.squareup.otto.Bus mBus;
    @Inject
    android.content.SharedPreferences mPreferences;
    com.navdy.hud.app.obd.ObdManager obdManager;
    com.navdy.hud.app.event.Shutdown$Reason shutDownCause;
    @Inject
    com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;
    
    public ShutDownScreen$Presenter() {
    }
    
    public void finish() {
        this.mBus.post(new com.navdy.service.library.events.ui.ShowScreen$Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_BACK).build());
        com.navdy.hud.app.event.Shutdown a = new com.navdy.hud.app.event.Shutdown(this.shutDownCause, com.navdy.hud.app.event.Shutdown$State.CANCELED);
        com.navdy.hud.app.screen.ShutDownScreen.access$000().i(new StringBuilder().append("Posting ").append(a).toString());
        this.mBus.post(a);
    }
    
    public String getCurrentVersion() {
        return com.navdy.hud.app.util.OTAUpdateService.getCurrentVersion();
    }
    
    public com.navdy.hud.app.event.Shutdown$Reason getShutDownCause() {
        return this.shutDownCause;
    }
    
    public String getUpdateVersion() {
        return com.navdy.hud.app.util.OTAUpdateService.getSWUpdateVersion();
    }
    
    public void installAndShutDown() {
        String s = null;
        if (this.obdManager.getConnectionType() != com.navdy.hud.app.obd.ObdManager$ConnectionType.OBD) {
            s = "INSTALL_UPDATE_SHUTDOWN";
        } else {
            s = (this.obdManager.getBatteryVoltage() < 13.100000381469727) ? "INSTALL_UPDATE_REBOOT_QUIET" : "INSTALL_UPDATE_SHUTDOWN";
        }
        android.content.Intent a = new android.content.Intent(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.util.OTAUpdateService.class);
        a.putExtra("COMMAND", s);
        com.navdy.hud.app.HudApplication.getAppContext().startService(a);
    }
    
    public void installDialUpdateAndShutDown() {
        android.os.Bundle a = new android.os.Bundle();
        a.putInt("PROGRESS_CAUSE", 2);
        this.mBus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_UPDATE_PROGRESS, a, false));
    }
    
    public boolean isDialFirmwareUpdatePending() {
        boolean b = false;
        com.navdy.hud.app.device.dial.DialManager a = com.navdy.hud.app.device.dial.DialManager.getInstance();
        boolean b0 = a.isDialConnected();
        label2: {
            label0: {
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (a.getDialFirmwareUpdater().getVersions().isUpdateAvailable()) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    public boolean isSoftwareUpdatePending() {
        return com.navdy.hud.app.util.OTAUpdateService.isUpdateAvailable();
    }
    
    @Subscribe
    public void onDriving(com.navdy.hud.app.event.DrivingStateChange a) {
        if (a.driving && this.getShutDownCause() == com.navdy.hud.app.event.Shutdown$Reason.INACTIVITY) {
            this.finish();
        }
    }
    
    public void onLoad(android.os.Bundle a) {
        Throwable a0 = null;
        super.onLoad(a);
        this.obdManager = com.navdy.hud.app.obd.ObdManager.getInstance();
        this.mBus.register(this);
        label1: {
            label0: {
                label3: {
                    label2: if (a != null) {
                        try {
                            Exception a1 = null;
                            try {
                                String s = a.getString("SHUTDOWN_CAUSE");
                                if (s == null) {
                                    break label2;
                                }
                                this.shutDownCause = com.navdy.hud.app.event.Shutdown$Reason.valueOf(s);
                                break label2;
                            } catch(Exception a2) {
                                a1 = a2;
                            }
                            com.navdy.hud.app.screen.ShutDownScreen.access$000().e(new StringBuilder().append("failed to parse shutdown reason ").append(a1.getMessage()).toString());
                            break label3;
                        } catch(Throwable a3) {
                            a0 = a3;
                            break label1;
                        }
                    }
                    if (this.shutDownCause != null) {
                        break label0;
                    }
                    com.navdy.hud.app.screen.ShutDownScreen.access$000().e("defaulting to inactivity reason");
                    this.shutDownCause = com.navdy.hud.app.event.Shutdown$Reason.INACTIVITY;
                    break label0;
                }
                if (this.shutDownCause == null) {
                    com.navdy.hud.app.screen.ShutDownScreen.access$000().e("defaulting to inactivity reason");
                    this.shutDownCause = com.navdy.hud.app.event.Shutdown$Reason.INACTIVITY;
                }
            }
            this.uiStateManager.enableSystemTray(false);
            this.uiStateManager.enableNotificationColor(false);
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(false);
            com.navdy.hud.app.service.ShutdownMonitor.getInstance().disableInactivityShutdown(true);
            return;
        }
        if (this.shutDownCause == null) {
            com.navdy.hud.app.screen.ShutDownScreen.access$000().e("defaulting to inactivity reason");
            this.shutDownCause = com.navdy.hud.app.event.Shutdown$Reason.INACTIVITY;
        }
        throw a0;
    }
    
    protected void onUnload() {
        com.navdy.hud.app.screen.ShutDownScreen.access$000().v("onUnload");
        this.uiStateManager.enableSystemTray(true);
        this.uiStateManager.enableNotificationColor(true);
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(true);
        com.navdy.hud.app.service.ShutdownMonitor.getInstance().disableInactivityShutdown(false);
        this.mBus.unregister(this);
        super.onUnload();
    }
    
    public void shutDown() {
        com.navdy.hud.app.event.Shutdown a = new com.navdy.hud.app.event.Shutdown(this.shutDownCause, com.navdy.hud.app.event.Shutdown$State.CONFIRMED);
        com.navdy.hud.app.screen.ShutDownScreen.access$000().i(new StringBuilder().append("Posting ").append(a).toString());
        this.mBus.post(a);
    }
}
