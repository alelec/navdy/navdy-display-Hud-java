package com.navdy.hud.app.screen;

final public class ForceUpdateScreen$Module$$ModuleAdapter$ProvideScreenProvidesAdapter extends dagger.internal.ProvidesBinding implements javax.inject.Provider {
    final private com.navdy.hud.app.screen.ForceUpdateScreen$Module module;
    
    public ForceUpdateScreen$Module$$ModuleAdapter$ProvideScreenProvidesAdapter(com.navdy.hud.app.screen.ForceUpdateScreen$Module a) {
        super("com.navdy.hud.app.screen.ForceUpdateScreen", false, "com.navdy.hud.app.screen.ForceUpdateScreen.Module", "provideScreen");
        this.module = a;
        this.setLibrary(false);
    }
    
    public com.navdy.hud.app.screen.ForceUpdateScreen get() {
        return this.module.provideScreen();
    }
    
    public Object get() {
        return this.get();
    }
}
