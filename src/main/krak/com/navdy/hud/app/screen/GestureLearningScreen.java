package com.navdy.hud.app.screen;

@Layout(R.layout.screen_learn_gesture_layout)
public class GestureLearningScreen extends com.navdy.hud.app.screen.BaseScreen {
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.screen.GestureLearningScreen.class);
    }
    
    public GestureLearningScreen() {
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    public Object getDaggerModule() {
        return new com.navdy.hud.app.screen.GestureLearningScreen$Module(this);
    }
    
    public String getMortarScopeName() {
        return com.navdy.hud.app.screen.GestureLearningScreen.class.getSimpleName();
    }
    
    public com.navdy.service.library.events.ui.Screen getScreen() {
        return com.navdy.service.library.events.ui.Screen.SCREEN_GESTURE_LEARNING;
    }
}
