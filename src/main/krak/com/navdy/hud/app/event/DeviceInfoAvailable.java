package com.navdy.hud.app.event;

public class DeviceInfoAvailable {
    final public com.navdy.service.library.events.DeviceInfo deviceInfo;
    
    public DeviceInfoAvailable(com.navdy.service.library.events.DeviceInfo a) {
        this.deviceInfo = a;
    }
}
