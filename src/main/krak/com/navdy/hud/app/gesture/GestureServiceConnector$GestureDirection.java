package com.navdy.hud.app.gesture;


public enum GestureServiceConnector$GestureDirection {
    UNKNOWN(0),
    LEFT(1),
    RIGHT(2);

    private int value;
    GestureServiceConnector$GestureDirection(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class GestureServiceConnector$GestureDirection extends Enum {
//    final private static com.navdy.hud.app.gesture.GestureServiceConnector$GestureDirection[] $VALUES;
//    final public static com.navdy.hud.app.gesture.GestureServiceConnector$GestureDirection LEFT;
//    final public static com.navdy.hud.app.gesture.GestureServiceConnector$GestureDirection RIGHT;
//    final public static com.navdy.hud.app.gesture.GestureServiceConnector$GestureDirection UNKNOWN;
//    
//    static {
//        UNKNOWN = new com.navdy.hud.app.gesture.GestureServiceConnector$GestureDirection("UNKNOWN", 0);
//        LEFT = new com.navdy.hud.app.gesture.GestureServiceConnector$GestureDirection("LEFT", 1);
//        RIGHT = new com.navdy.hud.app.gesture.GestureServiceConnector$GestureDirection("RIGHT", 2);
//        com.navdy.hud.app.gesture.GestureServiceConnector$GestureDirection[] a = new com.navdy.hud.app.gesture.GestureServiceConnector$GestureDirection[3];
//        a[0] = UNKNOWN;
//        a[1] = LEFT;
//        a[2] = RIGHT;
//        $VALUES = a;
//    }
//    
//    private GestureServiceConnector$GestureDirection(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.gesture.GestureServiceConnector$GestureDirection valueOf(String s) {
//        return (com.navdy.hud.app.gesture.GestureServiceConnector$GestureDirection)Enum.valueOf(com.navdy.hud.app.gesture.GestureServiceConnector$GestureDirection.class, s);
//    }
//    
//    public static com.navdy.hud.app.gesture.GestureServiceConnector$GestureDirection[] values() {
//        return $VALUES.clone();
//    }
//}
//