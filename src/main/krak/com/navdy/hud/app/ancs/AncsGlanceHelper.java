package com.navdy.hud.app.ancs;

public class AncsGlanceHelper {
    private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = com.navdy.hud.app.ancs.AncsServiceConnector.sLogger;
    }
    
    public AncsGlanceHelper() {
    }
    
    public static com.navdy.service.library.events.glances.GlanceEvent buildAppleCalendarEvent(String s, String s0, String s1, String s2, String s3, java.util.Date a) {
        String s4 = com.navdy.hud.app.framework.glance.GlanceHelper.getId();
        java.util.ArrayList a0 = new java.util.ArrayList();
        if (s1 != null) {
            ((java.util.List)a0).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TITLE.name(), s1));
        }
        if (s3 != null) {
            ((java.util.List)a0).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_LOCATION.name(), s3));
        }
        ((java.util.List)a0).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TIME_STR.name(), com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getTimeHelper().formatTime(a, (StringBuilder)null)));
        ((java.util.List)a0).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TIME.name(), String.valueOf(a.getTime())));
        return new com.navdy.service.library.events.glances.GlanceEvent$Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent$GlanceType.GLANCE_TYPE_CALENDAR).provider(s).id(s4).postTime(Long.valueOf(System.currentTimeMillis())).glanceData((java.util.List)a0).build();
    }
    
    public static com.navdy.service.library.events.glances.GlanceEvent buildAppleMailEvent(String s, String s0, String s1, String s2, String s3, java.util.Date a) {
        com.navdy.service.library.events.glances.GlanceEvent a0 = null;
        boolean b = s1.indexOf("@") != -1;
        String s4 = com.navdy.hud.app.framework.glance.GlanceHelper.getId();
        java.util.ArrayList a1 = new java.util.ArrayList();
        if (b) {
            ((java.util.List)a1).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.EmailConstants.EMAIL_FROM_EMAIL.name(), s1));
        } else {
            ((java.util.List)a1).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.EmailConstants.EMAIL_FROM_NAME.name(), s1));
        }
        if (s2 != null) {
            ((java.util.List)a1).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.EmailConstants.EMAIL_SUBJECT.name(), s2));
        }
        label2: {
            label1: {
                if (s3 == null) {
                    break label1;
                }
                boolean b0 = s3.toLowerCase().contains((CharSequence)"this message has no content");
                label0: {
                    if (b0) {
                        break label0;
                    }
                    ((java.util.List)a1).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.EmailConstants.EMAIL_BODY.name(), s3));
                    break label1;
                }
                sLogger.v(new StringBuilder().append("invalid apple mail glance:").append(s3).toString());
                a0 = null;
                break label2;
            }
            a0 = new com.navdy.service.library.events.glances.GlanceEvent$Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent$GlanceType.GLANCE_TYPE_EMAIL).provider(s).id(s4).postTime(Long.valueOf(a.getTime())).glanceData((java.util.List)a1).build();
        }
        return a0;
    }
    
    public static com.navdy.service.library.events.glances.GlanceEvent buildFacebookEvent(String s, String s0, String s1, String s2, String s3, java.util.Date a) {
        String s4 = com.navdy.hud.app.framework.glance.GlanceHelper.getId();
        java.util.ArrayList a0 = new java.util.ArrayList();
        ((java.util.List)a0).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.SocialConstants.SOCIAL_MESSAGE.name(), s3));
        return new com.navdy.service.library.events.glances.GlanceEvent$Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent$GlanceType.GLANCE_TYPE_SOCIAL).provider(s).id(s4).postTime(Long.valueOf(a.getTime())).glanceData((java.util.List)a0).build();
    }
    
    public static com.navdy.service.library.events.glances.GlanceEvent buildFacebookMessengerEvent(String s, String s0, String s1, String s2, String s3, java.util.Date a) {
        return com.navdy.hud.app.ancs.AncsGlanceHelper.buildMessageEvent(s, s3, a);
    }
    
    public static com.navdy.service.library.events.glances.GlanceEvent buildGenericEvent(String s, String s0, String s1, String s2, String s3, java.util.Date a) {
        String s4 = com.navdy.hud.app.framework.glance.GlanceHelper.getId();
        java.util.ArrayList a0 = new java.util.ArrayList();
        if (s1 != null) {
            ((java.util.List)a0).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.GenericConstants.GENERIC_TITLE.name(), s1));
        }
        if (s3 != null) {
            ((java.util.List)a0).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.GenericConstants.GENERIC_MESSAGE.name(), s3));
        }
        return new com.navdy.service.library.events.glances.GlanceEvent$Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent$GlanceType.GLANCE_TYPE_GENERIC).provider(s).id(s4).postTime(Long.valueOf(a.getTime())).glanceData((java.util.List)a0).build();
    }
    
    public static com.navdy.service.library.events.glances.GlanceEvent buildGenericMailEvent(String s, String s0, String s1, String s2, String s3, java.util.Date a) {
        boolean b = android.text.TextUtils.isEmpty((CharSequence)s3);
        com.navdy.service.library.events.glances.GlanceEvent a0 = null;
        if (!b) {
            boolean b0 = "com.microsoft.Office.Outlook".equals(s);
            a0 = null;
            if (b0) {
                a0 = com.navdy.hud.app.ancs.AncsGlanceHelper.buildOutlookMailEvent(s, s0, s1, s2, s3, a);
            }
        }
        return a0;
    }
    
    public static com.navdy.service.library.events.glances.GlanceEvent buildGoogleCalendarEvent(String s, String s0, String s1, String s2, String s3, java.util.Date a) {
        com.navdy.service.library.events.glances.GlanceEvent a0 = null;
        if (android.text.TextUtils.isEmpty((CharSequence)s3)) {
            a0 = null;
        } else {
            java.util.StringTokenizer a1 = new java.util.StringTokenizer(s3, "\n");
            String s4 = null;
            String s5 = null;
            String s6 = null;
            int i = 0;
            while(a1.hasMoreElements()) {
                String s7 = (String)a1.nextElement();
                switch(i) {
                    case 2: {
                        s6 = s7.trim();
                        break;
                    }
                    case 1: {
                        s5 = s7.trim();
                        break;
                    }
                    case 0: {
                        s4 = s7.trim();
                        break;
                    }
                }
                i = i + 1;
            }
            sLogger.v(new StringBuilder().append("[ancs-gcalendar] title[").append(s4).append("] when[").append(s5).append("] location[").append(s6).append("]").toString());
            String s8 = com.navdy.hud.app.framework.glance.GlanceHelper.getId();
            java.util.ArrayList a2 = new java.util.ArrayList();
            if (s4 != null) {
                ((java.util.List)a2).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TITLE.name(), s4));
            }
            if (s5 != null) {
                ((java.util.List)a2).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TIME_STR.name(), s5));
            }
            ((java.util.List)a2).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TIME.name(), String.valueOf(a.getTime())));
            if (s6 != null) {
                ((java.util.List)a2).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_LOCATION.name(), s6));
            }
            a0 = new com.navdy.service.library.events.glances.GlanceEvent$Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent$GlanceType.GLANCE_TYPE_CALENDAR).provider(s).id(s8).postTime(Long.valueOf(a.getTime())).glanceData((java.util.List)a2).build();
        }
        return a0;
    }
    
    public static com.navdy.service.library.events.glances.GlanceEvent buildGoogleHangoutEvent(String s, String s0, String s1, String s2, String s3, java.util.Date a) {
        return com.navdy.hud.app.ancs.AncsGlanceHelper.buildMessageEvent(s, s3, a);
    }
    
    public static com.navdy.service.library.events.glances.GlanceEvent buildGoogleMailEvent(String s, String s0, String s1, String s2, String s3, java.util.Date a) {
        com.navdy.service.library.events.glances.GlanceEvent a0 = null;
        if (android.text.TextUtils.isEmpty((CharSequence)s3)) {
            a0 = null;
        } else {
            boolean b = false;
            String s4 = null;
            String s5 = null;
            int i = s3.indexOf("\n");
            String s6 = null;
            if (i == -1) {
                b = false;
            } else {
                s6 = s3.substring(0, i).trim();
                b = s6.indexOf("@") != -1;
                s3 = s3.substring(i + 1);
            }
            int i0 = s3.indexOf("\u2022");
            if (i0 == -1) {
                s4 = s3.trim();
                s5 = null;
            } else {
                s5 = s3.substring(0, i0).trim();
                s4 = s3.substring(i0 + 1).trim();
            }
            sLogger.v(new StringBuilder().append("[ancs-gmail] from[").append(s6).append("] subject[").append(s5).append("] body[").append(s4).append("]").toString());
            String s7 = com.navdy.hud.app.framework.glance.GlanceHelper.getId();
            java.util.ArrayList a1 = new java.util.ArrayList();
            if (s6 != null) {
                if (b) {
                    ((java.util.List)a1).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.EmailConstants.EMAIL_FROM_EMAIL.name(), s6));
                } else {
                    ((java.util.List)a1).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.EmailConstants.EMAIL_FROM_NAME.name(), s6));
                }
            }
            if (s5 != null) {
                ((java.util.List)a1).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.EmailConstants.EMAIL_SUBJECT.name(), s5));
            }
            if (s4 != null) {
                ((java.util.List)a1).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.EmailConstants.EMAIL_BODY.name(), s4));
            }
            a0 = new com.navdy.service.library.events.glances.GlanceEvent$Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent$GlanceType.GLANCE_TYPE_EMAIL).provider(s).id(s7).postTime(Long.valueOf(a.getTime())).glanceData((java.util.List)a1).build();
        }
        return a0;
    }
    
    private static com.navdy.service.library.events.glances.GlanceEvent buildMessageEvent(String s, String s0, java.util.Date a) {
        com.navdy.service.library.events.glances.GlanceEvent a0 = null;
        if (android.text.TextUtils.isEmpty((CharSequence)s0)) {
            a0 = null;
        } else {
            String s1 = null;
            String s2 = null;
            int i = s0.indexOf(":");
            if (i == -1) {
                s1 = s0.trim();
                s2 = null;
            } else {
                s2 = s0.substring(0, i).trim();
                s1 = s0.substring(i + 1).trim();
            }
            String s3 = com.navdy.hud.app.framework.glance.GlanceHelper.getId();
            java.util.ArrayList a1 = new java.util.ArrayList();
            ((java.util.List)a1).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM.name(), s2));
            ((java.util.List)a1).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_BODY.name(), s1));
            a0 = new com.navdy.service.library.events.glances.GlanceEvent$Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent$GlanceType.GLANCE_TYPE_MESSAGE).provider(s).id(s3).postTime(Long.valueOf(a.getTime())).glanceData((java.util.List)a1).build();
        }
        return a0;
    }
    
    public static com.navdy.service.library.events.glances.GlanceEvent buildOutlookMailEvent(String s, String s0, String s1, String s2, String s3, java.util.Date a) {
        boolean b = android.text.TextUtils.isEmpty((CharSequence)s3);
        String s4 = null;
        String s5 = null;
        if (!b) {
            String[] a0 = s3.split("\n");
            if (a0.length <= 1) {
                s4 = a0[0];
                s5 = null;
            } else {
                s5 = a0[0];
                s4 = a0[1];
            }
        }
        sLogger.v(new StringBuilder().append("[ancs-outlook-email] from[").append(s1).append("] subject[").append(s5).append("] body[").append(s4).append("]").toString());
        String s6 = com.navdy.hud.app.framework.glance.GlanceHelper.getId();
        java.util.ArrayList a1 = new java.util.ArrayList();
        if (!android.text.TextUtils.isEmpty((CharSequence)s1)) {
            if (s1.contains((CharSequence)"@")) {
                ((java.util.List)a1).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.EmailConstants.EMAIL_FROM_EMAIL.name(), s1));
            } else {
                ((java.util.List)a1).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.EmailConstants.EMAIL_FROM_NAME.name(), s1));
            }
        }
        if (!android.text.TextUtils.isEmpty((CharSequence)s5)) {
            ((java.util.List)a1).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.EmailConstants.EMAIL_SUBJECT.name(), s5));
        }
        if (!android.text.TextUtils.isEmpty((CharSequence)s4)) {
            ((java.util.List)a1).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.EmailConstants.EMAIL_BODY.name(), s4));
        }
        return new com.navdy.service.library.events.glances.GlanceEvent$Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent$GlanceType.GLANCE_TYPE_EMAIL).provider(s).id(s6).postTime(Long.valueOf(a.getTime())).glanceData((java.util.List)a1).build();
    }
    
    public static com.navdy.service.library.events.glances.GlanceEvent buildSlackEvent(String s, String s0, String s1, String s2, String s3, java.util.Date a) {
        return com.navdy.hud.app.ancs.AncsGlanceHelper.buildMessageEvent(s, s3, a);
    }
    
    public static com.navdy.service.library.events.glances.GlanceEvent buildTwitterEvent(String s, String s0, String s1, String s2, String s3, java.util.Date a) {
        com.navdy.service.library.events.glances.GlanceEvent a0 = null;
        if (android.text.TextUtils.isEmpty((CharSequence)s3)) {
            a0 = null;
        } else {
            String s4 = null;
            String s5 = null;
            int i = s3.indexOf(":");
            String s6 = com.navdy.hud.app.framework.glance.GlanceHelper.getId();
            java.util.ArrayList a1 = new java.util.ArrayList();
            if (i == -1) {
                int i0 = s3.indexOf("\n");
                if (i0 == -1) {
                    int i1 = s3.indexOf(" ");
                    if (i1 == -1) {
                        s4 = s3.trim();
                        s1 = null;
                        s5 = null;
                    } else {
                        s5 = s3.substring(0, i1).trim();
                        s4 = s3.substring(i1 + 1).trim();
                        s1 = null;
                    }
                } else {
                    s4 = null;
                    String s7 = null;
                    s5 = null;
                    if (i0 == -1) {
                        s1 = s7;
                    } else {
                        String s8 = s3.substring(0, i0).trim();
                        s4 = s3.substring(i0 + 1).trim();
                        s1 = s8;
                        s5 = null;
                    }
                }
            } else {
                String s9 = s3.substring(0, i).trim();
                if (!android.text.TextUtils.isEmpty((CharSequence)s9)) {
                    int i2 = s9.indexOf("@");
                    s1 = (i2 == -1) ? s9 : s9.substring(i2).trim();
                }
                String s10 = s3.substring(i + 1).trim();
                int i3 = s10.indexOf(" ");
                if (i3 == -1) {
                    s4 = s10.trim();
                    s5 = null;
                    s3 = s10;
                } else {
                    s5 = s10.substring(0, i3).trim();
                    s4 = s10.substring(i3 + 1).trim();
                    s3 = s10;
                }
            }
            if (s1 != null) {
                ((java.util.List)a1).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.SocialConstants.SOCIAL_FROM.name(), s1));
            }
            if (s5 != null) {
                ((java.util.List)a1).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.SocialConstants.SOCIAL_TO.name(), s5));
            }
            if (s4 == null) {
                ((java.util.List)a1).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.SocialConstants.SOCIAL_MESSAGE.name(), s3));
            } else {
                ((java.util.List)a1).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.SocialConstants.SOCIAL_MESSAGE.name(), s4));
            }
            a0 = new com.navdy.service.library.events.glances.GlanceEvent$Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent$GlanceType.GLANCE_TYPE_SOCIAL).provider(s).id(s6).postTime(Long.valueOf(a.getTime())).glanceData((java.util.List)a1).build();
        }
        return a0;
    }
    
    public static com.navdy.service.library.events.glances.GlanceEvent buildWhatsappEvent(String s, String s0, String s1, String s2, String s3, java.util.Date a) {
        String s4 = com.navdy.hud.app.framework.glance.GlanceHelper.getId();
        java.util.ArrayList a0 = new java.util.ArrayList();
        ((java.util.List)a0).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM.name(), s1));
        ((java.util.List)a0).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_BODY.name(), s3));
        return new com.navdy.service.library.events.glances.GlanceEvent$Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent$GlanceType.GLANCE_TYPE_MESSAGE).provider(s).id(s4).postTime(Long.valueOf(a.getTime())).glanceData((java.util.List)a0).build();
    }
    
    public static com.navdy.service.library.events.glances.GlanceEvent buildiMessageEvent(String s, String s0, String s1, String s2, String s3, java.util.Date a) {
        String s4 = com.navdy.hud.app.framework.glance.GlanceHelper.getId();
        java.util.ArrayList a0 = new java.util.ArrayList();
        if (com.navdy.hud.app.framework.contacts.ContactUtil.isValidNumber(s1)) {
            ((java.util.List)a0).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM_NUMBER.name(), s1));
        } else {
            ((java.util.List)a0).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM.name(), s1));
        }
        ((java.util.List)a0).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_BODY.name(), s3));
        com.navdy.service.library.events.glances.GlanceEvent$Builder a1 = new com.navdy.service.library.events.glances.GlanceEvent$Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent$GlanceType.GLANCE_TYPE_MESSAGE).provider(s).id(s4).postTime(Long.valueOf(a.getTime())).glanceData((java.util.List)a0);
        if (com.navdy.hud.app.ui.component.UISettings.supportsIosSms()) {
            java.util.ArrayList a2 = new java.util.ArrayList(1);
            ((java.util.List)a2).add(com.navdy.service.library.events.glances.GlanceEvent$GlanceActions.REPLY);
            a1.actions((java.util.List)a2);
        }
        return a1.build();
    }
}
