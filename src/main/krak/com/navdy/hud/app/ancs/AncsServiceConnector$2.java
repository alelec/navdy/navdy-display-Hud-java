package com.navdy.hud.app.ancs;

class AncsServiceConnector$2 implements Runnable {
    final com.navdy.hud.app.ancs.AncsServiceConnector this$0;
    
    AncsServiceConnector$2(com.navdy.hud.app.ancs.AncsServiceConnector a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        com.navdy.hud.app.ancs.AncsServiceConnector.sLogger.d("Establishing ANCS connection");
        com.navdy.ancs.IAncsService a = this.this$0.mService;
        label0: {
            android.os.RemoteException a0 = null;
            if (a == null) {
                break label0;
            }
            if (com.navdy.hud.app.ancs.AncsServiceConnector.access$500(this.this$0) == null) {
                break label0;
            }
            if (!com.navdy.hud.app.ancs.AncsServiceConnector.access$600(this.this$0)) {
                break label0;
            }
            try {
                com.navdy.service.library.device.NavdyDeviceId a1 = new com.navdy.service.library.device.NavdyDeviceId(com.navdy.hud.app.ancs.AncsServiceConnector.access$500(this.this$0).deviceId);
                this.this$0.mService.connectToDevice(new android.os.ParcelUuid(java.util.UUID.fromString(com.navdy.hud.app.ancs.AncsServiceConnector.access$500(this.this$0).deviceUuid)), a1.getBluetoothAddress());
                break label0;
            } catch(android.os.RemoteException a2) {
                a0 = a2;
            }
            com.navdy.hud.app.ancs.AncsServiceConnector.sLogger.e("Failed to connect to navdy BTLE service", (Throwable)a0);
        }
    }
}
