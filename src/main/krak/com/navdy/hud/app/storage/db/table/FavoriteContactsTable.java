package com.navdy.hud.app.storage.db.table;

public class FavoriteContactsTable {
    final public static String DEFAULT_IMAGE_INDEX = "def_image";
    final public static String DRIVER_ID = "device_id";
    final public static String NAME = "name";
    final public static String NUMBER = "number";
    final public static String NUMBER_NUMERIC = "number_numeric";
    final public static String NUMBER_TYPE = "number_type";
    final public static String TABLE_NAME = "fav_contacts";
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.storage.db.table.FavoriteContactsTable.class);
    }
    
    public FavoriteContactsTable() {
    }
    
    public static void createTable(android.database.sqlite.SQLiteDatabase a) {
        com.navdy.hud.app.storage.db.table.FavoriteContactsTable.createTable_1(a);
    }
    
    public static void createTable_1(android.database.sqlite.SQLiteDatabase a) {
        a.execSQL(new StringBuilder().append("CREATE TABLE IF NOT EXISTS ").append("fav_contacts").append(" (").append("device_id").append(" TEXT NOT NULL,").append("name").append(" TEXT,").append("number").append(" TEXT NOT NULL,").append("number_type").append(" INTEGER NOT NULL,").append("def_image").append(" INTEGER,").append("number_numeric").append(" INTEGER").append(");").toString());
        sLogger.v(new StringBuilder().append("createdTable:").append("fav_contacts").toString());
        String s = new StringBuilder().append("fav_contacts").append("_").append("device_id").toString();
        a.execSQL(new StringBuilder().append("CREATE INDEX IF NOT EXISTS ").append(s).append(" ON ").append("fav_contacts").append("(").append("device_id").append(");").toString());
        sLogger.v(new StringBuilder().append("createdIndex:").append(s).toString());
        String s0 = new StringBuilder().append("fav_contacts").append("_").append("number").toString();
        a.execSQL(new StringBuilder().append("CREATE INDEX IF NOT EXISTS ").append(s0).append(" ON ").append("fav_contacts").append("(").append("number").append(");").toString());
        sLogger.v(new StringBuilder().append("createdIndex:").append(s0).toString());
        String s1 = new StringBuilder().append("fav_contacts").append("_").append("number_type").toString();
        a.execSQL(new StringBuilder().append("CREATE INDEX IF NOT EXISTS ").append(s1).append(" ON ").append("fav_contacts").append("(").append("number_type").append(");").toString());
        sLogger.v(new StringBuilder().append("createdIndex:").append(s1).toString());
        String s2 = new StringBuilder().append("fav_contacts").append("_").append("def_image").toString();
        a.execSQL(new StringBuilder().append("CREATE INDEX IF NOT EXISTS ").append(s2).append(" ON ").append("fav_contacts").append("(").append("def_image").append(");").toString());
        sLogger.v(new StringBuilder().append("createdIndex:").append(s2).toString());
        String s3 = new StringBuilder().append("fav_contacts").append("_").append("number_numeric").toString();
        a.execSQL(new StringBuilder().append("CREATE INDEX IF NOT EXISTS ").append(s3).append(" ON ").append("fav_contacts").append("(").append("number_numeric").append(");").toString());
        sLogger.v(new StringBuilder().append("createdIndex:").append(s3).toString());
    }
    
    public static void upgradeDatabase_1(android.database.sqlite.SQLiteDatabase a) {
        com.navdy.hud.app.storage.db.DatabaseUtil.dropTable(a, "fav_contacts", sLogger);
        com.navdy.hud.app.storage.db.table.FavoriteContactsTable.createTable_1(a);
    }
}
