package com.navdy.hud.app.storage.cache;

final public class DiskLruCacheAdapter implements com.navdy.hud.app.storage.cache.Cache {
    final private com.navdy.hud.app.storage.cache.DiskLruCache diskLruCache;
    
    public DiskLruCacheAdapter(String s, String s0, int i) {
        super();
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(s, "cacheName");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(s0, "directory");
        this.diskLruCache = new com.navdy.hud.app.storage.cache.DiskLruCache(s, s0, i);
    }
    
    public void clear() {
        this.diskLruCache.clear();
    }
    
    public boolean contains(Object a) {
        return this.contains((String)a);
    }
    
    public boolean contains(String s) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(s, "key");
        return this.diskLruCache.contains(s);
    }
    
    public Object get(Object a) {
        return this.get((String)a);
    }
    
    public byte[] get(String s) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(s, "key");
        return this.diskLruCache.get(s);
    }
    
    final public com.navdy.hud.app.storage.cache.DiskLruCache getDiskLruCache() {
        return this.diskLruCache;
    }
    
    public void put(Object a, Object a0) {
        this.put((String)a, (byte[])a0);
    }
    
    public void put(String s, byte[] a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(s, "key");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "data");
        this.diskLruCache.put(s, a);
    }
    
    public void remove(Object a) {
        this.remove((String)a);
    }
    
    public void remove(String s) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(s, "key");
        this.diskLruCache.remove(s);
    }
}
