package com.navdy.hud.app.storage.cache;

class DiskLruCache$MemoryLruCache extends android.util.LruCache {
    private com.navdy.hud.app.storage.cache.DiskLruCache parent;
    
    public DiskLruCache$MemoryLruCache(com.navdy.hud.app.storage.cache.DiskLruCache a, int i) {
        super(i);
        this.parent = a;
    }
    
    protected void entryRemoved(boolean b, Object a, Object a0, Object a1) {
        this.entryRemoved(b, (String)a, (com.navdy.hud.app.storage.cache.DiskLruCache$FileEntry)a0, (com.navdy.hud.app.storage.cache.DiskLruCache$FileEntry)a1);
    }
    
    protected void entryRemoved(boolean b, String s, com.navdy.hud.app.storage.cache.DiskLruCache$FileEntry a, com.navdy.hud.app.storage.cache.DiskLruCache$FileEntry a0) {
        if (a != null) {
            com.navdy.hud.app.storage.cache.DiskLruCache.access$000(this.parent, a);
            a.removed = true;
        }
    }
    
    protected int sizeOf(Object a, Object a0) {
        return this.sizeOf((String)a, (com.navdy.hud.app.storage.cache.DiskLruCache$FileEntry)a0);
    }
    
    protected int sizeOf(String s, com.navdy.hud.app.storage.cache.DiskLruCache$FileEntry a) {
        return a.size;
    }
}
