package com.navdy.hud.app.storage.db;

public class HudDatabase extends android.database.sqlite.SQLiteOpenHelper {
    final private static String DATABASE_FILE;
    final private static String DATABASE_NAME = "hud.db";
    final private static int DATABASE_VERSION = 17;
    final private static String UPGRADE_METHOD_PREFIX = "upgradeDatabase_";
    final private static int VERSION_1 = 1;
    final private static int VERSION_10 = 10;
    final private static int VERSION_11 = 11;
    final private static int VERSION_12 = 12;
    final private static int VERSION_13 = 13;
    final private static int VERSION_14 = 14;
    final private static int VERSION_15 = 15;
    final private static int VERSION_16 = 16;
    final private static int VERSION_17 = 17;
    final private static int VERSION_2 = 2;
    final private static int VERSION_3 = 3;
    final private static int VERSION_4 = 4;
    final private static int VERSION_5 = 5;
    final private static int VERSION_6 = 6;
    final private static int VERSION_7 = 7;
    final private static int VERSION_8 = 8;
    final private static int VERSION_9 = 9;
    final private static android.content.Context context;
    final private static Object lockObj;
    private static volatile boolean sDBError;
    private static volatile Throwable sDBErrorStr;
    private static android.database.DatabaseErrorHandler sDatabaseErrorHandler;
    private static com.navdy.service.library.log.Logger sLogger;
    private static volatile com.navdy.hud.app.storage.db.HudDatabase sSingleton;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.storage.db.HudDatabase.class);
        DATABASE_FILE = new StringBuilder().append(com.navdy.hud.app.storage.PathManager.getInstance().getDatabaseDir()).append(java.io.File.separator).append("hud.db").toString();
        lockObj = new Object();
        context = com.navdy.hud.app.HudApplication.getAppContext();
        sDatabaseErrorHandler = (android.database.DatabaseErrorHandler)new com.navdy.hud.app.storage.db.HudDatabase$1();
    }
    
    private HudDatabase() {
        super(context, DATABASE_FILE, (android.database.sqlite.SQLiteDatabase$CursorFactory)null, 17, sDatabaseErrorHandler);
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    static boolean access$102(boolean b) {
        sDBError = b;
        return b;
    }
    
    static Throwable access$202(Throwable a) {
        sDBErrorStr = a;
        return a;
    }
    
    static android.content.Context access$300() {
        return context;
    }
    
    public static void deleteDatabaseFile() {
        sLogger.v("deleteDatabaseFile");
        try {
            java.io.File a = new java.io.File(DATABASE_FILE);
            if (a.exists()) {
                String s = a.getAbsolutePath();
                boolean b = com.navdy.service.library.util.IOUtils.deleteFile(context, s);
                sLogger.e(new StringBuilder().append("delete:").append(b).append(" - ").append(s).toString());
            }
            java.io.File a0 = new java.io.File(new StringBuilder().append(DATABASE_FILE).append("-journal").toString());
            if (a0.exists()) {
                String s0 = a0.getAbsolutePath();
                boolean b0 = com.navdy.service.library.util.IOUtils.deleteFile(context, s0);
                sLogger.e(new StringBuilder().append("delete:").append(b0).append(" - ").append(s0).toString());
            }
            java.io.File a1 = new java.io.File(new StringBuilder().append(DATABASE_FILE).append("-shm").toString());
            if (a1.exists()) {
                String s1 = a1.getAbsolutePath();
                boolean b1 = com.navdy.service.library.util.IOUtils.deleteFile(context, s1);
                sLogger.e(new StringBuilder().append("delete:").append(b1).append(" - ").append(s1).toString());
            }
            java.io.File a2 = new java.io.File(new StringBuilder().append(DATABASE_FILE).append("-wal").toString());
            if (a2.exists()) {
                String s2 = a2.getAbsolutePath();
                boolean b2 = com.navdy.service.library.util.IOUtils.deleteFile(context, s2);
                sLogger.e(new StringBuilder().append("delete:").append(b2).append(" - ").append(s2).toString());
            }
            android.database.sqlite.SQLiteDatabase.deleteDatabase(a);
        } catch(Throwable ignoredException) {
            sLogger.e("Error while trying to delete dbase");
        }
    }
    
    public static Throwable getErrorStr() {
        return sDBErrorStr;
    }
    
    public static com.navdy.hud.app.storage.db.HudDatabase getInstance() {
        Object a = null;
        Throwable a0 = null;
        com.navdy.hud.app.storage.db.HudDatabase a1 = sSingleton;
        label0: {
            if (a1 == null) {
                synchronized(lockObj) {
                    com.navdy.hud.app.storage.db.HudDatabase a2 = sSingleton;
                    label1: {
                        Throwable a3 = null;
                        if (a2 != null) {
                            break label1;
                        }
                        try {
                            if (!com.navdy.hud.app.storage.db.HudDatabase.isDatabaseStable(DATABASE_FILE)) {
                                sLogger.i("db is not stable, deleting it");
                                com.navdy.hud.app.storage.db.HudDatabase.deleteDatabaseFile();
                                sLogger.i("db delete complete");
                            }
                            sLogger.v("creating db-instance");
                            sSingleton = new com.navdy.hud.app.storage.db.HudDatabase();
                            sLogger.v("created db-instance");
                            break label1;
                        } catch(Throwable a4) {
                            a3 = a4;
                        }
                        sDBError = true;
                        sDBErrorStr = a3;
                        sLogger.e(a3);
                    }
                    /*monexit(a)*/;
                }
            }
            return sSingleton;
        }
        while(true) {
            try {
                /*monexit(a)*/;
            } catch(IllegalMonitorStateException | NullPointerException a6) {
                Throwable a7 = a6;
                a0 = a7;
                continue;
            }
            throw a0;
        }
    }
    
    private static boolean isDatabaseIntegrityFine(android.database.sqlite.SQLiteDatabase a) {
        boolean b = false;
        label0: {
            android.database.sqlite.SQLiteStatement a0 = null;
            label1: {
                label2: {
                    boolean b0 = false;
                    try {
                        a0 = null;
                        long j = System.currentTimeMillis();
                        a0 = a.compileStatement("PRAGMA integrity_check");
                        String s = a0.simpleQueryForString();
                        long j0 = System.currentTimeMillis();
                        sLogger.v(new StringBuilder().append("db integrity check took:").append(j0 - j).toString());
                        if (android.text.TextUtils.isEmpty((CharSequence)s)) {
                            break label2;
                        }
                        b0 = "ok".equalsIgnoreCase(s);
                    } catch(Throwable a1) {
                        if (a0 != null) {
                            a0.close();
                        }
                        throw a1;
                    }
                    if (b0) {
                        break label1;
                    }
                }
                if (a0 == null) {
                    b = false;
                    break label0;
                } else {
                    a0.close();
                    b = false;
                    break label0;
                }
            }
            if (a0 == null) {
                b = true;
            } else {
                a0.close();
                b = true;
            }
        }
        return b;
    }
    
    private static boolean isDatabaseStable(String s) {
        boolean b = false;
        java.io.File a = new java.io.File(s);
        boolean b0 = a.exists();
        label0: {
            android.database.sqlite.SQLiteException a0 = null;
            label8: {
                android.database.sqlite.SQLiteDatabase a1 = null;
                Throwable a2 = null;
                label2: if (b0) {
                    label9: {
                        try {
                            a1 = android.database.sqlite.SQLiteDatabase.openDatabase(s, (android.database.sqlite.SQLiteDatabase$CursorFactory)null, 16);
                            if (a1 != null) {
                                break label9;
                            }
                            sLogger.e("dbase could not be opened");
                        } catch(android.database.sqlite.SQLiteException a3) {
                            a0 = a3;
                            break label8;
                        }
                        b = false;
                        break label0;
                    }
                    label7: {
                        label4: {
                            label6: {
                                label5: try {
                                    Exception a4 = null;
                                    try {
                                        boolean b1 = com.navdy.hud.app.storage.db.HudDatabase.isDatabaseIntegrityFine(a1);
                                        label3: {
                                            if (b1) {
                                                break label3;
                                            }
                                            sLogger.e("data integrity is not valid");
                                            break label4;
                                        }
                                        if (a1.inTransaction()) {
                                            sLogger.v("dbase in transaction");
                                            a1.endTransaction();
                                        }
                                        if (a1.isDbLockedByCurrentThread()) {
                                            sLogger.v("dbase is currently locked");
                                        }
                                        long j = System.currentTimeMillis();
                                        a1.execSQL("VACUUM");
                                        sLogger.i(new StringBuilder().append("dbase VACCUM complete in ").append(System.currentTimeMillis() - j).toString());
                                        break label5;
                                    } catch(Exception a5) {
                                        a4 = a5;
                                    }
                                    sLogger.e((Throwable)a4);
                                    break label6;
                                } catch(Throwable a6) {
                                    a2 = a6;
                                    break label2;
                                }
                                a1.close();
                                break label7;
                            }
                            a1.close();
                            break label7;
                        }
                        a1.close();
                        b = false;
                        break label0;
                    }
                    boolean b2 = a.exists();
                    label1: {
                        if (!b2) {
                            break label1;
                        }
                        if (a.canRead()) {
                            b = true;
                            break label0;
                        }
                    }
                    sLogger.e("dbase not stable");
                    b = false;
                    break label0;
                } else {
                    b = true;
                    break label0;
                }
                a1.close();
                throw a2;
            }
            sLogger.e("dbase could not be opened", (Throwable)a0);
            b = false;
        }
        return b;
    }
    
    public static boolean isInErrorState() {
        return sDBError;
    }
    
    public static void upgradeDatabase_10(android.database.sqlite.SQLiteDatabase a) {
        com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getSharedPreferences().edit().putBoolean("gesture.engine", true).commit();
        sLogger.v("enabling gesture engine");
    }
    
    public static void upgradeDatabase_11(android.database.sqlite.SQLiteDatabase a) {
        label1: try {
            sLogger.v(new StringBuilder().append("trying to set [").append("persist.sys.hud_gps").append("]").toString());
            int i = com.navdy.hud.app.util.SerialNumber.instance.revisionCode.charAt(0);
            label0: {
                if (i < 52) {
                    break label0;
                }
                if (i > 54) {
                    break label0;
                }
                com.navdy.hud.app.util.os.SystemProperties.set("persist.sys.hud_gps", "-1");
                sLogger.v(new StringBuilder().append("setting property for [").append(android.os.Build.SERIAL).append("] to [-1] digit[").append((char)i).append("]").toString());
                break label1;
            }
            sLogger.v(new StringBuilder().append("not setting property [").append(android.os.Build.SERIAL).append("]").toString());
        } catch(Throwable a0) {
            sLogger.e("dbupgrade --> 11 failed", a0);
        }
    }
    
    public static void upgradeDatabase_12(android.database.sqlite.SQLiteDatabase a) {
        try {
            android.content.Context a0 = com.navdy.hud.app.HudApplication.getAppContext();
            com.navdy.service.library.util.IOUtils.deleteDirectory(a0, new java.io.File(a0.getFilesDir(), ".Fabric"));
            sLogger.v("dbupgrade --> Fabric dir removed");
        } catch(Throwable a1) {
            sLogger.e("dbupgrade --> 12 failed", a1);
        }
    }
    
    public static void upgradeDatabase_13(android.database.sqlite.SQLiteDatabase a) {
        com.navdy.hud.app.storage.db.table.VinInformationTable.createTable(a);
    }
    
    public static void upgradeDatabase_14(android.database.sqlite.SQLiteDatabase a) {
        label2: try {
            label7: try {
                sLogger.v("looking for native crash files");
                java.io.File a0 = new java.io.File("/data/anr");
                boolean b = a0.exists();
                label8: {
                    label9: {
                        if (!b) {
                            break label9;
                        }
                        if (a0.isDirectory()) {
                            break label8;
                        }
                    }
                    sLogger.v("no native crashes to delete");
                    break label7;
                }
                String[] a1 = a0.list((java.io.FilenameFilter)new com.navdy.hud.app.storage.db.HudDatabase$2());
                label5: {
                    label6: {
                        if (a1 == null) {
                            break label6;
                        }
                        if (a1.length != 0) {
                            break label5;
                        }
                    }
                    sLogger.v("no native crash files");
                    break label7;
                }
                int i = 0;
                while(i < a1.length) {
                    java.io.File a2 = new java.io.File(new StringBuilder().append("/data/anr").append(java.io.File.separator).append(a1[i]).toString());
                    String s = a2.getAbsolutePath();
                    boolean b0 = a2.delete();
                    sLogger.v(new StringBuilder().append("removing native crash file[").append(s).append("] :").append(b0).toString());
                    i = i + 1;
                }
            } catch(Throwable a3) {
                sLogger.e(a3);
            }
            sLogger.v("looking for tombstone");
            java.io.File a4 = new java.io.File("/data/tombstones");
            boolean b1 = a4.exists();
            label3: {
                label4: {
                    if (!b1) {
                        break label4;
                    }
                    if (a4.isDirectory()) {
                        break label3;
                    }
                }
                sLogger.v("no tombstones to delete");
                break label2;
            }
            String[] a5 = a4.list();
            label0: {
                label1: {
                    if (a5 == null) {
                        break label1;
                    }
                    if (a5.length != 0) {
                        break label0;
                    }
                }
                sLogger.v("no tombstones");
                break label2;
            }
            int i0 = 0;
            while(i0 < a5.length) {
                java.io.File a6 = new java.io.File(new StringBuilder().append("/data/tombstones").append(java.io.File.separator).append(a5[i0]).toString());
                String s0 = a6.getAbsolutePath();
                boolean b2 = a6.delete();
                sLogger.v(new StringBuilder().append("removing tombstone file[").append(s0).append("] :").append(b2).toString());
                i0 = i0 + 1;
            }
        } catch(Throwable a7) {
            sLogger.e(a7);
        }
    }
    
    public static void upgradeDatabase_15(android.database.sqlite.SQLiteDatabase a) {
        com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getSharedPreferences().edit().remove("last_low_voltage_event").commit();
    }
    
    public static void upgradeDatabase_16(android.database.sqlite.SQLiteDatabase a) {
        com.navdy.hud.app.storage.db.table.MusicArtworkCacheTable.createTable(a);
    }
    
    public static void upgradeDatabase_17(android.database.sqlite.SQLiteDatabase a) {
        com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getSharedPreferences().edit().remove("dial_reminder_time").commit();
    }
    
    public static void upgradeDatabase_2(android.database.sqlite.SQLiteDatabase a) {
        com.navdy.hud.app.storage.db.table.RecentCallsTable.upgradeDatabase_2(a);
        com.navdy.hud.app.storage.db.table.FavoriteContactsTable.createTable(a);
    }
    
    public static void upgradeDatabase_3(android.database.sqlite.SQLiteDatabase a) {
    }
    
    public static void upgradeDatabase_4(android.database.sqlite.SQLiteDatabase a) {
    }
    
    public static void upgradeDatabase_5(android.database.sqlite.SQLiteDatabase a) {
    }
    
    public static void upgradeDatabase_6(android.database.sqlite.SQLiteDatabase a) {
        com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getSharedPreferences().edit().putString("map.scheme", "carnav.day").apply();
        sLogger.v("map scheme changed to carnav.day");
    }
    
    public static void upgradeDatabase_7(android.database.sqlite.SQLiteDatabase a) {
        java.io.File a0 = new java.io.File(com.navdy.hud.app.storage.PathManager.getInstance().getHereVoiceSkinsPath(), "en-US_TTS");
        com.navdy.service.library.util.IOUtils.deleteDirectory(com.navdy.hud.app.HudApplication.getAppContext(), a0);
        sLogger.v(new StringBuilder().append("deleted old here voice skins [").append(a0).append("]").toString());
    }
    
    public static void upgradeDatabase_8(android.database.sqlite.SQLiteDatabase a) {
        com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getSharedPreferences().edit().putString("screen.led_brightness", "255").apply();
        sLogger.v("default max LED brightness changed to 255");
    }
    
    public static void upgradeDatabase_9(android.database.sqlite.SQLiteDatabase a) {
        com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getSharedPreferences().edit().putBoolean("gesture.engine", false).apply();
        sLogger.v("disabling gesture engine");
    }
    
    public android.database.sqlite.SQLiteDatabase getWritableDatabase() {
        android.database.sqlite.SQLiteDatabase a = null;
        synchronized(this) {
            try {
                a = super.getWritableDatabase();
                break label0;
            } catch(Throwable a1) {
                try {
                    sLogger.e(a1);
                    if (a1 instanceof android.database.sqlite.SQLiteDatabaseCorruptException) {
                        sLogger.e(":: db corrupted");
                        a = null;
                        break label0;
                    } else {
                        sDBError = true;
                        sDBErrorStr = a1;
                        a = null;
                        break label0;
                    }
                } catch(Throwable a2) {
                    a0 = a2;
                }
            }
            /*monexit(this)*/;
            throw a0;
        }
        /*monexit(this)*/;
        return a;
    }
    
    public void onCreate(android.database.sqlite.SQLiteDatabase a) {
        sDBError = false;
        sDBErrorStr = null;
        sLogger.v("onCreate::start::");
        a.beginTransaction();
        try {
            com.navdy.hud.app.storage.db.table.RecentCallsTable.createTable(a);
            com.navdy.hud.app.storage.db.table.FavoriteContactsTable.createTable(a);
            com.navdy.hud.app.storage.db.table.VinInformationTable.createTable(a);
            com.navdy.hud.app.storage.db.table.MusicArtworkCacheTable.createTable(a);
            a.setTransactionSuccessful();
        } catch(Throwable a0) {
            a.endTransaction();
            throw a0;
        }
        a.endTransaction();
        sLogger.v("onCreate::end::");
    }
    
    public void onOpen(android.database.sqlite.SQLiteDatabase a) {
        sLogger.v("onOpen::");
        super.onOpen(a);
    }
    
    public void onUpgrade(android.database.sqlite.SQLiteDatabase a, int i, int i0) {
        sLogger.v(new StringBuilder().append("onUpgrade::start:: ").append(i).append(" ==>").append(i0).toString());
        int i1 = i + 1;
        while(true) {
            Throwable a0 = null;
            if (i1 > i0) {
                sLogger.v(new StringBuilder().append("onUpgrade::end:: ").append(i).append(" ==>").append(i0).toString());
                return;
            }
            try {
                sLogger.v(new StringBuilder().append("onUpgrade:: ").append(i1).toString());
                String s = new StringBuilder().append("upgradeDatabase_").append(i1).toString();
                Class[] a1 = new Class[1];
                a1[0] = android.database.sqlite.SQLiteDatabase.class;
                java.lang.reflect.Method a2 = com.navdy.hud.app.storage.db.HudDatabase.class.getMethod(s, a1);
                Object[] a3 = new Object[1];
                a3[0] = a;
                a2.invoke(null, a3);
                i1 = i1 + 1;
                continue;
            } catch(Throwable a4) {
                a0 = a4;
            }
            sLogger.e(a0);
            throw new RuntimeException(a0);
        }
    }
}
