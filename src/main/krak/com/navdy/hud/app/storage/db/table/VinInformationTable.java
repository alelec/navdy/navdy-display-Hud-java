package com.navdy.hud.app.storage.db.table;

public class VinInformationTable {
    final public static String TABLE_NAME = "vininfo";
    final public static String UNKNOWN_VIN = "UNKNOWN_VIN";
    final public static String VIN_INFO = "info";
    final public static String VIN_NUMBER = "vin";
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.storage.db.table.VinInformationTable.class);
    }
    
    public VinInformationTable() {
    }
    
    public static void createTable(android.database.sqlite.SQLiteDatabase a) {
        com.navdy.hud.app.storage.db.table.VinInformationTable.createTable_1(a);
    }
    
    public static void createTable_1(android.database.sqlite.SQLiteDatabase a) {
        a.execSQL(new StringBuilder().append("CREATE TABLE IF NOT EXISTS ").append("vininfo").append(" (").append("vin").append(" TEXT NOT NULL,").append("info").append(" TEXT").append(");").toString());
        sLogger.v(new StringBuilder().append("createdTable:").append("vininfo").toString());
        com.navdy.hud.app.storage.db.DatabaseUtil.createIndex(a, "vininfo", "vin", sLogger);
    }
}
