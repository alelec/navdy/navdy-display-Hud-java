package com.navdy.hud.app.storage.db.helper;

public class RecentCallPersistenceHelper {
    final private static String BULK_INSERT_SQL = "INSERT INTO recent_calls(device_id,category,name,number,number_type,call_time,call_type,def_image,number_numeric) VALUES (?,?,?,?,?,?,?,?,?)";
    final private static int CALL_TIME_ORDINAL = 5;
    final private static int CALL_TYPE_ORDINAL = 4;
    final private static int CATEGORY_ORDINAL = 0;
    final private static int DEFAULT_IMAGE_INDEX_ORDINAL = 6;
    final private static java.util.List EMPTY_LIST;
    final private static int NAME_ORDINAL = 1;
    final private static int NUMBER_NUMERIC_ORDINAL = 7;
    final private static int NUMBER_ORDINAL = 2;
    final private static int NUMBER_TYPE_ORDINAL = 3;
    final private static String[] RECENT_CALL_ARGS;
    final private static String RECENT_CALL_ORDER_BY = "call_time DESC";
    final private static String[] RECENT_CALL_PROJECTION;
    final private static String RECENT_CALL_WHERE = "device_id=?";
    final private static Object lockObj;
    final private static com.navdy.service.library.log.Logger sLogger;
    final private static java.util.Comparator sReverseComparator;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.storage.db.helper.RecentCallPersistenceHelper.class);
        lockObj = new Object();
        EMPTY_LIST = (java.util.List)new java.util.LinkedList();
        String[] a = new String[8];
        a[0] = "category";
        a[1] = "name";
        a[2] = "number";
        a[3] = "number_type";
        a[4] = "call_type";
        a[5] = "call_time";
        a[6] = "def_image";
        a[7] = "number_numeric";
        RECENT_CALL_PROJECTION = a;
        RECENT_CALL_ARGS = new String[1];
        sReverseComparator = (java.util.Comparator)new com.navdy.hud.app.storage.db.helper.RecentCallPersistenceHelper$1();
    }
    
    public RecentCallPersistenceHelper() {
    }
    
    private static void deleteRecentCalls(String s) {
        try {
            synchronized(lockObj) {
                android.database.sqlite.SQLiteDatabase a0 = com.navdy.hud.app.storage.db.HudDatabase.getInstance().getWritableDatabase();
                if (a0 == null) {
                    throw new com.navdy.hud.app.storage.db.DatabaseUtil$DatabaseNotAvailable();
                }
                RECENT_CALL_ARGS[0] = s;
                int i = a0.delete("recent_calls", "device_id=?", RECENT_CALL_ARGS);
                sLogger.v(new StringBuilder().append("recent-calls rows deleted:").append(i).toString());
                /*monexit(a)*/;
            }
        } catch(Throwable a5) {
            sLogger.e(a5);
        }
    }
    
    public static java.util.List getRecentsCalls(String s) {
        Object a = null;
        Throwable a0 = null;
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        boolean b = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile();
        label0: {
            Object a1 = null;
            label3: if (b) {
                a1 = EMPTY_LIST;
            } else {
                synchronized(lockObj) {
                    Object a2 = null;
                    java.util.ArrayList a3 = null;
                    android.database.sqlite.SQLiteDatabase a4 = com.navdy.hud.app.storage.db.HudDatabase.getInstance().getWritableDatabase();
                    if (a4 == null) {
                        throw new com.navdy.hud.app.storage.db.DatabaseUtil$DatabaseNotAvailable();
                    }
                    RECENT_CALL_ARGS[0] = s;
                    android.database.Cursor a5 = a4.query("recent_calls", RECENT_CALL_PROJECTION, "device_id=?", RECENT_CALL_ARGS, (String)null, (String)null, "call_time DESC");
                    label1: {
                        label2: {
                            if (a5 == null) {
                                break label2;
                            }
                            try {
                                a2 = a5;
                                if (!a5.moveToFirst()) {
                                    break label2;
                                }
                                a2 = a5;
                                com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance();
                                a3 = new java.util.ArrayList();
                                a2 = a5;
                                while(true) {
                                    int i = ((android.database.Cursor)a2).getInt(0);
                                    String s0 = ((android.database.Cursor)a2).getString(1);
                                    String s1 = ((android.database.Cursor)a2).getString(2);
                                    int i0 = ((android.database.Cursor)a2).getInt(3);
                                    int i1 = ((android.database.Cursor)a2).getInt(4);
                                    long j = ((android.database.Cursor)a2).getLong(5);
                                    int i2 = ((android.database.Cursor)a2).getInt(6);
                                    long j0 = ((android.database.Cursor)a2).getLong(7);
                                    com.navdy.hud.app.framework.contacts.NumberType a6 = com.navdy.hud.app.framework.contacts.NumberType.buildFromValue(i0);
                                    ((java.util.List)a3).add(new com.navdy.hud.app.framework.recentcall.RecentCall(s0, com.navdy.hud.app.framework.recentcall.RecentCall$Category.buildFromValue(i), s1, a6, new java.util.Date(1000L * j), com.navdy.hud.app.framework.recentcall.RecentCall$CallType.buildFromValue(i1), i2, j0));
                                    if (!((android.database.Cursor)a2).moveToNext()) {
                                        break label1;
                                    }
                                }
                            } catch(Throwable a7) {
                                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a2);
                                throw a7;
                            }
                        }
                        java.util.List a8 = EMPTY_LIST;
                        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a5);
                        /*monexit(a)*/;
                        a1 = a8;
                        break label3;
                    }
                    com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a2);
                    /*monexit(a)*/;
                    a1 = a3;
                }
            }
            return (java.util.List)a1;
        }
        while(true) {
            try {
                /*monexit(a)*/;
            } catch(IllegalMonitorStateException | NullPointerException a10) {
                Throwable a11 = a10;
                a0 = a11;
                continue;
            }
            throw a0;
        }
    }
    
    private static java.util.List mergeRecentCalls(java.util.List a, java.util.List a0, boolean b) {
        Object a1 = null;
        java.util.ArrayList a2 = null;
        java.util.HashMap a3 = new java.util.HashMap(52);
        if (a == null) {
            a1 = a0;
        } else {
            Object a4 = a.iterator();
            a1 = a0;
            while(((java.util.Iterator)a4).hasNext()) {
                com.navdy.hud.app.framework.recentcall.RecentCall a5 = (com.navdy.hud.app.framework.recentcall.RecentCall)((java.util.Iterator)a4).next();
                {
                    if (b && a5.category == com.navdy.hud.app.framework.recentcall.RecentCall$Category.PHONE_CALL) {
                        continue;
                    }
                    a3.put(Long.valueOf(a5.numericNumber), a5);
                }
            }
        }
        if (a1 != null) {
            Object a6 = ((java.util.List)a1).iterator();
            while(((java.util.Iterator)a6).hasNext()) {
                com.navdy.hud.app.framework.recentcall.RecentCall a7 = (com.navdy.hud.app.framework.recentcall.RecentCall)((java.util.Iterator)a6).next();
                com.navdy.hud.app.framework.recentcall.RecentCall a8 = (com.navdy.hud.app.framework.recentcall.RecentCall)a3.get(Long.valueOf(a7.numericNumber));
                if (a8 != null) {
                    if (b) {
                        a3.put(Long.valueOf(a7.numericNumber), a7);
                    } else if (a7.callTime.getTime() >= a8.callTime.getTime()) {
                        if (a7.category == com.navdy.hud.app.framework.recentcall.RecentCall$Category.MESSAGE && android.text.TextUtils.isEmpty((CharSequence)a7.name)) {
                            a7.name = a8.name;
                        }
                        a3.put(Long.valueOf(a7.numericNumber), a7);
                    }
                } else {
                    a3.put(Long.valueOf(a7.numericNumber), a7);
                }
            }
        }
        java.util.ArrayList a9 = new java.util.ArrayList(a3.values());
        java.util.Collections.sort((java.util.List)a9, sReverseComparator);
        if (((java.util.List)a9).size() <= 30) {
            a2 = a9;
        } else {
            a2 = new java.util.ArrayList(30);
            int i = 0;
            while(i < 30) {
                ((java.util.List)a2).add(((java.util.List)a9).get(i));
                i = i + 1;
            }
        }
        return (java.util.List)a2;
    }
    
    public static void storeRecentCalls(String s, java.util.List a, boolean b) {
        Object a0 = null;
        Throwable a1 = null;
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        com.navdy.hud.app.framework.recentcall.RecentCallManager a2 = com.navdy.hud.app.framework.recentcall.RecentCallManager.getInstance();
        java.util.List a3 = com.navdy.hud.app.storage.db.helper.RecentCallPersistenceHelper.mergeRecentCalls(a2.getRecentCalls(), a, b);
        com.navdy.hud.app.storage.db.helper.RecentCallPersistenceHelper.deleteRecentCalls(s);
        int i = a3.size();
        label0: {
            if (i != 0) {
                if (b) {
                    com.navdy.hud.app.framework.contacts.PhoneImageDownloader.getInstance().clearAllPhotoCheckEntries();
                }
                synchronized(lockObj) {
                    int i0 = 0;
                    android.database.sqlite.SQLiteDatabase a4 = com.navdy.hud.app.storage.db.HudDatabase.getInstance().getWritableDatabase();
                    if (a4 == null) {
                        throw new com.navdy.hud.app.storage.db.DatabaseUtil$DatabaseNotAvailable();
                    }
                    android.database.sqlite.SQLiteStatement a5 = a4.compileStatement("INSERT INTO recent_calls(device_id,category,name,number,number_type,call_time,call_type,def_image,number_numeric) VALUES (?,?,?,?,?,?,?,?,?)");
                    try {
                        com.navdy.hud.app.framework.contacts.ContactImageHelper a6 = com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance();
                        a4.beginTransaction();
                        java.util.Iterator a7 = a3.iterator();
                        Object a8 = a3;
                        i0 = 0;
                        Object a9 = a7;
                        while(((java.util.Iterator)a9).hasNext()) {
                            com.navdy.hud.app.framework.recentcall.RecentCall a10 = (com.navdy.hud.app.framework.recentcall.RecentCall)((java.util.Iterator)a9).next();
                            a5.clearBindings();
                            a5.bindString(1, s);
                            a5.bindLong(2, (long)a10.category.getValue());
                            if (android.text.TextUtils.isEmpty((CharSequence)a10.name)) {
                                a5.bindNull(3);
                            } else {
                                a5.bindString(3, a10.name);
                            }
                            a5.bindString(4, a10.number);
                            a5.bindLong(5, (long)a10.numberType.getValue());
                            a5.bindLong(6, a10.callTime.getTime() / 1000L);
                            a5.bindLong(7, (long)a10.callType.getValue());
                            a10.defaultImageIndex = a6.getContactImageIndex(a10.number);
                            a5.bindLong(8, (long)a10.defaultImageIndex);
                            a5.bindLong(9, a10.numericNumber);
                            a5.execute();
                            i0 = i0 + 1;
                            if (b) {
                                com.navdy.hud.app.framework.contacts.PhoneImageDownloader.getInstance().submitDownload(a10.number, com.navdy.hud.app.framework.contacts.PhoneImageDownloader$Priority.NORMAL, com.navdy.service.library.events.photo.PhotoType.PHOTO_CONTACT, a10.name);
                            }
                        }
                        a4.setTransactionSuccessful();
                        com.navdy.hud.app.framework.recentcall.RecentCallManager.getInstance().setRecentCalls((java.util.List)a8);
                    } catch(Throwable a11) {
                        a4.endTransaction();
                        throw a11;
                    }
                    a4.endTransaction();
                    sLogger.v(new StringBuilder().append("recent-calls rows added:").append(i0).toString());
                    /*monexit(a0)*/;
                }
            } else {
                a2.setRecentCalls((java.util.List)null);
            }
            return;
        }
        while(true) {
            try {
                /*monexit(a0)*/;
            } catch(IllegalMonitorStateException | NullPointerException a13) {
                Throwable a14 = a13;
                a1 = a14;
                continue;
            }
            throw a1;
        }
    }
}
