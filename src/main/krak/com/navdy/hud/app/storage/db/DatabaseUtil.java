package com.navdy.hud.app.storage.db;

public class DatabaseUtil {
    final public static String ROW_ID = "rowid";
    
    public DatabaseUtil() {
    }
    
    public static void createIndex(android.database.sqlite.SQLiteDatabase a, String s, String s0, com.navdy.service.library.log.Logger a0) {
        String s1 = new StringBuilder().append(s).append("_").append(s0).toString();
        a.execSQL(new StringBuilder().append("CREATE INDEX IF NOT EXISTS ").append(s1).append(" ON ").append(s).append("(").append(s0).append(");").toString());
        a0.v(new StringBuilder().append("createdIndex:").append(s1).toString());
    }
    
    public static void dropTable(android.database.sqlite.SQLiteDatabase a, String s, com.navdy.service.library.log.Logger a0) {
        try {
            if (!android.text.TextUtils.isEmpty((CharSequence)s)) {
                a.execSQL(new StringBuilder().append("DROP TABLE IF EXISTS ").append(s).toString());
                a0.v(new StringBuilder().append("dropped table[").append(s).append("]").toString());
            }
        } catch(Throwable a1) {
            a0.e(a1);
        }
    }
}
