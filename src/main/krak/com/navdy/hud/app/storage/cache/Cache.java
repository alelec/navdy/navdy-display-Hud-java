package com.navdy.hud.app.storage.cache;

abstract public interface Cache {
    abstract public void clear();
    
    
    abstract public boolean contains(Object arg);
    
    
    abstract public Object get(Object arg);
    
    
    abstract public void put(Object arg, Object arg0);
    
    
    abstract public void remove(Object arg);
}
