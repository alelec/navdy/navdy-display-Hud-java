package com.navdy.hud.app.storage.db.helper;

public class FavoriteContactsPersistenceHelper {
    final private static String BULK_INSERT_SQL = "INSERT INTO fav_contacts(device_id,name,number,number_type,def_image,number_numeric) VALUES (?,?,?,?,?,?)";
    final private static int DEFAULT_IMAGE_INDEX_ORDINAL = 3;
    final private static java.util.List EMPTY_LIST;
    final private static String[] FAV_CONTACT_ARGS;
    final private static String FAV_CONTACT_ORDER_BY = "rowid";
    final private static String[] FAV_CONTACT_PROJECTION;
    final private static String FAV_CONTACT_WHERE = "device_id=?";
    final private static int NAME_ORDINAL = 0;
    final private static int NUMBER_NUMERIC_ORDINAL = 4;
    final private static int NUMBER_ORDINAL = 1;
    final private static int NUMBER_TYPE_ORDINAL = 2;
    final private static Object lockObj;
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.storage.db.helper.FavoriteContactsPersistenceHelper.class);
        lockObj = new Object();
        EMPTY_LIST = (java.util.List)new java.util.LinkedList();
        String[] a = new String[5];
        a[0] = "name";
        a[1] = "number";
        a[2] = "number_type";
        a[3] = "def_image";
        a[4] = "number_numeric";
        FAV_CONTACT_PROJECTION = a;
        FAV_CONTACT_ARGS = new String[1];
    }
    
    public FavoriteContactsPersistenceHelper() {
    }
    
    private static void deleteFavoriteContacts(String s) {
        try {
            synchronized(lockObj) {
                android.database.sqlite.SQLiteDatabase a0 = com.navdy.hud.app.storage.db.HudDatabase.getInstance().getWritableDatabase();
                if (a0 == null) {
                    throw new com.navdy.hud.app.storage.db.DatabaseUtil$DatabaseNotAvailable();
                }
                FAV_CONTACT_ARGS[0] = s;
                int i = a0.delete("fav_contacts", "device_id=?", FAV_CONTACT_ARGS);
                sLogger.v(new StringBuilder().append("fav-contact rows deleted:").append(i).toString());
                /*monexit(a)*/;
            }
        } catch(Throwable a5) {
            sLogger.e(a5);
        }
    }
    
    public static java.util.List getFavoriteContacts(String s) {
        Object a = null;
        Throwable a0 = null;
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        boolean b = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile();
        label0: {
            Object a1 = null;
            label3: if (b) {
                a1 = EMPTY_LIST;
            } else {
                synchronized(lockObj) {
                    Object a2 = null;
                    java.util.ArrayList a3 = null;
                    android.database.sqlite.SQLiteDatabase a4 = com.navdy.hud.app.storage.db.HudDatabase.getInstance().getWritableDatabase();
                    if (a4 == null) {
                        throw new com.navdy.hud.app.storage.db.DatabaseUtil$DatabaseNotAvailable();
                    }
                    FAV_CONTACT_ARGS[0] = s;
                    android.database.Cursor a5 = a4.query("fav_contacts", FAV_CONTACT_PROJECTION, "device_id=?", FAV_CONTACT_ARGS, (String)null, (String)null, "rowid");
                    label1: {
                        label2: {
                            if (a5 == null) {
                                break label2;
                            }
                            try {
                                a2 = a5;
                                if (!a5.moveToFirst()) {
                                    break label2;
                                }
                                a2 = a5;
                                com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance();
                                a3 = new java.util.ArrayList();
                                a2 = a5;
                                while(true) {
                                    String s0 = ((android.database.Cursor)a2).getString(0);
                                    String s1 = ((android.database.Cursor)a2).getString(1);
                                    int i = ((android.database.Cursor)a2).getInt(2);
                                    int i0 = ((android.database.Cursor)a2).getInt(3);
                                    long j = ((android.database.Cursor)a2).getLong(4);
                                    ((java.util.List)a3).add(new com.navdy.hud.app.framework.contacts.Contact(s0, s1, com.navdy.hud.app.framework.contacts.NumberType.buildFromValue(i), i0, j));
                                    if (!((android.database.Cursor)a2).moveToNext()) {
                                        break label1;
                                    }
                                }
                            } catch(Throwable a6) {
                                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a2);
                                throw a6;
                            }
                        }
                        java.util.List a7 = EMPTY_LIST;
                        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a5);
                        /*monexit(a)*/;
                        a1 = a7;
                        break label3;
                    }
                    com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a2);
                    /*monexit(a)*/;
                    a1 = a3;
                }
            }
            return (java.util.List)a1;
        }
        while(true) {
            try {
                /*monexit(a)*/;
            } catch(IllegalMonitorStateException | NullPointerException a9) {
                Throwable a10 = a9;
                a0 = a10;
                continue;
            }
            throw a0;
        }
    }
    
    public static void storeFavoriteContacts(String s, java.util.List a, boolean b) {
        Object a0 = null;
        Throwable a1 = null;
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        sLogger.v(new StringBuilder().append("fav-contacts passed [").append(a.size()).append("]").toString());
        com.navdy.hud.app.framework.contacts.FavoriteContactsManager a2 = com.navdy.hud.app.framework.contacts.FavoriteContactsManager.getInstance();
        com.navdy.hud.app.storage.db.helper.FavoriteContactsPersistenceHelper.deleteFavoriteContacts(s);
        int i = a.size();
        label0: {
            if (i != 0) {
                com.navdy.hud.app.framework.contacts.PhoneImageDownloader.getInstance().clearAllPhotoCheckEntries();
                synchronized(lockObj) {
                    int i0 = 0;
                    android.database.sqlite.SQLiteDatabase a3 = com.navdy.hud.app.storage.db.HudDatabase.getInstance().getWritableDatabase();
                    if (a3 == null) {
                        throw new com.navdy.hud.app.storage.db.DatabaseUtil$DatabaseNotAvailable();
                    }
                    android.database.sqlite.SQLiteStatement a4 = a3.compileStatement("INSERT INTO fav_contacts(device_id,name,number,number_type,def_image,number_numeric) VALUES (?,?,?,?,?,?)");
                    try {
                        com.navdy.hud.app.framework.contacts.ContactImageHelper a5 = com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance();
                        a3.beginTransaction();
                        java.util.Iterator a6 = a.iterator();
                        Object a7 = a;
                        i0 = 0;
                        Object a8 = a6;
                        while(((java.util.Iterator)a8).hasNext()) {
                            com.navdy.hud.app.framework.contacts.Contact a9 = (com.navdy.hud.app.framework.contacts.Contact)((java.util.Iterator)a8).next();
                            a4.clearBindings();
                            a4.bindString(1, s);
                            if (android.text.TextUtils.isEmpty((CharSequence)a9.name)) {
                                a4.bindNull(2);
                            } else {
                                a4.bindString(2, a9.name);
                            }
                            a4.bindString(3, a9.number);
                            a4.bindLong(4, (long)a9.numberType.getValue());
                            a9.defaultImageIndex = a5.getContactImageIndex(a9.number);
                            a4.bindLong(5, (long)a9.defaultImageIndex);
                            a4.bindLong(6, a9.numericNumber);
                            a4.execute();
                            i0 = i0 + 1;
                            if (b) {
                                com.navdy.hud.app.framework.contacts.PhoneImageDownloader.getInstance().submitDownload(a9.number, com.navdy.hud.app.framework.contacts.PhoneImageDownloader$Priority.NORMAL, com.navdy.service.library.events.photo.PhotoType.PHOTO_CONTACT, a9.name);
                            }
                        }
                        a3.setTransactionSuccessful();
                        com.navdy.hud.app.framework.contacts.FavoriteContactsManager.getInstance().setFavoriteContacts((java.util.List)a7);
                    } catch(Throwable a10) {
                        a3.endTransaction();
                        throw a10;
                    }
                    a3.endTransaction();
                    sLogger.v(new StringBuilder().append("fav-contacts rows added:").append(i0).toString());
                    /*monexit(a0)*/;
                }
            } else {
                a2.setFavoriteContacts((java.util.List)null);
            }
            return;
        }
        while(true) {
            try {
                /*monexit(a0)*/;
            } catch(IllegalMonitorStateException | NullPointerException a12) {
                Throwable a13 = a12;
                a1 = a13;
                continue;
            }
            throw a1;
        }
    }
}
