package com.navdy.hud.app.common;

final public class ProdModule$$ModuleAdapter$ProvideObdDataReceorderProvidesAdapter extends dagger.internal.ProvidesBinding implements javax.inject.Provider {
    private dagger.internal.Binding bus;
    private dagger.internal.Binding connectionHandler;
    final private com.navdy.hud.app.common.ProdModule module;
    
    public ProdModule$$ModuleAdapter$ProvideObdDataReceorderProvidesAdapter(com.navdy.hud.app.common.ProdModule a) {
        super("com.navdy.hud.app.debug.DriveRecorder", true, "com.navdy.hud.app.common.ProdModule", "provideObdDataReceorder");
        this.module = a;
        this.setLibrary(true);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.bus = a.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.common.ProdModule.class, (this).getClass().getClassLoader());
        this.connectionHandler = a.requestBinding("com.navdy.hud.app.service.ConnectionHandler", com.navdy.hud.app.common.ProdModule.class, (this).getClass().getClassLoader());
    }
    
    public com.navdy.hud.app.debug.DriveRecorder get() {
        return this.module.provideObdDataReceorder((com.squareup.otto.Bus)this.bus.get(), (com.navdy.hud.app.service.ConnectionHandler)this.connectionHandler.get());
    }
    
    public Object get() {
        return this.get();
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a.add(this.bus);
        a.add(this.connectionHandler);
    }
}
