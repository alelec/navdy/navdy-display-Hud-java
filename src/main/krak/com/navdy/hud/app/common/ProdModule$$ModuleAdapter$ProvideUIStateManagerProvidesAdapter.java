package com.navdy.hud.app.common;

final public class ProdModule$$ModuleAdapter$ProvideUIStateManagerProvidesAdapter extends dagger.internal.ProvidesBinding implements javax.inject.Provider {
    final private com.navdy.hud.app.common.ProdModule module;
    
    public ProdModule$$ModuleAdapter$ProvideUIStateManagerProvidesAdapter(com.navdy.hud.app.common.ProdModule a) {
        super("com.navdy.hud.app.ui.framework.UIStateManager", true, "com.navdy.hud.app.common.ProdModule", "provideUIStateManager");
        this.module = a;
        this.setLibrary(true);
    }
    
    public com.navdy.hud.app.ui.framework.UIStateManager get() {
        return this.module.provideUIStateManager();
    }
    
    public Object get() {
        return this.get();
    }
}
