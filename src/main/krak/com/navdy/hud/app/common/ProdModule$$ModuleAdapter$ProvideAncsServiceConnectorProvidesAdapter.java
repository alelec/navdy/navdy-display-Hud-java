package com.navdy.hud.app.common;

final public class ProdModule$$ModuleAdapter$ProvideAncsServiceConnectorProvidesAdapter extends dagger.internal.ProvidesBinding implements javax.inject.Provider {
    private dagger.internal.Binding bus;
    final private com.navdy.hud.app.common.ProdModule module;
    
    public ProdModule$$ModuleAdapter$ProvideAncsServiceConnectorProvidesAdapter(com.navdy.hud.app.common.ProdModule a) {
        super("com.navdy.hud.app.ancs.AncsServiceConnector", true, "com.navdy.hud.app.common.ProdModule", "provideAncsServiceConnector");
        this.module = a;
        this.setLibrary(true);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.bus = a.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.common.ProdModule.class, (this).getClass().getClassLoader());
    }
    
    public com.navdy.hud.app.ancs.AncsServiceConnector get() {
        return this.module.provideAncsServiceConnector((com.squareup.otto.Bus)this.bus.get());
    }
    
    public Object get() {
        return this.get();
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a.add(this.bus);
    }
}
