package com.navdy.hud.app.common;

class MainThreadBus$1 implements Runnable {
    final com.navdy.hud.app.common.MainThreadBus this$0;
    final Object val$event;
    
    MainThreadBus$1(com.navdy.hud.app.common.MainThreadBus a, Object a0) {
        super();
        this.this$0 = a;
        this.val$event = a0;
    }
    
    public void run() {
        long j = (this.this$0.threshold <= 0) ? 0L : android.os.SystemClock.elapsedRealtime();
        com.navdy.hud.app.common.MainThreadBus.access$001(this.this$0, this.val$event);
        if (this.this$0.threshold > 0) {
            long j0 = android.os.SystemClock.elapsedRealtime() - j;
            if (j0 >= (long)this.this$0.threshold) {
                com.navdy.hud.app.common.MainThreadBus.access$100(this.this$0).v(new StringBuilder().append("[").append(j0).append("] MainThreadBus-event [").append(this.val$event).append("]").toString());
            }
        }
    }
}
