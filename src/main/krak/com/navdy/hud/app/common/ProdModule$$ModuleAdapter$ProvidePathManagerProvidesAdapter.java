package com.navdy.hud.app.common;

final public class ProdModule$$ModuleAdapter$ProvidePathManagerProvidesAdapter extends dagger.internal.ProvidesBinding implements javax.inject.Provider {
    final private com.navdy.hud.app.common.ProdModule module;
    
    public ProdModule$$ModuleAdapter$ProvidePathManagerProvidesAdapter(com.navdy.hud.app.common.ProdModule a) {
        super("com.navdy.hud.app.storage.PathManager", true, "com.navdy.hud.app.common.ProdModule", "providePathManager");
        this.module = a;
        this.setLibrary(true);
    }
    
    public com.navdy.hud.app.storage.PathManager get() {
        return this.module.providePathManager();
    }
    
    public Object get() {
        return this.get();
    }
}
