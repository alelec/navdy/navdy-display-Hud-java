package com.navdy.hud.app.view;

public class MaxWidthLinearLayout extends android.widget.LinearLayout {
    private int maxWidth;
    
    public MaxWidthLinearLayout(android.content.Context a) {
        super(a);
        this.maxWidth = 0;
    }
    
    public MaxWidthLinearLayout(android.content.Context a, android.util.AttributeSet a0) {
        super(a, a0);
        android.content.res.TypedArray a1 = this.getContext().obtainStyledAttributes(a0, com.navdy.hud.app.R$styleable.MaxWidthLinearLayout);
        this.maxWidth = a1.getDimensionPixelSize(0, 2147483647);
        a1.recycle();
    }
    
    public int getMaxWidth() {
        return this.maxWidth;
    }
    
    protected void onMeasure(int i, int i0) {
        int i1 = android.view.View$MeasureSpec.getSize(i);
        if (this.maxWidth > 0 && this.maxWidth < i1) {
            int i2 = android.view.View$MeasureSpec.getMode(i);
            i = android.view.View$MeasureSpec.makeMeasureSpec(this.maxWidth, i2);
        }
        super.onMeasure(i, i0);
    }
    
    public void setMaxWidth(int i) {
        this.maxWidth = i;
        this.invalidate();
    }
}
