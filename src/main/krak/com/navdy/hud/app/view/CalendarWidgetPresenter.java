package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class CalendarWidgetPresenter extends com.navdy.hud.app.view.DashboardWidgetPresenter {
    final private static long MILLISECONDS_IN_A_DAY = 86400000L;
    final private static int REFRESH_INTERVAL;
    final private static com.navdy.service.library.log.Logger sLogger;
    private String calendarGaugeName;
    com.navdy.hud.app.framework.calendar.CalendarManager calendarManager;
    private android.content.Context context;
    private String formattedText;
    private android.os.Handler handler;
    private boolean nextEventExists;
    private int paddingLeft;
    private Runnable refreshRunnable;
    private com.navdy.hud.app.common.TimeHelper timeHelper;
    private String title;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.view.CalendarWidgetPresenter.class);
        REFRESH_INTERVAL = (int)java.util.concurrent.TimeUnit.MINUTES.toMillis(5L);
    }
    
    public CalendarWidgetPresenter(android.content.Context a) {
        this.timeHelper = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getTimeHelper();
        this.context = a;
        this.paddingLeft = a.getResources().getDimensionPixelSize(R.dimen.calendar_widget_left_padding);
        this.calendarGaugeName = a.getResources().getString(R.string.widget_calendar);
        this.calendarManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getCalendarManager();
        this.handler = new android.os.Handler();
        this.refreshRunnable = (Runnable)new com.navdy.hud.app.view.CalendarWidgetPresenter$1(this);
    }
    
    private String formatTime(long j) {
        StringBuilder a = new StringBuilder();
        String s = this.timeHelper.formatTime(new java.util.Date(j), a);
        if (a.length() > 0) {
            s = new StringBuilder().append(s).append(" ").append(a.toString()).toString();
        }
        return s;
    }
    
    public android.graphics.drawable.Drawable getDrawable() {
        return null;
    }
    
    public String getWidgetIdentifier() {
        return "CALENDAR_WIDGET";
    }
    
    public String getWidgetName() {
        return this.calendarGaugeName;
    }
    
    protected boolean isRegisteringToBusRequired() {
        return true;
    }
    
    public void onCalendarManagerEvent(com.navdy.hud.app.framework.calendar.CalendarManager$CalendarManagerEvent a) {
        if (com.navdy.hud.app.view.CalendarWidgetPresenter$2.$SwitchMap$com$navdy$hud$app$framework$calendar$CalendarManager$CalendarManagerEvent[a.ordinal()] != 0) {
            this.refreshCalendarEvent();
        }
    }
    
    public void onClockChanged(com.navdy.hud.app.common.TimeHelper$UpdateClock a) {
        this.refreshCalendarEvent();
    }
    
    public void refreshCalendarEvent() {
        java.util.List a = this.calendarManager.getCalendarEvents();
        long j = System.currentTimeMillis();
        java.util.TimeZone a0 = this.timeHelper.getTimeZone();
        java.util.Calendar a1 = java.util.Calendar.getInstance(a0);
        a1.setTime(new java.util.Date(j));
        com.navdy.service.library.events.calendars.CalendarEvent a2 = null;
        if (a != null) {
            Object a3 = a.iterator();
            a2 = null;
            while(true) {
                boolean b = ((java.util.Iterator)a3).hasNext();
                com.navdy.service.library.events.calendars.CalendarEvent a4 = a2;
                if (!b) {
                    break;
                }
                a2 = (com.navdy.service.library.events.calendars.CalendarEvent)((java.util.Iterator)a3).next();
                sLogger.d(new StringBuilder().append("Event :").append(a2.display_name).append(", From : ").append(a2.start_time).append(", To : ").append(a2.end_time).append(", All day : ").append(a2.all_day).toString());
                long j0 = ((Long)com.squareup.wire.Wire.get(a2.end_time, Long.valueOf(0L))).longValue();
                long j1 = ((Long)com.squareup.wire.Wire.get(a2.start_time, Long.valueOf(0L))).longValue();
                java.util.Calendar a5 = java.util.Calendar.getInstance(a0);
                a5.setTime(new java.util.Date(j1));
                {
                    if (j0 <= j) {
                        a2 = a4;
                        continue;
                    }
                    if (a1.get(1) != a5.get(1)) {
                        a2 = a4;
                        continue;
                    }
                    if (a1.get(6) != a5.get(6)) {
                        a2 = a4;
                        continue;
                    }
                    if (!a2.all_day.booleanValue()) {
                        break;
                    }
                    if (a4 != null) {
                        a2 = a4;
                    }
                }
            }
        }
        if (a2 == null) {
            sLogger.d("No event shown");
            this.nextEventExists = false;
            this.formattedText = "";
            this.title = "";
        } else {
            sLogger.d(new StringBuilder().append("Next event shown : ").append(a2.display_name).toString());
            this.nextEventExists = true;
            this.formattedText = this.formatTime(a2.start_time.longValue());
            this.title = a2.display_name;
        }
        this.handler.removeCallbacks(this.refreshRunnable);
        this.handler.postDelayed(this.refreshRunnable, (long)REFRESH_INTERVAL);
        this.reDraw();
    }
    
    public void setView(com.navdy.hud.app.view.DashboardWidgetView a, android.os.Bundle a0) {
        if (a == null) {
            this.handler.removeCallbacks(this.refreshRunnable);
        } else {
            int i = (a0 == null) ? 0 : a0.getInt("EXTRA_GRAVITY", 0);
            a.setContentView(R.layout.calendar_widget);
            int i0 = (i != 2) ? 0 : this.paddingLeft;
            a.getChildAt(0).setPadding(i0, 0, 0, 0);
            this.handler.removeCallbacks(this.refreshRunnable);
            this.refreshCalendarEvent();
        }
        super.setView(a, a0);
    }
    
    protected void updateGauge() {
        if (this.mWidgetView != null) {
            android.widget.TextView a = (android.widget.TextView)this.mWidgetView.findViewById(R.id.txt_unit);
            android.widget.TextView a0 = (android.widget.TextView)this.mWidgetView.findViewById(R.id.txt_value);
            if (this.nextEventExists) {
                a.setText((CharSequence)this.formattedText);
                a0.setText((CharSequence)this.title);
            } else {
                a.setText(R.string.today);
                a0.setText(R.string.no_more_events);
            }
        }
    }
}
