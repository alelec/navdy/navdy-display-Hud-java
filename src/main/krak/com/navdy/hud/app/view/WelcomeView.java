package com.navdy.hud.app.view;

public class WelcomeView extends android.widget.FrameLayout implements com.navdy.hud.app.manager.InputManager$IInputHandler {
    final private static int DOWNLOAD_APP_STAY_INTERVAL;
    final private static int DOWNLOAD_APP_TRANSITION_INTERVAL;
    final public static int STATE_DOWNLOAD_APP = 1;
    final public static int STATE_PICKER = 2;
    final public static int STATE_UNKNOWN = -1;
    final public static int STATE_WELCOME = 3;
    private static int[] appStoreIcons;
    private static int downloadColor1;
    private static int downloadColor2;
    private static String[] downloadText1;
    private static String[] downloadText2;
    private static int[] playStoreIcons;
    final private static com.navdy.service.library.log.Logger sLogger;
    private Runnable animationRunnable;
    @InjectView(R.id.animatorView)
    public com.navdy.hud.app.ui.component.FluctuatorAnimatorView animatorView;
    @InjectView(R.id.app_store_img_1)
    public android.widget.ImageView appStoreImage1;
    @InjectView(R.id.app_store_img_2)
    public android.widget.ImageView appStoreImage2;
    @InjectView(R.id.carousel)
    public com.navdy.hud.app.ui.component.carousel.CarouselLayout carousel;
    @InjectView(R.id.choiceLayout)
    public com.navdy.hud.app.ui.component.ChoiceLayout choiceLayout;
    private int currentDownloadIndex;
    private int currentDownloadView;
    @InjectView(R.id.download_app)
    public android.widget.RelativeLayout downloadAppContainer;
    @InjectView(R.id.download_text_1)
    public android.widget.TextView downloadAppTextView1;
    @InjectView(R.id.download_text_2)
    public android.widget.TextView downloadAppTextView2;
    private android.os.Handler handler;
    private int initialState;
    @InjectView(R.id.leftDot)
    public android.widget.ImageView leftDot;
    public java.util.List list;
    @InjectView(R.id.message)
    public android.widget.TextView messageView;
    @InjectView(R.id.play_store_img_1)
    public android.widget.ImageView playStoreImage1;
    @InjectView(R.id.play_store_img_2)
    public android.widget.ImageView playStoreImage2;
    @Inject
    public com.navdy.hud.app.screen.WelcomeScreen$Presenter presenter;
    @InjectView(R.id.rightDot)
    public android.widget.ImageView rightDot;
    @InjectView(R.id.rootContainer)
    public android.view.View rootContainer;
    com.squareup.picasso.Transformation roundTransformation;
    private boolean searching;
    private int state;
    @InjectView(R.id.subTitle)
    public android.widget.TextView subTitleView;
    @InjectView(R.id.title)
    public android.widget.TextView titleView;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.view.WelcomeView.class);
        DOWNLOAD_APP_TRANSITION_INTERVAL = (int)java.util.concurrent.TimeUnit.MILLISECONDS.toMillis(200L);
        DOWNLOAD_APP_STAY_INTERVAL = (int)java.util.concurrent.TimeUnit.SECONDS.toMillis(5L) - DOWNLOAD_APP_TRANSITION_INTERVAL;
    }
    
    public WelcomeView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public WelcomeView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public WelcomeView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.state = -1;
        this.currentDownloadIndex = -1;
        this.currentDownloadView = -1;
        this.list = (java.util.List)new java.util.ArrayList();
        this.roundTransformation = new com.makeramen.RoundedTransformationBuilder().oval(true).build();
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.animationRunnable = (Runnable)new com.navdy.hud.app.view.WelcomeView$1(this);
        if (!this.isInEditMode()) {
            mortar.Mortar.inject(a, this);
        }
        this.initFromAttributes(a, a0);
    }
    
    static int access$000(com.navdy.hud.app.view.WelcomeView a) {
        return a.currentDownloadIndex;
    }
    
    static int access$002(com.navdy.hud.app.view.WelcomeView a, int i) {
        a.currentDownloadIndex = i;
        return i;
    }
    
    static int access$008(com.navdy.hud.app.view.WelcomeView a) {
        int i = a.currentDownloadIndex;
        a.currentDownloadIndex = i + 1;
        return i;
    }
    
    static int access$100(com.navdy.hud.app.view.WelcomeView a) {
        return a.currentDownloadView;
    }
    
    static void access$1000(com.navdy.hud.app.view.WelcomeView a, java.io.File a0, android.widget.ImageView a1, com.squareup.picasso.Callback a2) {
        a.setImage(a0, a1, a2);
    }
    
    static int access$102(com.navdy.hud.app.view.WelcomeView a, int i) {
        a.currentDownloadView = i;
        return i;
    }
    
    static void access$1100(com.navdy.hud.app.view.WelcomeView a, com.navdy.hud.app.ui.component.carousel.Carousel$Model a0) {
        a.updateInfo(a0);
    }
    
    static int access$200() {
        return DOWNLOAD_APP_TRANSITION_INTERVAL;
    }
    
    static int[] access$300() {
        return appStoreIcons;
    }
    
    static Runnable access$400(com.navdy.hud.app.view.WelcomeView a) {
        return a.animationRunnable;
    }
    
    static android.os.Handler access$500(com.navdy.hud.app.view.WelcomeView a) {
        return a.handler;
    }
    
    static int access$600() {
        return DOWNLOAD_APP_STAY_INTERVAL;
    }
    
    static void access$700(com.navdy.hud.app.view.WelcomeView a, android.widget.TextView a0, int i) {
        a.setDownloadText(a0, i);
    }
    
    static void access$800(com.navdy.hud.app.view.WelcomeView a, android.widget.ImageView a0, int i) {
        a.setDownloadImage(a0, i);
    }
    
    static int[] access$900() {
        return playStoreIcons;
    }
    
    private void initFromAttributes(android.content.Context a, android.util.AttributeSet a0) {
        android.content.res.TypedArray a1 = a.getTheme().obtainStyledAttributes(a0, com.navdy.hud.app.R$styleable.WelcomeView, 0, 0);
        try {
            this.initialState = a1.getInt(0, 1);
        } catch(Throwable a2) {
            a1.recycle();
            throw a2;
        }
        a1.recycle();
    }
    
    private void initializeCarousel() {
        com.navdy.hud.app.ui.component.carousel.Carousel$InitParams a = new com.navdy.hud.app.ui.component.carousel.Carousel$InitParams();
        a.model = this.list;
        a.rootContainer = this.rootContainer;
        a.viewProcessor = (com.navdy.hud.app.ui.component.carousel.Carousel$ViewProcessor)new com.navdy.hud.app.view.WelcomeView$3(this);
        a.infoLayoutResourceId = R.layout.carousel_view_menu;
        a.imageLytResourceId = R.layout.crossfade_image_lyt;
        a.carouselIndicator = null;
        a.animator = (com.navdy.hud.app.ui.component.carousel.AnimationStrategy)new com.navdy.hud.app.ui.component.carousel.ShrinkAnimator();
        this.carousel.init(a);
        this.carousel.setListener((com.navdy.hud.app.ui.component.carousel.Carousel$Listener)new com.navdy.hud.app.view.WelcomeView$4(this));
    }
    
    private void setDownloadImage(android.widget.ImageView a, int i) {
        a.setImageResource(i);
    }
    
    private void setDownloadText(android.widget.TextView a, int i) {
        android.text.SpannableStringBuilder a0 = new android.text.SpannableStringBuilder();
        a0.append((CharSequence)downloadText1[i]);
        a0.setSpan(new android.text.style.ForegroundColorSpan(downloadColor1), 0, a0.length(), 33);
        a0.append((CharSequence)" ");
        int i0 = a0.length();
        a0.append((CharSequence)downloadText2[i]);
        a0.setSpan(new android.text.style.ForegroundColorSpan(downloadColor2), i0, a0.length(), 33);
        a0.setSpan(new android.text.style.StyleSpan(1), i0, a0.length(), 33);
        a.setText((CharSequence)a0);
    }
    
    private void setImage(java.io.File a, android.widget.ImageView a0, com.squareup.picasso.Callback a1) {
        com.navdy.hud.app.util.picasso.PicassoUtil.getInstance().load(a).noPlaceholder().fit().transform(this.roundTransformation).noFade().into(a0, a1);
    }
    
    private void startDownloadAppAnimation() {
        sLogger.v("startDownloadAppAnimation");
        android.content.res.Resources a = this.getContext().getResources();
        if (downloadText1 == null) {
            downloadText1 = a.getStringArray(R.array.download_app_1);
            downloadText2 = a.getStringArray(R.array.download_app_2);
            downloadColor1 = a.getColor(R.color.download_app_1);
            downloadColor2 = a.getColor(R.color.download_app_2);
            android.content.res.TypedArray a0 = this.getResources().obtainTypedArray(R.array.download_app_icon_app_store);
            appStoreIcons = new int[downloadText1.length];
            int i = 0;
            while(i < appStoreIcons.length) {
                appStoreIcons[i] = a0.getResourceId(i, 0);
                i = i + 1;
            }
            a0.recycle();
            android.content.res.TypedArray a1 = this.getResources().obtainTypedArray(R.array.download_app_icon_play_store);
            playStoreIcons = new int[downloadText1.length];
            int i0 = 0;
            while(i0 < playStoreIcons.length) {
                playStoreIcons[i0] = a1.getResourceId(i0, 0);
                i0 = i0 + 1;
            }
            a1.recycle();
        }
        this.currentDownloadIndex = 0;
        this.currentDownloadView = 0;
        int i1 = this.currentDownloadIndex;
        int i2 = this.currentDownloadIndex + 1;
        this.setDownloadText(this.downloadAppTextView1, i1);
        this.setDownloadText(this.downloadAppTextView2, i2);
        this.setDownloadImage(this.appStoreImage1, (appStoreIcons[i1] != 0) ? 1 : 0);
        this.setDownloadImage(this.appStoreImage2, (appStoreIcons[i2] != 0) ? 1 : 0);
        this.setDownloadImage(this.playStoreImage1, (playStoreIcons[i1] != 0) ? 1 : 0);
        this.setDownloadImage(this.playStoreImage2, (playStoreIcons[i2] != 0) ? 1 : 0);
        this.downloadAppTextView1.setAlpha(1f);
        this.downloadAppTextView2.setAlpha(0.0f);
        this.appStoreImage1.setAlpha(1f);
        this.appStoreImage2.setAlpha(0.0f);
        this.playStoreImage1.setAlpha(1f);
        this.playStoreImage2.setAlpha(0.0f);
        this.downloadAppContainer.setVisibility(0);
        this.currentDownloadIndex = this.currentDownloadIndex + 1;
        this.handler.removeCallbacks(this.animationRunnable);
        this.handler.postDelayed(this.animationRunnable, (long)DOWNLOAD_APP_STAY_INTERVAL);
    }
    
    private void stopDownloadAppAnimation() {
        sLogger.v("stopDownloadAppAnimation");
        this.handler.removeCallbacks(this.animationRunnable);
        this.downloadAppTextView1.animate().cancel();
        this.downloadAppTextView2.animate().cancel();
        this.appStoreImage1.animate().cancel();
        this.appStoreImage2.animate().cancel();
        this.playStoreImage1.animate().cancel();
        this.playStoreImage2.animate().cancel();
        this.downloadAppContainer.setVisibility(8);
        this.currentDownloadIndex = -1;
    }
    
    private void updateInfo(com.navdy.hud.app.ui.component.carousel.Carousel$Model a) {
        if (a != null && a.infoMap != null) {
            String s = (String)a.infoMap.get(Integer.valueOf(R.id.title));
            String s0 = (String)a.infoMap.get(Integer.valueOf(R.id.subTitle));
            String s1 = (String)a.infoMap.get(Integer.valueOf(R.id.message));
            android.widget.TextView a0 = this.titleView;
            if (s == null) {
                s = "";
            }
            a0.setText((CharSequence)s);
            android.widget.TextView a1 = this.subTitleView;
            if (s0 == null) {
                s0 = "";
            }
            a1.setText((CharSequence)s0);
            android.widget.TextView a2 = this.messageView;
            if (s1 == null) {
                s1 = "";
            }
            a2.setText((CharSequence)s1);
        }
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        return com.navdy.hud.app.manager.InputManager.nextContainingHandler((android.view.View)this);
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
        if (this.searching) {
            this.animatorView.start();
        }
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.searching = false;
        this.animatorView.stop();
        this.stopDownloadAppAnimation();
        if (this.presenter != null) {
            this.presenter.dropView((android.view.View)this);
        }
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View)this);
        this.setState(this.initialState);
        this.initializeCarousel();
        java.util.ArrayList a = new java.util.ArrayList(1);
        ((java.util.List)a).add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(this.getResources().getString(R.string.welcome_cancel), 0));
        this.choiceLayout.setChoices(com.navdy.hud.app.ui.component.ChoiceLayout$Mode.LABEL, (java.util.List)a, 0, (com.navdy.hud.app.ui.component.ChoiceLayout$IListener)new com.navdy.hud.app.view.WelcomeView$2(this));
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        return false;
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        boolean b = false;
        if (this.state != 1) {
            b = this.carousel.onKey(a);
        } else {
            switch(com.navdy.hud.app.view.WelcomeView$5.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[a.ordinal()]) {
                case 3: {
                    this.choiceLayout.executeSelectedItem(true);
                    b = true;
                    break;
                }
                case 2: {
                    this.choiceLayout.moveSelectionRight();
                    b = true;
                    break;
                }
                case 1: {
                    this.choiceLayout.moveSelectionLeft();
                    b = true;
                    break;
                }
                default: {
                    b = false;
                }
            }
        }
        return b;
    }
    
    public void setSearching(boolean b) {
        if (b != this.searching) {
            this.searching = b;
            if (b) {
                this.animatorView.setVisibility(0);
                this.animatorView.start();
            } else {
                this.animatorView.stop();
                this.animatorView.setVisibility(8);
            }
        }
    }
    
    public void setState(int i) {
        if (this.state != i) {
            this.state = i;
            this.rootContainer.setVisibility(8);
            this.stopDownloadAppAnimation();
            this.leftDot.setVisibility(8);
            this.rightDot.setVisibility(8);
            switch(i) {
                case 2: case 3: {
                    this.rootContainer.setVisibility(0);
                    break;
                }
                case 1: {
                    this.startDownloadAppAnimation();
                    break;
                }
            }
        }
    }
}
