package com.navdy.hud.app.view;

public class ToastView extends android.widget.FrameLayout implements com.navdy.hud.app.manager.InputManager$IInputHandler {
    final private static com.navdy.service.library.log.Logger sLogger;
    private com.squareup.otto.Bus bus;
    public boolean gestureOn;
    com.navdy.hud.app.manager.InputManager inputManager;
    private com.navdy.hud.app.ui.activity.Main mainScreen;
    @InjectView(R.id.mainView)
    com.navdy.hud.app.ui.component.ConfirmationLayout mainView;
    private volatile boolean sendShowEvent;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.view.ToastView.class);
    }
    
    public ToastView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public ToastView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public ToastView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        com.navdy.hud.app.manager.RemoteDeviceManager a1 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance();
        this.inputManager = a1.getInputManager();
        this.bus = a1.getBus();
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    static com.squareup.otto.Bus access$100(com.navdy.hud.app.view.ToastView a) {
        return a.bus;
    }
    
    static boolean access$202(com.navdy.hud.app.view.ToastView a, boolean b) {
        a.sendShowEvent = b;
        return b;
    }
    
    static com.navdy.hud.app.ui.activity.Main access$300(com.navdy.hud.app.view.ToastView a) {
        return a.mainScreen;
    }
    
    static com.navdy.hud.app.ui.activity.Main access$302(com.navdy.hud.app.view.ToastView a, com.navdy.hud.app.ui.activity.Main a0) {
        a.mainScreen = a0;
        return a0;
    }
    
    public void animateIn(String s, String s0, String s1, boolean b) {
        this.sendShowEvent = false;
        this.mainView.setTranslationY((float)com.navdy.hud.app.framework.toast.ToastPresenter.ANIMATION_TRANSLATION);
        this.mainView.setAlpha(0.0f);
        this.mainView.animate().alpha(1f).translationY(0.0f).setDuration(b ? 0L : 100L).setStartDelay(250L).withStartAction((Runnable)new com.navdy.hud.app.view.ToastView$2(this, s0)).withEndAction((Runnable)new com.navdy.hud.app.view.ToastView$1(this, s, s1, s0)).start();
    }
    
    public void animateOut(boolean b) {
        int i = b ? 50 : 100;
        if (!this.sendShowEvent) {
            this.bus.post(new com.navdy.hud.app.framework.toast.ToastManager$ShowToast(com.navdy.hud.app.framework.toast.ToastPresenter.getCurrentId()));
            this.sendShowEvent = true;
        }
        this.mainView.animate().alpha(0.0f).translationY((float)com.navdy.hud.app.framework.toast.ToastPresenter.ANIMATION_TRANSLATION).setDuration((long)i).withStartAction((Runnable)new com.navdy.hud.app.view.ToastView$4(this)).withEndAction((Runnable)new com.navdy.hud.app.view.ToastView$3(this)).start();
    }
    
    public void dismissToast() {
        if (com.navdy.hud.app.framework.toast.ToastManager.getInstance().isToastDisplayed()) {
            this.animateOut(false);
        }
    }
    
    public com.navdy.hud.app.ui.component.ConfirmationLayout getConfirmation() {
        return this.mainView;
    }
    
    public com.navdy.hud.app.ui.component.ConfirmationLayout getMainLayout() {
        return this.mainView;
    }
    
    public com.navdy.hud.app.ui.component.ConfirmationLayout getView() {
        return this.mainView;
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        return null;
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View)this);
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        boolean b = false;
        boolean b0 = this.gestureOn;
        label0: {
            label2: {
                label1: {
                    label3: {
                        if (!b0) {
                            break label3;
                        }
                        if (a.gesture == null) {
                            break label3;
                        }
                        com.navdy.hud.app.framework.toast.IToastCallback a0 = com.navdy.hud.app.framework.toast.ToastPresenter.getCurrentCallback();
                        if (a0 == null) {
                            break label2;
                        }
                        java.util.List a1 = this.mainView.choiceLayout.getChoices();
                        if (a1 == null) {
                            break label1;
                        }
                        if (a1.size() == 0) {
                            break label1;
                        }
                        switch(com.navdy.hud.app.view.ToastView$5.$SwitchMap$com$navdy$service$library$events$input$Gesture[a.gesture.ordinal()]) {
                            case 2: {
                                if (a1.size() != 1) {
                                    a0.executeChoiceItem(1, ((com.navdy.hud.app.ui.component.ChoiceLayout$Choice)a1.get(1)).id);
                                } else {
                                    a0.executeChoiceItem(0, ((com.navdy.hud.app.ui.component.ChoiceLayout$Choice)a1.get(0)).id);
                                }
                                b = true;
                                break label0;
                            }
                            case 1: {
                                a0.executeChoiceItem(0, ((com.navdy.hud.app.ui.component.ChoiceLayout$Choice)a1.get(0)).id);
                                b = true;
                                break label0;
                            }
                        }
                    }
                    b = false;
                    break label0;
                }
                b = false;
                break label0;
            }
            b = false;
        }
        return b;
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        boolean b = false;
        com.navdy.hud.app.framework.toast.IToastCallback a0 = com.navdy.hud.app.framework.toast.ToastPresenter.getCurrentCallback();
        label2: {
            label3: {
                if (a0 == null) {
                    break label3;
                }
                if (a0.onKey(a)) {
                    b = true;
                    break label2;
                }
            }
            switch(com.navdy.hud.app.view.ToastView$5.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[a.ordinal()]) {
                case 4: case 5: {
                    b = false;
                    break;
                }
                case 3: {
                    boolean b0 = com.navdy.hud.app.framework.toast.ToastPresenter.hasTimeout();
                    label0: {
                        label1: {
                            if (!b0) {
                                break label1;
                            }
                            if (this.mainView.choiceLayout.getVisibility() != 0) {
                                break label0;
                            }
                        }
                        this.mainView.choiceLayout.executeSelectedItem(true);
                        b = true;
                        break;
                    }
                    this.dismissToast();
                    b = true;
                    break;
                }
                case 2: {
                    this.mainView.choiceLayout.moveSelectionRight();
                    b = true;
                    break;
                }
                case 1: {
                    this.mainView.choiceLayout.moveSelectionLeft();
                    b = true;
                    break;
                }
                default: {
                    b = true;
                }
            }
        }
        return b;
    }
    
    public void revertToOriginal() {
        this.mainView.fluctuatorView.stop();
        this.gestureOn = false;
    }
}
