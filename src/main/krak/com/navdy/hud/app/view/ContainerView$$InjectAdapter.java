package com.navdy.hud.app.view;

final public class ContainerView$$InjectAdapter extends dagger.internal.Binding implements dagger.MembersInjector {
    private dagger.internal.Binding uiStateManager;
    
    public ContainerView$$InjectAdapter() {
        super((String)null, "members/com.navdy.hud.app.view.ContainerView", false, com.navdy.hud.app.view.ContainerView.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.uiStateManager = a.requestBinding("com.navdy.hud.app.ui.framework.UIStateManager", com.navdy.hud.app.view.ContainerView.class, (this).getClass().getClassLoader());
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.uiStateManager);
    }
    
    public void injectMembers(com.navdy.hud.app.view.ContainerView a) {
        a.uiStateManager = (com.navdy.hud.app.ui.framework.UIStateManager)this.uiStateManager.get();
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.view.ContainerView)a);
    }
}
