package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class AutoBrightnessView extends android.widget.FrameLayout implements com.navdy.hud.app.manager.InputManager$IInputHandler {
    @InjectView(R.id.choiceLayout)
    com.navdy.hud.app.ui.component.ChoiceLayout autoBrightnessChoices;
    @InjectView(R.id.image)
    android.widget.ImageView autoBrightnessIcon;
    @InjectView(R.id.mainTitle)
    android.widget.TextView autoBrightnessTitle;
    @InjectView(R.id.title2)
    android.widget.TextView brightnessTitle;
    @InjectView(R.id.title3)
    android.widget.TextView brightnessTitleDesc;
    @InjectView(R.id.infoContainer)
    android.widget.LinearLayout infoContainer;
    @Inject
    com.navdy.hud.app.screen.AutoBrightnessScreen$Presenter presenter;
    String text;
    @InjectView(R.id.title1)
    android.widget.TextView title1;
    
    public AutoBrightnessView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public AutoBrightnessView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public AutoBrightnessView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        if (!this.isInEditMode()) {
            mortar.Mortar.inject(a, this);
        }
    }
    
    public void executeSelectedItem() {
        this.autoBrightnessChoices.executeSelectedItem(true);
    }
    
    public void moveSelectionLeft() {
        this.autoBrightnessChoices.moveSelectionLeft();
    }
    
    public void moveSelectionRight() {
        this.autoBrightnessChoices.moveSelectionRight();
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        return com.navdy.hud.app.manager.InputManager.nextContainingHandler((android.view.View)this);
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.presenter != null) {
            this.presenter.dropView((android.view.View)this);
        }
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View)this);
        android.content.res.Resources a = this.getResources();
        int i = (int)a.getDimension(R.dimen.auto_brightness_section_width);
        ((android.view.ViewGroup$MarginLayoutParams)this.infoContainer.getLayoutParams()).width = i;
        this.infoContainer.setMinimumWidth(i);
        int i0 = (int)a.getDimension(R.dimen.auto_brightness_choice_margin_bottom);
        ((android.view.ViewGroup$MarginLayoutParams)this.autoBrightnessChoices.getLayoutParams()).bottomMargin = i0;
        android.graphics.Typeface a0 = android.graphics.Typeface.create("sans-serif-medium", 0);
        this.autoBrightnessTitle.setTypeface(a0);
        this.title1.setVisibility(8);
        android.graphics.Typeface a1 = android.graphics.Typeface.create("sans-serif-light", 0);
        this.autoBrightnessTitle.setTypeface(a1);
        android.graphics.Typeface a2 = android.graphics.Typeface.create("sans-serif", 0);
        this.autoBrightnessTitle.setTypeface(a2);
        this.brightnessTitleDesc.setSingleLine(false);
        this.brightnessTitleDesc.setMaxLines(3);
        this.brightnessTitleDesc.setText((CharSequence)a.getString(R.string.auto_brightness_description_text));
        this.text = a.getString(R.string.auto_brightness_status_label_text);
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        return false;
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        return this.presenter.handleKey(a);
    }
    
    public void setChoices(java.util.List a, com.navdy.hud.app.ui.component.ChoiceLayout$IListener a0) {
        this.autoBrightnessChoices.setChoices(com.navdy.hud.app.ui.component.ChoiceLayout$Mode.LABEL, a, 0, a0);
    }
    
    public void setIcon(android.graphics.drawable.Drawable a) {
        this.autoBrightnessIcon.setImageDrawable(a);
    }
    
    public void setStatusLabel(String s) {
        android.text.SpannableStringBuilder a = new android.text.SpannableStringBuilder();
        a.append((CharSequence)this.text);
        a.setSpan(new android.text.style.TypefaceSpan("sans-serif-light"), 0, a.length(), 33);
        int i = a.length();
        a.append((CharSequence)s);
        a.setSpan(new android.text.style.StyleSpan(1), i, a.length(), 33);
        this.brightnessTitle.setText((CharSequence)a);
    }
    
    public void setTitle(String s) {
        this.autoBrightnessTitle.setText((CharSequence)s);
    }
}
