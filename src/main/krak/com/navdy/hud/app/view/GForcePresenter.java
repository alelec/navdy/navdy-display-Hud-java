package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class GForcePresenter extends com.navdy.hud.app.view.DashboardWidgetPresenter {
    com.navdy.hud.app.view.GForceDrawable drawable;
    private String gMeterGaugeName;
    private boolean registered;
    private float xAccel;
    private float yAccel;
    private float zAccel;
    
    public GForcePresenter(android.content.Context a) {
        this.drawable = new com.navdy.hud.app.view.GForceDrawable(a);
        this.gMeterGaugeName = a.getResources().getString(R.string.widget_g_meter);
    }
    
    public android.graphics.drawable.Drawable getDrawable() {
        return this.drawable;
    }
    
    public String getWidgetIdentifier() {
        return "GFORCE_WIDGET";
    }
    
    public String getWidgetName() {
        return this.gMeterGaugeName;
    }
    
    protected boolean isRegisteringToBusRequired() {
        return true;
    }
    
    public void onCalibratedGForceData(com.navdy.hud.app.device.gps.CalibratedGForceData a) {
        this.xAccel = a.getXAccel();
        this.yAccel = a.getYAccel();
        this.zAccel = a.getZAccel();
        this.reDraw();
    }
    
    public void setView(com.navdy.hud.app.view.DashboardWidgetView a) {
        if (a != null) {
            a.setContentView(R.layout.smart_dash_widget_circular_content_layout);
        }
        super.setView(a);
    }
    
    protected void updateGauge() {
        this.drawable.setAcceleration(this.xAccel, this.yAccel, this.zAccel);
    }
}
