package com.navdy.hud.app.view;

class TachometerGaugePresenter$1 implements Runnable {
    final com.navdy.hud.app.view.TachometerGaugePresenter this$0;
    
    TachometerGaugePresenter$1(com.navdy.hud.app.view.TachometerGaugePresenter a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        com.navdy.hud.app.view.TachometerGaugePresenter.access$002(this.this$0, new android.animation.ValueAnimator());
        android.animation.ValueAnimator a = com.navdy.hud.app.view.TachometerGaugePresenter.access$000(this.this$0);
        int[] a0 = new int[2];
        a0[0] = 255;
        a0[1] = 0;
        a.setIntValues(a0);
        com.navdy.hud.app.view.TachometerGaugePresenter.access$000(this.this$0).setDuration(1000L);
        com.navdy.hud.app.view.TachometerGaugePresenter.access$000(this.this$0).addUpdateListener((android.animation.ValueAnimator$AnimatorUpdateListener)new com.navdy.hud.app.view.TachometerGaugePresenter$1$1(this));
        com.navdy.hud.app.view.TachometerGaugePresenter.access$000(this.this$0).addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.view.TachometerGaugePresenter$1$2(this));
        com.navdy.hud.app.view.TachometerGaugePresenter.access$000(this.this$0).start();
    }
}
