package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class FirstLaunchView$$ViewInjector {
    public FirstLaunchView$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.view.FirstLaunchView a0, Object a1) {
        a0.firstLaunchLogo = (android.widget.ImageView)a.findRequiredView(a1, R.id.firstLaunchLogo, "field 'firstLaunchLogo'");
    }
    
    public static void reset(com.navdy.hud.app.view.FirstLaunchView a) {
        a.firstLaunchLogo = null;
    }
}
