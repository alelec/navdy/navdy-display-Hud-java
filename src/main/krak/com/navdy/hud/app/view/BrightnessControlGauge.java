package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class BrightnessControlGauge extends com.navdy.hud.app.view.Gauge {
    final private static int PIVOT_ANGLE = 5;
    final private static int THUMB_ANGLE = 10;
    private int mPivotIndicatorColor;
    private android.graphics.Paint mPivotIndicatorPaint;
    private int mPivotValue;
    private int mThumbColor;
    private android.graphics.Paint mThumbPaint;
    
    public BrightnessControlGauge(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public BrightnessControlGauge(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public BrightnessControlGauge(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        android.content.res.Resources a1 = this.getResources();
        this.mPivotValue = a.getTheme().obtainStyledAttributes(a0, com.navdy.hud.app.R$styleable.BrightnessControlGauge, 0, 0).getInteger(0, 0);
        this.mThumbColor = a1.getColor(R.color.thumb_color);
        this.mPivotIndicatorColor = a1.getColor(R.color.pivot_indicator_color);
    }
    
    protected void drawGauge(android.graphics.Canvas a) {
        float f = (float)this.getWidth();
        float f0 = (float)this.getHeight();
        float f1 = this.getRadius();
        float f2 = this.deltaToAngle(this.mPivotValue - this.mMinValue);
        float f3 = 5f / 2f;
        float f4 = f / 2f;
        float f5 = f0 / 2f;
        float f6 = f / 2f;
        float f7 = f0 / 2f;
        android.graphics.RectF a0 = new android.graphics.RectF();
        a0.set(f4 - f1, f5 - f1, f6 + f1, f7 + f1);
        a.drawArc(a0, (float)this.mStartAngle + (f2 - f3), 5f, false, this.mPivotIndicatorPaint);
        if (this.mValue <= this.mPivotValue) {
            if (this.mValue < this.mPivotValue) {
                this.drawIndicator(a, this.mValue, this.mPivotValue);
            }
        } else {
            this.drawIndicator(a, this.mPivotValue, this.mValue);
        }
        float f8 = this.deltaToAngle(this.mValue - this.mMinValue);
        float f9 = 10f / 2f;
        a.drawArc(a0, (float)this.mStartAngle + (f8 - f9), 10f, false, this.mThumbPaint);
    }
    
    protected void initDrawingTools() {
        super.initDrawingTools();
        this.mPivotIndicatorPaint = new android.graphics.Paint();
        this.mPivotIndicatorPaint.setStrokeWidth((float)this.mBackgroundThickness);
        this.mPivotIndicatorPaint.setAntiAlias(true);
        this.mPivotIndicatorPaint.setStrokeCap(android.graphics.Paint$Cap.BUTT);
        this.mPivotIndicatorPaint.setStyle(android.graphics.Paint$Style.STROKE);
        this.mThumbPaint = new android.graphics.Paint();
        this.mThumbPaint.setColor(this.mThumbColor);
        this.mThumbPaint.setStrokeWidth((float)this.mThickness);
        this.mThumbPaint.setAntiAlias(true);
        this.mThumbPaint.setStrokeCap(android.graphics.Paint$Cap.BUTT);
        this.mThumbPaint.setStyle(android.graphics.Paint$Style.STROKE);
    }
}
