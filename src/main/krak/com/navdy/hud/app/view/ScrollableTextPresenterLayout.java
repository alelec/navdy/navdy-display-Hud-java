package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class ScrollableTextPresenterLayout extends android.widget.RelativeLayout implements com.navdy.hud.app.ui.component.ChoiceLayout$IListener, com.navdy.hud.app.view.ObservableScrollView$IScrollListener, com.navdy.hud.app.manager.InputManager$IInputHandler {
    final public static int TAG_BACK = 0;
    @InjectView(R.id.bottomScrub)
    android.view.View bottomScrub;
    @InjectView(R.id.choiceLayout)
    com.navdy.hud.app.ui.component.ChoiceLayout mChoiceLayout;
    boolean mContentNeedsScrolling;
    private boolean mContentReachedBottom;
    private boolean mContentReachedTop;
    private int mCount;
    private int mCurrentItem;
    @InjectView(R.id.mainImage)
    android.widget.ImageView mMainImageView;
    @InjectView(R.id.mainTitle)
    android.widget.TextView mMainTitleText;
    @InjectView(R.id.message)
    android.widget.TextView mMessageText;
    @InjectView(R.id.notifIndicator)
    com.navdy.hud.app.ui.component.carousel.CarouselIndicator mNotificationIndicator;
    @InjectView(R.id.notifScrollIndicator)
    com.navdy.hud.app.ui.component.carousel.ProgressIndicator mNotificationScrollIndicator;
    @InjectView(R.id.scrollView)
    com.navdy.hud.app.view.ObservableScrollView mObservableScrollView;
    @Inject
    com.navdy.hud.app.screen.GestureLearningScreen$Presenter mPresenter;
    java.util.ArrayList mTextContents;
    @InjectView(R.id.title)
    android.widget.TextView mTitleText;
    private int scrollColor;
    @InjectView(R.id.topScrub)
    android.view.View topScrub;
    
    public ScrollableTextPresenterLayout(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public ScrollableTextPresenterLayout(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public ScrollableTextPresenterLayout(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.mTextContents = new java.util.ArrayList();
        this.mContentNeedsScrolling = false;
        if (!this.isInEditMode()) {
            mortar.Mortar.inject(a, this);
        }
    }
    
    static void access$000(com.navdy.hud.app.view.ScrollableTextPresenterLayout a, boolean b) {
        a.displayScrollIndicator(b);
    }
    
    static void access$100(com.navdy.hud.app.view.ScrollableTextPresenterLayout a) {
        a.layoutScrollIndicator();
    }
    
    private void displayScrollIndicator(boolean b) {
        if (this.mCount > 0) {
            this.mNotificationIndicator.setVisibility(0);
        }
        this.mNotificationIndicator.setCurrentItem(this.mCurrentItem, this.scrollColor);
        if (this.mContentNeedsScrolling) {
            if (b) {
                this.mNotificationIndicator.getViewTreeObserver().addOnGlobalLayoutListener((android.view.ViewTreeObserver$OnGlobalLayoutListener)new com.navdy.hud.app.view.ScrollableTextPresenterLayout$2(this));
            } else {
                this.layoutScrollIndicator();
            }
        } else {
            ((android.view.View)this.mNotificationScrollIndicator.getParent()).setVisibility(8);
        }
    }
    
    private void handleLeft() {
        boolean b = false;
        label2: if (this.mContentNeedsScrolling) {
            boolean b0 = this.mObservableScrollView.arrowScroll(33);
            label0: {
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.mContentReachedTop) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        } else {
            b = false;
        }
        if (!b && this.mCurrentItem > 0) {
            this.setCurrentItem(this.mCurrentItem - 1, true, true);
        }
    }
    
    private void handleRight() {
        boolean b = false;
        label2: if (this.mContentNeedsScrolling) {
            boolean b0 = this.mObservableScrollView.arrowScroll(130);
            label0: {
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.mContentReachedBottom) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        } else {
            b = false;
        }
        if (!b && this.mCurrentItem < this.mCount - 1) {
            this.setCurrentItem(this.mCurrentItem + 1, true, false);
        }
    }
    
    private void layoutScrollIndicator() {
        android.graphics.RectF a = this.mNotificationIndicator.getItemPos(this.mCurrentItem);
        label0: if (a != null) {
            float f = a.left;
            int i = (f > 0.0f) ? 1 : (f == 0.0f) ? 0 : -1;
            label1: {
                if (i != 0) {
                    break label1;
                }
                if (a.top == 0.0f) {
                    break label0;
                }
            }
            this.mNotificationScrollIndicator.setCurrentItem(1);
            android.view.View a0 = (android.view.View)this.mNotificationScrollIndicator.getParent();
            a0.setX(this.mNotificationIndicator.getX() + (float)com.navdy.hud.app.framework.glance.GlanceConstants.scrollingIndicatorLeftPadding);
            a0.setY(this.mNotificationIndicator.getY() + a.top + (float)(com.navdy.hud.app.framework.glance.GlanceConstants.scrollingIndicatorCircleSize / 2) - (float)(com.navdy.hud.app.framework.glance.GlanceConstants.scrollingIndicatorHeight / 2));
            a0.setVisibility(0);
        }
    }
    
    private void setCurrentItem(int i, boolean b, boolean b0) {
        if (i >= 0 && i < this.mCount) {
            this.mCurrentItem = i;
            Object a = this.mTextContents.get(this.mCurrentItem);
            this.mContentNeedsScrolling = com.navdy.hud.app.framework.glance.GlanceHelper.needsScrollLayout(((CharSequence)a).toString());
            this.mObservableScrollView.fullScroll(33);
            this.mMessageText.setText((CharSequence)a);
            this.mMessageText.setVisibility(0);
            this.topScrub.setVisibility(8);
            this.bottomScrub.setVisibility(0);
            this.mContentReachedTop = true;
            this.mContentReachedBottom = false;
            if (b) {
                android.animation.AnimatorSet a0 = this.mNotificationIndicator.getItemMoveAnimator(this.mCurrentItem, this.scrollColor);
                if (a0 == null) {
                    this.displayScrollIndicator(false);
                } else {
                    a0.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.view.ScrollableTextPresenterLayout$1(this));
                    a0.start();
                }
            } else {
                this.displayScrollIndicator(true);
            }
        }
    }
    
    public void executeItem(int i, int i0) {
        if (i0 == 0) {
            this.mPresenter.hideTips();
        }
    }
    
    public void itemSelected(int i, int i0) {
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        return null;
    }
    
    public void onBottom() {
        if (this.mContentNeedsScrolling) {
            this.mContentReachedTop = false;
            this.mContentReachedBottom = true;
            this.onPosChange(100);
        }
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View)this);
        this.mChoiceLayout.setHighlightPersistent(true);
        this.scrollColor = this.getResources().getColor(R.color.scroll_option_color);
        this.mObservableScrollView.setScrollListener((com.navdy.hud.app.view.ObservableScrollView$IScrollListener)this);
        java.util.ArrayList a = new java.util.ArrayList();
        ((java.util.List)a).add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(this.getContext().getString(R.string.back), 0));
        this.mChoiceLayout.setChoices(com.navdy.hud.app.ui.component.ChoiceLayout$Mode.LABEL, (java.util.List)a, 0, (com.navdy.hud.app.ui.component.ChoiceLayout$IListener)this);
        this.mMainImageView.setImageResource(R.drawable.icon_settings_learning_to_gesture);
        this.mMainTitleText.setText(R.string.gesture_tips_title);
        this.mNotificationScrollIndicator.setOrientation(com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation.VERTICAL);
        this.mNotificationScrollIndicator.setProperties(com.navdy.hud.app.framework.glance.GlanceConstants.scrollingRoundSize, com.navdy.hud.app.framework.glance.GlanceConstants.scrollingIndicatorProgressSize, 0, 0, com.navdy.hud.app.framework.glance.GlanceConstants.colorWhite, com.navdy.hud.app.framework.glance.GlanceConstants.colorWhite, true, com.navdy.hud.app.framework.glance.GlanceConstants.scrollingIndicatorPadding, -1, -1);
        this.mNotificationScrollIndicator.setItemCount(100);
        this.mNotificationIndicator.setOrientation(com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation.VERTICAL);
        this.mNotificationScrollIndicator.setBackgroundColor(this.scrollColor);
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        return false;
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        boolean b = false;
        switch(com.navdy.hud.app.view.ScrollableTextPresenterLayout$3.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[a.ordinal()]) {
            case 3: {
                this.mChoiceLayout.executeSelectedItem(true);
                b = true;
                break;
            }
            case 2: {
                this.handleRight();
                b = true;
                break;
            }
            case 1: {
                this.handleLeft();
                b = true;
                break;
            }
            default: {
                b = false;
            }
        }
        return b;
    }
    
    public void onPosChange(int i) {
        if (i > 0) {
            if (i > 100) {
                i = 100;
            }
        } else {
            i = 1;
        }
        if (this.mNotificationScrollIndicator.getCurrentItem() != i) {
            if (i != 1) {
            }
            this.mNotificationScrollIndicator.setCurrentItem(i);
        }
    }
    
    public void onScroll(int i, int i0, int i1, int i2) {
        if (this.mContentNeedsScrolling) {
            this.topScrub.setVisibility(0);
            this.mContentReachedTop = false;
            this.mContentReachedBottom = false;
            int i3 = this.mObservableScrollView.getChildAt(0).getBottom();
            int i4 = this.mObservableScrollView.getHeight();
            this.onPosChange((int)((double)i0 * 100.0 / (double)(i3 - i4)));
        }
    }
    
    public void onTop() {
        if (this.mContentNeedsScrolling) {
            this.topScrub.setVisibility(8);
            this.mContentReachedTop = true;
            this.mContentReachedBottom = false;
            this.onPosChange(1);
        }
    }
    
    public void setTextContents(CharSequence[] a) {
        this.mTextContents.clear();
        int i = a.length;
        Object[] a0 = a;
        int i0 = 0;
        while(i0 < i) {
            Object a1 = a0[i0];
            this.mTextContents.add(a1);
            i0 = i0 + 1;
        }
        this.mCount = this.mTextContents.size();
        this.mNotificationIndicator.setItemCount(this.mCount);
        this.setCurrentItem(0, false, false);
    }
    
    public void setVisibility(int i) {
        super.setVisibility(i);
        if (i == 0) {
            this.setCurrentItem(this.mCurrentItem, false, false);
        }
    }
}
