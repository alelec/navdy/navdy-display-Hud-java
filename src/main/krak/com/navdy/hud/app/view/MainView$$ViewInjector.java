package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class MainView$$ViewInjector {
    public MainView$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.view.MainView a0, Object a1) {
        a0.containerView = (com.navdy.hud.app.view.ContainerView)a.findRequiredView(a1, R.id.container, "field 'containerView'");
        a0.notificationView = (com.navdy.hud.app.view.NotificationView)a.findRequiredView(a1, R.id.notification, "field 'notificationView'");
        a0.notifIndicator = (com.navdy.hud.app.ui.component.carousel.CarouselIndicator)a.findRequiredView(a1, R.id.notifIndicator, "field 'notifIndicator'");
        a0.notifScrollIndicator = (com.navdy.hud.app.ui.component.carousel.ProgressIndicator)a.findRequiredView(a1, R.id.notifScrollIndicator, "field 'notifScrollIndicator'");
        a0.expandedNotificationView = (android.widget.FrameLayout)a.findRequiredView(a1, R.id.expandedNotifView, "field 'expandedNotificationView'");
        a0.notificationExtensionView = (android.widget.FrameLayout)a.findRequiredView(a1, R.id.notificationExtensionView, "field 'notificationExtensionView'");
        a0.mainLowerView = (com.navdy.hud.app.ui.component.main.MainLowerView)a.findRequiredView(a1, R.id.mainLowerView, "field 'mainLowerView'");
        a0.expandedNotificationCoverView = a.findRequiredView(a1, R.id.expandedNotifCoverView, "field 'expandedNotificationCoverView'");
        a0.splitterView = (android.widget.FrameLayout)a.findRequiredView(a1, R.id.splitter, "field 'splitterView'");
        a0.notificationColorView = (com.navdy.hud.app.ui.component.image.ColorImageView)a.findRequiredView(a1, R.id.notificationColorView, "field 'notificationColorView'");
        a0.screenContainer = (android.widget.FrameLayout)a.findRequiredView(a1, R.id.screenContainer, "field 'screenContainer'");
        a0.systemTray = (com.navdy.hud.app.ui.component.SystemTrayView)a.findRequiredView(a1, R.id.systemTray, "field 'systemTray'");
        a0.toastView = (com.navdy.hud.app.view.ToastView)a.findRequiredView(a1, R.id.toastView, "field 'toastView'");
    }
    
    public static void reset(com.navdy.hud.app.view.MainView a) {
        a.containerView = null;
        a.notificationView = null;
        a.notifIndicator = null;
        a.notifScrollIndicator = null;
        a.expandedNotificationView = null;
        a.notificationExtensionView = null;
        a.mainLowerView = null;
        a.expandedNotificationCoverView = null;
        a.splitterView = null;
        a.notificationColorView = null;
        a.screenContainer = null;
        a.systemTray = null;
        a.toastView = null;
    }
}
