package com.navdy.hud.app.view;

class GestureVideoCaptureView$UploadTimeoutRunnable implements Runnable {
    final private String sessionName;
    final com.navdy.hud.app.view.GestureVideoCaptureView this$0;
    
    GestureVideoCaptureView$UploadTimeoutRunnable(com.navdy.hud.app.view.GestureVideoCaptureView a, String s) {
        super();
        this.this$0 = a;
        this.sessionName = s;
    }
    
    public void run() {
        com.navdy.hud.app.view.GestureVideoCaptureView.sLogger.d("Upload has timed out.");
        if (this.sessionName.equals(com.navdy.hud.app.view.GestureVideoCaptureView.access$000(this.this$0))) {
            com.navdy.hud.app.view.GestureVideoCaptureView.access$100(this.this$0, com.navdy.hud.app.view.GestureVideoCaptureView$State.UPLOADING_FAILURE);
        }
    }
}
