package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class ContainerView extends android.widget.FrameLayout implements com.navdy.hud.app.util.CanShowScreen, com.navdy.hud.app.util.Pauseable {
    private com.navdy.hud.app.util.ScreenConductor screenMaestro;
    @Inject
    com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;
    
    public ContainerView(android.content.Context a, android.util.AttributeSet a0) {
        super(a, a0);
        if (!this.isInEditMode()) {
            mortar.Mortar.inject(a, this);
        }
    }
    
    public void createScreens() {
        this.screenMaestro.createScreens();
    }
    
    public android.view.View getCurrentView() {
        return this.screenMaestro.getChildView();
    }
    
    public android.animation.Animator getCustomContainerAnimator(com.navdy.hud.app.view.MainView$CustomAnimationMode a) {
        android.animation.Animator a0 = null;
        android.view.View a1 = this.getCurrentView();
        label2: {
            label0: {
                label1: {
                    if (a1 == null) {
                        break label1;
                    }
                    if (a1 instanceof com.navdy.hud.app.view.ICustomAnimator) {
                        break label0;
                    }
                }
                a0 = null;
                break label2;
            }
            a0 = ((com.navdy.hud.app.view.ICustomAnimator)a1).getCustomAnimator(a);
        }
        return a0;
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        android.view.ViewGroup a = (android.view.ViewGroup)this.findViewById(R.id.screenContainer);
        this.screenMaestro = new com.navdy.hud.app.util.ScreenConductor(this.getContext(), a, this.uiStateManager);
    }
    
    public void onPause() {
        android.view.View a = this.getCurrentView();
        if (a instanceof com.navdy.hud.app.util.Pauseable) {
            ((com.navdy.hud.app.util.Pauseable)a).onPause();
        }
    }
    
    public void onResume() {
        android.view.View a = this.getCurrentView();
        if (a instanceof com.navdy.hud.app.util.Pauseable) {
            ((com.navdy.hud.app.util.Pauseable)a).onResume();
        }
    }
    
    public void showScreen(mortar.Blueprint a, flow.Flow$Direction a0, int i, int i0) {
        this.screenMaestro.showScreen(a, a0, i, i0);
    }
}
