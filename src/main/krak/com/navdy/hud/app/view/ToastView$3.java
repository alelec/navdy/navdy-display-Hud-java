package com.navdy.hud.app.view;

class ToastView$3 implements Runnable {
    final com.navdy.hud.app.view.ToastView this$0;
    
    ToastView$3(com.navdy.hud.app.view.ToastView a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        com.navdy.hud.app.view.ToastView.access$000().v("toast:animationOut");
        if (com.navdy.hud.app.view.ToastView.access$300(this.this$0) == null) {
            com.navdy.hud.app.view.ToastView.access$302(this.this$0, com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().getRootScreen());
        }
        if (com.navdy.hud.app.view.ToastView.access$300(this.this$0) != null) {
            this.this$0.inputManager.setFocus((com.navdy.hud.app.manager.InputManager$IInputHandler)null);
            com.navdy.hud.app.view.ToastView.access$300(this.this$0).setInputFocus();
        }
        this.this$0.setVisibility(8);
        this.this$0.revertToOriginal();
        com.navdy.hud.app.framework.toast.ToastPresenter.clear();
        com.navdy.hud.app.framework.toast.ToastManager.getInstance().setToastDisplayFlag(false);
    }
}
