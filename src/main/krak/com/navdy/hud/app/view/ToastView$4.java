package com.navdy.hud.app.view;

class ToastView$4 implements Runnable {
    final com.navdy.hud.app.view.ToastView this$0;
    
    ToastView$4(com.navdy.hud.app.view.ToastView a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        String s = com.navdy.hud.app.framework.toast.ToastPresenter.getScreenName();
        label0: {
            Throwable a = null;
            if (s == null) {
                break label0;
            }
            try {
                com.navdy.hud.app.view.ToastView.access$000().v(new StringBuilder().append("out dismiss screen:").append(s).toString());
                com.navdy.hud.app.view.ToastView.access$100(this.this$0).post(new com.navdy.service.library.events.ui.ShowScreen$Builder().screen(com.navdy.service.library.events.ui.Screen.valueOf(s)).build());
                com.navdy.hud.app.framework.toast.ToastPresenter.clearScreenName();
                break label0;
            } catch(Throwable a0) {
                a = a0;
            }
            com.navdy.hud.app.view.ToastView.access$000().e(a);
        }
    }
}
