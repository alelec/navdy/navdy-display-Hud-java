package com.navdy.hud.app.view;

class ChoicesView$2 implements android.view.View$OnClickListener {
    final com.navdy.hud.app.view.ChoicesView this$0;
    
    ChoicesView$2(com.navdy.hud.app.view.ChoicesView a) {
        super();
        this.this$0 = a;
    }
    
    public void onClick(android.view.View a) {
        android.graphics.Rect a0 = new android.graphics.Rect();
        int[] a1 = new int[2];
        int[] a2 = new int[2];
        this.this$0.getLocationInWindow(a1);
        int i = 0;
        while(true) {
            if (i < this.this$0.getItemCount()) {
                android.view.View a3 = this.this$0.getItemAt(i);
                ((android.view.View)a3.getParent()).getLocationInWindow(a2);
                int i0 = this.this$0.lastTouchX;
                int i1 = a2[0];
                int i2 = a1[0];
                int i3 = this.this$0.lastTouchY;
                int i4 = a2[1];
                int i5 = a1[1];
                a3.getHitRect(a0);
                if (!a0.contains(i0 - (i1 - i2), i3 - (i4 - i5))) {
                    i = i + 1;
                    continue;
                }
                if (i != this.this$0.getSelectedItem()) {
                    this.this$0.setSelectedItem(i);
                } else {
                    this.this$0.executeSelectedItem(this.this$0.animateItemOnExecution);
                }
            }
            return;
        }
    }
}
