package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class DashboardWidgetView extends android.widget.RelativeLayout {
    @InjectView(R.id.custom_drawable)
    @Optional
    protected android.view.View mCustomView;
    private int mLayout;
    
    public DashboardWidgetView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public DashboardWidgetView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public DashboardWidgetView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        android.content.res.TypedArray a1 = a.obtainStyledAttributes(a0, com.navdy.hud.app.R$styleable.GaugeView, i, 0);
        if (a1 != null) {
            this.setContentView(a1.getResourceId(1, R.layout.small_gauge_view));
            butterknife.ButterKnife.inject((android.view.View)this);
            a1.recycle();
        }
    }
    
    public void clear() {
        if (this.mCustomView != null) {
            this.mCustomView.setBackground((android.graphics.drawable.Drawable)null);
        }
    }
    
    public android.view.View getCustomView() {
        return this.mCustomView;
    }
    
    public boolean setContentView(int i) {
        boolean b = false;
        label2: {
            label0: {
                label1: {
                    if (i <= 0) {
                        break label1;
                    }
                    if (i != this.mLayout) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            this.mLayout = i;
            this.removeAllViews();
            android.view.LayoutInflater.from(this.getContext()).inflate(i, (android.view.ViewGroup)this);
            butterknife.ButterKnife.inject((android.view.View)this);
            b = true;
        }
        return b;
    }
    
    public boolean setContentView(android.view.View a) {
        boolean b = false;
        this.mLayout = -1;
        this.removeAllViews();
        if (a == null) {
            b = false;
        } else {
            this.addView(a);
            b = true;
        }
        return b;
    }
}
