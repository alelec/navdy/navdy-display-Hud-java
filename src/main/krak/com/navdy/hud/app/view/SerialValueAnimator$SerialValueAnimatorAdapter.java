package com.navdy.hud.app.view;

abstract interface SerialValueAnimator$SerialValueAnimatorAdapter {
    abstract public void animationComplete(float arg);
    
    
    abstract public float getValue();
    
    
    abstract public void setValue(float arg);
}
