package com.navdy.hud.app.view;

public class GaugeView extends com.navdy.hud.app.view.DashboardWidgetView {
    @InjectView(R.id.txt_unit)
    @Optional
    android.widget.TextView mTvUnit;
    @InjectView(R.id.txt_value)
    android.widget.TextView mTvValue;
    
    public GaugeView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public GaugeView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public GaugeView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        android.content.res.TypedArray a1 = a.obtainStyledAttributes(a0, com.navdy.hud.app.R$styleable.GaugeView, i, 0);
        if (a1 != null) {
            this.setValueTextSize(a1.getDimensionPixelSize(0, 0));
            a1.recycle();
        }
    }
    
    public void clear() {
        super.clear();
        this.mTvUnit.setText((CharSequence)null);
        this.mTvValue.setText((CharSequence)null);
    }
    
    public android.widget.TextView getUnitTextView() {
        return this.mTvUnit;
    }
    
    public android.widget.TextView getUnitView() {
        return this.mTvUnit;
    }
    
    public android.widget.TextView getValueTextView() {
        return this.mTvValue;
    }
    
    public android.widget.TextView getValueView() {
        return this.mTvValue;
    }
    
    protected void onMeasure(int i, int i0) {
        super.onMeasure(i, i0);
    }
    
    public void setUnitText(CharSequence a) {
        if (this.mTvUnit != null) {
            this.mTvUnit.setText(a);
        }
    }
    
    public void setValueText(CharSequence a) {
        if (this.mTvValue != null) {
            this.mTvValue.setText(a);
        }
    }
    
    public void setValueTextSize(int i) {
        this.mTvValue.setTextSize(0, (float)i);
    }
}
