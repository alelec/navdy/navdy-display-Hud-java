package com.navdy.hud.app.view;

abstract public interface ObservableScrollView$IScrollListener {
    abstract public void onBottom();
    
    
    abstract public void onScroll(int arg, int arg0, int arg1, int arg2);
    
    
    abstract public void onTop();
}
