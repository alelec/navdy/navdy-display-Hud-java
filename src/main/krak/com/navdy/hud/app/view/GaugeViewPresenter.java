package com.navdy.hud.app.view;

abstract public class GaugeViewPresenter extends com.navdy.hud.app.view.DashboardWidgetPresenter {
    protected com.navdy.hud.app.view.GaugeView mGaugeView;
    
    public GaugeViewPresenter() {
    }
    
    public void setView(com.navdy.hud.app.view.DashboardWidgetView a) {
        super.setView(a);
        if (a != null) {
            if (!(a instanceof com.navdy.hud.app.view.GaugeView)) {
                throw new IllegalArgumentException("The view has to be of type GaugeView");
            }
            this.mGaugeView = (com.navdy.hud.app.view.GaugeView)a;
        } else {
            this.mGaugeView = null;
        }
    }
}
