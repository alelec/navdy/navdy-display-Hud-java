package com.navdy.hud.app.view;

class SerialValueAnimator$1 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
    final com.navdy.hud.app.view.SerialValueAnimator this$0;
    
    SerialValueAnimator$1(com.navdy.hud.app.view.SerialValueAnimator a) {
        super();
        this.this$0 = a;
    }
    
    public void onAnimationEnd(android.animation.Animator a) {
        if (a instanceof android.animation.ValueAnimator) {
            android.animation.ValueAnimator a0 = (android.animation.ValueAnimator)a;
            com.navdy.hud.app.view.SerialValueAnimator.access$000(this.this$0).animationComplete(((Float)a0.getAnimatedValue()).floatValue());
        }
        if (com.navdy.hud.app.view.SerialValueAnimator.access$100(this.this$0).size() != 0) {
            float f = ((Float)com.navdy.hud.app.view.SerialValueAnimator.access$100(this.this$0).remove()).floatValue();
            com.navdy.hud.app.view.SerialValueAnimator.access$400(this.this$0, f);
        } else {
            com.navdy.hud.app.view.SerialValueAnimator.access$202(this.this$0, false);
            com.navdy.hud.app.view.SerialValueAnimator.access$302(this.this$0, (android.animation.ValueAnimator)null);
        }
    }
}
