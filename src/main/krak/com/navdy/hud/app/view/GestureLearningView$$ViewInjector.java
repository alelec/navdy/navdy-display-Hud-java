package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class GestureLearningView$$ViewInjector {
    public GestureLearningView$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.view.GestureLearningView a0, Object a1) {
        a0.mChoiceLayout = (com.navdy.hud.app.ui.component.ChoiceLayout)a.findRequiredView(a1, R.id.choiceLayout, "field 'mChoiceLayout'");
        a0.mTipsScroller = (android.widget.LinearLayout)a.findRequiredView(a1, R.id.tips_scroller, "field 'mTipsScroller'");
        a0.mTipsTextView1 = (android.widget.TextView)a.findRequiredView(a1, R.id.tipsText1, "field 'mTipsTextView1'");
        a0.mTipsTextView2 = (android.widget.TextView)a.findRequiredView(a1, R.id.tipsText2, "field 'mTipsTextView2'");
        a0.mCenterImage = (android.widget.ImageView)a.findRequiredView(a1, R.id.center_image, "field 'mCenterImage'");
        a0.mGestureProgressIndicator = a.findRequiredView(a1, R.id.gesture_progress_indicator, "field 'mGestureProgressIndicator'");
        a0.mLeftSwipeLayout = (android.widget.LinearLayout)a.findRequiredView(a1, R.id.lyt_left_swipe, "field 'mLeftSwipeLayout'");
        a0.mRightSwipeLayout = (android.widget.LinearLayout)a.findRequiredView(a1, R.id.lyt_right_swipe, "field 'mRightSwipeLayout'");
    }
    
    public static void reset(com.navdy.hud.app.view.GestureLearningView a) {
        a.mChoiceLayout = null;
        a.mTipsScroller = null;
        a.mTipsTextView1 = null;
        a.mTipsTextView2 = null;
        a.mCenterImage = null;
        a.mGestureProgressIndicator = null;
        a.mLeftSwipeLayout = null;
        a.mRightSwipeLayout = null;
    }
}
