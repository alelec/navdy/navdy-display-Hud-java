package com.navdy.hud.app.view.drawable;

public class CustomDrawable extends android.graphics.drawable.Drawable {
    protected android.graphics.Paint mPaint;
    
    public CustomDrawable() {
        this.mPaint = new android.graphics.Paint();
    }
    
    public void draw(android.graphics.Canvas a) {
    }
    
    public int getOpacity() {
        return -3;
    }
    
    public void setAlpha(int i) {
        this.mPaint.setAlpha(i);
    }
    
    public void setColorFilter(android.graphics.ColorFilter a) {
        this.mPaint.setColorFilter(a);
    }
}
