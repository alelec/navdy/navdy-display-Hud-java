package com.navdy.hud.app.view;

public class TemperatureWarningView$$ViewInjector {
    public TemperatureWarningView$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.view.TemperatureWarningView a0, Object a1) {
        a0.mScreenTitleText = (android.widget.TextView)a.findRequiredView(a1, R.id.mainTitle, "field 'mScreenTitleText'");
        a0.mTitle1 = (android.widget.TextView)a.findRequiredView(a1, R.id.title1, "field 'mTitle1'");
        a0.mMainTitleText = (android.widget.TextView)a.findRequiredView(a1, R.id.title2, "field 'mMainTitleText'");
        a0.mWarningMessage = (android.widget.TextView)a.findRequiredView(a1, R.id.title3, "field 'mWarningMessage'");
        a0.mChoiceLayout = (com.navdy.hud.app.ui.component.ChoiceLayout)a.findRequiredView(a1, R.id.choiceLayout, "field 'mChoiceLayout'");
        a0.mRightSwipe = (android.widget.ImageView)a.findRequiredView(a1, R.id.rightSwipe, "field 'mRightSwipe'");
        a0.mIcon = (android.widget.ImageView)a.findRequiredView(a1, R.id.image, "field 'mIcon'");
    }
    
    public static void reset(com.navdy.hud.app.view.TemperatureWarningView a) {
        a.mScreenTitleText = null;
        a.mTitle1 = null;
        a.mMainTitleText = null;
        a.mWarningMessage = null;
        a.mChoiceLayout = null;
        a.mRightSwipe = null;
        a.mIcon = null;
    }
}
