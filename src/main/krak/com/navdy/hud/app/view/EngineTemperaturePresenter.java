package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

final public class EngineTemperaturePresenter extends com.navdy.hud.app.view.DashboardWidgetPresenter {
    final public static com.navdy.hud.app.view.EngineTemperaturePresenter$Companion Companion;
    final private static double TEMPERATURE_GAUGE_COLD_THRESHOLD = 45.7;
    final private static double TEMPERATURE_GAUGE_HOT_THRESHOLD = 105.0;
    final private static double TEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS = 38.0;
    final private static double TEMPERATURE_GAUGE_MID_POINT_CELSIUS = 76.5;
    final private static double TEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS = 115.0;
    private int STATE_COLD;
    private int STATE_HOT;
    private int STATE_NORMAL;
    public com.squareup.otto.Bus bus;
    private String celsiusLabel;
    final private android.content.Context context;
    public com.navdy.hud.app.profile.DriverProfileManager driverProfileManager;
    private double engineTemperature;
    private com.navdy.hud.app.view.drawable.EngineTemperatureDrawable engineTemperatureDrawable;
    private String fahrenheitLabel;
    private boolean mLeftOriented;
    private boolean registered;
    public android.widget.TextView tempText;
    public android.widget.TextView tempUnitText;
    final private String widgetNameString;
    
    static {
        Companion = new com.navdy.hud.app.view.EngineTemperaturePresenter$Companion((kotlin.jvm.internal.DefaultConstructorMarker)null);
        TEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS = 38.0;
        TEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS = 115.0;
        TEMPERATURE_GAUGE_MID_POINT_CELSIUS = 76.5;
        TEMPERATURE_GAUGE_COLD_THRESHOLD = 45.7;
        TEMPERATURE_GAUGE_HOT_THRESHOLD = 105.0;
    }
    
    public EngineTemperaturePresenter(android.content.Context a) {
        super();
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "context");
        this.context = a;
        this.STATE_COLD = 1;
        this.STATE_HOT = 2;
        this.mLeftOriented = true;
        this.fahrenheitLabel = "";
        this.celsiusLabel = "";
        this.engineTemperatureDrawable = new com.navdy.hud.app.view.drawable.EngineTemperatureDrawable(this.context, R.array.smart_dash_engine_temperature_state_colors);
        com.navdy.hud.app.view.drawable.EngineTemperatureDrawable a0 = this.engineTemperatureDrawable;
        if (a0 != null) {
            a0.setMinValue((float)Companion.getTEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS());
        }
        com.navdy.hud.app.view.drawable.EngineTemperatureDrawable a1 = this.engineTemperatureDrawable;
        if (a1 != null) {
            a1.setMaxGaugeValue((float)Companion.getTEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS());
        }
        android.content.res.Resources a2 = this.context.getResources();
        String s = a2.getString(R.string.temperature_unit_fahrenheit);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(s, "resources.getString(R.st\u2026perature_unit_fahrenheit)");
        this.fahrenheitLabel = s;
        String s0 = a2.getString(R.string.temperature_unit_celsius);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(s0, "resources.getString(R.st\u2026temperature_unit_celsius)");
        this.celsiusLabel = s0;
        mortar.Mortar.inject(com.navdy.hud.app.HudApplication.getAppContext(), this);
        String s1 = a2.getString(R.string.widget_engine_temperature);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(s1, "resources.getString(R.st\u2026idget_engine_temperature)");
        this.widgetNameString = s1;
    }
    
    final public static double access$getTEMPERATURE_GAUGE_COLD_THRESHOLD$cp() {
        return TEMPERATURE_GAUGE_COLD_THRESHOLD;
    }
    
    final public static double access$getTEMPERATURE_GAUGE_HOT_THRESHOLD$cp() {
        return TEMPERATURE_GAUGE_HOT_THRESHOLD;
    }
    
    final public static double access$getTEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS$cp() {
        return TEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS;
    }
    
    final public static double access$getTEMPERATURE_GAUGE_MID_POINT_CELSIUS$cp() {
        return TEMPERATURE_GAUGE_MID_POINT_CELSIUS;
    }
    
    final public static double access$getTEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS$cp() {
        return TEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS;
    }
    
    final public com.squareup.otto.Bus getBus() {
        com.squareup.otto.Bus a = this.bus;
        if (a == null) {
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("bus");
        }
        return a;
    }
    
    final public String getCelsiusLabel() {
        return this.celsiusLabel;
    }
    
    final public android.content.Context getContext() {
        return this.context;
    }
    
    public android.graphics.drawable.Drawable getDrawable() {
        return this.engineTemperatureDrawable;
    }
    
    final public com.navdy.hud.app.profile.DriverProfileManager getDriverProfileManager() {
        com.navdy.hud.app.profile.DriverProfileManager a = this.driverProfileManager;
        if (a == null) {
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("driverProfileManager");
        }
        return a;
    }
    
    final public double getEngineTemperature() {
        return this.engineTemperature;
    }
    
    final public com.navdy.hud.app.view.drawable.EngineTemperatureDrawable getEngineTemperatureDrawable() {
        return this.engineTemperatureDrawable;
    }
    
    final public String getFahrenheitLabel() {
        return this.fahrenheitLabel;
    }
    
    final public int getSTATE_COLD() {
        return this.STATE_COLD;
    }
    
    final public int getSTATE_HOT() {
        return this.STATE_HOT;
    }
    
    final public int getSTATE_NORMAL() {
        return this.STATE_NORMAL;
    }
    
    final public android.widget.TextView getTempText() {
        android.widget.TextView a = this.tempText;
        if (a == null) {
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("tempText");
        }
        return a;
    }
    
    final public android.widget.TextView getTempUnitText() {
        android.widget.TextView a = this.tempUnitText;
        if (a == null) {
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("tempUnitText");
        }
        return a;
    }
    
    public String getWidgetIdentifier() {
        return "ENGINE_TEMPERATURE_GAUGE_ID";
    }
    
    public String getWidgetName() {
        return this.widgetNameString;
    }
    
    final public String getWidgetNameString() {
        return this.widgetNameString;
    }
    
    protected boolean isRegisteringToBusRequired() {
        return true;
    }
    
    final public void onSpeedUnitChanged(com.navdy.hud.app.manager.SpeedManager$SpeedUnitChanged a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "speedUnitChangedEvent");
        this.reDraw();
    }
    
    final public void setBus(com.squareup.otto.Bus a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "<set-?>");
        this.bus = a;
    }
    
    final public void setCelsiusLabel(String s) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(s, "<set-?>");
        this.celsiusLabel = s;
    }
    
    final public void setDriverProfileManager(com.navdy.hud.app.profile.DriverProfileManager a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "<set-?>");
        this.driverProfileManager = a;
    }
    
    final public void setEngineTemperature(double d) {
        this.engineTemperature = d;
        this.reDraw();
    }
    
    final public void setEngineTemperatureDrawable(com.navdy.hud.app.view.drawable.EngineTemperatureDrawable a) {
        this.engineTemperatureDrawable = a;
    }
    
    final public void setFahrenheitLabel(String s) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(s, "<set-?>");
        this.fahrenheitLabel = s;
    }
    
    final public void setSTATE_COLD(int i) {
        this.STATE_COLD = i;
    }
    
    final public void setSTATE_HOT(int i) {
        this.STATE_HOT = i;
    }
    
    final public void setSTATE_NORMAL(int i) {
        this.STATE_NORMAL = i;
    }
    
    final public void setTempText(android.widget.TextView a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "<set-?>");
        this.tempText = a;
    }
    
    final public void setTempUnitText(android.widget.TextView a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "<set-?>");
        this.tempUnitText = a;
    }
    
    public void setView(com.navdy.hud.app.view.DashboardWidgetView a, android.os.Bundle a0) {
        int i = 0;
        if (a0 == null) {
            i = R.layout.engine_temp_gauge_left;
        } else {
            switch(a0.getInt("EXTRA_GRAVITY")) {
                case 2: {
                    this.mLeftOriented = false;
                    i = R.layout.engine_temp_gauge_right;
                    break;
                }
                case 0: {
                    this.mLeftOriented = true;
                    i = R.layout.engine_temp_gauge_left;
                    break;
                }
                default: {
                    i = R.layout.engine_temp_gauge_left;
                    break;
                }
                case 1: {
                    i = R.layout.engine_temp_gauge_left;
                }
            }
        }
        if (a != null) {
            a.setContentView(i);
            android.view.View a1 = a.findViewById(R.id.txt_value);
            if (a1 == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type android.widget.TextView");
            }
            this.tempText = (android.widget.TextView)a1;
            android.view.View a2 = a.findViewById(R.id.txt_unit);
            if (a2 == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type android.widget.TextView");
            }
            this.tempUnitText = (android.widget.TextView)a2;
        }
        super.setView(a, a0);
        this.reDraw();
    }
    
    protected void updateGauge() {
        if (this.mWidgetView != null) {
            com.navdy.service.library.events.preferences.DriverProfilePreferences$UnitSystem a = null;
            com.navdy.hud.app.profile.DriverProfileManager a0 = this.driverProfileManager;
            if (a0 == null) {
                kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("driverProfileManager");
            }
            label0: {
                label1: {
                    if (a0 == null) {
                        break label1;
                    }
                    com.navdy.hud.app.profile.DriverProfile a1 = a0.getCurrentProfile();
                    if (a1 == null) {
                        break label1;
                    }
                    a = a1.getUnitSystem();
                    if (a != null) {
                        break label0;
                    }
                }
                a = com.navdy.service.library.events.preferences.DriverProfilePreferences$UnitSystem.UNIT_SYSTEM_METRIC;
            }
            com.navdy.hud.app.view.drawable.EngineTemperatureDrawable a2 = this.engineTemperatureDrawable;
            if (a2 != null) {
                a2.setGaugeValue((float)com.navdy.hud.app.ExtensionsKt.clamp(this.engineTemperature, Companion.getTEMPERATURE_GAUGE_COLD_THRESHOLD(), Companion.getTEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS()));
            }
            com.navdy.hud.app.view.drawable.EngineTemperatureDrawable a3 = this.engineTemperatureDrawable;
            if (a3 != null) {
                a3.setLeftOriented(this.mLeftOriented);
            }
            int i = this.STATE_NORMAL;
            if (this.engineTemperature <= Companion.getTEMPERATURE_GAUGE_COLD_THRESHOLD()) {
                i = this.STATE_COLD;
            } else if (this.engineTemperature > Companion.getTEMPERATURE_GAUGE_HOT_THRESHOLD()) {
                i = this.STATE_HOT;
            }
            com.navdy.hud.app.view.drawable.EngineTemperatureDrawable a4 = this.engineTemperatureDrawable;
            if (a4 != null) {
                a4.setState(i);
            }
            android.widget.TextView a5 = this.tempText;
            if (a5 == null) {
                kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("tempText");
            }
            if (a5 != null) {
                String s = null;
                switch(com.navdy.hud.app.view.EngineTemperaturePresenter$WhenMappings.$EnumSwitchMapping$0[a.ordinal()]) {
                    case 2: {
                        s = String.valueOf((int)com.navdy.hud.app.ExtensionsKt.celsiusToFahrenheit(this.engineTemperature));
                        break;
                    }
                    case 1: {
                        s = String.valueOf((int)this.engineTemperature);
                        break;
                    }
                    default: {
                        throw new kotlin.NoWhenBranchMatchedException();
                    }
                }
                a5.setText((CharSequence)s);
            }
            android.widget.TextView a6 = this.tempUnitText;
            if (a6 == null) {
                kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("tempUnitText");
            }
            if (a6 != null) {
                String s0 = null;
                switch(com.navdy.hud.app.view.EngineTemperaturePresenter$WhenMappings.$EnumSwitchMapping$1[a.ordinal()]) {
                    case 2: {
                        s0 = this.fahrenheitLabel;
                        break;
                    }
                    case 1: {
                        s0 = this.celsiusLabel;
                        break;
                    }
                    default: {
                        throw new kotlin.NoWhenBranchMatchedException();
                    }
                }
                a6.setText((CharSequence)s0);
            }
        }
    }
}
