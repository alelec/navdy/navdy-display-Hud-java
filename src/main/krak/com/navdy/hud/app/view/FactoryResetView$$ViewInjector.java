package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class FactoryResetView$$ViewInjector {
    public FactoryResetView$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.view.FactoryResetView a0, Object a1) {
        a0.factoryResetConfirmation = (com.navdy.hud.app.ui.component.ConfirmationLayout)a.findRequiredView(a1, R.id.factory_reset_confirmation, "field 'factoryResetConfirmation'");
    }
    
    public static void reset(com.navdy.hud.app.view.FactoryResetView a) {
        a.factoryResetConfirmation = null;
    }
}
