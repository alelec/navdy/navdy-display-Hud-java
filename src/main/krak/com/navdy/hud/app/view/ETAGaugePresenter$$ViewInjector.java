package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class ETAGaugePresenter$$ViewInjector {
    public ETAGaugePresenter$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.view.ETAGaugePresenter a0, Object a1) {
        a0.ttaText1 = (android.widget.TextView)a.findRequiredView(a1, R.id.txt_tta1, "field 'ttaText1'");
        a0.ttaText2 = (android.widget.TextView)a.findRequiredView(a1, R.id.txt_tta2, "field 'ttaText2'");
        a0.ttaText3 = (android.widget.TextView)a.findRequiredView(a1, R.id.txt_tta3, "field 'ttaText3'");
        a0.ttaText4 = (android.widget.TextView)a.findRequiredView(a1, R.id.txt_tta4, "field 'ttaText4'");
        a0.remainingDistanceText = (android.widget.TextView)a.findRequiredView(a1, R.id.txt_remaining_distance, "field 'remainingDistanceText'");
        a0.remainingDistanceUnitText = (android.widget.TextView)a.findRequiredView(a1, R.id.txt_remaining_distance_unit, "field 'remainingDistanceUnitText'");
        a0.etaText = (android.widget.TextView)a.findRequiredView(a1, R.id.txt_eta, "field 'etaText'");
        a0.etaAmPmText = (android.widget.TextView)a.findRequiredView(a1, R.id.txt_eta_am_pm, "field 'etaAmPmText'");
        a0.etaView = (android.view.ViewGroup)a.findRequiredView(a1, R.id.eta_view, "field 'etaView'");
        a0.tripOpenMapView = (android.support.constraint.ConstraintLayout)a.findRequiredView(a1, R.id.tripOpenMap, "field 'tripOpenMapView'");
        a0.tripDistanceView = (android.widget.TextView)a.findRequiredView(a1, R.id.tripDistance, "field 'tripDistanceView'");
        a0.tripTimeView = (android.widget.TextView)a.findRequiredView(a1, R.id.tripTime, "field 'tripTimeView'");
        a0.tripIconView = (android.widget.ImageView)a.findRequiredView(a1, R.id.tripIcon, "field 'tripIconView'");
    }
    
    public static void reset(com.navdy.hud.app.view.ETAGaugePresenter a) {
        a.ttaText1 = null;
        a.ttaText2 = null;
        a.ttaText3 = null;
        a.ttaText4 = null;
        a.remainingDistanceText = null;
        a.remainingDistanceUnitText = null;
        a.etaText = null;
        a.etaAmPmText = null;
        a.etaView = null;
        a.tripOpenMapView = null;
        a.tripDistanceView = null;
        a.tripTimeView = null;
        a.tripIconView = null;
    }
}
