package com.navdy.hud.app.view.drawable;
import com.navdy.hud.app.R;

public class ETAProgressDrawable extends com.navdy.hud.app.view.drawable.GaugeDrawable {
    private int backgroundColor;
    private int foreGroundColor;
    private android.graphics.drawable.Drawable progressDrawable;
    private int verticalMargin;
    
    public ETAProgressDrawable(android.content.Context a) {
        super(a, 0);
        android.content.res.Resources a0 = a.getResources();
        this.backgroundColor = a0.getColor(R.color.cyan);
        this.foreGroundColor = a0.getColor(R.color.grey_4a);
        this.progressDrawable = a0.getDrawable(R.drawable.trip_progress_point_indicator);
        this.verticalMargin = a0.getDimensionPixelSize(R.dimen.eta_progress_vertical_margin);
    }
    
    public void draw(android.graphics.Canvas a) {
        super.draw(a);
        android.graphics.Rect a0 = this.getBounds();
        float f = (float)a0.height();
        float f0 = (float)a0.width();
        android.graphics.Rect a1 = new android.graphics.Rect(a0);
        a1.inset(0, this.verticalMargin);
        this.mPaint.setColor(this.backgroundColor);
        this.mPaint.setStyle(android.graphics.Paint$Style.FILL);
        a.drawRect((float)a1.left, (float)a1.top, (float)a1.right, (float)a1.bottom, this.mPaint);
        int i = (int)(f0 * (this.mValue / (this.mMaxValue - this.mMinValue)));
        this.mPaint.setColor(this.foreGroundColor);
        a.drawRect((float)a1.left, (float)a1.top, (float)(a1.left + i), (float)a1.bottom, this.mPaint);
        int i0 = (int)Math.min((float)(a1.left + i), f0 - f);
        int i1 = (int)((float)i0 + f);
        this.progressDrawable.setBounds(i0, a0.top, i1, a0.bottom);
        this.progressDrawable.draw(a);
    }
}
