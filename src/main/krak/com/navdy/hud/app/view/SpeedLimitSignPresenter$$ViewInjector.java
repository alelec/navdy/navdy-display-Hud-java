package com.navdy.hud.app.view;

public class SpeedLimitSignPresenter$$ViewInjector {
    public SpeedLimitSignPresenter$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.view.SpeedLimitSignPresenter a0, Object a1) {
        a0.speedLimitSignView = (com.navdy.hud.app.view.SpeedLimitSignView)a.findRequiredView(a1, R.id.speed_limit_sign, "field 'speedLimitSignView'");
        a0.speedLimitUnavailableText = (android.widget.TextView)a.findRequiredView(a1, R.id.txt_speed_limit_unavailable, "field 'speedLimitUnavailableText'");
    }
    
    public static void reset(com.navdy.hud.app.view.SpeedLimitSignPresenter a) {
        a.speedLimitSignView = null;
        a.speedLimitUnavailableText = null;
    }
}
