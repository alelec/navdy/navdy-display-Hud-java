package com.navdy.hud.app.view;

public class HTMLTextView extends android.widget.TextView {
    public HTMLTextView(android.content.Context a) {
        super(a);
    }
    
    public HTMLTextView(android.content.Context a, android.util.AttributeSet a0) {
        super(a, a0);
    }
    
    public HTMLTextView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
    }
    
    public void setText(CharSequence a, android.widget.TextView$BufferType a0) {
        super.setText((CharSequence)android.text.Html.fromHtml(a.toString()), a0);
    }
}
