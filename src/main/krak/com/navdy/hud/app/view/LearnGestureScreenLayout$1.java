package com.navdy.hud.app.view;

class LearnGestureScreenLayout$1 {
    final static int[] $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent;
    final static int[] $SwitchMap$com$navdy$hud$app$view$LearnGestureScreenLayout$Mode;
    
    static {
        $SwitchMap$com$navdy$hud$app$view$LearnGestureScreenLayout$Mode = new int[com.navdy.hud.app.view.LearnGestureScreenLayout$Mode.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$view$LearnGestureScreenLayout$Mode;
        com.navdy.hud.app.view.LearnGestureScreenLayout$Mode a0 = com.navdy.hud.app.view.LearnGestureScreenLayout$Mode.GESTURE;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$view$LearnGestureScreenLayout$Mode[com.navdy.hud.app.view.LearnGestureScreenLayout$Mode.TIPS.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$view$LearnGestureScreenLayout$Mode[com.navdy.hud.app.view.LearnGestureScreenLayout$Mode.CAPTURE.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent = new int[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.values().length];
        int[] a1 = $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent;
        com.navdy.hud.app.manager.InputManager$CustomKeyEvent a2 = com.navdy.hud.app.manager.InputManager$CustomKeyEvent.LEFT;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.RIGHT.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException3) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.SELECT.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException4) {
        }
    }
}
