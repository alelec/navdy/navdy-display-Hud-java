package com.navdy.hud.app.view;

class ShutDownConfirmationView$7 {
    final static int[] $SwitchMap$com$navdy$hud$app$event$Shutdown$Reason;
    final static int[] $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent;
    final static int[] $SwitchMap$com$navdy$service$library$events$input$Gesture;
    
    static {
        $SwitchMap$com$navdy$hud$app$event$Shutdown$Reason = new int[com.navdy.hud.app.event.Shutdown$Reason.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$event$Shutdown$Reason;
        com.navdy.hud.app.event.Shutdown$Reason a0 = com.navdy.hud.app.event.Shutdown$Reason.ENGINE_OFF;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$event$Shutdown$Reason[com.navdy.hud.app.event.Shutdown$Reason.ACCELERATE_SHUTDOWN.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$event$Shutdown$Reason[com.navdy.hud.app.event.Shutdown$Reason.INACTIVITY.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent = new int[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.values().length];
        int[] a1 = $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent;
        com.navdy.hud.app.manager.InputManager$CustomKeyEvent a2 = com.navdy.hud.app.manager.InputManager$CustomKeyEvent.LEFT;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.RIGHT.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException3) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.SELECT.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException4) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.POWER_BUTTON_DOUBLE_CLICK.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException5) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.POWER_BUTTON_CLICK.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException6) {
        }
        $SwitchMap$com$navdy$service$library$events$input$Gesture = new int[com.navdy.service.library.events.input.Gesture.values().length];
        int[] a3 = $SwitchMap$com$navdy$service$library$events$input$Gesture;
        com.navdy.service.library.events.input.Gesture a4 = com.navdy.service.library.events.input.Gesture.GESTURE_SWIPE_LEFT;
        try {
            a3[a4.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException7) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$input$Gesture[com.navdy.service.library.events.input.Gesture.GESTURE_SWIPE_RIGHT.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException8) {
        }
    }
}
