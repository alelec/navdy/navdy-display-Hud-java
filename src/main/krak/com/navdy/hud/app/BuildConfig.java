package com.navdy.hud.app;

final public class BuildConfig {
    final public static String APPLICATION_ID = "com.navdy.hud.app";
    final public static String BUILD_TYPE = "release";
    final public static boolean DEBUG = true;
    final public static String FLAVOR = "hud";
    final public static int VERSION_CODE = 3049;
    final public static String VERSION_NAME = "1.3.3051-corona";
    
    public BuildConfig() {
    }
}
