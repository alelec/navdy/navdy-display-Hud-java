package com.navdy.hud.app.obd;

public class CarDetails {
    final public static com.navdy.service.library.log.Logger sLogger;
    String aaia;
    String engine;
    String engineType;
    String make;
    String model;
    String vin;
    String year;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.obd.CarDetails.class);
    }
    
    public CarDetails() {
    }
    
    public static com.navdy.hud.app.obd.CarDetails fromJson(String s) {
        com.navdy.hud.app.obd.CarDetails a = null;
        try {
            a = (com.navdy.hud.app.obd.CarDetails)new com.google.gson.Gson().fromJson(s.toString(), com.navdy.hud.app.obd.CarDetails.class);
        } catch(com.google.gson.JsonSyntaxException a0) {
            sLogger.e("Exception while parsing the car details response from CarMD ", (Throwable)a0);
            a = null;
        }
        return a;
    }
    
    public static boolean matches(com.navdy.hud.app.obd.CarDetails a, String s) {
        boolean b = false;
        boolean b0 = com.navdy.hud.app.obd.ObdDeviceConfigurationManager.isValidVin(s);
        label2: {
            label0: {
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (a == null) {
                        break label1;
                    }
                    if (android.text.TextUtils.equals((CharSequence)a.vin, (CharSequence)s)) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
}
