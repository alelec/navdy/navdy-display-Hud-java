package com.navdy.hud.app.obd;

class CarMDVinDecoder$1 implements Runnable {
    final com.navdy.hud.app.obd.CarMDVinDecoder this$0;
    final String val$vinNumber;
    
    CarMDVinDecoder$1(com.navdy.hud.app.obd.CarMDVinDecoder a, String s) {
        super();
        this.this$0 = a;
        this.val$vinNumber = s;
    }
    
    public void run() {
        label2: try {
            String s = com.navdy.hud.app.obd.CarMDVinDecoder.access$000(this.this$0, this.val$vinNumber);
            boolean b = android.text.TextUtils.isEmpty((CharSequence)s);
            label1: {
                if (b) {
                    break label1;
                }
                com.navdy.hud.app.obd.CarMDVinDecoder.access$100().d(new StringBuilder().append("Entry found in the cache for the vinNumber :").append(this.val$vinNumber).append(", Data :").append(s).toString());
                com.navdy.hud.app.obd.CarDetails a = com.navdy.hud.app.obd.CarDetails.fromJson(s);
                label0: {
                    if (a != null) {
                        break label0;
                    }
                    com.navdy.hud.app.obd.CarMDVinDecoder.access$100().d("Error parsing the CarMD response to CarDetails");
                    break label1;
                }
                com.navdy.hud.app.obd.CarMDVinDecoder.access$200(this.this$0).setDecodedCarDetails(a);
                com.navdy.hud.app.obd.CarMDVinDecoder.access$302(this.this$0, false);
                break label2;
            }
            if (com.navdy.hud.app.obd.CarMDVinDecoder.access$400(this.this$0)) {
                okhttp3.HttpUrl$Builder a0 = com.navdy.hud.app.obd.CarMDVinDecoder.access$500().newBuilder();
                a0.addQueryParameter("vin", this.val$vinNumber);
                okhttp3.HttpUrl a1 = a0.build();
                okhttp3.Request$Builder a2 = new okhttp3.Request$Builder().url(a1);
                if (android.text.TextUtils.isEmpty((CharSequence)com.navdy.hud.app.obd.CarMDVinDecoder.access$600(this.this$0))) {
                    com.navdy.hud.app.obd.CarMDVinDecoder.access$100().e("Missing car md auth credential !");
                    com.navdy.hud.app.obd.CarMDVinDecoder.access$302(this.this$0, false);
                } else {
                    a2.addHeader("authorization", new StringBuilder().append("Basic ").append(com.navdy.hud.app.obd.CarMDVinDecoder.access$600(this.this$0)).toString());
                    if (android.text.TextUtils.isEmpty((CharSequence)com.navdy.hud.app.obd.CarMDVinDecoder.access$700(this.this$0))) {
                        com.navdy.hud.app.obd.CarMDVinDecoder.access$100().e("Missing car md token credential !");
                        com.navdy.hud.app.obd.CarMDVinDecoder.access$302(this.this$0, false);
                    } else {
                        a2.addHeader("partner-token", com.navdy.hud.app.obd.CarMDVinDecoder.access$700(this.this$0));
                        okhttp3.Request a3 = a2.build();
                        com.navdy.hud.app.obd.CarMDVinDecoder.access$900(this.this$0).newCall(a3).enqueue((okhttp3.Callback)new com.navdy.hud.app.obd.CarMDVinDecoder$1$1(this));
                    }
                }
            } else {
                com.navdy.hud.app.obd.CarMDVinDecoder.access$302(this.this$0, false);
            }
        } catch(Exception ignoredException) {
            com.navdy.hud.app.obd.CarMDVinDecoder.access$100().e("Exception while fetching the vin from CarMD");
            com.navdy.hud.app.obd.CarMDVinDecoder.access$302(this.this$0, false);
        }
    }
}
