package com.navdy.hud.app.obd;

public class CarServiceConnector extends com.navdy.hud.app.common.ServiceReconnector {
    private com.navdy.obd.ICarService carApi;
    
    public CarServiceConnector(android.content.Context a) {
        super(a, new android.content.Intent("com.navdy.obd.action.START_AUTO_CONNECT"), com.navdy.obd.ICarService.class.getName());
    }
    
    public com.navdy.obd.ICarService getCarApi() {
        return this.carApi;
    }
    
    protected void onConnected(android.content.ComponentName a, android.os.IBinder a0) {
        this.carApi = com.navdy.obd.ICarService$Stub.asInterface(a0);
        com.navdy.hud.app.obd.ObdManager.getInstance().serviceConnected();
    }
    
    protected void onDisconnected(android.content.ComponentName a) {
        this.carApi = null;
        com.navdy.hud.app.obd.ObdManager.getInstance().serviceDisconnected();
    }
}
