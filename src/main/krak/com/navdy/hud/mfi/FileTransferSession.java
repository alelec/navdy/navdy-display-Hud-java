package com.navdy.hud.mfi;

public class FileTransferSession {
    final public static int DATA_OFFSET = 2;
    final private static int INDEX_OF_COMMAND = 1;
    final private static int INDEX_OF_ID = 0;
    final public static int MESSAGE_SIZE = 2;
    final private static String TAG;
    private boolean mCanceled;
    private byte[] mFileData;
    private int mFileReceivedSoFar;
    private long mFileSize;
    public int mFileTransferIdentifier;
    com.navdy.hud.mfi.IIAPFileTransferManager mFileTransferManager;
    private boolean mFinished;
    private boolean mStarted;
    
    static {
        TAG = com.navdy.hud.mfi.FileTransferSession.class.getSimpleName();
    }
    
    public FileTransferSession(int i, com.navdy.hud.mfi.IIAPFileTransferManager a) {
        this.mStarted = false;
        this.mFinished = false;
        this.mCanceled = false;
        this.mFileTransferManager = a;
        this.mFileTransferIdentifier = i;
    }
    
    private void error(Throwable a) {
        this.mFinished = true;
        this.sendCommand(com.navdy.hud.mfi.FileTransferSession$FileTransferCommand.COMMAND_FAILURE);
        this.mFileTransferManager.onError(this.mFileTransferIdentifier, a);
    }
    
    private void sendCommand(com.navdy.hud.mfi.FileTransferSession$FileTransferCommand a) {
        int i = (byte)(this.mFileTransferIdentifier & 255);
        int i0 = (byte)a.value;
        com.navdy.hud.mfi.IIAPFileTransferManager a0 = this.mFileTransferManager;
        int i1 = this.mFileTransferIdentifier;
        byte[] a1 = new byte[2];
        a1[0] = (byte)i;
        a1[1] = (byte)i0;
        a0.sendMessage(i1, a1);
        android.util.Log.d("MFi", new StringBuilder().append("Accessory(F): ").append(this.mFileTransferIdentifier).append(" Command : ").append(a.name()).toString());
    }
    
    public void bProcessFileTransferMessage(byte[] a) {
        int i = a[1];
        com.navdy.hud.mfi.FileTransferSession$FileTransferCommand a0 = com.navdy.hud.mfi.FileTransferSession$FileTransferCommand.fromInt(i & 255);
        com.navdy.hud.mfi.FileTransferSession$FileTransferCommand a1 = com.navdy.hud.mfi.FileTransferSession$FileTransferCommand.COMMAND_SETUP;
        label2: {
            label1: {
                if (a0 == a1) {
                    break label1;
                }
                if (this.mStarted) {
                    break label1;
                }
                android.util.Log.e(TAG, new StringBuilder().append("File transfer is not initiated, bad state ").append(a0).toString());
                this.mFileTransferManager.onError(this.mFileTransferIdentifier, (Throwable)new IllegalStateException("File transfer not started"));
                break label2;
            }
            switch(com.navdy.hud.mfi.FileTransferSession$1.$SwitchMap$com$navdy$hud$mfi$FileTransferSession$FileTransferCommand[a0.ordinal()]) {
                case 7: {
                    android.util.Log.d("MFi", new StringBuilder().append("Apple Device(F): ").append(this.mFileTransferIdentifier).append(" Command : ").append(a0.name()).toString());
                    this.mFileTransferManager.onPaused(this.mFileTransferIdentifier);
                    break;
                }
                case 6: {
                    this.mFinished = true;
                    android.util.Log.d("MFi", new StringBuilder().append("Apple Device(F): ").append(this.mFileTransferIdentifier).append(" Command : ").append(a0.name()).toString());
                    this.mFileTransferManager.onCanceled(this.mFileTransferIdentifier);
                    break;
                }
                case 5: {
                    Throwable a2 = null;
                    label0: {
                        int i0 = 0;
                        try {
                            System.arraycopy(a, 2, this.mFileData, this.mFileReceivedSoFar, a.length - 2);
                            this.mFileReceivedSoFar = this.mFileReceivedSoFar + (a.length - 2);
                            android.util.Log.d("MFi", new StringBuilder().append("Apple Device(F): ").append(this.mFileTransferIdentifier).append(" Command : ").append(a0.name()).append(", Received : ").append(this.mFileReceivedSoFar).toString());
                            long j = (long)this.mFileReceivedSoFar;
                            long j0 = this.mFileSize;
                            i0 = (j < j0) ? -1 : (j == j0) ? 0 : 1;
                        } catch(Throwable a3) {
                            a2 = a3;
                            break label0;
                        }
                        try {
                            if (i0 == 0) {
                                this.sendCommand(com.navdy.hud.mfi.FileTransferSession$FileTransferCommand.COMMAND_SUCCESS);
                                this.mFinished = true;
                                this.mFileTransferManager.onSuccess(this.mFileTransferIdentifier, this.mFileData);
                                break;
                            } else {
                                this.sendCommand(com.navdy.hud.mfi.FileTransferSession$FileTransferCommand.COMMAND_FAILURE);
                                this.mFinished = true;
                                this.mFileTransferManager.onError(this.mFileTransferIdentifier, (Throwable)new java.io.IOException("Missed packets"));
                                break;
                            }
                        } catch(Throwable a4) {
                            a2 = a4;
                        }
                    }
                    this.error(a2);
                    break;
                }
                case 4: {
                    try {
                        System.arraycopy(a, 2, this.mFileData, this.mFileReceivedSoFar, a.length - 2);
                        this.mFileReceivedSoFar = this.mFileReceivedSoFar + (a.length - 2);
                        android.util.Log.d("MFi", new StringBuilder().append("Apple Device(F): ").append(this.mFileTransferIdentifier).append(" Command : ").append(a0.name()).append(", Received : ").append(this.mFileReceivedSoFar).toString());
                        break;
                    } catch(Throwable a5) {
                        this.error(a5);
                        break;
                    }
                }
                case 3: {
                    try {
                        System.arraycopy(a, 2, this.mFileData, this.mFileReceivedSoFar, a.length - 2);
                        this.mFileReceivedSoFar = this.mFileReceivedSoFar + (a.length - 2);
                        android.util.Log.d("MFi", new StringBuilder().append("Apple Device(F): ").append(this.mFileTransferIdentifier).append(" Command : ").append(a0.name()).append(", Received : ").append(this.mFileReceivedSoFar).toString());
                        this.sendCommand(com.navdy.hud.mfi.FileTransferSession$FileTransferCommand.COMMAND_SUCCESS);
                        this.mFinished = true;
                        this.mFileTransferManager.onSuccess(this.mFileTransferIdentifier, this.mFileData);
                        break;
                    } catch(Throwable a6) {
                        this.error(a6);
                        break;
                    }
                }
                case 2: {
                    try {
                        System.arraycopy(a, 2, this.mFileData, this.mFileReceivedSoFar, a.length - 2);
                        this.mFileReceivedSoFar = this.mFileReceivedSoFar + (a.length - 2);
                        android.util.Log.d("MFi", new StringBuilder().append("Apple Device(F): ").append(this.mFileTransferIdentifier).append(" Command : ").append(a0.name()).append(", Received : ").append(this.mFileReceivedSoFar).toString());
                        break;
                    } catch(Throwable a7) {
                        this.error(a7);
                        break;
                    }
                }
                case 1: {
                    this.mStarted = true;
                    this.mFileSize = com.navdy.hud.mfi.Utils.unpackInt64(a, 2);
                    android.util.Log.d("MFi", new StringBuilder().append("Apple Device(F): ").append(this.mFileTransferIdentifier).append(" Command : ").append(a0.name()).append(", File_Size : ").append(this.mFileSize).toString());
                    if (this.mFileSize <= this.mFileTransferManager.getFileTransferLimit()) {
                        this.mFileTransferManager.onFileTransferSetupRequest(this.mFileTransferIdentifier, this.mFileSize);
                        if (this.mFileSize != 0L) {
                            break;
                        }
                        this.sendCommand(com.navdy.hud.mfi.FileTransferSession$FileTransferCommand.COMMAND_CANCEL);
                        this.mFileTransferManager.onSuccess(this.mFileTransferIdentifier, (byte[])null);
                        break;
                    } else {
                        android.util.Log.e(TAG, "File too large, not starting the transfer");
                        this.sendCommand(com.navdy.hud.mfi.FileTransferSession$FileTransferCommand.COMMAND_CANCEL);
                        break;
                    }
                }
            }
        }
    }
    
    public void cancel() {
        synchronized(this) {
            this.mCanceled = true;
            if (this.mFileTransferManager != null) {
                this.sendCommand(com.navdy.hud.mfi.FileTransferSession$FileTransferCommand.COMMAND_CANCEL);
                this.mFinished = true;
            }
        }
        /*monexit(this)*/;
    }
    
    public boolean isActive() {
        boolean b = false;
        boolean b0 = this.mStarted;
        label2: {
            label0: {
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.mFinished) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    public void proceed() {
        synchronized(this) {
            if (!this.mCanceled) {
                this.mFileData = new byte[(int)this.mFileSize];
                if (this.mFileTransferManager != null) {
                    this.sendCommand(com.navdy.hud.mfi.FileTransferSession$FileTransferCommand.COMMAND_START);
                }
            }
        }
        /*monexit(this)*/;
    }
    
    public void release() {
    }
}
