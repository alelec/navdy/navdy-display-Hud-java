package com.navdy.hud.mfi;

public class NowPlayingUpdate {
    public String mAppBundleId;
    public String mAppName;
    public long mPlaybackElapsedTimeMilliseconds;
    public com.navdy.hud.mfi.NowPlayingUpdate$PlaybackStatus mPlaybackStatus;
    public String mediaItemAlbumTitle;
    public int mediaItemAlbumTrackCount;
    public int mediaItemAlbumTrackNumber;
    public String mediaItemArtist;
    public int mediaItemArtworkFileTransferIdentifier;
    public String mediaItemGenre;
    public java.math.BigInteger mediaItemPersistentIdentifier;
    public long mediaItemPlaybackDurationInMilliseconds;
    public String mediaItemTitle;
    public com.navdy.hud.mfi.NowPlayingUpdate$PlaybackRepeat playbackRepeat;
    public com.navdy.hud.mfi.NowPlayingUpdate$PlaybackShuffle playbackShuffle;
    
    public NowPlayingUpdate() {
    }
    
    public String toString() {
        StringBuilder a = new StringBuilder("NowPlayingUpdate{");
        if (this.mediaItemTitle != null) {
            a.append("title='").append(this.mediaItemTitle).append((char)39);
        }
        if (this.mediaItemPlaybackDurationInMilliseconds != 0L) {
            a.append(", duration=").append(this.mediaItemPlaybackDurationInMilliseconds);
        }
        if (this.mediaItemAlbumTitle != null) {
            a.append(", albumTitle='").append(this.mediaItemAlbumTitle).append((char)39);
        }
        if (this.mediaItemAlbumTrackNumber != 0) {
            a.append(", albumTrackNumber=").append(this.mediaItemAlbumTrackNumber);
        }
        if (this.mediaItemAlbumTrackCount != 0) {
            a.append(", albumTrackCount=").append(this.mediaItemAlbumTrackCount);
        }
        if (this.mediaItemArtist != null) {
            a.append(", artist='").append(this.mediaItemArtist).append((char)39);
        }
        if (this.mediaItemGenre != null) {
            a.append(", itemGenre='").append(this.mediaItemGenre).append((char)39);
        }
        if (this.mediaItemArtworkFileTransferIdentifier != 0) {
            a.append(", fileTransferIdentifier=").append(this.mediaItemArtworkFileTransferIdentifier);
        }
        if (this.mPlaybackStatus != null) {
            a.append(", playbackStatus=").append(this.mPlaybackStatus);
        }
        a.append(", playbackElapsedTimeMilliseconds=").append(this.mPlaybackElapsedTimeMilliseconds);
        a.append((char)125);
        return a.toString();
    }
}
