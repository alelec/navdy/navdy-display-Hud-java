package com.navdy.hud.mfi;


    public enum iAPProcessor$RequestAppLaunch {
        AppBundleID(0),
    LaunchAlert(1);

        private int value;
        iAPProcessor$RequestAppLaunch(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class iAPProcessor$RequestAppLaunch extends Enum {
//    final private static com.navdy.hud.mfi.iAPProcessor$RequestAppLaunch[] $VALUES;
//    final public static com.navdy.hud.mfi.iAPProcessor$RequestAppLaunch AppBundleID;
//    final public static com.navdy.hud.mfi.iAPProcessor$RequestAppLaunch LaunchAlert;
//    
//    static {
//        AppBundleID = new com.navdy.hud.mfi.iAPProcessor$RequestAppLaunch("AppBundleID", 0);
//        LaunchAlert = new com.navdy.hud.mfi.iAPProcessor$RequestAppLaunch("LaunchAlert", 1);
//        com.navdy.hud.mfi.iAPProcessor$RequestAppLaunch[] a = new com.navdy.hud.mfi.iAPProcessor$RequestAppLaunch[2];
//        a[0] = AppBundleID;
//        a[1] = LaunchAlert;
//        $VALUES = a;
//    }
//    
//    private iAPProcessor$RequestAppLaunch(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.mfi.iAPProcessor$RequestAppLaunch valueOf(String s) {
//        return (com.navdy.hud.mfi.iAPProcessor$RequestAppLaunch)Enum.valueOf(com.navdy.hud.mfi.iAPProcessor$RequestAppLaunch.class, s);
//    }
//    
//    public static com.navdy.hud.mfi.iAPProcessor$RequestAppLaunch[] values() {
//        return $VALUES.clone();
//    }
//}
//