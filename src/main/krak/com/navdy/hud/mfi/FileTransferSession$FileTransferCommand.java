package com.navdy.hud.mfi;


    public enum FileTransferSession$FileTransferCommand {
        COMMAND_DATA(0),
    COMMAND_START(1),
    COMMAND_CANCEL(2),
    COMMAND_PAUSE(3),
    COMMAND_SETUP(4),
    COMMAND_SUCCESS(5),
    COMMAND_FAILURE(6),
    COMMAND_LAST_DATA(7),
    COMMAND_FIRST_DATA(8),
    COMMAND_FIRST_AND_ONLY_DATA(9);

        private int value;
        FileTransferSession$FileTransferCommand(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class FileTransferSession$FileTransferCommand extends Enum {
//    final private static com.navdy.hud.mfi.FileTransferSession$FileTransferCommand[] $VALUES;
//    final public static com.navdy.hud.mfi.FileTransferSession$FileTransferCommand COMMAND_CANCEL;
//    final public static com.navdy.hud.mfi.FileTransferSession$FileTransferCommand COMMAND_DATA;
//    final public static com.navdy.hud.mfi.FileTransferSession$FileTransferCommand COMMAND_FAILURE;
//    final public static com.navdy.hud.mfi.FileTransferSession$FileTransferCommand COMMAND_FIRST_AND_ONLY_DATA;
//    final public static com.navdy.hud.mfi.FileTransferSession$FileTransferCommand COMMAND_FIRST_DATA;
//    final public static com.navdy.hud.mfi.FileTransferSession$FileTransferCommand COMMAND_LAST_DATA;
//    final public static com.navdy.hud.mfi.FileTransferSession$FileTransferCommand COMMAND_PAUSE;
//    final public static com.navdy.hud.mfi.FileTransferSession$FileTransferCommand COMMAND_SETUP;
//    final public static com.navdy.hud.mfi.FileTransferSession$FileTransferCommand COMMAND_START;
//    final public static com.navdy.hud.mfi.FileTransferSession$FileTransferCommand COMMAND_SUCCESS;
//    final public int value;
//    
//    static {
//        COMMAND_DATA = new com.navdy.hud.mfi.FileTransferSession$FileTransferCommand("COMMAND_DATA", 0, 0);
//        COMMAND_START = new com.navdy.hud.mfi.FileTransferSession$FileTransferCommand("COMMAND_START", 1, 1);
//        COMMAND_CANCEL = new com.navdy.hud.mfi.FileTransferSession$FileTransferCommand("COMMAND_CANCEL", 2, 2);
//        COMMAND_PAUSE = new com.navdy.hud.mfi.FileTransferSession$FileTransferCommand("COMMAND_PAUSE", 3, 3);
//        COMMAND_SETUP = new com.navdy.hud.mfi.FileTransferSession$FileTransferCommand("COMMAND_SETUP", 4, 4);
//        COMMAND_SUCCESS = new com.navdy.hud.mfi.FileTransferSession$FileTransferCommand("COMMAND_SUCCESS", 5, 5);
//        COMMAND_FAILURE = new com.navdy.hud.mfi.FileTransferSession$FileTransferCommand("COMMAND_FAILURE", 6, 6);
//        COMMAND_LAST_DATA = new com.navdy.hud.mfi.FileTransferSession$FileTransferCommand("COMMAND_LAST_DATA", 7, 64);
//        COMMAND_FIRST_DATA = new com.navdy.hud.mfi.FileTransferSession$FileTransferCommand("COMMAND_FIRST_DATA", 8, 128);
//        COMMAND_FIRST_AND_ONLY_DATA = new com.navdy.hud.mfi.FileTransferSession$FileTransferCommand("COMMAND_FIRST_AND_ONLY_DATA", 9, 192);
//        com.navdy.hud.mfi.FileTransferSession$FileTransferCommand[] a = new com.navdy.hud.mfi.FileTransferSession$FileTransferCommand[10];
//        a[0] = COMMAND_DATA;
//        a[1] = COMMAND_START;
//        a[2] = COMMAND_CANCEL;
//        a[3] = COMMAND_PAUSE;
//        a[4] = COMMAND_SETUP;
//        a[5] = COMMAND_SUCCESS;
//        a[6] = COMMAND_FAILURE;
//        a[7] = COMMAND_LAST_DATA;
//        a[8] = COMMAND_FIRST_DATA;
//        a[9] = COMMAND_FIRST_AND_ONLY_DATA;
//        $VALUES = a;
//    }
//    
//    private FileTransferSession$FileTransferCommand(String s, int i, int i0) {
//        super(s, i);
//        this.value = i0;
//    }
//    
//    public static com.navdy.hud.mfi.FileTransferSession$FileTransferCommand fromInt(int i) {
//        com.navdy.hud.mfi.FileTransferSession$FileTransferCommand a = null;
//        int i0 = COMMAND_DATA.value;
//        label1: {
//            label0: {
//                if (i < i0) {
//                    break label0;
//                }
//                if (i > COMMAND_FAILURE.value) {
//                    break label0;
//                }
//                a = com.navdy.hud.mfi.FileTransferSession$FileTransferCommand.values()[i];
//                break label1;
//            }
//            a = (i != COMMAND_LAST_DATA.value) ? (i != COMMAND_FIRST_DATA.value) ? (i != COMMAND_FIRST_AND_ONLY_DATA.value) ? null : COMMAND_FIRST_AND_ONLY_DATA : COMMAND_FIRST_DATA : COMMAND_LAST_DATA;
//        }
//        return a;
//    }
//    
//    public static com.navdy.hud.mfi.FileTransferSession$FileTransferCommand valueOf(String s) {
//        return (com.navdy.hud.mfi.FileTransferSession$FileTransferCommand)Enum.valueOf(com.navdy.hud.mfi.FileTransferSession$FileTransferCommand.class, s);
//    }
//    
//    public static com.navdy.hud.mfi.FileTransferSession$FileTransferCommand[] values() {
//        return $VALUES.clone();
//    }
//}
//