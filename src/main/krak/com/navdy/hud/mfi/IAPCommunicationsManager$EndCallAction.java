package com.navdy.hud.mfi;


    public enum IAPCommunicationsManager$EndCallAction {
        EndDecline(0),
    EndAll(1);

        private int value;
        IAPCommunicationsManager$EndCallAction(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class IAPCommunicationsManager$EndCallAction extends Enum {
//    final private static com.navdy.hud.mfi.IAPCommunicationsManager$EndCallAction[] $VALUES;
//    final public static com.navdy.hud.mfi.IAPCommunicationsManager$EndCallAction EndAll;
//    final public static com.navdy.hud.mfi.IAPCommunicationsManager$EndCallAction EndDecline;
//    
//    static {
//        EndDecline = new com.navdy.hud.mfi.IAPCommunicationsManager$EndCallAction("EndDecline", 0);
//        EndAll = new com.navdy.hud.mfi.IAPCommunicationsManager$EndCallAction("EndAll", 1);
//        com.navdy.hud.mfi.IAPCommunicationsManager$EndCallAction[] a = new com.navdy.hud.mfi.IAPCommunicationsManager$EndCallAction[2];
//        a[0] = EndDecline;
//        a[1] = EndAll;
//        $VALUES = a;
//    }
//    
//    private IAPCommunicationsManager$EndCallAction(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.mfi.IAPCommunicationsManager$EndCallAction valueOf(String s) {
//        return (com.navdy.hud.mfi.IAPCommunicationsManager$EndCallAction)Enum.valueOf(com.navdy.hud.mfi.IAPCommunicationsManager$EndCallAction.class, s);
//    }
//    
//    public static com.navdy.hud.mfi.IAPCommunicationsManager$EndCallAction[] values() {
//        return $VALUES.clone();
//    }
//}
//