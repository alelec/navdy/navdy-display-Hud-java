package com.navdy.hud.mfi;

abstract public interface LinkPacketReceiver {
    abstract public void queue(com.navdy.hud.mfi.LinkPacket arg);
}
