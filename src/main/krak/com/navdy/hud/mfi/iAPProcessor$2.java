package com.navdy.hud.mfi;

class iAPProcessor$2 {
    final static int[] $SwitchMap$com$navdy$hud$mfi$iAPProcessor$MediaRemoteComponent;
    final static int[] $SwitchMap$com$navdy$hud$mfi$iAPProcessor$iAPMessage;
    
    static {
        $SwitchMap$com$navdy$hud$mfi$iAPProcessor$MediaRemoteComponent = new int[com.navdy.hud.mfi.iAPProcessor$MediaRemoteComponent.values().length];
        int[] a = $SwitchMap$com$navdy$hud$mfi$iAPProcessor$MediaRemoteComponent;
        com.navdy.hud.mfi.iAPProcessor$MediaRemoteComponent a0 = com.navdy.hud.mfi.iAPProcessor$MediaRemoteComponent.HIDKeyboard;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$mfi$iAPProcessor$MediaRemoteComponent[com.navdy.hud.mfi.iAPProcessor$MediaRemoteComponent.HIDMediaRemote.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        $SwitchMap$com$navdy$hud$mfi$iAPProcessor$iAPMessage = new int[com.navdy.hud.mfi.iAPProcessor$iAPMessage.values().length];
        int[] a1 = $SwitchMap$com$navdy$hud$mfi$iAPProcessor$iAPMessage;
        com.navdy.hud.mfi.iAPProcessor$iAPMessage a2 = com.navdy.hud.mfi.iAPProcessor$iAPMessage.RequestAuthenticationRequest;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$hud$mfi$iAPProcessor$iAPMessage[com.navdy.hud.mfi.iAPProcessor$iAPMessage.RequestAuthenticationChallengeResponse.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$hud$mfi$iAPProcessor$iAPMessage[com.navdy.hud.mfi.iAPProcessor$iAPMessage.AuthenticationSucceeded.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException3) {
        }
        try {
            $SwitchMap$com$navdy$hud$mfi$iAPProcessor$iAPMessage[com.navdy.hud.mfi.iAPProcessor$iAPMessage.StartIdentification.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException4) {
        }
        try {
            $SwitchMap$com$navdy$hud$mfi$iAPProcessor$iAPMessage[com.navdy.hud.mfi.iAPProcessor$iAPMessage.IdentificationAccepted.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException5) {
        }
        try {
            $SwitchMap$com$navdy$hud$mfi$iAPProcessor$iAPMessage[com.navdy.hud.mfi.iAPProcessor$iAPMessage.IdentificationRejected.ordinal()] = 6;
        } catch(NoSuchFieldError ignoredException6) {
        }
        try {
            $SwitchMap$com$navdy$hud$mfi$iAPProcessor$iAPMessage[com.navdy.hud.mfi.iAPProcessor$iAPMessage.DeviceAuthenticationCertificate.ordinal()] = 7;
        } catch(NoSuchFieldError ignoredException7) {
        }
        try {
            $SwitchMap$com$navdy$hud$mfi$iAPProcessor$iAPMessage[com.navdy.hud.mfi.iAPProcessor$iAPMessage.DeviceAuthenticationResponse.ordinal()] = 8;
        } catch(NoSuchFieldError ignoredException8) {
        }
        try {
            $SwitchMap$com$navdy$hud$mfi$iAPProcessor$iAPMessage[com.navdy.hud.mfi.iAPProcessor$iAPMessage.StartExternalAccessoryProtocolSession.ordinal()] = 9;
        } catch(NoSuchFieldError ignoredException9) {
        }
        try {
            $SwitchMap$com$navdy$hud$mfi$iAPProcessor$iAPMessage[com.navdy.hud.mfi.iAPProcessor$iAPMessage.StopExternalAccessoryProtocolSession.ordinal()] = 10;
        } catch(NoSuchFieldError ignoredException10) {
        }
    }
}
