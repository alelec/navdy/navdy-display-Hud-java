package com.navdy.hud.mfi;


    public enum iAPProcessor$StartHID {
        HIDComponentIdentifier(0),
    VendorIdentifier(1),
    ProductIdentifier(2),
    LocalizedKeyboardCountryCode(3),
    HIDReportDescriptor(4);

        private int value;
        iAPProcessor$StartHID(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class iAPProcessor$StartHID extends Enum {
//    final private static com.navdy.hud.mfi.iAPProcessor$StartHID[] $VALUES;
//    final public static com.navdy.hud.mfi.iAPProcessor$StartHID HIDComponentIdentifier;
//    final public static com.navdy.hud.mfi.iAPProcessor$StartHID HIDReportDescriptor;
//    final public static com.navdy.hud.mfi.iAPProcessor$StartHID LocalizedKeyboardCountryCode;
//    final public static com.navdy.hud.mfi.iAPProcessor$StartHID ProductIdentifier;
//    final public static com.navdy.hud.mfi.iAPProcessor$StartHID VendorIdentifier;
//    
//    static {
//        HIDComponentIdentifier = new com.navdy.hud.mfi.iAPProcessor$StartHID("HIDComponentIdentifier", 0);
//        VendorIdentifier = new com.navdy.hud.mfi.iAPProcessor$StartHID("VendorIdentifier", 1);
//        ProductIdentifier = new com.navdy.hud.mfi.iAPProcessor$StartHID("ProductIdentifier", 2);
//        LocalizedKeyboardCountryCode = new com.navdy.hud.mfi.iAPProcessor$StartHID("LocalizedKeyboardCountryCode", 3);
//        HIDReportDescriptor = new com.navdy.hud.mfi.iAPProcessor$StartHID("HIDReportDescriptor", 4);
//        com.navdy.hud.mfi.iAPProcessor$StartHID[] a = new com.navdy.hud.mfi.iAPProcessor$StartHID[5];
//        a[0] = HIDComponentIdentifier;
//        a[1] = VendorIdentifier;
//        a[2] = ProductIdentifier;
//        a[3] = LocalizedKeyboardCountryCode;
//        a[4] = HIDReportDescriptor;
//        $VALUES = a;
//    }
//    
//    private iAPProcessor$StartHID(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.mfi.iAPProcessor$StartHID valueOf(String s) {
//        return (com.navdy.hud.mfi.iAPProcessor$StartHID)Enum.valueOf(com.navdy.hud.mfi.iAPProcessor$StartHID.class, s);
//    }
//    
//    public static com.navdy.hud.mfi.iAPProcessor$StartHID[] values() {
//        return $VALUES.clone();
//    }
//}
//