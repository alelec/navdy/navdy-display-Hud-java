package com.navdy.hud.mfi;

public class SessionPacket extends com.navdy.hud.mfi.Packet {
    int session;
    
    public SessionPacket(int i, byte[] a) {
        super(a);
        this.session = i;
    }
}
