package com.navdy.hud.mfi;


    public enum IAPCommunicationsManager$InitiateCallType {
        Destination(0),
    VoiceMail(1),
    Redial(2);

        private int value;
        IAPCommunicationsManager$InitiateCallType(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class IAPCommunicationsManager$InitiateCallType extends Enum {
//    final private static com.navdy.hud.mfi.IAPCommunicationsManager$InitiateCallType[] $VALUES;
//    final public static com.navdy.hud.mfi.IAPCommunicationsManager$InitiateCallType Destination;
//    final public static com.navdy.hud.mfi.IAPCommunicationsManager$InitiateCallType Redial;
//    final public static com.navdy.hud.mfi.IAPCommunicationsManager$InitiateCallType VoiceMail;
//    
//    static {
//        Destination = new com.navdy.hud.mfi.IAPCommunicationsManager$InitiateCallType("Destination", 0);
//        VoiceMail = new com.navdy.hud.mfi.IAPCommunicationsManager$InitiateCallType("VoiceMail", 1);
//        Redial = new com.navdy.hud.mfi.IAPCommunicationsManager$InitiateCallType("Redial", 2);
//        com.navdy.hud.mfi.IAPCommunicationsManager$InitiateCallType[] a = new com.navdy.hud.mfi.IAPCommunicationsManager$InitiateCallType[3];
//        a[0] = Destination;
//        a[1] = VoiceMail;
//        a[2] = Redial;
//        $VALUES = a;
//    }
//    
//    private IAPCommunicationsManager$InitiateCallType(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.mfi.IAPCommunicationsManager$InitiateCallType valueOf(String s) {
//        return (com.navdy.hud.mfi.IAPCommunicationsManager$InitiateCallType)Enum.valueOf(com.navdy.hud.mfi.IAPCommunicationsManager$InitiateCallType.class, s);
//    }
//    
//    public static com.navdy.hud.mfi.IAPCommunicationsManager$InitiateCallType[] values() {
//        return $VALUES.clone();
//    }
//}
//