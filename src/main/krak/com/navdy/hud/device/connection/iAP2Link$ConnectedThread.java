package com.navdy.hud.device.connection;

class iAP2Link$ConnectedThread extends com.navdy.service.library.device.link.IOThread {
    final private java.io.InputStream mmInStream;
    final private java.io.OutputStream mmOutStream;
    final private com.navdy.service.library.network.SocketAdapter mmSocket;
    final com.navdy.hud.device.connection.iAP2Link this$0;
    
    public iAP2Link$ConnectedThread(com.navdy.hud.device.connection.iAP2Link a, com.navdy.service.library.network.SocketAdapter a0) {
        super();
        java.io.InputStream a1 = null;
        java.io.OutputStream a2 = null;
        this.this$0 = a;
        com.navdy.hud.device.connection.iAP2Link.access$200().d("create ConnectedThread");
        this.mmSocket = a0;
        try {
            a1 = null;
            a1 = a0.getInputStream();
            a2 = a0.getOutputStream();
        } catch(java.io.IOException a3) {
            com.navdy.hud.device.connection.iAP2Link.access$200().e("temp sockets not created", (Throwable)a3);
            a2 = null;
        }
        this.mmInStream = a1;
        this.mmOutStream = a2;
    }
    
    public void cancel() {
        super.cancel();
        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)this.mmOutStream);
        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)this.mmSocket);
    }
    
    public void run() {
        android.bluetooth.BluetoothDevice a = com.navdy.hud.device.connection.iAP2Link.access$800(this.this$0, this.mmSocket);
        com.navdy.hud.device.connection.iAP2Link.access$200().i(new StringBuilder().append("BEGIN mConnectedThread - ").append(a.getAddress()).append(" name:").append(a.getName()).toString());
        com.navdy.service.library.device.connection.Connection$DisconnectCause a0 = com.navdy.service.library.device.connection.Connection$DisconnectCause.NORMAL;
        com.navdy.hud.device.connection.iAP2Link.access$900(this.this$0).linkEstablished(com.navdy.service.library.device.connection.ConnectionType.BT_IAP2_LINK);
        com.navdy.hud.device.connection.iAP2Link.access$1000(this.this$0).connectionStarted(a.getAddress(), a.getName());
        byte[] a1 = new byte[1024];
        while(true) {
            boolean b = this.closing;
            label0: {
                java.io.IOException a2 = null;
                if (b) {
                    break label0;
                }
                try {
                    byte[] a3 = java.util.Arrays.copyOf(a1, this.mmInStream.read(a1));
                    com.navdy.hud.device.connection.iAP2Link.access$1000(this.this$0).queue(new com.navdy.hud.mfi.LinkPacket(a3));
                    continue;
                } catch(java.io.IOException a4) {
                    a2 = a4;
                }
                if (!this.closing) {
                    com.navdy.hud.device.connection.iAP2Link.access$200().e(new StringBuilder().append("disconnected: ").append(a2.getMessage()).toString());
                    a0 = com.navdy.service.library.device.connection.Connection$DisconnectCause.ABORTED;
                }
            }
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)this.mmInStream);
            com.navdy.hud.device.connection.iAP2Link.access$1000(this.this$0).connectionEnded();
            com.navdy.hud.device.connection.iAP2Link.access$900(this.this$0).linkLost(com.navdy.service.library.device.connection.ConnectionType.BT_IAP2_LINK, a0);
            return;
        }
    }
    
    public void write(byte[] a) {
        try {
            this.mmOutStream.write(a);
        } catch(java.io.IOException a0) {
            com.navdy.hud.device.connection.iAP2Link.access$200().e("Exception during write", (Throwable)a0);
        }
    }
}
