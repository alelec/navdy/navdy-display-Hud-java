package com.navdy.hud.device.connection;

class IAPMessageUtility$4 {
    final static int[] $SwitchMap$com$navdy$hud$mfi$CallStateUpdate$Status;
    final static int[] $SwitchMap$com$navdy$service$library$events$callcontrol$CallAction;
    
    static {
        $SwitchMap$com$navdy$service$library$events$callcontrol$CallAction = new int[com.navdy.service.library.events.callcontrol.CallAction.values().length];
        int[] a = $SwitchMap$com$navdy$service$library$events$callcontrol$CallAction;
        com.navdy.service.library.events.callcontrol.CallAction a0 = com.navdy.service.library.events.callcontrol.CallAction.CALL_ACCEPT;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$callcontrol$CallAction[com.navdy.service.library.events.callcontrol.CallAction.CALL_END.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$callcontrol$CallAction[com.navdy.service.library.events.callcontrol.CallAction.CALL_REJECT.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$callcontrol$CallAction[com.navdy.service.library.events.callcontrol.CallAction.CALL_DIAL.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$callcontrol$CallAction[com.navdy.service.library.events.callcontrol.CallAction.CALL_MUTE.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException3) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$callcontrol$CallAction[com.navdy.service.library.events.callcontrol.CallAction.CALL_UNMUTE.ordinal()] = 6;
        } catch(NoSuchFieldError ignoredException4) {
        }
        $SwitchMap$com$navdy$hud$mfi$CallStateUpdate$Status = new int[com.navdy.hud.mfi.CallStateUpdate$Status.values().length];
        int[] a1 = $SwitchMap$com$navdy$hud$mfi$CallStateUpdate$Status;
        com.navdy.hud.mfi.CallStateUpdate$Status a2 = com.navdy.hud.mfi.CallStateUpdate$Status.Disconnected;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException5) {
        }
        try {
            $SwitchMap$com$navdy$hud$mfi$CallStateUpdate$Status[com.navdy.hud.mfi.CallStateUpdate$Status.Sending.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException6) {
        }
        try {
            $SwitchMap$com$navdy$hud$mfi$CallStateUpdate$Status[com.navdy.hud.mfi.CallStateUpdate$Status.Ringing.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException7) {
        }
        try {
            $SwitchMap$com$navdy$hud$mfi$CallStateUpdate$Status[com.navdy.hud.mfi.CallStateUpdate$Status.Connecting.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException8) {
        }
        try {
            $SwitchMap$com$navdy$hud$mfi$CallStateUpdate$Status[com.navdy.hud.mfi.CallStateUpdate$Status.Active.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException9) {
        }
        try {
            $SwitchMap$com$navdy$hud$mfi$CallStateUpdate$Status[com.navdy.hud.mfi.CallStateUpdate$Status.Held.ordinal()] = 6;
        } catch(NoSuchFieldError ignoredException10) {
        }
        try {
            $SwitchMap$com$navdy$hud$mfi$CallStateUpdate$Status[com.navdy.hud.mfi.CallStateUpdate$Status.Disconnecting.ordinal()] = 7;
        } catch(NoSuchFieldError ignoredException11) {
        }
    }
}
