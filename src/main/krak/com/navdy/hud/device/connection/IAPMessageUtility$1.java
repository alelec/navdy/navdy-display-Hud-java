package com.navdy.hud.device.connection;

final class IAPMessageUtility$1 extends java.util.HashMap {
    IAPMessageUtility$1() {
        this.put(com.navdy.hud.mfi.NowPlayingUpdate$PlaybackStatus.Stopped, com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_STOPPED);
        this.put(com.navdy.hud.mfi.NowPlayingUpdate$PlaybackStatus.Playing, com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_PLAYING);
        this.put(com.navdy.hud.mfi.NowPlayingUpdate$PlaybackStatus.Paused, com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_PAUSED);
        this.put(com.navdy.hud.mfi.NowPlayingUpdate$PlaybackStatus.SeekForward, com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_FAST_FORWARDING);
        this.put(com.navdy.hud.mfi.NowPlayingUpdate$PlaybackStatus.SeekBackward, com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_REWINDING);
    }
}
