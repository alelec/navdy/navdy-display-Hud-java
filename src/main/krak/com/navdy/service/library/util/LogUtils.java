package com.navdy.service.library.util;

final public class LogUtils {
    final private static int BUFFER_SIZE = 32768;
    final private static String CRASH_DUMP_FILE = "crash.log";
    final private static String CRASH_DUMP_STAGING = "staging";
    final private static byte[] CRLF;
    private static com.navdy.service.library.util.LogUtils$RotatingLogFile[] LOG_FILES;
    private static com.navdy.service.library.util.LogUtils$RotatingLogFile[] LOG_FILES_FOR_SNAPSHOT;
    final private static String LOG_MARKER = "--------- beginning of main";
    final private static String SYSTEM_LOG_PATH = ".logs";
    final private static String TAG;
    private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.util.LogUtils.class);
        TAG = com.navdy.service.library.util.LogUtils.class.getName();
        com.navdy.service.library.util.LogUtils$RotatingLogFile[] a = new com.navdy.service.library.util.LogUtils$RotatingLogFile[6];
        a[0] = new com.navdy.service.library.util.LogUtils$RotatingLogFile("a.log", 4);
        a[1] = new com.navdy.service.library.util.LogUtils$RotatingLogFile("obd.log", 2);
        a[2] = new com.navdy.service.library.util.LogUtils$RotatingLogFile("mfi.log", 2);
        a[3] = new com.navdy.service.library.util.LogUtils$RotatingLogFile("lighting.log", 2);
        a[4] = new com.navdy.service.library.util.LogUtils$RotatingLogFile("dial.log", 4);
        a[5] = new com.navdy.service.library.util.LogUtils$RotatingLogFile("obdRaw.log", 2);
        LOG_FILES = a;
        com.navdy.service.library.util.LogUtils$RotatingLogFile[] a0 = new com.navdy.service.library.util.LogUtils$RotatingLogFile[6];
        a0[0] = new com.navdy.service.library.util.LogUtils$RotatingLogFile("a.log", 2);
        a0[1] = new com.navdy.service.library.util.LogUtils$RotatingLogFile("obd.log", 2);
        a0[2] = new com.navdy.service.library.util.LogUtils$RotatingLogFile("mfi.log", 2);
        a0[3] = new com.navdy.service.library.util.LogUtils$RotatingLogFile("lighting.log", 2);
        a0[4] = new com.navdy.service.library.util.LogUtils$RotatingLogFile("dial.log", 2);
        a0[5] = new com.navdy.service.library.util.LogUtils$RotatingLogFile("obdRaw.log", 2);
        LOG_FILES_FOR_SNAPSHOT = a0;
        CRLF = "\r\n".getBytes();
    }
    
    public LogUtils() {
    }
    
    public static void copyComprehensiveSystemLogs(String s) {
        com.navdy.service.library.util.LogUtils.copySystemLogs(s, LOG_FILES);
    }
    
    public static void copySnapshotSystemLogs(String s) {
        com.navdy.service.library.util.LogUtils.copySystemLogs(s, LOG_FILES_FOR_SNAPSHOT);
    }
    
    public static void copySystemLogs(String s, com.navdy.service.library.util.LogUtils$RotatingLogFile[] a) {
        String s0 = new StringBuilder().append(android.os.Environment.getExternalStorageDirectory().getAbsolutePath()).append(java.io.File.separator).append(".logs").toString();
        int i = a.length;
        int i0 = 0;
        while(i0 < i) {
            com.navdy.service.library.util.LogUtils$RotatingLogFile a0 = a[i0];
            String s1 = a0.name;
            int i1 = a0.maxFiles;
            int i2 = 0;
            while(i2 < i1) {
                String s2 = (i2 <= 0) ? s1 : new StringBuilder().append(s1).append(".").append(i2).toString();
                com.navdy.service.library.util.IOUtils.copyFile(new StringBuilder().append(s0).append(java.io.File.separator).append(s2).toString(), new StringBuilder().append(s).append(java.io.File.separator).append(s2).toString());
                i2 = i2 + 1;
            }
            i0 = i0 + 1;
        }
    }
    
    public static void createCrashDump(android.content.Context a, String s, Thread a0, Throwable a1, boolean b) {
        label2: synchronized(com.navdy.service.library.util.LogUtils.class) {
            java.io.FileOutputStream a2 = null;
            Throwable a3 = null;
            label0: {
                try {
                    String s0 = com.navdy.service.library.util.SystemUtils.getProcessName(a, android.os.Process.myPid());
                    android.util.Log.v(TAG, "creating CrashDump");
                    java.io.File a4 = new java.io.File(new StringBuilder().append(s).append(java.io.File.separator).append("staging").toString());
                    if (a4.exists()) {
                        com.navdy.service.library.util.IOUtils.deleteDirectory(a, a4);
                    }
                    com.navdy.service.library.util.IOUtils.createDirectory(a4);
                    String s1 = a4.getAbsolutePath();
                    a2 = new java.io.FileOutputStream(new StringBuilder().append(s1).append(java.io.File.separator).append("crash.log").toString());
                    label1: {
                        try {
                            com.navdy.service.library.util.LogUtils.writeCrashInfo(a2, a1, s0, a0, b);
                            com.navdy.service.library.util.IOUtils.fileSync(a2);
                            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a2);
                            break label1;
                        } catch(Throwable a5) {
                            a3 = a5;
                        }
                        break label0;
                    }
                    try {
                        com.navdy.service.library.util.LogUtils.copyComprehensiveSystemLogs(s1);
                    } catch(Throwable a6) {
                        android.util.Log.e(TAG, "copySystemLogs", a6);
                    }
                    String s2 = b ? "_native" : "";
                    com.navdy.service.library.util.LogUtils.zipStaging(a, new StringBuilder().append(s).append(java.io.File.separator).append(s0).append(s2).append("_crash_").append(System.currentTimeMillis()).append(".zip").toString(), s1);
                    com.navdy.service.library.util.IOUtils.deleteDirectory(a, new java.io.File(s1));
                } catch(Throwable a7) {
                    a2 = null;
                    a3 = a7;
                    break label0;
                }
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)null);
                break label2;
            }
            try {
                android.util.Log.e(TAG, "createCrashDump", a3);
            } catch(Throwable a8) {
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a2);
                throw a8;
            }
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a2);
        }
        /*monexit(com.navdy.service.library.util.LogUtils.class)*/;
    }
    
    public static void createKernelCrashDump(android.content.Context a, String s, String[] a0) {
        synchronized(com.navdy.service.library.util.LogUtils.class) {
            label1: {
                String s0 = null;
                int i = 0;
                int i0 = 0;
                int i1 = 0;
                try {
                    java.io.File a2 = new java.io.File(new StringBuilder().append(s).append(java.io.File.separator).append("staging").toString());
                    if (a2.exists()) {
                        com.navdy.service.library.util.IOUtils.deleteDirectory(a, a2);
                    }
                    com.navdy.service.library.util.IOUtils.createDirectory(a2);
                    s0 = a2.getAbsolutePath();
                    i = a0.length;
                    i0 = 0;
                    i1 = 0;
                } catch(Throwable a3) {
                    a1 = a3;
                    break label1;
                }
                while(i1 < i) {
                    String s1 = a0[i1];
                    try {
                        java.io.File a4 = new java.io.File(s1);
                        if (com.navdy.service.library.util.IOUtils.copyFile(s1, new StringBuilder().append(s0).append(java.io.File.separator).append(a4.getName()).toString())) {
                            sLogger.v(new StringBuilder().append("copied:").append(s1).toString());
                            i0 = i0 + 1;
                        }
                        i1 = i1 + 1;
                    } catch(Throwable a5) {
                        a1 = a5;
                        break label1;
                    }
                }
                try {
                    if (i0 <= 0) {
                        sLogger.v("no kernel crash files");
                    } else {
                        String s2 = new StringBuilder().append(s).append(java.io.File.separator).append("kernel_crash_").append(System.currentTimeMillis()).append(".zip").toString();
                        com.navdy.service.library.util.LogUtils.zipStaging(a, s2, s0);
                        sLogger.v(new StringBuilder().append("kernel dump created:").append(s2).toString());
                    }
                    com.navdy.service.library.util.IOUtils.deleteDirectory(a, new java.io.File(s0));
                    break label0;
                } catch(Throwable a6) {
                    a1 = a6;
                }
            }
            try {
                android.util.Log.e(TAG, "createKernelCrashDump", a1);
            } catch(Throwable a7) {
                /*monexit(com.navdy.service.library.util.LogUtils.class)*/;
                throw a7;
            }
        }
        /*monexit(com.navdy.service.library.util.LogUtils.class)*/;
    }
    
    public static void dumpLog(android.content.Context a, String s) {
        label0: synchronized(com.navdy.service.library.util.LogUtils.class) {
            java.io.BufferedOutputStream a0 = null;
            Process a1 = null;
            java.io.InputStream a2 = null;
            java.io.BufferedInputStream a3 = null;
            Throwable a4 = null;
            if (new java.io.File(s).exists()) {
                com.navdy.service.library.util.IOUtils.deleteFile(a, s);
            }
            byte[] a5 = new byte[32768];
            java.io.FileOutputStream a6 = new java.io.FileOutputStream(s);
            label2: {
                Throwable a7 = null;
                label4: {
                    label5: {
                        try {
                            a0 = new java.io.BufferedOutputStream((java.io.OutputStream)a6, 32768);
                            break label5;
                        } catch(Throwable a8) {
                            a7 = a8;
                        }
                        a1 = null;
                        a0 = null;
                        a2 = null;
                        a3 = null;
                        break label4;
                    }
                    label3: {
                        try {
                            a2 = null;
                            a1 = null;
                            Runtime a9 = Runtime.getRuntime();
                            String[] a10 = new String[4];
                            a10[0] = "logcat";
                            a10[1] = "-d";
                            a10[2] = "-v";
                            a10[3] = "time";
                            a1 = a9.exec(a10);
                            a2 = null;
                            a2 = a1.getInputStream();
                            a3 = new java.io.BufferedInputStream(a2);
                            break label3;
                        } catch(Throwable a11) {
                            a7 = a11;
                        }
                        a3 = null;
                        break label4;
                    }
                    try {
                        while(true) {
                            int i = a3.read(a5);
                            if (i <= 0) {
                                break;
                            }
                            a0.write(a5, 0, i);
                        }
                        a0.flush();
                        break label2;
                    } catch(Throwable a12) {
                        a7 = a12;
                    }
                }
                com.navdy.service.library.util.IOUtils.fileSync(a6);
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a3);
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a6);
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a2);
                label1: {
                    Throwable a13 = null;
                    if (a1 == null) {
                        break label1;
                    }
                    try {
                        a1.destroy();
                        break label1;
                    } catch(Throwable a14) {
                        a13 = a14;
                    }
                    sLogger.e(a13);
                }
                throw a7;
            }
            com.navdy.service.library.util.IOUtils.fileSync(a6);
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a3);
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a6);
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a2);
            if (a1 == null) {
                break label0;
            }
            try {
                a1.destroy();
                break label0;
            } catch(Throwable a15) {
                a4 = a15;
            }
            sLogger.e(a4);
        }
        /*monexit(com.navdy.service.library.util.LogUtils.class)*/;
    }
    
    private static String getSystemLog() {
        String s = null;
        String s0 = new StringBuilder().append(android.os.Environment.getExternalStorageDirectory().getAbsolutePath()).append(java.io.File.separator).append(".logs").toString();
        String s1 = new StringBuilder().append(s0).append(java.io.File.separator).append("a.log").toString();
        try {
            s = com.navdy.service.library.util.IOUtils.convertFileToString(s1);
        } catch(java.io.IOException ignoredException) {
            android.util.Log.i(TAG, "Failed to grab system log");
            s = null;
        }
        return s;
    }
    
    public static String systemLogStr(int i, String s) {
        String s0 = com.navdy.service.library.util.LogUtils.getSystemLog();
        if (s0 != null) {
            int i0 = s0.length();
            if (s != null) {
                int i1 = s.length();
                int i2 = s0.lastIndexOf(s);
                if (i2 != -1) {
                    i0 = Math.min(i2 + i1 + 10, i0);
                }
            }
            s0 = s0.substring(Math.max(i0 - i, 0), i0);
        }
        return s0;
    }
    
    private static void writeCrashInfo(java.io.FileOutputStream a, Throwable a0, String s, Thread a1, boolean b) {
        Runtime a2 = Runtime.getRuntime();
        java.io.BufferedOutputStream a3 = new java.io.BufferedOutputStream((java.io.OutputStream)a);
        a3.write(new StringBuilder().append("process: ").append(s).toString().getBytes());
        a3.write(CRLF);
        a3.write(new StringBuilder().append("time: ").append(java.util.Calendar.getInstance().getTime().toString()).toString().getBytes());
        a3.write(CRLF);
        a3.write(new StringBuilder().append("serial: ").append(android.os.Build.SERIAL).toString().getBytes());
        a3.write(CRLF);
        a3.write(new StringBuilder().append("device: ").append(android.os.Build.DEVICE).toString().getBytes());
        a3.write(CRLF);
        a3.write(new StringBuilder().append("model: ").append(android.os.Build.MODEL).toString().getBytes());
        a3.write(CRLF);
        a3.write(new StringBuilder().append("hw: ").append(android.os.Build.HARDWARE).toString().getBytes());
        a3.write(CRLF);
        a3.write(new StringBuilder().append("id: ").append(android.os.Build.ID).toString().getBytes());
        a3.write(CRLF);
        a3.write(new StringBuilder().append("version: ").append(android.os.Build.VERSION.SDK_INT).toString().getBytes());
        a3.write(CRLF);
        a3.write(new StringBuilder().append("bootloader: ").append(android.os.Build.BOOTLOADER).toString().getBytes());
        a3.write(CRLF);
        a3.write(new StringBuilder().append("board: ").append(android.os.Build.BOARD).toString().getBytes());
        a3.write(CRLF);
        a3.write(new StringBuilder().append("brand: ").append(android.os.Build.BRAND).toString().getBytes());
        a3.write(CRLF);
        a3.write(new StringBuilder().append("manufacturer: ").append(android.os.Build.MANUFACTURER).toString().getBytes());
        a3.write(CRLF);
        a3.write(new StringBuilder().append("cpu_abi: ").append(android.os.Build.CPU_ABI).toString().getBytes());
        a3.write(CRLF);
        a3.write(new StringBuilder().append("cpu_abi2: ").append(android.os.Build.CPU_ABI2).toString().getBytes());
        a3.write(CRLF);
        a3.write(new StringBuilder().append("processors: ").append(a2.availableProcessors()).toString().getBytes());
        a3.write(CRLF);
        a3.write(CRLF);
        if (b) {
            a3.write("------ native crash -----".getBytes());
        } else {
            a3.write("------ crash -----".getBytes());
        }
        a3.write(CRLF);
        a3.write(new StringBuilder().append("thread name crashed:").append(a1.getName()).toString().getBytes());
        a3.write(CRLF);
        a3.write(CRLF);
        java.io.StringWriter a4 = new java.io.StringWriter();
        com.navdy.service.library.log.FastPrintWriter a5 = new com.navdy.service.library.log.FastPrintWriter((java.io.Writer)a4, false, 256);
        a0.printStackTrace((java.io.PrintWriter)a5);
        ((java.io.PrintWriter)a5).flush();
        a3.write(a4.toString().getBytes());
        a3.write(CRLF);
        a3.write(CRLF);
        a3.write("------ state -----".getBytes());
        a3.write(CRLF);
        a3.write(new StringBuilder().append("total:").append(a2.totalMemory()).toString().getBytes());
        a3.write(CRLF);
        a3.write(new StringBuilder().append("free:").append(a2.freeMemory()).toString().getBytes());
        a3.write(CRLF);
        a3.write(new StringBuilder().append("max:").append(a2.maxMemory()).toString().getBytes());
        a3.write(CRLF);
        a3.write(new StringBuilder().append("native heap size: ").append(android.os.Debug.getNativeHeapSize()).toString().getBytes());
        a3.write(CRLF);
        a3.write(new StringBuilder().append("native heap allocated: ").append(android.os.Debug.getNativeHeapAllocatedSize()).toString().getBytes());
        a3.write(CRLF);
        a3.write(new StringBuilder().append("native heap free: ").append(android.os.Debug.getNativeHeapFreeSize()).toString().getBytes());
        a3.write(CRLF);
        a3.write(CRLF);
        java.util.Map a6 = Thread.getAllStackTraces();
        a3.write(new StringBuilder().append("------ thread stackstraces count = ").append(a6.size()).append(" -----").toString().getBytes());
        a3.write(CRLF);
        a3.write(CRLF);
        Object a7 = a6.entrySet().iterator();
        while(((java.util.Iterator)a7).hasNext()) {
            Object a8 = ((java.util.Iterator)a7).next();
            Thread a9 = (Thread)((java.util.Map.Entry)a8).getKey();
            a3.write(new StringBuilder().append("\"").append(a9.getName()).append("\"").toString().getBytes());
            a3.write(CRLF);
            a3.write(new StringBuilder().append("    Thread.State: ").append(a9.getState()).toString().getBytes());
            a3.write(CRLF);
            StackTraceElement[] a10 = (StackTraceElement[])((java.util.Map.Entry)a8).getValue();
            int i = a10.length;
            int i0 = 0;
            while(i0 < i) {
                StackTraceElement a11 = a10[i0];
                a3.write(new StringBuilder().append("       at ").append(a11).toString().getBytes());
                a3.write(CRLF);
                i0 = i0 + 1;
            }
            a3.write(CRLF);
            a3.write(CRLF);
        }
        a3.flush();
    }
    
    public static void zipStaging(android.content.Context a, String s, String s0) {
        java.io.FileOutputStream a0 = null;
        Throwable a1 = null;
        java.io.FileInputStream a2 = null;
        java.util.zip.ZipOutputStream a3 = null;
        android.util.Log.i(TAG, new StringBuilder().append("zip started:").append(s).toString());
        label0: {
            long j = 0L;
            byte[] a4 = null;
            label5: {
                try {
                    j = android.os.SystemClock.elapsedRealtime();
                    a4 = new byte[16384];
                    a0 = new java.io.FileOutputStream(s);
                    break label5;
                } catch(Throwable a5) {
                    a1 = a5;
                }
                a0 = null;
                a2 = null;
                a3 = null;
                break label0;
            }
            label4: {
                try {
                    a3 = new java.util.zip.ZipOutputStream((java.io.OutputStream)a0);
                    break label4;
                } catch(Throwable a6) {
                    a1 = a6;
                }
                a2 = null;
                a3 = null;
                break label0;
            }
            label3: try {
                a2 = null;
                a2 = null;
                java.io.File a7 = new java.io.File(s0);
                a2 = null;
                java.io.File[] a8 = a7.listFiles();
                label1: {
                    label2: if (a8 != null) {
                        int i = 0;
                        while(true) {
                            try {
                                if (i >= a8.length) {
                                    break label2;
                                }
                                a2 = new java.io.FileInputStream(a8[i]);
                            } catch(Throwable a9) {
                                a1 = a9;
                                break label1;
                            }
                            a3.putNextEntry(new java.util.zip.ZipEntry(a8[i].getName()));
                            while(true) {
                                int i0 = a2.read(a4);
                                if (i0 <= 0) {
                                    a3.closeEntry();
                                    com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a2);
                                    i = i + 1;
                                    break;
                                } else {
                                    a3.write(a4, 0, i0);
                                }
                            }
                        }
                    }
                    a2 = null;
                    long j0 = android.os.SystemClock.elapsedRealtime();
                    android.util.Log.i(TAG, new StringBuilder().append("zip finished:").append(s).append(" time:").append(j0 - j).toString());
                    break label3;
                }
                a2 = null;
                break label0;
            } catch(Throwable a10) {
                a1 = a10;
                break label0;
            }
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a3);
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)null);
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
            return;
        }
        try {
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
            a0 = null;
            com.navdy.service.library.util.IOUtils.deleteFile(a, s);
            String s1 = TAG;
            a0 = null;
            android.util.Log.i(s1, "zip error", a1);
            a0 = null;
            throw a1;
        } catch(Throwable a11) {
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a3);
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a2);
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
            throw a11;
        }
    }
}
