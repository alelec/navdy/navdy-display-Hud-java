package com.navdy.service.library.network.http.services;

class JiraClient$2 implements okhttp3.Callback {
    final com.navdy.service.library.network.http.services.JiraClient this$0;
    final com.navdy.service.library.network.http.services.JiraClient$ResultCallback val$callback;
    final String val$user;
    
    JiraClient$2(com.navdy.service.library.network.http.services.JiraClient a, com.navdy.service.library.network.http.services.JiraClient$ResultCallback a0, String s) {
        super();
        this.this$0 = a;
        this.val$callback = a0;
        this.val$user = s;
    }
    
    public void onFailure(okhttp3.Call a, java.io.IOException a0) {
        com.navdy.service.library.network.http.services.JiraClient.access$000().e("onFailure ", (Throwable)a0);
        if (this.val$callback != null) {
            this.val$callback.onError((Throwable)a0);
        }
    }
    
    public void onResponse(okhttp3.Call a, okhttp3.Response a0) {
        label0: try {
            int i = a0.code();
            label1: {
                label2: {
                    if (i == 200) {
                        break label2;
                    }
                    if (i != 204) {
                        break label1;
                    }
                }
                if (this.val$callback == null) {
                    break label0;
                }
                this.val$callback.onSuccess(this.val$user);
                break label0;
            }
            String s = a0.body().string();
            com.navdy.service.library.network.http.services.JiraClient.access$000().d(new StringBuilder().append("Response data ").append(s).toString());
            if (this.val$callback != null) {
                this.val$callback.onError((Throwable)new java.io.IOException(new StringBuilder().append("Jira request failed with ").append(i).toString()));
            }
        } catch(Throwable a1) {
            com.navdy.service.library.network.http.services.JiraClient.access$000().e("Exception paring response ", a1);
            if (this.val$callback != null) {
                this.val$callback.onError(a1);
            }
        }
    }
}
