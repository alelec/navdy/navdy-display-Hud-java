package com.navdy.service.library.network.http.services;

abstract public interface JiraClient$ResultCallback {
    abstract public void onError(Throwable arg);
    
    
    abstract public void onSuccess(Object arg);
}
