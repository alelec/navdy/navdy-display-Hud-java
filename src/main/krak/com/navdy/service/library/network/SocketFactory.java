package com.navdy.service.library.network;

abstract public interface SocketFactory {
    abstract public com.navdy.service.library.network.SocketAdapter build();
}
