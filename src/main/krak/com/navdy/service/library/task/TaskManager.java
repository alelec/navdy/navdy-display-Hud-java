package com.navdy.service.library.task;

final public class TaskManager {
    final private static int INITIAL_CAPACITY = 10;
    final private static boolean VERBOSE = false;
    private static java.util.concurrent.atomic.AtomicLong orderCounter;
    private static com.navdy.service.library.log.Logger sLogger;
    final private static java.util.Comparator sPriorityComparator;
    final private static com.navdy.service.library.task.TaskManager sSingleton;
    final private static java.util.concurrent.atomic.AtomicInteger threadNameCounter;
    final private android.util.SparseArray executors;
    private boolean initialized;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.task.TaskManager.class);
        orderCounter = new java.util.concurrent.atomic.AtomicLong(1L);
        threadNameCounter = new java.util.concurrent.atomic.AtomicInteger(0);
        sPriorityComparator = (java.util.Comparator)new com.navdy.service.library.task.TaskManager$PriorityTaskComparator((com.navdy.service.library.task.TaskManager$1)null);
        sSingleton = new com.navdy.service.library.task.TaskManager();
    }
    
    private TaskManager() {
        this.executors = new android.util.SparseArray();
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    static java.util.concurrent.atomic.AtomicInteger access$200() {
        return threadNameCounter;
    }
    
    private java.util.concurrent.ExecutorService createExecutor(int i) {
        return (java.util.concurrent.ExecutorService)new com.navdy.service.library.task.TaskManager$TaskManagerExecutor(i, i, 60L, java.util.concurrent.TimeUnit.SECONDS, (java.util.concurrent.BlockingQueue)new java.util.concurrent.PriorityBlockingQueue(10, sPriorityComparator), (java.util.concurrent.ThreadFactory)new com.navdy.service.library.task.TaskManager$1(this));
    }
    
    public static com.navdy.service.library.task.TaskManager getInstance() {
        return sSingleton;
    }
    
    public void addTaskQueue(int i, int i0) {
        if (this.initialized) {
            throw new IllegalStateException("already initialized");
        }
        if (this.executors.get(i) != null) {
            throw new IllegalArgumentException("already exists");
        }
        this.executors.put(i, this.createExecutor(i0));
    }
    
    public java.util.concurrent.Future execute(Runnable a, int i) {
        return this.execute(a, i, com.navdy.service.library.task.TaskManager$TaskPriority.NORMAL);
    }
    
    public java.util.concurrent.Future execute(Runnable a, int i, com.navdy.service.library.task.TaskManager$TaskPriority a0) {
        java.util.concurrent.Future a1 = null;
        if (this.initialized) {
            if (a == null) {
                throw new IllegalArgumentException();
            }
            a1 = ((java.util.concurrent.ExecutorService)this.executors.get(i)).submit((Runnable)new com.navdy.service.library.task.TaskManager$PriorityRunnable(a, a0, orderCounter.getAndIncrement()));
        } else {
            sLogger.w("Task manager not initialized", new Throwable());
            a1 = null;
        }
        return a1;
    }
    
    public java.util.concurrent.ExecutorService getExecutor(int i) {
        return (java.util.concurrent.ExecutorService)this.executors.get(i);
    }
    
    public void init() {
        if (this.initialized) {
            throw new IllegalStateException("already initialized");
        }
        this.initialized = true;
    }
}
