package com.navdy.service.library.events.ui;

final public class ShowScreen$Builder extends com.squareup.wire.Message.Builder {
    public java.util.List arguments;
    public com.navdy.service.library.events.ui.Screen screen;
    
    public ShowScreen$Builder() {
    }
    
    public ShowScreen$Builder(com.navdy.service.library.events.ui.ShowScreen a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.screen = a.screen;
            this.arguments = com.navdy.service.library.events.ui.ShowScreen.access$000(a.arguments);
        }
    }
    
    public com.navdy.service.library.events.ui.ShowScreen$Builder arguments(java.util.List a) {
        this.arguments = com.navdy.service.library.events.ui.ShowScreen$Builder.checkForNulls(a);
        return this;
    }
    
    public com.navdy.service.library.events.ui.ShowScreen build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.ui.ShowScreen(this, (com.navdy.service.library.events.ui.ShowScreen$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.ui.ShowScreen$Builder screen(com.navdy.service.library.events.ui.Screen a) {
        this.screen = a;
        return this;
    }
}
