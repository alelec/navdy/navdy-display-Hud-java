package com.navdy.service.library.events.location;

final public class LatLong$Builder extends com.squareup.wire.Message.Builder {
    public Double latitude;
    public Double longitude;
    
    public LatLong$Builder() {
    }
    
    public LatLong$Builder(com.navdy.service.library.events.location.LatLong a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.latitude = a.latitude;
            this.longitude = a.longitude;
        }
    }
    
    public com.navdy.service.library.events.location.LatLong build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.location.LatLong(this, (com.navdy.service.library.events.location.LatLong$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.location.LatLong$Builder latitude(Double a) {
        this.latitude = a;
        return this;
    }
    
    public com.navdy.service.library.events.location.LatLong$Builder longitude(Double a) {
        this.longitude = a;
        return this;
    }
}
