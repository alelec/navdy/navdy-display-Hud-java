package com.navdy.service.library.events.file;

final public class PreviewFileRequest$Builder extends com.squareup.wire.Message.Builder {
    public String filename;
    
    public PreviewFileRequest$Builder() {
    }
    
    public PreviewFileRequest$Builder(com.navdy.service.library.events.file.PreviewFileRequest a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.filename = a.filename;
        }
    }
    
    public com.navdy.service.library.events.file.PreviewFileRequest build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.file.PreviewFileRequest(this, (com.navdy.service.library.events.file.PreviewFileRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.file.PreviewFileRequest$Builder filename(String s) {
        this.filename = s;
        return this;
    }
}
