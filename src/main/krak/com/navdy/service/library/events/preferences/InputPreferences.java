package com.navdy.service.library.events.preferences;

final public class InputPreferences extends com.squareup.wire.Message {
    final public static com.navdy.service.library.events.preferences.InputPreferences$DialSensitivity DEFAULT_DIAL_SENSITIVITY;
    final public static Long DEFAULT_SERIAL_NUMBER;
    final public static Boolean DEFAULT_USE_GESTURES;
    final private static long serialVersionUID = 0L;
    final public com.navdy.service.library.events.preferences.InputPreferences$DialSensitivity dial_sensitivity;
    final public Long serial_number;
    final public Boolean use_gestures;
    
    static {
        DEFAULT_SERIAL_NUMBER = Long.valueOf(0L);
        DEFAULT_USE_GESTURES = Boolean.valueOf(true);
        DEFAULT_DIAL_SENSITIVITY = com.navdy.service.library.events.preferences.InputPreferences$DialSensitivity.DIAL_SENSITIVITY_STANDARD;
    }
    
    private InputPreferences(com.navdy.service.library.events.preferences.InputPreferences$Builder a) {
        this(a.serial_number, a.use_gestures, a.dial_sensitivity);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    InputPreferences(com.navdy.service.library.events.preferences.InputPreferences$Builder a, com.navdy.service.library.events.preferences.InputPreferences$1 a0) {
        this(a);
    }
    
    public InputPreferences(Long a, Boolean a0, com.navdy.service.library.events.preferences.InputPreferences$DialSensitivity a1) {
        this.serial_number = a;
        this.use_gestures = a0;
        this.dial_sensitivity = a1;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.preferences.InputPreferences) {
                com.navdy.service.library.events.preferences.InputPreferences a0 = (com.navdy.service.library.events.preferences.InputPreferences)a;
                boolean b0 = this.equals(this.serial_number, a0.serial_number);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.use_gestures, a0.use_gestures)) {
                        break label1;
                    }
                    if (this.equals(this.dial_sensitivity, a0.dial_sensitivity)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((this.serial_number == null) ? 0 : this.serial_number.hashCode()) * 37 + ((this.use_gestures == null) ? 0 : this.use_gestures.hashCode())) * 37 + ((this.dial_sensitivity == null) ? 0 : this.dial_sensitivity.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
