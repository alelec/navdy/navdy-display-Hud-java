package com.navdy.service.library.events.audio;

final public class VoiceAssistResponse extends com.squareup.wire.Message {
    final public static com.navdy.service.library.events.audio.VoiceAssistResponse$VoiceAssistState DEFAULT_STATE;
    final private static long serialVersionUID = 0L;
    final public com.navdy.service.library.events.audio.VoiceAssistResponse$VoiceAssistState state;
    
    static {
        DEFAULT_STATE = com.navdy.service.library.events.audio.VoiceAssistResponse$VoiceAssistState.VOICE_ASSIST_AVAILABLE;
    }
    
    private VoiceAssistResponse(com.navdy.service.library.events.audio.VoiceAssistResponse$Builder a) {
        this(a.state);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    VoiceAssistResponse(com.navdy.service.library.events.audio.VoiceAssistResponse$Builder a, com.navdy.service.library.events.audio.VoiceAssistResponse$1 a0) {
        this(a);
    }
    
    public VoiceAssistResponse(com.navdy.service.library.events.audio.VoiceAssistResponse$VoiceAssistState a) {
        this.state = a;
    }
    
    public boolean equals(Object a) {
        return a == this || a instanceof com.navdy.service.library.events.audio.VoiceAssistResponse && this.equals(this.state, ((com.navdy.service.library.events.audio.VoiceAssistResponse)a).state);
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (this.state == null) ? 0 : this.state.hashCode();
            this.hashCode = i;
        }
        return i;
    }
}
