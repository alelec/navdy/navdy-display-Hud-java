package com.navdy.service.library.events.audio;

final public class MusicCollectionResponse extends com.squareup.wire.Message {
    final public static java.util.List DEFAULT_CHARACTERMAP;
    final public static java.util.List DEFAULT_MUSICCOLLECTIONS;
    final public static java.util.List DEFAULT_MUSICTRACKS;
    final private static long serialVersionUID = 0L;
    final public java.util.List characterMap;
    final public com.navdy.service.library.events.audio.MusicCollectionInfo collectionInfo;
    final public java.util.List musicCollections;
    final public java.util.List musicTracks;
    
    static {
        DEFAULT_MUSICCOLLECTIONS = java.util.Collections.emptyList();
        DEFAULT_MUSICTRACKS = java.util.Collections.emptyList();
        DEFAULT_CHARACTERMAP = java.util.Collections.emptyList();
    }
    
    public MusicCollectionResponse(com.navdy.service.library.events.audio.MusicCollectionInfo a, java.util.List a0, java.util.List a1, java.util.List a2) {
        this.collectionInfo = a;
        this.musicCollections = com.navdy.service.library.events.audio.MusicCollectionResponse.immutableCopyOf(a0);
        this.musicTracks = com.navdy.service.library.events.audio.MusicCollectionResponse.immutableCopyOf(a1);
        this.characterMap = com.navdy.service.library.events.audio.MusicCollectionResponse.immutableCopyOf(a2);
    }
    
    private MusicCollectionResponse(com.navdy.service.library.events.audio.MusicCollectionResponse$Builder a) {
        this(a.collectionInfo, a.musicCollections, a.musicTracks, a.characterMap);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    MusicCollectionResponse(com.navdy.service.library.events.audio.MusicCollectionResponse$Builder a, com.navdy.service.library.events.audio.MusicCollectionResponse$1 a0) {
        this(a);
    }
    
    static java.util.List access$000(java.util.List a) {
        return com.navdy.service.library.events.audio.MusicCollectionResponse.copyOf(a);
    }
    
    static java.util.List access$100(java.util.List a) {
        return com.navdy.service.library.events.audio.MusicCollectionResponse.copyOf(a);
    }
    
    static java.util.List access$200(java.util.List a) {
        return com.navdy.service.library.events.audio.MusicCollectionResponse.copyOf(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.audio.MusicCollectionResponse) {
                com.navdy.service.library.events.audio.MusicCollectionResponse a0 = (com.navdy.service.library.events.audio.MusicCollectionResponse)a;
                boolean b0 = this.equals(this.collectionInfo, a0.collectionInfo);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.musicCollections, a0.musicCollections)) {
                        break label1;
                    }
                    if (!this.equals(this.musicTracks, a0.musicTracks)) {
                        break label1;
                    }
                    if (this.equals(this.characterMap, a0.characterMap)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((((this.collectionInfo == null) ? 0 : this.collectionInfo.hashCode()) * 37 + ((this.musicCollections == null) ? 1 : this.musicCollections.hashCode())) * 37 + ((this.musicTracks == null) ? 1 : this.musicTracks.hashCode())) * 37 + ((this.characterMap == null) ? 1 : this.characterMap.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
