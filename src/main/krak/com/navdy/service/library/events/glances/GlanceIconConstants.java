package com.navdy.service.library.events.glances;

final public class GlanceIconConstants extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.glances.GlanceIconConstants[] $VALUES;
    final public static com.navdy.service.library.events.glances.GlanceIconConstants GLANCE_ICON_MESSAGE_SIDE_BLUE;
    final public static com.navdy.service.library.events.glances.GlanceIconConstants GLANCE_ICON_NAVDY_MAIN;
    final private int value;
    
    static {
        GLANCE_ICON_NAVDY_MAIN = new com.navdy.service.library.events.glances.GlanceIconConstants("GLANCE_ICON_NAVDY_MAIN", 0, 0);
        GLANCE_ICON_MESSAGE_SIDE_BLUE = new com.navdy.service.library.events.glances.GlanceIconConstants("GLANCE_ICON_MESSAGE_SIDE_BLUE", 1, 1);
        com.navdy.service.library.events.glances.GlanceIconConstants[] a = new com.navdy.service.library.events.glances.GlanceIconConstants[2];
        a[0] = GLANCE_ICON_NAVDY_MAIN;
        a[1] = GLANCE_ICON_MESSAGE_SIDE_BLUE;
        $VALUES = a;
    }
    
    private GlanceIconConstants(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.glances.GlanceIconConstants valueOf(String s) {
        return (com.navdy.service.library.events.glances.GlanceIconConstants)Enum.valueOf(com.navdy.service.library.events.glances.GlanceIconConstants.class, s);
    }
    
    public static com.navdy.service.library.events.glances.GlanceIconConstants[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
