package com.navdy.service.library.events.places;

final public class PlaceTypeSearchRequest extends com.squareup.wire.Message {
    final public static com.navdy.service.library.events.places.PlaceType DEFAULT_PLACE_TYPE;
    final public static String DEFAULT_REQUEST_ID = "";
    final private static long serialVersionUID = 0L;
    final public com.navdy.service.library.events.places.PlaceType place_type;
    final public String request_id;
    
    static {
        DEFAULT_PLACE_TYPE = com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_UNKNOWN;
    }
    
    private PlaceTypeSearchRequest(com.navdy.service.library.events.places.PlaceTypeSearchRequest$Builder a) {
        this(a.request_id, a.place_type);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    PlaceTypeSearchRequest(com.navdy.service.library.events.places.PlaceTypeSearchRequest$Builder a, com.navdy.service.library.events.places.PlaceTypeSearchRequest$1 a0) {
        this(a);
    }
    
    public PlaceTypeSearchRequest(String s, com.navdy.service.library.events.places.PlaceType a) {
        this.request_id = s;
        this.place_type = a;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.places.PlaceTypeSearchRequest) {
                com.navdy.service.library.events.places.PlaceTypeSearchRequest a0 = (com.navdy.service.library.events.places.PlaceTypeSearchRequest)a;
                boolean b0 = this.equals(this.request_id, a0.request_id);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (this.equals(this.place_type, a0.place_type)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((this.request_id == null) ? 0 : this.request_id.hashCode()) * 37 + ((this.place_type == null) ? 0 : this.place_type.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
