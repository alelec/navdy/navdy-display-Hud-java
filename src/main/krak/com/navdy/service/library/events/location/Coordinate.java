package com.navdy.service.library.events.location;

final public class Coordinate extends com.squareup.wire.Message {
    final public static Float DEFAULT_ACCURACY;
    final public static Double DEFAULT_ALTITUDE;
    final public static Float DEFAULT_BEARING;
    final public static Double DEFAULT_LATITUDE;
    final public static Double DEFAULT_LONGITUDE;
    final public static String DEFAULT_PROVIDER = "";
    final public static Float DEFAULT_SPEED;
    final public static Long DEFAULT_TIMESTAMP;
    final private static long serialVersionUID = 0L;
    final public Float accuracy;
    final public Double altitude;
    final public Float bearing;
    final public Double latitude;
    final public Double longitude;
    final public String provider;
    final public Float speed;
    final public Long timestamp;
    
    static {
        DEFAULT_LATITUDE = Double.valueOf(0.0);
        DEFAULT_LONGITUDE = Double.valueOf(0.0);
        DEFAULT_ACCURACY = Float.valueOf(0.0f);
        DEFAULT_ALTITUDE = Double.valueOf(0.0);
        DEFAULT_BEARING = Float.valueOf(0.0f);
        DEFAULT_SPEED = Float.valueOf(0.0f);
        DEFAULT_TIMESTAMP = Long.valueOf(0L);
    }
    
    private Coordinate(com.navdy.service.library.events.location.Coordinate$Builder a) {
        this(a.latitude, a.longitude, a.accuracy, a.altitude, a.bearing, a.speed, a.timestamp, a.provider);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    Coordinate(com.navdy.service.library.events.location.Coordinate$Builder a, com.navdy.service.library.events.location.Coordinate$1 a0) {
        this(a);
    }
    
    public Coordinate(Double a, Double a0, Float a1, Double a2, Float a3, Float a4, Long a5, String s) {
        this.latitude = a;
        this.longitude = a0;
        this.accuracy = a1;
        this.altitude = a2;
        this.bearing = a3;
        this.speed = a4;
        this.timestamp = a5;
        this.provider = s;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.location.Coordinate) {
                com.navdy.service.library.events.location.Coordinate a0 = (com.navdy.service.library.events.location.Coordinate)a;
                boolean b0 = this.equals(this.latitude, a0.latitude);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.longitude, a0.longitude)) {
                        break label1;
                    }
                    if (!this.equals(this.accuracy, a0.accuracy)) {
                        break label1;
                    }
                    if (!this.equals(this.altitude, a0.altitude)) {
                        break label1;
                    }
                    if (!this.equals(this.bearing, a0.bearing)) {
                        break label1;
                    }
                    if (!this.equals(this.speed, a0.speed)) {
                        break label1;
                    }
                    if (!this.equals(this.timestamp, a0.timestamp)) {
                        break label1;
                    }
                    if (this.equals(this.provider, a0.provider)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((((((((this.latitude == null) ? 0 : this.latitude.hashCode()) * 37 + ((this.longitude == null) ? 0 : this.longitude.hashCode())) * 37 + ((this.accuracy == null) ? 0 : this.accuracy.hashCode())) * 37 + ((this.altitude == null) ? 0 : this.altitude.hashCode())) * 37 + ((this.bearing == null) ? 0 : this.bearing.hashCode())) * 37 + ((this.speed == null) ? 0 : this.speed.hashCode())) * 37 + ((this.timestamp == null) ? 0 : this.timestamp.hashCode())) * 37 + ((this.provider == null) ? 0 : this.provider.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
