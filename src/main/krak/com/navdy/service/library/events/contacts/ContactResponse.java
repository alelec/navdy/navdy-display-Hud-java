package com.navdy.service.library.events.contacts;

final public class ContactResponse extends com.squareup.wire.Message {
    final public static java.util.List DEFAULT_CONTACTS;
    final public static String DEFAULT_IDENTIFIER = "";
    final public static com.navdy.service.library.events.RequestStatus DEFAULT_STATUS;
    final public static String DEFAULT_STATUSDETAIL = "";
    final private static long serialVersionUID = 0L;
    final public java.util.List contacts;
    final public String identifier;
    final public com.navdy.service.library.events.RequestStatus status;
    final public String statusDetail;
    
    static {
        DEFAULT_STATUS = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
        DEFAULT_CONTACTS = java.util.Collections.emptyList();
    }
    
    public ContactResponse(com.navdy.service.library.events.RequestStatus a, String s, String s0, java.util.List a0) {
        this.status = a;
        this.statusDetail = s;
        this.identifier = s0;
        this.contacts = com.navdy.service.library.events.contacts.ContactResponse.immutableCopyOf(a0);
    }
    
    private ContactResponse(com.navdy.service.library.events.contacts.ContactResponse$Builder a) {
        this(a.status, a.statusDetail, a.identifier, a.contacts);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    ContactResponse(com.navdy.service.library.events.contacts.ContactResponse$Builder a, com.navdy.service.library.events.contacts.ContactResponse$1 a0) {
        this(a);
    }
    
    static java.util.List access$000(java.util.List a) {
        return com.navdy.service.library.events.contacts.ContactResponse.copyOf(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.contacts.ContactResponse) {
                com.navdy.service.library.events.contacts.ContactResponse a0 = (com.navdy.service.library.events.contacts.ContactResponse)a;
                boolean b0 = this.equals(this.status, a0.status);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.statusDetail, a0.statusDetail)) {
                        break label1;
                    }
                    if (!this.equals(this.identifier, a0.identifier)) {
                        break label1;
                    }
                    if (this.equals(this.contacts, a0.contacts)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((((this.status == null) ? 0 : this.status.hashCode()) * 37 + ((this.statusDetail == null) ? 0 : this.statusDetail.hashCode())) * 37 + ((this.identifier == null) ? 0 : this.identifier.hashCode())) * 37 + ((this.contacts == null) ? 1 : this.contacts.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
