package com.navdy.service.library.events.calendars;

final public class CalendarEventUpdates$Builder extends com.squareup.wire.Message.Builder {
    public java.util.List calendar_events;
    
    public CalendarEventUpdates$Builder() {
    }
    
    public CalendarEventUpdates$Builder(com.navdy.service.library.events.calendars.CalendarEventUpdates a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.calendar_events = com.navdy.service.library.events.calendars.CalendarEventUpdates.access$000(a.calendar_events);
        }
    }
    
    public com.navdy.service.library.events.calendars.CalendarEventUpdates build() {
        return new com.navdy.service.library.events.calendars.CalendarEventUpdates(this, (com.navdy.service.library.events.calendars.CalendarEventUpdates$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.calendars.CalendarEventUpdates$Builder calendar_events(java.util.List a) {
        this.calendar_events = com.navdy.service.library.events.calendars.CalendarEventUpdates$Builder.checkForNulls(a);
        return this;
    }
}
