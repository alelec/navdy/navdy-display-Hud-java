package com.navdy.service.library.events.notification;

final public class NotificationsStatusRequest$Builder extends com.squareup.wire.Message.Builder {
    public com.navdy.service.library.events.notification.NotificationsState newState;
    public com.navdy.service.library.events.notification.ServiceType service;
    
    public NotificationsStatusRequest$Builder() {
    }
    
    public NotificationsStatusRequest$Builder(com.navdy.service.library.events.notification.NotificationsStatusRequest a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.newState = a.newState;
            this.service = a.service;
        }
    }
    
    public com.navdy.service.library.events.notification.NotificationsStatusRequest build() {
        return new com.navdy.service.library.events.notification.NotificationsStatusRequest(this, (com.navdy.service.library.events.notification.NotificationsStatusRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.notification.NotificationsStatusRequest$Builder newState(com.navdy.service.library.events.notification.NotificationsState a) {
        this.newState = a;
        return this;
    }
    
    public com.navdy.service.library.events.notification.NotificationsStatusRequest$Builder service(com.navdy.service.library.events.notification.ServiceType a) {
        this.service = a;
        return this;
    }
}
