package com.navdy.service.library.events.audio;

final public class MusicArtworkRequest extends com.squareup.wire.Message {
    final public static String DEFAULT_ALBUM = "";
    final public static String DEFAULT_AUTHOR = "";
    final public static String DEFAULT_COLLECTIONID = "";
    final public static com.navdy.service.library.events.audio.MusicCollectionSource DEFAULT_COLLECTIONSOURCE;
    final public static com.navdy.service.library.events.audio.MusicCollectionType DEFAULT_COLLECTIONTYPE;
    final public static String DEFAULT_NAME = "";
    final public static Integer DEFAULT_SIZE;
    final private static long serialVersionUID = 0L;
    final public String album;
    final public String author;
    final public String collectionId;
    final public com.navdy.service.library.events.audio.MusicCollectionSource collectionSource;
    final public com.navdy.service.library.events.audio.MusicCollectionType collectionType;
    final public String name;
    final public Integer size;
    
    static {
        DEFAULT_COLLECTIONSOURCE = com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN;
        DEFAULT_SIZE = Integer.valueOf(0);
        DEFAULT_COLLECTIONTYPE = com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_UNKNOWN;
    }
    
    private MusicArtworkRequest(com.navdy.service.library.events.audio.MusicArtworkRequest$Builder a) {
        this(a.collectionSource, a.name, a.album, a.author, a.size, a.collectionType, a.collectionId);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    MusicArtworkRequest(com.navdy.service.library.events.audio.MusicArtworkRequest$Builder a, com.navdy.service.library.events.audio.MusicArtworkRequest$1 a0) {
        this(a);
    }
    
    public MusicArtworkRequest(com.navdy.service.library.events.audio.MusicCollectionSource a, String s, String s0, String s1, Integer a0, com.navdy.service.library.events.audio.MusicCollectionType a1, String s2) {
        this.collectionSource = a;
        this.name = s;
        this.album = s0;
        this.author = s1;
        this.size = a0;
        this.collectionType = a1;
        this.collectionId = s2;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.audio.MusicArtworkRequest) {
                com.navdy.service.library.events.audio.MusicArtworkRequest a0 = (com.navdy.service.library.events.audio.MusicArtworkRequest)a;
                boolean b0 = this.equals(this.collectionSource, a0.collectionSource);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.name, a0.name)) {
                        break label1;
                    }
                    if (!this.equals(this.album, a0.album)) {
                        break label1;
                    }
                    if (!this.equals(this.author, a0.author)) {
                        break label1;
                    }
                    if (!this.equals(this.size, a0.size)) {
                        break label1;
                    }
                    if (!this.equals(this.collectionType, a0.collectionType)) {
                        break label1;
                    }
                    if (this.equals(this.collectionId, a0.collectionId)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((((((this.collectionSource == null) ? 0 : this.collectionSource.hashCode()) * 37 + ((this.name == null) ? 0 : this.name.hashCode())) * 37 + ((this.album == null) ? 0 : this.album.hashCode())) * 37 + ((this.author == null) ? 0 : this.author.hashCode())) * 37 + ((this.size == null) ? 0 : this.size.hashCode())) * 37 + ((this.collectionType == null) ? 0 : this.collectionType.hashCode())) * 37 + ((this.collectionId == null) ? 0 : this.collectionId.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
