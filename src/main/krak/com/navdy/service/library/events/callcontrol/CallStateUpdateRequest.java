package com.navdy.service.library.events.callcontrol;

final public class CallStateUpdateRequest extends com.squareup.wire.Message {
    final public static Boolean DEFAULT_START;
    final private static long serialVersionUID = 0L;
    final public Boolean start;
    
    static {
        DEFAULT_START = Boolean.valueOf(false);
    }
    
    private CallStateUpdateRequest(com.navdy.service.library.events.callcontrol.CallStateUpdateRequest$Builder a) {
        this(a.start);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    CallStateUpdateRequest(com.navdy.service.library.events.callcontrol.CallStateUpdateRequest$Builder a, com.navdy.service.library.events.callcontrol.CallStateUpdateRequest$1 a0) {
        this(a);
    }
    
    public CallStateUpdateRequest(Boolean a) {
        this.start = a;
    }
    
    public boolean equals(Object a) {
        return a == this || a instanceof com.navdy.service.library.events.callcontrol.CallStateUpdateRequest && this.equals(this.start, ((com.navdy.service.library.events.callcontrol.CallStateUpdateRequest)a).start);
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (this.start == null) ? 0 : this.start.hashCode();
            this.hashCode = i;
        }
        return i;
    }
}
