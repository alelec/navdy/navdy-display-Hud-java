package com.navdy.service.library.events.places;

final public class DestinationSelectedResponse extends com.squareup.wire.Message {
    final public static String DEFAULT_REQUEST_ID = "";
    final public static com.navdy.service.library.events.RequestStatus DEFAULT_REQUEST_STATUS;
    final private static long serialVersionUID = 0L;
    final public com.navdy.service.library.events.destination.Destination destination;
    final public String request_id;
    final public com.navdy.service.library.events.RequestStatus request_status;
    
    static {
        DEFAULT_REQUEST_STATUS = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
    }
    
    private DestinationSelectedResponse(com.navdy.service.library.events.places.DestinationSelectedResponse$Builder a) {
        this(a.request_id, a.request_status, a.destination);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    DestinationSelectedResponse(com.navdy.service.library.events.places.DestinationSelectedResponse$Builder a, com.navdy.service.library.events.places.DestinationSelectedResponse$1 a0) {
        this(a);
    }
    
    public DestinationSelectedResponse(String s, com.navdy.service.library.events.RequestStatus a, com.navdy.service.library.events.destination.Destination a0) {
        this.request_id = s;
        this.request_status = a;
        this.destination = a0;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.places.DestinationSelectedResponse) {
                com.navdy.service.library.events.places.DestinationSelectedResponse a0 = (com.navdy.service.library.events.places.DestinationSelectedResponse)a;
                boolean b0 = this.equals(this.request_id, a0.request_id);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.request_status, a0.request_status)) {
                        break label1;
                    }
                    if (this.equals(this.destination, a0.destination)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((this.request_id == null) ? 0 : this.request_id.hashCode()) * 37 + ((this.request_status == null) ? 0 : this.request_status.hashCode())) * 37 + ((this.destination == null) ? 0 : this.destination.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
