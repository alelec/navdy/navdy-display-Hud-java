package com.navdy.service.library.events.navigation;

final public class NavigationSessionResponse extends com.squareup.wire.Message {
    final public static com.navdy.service.library.events.navigation.NavigationSessionState DEFAULT_PENDINGSESSIONSTATE;
    final public static String DEFAULT_ROUTEID = "";
    final public static com.navdy.service.library.events.RequestStatus DEFAULT_STATUS;
    final public static String DEFAULT_STATUSDETAIL = "";
    final private static long serialVersionUID = 0L;
    final public com.navdy.service.library.events.navigation.NavigationSessionState pendingSessionState;
    final public String routeId;
    final public com.navdy.service.library.events.RequestStatus status;
    final public String statusDetail;
    
    static {
        DEFAULT_STATUS = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
        DEFAULT_PENDINGSESSIONSTATE = com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_ENGINE_NOT_READY;
    }
    
    public NavigationSessionResponse(com.navdy.service.library.events.RequestStatus a, String s, com.navdy.service.library.events.navigation.NavigationSessionState a0, String s0) {
        this.status = a;
        this.statusDetail = s;
        this.pendingSessionState = a0;
        this.routeId = s0;
    }
    
    private NavigationSessionResponse(com.navdy.service.library.events.navigation.NavigationSessionResponse$Builder a) {
        this(a.status, a.statusDetail, a.pendingSessionState, a.routeId);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    NavigationSessionResponse(com.navdy.service.library.events.navigation.NavigationSessionResponse$Builder a, com.navdy.service.library.events.navigation.NavigationSessionResponse$1 a0) {
        this(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.navigation.NavigationSessionResponse) {
                com.navdy.service.library.events.navigation.NavigationSessionResponse a0 = (com.navdy.service.library.events.navigation.NavigationSessionResponse)a;
                boolean b0 = this.equals(this.status, a0.status);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.statusDetail, a0.statusDetail)) {
                        break label1;
                    }
                    if (!this.equals(this.pendingSessionState, a0.pendingSessionState)) {
                        break label1;
                    }
                    if (this.equals(this.routeId, a0.routeId)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((((this.status == null) ? 0 : this.status.hashCode()) * 37 + ((this.statusDetail == null) ? 0 : this.statusDetail.hashCode())) * 37 + ((this.pendingSessionState == null) ? 0 : this.pendingSessionState.hashCode())) * 37 + ((this.routeId == null) ? 0 : this.routeId.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
