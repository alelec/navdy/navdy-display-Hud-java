package com.navdy.service.library.events.destination;

final public class Destination$FavoriteType extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.destination.Destination$FavoriteType[] $VALUES;
    final public static com.navdy.service.library.events.destination.Destination$FavoriteType FAVORITE_CONTACT;
    final public static com.navdy.service.library.events.destination.Destination$FavoriteType FAVORITE_CUSTOM;
    final public static com.navdy.service.library.events.destination.Destination$FavoriteType FAVORITE_HOME;
    final public static com.navdy.service.library.events.destination.Destination$FavoriteType FAVORITE_NONE;
    final public static com.navdy.service.library.events.destination.Destination$FavoriteType FAVORITE_WORK;
    final private int value;
    
    static {
        FAVORITE_NONE = new com.navdy.service.library.events.destination.Destination$FavoriteType("FAVORITE_NONE", 0, 0);
        FAVORITE_HOME = new com.navdy.service.library.events.destination.Destination$FavoriteType("FAVORITE_HOME", 1, 1);
        FAVORITE_WORK = new com.navdy.service.library.events.destination.Destination$FavoriteType("FAVORITE_WORK", 2, 2);
        FAVORITE_CONTACT = new com.navdy.service.library.events.destination.Destination$FavoriteType("FAVORITE_CONTACT", 3, 3);
        FAVORITE_CUSTOM = new com.navdy.service.library.events.destination.Destination$FavoriteType("FAVORITE_CUSTOM", 4, 10);
        com.navdy.service.library.events.destination.Destination$FavoriteType[] a = new com.navdy.service.library.events.destination.Destination$FavoriteType[5];
        a[0] = FAVORITE_NONE;
        a[1] = FAVORITE_HOME;
        a[2] = FAVORITE_WORK;
        a[3] = FAVORITE_CONTACT;
        a[4] = FAVORITE_CUSTOM;
        $VALUES = a;
    }
    
    private Destination$FavoriteType(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.destination.Destination$FavoriteType valueOf(String s) {
        return (com.navdy.service.library.events.destination.Destination$FavoriteType)Enum.valueOf(com.navdy.service.library.events.destination.Destination$FavoriteType.class, s);
    }
    
    public static com.navdy.service.library.events.destination.Destination$FavoriteType[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
