package com.navdy.service.library.events.settings;

final public class DateTimeConfiguration$Builder extends com.squareup.wire.Message.Builder {
    public com.navdy.service.library.events.settings.DateTimeConfiguration$Clock format;
    public Long timestamp;
    public String timezone;
    
    public DateTimeConfiguration$Builder() {
    }
    
    public DateTimeConfiguration$Builder(com.navdy.service.library.events.settings.DateTimeConfiguration a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.timestamp = a.timestamp;
            this.timezone = a.timezone;
            this.format = a.format;
        }
    }
    
    public com.navdy.service.library.events.settings.DateTimeConfiguration build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.settings.DateTimeConfiguration(this, (com.navdy.service.library.events.settings.DateTimeConfiguration$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.settings.DateTimeConfiguration$Builder format(com.navdy.service.library.events.settings.DateTimeConfiguration$Clock a) {
        this.format = a;
        return this;
    }
    
    public com.navdy.service.library.events.settings.DateTimeConfiguration$Builder timestamp(Long a) {
        this.timestamp = a;
        return this;
    }
    
    public com.navdy.service.library.events.settings.DateTimeConfiguration$Builder timezone(String s) {
        this.timezone = s;
        return this;
    }
}
