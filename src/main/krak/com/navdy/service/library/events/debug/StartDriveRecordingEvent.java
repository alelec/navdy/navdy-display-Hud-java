package com.navdy.service.library.events.debug;

final public class StartDriveRecordingEvent extends com.squareup.wire.Message {
    final public static String DEFAULT_LABEL = "";
    final private static long serialVersionUID = 0L;
    final public String label;
    
    private StartDriveRecordingEvent(com.navdy.service.library.events.debug.StartDriveRecordingEvent$Builder a) {
        this(a.label);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    StartDriveRecordingEvent(com.navdy.service.library.events.debug.StartDriveRecordingEvent$Builder a, com.navdy.service.library.events.debug.StartDriveRecordingEvent$1 a0) {
        this(a);
    }
    
    public StartDriveRecordingEvent(String s) {
        this.label = s;
    }
    
    public boolean equals(Object a) {
        return a == this || a instanceof com.navdy.service.library.events.debug.StartDriveRecordingEvent && this.equals(this.label, ((com.navdy.service.library.events.debug.StartDriveRecordingEvent)a).label);
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (this.label == null) ? 0 : this.label.hashCode();
            this.hashCode = i;
        }
        return i;
    }
}
