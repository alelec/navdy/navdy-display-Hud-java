package com.navdy.service.library.events.preferences;

final public class DisplaySpeakerPreferences extends com.squareup.wire.Message {
    final public static Boolean DEFAULT_FEEDBACK_SOUND;
    final public static Boolean DEFAULT_MASTER_SOUND;
    final public static Boolean DEFAULT_NOTIFICATION_SOUND;
    final public static Long DEFAULT_SERIAL_NUMBER;
    final public static Integer DEFAULT_VOLUME_LEVEL;
    final private static long serialVersionUID = 0L;
    final public Boolean feedback_sound;
    final public Boolean master_sound;
    final public Boolean notification_sound;
    final public Long serial_number;
    final public Integer volume_level;
    
    static {
        DEFAULT_SERIAL_NUMBER = Long.valueOf(0L);
        DEFAULT_MASTER_SOUND = Boolean.valueOf(true);
        DEFAULT_VOLUME_LEVEL = Integer.valueOf(50);
        DEFAULT_FEEDBACK_SOUND = Boolean.valueOf(true);
        DEFAULT_NOTIFICATION_SOUND = Boolean.valueOf(true);
    }
    
    private DisplaySpeakerPreferences(com.navdy.service.library.events.preferences.DisplaySpeakerPreferences$Builder a) {
        this(a.serial_number, a.master_sound, a.volume_level, a.feedback_sound, a.notification_sound);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    DisplaySpeakerPreferences(com.navdy.service.library.events.preferences.DisplaySpeakerPreferences$Builder a, com.navdy.service.library.events.preferences.DisplaySpeakerPreferences$1 a0) {
        this(a);
    }
    
    public DisplaySpeakerPreferences(Long a, Boolean a0, Integer a1, Boolean a2, Boolean a3) {
        this.serial_number = a;
        this.master_sound = a0;
        this.volume_level = a1;
        this.feedback_sound = a2;
        this.notification_sound = a3;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.preferences.DisplaySpeakerPreferences) {
                com.navdy.service.library.events.preferences.DisplaySpeakerPreferences a0 = (com.navdy.service.library.events.preferences.DisplaySpeakerPreferences)a;
                boolean b0 = this.equals(this.serial_number, a0.serial_number);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.master_sound, a0.master_sound)) {
                        break label1;
                    }
                    if (!this.equals(this.volume_level, a0.volume_level)) {
                        break label1;
                    }
                    if (!this.equals(this.feedback_sound, a0.feedback_sound)) {
                        break label1;
                    }
                    if (this.equals(this.notification_sound, a0.notification_sound)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((((this.serial_number == null) ? 0 : this.serial_number.hashCode()) * 37 + ((this.master_sound == null) ? 0 : this.master_sound.hashCode())) * 37 + ((this.volume_level == null) ? 0 : this.volume_level.hashCode())) * 37 + ((this.feedback_sound == null) ? 0 : this.feedback_sound.hashCode())) * 37 + ((this.notification_sound == null) ? 0 : this.notification_sound.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
