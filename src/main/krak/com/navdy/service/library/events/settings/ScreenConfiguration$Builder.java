package com.navdy.service.library.events.settings;

final public class ScreenConfiguration$Builder extends com.squareup.wire.Message.Builder {
    public Integer marginBottom;
    public Integer marginLeft;
    public Integer marginRight;
    public Integer marginTop;
    public Float scaleX;
    public Float scaleY;
    
    public ScreenConfiguration$Builder() {
    }
    
    public ScreenConfiguration$Builder(com.navdy.service.library.events.settings.ScreenConfiguration a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.marginLeft = a.marginLeft;
            this.marginTop = a.marginTop;
            this.marginRight = a.marginRight;
            this.marginBottom = a.marginBottom;
            this.scaleX = a.scaleX;
            this.scaleY = a.scaleY;
        }
    }
    
    public com.navdy.service.library.events.settings.ScreenConfiguration build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.settings.ScreenConfiguration(this, (com.navdy.service.library.events.settings.ScreenConfiguration$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.settings.ScreenConfiguration$Builder marginBottom(Integer a) {
        this.marginBottom = a;
        return this;
    }
    
    public com.navdy.service.library.events.settings.ScreenConfiguration$Builder marginLeft(Integer a) {
        this.marginLeft = a;
        return this;
    }
    
    public com.navdy.service.library.events.settings.ScreenConfiguration$Builder marginRight(Integer a) {
        this.marginRight = a;
        return this;
    }
    
    public com.navdy.service.library.events.settings.ScreenConfiguration$Builder marginTop(Integer a) {
        this.marginTop = a;
        return this;
    }
    
    public com.navdy.service.library.events.settings.ScreenConfiguration$Builder scaleX(Float a) {
        this.scaleX = a;
        return this;
    }
    
    public com.navdy.service.library.events.settings.ScreenConfiguration$Builder scaleY(Float a) {
        this.scaleY = a;
        return this;
    }
}
