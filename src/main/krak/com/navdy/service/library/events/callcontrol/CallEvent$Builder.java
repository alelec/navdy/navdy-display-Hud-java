package com.navdy.service.library.events.callcontrol;

final public class CallEvent$Builder extends com.squareup.wire.Message.Builder {
    public Boolean answered;
    public String contact_name;
    public Long duration;
    public Boolean incoming;
    public String number;
    public Long start_time;
    
    public CallEvent$Builder() {
    }
    
    public CallEvent$Builder(com.navdy.service.library.events.callcontrol.CallEvent a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.incoming = a.incoming;
            this.answered = a.answered;
            this.number = a.number;
            this.contact_name = a.contact_name;
            this.start_time = a.start_time;
            this.duration = a.duration;
        }
    }
    
    public com.navdy.service.library.events.callcontrol.CallEvent$Builder answered(Boolean a) {
        this.answered = a;
        return this;
    }
    
    public com.navdy.service.library.events.callcontrol.CallEvent build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.callcontrol.CallEvent(this, (com.navdy.service.library.events.callcontrol.CallEvent$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.callcontrol.CallEvent$Builder contact_name(String s) {
        this.contact_name = s;
        return this;
    }
    
    public com.navdy.service.library.events.callcontrol.CallEvent$Builder duration(Long a) {
        this.duration = a;
        return this;
    }
    
    public com.navdy.service.library.events.callcontrol.CallEvent$Builder incoming(Boolean a) {
        this.incoming = a;
        return this;
    }
    
    public com.navdy.service.library.events.callcontrol.CallEvent$Builder number(String s) {
        this.number = s;
        return this;
    }
    
    public com.navdy.service.library.events.callcontrol.CallEvent$Builder start_time(Long a) {
        this.start_time = a;
        return this;
    }
}
