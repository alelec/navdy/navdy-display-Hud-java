package com.navdy.service.library.events.photo;

final public class PhotoType extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.photo.PhotoType[] $VALUES;
    final public static com.navdy.service.library.events.photo.PhotoType PHOTO_ALBUM_ART;
    final public static com.navdy.service.library.events.photo.PhotoType PHOTO_CONTACT;
    final public static com.navdy.service.library.events.photo.PhotoType PHOTO_DRIVER_PROFILE;
    final private int value;
    
    static {
        PHOTO_CONTACT = new com.navdy.service.library.events.photo.PhotoType("PHOTO_CONTACT", 0, 0);
        PHOTO_ALBUM_ART = new com.navdy.service.library.events.photo.PhotoType("PHOTO_ALBUM_ART", 1, 1);
        PHOTO_DRIVER_PROFILE = new com.navdy.service.library.events.photo.PhotoType("PHOTO_DRIVER_PROFILE", 2, 2);
        com.navdy.service.library.events.photo.PhotoType[] a = new com.navdy.service.library.events.photo.PhotoType[3];
        a[0] = PHOTO_CONTACT;
        a[1] = PHOTO_ALBUM_ART;
        a[2] = PHOTO_DRIVER_PROFILE;
        $VALUES = a;
    }
    
    private PhotoType(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.photo.PhotoType valueOf(String s) {
        return (com.navdy.service.library.events.photo.PhotoType)Enum.valueOf(com.navdy.service.library.events.photo.PhotoType.class, s);
    }
    
    public static com.navdy.service.library.events.photo.PhotoType[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
