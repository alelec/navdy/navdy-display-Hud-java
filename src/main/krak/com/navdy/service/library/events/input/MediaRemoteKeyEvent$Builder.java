package com.navdy.service.library.events.input;

final public class MediaRemoteKeyEvent$Builder extends com.squareup.wire.Message.Builder {
    public com.navdy.service.library.events.input.KeyEvent action;
    public com.navdy.service.library.events.input.MediaRemoteKey key;
    
    public MediaRemoteKeyEvent$Builder() {
    }
    
    public MediaRemoteKeyEvent$Builder(com.navdy.service.library.events.input.MediaRemoteKeyEvent a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.key = a.key;
            this.action = a.action;
        }
    }
    
    public com.navdy.service.library.events.input.MediaRemoteKeyEvent$Builder action(com.navdy.service.library.events.input.KeyEvent a) {
        this.action = a;
        return this;
    }
    
    public com.navdy.service.library.events.input.MediaRemoteKeyEvent build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.input.MediaRemoteKeyEvent(this, (com.navdy.service.library.events.input.MediaRemoteKeyEvent$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.input.MediaRemoteKeyEvent$Builder key(com.navdy.service.library.events.input.MediaRemoteKey a) {
        this.key = a;
        return this;
    }
}
