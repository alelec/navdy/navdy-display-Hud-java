package com.navdy.service.library.events.debug;

final public class DriveRecordingsRequest extends com.squareup.wire.Message {
    final private static long serialVersionUID = 0L;
    
    public DriveRecordingsRequest() {
    }
    
    private DriveRecordingsRequest(com.navdy.service.library.events.debug.DriveRecordingsRequest$Builder a) {
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    DriveRecordingsRequest(com.navdy.service.library.events.debug.DriveRecordingsRequest$Builder a, com.navdy.service.library.events.debug.DriveRecordingsRequest$1 a0) {
        this(a);
    }
    
    public boolean equals(Object a) {
        return a instanceof com.navdy.service.library.events.debug.DriveRecordingsRequest;
    }
    
    public int hashCode() {
        return 0;
    }
}
