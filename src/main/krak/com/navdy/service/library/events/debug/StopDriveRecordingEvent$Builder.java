package com.navdy.service.library.events.debug;

final public class StopDriveRecordingEvent$Builder extends com.squareup.wire.Message.Builder {
    public StopDriveRecordingEvent$Builder() {
    }
    
    public StopDriveRecordingEvent$Builder(com.navdy.service.library.events.debug.StopDriveRecordingEvent a) {
        super((com.squareup.wire.Message)a);
    }
    
    public com.navdy.service.library.events.debug.StopDriveRecordingEvent build() {
        return new com.navdy.service.library.events.debug.StopDriveRecordingEvent(this, (com.navdy.service.library.events.debug.StopDriveRecordingEvent$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
}
