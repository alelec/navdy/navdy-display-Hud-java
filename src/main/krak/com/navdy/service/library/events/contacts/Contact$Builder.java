package com.navdy.service.library.events.contacts;

final public class Contact$Builder extends com.squareup.wire.Message.Builder {
    public String label;
    public String name;
    public String number;
    public com.navdy.service.library.events.contacts.PhoneNumberType numberType;
    
    public Contact$Builder() {
    }
    
    public Contact$Builder(com.navdy.service.library.events.contacts.Contact a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.name = a.name;
            this.number = a.number;
            this.numberType = a.numberType;
            this.label = a.label;
        }
    }
    
    public com.navdy.service.library.events.contacts.Contact build() {
        return new com.navdy.service.library.events.contacts.Contact(this, (com.navdy.service.library.events.contacts.Contact$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.contacts.Contact$Builder label(String s) {
        this.label = s;
        return this;
    }
    
    public com.navdy.service.library.events.contacts.Contact$Builder name(String s) {
        this.name = s;
        return this;
    }
    
    public com.navdy.service.library.events.contacts.Contact$Builder number(String s) {
        this.number = s;
        return this;
    }
    
    public com.navdy.service.library.events.contacts.Contact$Builder numberType(com.navdy.service.library.events.contacts.PhoneNumberType a) {
        this.numberType = a;
        return this;
    }
}
