package com.navdy.service.library.events.file;

final public class FileType extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.file.FileType[] $VALUES;
    final public static com.navdy.service.library.events.file.FileType FILE_TYPE_LOGS;
    final public static com.navdy.service.library.events.file.FileType FILE_TYPE_OTA;
    final public static com.navdy.service.library.events.file.FileType FILE_TYPE_PERF_TEST;
    final private int value;
    
    static {
        FILE_TYPE_OTA = new com.navdy.service.library.events.file.FileType("FILE_TYPE_OTA", 0, 1);
        FILE_TYPE_LOGS = new com.navdy.service.library.events.file.FileType("FILE_TYPE_LOGS", 1, 2);
        FILE_TYPE_PERF_TEST = new com.navdy.service.library.events.file.FileType("FILE_TYPE_PERF_TEST", 2, 3);
        com.navdy.service.library.events.file.FileType[] a = new com.navdy.service.library.events.file.FileType[3];
        a[0] = FILE_TYPE_OTA;
        a[1] = FILE_TYPE_LOGS;
        a[2] = FILE_TYPE_PERF_TEST;
        $VALUES = a;
    }
    
    private FileType(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.file.FileType valueOf(String s) {
        return (com.navdy.service.library.events.file.FileType)Enum.valueOf(com.navdy.service.library.events.file.FileType.class, s);
    }
    
    public static com.navdy.service.library.events.file.FileType[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
