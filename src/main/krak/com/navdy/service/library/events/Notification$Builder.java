package com.navdy.service.library.events;

final public class Notification$Builder extends com.squareup.wire.Message.Builder {
    public String message_data;
    public String origin;
    public String sender;
    public Integer sms_class;
    public String target;
    public String type;
    
    public Notification$Builder() {
    }
    
    public Notification$Builder(com.navdy.service.library.events.Notification a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.sms_class = a.sms_class;
            this.origin = a.origin;
            this.sender = a.sender;
            this.target = a.target;
            this.message_data = a.message_data;
            this.type = a.type;
        }
    }
    
    public com.navdy.service.library.events.Notification build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.Notification(this, (com.navdy.service.library.events.Notification$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.Notification$Builder message_data(String s) {
        this.message_data = s;
        return this;
    }
    
    public com.navdy.service.library.events.Notification$Builder origin(String s) {
        this.origin = s;
        return this;
    }
    
    public com.navdy.service.library.events.Notification$Builder sender(String s) {
        this.sender = s;
        return this;
    }
    
    public com.navdy.service.library.events.Notification$Builder sms_class(Integer a) {
        this.sms_class = a;
        return this;
    }
    
    public com.navdy.service.library.events.Notification$Builder target(String s) {
        this.target = s;
        return this;
    }
    
    public com.navdy.service.library.events.Notification$Builder type(String s) {
        this.type = s;
        return this;
    }
}
