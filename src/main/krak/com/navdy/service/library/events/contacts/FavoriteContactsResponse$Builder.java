package com.navdy.service.library.events.contacts;

final public class FavoriteContactsResponse$Builder extends com.squareup.wire.Message.Builder {
    public java.util.List contacts;
    public com.navdy.service.library.events.RequestStatus status;
    public String statusDetail;
    
    public FavoriteContactsResponse$Builder() {
    }
    
    public FavoriteContactsResponse$Builder(com.navdy.service.library.events.contacts.FavoriteContactsResponse a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.status = a.status;
            this.statusDetail = a.statusDetail;
            this.contacts = com.navdy.service.library.events.contacts.FavoriteContactsResponse.access$000(a.contacts);
        }
    }
    
    public com.navdy.service.library.events.contacts.FavoriteContactsResponse build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.contacts.FavoriteContactsResponse(this, (com.navdy.service.library.events.contacts.FavoriteContactsResponse$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.contacts.FavoriteContactsResponse$Builder contacts(java.util.List a) {
        this.contacts = com.navdy.service.library.events.contacts.FavoriteContactsResponse$Builder.checkForNulls(a);
        return this;
    }
    
    public com.navdy.service.library.events.contacts.FavoriteContactsResponse$Builder status(com.navdy.service.library.events.RequestStatus a) {
        this.status = a;
        return this;
    }
    
    public com.navdy.service.library.events.contacts.FavoriteContactsResponse$Builder statusDetail(String s) {
        this.statusDetail = s;
        return this;
    }
}
