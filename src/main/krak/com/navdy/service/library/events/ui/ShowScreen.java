package com.navdy.service.library.events.ui;

final public class ShowScreen extends com.squareup.wire.Message {
    final public static java.util.List DEFAULT_ARGUMENTS;
    final public static com.navdy.service.library.events.ui.Screen DEFAULT_SCREEN;
    final private static long serialVersionUID = 0L;
    final public java.util.List arguments;
    final public com.navdy.service.library.events.ui.Screen screen;
    
    static {
        DEFAULT_SCREEN = com.navdy.service.library.events.ui.Screen.SCREEN_DASHBOARD;
        DEFAULT_ARGUMENTS = java.util.Collections.emptyList();
    }
    
    public ShowScreen(com.navdy.service.library.events.ui.Screen a, java.util.List a0) {
        this.screen = a;
        this.arguments = com.navdy.service.library.events.ui.ShowScreen.immutableCopyOf(a0);
    }
    
    private ShowScreen(com.navdy.service.library.events.ui.ShowScreen$Builder a) {
        this(a.screen, a.arguments);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    ShowScreen(com.navdy.service.library.events.ui.ShowScreen$Builder a, com.navdy.service.library.events.ui.ShowScreen$1 a0) {
        this(a);
    }
    
    static java.util.List access$000(java.util.List a) {
        return com.navdy.service.library.events.ui.ShowScreen.copyOf(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.ui.ShowScreen) {
                com.navdy.service.library.events.ui.ShowScreen a0 = (com.navdy.service.library.events.ui.ShowScreen)a;
                boolean b0 = this.equals(this.screen, a0.screen);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (this.equals(this.arguments, a0.arguments)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((this.screen == null) ? 0 : this.screen.hashCode()) * 37 + ((this.arguments == null) ? 1 : this.arguments.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
