package com.navdy.service.library.events;

final public class FavoriteDestinationsRequest$Builder extends com.squareup.wire.Message.Builder {
    public Long serial_number;
    
    public FavoriteDestinationsRequest$Builder() {
    }
    
    public FavoriteDestinationsRequest$Builder(com.navdy.service.library.events.FavoriteDestinationsRequest a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.serial_number = a.serial_number;
        }
    }
    
    public com.navdy.service.library.events.FavoriteDestinationsRequest build() {
        return new com.navdy.service.library.events.FavoriteDestinationsRequest(this, (com.navdy.service.library.events.FavoriteDestinationsRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.FavoriteDestinationsRequest$Builder serial_number(Long a) {
        this.serial_number = a;
        return this;
    }
}
