package com.navdy.service.library.events.notification;

final public class NotificationEvent$Builder extends com.squareup.wire.Message.Builder {
    public java.util.List actions;
    public String appId;
    public String appName;
    public Boolean cannotReplyBack;
    public com.navdy.service.library.events.notification.NotificationCategory category;
    public String iconResourceName;
    public Integer id;
    public String imageResourceName;
    public String message;
    public okio.ByteString photo;
    public String sourceIdentifier;
    public String subtitle;
    public String title;
    
    public NotificationEvent$Builder() {
    }
    
    public NotificationEvent$Builder(com.navdy.service.library.events.notification.NotificationEvent a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.id = a.id;
            this.category = a.category;
            this.title = a.title;
            this.subtitle = a.subtitle;
            this.message = a.message;
            this.appId = a.appId;
            this.actions = com.navdy.service.library.events.notification.NotificationEvent.access$000(a.actions);
            this.photo = a.photo;
            this.iconResourceName = a.iconResourceName;
            this.imageResourceName = a.imageResourceName;
            this.sourceIdentifier = a.sourceIdentifier;
            this.cannotReplyBack = a.cannotReplyBack;
            this.appName = a.appName;
        }
    }
    
    public com.navdy.service.library.events.notification.NotificationEvent$Builder actions(java.util.List a) {
        this.actions = com.navdy.service.library.events.notification.NotificationEvent$Builder.checkForNulls(a);
        return this;
    }
    
    public com.navdy.service.library.events.notification.NotificationEvent$Builder appId(String s) {
        this.appId = s;
        return this;
    }
    
    public com.navdy.service.library.events.notification.NotificationEvent$Builder appName(String s) {
        this.appName = s;
        return this;
    }
    
    public com.navdy.service.library.events.notification.NotificationEvent build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.notification.NotificationEvent(this, (com.navdy.service.library.events.notification.NotificationEvent$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.notification.NotificationEvent$Builder cannotReplyBack(Boolean a) {
        this.cannotReplyBack = a;
        return this;
    }
    
    public com.navdy.service.library.events.notification.NotificationEvent$Builder category(com.navdy.service.library.events.notification.NotificationCategory a) {
        this.category = a;
        return this;
    }
    
    public com.navdy.service.library.events.notification.NotificationEvent$Builder iconResourceName(String s) {
        this.iconResourceName = s;
        return this;
    }
    
    public com.navdy.service.library.events.notification.NotificationEvent$Builder id(Integer a) {
        this.id = a;
        return this;
    }
    
    public com.navdy.service.library.events.notification.NotificationEvent$Builder imageResourceName(String s) {
        this.imageResourceName = s;
        return this;
    }
    
    public com.navdy.service.library.events.notification.NotificationEvent$Builder message(String s) {
        this.message = s;
        return this;
    }
    
    public com.navdy.service.library.events.notification.NotificationEvent$Builder photo(okio.ByteString a) {
        this.photo = a;
        return this;
    }
    
    public com.navdy.service.library.events.notification.NotificationEvent$Builder sourceIdentifier(String s) {
        this.sourceIdentifier = s;
        return this;
    }
    
    public com.navdy.service.library.events.notification.NotificationEvent$Builder subtitle(String s) {
        this.subtitle = s;
        return this;
    }
    
    public com.navdy.service.library.events.notification.NotificationEvent$Builder title(String s) {
        this.title = s;
        return this;
    }
}
