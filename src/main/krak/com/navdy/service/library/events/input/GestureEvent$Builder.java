package com.navdy.service.library.events.input;

final public class GestureEvent$Builder extends com.squareup.wire.Message.Builder {
    public com.navdy.service.library.events.input.Gesture gesture;
    public Integer x;
    public Integer y;
    
    public GestureEvent$Builder() {
    }
    
    public GestureEvent$Builder(com.navdy.service.library.events.input.GestureEvent a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.gesture = a.gesture;
            this.x = a.x;
            this.y = a.y;
        }
    }
    
    public com.navdy.service.library.events.input.GestureEvent build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.input.GestureEvent(this, (com.navdy.service.library.events.input.GestureEvent$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.input.GestureEvent$Builder gesture(com.navdy.service.library.events.input.Gesture a) {
        this.gesture = a;
        return this;
    }
    
    public com.navdy.service.library.events.input.GestureEvent$Builder x(Integer a) {
        this.x = a;
        return this;
    }
    
    public com.navdy.service.library.events.input.GestureEvent$Builder y(Integer a) {
        this.y = a;
        return this;
    }
}
