package com.navdy.service.library.events;

final public class DeviceInfo$Builder extends com.squareup.wire.Message.Builder {
    public String buildType;
    public com.navdy.service.library.events.Capabilities capabilities;
    public String clientVersion;
    public String deviceId;
    public String deviceMake;
    public String deviceName;
    public String deviceUuid;
    public Boolean forceFullUpdate;
    public String kernelVersion;
    public java.util.List legacyCapabilities;
    public String model;
    public java.util.List musicPlayers_OBSOLETE;
    public com.navdy.service.library.events.DeviceInfo$Platform platform;
    public String protocolVersion;
    public Integer systemApiLevel;
    public String systemVersion;
    
    public DeviceInfo$Builder() {
    }
    
    public DeviceInfo$Builder(com.navdy.service.library.events.DeviceInfo a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.deviceId = a.deviceId;
            this.clientVersion = a.clientVersion;
            this.protocolVersion = a.protocolVersion;
            this.deviceName = a.deviceName;
            this.systemVersion = a.systemVersion;
            this.model = a.model;
            this.deviceUuid = a.deviceUuid;
            this.systemApiLevel = a.systemApiLevel;
            this.kernelVersion = a.kernelVersion;
            this.platform = a.platform;
            this.buildType = a.buildType;
            this.deviceMake = a.deviceMake;
            this.forceFullUpdate = a.forceFullUpdate;
            this.legacyCapabilities = com.navdy.service.library.events.DeviceInfo.access$000(a.legacyCapabilities);
            this.musicPlayers_OBSOLETE = com.navdy.service.library.events.DeviceInfo.access$100(a.musicPlayers_OBSOLETE);
            this.capabilities = a.capabilities;
        }
    }
    
    public com.navdy.service.library.events.DeviceInfo build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.DeviceInfo(this, (com.navdy.service.library.events.DeviceInfo$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.DeviceInfo$Builder buildType(String s) {
        this.buildType = s;
        return this;
    }
    
    public com.navdy.service.library.events.DeviceInfo$Builder capabilities(com.navdy.service.library.events.Capabilities a) {
        this.capabilities = a;
        return this;
    }
    
    public com.navdy.service.library.events.DeviceInfo$Builder clientVersion(String s) {
        this.clientVersion = s;
        return this;
    }
    
    public com.navdy.service.library.events.DeviceInfo$Builder deviceId(String s) {
        this.deviceId = s;
        return this;
    }
    
    public com.navdy.service.library.events.DeviceInfo$Builder deviceMake(String s) {
        this.deviceMake = s;
        return this;
    }
    
    public com.navdy.service.library.events.DeviceInfo$Builder deviceName(String s) {
        this.deviceName = s;
        return this;
    }
    
    public com.navdy.service.library.events.DeviceInfo$Builder deviceUuid(String s) {
        this.deviceUuid = s;
        return this;
    }
    
    public com.navdy.service.library.events.DeviceInfo$Builder forceFullUpdate(Boolean a) {
        this.forceFullUpdate = a;
        return this;
    }
    
    public com.navdy.service.library.events.DeviceInfo$Builder kernelVersion(String s) {
        this.kernelVersion = s;
        return this;
    }
    
    public com.navdy.service.library.events.DeviceInfo$Builder legacyCapabilities(java.util.List a) {
        this.legacyCapabilities = com.navdy.service.library.events.DeviceInfo$Builder.checkForNulls(a);
        return this;
    }
    
    public com.navdy.service.library.events.DeviceInfo$Builder model(String s) {
        this.model = s;
        return this;
    }
    
    public com.navdy.service.library.events.DeviceInfo$Builder musicPlayers_OBSOLETE(java.util.List a) {
        this.musicPlayers_OBSOLETE = com.navdy.service.library.events.DeviceInfo$Builder.checkForNulls(a);
        return this;
    }
    
    public com.navdy.service.library.events.DeviceInfo$Builder platform(com.navdy.service.library.events.DeviceInfo$Platform a) {
        this.platform = a;
        return this;
    }
    
    public com.navdy.service.library.events.DeviceInfo$Builder protocolVersion(String s) {
        this.protocolVersion = s;
        return this;
    }
    
    public com.navdy.service.library.events.DeviceInfo$Builder systemApiLevel(Integer a) {
        this.systemApiLevel = a;
        return this;
    }
    
    public com.navdy.service.library.events.DeviceInfo$Builder systemVersion(String s) {
        this.systemVersion = s;
        return this;
    }
}
