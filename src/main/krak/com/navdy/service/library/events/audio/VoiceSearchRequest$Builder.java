package com.navdy.service.library.events.audio;

final public class VoiceSearchRequest$Builder extends com.squareup.wire.Message.Builder {
    public Boolean end;
    
    public VoiceSearchRequest$Builder() {
    }
    
    public VoiceSearchRequest$Builder(com.navdy.service.library.events.audio.VoiceSearchRequest a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.end = a.end;
        }
    }
    
    public com.navdy.service.library.events.audio.VoiceSearchRequest build() {
        return new com.navdy.service.library.events.audio.VoiceSearchRequest(this, (com.navdy.service.library.events.audio.VoiceSearchRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.audio.VoiceSearchRequest$Builder end(Boolean a) {
        this.end = a;
        return this;
    }
}
