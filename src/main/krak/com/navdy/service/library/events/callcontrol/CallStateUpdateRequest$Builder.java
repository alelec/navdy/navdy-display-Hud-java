package com.navdy.service.library.events.callcontrol;

final public class CallStateUpdateRequest$Builder extends com.squareup.wire.Message.Builder {
    public Boolean start;
    
    public CallStateUpdateRequest$Builder() {
    }
    
    public CallStateUpdateRequest$Builder(com.navdy.service.library.events.callcontrol.CallStateUpdateRequest a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.start = a.start;
        }
    }
    
    public com.navdy.service.library.events.callcontrol.CallStateUpdateRequest build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.callcontrol.CallStateUpdateRequest(this, (com.navdy.service.library.events.callcontrol.CallStateUpdateRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.callcontrol.CallStateUpdateRequest$Builder start(Boolean a) {
        this.start = a;
        return this;
    }
}
