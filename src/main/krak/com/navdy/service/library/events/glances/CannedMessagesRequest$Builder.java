package com.navdy.service.library.events.glances;

final public class CannedMessagesRequest$Builder extends com.squareup.wire.Message.Builder {
    public Long serial_number;
    
    public CannedMessagesRequest$Builder() {
    }
    
    public CannedMessagesRequest$Builder(com.navdy.service.library.events.glances.CannedMessagesRequest a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.serial_number = a.serial_number;
        }
    }
    
    public com.navdy.service.library.events.glances.CannedMessagesRequest build() {
        return new com.navdy.service.library.events.glances.CannedMessagesRequest(this, (com.navdy.service.library.events.glances.CannedMessagesRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.glances.CannedMessagesRequest$Builder serial_number(Long a) {
        this.serial_number = a;
        return this;
    }
}
