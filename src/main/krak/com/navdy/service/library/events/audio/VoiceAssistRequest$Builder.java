package com.navdy.service.library.events.audio;

final public class VoiceAssistRequest$Builder extends com.squareup.wire.Message.Builder {
    public Boolean end;
    
    public VoiceAssistRequest$Builder() {
    }
    
    public VoiceAssistRequest$Builder(com.navdy.service.library.events.audio.VoiceAssistRequest a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.end = a.end;
        }
    }
    
    public com.navdy.service.library.events.audio.VoiceAssistRequest build() {
        return new com.navdy.service.library.events.audio.VoiceAssistRequest(this, (com.navdy.service.library.events.audio.VoiceAssistRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.audio.VoiceAssistRequest$Builder end(Boolean a) {
        this.end = a;
        return this;
    }
}
