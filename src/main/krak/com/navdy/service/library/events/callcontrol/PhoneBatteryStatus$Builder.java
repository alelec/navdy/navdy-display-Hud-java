package com.navdy.service.library.events.callcontrol;

final public class PhoneBatteryStatus$Builder extends com.squareup.wire.Message.Builder {
    public Boolean charging;
    public Integer level;
    public com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$BatteryStatus status;
    
    public PhoneBatteryStatus$Builder() {
    }
    
    public PhoneBatteryStatus$Builder(com.navdy.service.library.events.callcontrol.PhoneBatteryStatus a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.status = a.status;
            this.level = a.level;
            this.charging = a.charging;
        }
    }
    
    public com.navdy.service.library.events.callcontrol.PhoneBatteryStatus build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.callcontrol.PhoneBatteryStatus(this, (com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$Builder charging(Boolean a) {
        this.charging = a;
        return this;
    }
    
    public com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$Builder level(Integer a) {
        this.level = a;
        return this;
    }
    
    public com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$Builder status(com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$BatteryStatus a) {
        this.status = a;
        return this;
    }
}
