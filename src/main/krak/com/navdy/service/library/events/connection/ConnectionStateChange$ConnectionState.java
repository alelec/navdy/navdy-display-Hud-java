package com.navdy.service.library.events.connection;

final public class ConnectionStateChange$ConnectionState extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState[] $VALUES;
    final public static com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState CONNECTION_CONNECTED;
    final public static com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState CONNECTION_DISCONNECTED;
    final public static com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState CONNECTION_LINK_ESTABLISHED;
    final public static com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState CONNECTION_LINK_LOST;
    final public static com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState CONNECTION_VERIFIED;
    final private int value;
    
    static {
        CONNECTION_CONNECTED = new com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState("CONNECTION_CONNECTED", 0, 1);
        CONNECTION_DISCONNECTED = new com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState("CONNECTION_DISCONNECTED", 1, 2);
        CONNECTION_LINK_ESTABLISHED = new com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState("CONNECTION_LINK_ESTABLISHED", 2, 3);
        CONNECTION_LINK_LOST = new com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState("CONNECTION_LINK_LOST", 3, 4);
        CONNECTION_VERIFIED = new com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState("CONNECTION_VERIFIED", 4, 5);
        com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState[] a = new com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState[5];
        a[0] = CONNECTION_CONNECTED;
        a[1] = CONNECTION_DISCONNECTED;
        a[2] = CONNECTION_LINK_ESTABLISHED;
        a[3] = CONNECTION_LINK_LOST;
        a[4] = CONNECTION_VERIFIED;
        $VALUES = a;
    }
    
    private ConnectionStateChange$ConnectionState(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState valueOf(String s) {
        return (com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState)Enum.valueOf(com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState.class, s);
    }
    
    public static com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
