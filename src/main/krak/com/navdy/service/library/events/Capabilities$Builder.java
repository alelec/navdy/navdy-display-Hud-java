package com.navdy.service.library.events;

final public class Capabilities$Builder extends com.squareup.wire.Message.Builder {
    public Boolean cannedResponseToSms;
    public Boolean compactUi;
    public Boolean customDialLongPress;
    public Boolean localMusicBrowser;
    public Boolean musicArtworkCache;
    public Boolean navCoordsLookup;
    public Boolean placeTypeSearch;
    public Boolean searchResultList;
    public Boolean supportsPhoneticTts;
    public Boolean voiceSearch;
    public Boolean voiceSearchNewIOSPauseBehaviors;
    
    public Capabilities$Builder() {
    }
    
    public Capabilities$Builder(com.navdy.service.library.events.Capabilities a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.compactUi = a.compactUi;
            this.placeTypeSearch = a.placeTypeSearch;
            this.voiceSearch = a.voiceSearch;
            this.localMusicBrowser = a.localMusicBrowser;
            this.navCoordsLookup = a.navCoordsLookup;
            this.searchResultList = a.searchResultList;
            this.musicArtworkCache = a.musicArtworkCache;
            this.voiceSearchNewIOSPauseBehaviors = a.voiceSearchNewIOSPauseBehaviors;
            this.cannedResponseToSms = a.cannedResponseToSms;
            this.customDialLongPress = a.customDialLongPress;
            this.supportsPhoneticTts = a.supportsPhoneticTts;
        }
    }
    
    public com.navdy.service.library.events.Capabilities build() {
        return new com.navdy.service.library.events.Capabilities(this, (com.navdy.service.library.events.Capabilities$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.Capabilities$Builder cannedResponseToSms(Boolean a) {
        this.cannedResponseToSms = a;
        return this;
    }
    
    public com.navdy.service.library.events.Capabilities$Builder compactUi(Boolean a) {
        this.compactUi = a;
        return this;
    }
    
    public com.navdy.service.library.events.Capabilities$Builder customDialLongPress(Boolean a) {
        this.customDialLongPress = a;
        return this;
    }
    
    public com.navdy.service.library.events.Capabilities$Builder localMusicBrowser(Boolean a) {
        this.localMusicBrowser = a;
        return this;
    }
    
    public com.navdy.service.library.events.Capabilities$Builder musicArtworkCache(Boolean a) {
        this.musicArtworkCache = a;
        return this;
    }
    
    public com.navdy.service.library.events.Capabilities$Builder navCoordsLookup(Boolean a) {
        this.navCoordsLookup = a;
        return this;
    }
    
    public com.navdy.service.library.events.Capabilities$Builder placeTypeSearch(Boolean a) {
        this.placeTypeSearch = a;
        return this;
    }
    
    public com.navdy.service.library.events.Capabilities$Builder searchResultList(Boolean a) {
        this.searchResultList = a;
        return this;
    }
    
    public com.navdy.service.library.events.Capabilities$Builder supportsPhoneticTts(Boolean a) {
        this.supportsPhoneticTts = a;
        return this;
    }
    
    public com.navdy.service.library.events.Capabilities$Builder voiceSearch(Boolean a) {
        this.voiceSearch = a;
        return this;
    }
    
    public com.navdy.service.library.events.Capabilities$Builder voiceSearchNewIOSPauseBehaviors(Boolean a) {
        this.voiceSearchNewIOSPauseBehaviors = a;
        return this;
    }
}
