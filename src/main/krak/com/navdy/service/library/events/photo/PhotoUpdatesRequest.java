package com.navdy.service.library.events.photo;

final public class PhotoUpdatesRequest extends com.squareup.wire.Message {
    final public static com.navdy.service.library.events.photo.PhotoType DEFAULT_PHOTOTYPE;
    final public static Boolean DEFAULT_START;
    final private static long serialVersionUID = 0L;
    final public com.navdy.service.library.events.photo.PhotoType photoType;
    final public Boolean start;
    
    static {
        DEFAULT_START = Boolean.valueOf(false);
        DEFAULT_PHOTOTYPE = com.navdy.service.library.events.photo.PhotoType.PHOTO_ALBUM_ART;
    }
    
    private PhotoUpdatesRequest(com.navdy.service.library.events.photo.PhotoUpdatesRequest$Builder a) {
        this(a.start, a.photoType);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    PhotoUpdatesRequest(com.navdy.service.library.events.photo.PhotoUpdatesRequest$Builder a, com.navdy.service.library.events.photo.PhotoUpdatesRequest$1 a0) {
        this(a);
    }
    
    public PhotoUpdatesRequest(Boolean a, com.navdy.service.library.events.photo.PhotoType a0) {
        this.start = a;
        this.photoType = a0;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.photo.PhotoUpdatesRequest) {
                com.navdy.service.library.events.photo.PhotoUpdatesRequest a0 = (com.navdy.service.library.events.photo.PhotoUpdatesRequest)a;
                boolean b0 = this.equals(this.start, a0.start);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (this.equals(this.photoType, a0.photoType)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((this.start == null) ? 0 : this.start.hashCode()) * 37 + ((this.photoType == null) ? 0 : this.photoType.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
