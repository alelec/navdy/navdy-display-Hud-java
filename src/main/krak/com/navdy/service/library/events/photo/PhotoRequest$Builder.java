package com.navdy.service.library.events.photo;

final public class PhotoRequest$Builder extends com.squareup.wire.Message.Builder {
    public String identifier;
    public String name;
    public String photoChecksum;
    public com.navdy.service.library.events.photo.PhotoType photoType;
    
    public PhotoRequest$Builder() {
    }
    
    public PhotoRequest$Builder(com.navdy.service.library.events.photo.PhotoRequest a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.identifier = a.identifier;
            this.photoChecksum = a.photoChecksum;
            this.photoType = a.photoType;
            this.name = a.name;
        }
    }
    
    public com.navdy.service.library.events.photo.PhotoRequest build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.photo.PhotoRequest(this, (com.navdy.service.library.events.photo.PhotoRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.photo.PhotoRequest$Builder identifier(String s) {
        this.identifier = s;
        return this;
    }
    
    public com.navdy.service.library.events.photo.PhotoRequest$Builder name(String s) {
        this.name = s;
        return this;
    }
    
    public com.navdy.service.library.events.photo.PhotoRequest$Builder photoChecksum(String s) {
        this.photoChecksum = s;
        return this;
    }
    
    public com.navdy.service.library.events.photo.PhotoRequest$Builder photoType(com.navdy.service.library.events.photo.PhotoType a) {
        this.photoType = a;
        return this;
    }
}
