package com.navdy.service.library.events.navigation;

final public class NavigationRouteCancelRequest$Builder extends com.squareup.wire.Message.Builder {
    public String handle;
    
    public NavigationRouteCancelRequest$Builder() {
    }
    
    public NavigationRouteCancelRequest$Builder(com.navdy.service.library.events.navigation.NavigationRouteCancelRequest a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.handle = a.handle;
        }
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteCancelRequest build() {
        return new com.navdy.service.library.events.navigation.NavigationRouteCancelRequest(this, (com.navdy.service.library.events.navigation.NavigationRouteCancelRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteCancelRequest$Builder handle(String s) {
        this.handle = s;
        return this;
    }
}
