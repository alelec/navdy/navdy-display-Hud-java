package com.navdy.service.library.events.settings;

final public class ReadSettingsResponse$Builder extends com.squareup.wire.Message.Builder {
    public com.navdy.service.library.events.settings.ScreenConfiguration screenConfiguration;
    public java.util.List settings;
    
    public ReadSettingsResponse$Builder() {
    }
    
    public ReadSettingsResponse$Builder(com.navdy.service.library.events.settings.ReadSettingsResponse a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.screenConfiguration = a.screenConfiguration;
            this.settings = com.navdy.service.library.events.settings.ReadSettingsResponse.access$000(a.settings);
        }
    }
    
    public com.navdy.service.library.events.settings.ReadSettingsResponse build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.settings.ReadSettingsResponse(this, (com.navdy.service.library.events.settings.ReadSettingsResponse$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.settings.ReadSettingsResponse$Builder screenConfiguration(com.navdy.service.library.events.settings.ScreenConfiguration a) {
        this.screenConfiguration = a;
        return this;
    }
    
    public com.navdy.service.library.events.settings.ReadSettingsResponse$Builder settings(java.util.List a) {
        this.settings = com.navdy.service.library.events.settings.ReadSettingsResponse$Builder.checkForNulls(a);
        return this;
    }
}
