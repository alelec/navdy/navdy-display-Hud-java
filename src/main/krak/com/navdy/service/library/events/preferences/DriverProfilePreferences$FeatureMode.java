package com.navdy.service.library.events.preferences;

final public class DriverProfilePreferences$FeatureMode extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.preferences.DriverProfilePreferences$FeatureMode[] $VALUES;
    final public static com.navdy.service.library.events.preferences.DriverProfilePreferences$FeatureMode FEATURE_MODE_BETA;
    final public static com.navdy.service.library.events.preferences.DriverProfilePreferences$FeatureMode FEATURE_MODE_EXPERIMENTAL;
    final public static com.navdy.service.library.events.preferences.DriverProfilePreferences$FeatureMode FEATURE_MODE_RELEASE;
    final private int value;
    
    static {
        FEATURE_MODE_RELEASE = new com.navdy.service.library.events.preferences.DriverProfilePreferences$FeatureMode("FEATURE_MODE_RELEASE", 0, 1);
        FEATURE_MODE_BETA = new com.navdy.service.library.events.preferences.DriverProfilePreferences$FeatureMode("FEATURE_MODE_BETA", 1, 2);
        FEATURE_MODE_EXPERIMENTAL = new com.navdy.service.library.events.preferences.DriverProfilePreferences$FeatureMode("FEATURE_MODE_EXPERIMENTAL", 2, 3);
        com.navdy.service.library.events.preferences.DriverProfilePreferences$FeatureMode[] a = new com.navdy.service.library.events.preferences.DriverProfilePreferences$FeatureMode[3];
        a[0] = FEATURE_MODE_RELEASE;
        a[1] = FEATURE_MODE_BETA;
        a[2] = FEATURE_MODE_EXPERIMENTAL;
        $VALUES = a;
    }
    
    private DriverProfilePreferences$FeatureMode(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.preferences.DriverProfilePreferences$FeatureMode valueOf(String s) {
        return (com.navdy.service.library.events.preferences.DriverProfilePreferences$FeatureMode)Enum.valueOf(com.navdy.service.library.events.preferences.DriverProfilePreferences$FeatureMode.class, s);
    }
    
    public static com.navdy.service.library.events.preferences.DriverProfilePreferences$FeatureMode[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
