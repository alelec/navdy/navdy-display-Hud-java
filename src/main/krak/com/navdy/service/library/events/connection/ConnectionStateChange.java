package com.navdy.service.library.events.connection;

final public class ConnectionStateChange extends com.squareup.wire.Message {
    final public static String DEFAULT_REMOTEDEVICEID = "";
    final public static com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState DEFAULT_STATE;
    final private static long serialVersionUID = 0L;
    final public String remoteDeviceId;
    final public com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState state;
    
    static {
        DEFAULT_STATE = com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState.CONNECTION_CONNECTED;
    }
    
    private ConnectionStateChange(com.navdy.service.library.events.connection.ConnectionStateChange$Builder a) {
        this(a.remoteDeviceId, a.state);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    ConnectionStateChange(com.navdy.service.library.events.connection.ConnectionStateChange$Builder a, com.navdy.service.library.events.connection.ConnectionStateChange$1 a0) {
        this(a);
    }
    
    public ConnectionStateChange(String s, com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState a) {
        this.remoteDeviceId = s;
        this.state = a;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.connection.ConnectionStateChange) {
                com.navdy.service.library.events.connection.ConnectionStateChange a0 = (com.navdy.service.library.events.connection.ConnectionStateChange)a;
                boolean b0 = this.equals(this.remoteDeviceId, a0.remoteDeviceId);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (this.equals(this.state, a0.state)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((this.remoteDeviceId == null) ? 0 : this.remoteDeviceId.hashCode()) * 37 + ((this.state == null) ? 0 : this.state.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
