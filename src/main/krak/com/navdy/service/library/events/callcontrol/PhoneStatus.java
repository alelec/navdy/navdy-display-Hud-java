package com.navdy.service.library.events.callcontrol;

final public class PhoneStatus extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.callcontrol.PhoneStatus[] $VALUES;
    final public static com.navdy.service.library.events.callcontrol.PhoneStatus PHONE_CONNECTING;
    final public static com.navdy.service.library.events.callcontrol.PhoneStatus PHONE_DIALING;
    final public static com.navdy.service.library.events.callcontrol.PhoneStatus PHONE_DISCONNECTING;
    final public static com.navdy.service.library.events.callcontrol.PhoneStatus PHONE_HELD;
    final public static com.navdy.service.library.events.callcontrol.PhoneStatus PHONE_IDLE;
    final public static com.navdy.service.library.events.callcontrol.PhoneStatus PHONE_OFFHOOK;
    final public static com.navdy.service.library.events.callcontrol.PhoneStatus PHONE_RINGING;
    final private int value;
    
    static {
        PHONE_IDLE = new com.navdy.service.library.events.callcontrol.PhoneStatus("PHONE_IDLE", 0, 1);
        PHONE_RINGING = new com.navdy.service.library.events.callcontrol.PhoneStatus("PHONE_RINGING", 1, 2);
        PHONE_OFFHOOK = new com.navdy.service.library.events.callcontrol.PhoneStatus("PHONE_OFFHOOK", 2, 3);
        PHONE_DIALING = new com.navdy.service.library.events.callcontrol.PhoneStatus("PHONE_DIALING", 3, 4);
        PHONE_HELD = new com.navdy.service.library.events.callcontrol.PhoneStatus("PHONE_HELD", 4, 5);
        PHONE_CONNECTING = new com.navdy.service.library.events.callcontrol.PhoneStatus("PHONE_CONNECTING", 5, 6);
        PHONE_DISCONNECTING = new com.navdy.service.library.events.callcontrol.PhoneStatus("PHONE_DISCONNECTING", 6, 7);
        com.navdy.service.library.events.callcontrol.PhoneStatus[] a = new com.navdy.service.library.events.callcontrol.PhoneStatus[7];
        a[0] = PHONE_IDLE;
        a[1] = PHONE_RINGING;
        a[2] = PHONE_OFFHOOK;
        a[3] = PHONE_DIALING;
        a[4] = PHONE_HELD;
        a[5] = PHONE_CONNECTING;
        a[6] = PHONE_DISCONNECTING;
        $VALUES = a;
    }
    
    private PhoneStatus(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.callcontrol.PhoneStatus valueOf(String s) {
        return (com.navdy.service.library.events.callcontrol.PhoneStatus)Enum.valueOf(com.navdy.service.library.events.callcontrol.PhoneStatus.class, s);
    }
    
    public static com.navdy.service.library.events.callcontrol.PhoneStatus[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
