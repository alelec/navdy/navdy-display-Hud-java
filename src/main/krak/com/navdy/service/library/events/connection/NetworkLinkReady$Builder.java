package com.navdy.service.library.events.connection;

final public class NetworkLinkReady$Builder extends com.squareup.wire.Message.Builder {
    public NetworkLinkReady$Builder() {
    }
    
    public NetworkLinkReady$Builder(com.navdy.service.library.events.connection.NetworkLinkReady a) {
        super((com.squareup.wire.Message)a);
    }
    
    public com.navdy.service.library.events.connection.NetworkLinkReady build() {
        return new com.navdy.service.library.events.connection.NetworkLinkReady(this, (com.navdy.service.library.events.connection.NetworkLinkReady$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
}
