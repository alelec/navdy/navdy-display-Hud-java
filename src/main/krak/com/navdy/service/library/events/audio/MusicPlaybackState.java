package com.navdy.service.library.events.audio;

final public class MusicPlaybackState extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.audio.MusicPlaybackState[] $VALUES;
    final public static com.navdy.service.library.events.audio.MusicPlaybackState PLAYBACK_BUFFERING;
    final public static com.navdy.service.library.events.audio.MusicPlaybackState PLAYBACK_CONNECTING;
    final public static com.navdy.service.library.events.audio.MusicPlaybackState PLAYBACK_ERROR;
    final public static com.navdy.service.library.events.audio.MusicPlaybackState PLAYBACK_FAST_FORWARDING;
    final public static com.navdy.service.library.events.audio.MusicPlaybackState PLAYBACK_NONE;
    final public static com.navdy.service.library.events.audio.MusicPlaybackState PLAYBACK_PAUSED;
    final public static com.navdy.service.library.events.audio.MusicPlaybackState PLAYBACK_PLAYING;
    final public static com.navdy.service.library.events.audio.MusicPlaybackState PLAYBACK_REWINDING;
    final public static com.navdy.service.library.events.audio.MusicPlaybackState PLAYBACK_SKIPPING_TO_NEXT;
    final public static com.navdy.service.library.events.audio.MusicPlaybackState PLAYBACK_SKIPPING_TO_PREVIOUS;
    final public static com.navdy.service.library.events.audio.MusicPlaybackState PLAYBACK_STOPPED;
    final private int value;
    
    static {
        PLAYBACK_NONE = new com.navdy.service.library.events.audio.MusicPlaybackState("PLAYBACK_NONE", 0, 1);
        PLAYBACK_BUFFERING = new com.navdy.service.library.events.audio.MusicPlaybackState("PLAYBACK_BUFFERING", 1, 2);
        PLAYBACK_CONNECTING = new com.navdy.service.library.events.audio.MusicPlaybackState("PLAYBACK_CONNECTING", 2, 3);
        PLAYBACK_ERROR = new com.navdy.service.library.events.audio.MusicPlaybackState("PLAYBACK_ERROR", 3, 4);
        PLAYBACK_PLAYING = new com.navdy.service.library.events.audio.MusicPlaybackState("PLAYBACK_PLAYING", 4, 5);
        PLAYBACK_PAUSED = new com.navdy.service.library.events.audio.MusicPlaybackState("PLAYBACK_PAUSED", 5, 6);
        PLAYBACK_STOPPED = new com.navdy.service.library.events.audio.MusicPlaybackState("PLAYBACK_STOPPED", 6, 7);
        PLAYBACK_FAST_FORWARDING = new com.navdy.service.library.events.audio.MusicPlaybackState("PLAYBACK_FAST_FORWARDING", 7, 8);
        PLAYBACK_REWINDING = new com.navdy.service.library.events.audio.MusicPlaybackState("PLAYBACK_REWINDING", 8, 9);
        PLAYBACK_SKIPPING_TO_NEXT = new com.navdy.service.library.events.audio.MusicPlaybackState("PLAYBACK_SKIPPING_TO_NEXT", 9, 10);
        PLAYBACK_SKIPPING_TO_PREVIOUS = new com.navdy.service.library.events.audio.MusicPlaybackState("PLAYBACK_SKIPPING_TO_PREVIOUS", 10, 11);
        com.navdy.service.library.events.audio.MusicPlaybackState[] a = new com.navdy.service.library.events.audio.MusicPlaybackState[11];
        a[0] = PLAYBACK_NONE;
        a[1] = PLAYBACK_BUFFERING;
        a[2] = PLAYBACK_CONNECTING;
        a[3] = PLAYBACK_ERROR;
        a[4] = PLAYBACK_PLAYING;
        a[5] = PLAYBACK_PAUSED;
        a[6] = PLAYBACK_STOPPED;
        a[7] = PLAYBACK_FAST_FORWARDING;
        a[8] = PLAYBACK_REWINDING;
        a[9] = PLAYBACK_SKIPPING_TO_NEXT;
        a[10] = PLAYBACK_SKIPPING_TO_PREVIOUS;
        $VALUES = a;
    }
    
    private MusicPlaybackState(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.audio.MusicPlaybackState valueOf(String s) {
        return (com.navdy.service.library.events.audio.MusicPlaybackState)Enum.valueOf(com.navdy.service.library.events.audio.MusicPlaybackState.class, s);
    }
    
    public static com.navdy.service.library.events.audio.MusicPlaybackState[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
