package com.navdy.service.library.events.audio;

final public class MusicCapabilitiesResponse$Builder extends com.squareup.wire.Message.Builder {
    public java.util.List capabilities;
    
    public MusicCapabilitiesResponse$Builder() {
    }
    
    public MusicCapabilitiesResponse$Builder(com.navdy.service.library.events.audio.MusicCapabilitiesResponse a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.capabilities = com.navdy.service.library.events.audio.MusicCapabilitiesResponse.access$000(a.capabilities);
        }
    }
    
    public com.navdy.service.library.events.audio.MusicCapabilitiesResponse build() {
        return new com.navdy.service.library.events.audio.MusicCapabilitiesResponse(this, (com.navdy.service.library.events.audio.MusicCapabilitiesResponse$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.audio.MusicCapabilitiesResponse$Builder capabilities(java.util.List a) {
        this.capabilities = com.navdy.service.library.events.audio.MusicCapabilitiesResponse$Builder.checkForNulls(a);
        return this;
    }
}
