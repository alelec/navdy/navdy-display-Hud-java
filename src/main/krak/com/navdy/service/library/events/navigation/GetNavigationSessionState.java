package com.navdy.service.library.events.navigation;

final public class GetNavigationSessionState extends com.squareup.wire.Message {
    final private static long serialVersionUID = 0L;
    
    public GetNavigationSessionState() {
    }
    
    private GetNavigationSessionState(com.navdy.service.library.events.navigation.GetNavigationSessionState$Builder a) {
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    GetNavigationSessionState(com.navdy.service.library.events.navigation.GetNavigationSessionState$Builder a, com.navdy.service.library.events.navigation.GetNavigationSessionState$1 a0) {
        this(a);
    }
    
    public boolean equals(Object a) {
        return a instanceof com.navdy.service.library.events.navigation.GetNavigationSessionState;
    }
    
    public int hashCode() {
        return 0;
    }
}
