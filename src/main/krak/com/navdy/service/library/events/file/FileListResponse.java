package com.navdy.service.library.events.file;

final public class FileListResponse extends com.squareup.wire.Message {
    final public static java.util.List DEFAULT_FILES;
    final public static com.navdy.service.library.events.file.FileType DEFAULT_FILE_TYPE;
    final public static com.navdy.service.library.events.RequestStatus DEFAULT_STATUS;
    final public static String DEFAULT_STATUS_DETAIL = "";
    final private static long serialVersionUID = 0L;
    final public com.navdy.service.library.events.file.FileType file_type;
    final public java.util.List files;
    final public com.navdy.service.library.events.RequestStatus status;
    final public String status_detail;
    
    static {
        DEFAULT_STATUS = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
        DEFAULT_FILE_TYPE = com.navdy.service.library.events.file.FileType.FILE_TYPE_OTA;
        DEFAULT_FILES = java.util.Collections.emptyList();
    }
    
    public FileListResponse(com.navdy.service.library.events.RequestStatus a, String s, com.navdy.service.library.events.file.FileType a0, java.util.List a1) {
        this.status = a;
        this.status_detail = s;
        this.file_type = a0;
        this.files = com.navdy.service.library.events.file.FileListResponse.immutableCopyOf(a1);
    }
    
    private FileListResponse(com.navdy.service.library.events.file.FileListResponse$Builder a) {
        this(a.status, a.status_detail, a.file_type, a.files);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    FileListResponse(com.navdy.service.library.events.file.FileListResponse$Builder a, com.navdy.service.library.events.file.FileListResponse$1 a0) {
        this(a);
    }
    
    static java.util.List access$000(java.util.List a) {
        return com.navdy.service.library.events.file.FileListResponse.copyOf(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.file.FileListResponse) {
                com.navdy.service.library.events.file.FileListResponse a0 = (com.navdy.service.library.events.file.FileListResponse)a;
                boolean b0 = this.equals(this.status, a0.status);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.status_detail, a0.status_detail)) {
                        break label1;
                    }
                    if (!this.equals(this.file_type, a0.file_type)) {
                        break label1;
                    }
                    if (this.equals(this.files, a0.files)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((((this.status == null) ? 0 : this.status.hashCode()) * 37 + ((this.status_detail == null) ? 0 : this.status_detail.hashCode())) * 37 + ((this.file_type == null) ? 0 : this.file_type.hashCode())) * 37 + ((this.files == null) ? 1 : this.files.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
