package com.navdy.service.library.events.dial;

final public class DialStatusRequest extends com.squareup.wire.Message {
    final private static long serialVersionUID = 0L;
    
    public DialStatusRequest() {
    }
    
    private DialStatusRequest(com.navdy.service.library.events.dial.DialStatusRequest$Builder a) {
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    DialStatusRequest(com.navdy.service.library.events.dial.DialStatusRequest$Builder a, com.navdy.service.library.events.dial.DialStatusRequest$1 a0) {
        this(a);
    }
    
    public boolean equals(Object a) {
        return a instanceof com.navdy.service.library.events.dial.DialStatusRequest;
    }
    
    public int hashCode() {
        return 0;
    }
}
