package com.navdy.service.library.events.audio;

final public class MusicCollectionInfo extends com.squareup.wire.Message {
    final public static Boolean DEFAULT_CANSHUFFLE;
    final public static String DEFAULT_COLLECTIONID = "";
    final public static com.navdy.service.library.events.audio.MusicCollectionSource DEFAULT_COLLECTIONSOURCE;
    final public static com.navdy.service.library.events.audio.MusicCollectionType DEFAULT_COLLECTIONTYPE;
    final public static Integer DEFAULT_INDEX;
    final public static String DEFAULT_NAME = "";
    final public static Integer DEFAULT_SIZE;
    final public static String DEFAULT_SUBTITLE = "";
    final private static long serialVersionUID = 0L;
    final public Boolean canShuffle;
    final public String collectionId;
    final public com.navdy.service.library.events.audio.MusicCollectionSource collectionSource;
    final public com.navdy.service.library.events.audio.MusicCollectionType collectionType;
    final public Integer index;
    final public String name;
    final public Integer size;
    final public String subtitle;
    
    static {
        DEFAULT_COLLECTIONSOURCE = com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN;
        DEFAULT_COLLECTIONTYPE = com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_UNKNOWN;
        DEFAULT_SIZE = Integer.valueOf(0);
        DEFAULT_INDEX = Integer.valueOf(0);
        DEFAULT_CANSHUFFLE = Boolean.valueOf(false);
    }
    
    private MusicCollectionInfo(com.navdy.service.library.events.audio.MusicCollectionInfo$Builder a) {
        this(a.collectionSource, a.collectionType, a.collectionId, a.name, a.size, a.index, a.subtitle, a.canShuffle);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    MusicCollectionInfo(com.navdy.service.library.events.audio.MusicCollectionInfo$Builder a, com.navdy.service.library.events.audio.MusicCollectionInfo$1 a0) {
        this(a);
    }
    
    public MusicCollectionInfo(com.navdy.service.library.events.audio.MusicCollectionSource a, com.navdy.service.library.events.audio.MusicCollectionType a0, String s, String s0, Integer a1, Integer a2, String s1, Boolean a3) {
        this.collectionSource = a;
        this.collectionType = a0;
        this.collectionId = s;
        this.name = s0;
        this.size = a1;
        this.index = a2;
        this.subtitle = s1;
        this.canShuffle = a3;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.audio.MusicCollectionInfo) {
                com.navdy.service.library.events.audio.MusicCollectionInfo a0 = (com.navdy.service.library.events.audio.MusicCollectionInfo)a;
                boolean b0 = this.equals(this.collectionSource, a0.collectionSource);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.collectionType, a0.collectionType)) {
                        break label1;
                    }
                    if (!this.equals(this.collectionId, a0.collectionId)) {
                        break label1;
                    }
                    if (!this.equals(this.name, a0.name)) {
                        break label1;
                    }
                    if (!this.equals(this.size, a0.size)) {
                        break label1;
                    }
                    if (!this.equals(this.index, a0.index)) {
                        break label1;
                    }
                    if (!this.equals(this.subtitle, a0.subtitle)) {
                        break label1;
                    }
                    if (this.equals(this.canShuffle, a0.canShuffle)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((((((((this.collectionSource == null) ? 0 : this.collectionSource.hashCode()) * 37 + ((this.collectionType == null) ? 0 : this.collectionType.hashCode())) * 37 + ((this.collectionId == null) ? 0 : this.collectionId.hashCode())) * 37 + ((this.name == null) ? 0 : this.name.hashCode())) * 37 + ((this.size == null) ? 0 : this.size.hashCode())) * 37 + ((this.index == null) ? 0 : this.index.hashCode())) * 37 + ((this.subtitle == null) ? 0 : this.subtitle.hashCode())) * 37 + ((this.canShuffle == null) ? 0 : this.canShuffle.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
