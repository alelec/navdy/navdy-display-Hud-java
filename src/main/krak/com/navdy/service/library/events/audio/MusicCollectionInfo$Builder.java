package com.navdy.service.library.events.audio;

final public class MusicCollectionInfo$Builder extends com.squareup.wire.Message.Builder {
    public Boolean canShuffle;
    public String collectionId;
    public com.navdy.service.library.events.audio.MusicCollectionSource collectionSource;
    public com.navdy.service.library.events.audio.MusicCollectionType collectionType;
    public Integer index;
    public String name;
    public Integer size;
    public String subtitle;
    
    public MusicCollectionInfo$Builder() {
    }
    
    public MusicCollectionInfo$Builder(com.navdy.service.library.events.audio.MusicCollectionInfo a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.collectionSource = a.collectionSource;
            this.collectionType = a.collectionType;
            this.collectionId = a.collectionId;
            this.name = a.name;
            this.size = a.size;
            this.index = a.index;
            this.subtitle = a.subtitle;
            this.canShuffle = a.canShuffle;
        }
    }
    
    public com.navdy.service.library.events.audio.MusicCollectionInfo build() {
        return new com.navdy.service.library.events.audio.MusicCollectionInfo(this, (com.navdy.service.library.events.audio.MusicCollectionInfo$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.audio.MusicCollectionInfo$Builder canShuffle(Boolean a) {
        this.canShuffle = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicCollectionInfo$Builder collectionId(String s) {
        this.collectionId = s;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicCollectionInfo$Builder collectionSource(com.navdy.service.library.events.audio.MusicCollectionSource a) {
        this.collectionSource = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicCollectionInfo$Builder collectionType(com.navdy.service.library.events.audio.MusicCollectionType a) {
        this.collectionType = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicCollectionInfo$Builder index(Integer a) {
        this.index = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicCollectionInfo$Builder name(String s) {
        this.name = s;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicCollectionInfo$Builder size(Integer a) {
        this.size = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicCollectionInfo$Builder subtitle(String s) {
        this.subtitle = s;
        return this;
    }
}
