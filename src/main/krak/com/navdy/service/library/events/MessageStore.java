package com.navdy.service.library.events;

public class MessageStore {
    final private static com.navdy.service.library.log.Logger sLogger;
    private java.io.File directory;
    private com.squareup.wire.Wire wire;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.events.MessageStore.class);
    }
    
    public MessageStore(java.io.File a) {
        this.directory = a;
        Class[] a0 = new Class[1];
        a0[0] = com.navdy.service.library.events.Ext_NavdyEvent.class;
        this.wire = new com.squareup.wire.Wire(a0);
    }
    
    public static com.squareup.wire.Message removeNulls(com.squareup.wire.Message a) {
        return com.navdy.service.library.events.NavdyEventUtil.applyDefaults(a);
    }
    
    public com.squareup.wire.Message readMessage(String s, Class a) {
        return this.readMessage(s, a, (com.navdy.service.library.events.NavdyEventUtil$Initializer)null);
    }
    
    public com.squareup.wire.Message readMessage(String s, Class a, com.navdy.service.library.events.NavdyEventUtil$Initializer a0) {
        com.squareup.wire.Message a1 = null;
        java.io.File a2 = new java.io.File(this.directory, s);
        label7: {
            java.io.FileInputStream a3 = null;
            label5: {
                label1: {
                    java.io.FileInputStream a4 = null;
                    Throwable a5 = null;
                    label6: try {
                        {
                            label0: {
                                Exception a6 = null;
                                label4: {
                                    label3: {
                                        try {
                                            a4 = null;
                                            long j = a2.length();
                                            int i = (j < 0L) ? -1 : (j == 0L) ? 0 : 1;
                                            label2: {
                                                if (i == 0) {
                                                    break label2;
                                                }
                                                a4 = null;
                                                a3 = new java.io.FileInputStream(a2);
                                                break label3;
                                            }
                                            a4 = null;
                                            a1 = com.navdy.service.library.events.NavdyEventUtil.getDefault(a, a0);
                                            a3 = null;
                                            break label1;
                                        } catch(Exception a7) {
                                            a6 = a7;
                                        }
                                        a3 = null;
                                        break label4;
                                    }
                                    try {
                                        try {
                                            a1 = this.wire.parseFrom((java.io.InputStream)a3, a);
                                            break label1;
                                        } catch(Exception a8) {
                                            a6 = a8;
                                        }
                                    } catch(Throwable a9) {
                                        a5 = a9;
                                        break label0;
                                    }
                                }
                                a4 = a3;
                                a1 = com.navdy.service.library.events.NavdyEventUtil.getDefault(a, a0);
                                sLogger.w(new StringBuilder().append("Failed to read message from ").append(s).toString(), (Throwable)a6);
                                break label5;
                            }
                            a4 = a3;
                            break label6;
                        }
                    } catch(Throwable a10) {
                        a5 = a10;
                    }
                    com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a4);
                    throw a5;
                }
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a3);
                break label7;
            }
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a3);
        }
        return com.navdy.service.library.events.MessageStore.removeNulls(a1);
    }
    
    public void writeMessage(com.squareup.wire.Message a, String s) {
        String s0 = new StringBuilder().append(s).append(".new").toString();
        java.io.File a0 = new java.io.File(this.directory, s0);
        try {
            byte[] a1 = a.toByteArray();
            int i = com.navdy.service.library.util.IOUtils.copyFile(a0.getAbsolutePath(), a1);
            if (i == a1.length) {
                if (!a0.renameTo(new java.io.File(this.directory, s))) {
                    sLogger.e(new StringBuilder().append("could not replace messages in ").append(s).toString());
                }
            } else {
                sLogger.e(new StringBuilder().append("failed to write messages to ").append(s0).append("wrote: ").append(i).append(" bytes, length:").append(a1.length).toString());
            }
        } catch(Exception a2) {
            sLogger.e(new StringBuilder().append("could not write new messages to ").append(s).append(" : ").toString(), (Throwable)a2);
        }
    }
}
