package com.navdy.service.library.events.navigation;

final public class NavigationSessionState extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.navigation.NavigationSessionState[] $VALUES;
    final public static com.navdy.service.library.events.navigation.NavigationSessionState NAV_SESSION_ARRIVED;
    final public static com.navdy.service.library.events.navigation.NavigationSessionState NAV_SESSION_ENGINE_NOT_READY;
    final public static com.navdy.service.library.events.navigation.NavigationSessionState NAV_SESSION_ENGINE_READY;
    final public static com.navdy.service.library.events.navigation.NavigationSessionState NAV_SESSION_ERROR;
    final public static com.navdy.service.library.events.navigation.NavigationSessionState NAV_SESSION_LOCATION_SERVICES_NOT_READY;
    final public static com.navdy.service.library.events.navigation.NavigationSessionState NAV_SESSION_PAUSED;
    final public static com.navdy.service.library.events.navigation.NavigationSessionState NAV_SESSION_REROUTED;
    final public static com.navdy.service.library.events.navigation.NavigationSessionState NAV_SESSION_STARTED;
    final public static com.navdy.service.library.events.navigation.NavigationSessionState NAV_SESSION_STOPPED;
    final private int value;
    
    static {
        NAV_SESSION_ENGINE_NOT_READY = new com.navdy.service.library.events.navigation.NavigationSessionState("NAV_SESSION_ENGINE_NOT_READY", 0, 1);
        NAV_SESSION_LOCATION_SERVICES_NOT_READY = new com.navdy.service.library.events.navigation.NavigationSessionState("NAV_SESSION_LOCATION_SERVICES_NOT_READY", 1, 2);
        NAV_SESSION_STOPPED = new com.navdy.service.library.events.navigation.NavigationSessionState("NAV_SESSION_STOPPED", 2, 3);
        NAV_SESSION_STARTED = new com.navdy.service.library.events.navigation.NavigationSessionState("NAV_SESSION_STARTED", 3, 4);
        NAV_SESSION_PAUSED = new com.navdy.service.library.events.navigation.NavigationSessionState("NAV_SESSION_PAUSED", 4, 5);
        NAV_SESSION_ERROR = new com.navdy.service.library.events.navigation.NavigationSessionState("NAV_SESSION_ERROR", 5, 6);
        NAV_SESSION_REROUTED = new com.navdy.service.library.events.navigation.NavigationSessionState("NAV_SESSION_REROUTED", 6, 7);
        NAV_SESSION_ENGINE_READY = new com.navdy.service.library.events.navigation.NavigationSessionState("NAV_SESSION_ENGINE_READY", 7, 8);
        NAV_SESSION_ARRIVED = new com.navdy.service.library.events.navigation.NavigationSessionState("NAV_SESSION_ARRIVED", 8, 9);
        com.navdy.service.library.events.navigation.NavigationSessionState[] a = new com.navdy.service.library.events.navigation.NavigationSessionState[9];
        a[0] = NAV_SESSION_ENGINE_NOT_READY;
        a[1] = NAV_SESSION_LOCATION_SERVICES_NOT_READY;
        a[2] = NAV_SESSION_STOPPED;
        a[3] = NAV_SESSION_STARTED;
        a[4] = NAV_SESSION_PAUSED;
        a[5] = NAV_SESSION_ERROR;
        a[6] = NAV_SESSION_REROUTED;
        a[7] = NAV_SESSION_ENGINE_READY;
        a[8] = NAV_SESSION_ARRIVED;
        $VALUES = a;
    }
    
    private NavigationSessionState(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.navigation.NavigationSessionState valueOf(String s) {
        return (com.navdy.service.library.events.navigation.NavigationSessionState)Enum.valueOf(com.navdy.service.library.events.navigation.NavigationSessionState.class, s);
    }
    
    public static com.navdy.service.library.events.navigation.NavigationSessionState[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
