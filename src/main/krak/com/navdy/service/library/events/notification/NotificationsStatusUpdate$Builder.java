package com.navdy.service.library.events.notification;

final public class NotificationsStatusUpdate$Builder extends com.squareup.wire.Message.Builder {
    public com.navdy.service.library.events.notification.NotificationsError errorDetails;
    public com.navdy.service.library.events.notification.ServiceType service;
    public com.navdy.service.library.events.notification.NotificationsState state;
    
    public NotificationsStatusUpdate$Builder() {
    }
    
    public NotificationsStatusUpdate$Builder(com.navdy.service.library.events.notification.NotificationsStatusUpdate a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.state = a.state;
            this.service = a.service;
            this.errorDetails = a.errorDetails;
        }
    }
    
    public com.navdy.service.library.events.notification.NotificationsStatusUpdate build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.notification.NotificationsStatusUpdate(this, (com.navdy.service.library.events.notification.NotificationsStatusUpdate$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.notification.NotificationsStatusUpdate$Builder errorDetails(com.navdy.service.library.events.notification.NotificationsError a) {
        this.errorDetails = a;
        return this;
    }
    
    public com.navdy.service.library.events.notification.NotificationsStatusUpdate$Builder service(com.navdy.service.library.events.notification.ServiceType a) {
        this.service = a;
        return this;
    }
    
    public com.navdy.service.library.events.notification.NotificationsStatusUpdate$Builder state(com.navdy.service.library.events.notification.NotificationsState a) {
        this.state = a;
        return this;
    }
}
