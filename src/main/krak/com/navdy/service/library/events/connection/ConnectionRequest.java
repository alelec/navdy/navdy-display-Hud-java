package com.navdy.service.library.events.connection;

final public class ConnectionRequest extends com.squareup.wire.Message {
    final public static com.navdy.service.library.events.connection.ConnectionRequest$Action DEFAULT_ACTION;
    final public static String DEFAULT_REMOTEDEVICEID = "";
    final private static long serialVersionUID = 0L;
    final public com.navdy.service.library.events.connection.ConnectionRequest$Action action;
    final public String remoteDeviceId;
    
    static {
        DEFAULT_ACTION = com.navdy.service.library.events.connection.ConnectionRequest$Action.CONNECTION_START_SEARCH;
    }
    
    public ConnectionRequest(com.navdy.service.library.events.connection.ConnectionRequest$Action a, String s) {
        this.action = a;
        this.remoteDeviceId = s;
    }
    
    private ConnectionRequest(com.navdy.service.library.events.connection.ConnectionRequest$Builder a) {
        this(a.action, a.remoteDeviceId);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    ConnectionRequest(com.navdy.service.library.events.connection.ConnectionRequest$Builder a, com.navdy.service.library.events.connection.ConnectionRequest$1 a0) {
        this(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.connection.ConnectionRequest) {
                com.navdy.service.library.events.connection.ConnectionRequest a0 = (com.navdy.service.library.events.connection.ConnectionRequest)a;
                boolean b0 = this.equals(this.action, a0.action);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (this.equals(this.remoteDeviceId, a0.remoteDeviceId)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((this.action == null) ? 0 : this.action.hashCode()) * 37 + ((this.remoteDeviceId == null) ? 0 : this.remoteDeviceId.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
