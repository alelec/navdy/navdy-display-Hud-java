package com.navdy.service.library.events.audio;

final public class CancelSpeechRequest extends com.squareup.wire.Message {
    final public static String DEFAULT_ID = "";
    final private static long serialVersionUID = 0L;
    final public String id;
    
    private CancelSpeechRequest(com.navdy.service.library.events.audio.CancelSpeechRequest$Builder a) {
        this(a.id);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    CancelSpeechRequest(com.navdy.service.library.events.audio.CancelSpeechRequest$Builder a, com.navdy.service.library.events.audio.CancelSpeechRequest$1 a0) {
        this(a);
    }
    
    public CancelSpeechRequest(String s) {
        this.id = s;
    }
    
    public boolean equals(Object a) {
        return a == this || a instanceof com.navdy.service.library.events.audio.CancelSpeechRequest && this.equals(this.id, ((com.navdy.service.library.events.audio.CancelSpeechRequest)a).id);
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (this.id == null) ? 0 : this.id.hashCode();
            this.hashCode = i;
        }
        return i;
    }
}
