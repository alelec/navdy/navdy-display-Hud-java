package com.navdy.service.library.events.navigation;

final public class RouteManeuver$Builder extends com.squareup.wire.Message.Builder {
    public String currentRoad;
    public Float distanceToPendingRoad;
    public com.navdy.service.library.events.navigation.DistanceUnit distanceToPendingRoadUnit;
    public Integer maneuverTime;
    public String pendingRoad;
    public com.navdy.service.library.events.navigation.NavigationTurn pendingTurn;
    
    public RouteManeuver$Builder() {
    }
    
    public RouteManeuver$Builder(com.navdy.service.library.events.navigation.RouteManeuver a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.currentRoad = a.currentRoad;
            this.pendingTurn = a.pendingTurn;
            this.pendingRoad = a.pendingRoad;
            this.distanceToPendingRoad = a.distanceToPendingRoad;
            this.distanceToPendingRoadUnit = a.distanceToPendingRoadUnit;
            this.maneuverTime = a.maneuverTime;
        }
    }
    
    public com.navdy.service.library.events.navigation.RouteManeuver build() {
        return new com.navdy.service.library.events.navigation.RouteManeuver(this, (com.navdy.service.library.events.navigation.RouteManeuver$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.navigation.RouteManeuver$Builder currentRoad(String s) {
        this.currentRoad = s;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.RouteManeuver$Builder distanceToPendingRoad(Float a) {
        this.distanceToPendingRoad = a;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.RouteManeuver$Builder distanceToPendingRoadUnit(com.navdy.service.library.events.navigation.DistanceUnit a) {
        this.distanceToPendingRoadUnit = a;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.RouteManeuver$Builder maneuverTime(Integer a) {
        this.maneuverTime = a;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.RouteManeuver$Builder pendingRoad(String s) {
        this.pendingRoad = s;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.RouteManeuver$Builder pendingTurn(com.navdy.service.library.events.navigation.NavigationTurn a) {
        this.pendingTurn = a;
        return this;
    }
}
