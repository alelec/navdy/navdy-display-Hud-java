package com.navdy.service.library.events.glances;

final public class GlanceEvent$GlanceActions extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.glances.GlanceEvent$GlanceActions[] $VALUES;
    final public static com.navdy.service.library.events.glances.GlanceEvent$GlanceActions REPLY;
    final private int value;
    
    static {
        REPLY = new com.navdy.service.library.events.glances.GlanceEvent$GlanceActions("REPLY", 0, 0);
        com.navdy.service.library.events.glances.GlanceEvent$GlanceActions[] a = new com.navdy.service.library.events.glances.GlanceEvent$GlanceActions[1];
        a[0] = REPLY;
        $VALUES = a;
    }
    
    private GlanceEvent$GlanceActions(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.glances.GlanceEvent$GlanceActions valueOf(String s) {
        return (com.navdy.service.library.events.glances.GlanceEvent$GlanceActions)Enum.valueOf(com.navdy.service.library.events.glances.GlanceEvent$GlanceActions.class, s);
    }
    
    public static com.navdy.service.library.events.glances.GlanceEvent$GlanceActions[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
