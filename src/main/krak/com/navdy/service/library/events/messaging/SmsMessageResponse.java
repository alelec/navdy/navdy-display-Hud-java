package com.navdy.service.library.events.messaging;

final public class SmsMessageResponse extends com.squareup.wire.Message {
    final public static String DEFAULT_ID = "";
    final public static String DEFAULT_NAME = "";
    final public static String DEFAULT_NUMBER = "";
    final public static com.navdy.service.library.events.RequestStatus DEFAULT_STATUS;
    final private static long serialVersionUID = 0L;
    final public String id;
    final public String name;
    final public String number;
    final public com.navdy.service.library.events.RequestStatus status;
    
    static {
        DEFAULT_STATUS = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
    }
    
    public SmsMessageResponse(com.navdy.service.library.events.RequestStatus a, String s, String s0, String s1) {
        this.status = a;
        this.number = s;
        this.name = s0;
        this.id = s1;
    }
    
    private SmsMessageResponse(com.navdy.service.library.events.messaging.SmsMessageResponse$Builder a) {
        this(a.status, a.number, a.name, a.id);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    SmsMessageResponse(com.navdy.service.library.events.messaging.SmsMessageResponse$Builder a, com.navdy.service.library.events.messaging.SmsMessageResponse$1 a0) {
        this(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.messaging.SmsMessageResponse) {
                com.navdy.service.library.events.messaging.SmsMessageResponse a0 = (com.navdy.service.library.events.messaging.SmsMessageResponse)a;
                boolean b0 = this.equals(this.status, a0.status);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.number, a0.number)) {
                        break label1;
                    }
                    if (!this.equals(this.name, a0.name)) {
                        break label1;
                    }
                    if (this.equals(this.id, a0.id)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((((this.status == null) ? 0 : this.status.hashCode()) * 37 + ((this.number == null) ? 0 : this.number.hashCode())) * 37 + ((this.name == null) ? 0 : this.name.hashCode())) * 37 + ((this.id == null) ? 0 : this.id.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
