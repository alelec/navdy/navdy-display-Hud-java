package com.navdy.service.library.events.glances;

final public class CannedMessagesRequest extends com.squareup.wire.Message {
    final public static Long DEFAULT_SERIAL_NUMBER;
    final private static long serialVersionUID = 0L;
    final public Long serial_number;
    
    static {
        DEFAULT_SERIAL_NUMBER = Long.valueOf(0L);
    }
    
    private CannedMessagesRequest(com.navdy.service.library.events.glances.CannedMessagesRequest$Builder a) {
        this(a.serial_number);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    CannedMessagesRequest(com.navdy.service.library.events.glances.CannedMessagesRequest$Builder a, com.navdy.service.library.events.glances.CannedMessagesRequest$1 a0) {
        this(a);
    }
    
    public CannedMessagesRequest(Long a) {
        this.serial_number = a;
    }
    
    public boolean equals(Object a) {
        return a == this || a instanceof com.navdy.service.library.events.glances.CannedMessagesRequest && this.equals(this.serial_number, ((com.navdy.service.library.events.glances.CannedMessagesRequest)a).serial_number);
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (this.serial_number == null) ? 0 : this.serial_number.hashCode();
            this.hashCode = i;
        }
        return i;
    }
}
