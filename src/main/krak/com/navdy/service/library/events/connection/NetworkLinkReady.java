package com.navdy.service.library.events.connection;

final public class NetworkLinkReady extends com.squareup.wire.Message {
    final private static long serialVersionUID = 0L;
    
    public NetworkLinkReady() {
    }
    
    private NetworkLinkReady(com.navdy.service.library.events.connection.NetworkLinkReady$Builder a) {
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    NetworkLinkReady(com.navdy.service.library.events.connection.NetworkLinkReady$Builder a, com.navdy.service.library.events.connection.NetworkLinkReady$1 a0) {
        this(a);
    }
    
    public boolean equals(Object a) {
        return a instanceof com.navdy.service.library.events.connection.NetworkLinkReady;
    }
    
    public int hashCode() {
        return 0;
    }
}
