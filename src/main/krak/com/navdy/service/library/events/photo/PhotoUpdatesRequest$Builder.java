package com.navdy.service.library.events.photo;

final public class PhotoUpdatesRequest$Builder extends com.squareup.wire.Message.Builder {
    public com.navdy.service.library.events.photo.PhotoType photoType;
    public Boolean start;
    
    public PhotoUpdatesRequest$Builder() {
    }
    
    public PhotoUpdatesRequest$Builder(com.navdy.service.library.events.photo.PhotoUpdatesRequest a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.start = a.start;
            this.photoType = a.photoType;
        }
    }
    
    public com.navdy.service.library.events.photo.PhotoUpdatesRequest build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.photo.PhotoUpdatesRequest(this, (com.navdy.service.library.events.photo.PhotoUpdatesRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.photo.PhotoUpdatesRequest$Builder photoType(com.navdy.service.library.events.photo.PhotoType a) {
        this.photoType = a;
        return this;
    }
    
    public com.navdy.service.library.events.photo.PhotoUpdatesRequest$Builder start(Boolean a) {
        this.start = a;
        return this;
    }
}
