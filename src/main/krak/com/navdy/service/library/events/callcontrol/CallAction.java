package com.navdy.service.library.events.callcontrol;

final public class CallAction extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.callcontrol.CallAction[] $VALUES;
    final public static com.navdy.service.library.events.callcontrol.CallAction CALL_ACCEPT;
    final public static com.navdy.service.library.events.callcontrol.CallAction CALL_DIAL;
    final public static com.navdy.service.library.events.callcontrol.CallAction CALL_END;
    final public static com.navdy.service.library.events.callcontrol.CallAction CALL_MUTE;
    final public static com.navdy.service.library.events.callcontrol.CallAction CALL_REJECT;
    final public static com.navdy.service.library.events.callcontrol.CallAction CALL_UNMUTE;
    final private int value;
    
    static {
        CALL_ACCEPT = new com.navdy.service.library.events.callcontrol.CallAction("CALL_ACCEPT", 0, 1);
        CALL_END = new com.navdy.service.library.events.callcontrol.CallAction("CALL_END", 1, 2);
        CALL_DIAL = new com.navdy.service.library.events.callcontrol.CallAction("CALL_DIAL", 2, 3);
        CALL_MUTE = new com.navdy.service.library.events.callcontrol.CallAction("CALL_MUTE", 3, 4);
        CALL_UNMUTE = new com.navdy.service.library.events.callcontrol.CallAction("CALL_UNMUTE", 4, 5);
        CALL_REJECT = new com.navdy.service.library.events.callcontrol.CallAction("CALL_REJECT", 5, 6);
        com.navdy.service.library.events.callcontrol.CallAction[] a = new com.navdy.service.library.events.callcontrol.CallAction[6];
        a[0] = CALL_ACCEPT;
        a[1] = CALL_END;
        a[2] = CALL_DIAL;
        a[3] = CALL_MUTE;
        a[4] = CALL_UNMUTE;
        a[5] = CALL_REJECT;
        $VALUES = a;
    }
    
    private CallAction(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.callcontrol.CallAction valueOf(String s) {
        return (com.navdy.service.library.events.callcontrol.CallAction)Enum.valueOf(com.navdy.service.library.events.callcontrol.CallAction.class, s);
    }
    
    public static com.navdy.service.library.events.callcontrol.CallAction[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
