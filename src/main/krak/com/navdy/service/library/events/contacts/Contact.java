package com.navdy.service.library.events.contacts;

final public class Contact extends com.squareup.wire.Message {
    final public static String DEFAULT_LABEL = "";
    final public static String DEFAULT_NAME = "";
    final public static String DEFAULT_NUMBER = "";
    final public static com.navdy.service.library.events.contacts.PhoneNumberType DEFAULT_NUMBERTYPE;
    final private static long serialVersionUID = 0L;
    final public String label;
    final public String name;
    final public String number;
    final public com.navdy.service.library.events.contacts.PhoneNumberType numberType;
    
    static {
        DEFAULT_NUMBERTYPE = com.navdy.service.library.events.contacts.PhoneNumberType.PHONE_NUMBER_HOME;
    }
    
    private Contact(com.navdy.service.library.events.contacts.Contact$Builder a) {
        this(a.name, a.number, a.numberType, a.label);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    Contact(com.navdy.service.library.events.contacts.Contact$Builder a, com.navdy.service.library.events.contacts.Contact$1 a0) {
        this(a);
    }
    
    public Contact(String s, String s0, com.navdy.service.library.events.contacts.PhoneNumberType a, String s1) {
        this.name = s;
        this.number = s0;
        this.numberType = a;
        this.label = s1;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.contacts.Contact) {
                com.navdy.service.library.events.contacts.Contact a0 = (com.navdy.service.library.events.contacts.Contact)a;
                boolean b0 = this.equals(this.name, a0.name);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.number, a0.number)) {
                        break label1;
                    }
                    if (!this.equals(this.numberType, a0.numberType)) {
                        break label1;
                    }
                    if (this.equals(this.label, a0.label)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((((this.name == null) ? 0 : this.name.hashCode()) * 37 + ((this.number == null) ? 0 : this.number.hashCode())) * 37 + ((this.numberType == null) ? 0 : this.numberType.hashCode())) * 37 + ((this.label == null) ? 0 : this.label.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
