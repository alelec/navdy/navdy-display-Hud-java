package com.navdy.service.library.events.connection;

final public class DisconnectRequest extends com.squareup.wire.Message {
    final public static Boolean DEFAULT_FORGET;
    final private static long serialVersionUID = 0L;
    final public Boolean forget;
    
    static {
        DEFAULT_FORGET = Boolean.valueOf(false);
    }
    
    private DisconnectRequest(com.navdy.service.library.events.connection.DisconnectRequest$Builder a) {
        this(a.forget);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    DisconnectRequest(com.navdy.service.library.events.connection.DisconnectRequest$Builder a, com.navdy.service.library.events.connection.DisconnectRequest$1 a0) {
        this(a);
    }
    
    public DisconnectRequest(Boolean a) {
        this.forget = a;
    }
    
    public boolean equals(Object a) {
        return a == this || a instanceof com.navdy.service.library.events.connection.DisconnectRequest && this.equals(this.forget, ((com.navdy.service.library.events.connection.DisconnectRequest)a).forget);
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (this.forget == null) ? 0 : this.forget.hashCode();
            this.hashCode = i;
        }
        return i;
    }
}
