package com.navdy.service.library.events.dial;

final public class DialError extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.dial.DialError[] $VALUES;
    final public static com.navdy.service.library.events.dial.DialError DIAL_ALREADY_PAIRED;
    final public static com.navdy.service.library.events.dial.DialError DIAL_ERROR;
    final public static com.navdy.service.library.events.dial.DialError DIAL_NOT_FOUND;
    final public static com.navdy.service.library.events.dial.DialError DIAL_NOT_PAIRED;
    final public static com.navdy.service.library.events.dial.DialError DIAL_OPERATION_IN_PROGRESS;
    final public static com.navdy.service.library.events.dial.DialError DIAL_PAIRED;
    final private int value;
    
    static {
        DIAL_ERROR = new com.navdy.service.library.events.dial.DialError("DIAL_ERROR", 0, 1);
        DIAL_ALREADY_PAIRED = new com.navdy.service.library.events.dial.DialError("DIAL_ALREADY_PAIRED", 1, 2);
        DIAL_OPERATION_IN_PROGRESS = new com.navdy.service.library.events.dial.DialError("DIAL_OPERATION_IN_PROGRESS", 2, 3);
        DIAL_PAIRED = new com.navdy.service.library.events.dial.DialError("DIAL_PAIRED", 3, 4);
        DIAL_NOT_FOUND = new com.navdy.service.library.events.dial.DialError("DIAL_NOT_FOUND", 4, 5);
        DIAL_NOT_PAIRED = new com.navdy.service.library.events.dial.DialError("DIAL_NOT_PAIRED", 5, 6);
        com.navdy.service.library.events.dial.DialError[] a = new com.navdy.service.library.events.dial.DialError[6];
        a[0] = DIAL_ERROR;
        a[1] = DIAL_ALREADY_PAIRED;
        a[2] = DIAL_OPERATION_IN_PROGRESS;
        a[3] = DIAL_PAIRED;
        a[4] = DIAL_NOT_FOUND;
        a[5] = DIAL_NOT_PAIRED;
        $VALUES = a;
    }
    
    private DialError(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.dial.DialError valueOf(String s) {
        return (com.navdy.service.library.events.dial.DialError)Enum.valueOf(com.navdy.service.library.events.dial.DialError.class, s);
    }
    
    public static com.navdy.service.library.events.dial.DialError[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
