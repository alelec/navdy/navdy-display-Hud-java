package com.navdy.service.library.events.audio;

final public class MusicCapability$MusicAuthorizationStatus extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.audio.MusicCapability$MusicAuthorizationStatus[] $VALUES;
    final public static com.navdy.service.library.events.audio.MusicCapability$MusicAuthorizationStatus MUSIC_AUTHORIZATION_AUTHORIZED;
    final public static com.navdy.service.library.events.audio.MusicCapability$MusicAuthorizationStatus MUSIC_AUTHORIZATION_DENIED;
    final public static com.navdy.service.library.events.audio.MusicCapability$MusicAuthorizationStatus MUSIC_AUTHORIZATION_NOT_DETERMINED;
    final private int value;
    
    static {
        MUSIC_AUTHORIZATION_NOT_DETERMINED = new com.navdy.service.library.events.audio.MusicCapability$MusicAuthorizationStatus("MUSIC_AUTHORIZATION_NOT_DETERMINED", 0, 1);
        MUSIC_AUTHORIZATION_AUTHORIZED = new com.navdy.service.library.events.audio.MusicCapability$MusicAuthorizationStatus("MUSIC_AUTHORIZATION_AUTHORIZED", 1, 2);
        MUSIC_AUTHORIZATION_DENIED = new com.navdy.service.library.events.audio.MusicCapability$MusicAuthorizationStatus("MUSIC_AUTHORIZATION_DENIED", 2, 3);
        com.navdy.service.library.events.audio.MusicCapability$MusicAuthorizationStatus[] a = new com.navdy.service.library.events.audio.MusicCapability$MusicAuthorizationStatus[3];
        a[0] = MUSIC_AUTHORIZATION_NOT_DETERMINED;
        a[1] = MUSIC_AUTHORIZATION_AUTHORIZED;
        a[2] = MUSIC_AUTHORIZATION_DENIED;
        $VALUES = a;
    }
    
    private MusicCapability$MusicAuthorizationStatus(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.audio.MusicCapability$MusicAuthorizationStatus valueOf(String s) {
        return (com.navdy.service.library.events.audio.MusicCapability$MusicAuthorizationStatus)Enum.valueOf(com.navdy.service.library.events.audio.MusicCapability$MusicAuthorizationStatus.class, s);
    }
    
    public static com.navdy.service.library.events.audio.MusicCapability$MusicAuthorizationStatus[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
