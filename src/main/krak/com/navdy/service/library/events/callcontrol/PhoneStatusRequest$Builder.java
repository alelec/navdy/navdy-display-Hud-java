package com.navdy.service.library.events.callcontrol;

final public class PhoneStatusRequest$Builder extends com.squareup.wire.Message.Builder {
    public PhoneStatusRequest$Builder() {
    }
    
    public PhoneStatusRequest$Builder(com.navdy.service.library.events.callcontrol.PhoneStatusRequest a) {
        super((com.squareup.wire.Message)a);
    }
    
    public com.navdy.service.library.events.callcontrol.PhoneStatusRequest build() {
        return new com.navdy.service.library.events.callcontrol.PhoneStatusRequest(this, (com.navdy.service.library.events.callcontrol.PhoneStatusRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
}
