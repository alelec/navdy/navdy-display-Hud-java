package com.navdy.service.library.events.file;

final public class FileTransferRequest$Builder extends com.squareup.wire.Message.Builder {
    public String destinationFileName;
    public String fileDataChecksum;
    public Long fileSize;
    public com.navdy.service.library.events.file.FileType fileType;
    public Long offset;
    public Boolean override;
    public Boolean supportsAcks;
    
    public FileTransferRequest$Builder() {
    }
    
    public FileTransferRequest$Builder(com.navdy.service.library.events.file.FileTransferRequest a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.fileType = a.fileType;
            this.destinationFileName = a.destinationFileName;
            this.fileSize = a.fileSize;
            this.offset = a.offset;
            this.fileDataChecksum = a.fileDataChecksum;
            this.override = a.override;
            this.supportsAcks = a.supportsAcks;
        }
    }
    
    public com.navdy.service.library.events.file.FileTransferRequest build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.file.FileTransferRequest(this, (com.navdy.service.library.events.file.FileTransferRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.file.FileTransferRequest$Builder destinationFileName(String s) {
        this.destinationFileName = s;
        return this;
    }
    
    public com.navdy.service.library.events.file.FileTransferRequest$Builder fileDataChecksum(String s) {
        this.fileDataChecksum = s;
        return this;
    }
    
    public com.navdy.service.library.events.file.FileTransferRequest$Builder fileSize(Long a) {
        this.fileSize = a;
        return this;
    }
    
    public com.navdy.service.library.events.file.FileTransferRequest$Builder fileType(com.navdy.service.library.events.file.FileType a) {
        this.fileType = a;
        return this;
    }
    
    public com.navdy.service.library.events.file.FileTransferRequest$Builder offset(Long a) {
        this.offset = a;
        return this;
    }
    
    public com.navdy.service.library.events.file.FileTransferRequest$Builder override(Boolean a) {
        this.override = a;
        return this;
    }
    
    public com.navdy.service.library.events.file.FileTransferRequest$Builder supportsAcks(Boolean a) {
        this.supportsAcks = a;
        return this;
    }
}
