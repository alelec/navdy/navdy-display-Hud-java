package com.navdy.service.library.events.input;

final public class MediaRemoteKeyEvent extends com.squareup.wire.Message {
    final public static com.navdy.service.library.events.input.KeyEvent DEFAULT_ACTION;
    final public static com.navdy.service.library.events.input.MediaRemoteKey DEFAULT_KEY;
    final private static long serialVersionUID = 0L;
    final public com.navdy.service.library.events.input.KeyEvent action;
    final public com.navdy.service.library.events.input.MediaRemoteKey key;
    
    static {
        DEFAULT_KEY = com.navdy.service.library.events.input.MediaRemoteKey.MEDIA_REMOTE_KEY_SIRI;
        DEFAULT_ACTION = com.navdy.service.library.events.input.KeyEvent.KEY_DOWN;
    }
    
    public MediaRemoteKeyEvent(com.navdy.service.library.events.input.MediaRemoteKey a, com.navdy.service.library.events.input.KeyEvent a0) {
        this.key = a;
        this.action = a0;
    }
    
    private MediaRemoteKeyEvent(com.navdy.service.library.events.input.MediaRemoteKeyEvent$Builder a) {
        this(a.key, a.action);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    MediaRemoteKeyEvent(com.navdy.service.library.events.input.MediaRemoteKeyEvent$Builder a, com.navdy.service.library.events.input.MediaRemoteKeyEvent$1 a0) {
        this(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.input.MediaRemoteKeyEvent) {
                com.navdy.service.library.events.input.MediaRemoteKeyEvent a0 = (com.navdy.service.library.events.input.MediaRemoteKeyEvent)a;
                boolean b0 = this.equals(this.key, a0.key);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (this.equals(this.action, a0.action)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((this.key == null) ? 0 : this.key.hashCode()) * 37 + ((this.action == null) ? 0 : this.action.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
