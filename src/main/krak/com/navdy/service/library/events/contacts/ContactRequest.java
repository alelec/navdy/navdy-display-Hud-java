package com.navdy.service.library.events.contacts;

final public class ContactRequest extends com.squareup.wire.Message {
    final public static String DEFAULT_IDENTIFIER = "";
    final private static long serialVersionUID = 0L;
    final public String identifier;
    
    private ContactRequest(com.navdy.service.library.events.contacts.ContactRequest$Builder a) {
        this(a.identifier);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    ContactRequest(com.navdy.service.library.events.contacts.ContactRequest$Builder a, com.navdy.service.library.events.contacts.ContactRequest$1 a0) {
        this(a);
    }
    
    public ContactRequest(String s) {
        this.identifier = s;
    }
    
    public boolean equals(Object a) {
        return a == this || a instanceof com.navdy.service.library.events.contacts.ContactRequest && this.equals(this.identifier, ((com.navdy.service.library.events.contacts.ContactRequest)a).identifier);
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (this.identifier == null) ? 0 : this.identifier.hashCode();
            this.hashCode = i;
        }
        return i;
    }
}
