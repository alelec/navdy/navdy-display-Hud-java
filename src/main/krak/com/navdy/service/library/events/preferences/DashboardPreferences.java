package com.navdy.service.library.events.preferences;

final public class DashboardPreferences extends com.squareup.wire.Message {
    final public static String DEFAULT_LEFTGAUGEID = "";
    final public static com.navdy.service.library.events.preferences.MiddleGauge DEFAULT_MIDDLEGAUGE;
    final public static String DEFAULT_RIGHTGAUGEID = "";
    final public static com.navdy.service.library.events.preferences.ScrollableSide DEFAULT_SCROLLABLESIDE;
    final private static long serialVersionUID = 0L;
    final public String leftGaugeId;
    final public com.navdy.service.library.events.preferences.MiddleGauge middleGauge;
    final public String rightGaugeId;
    final public com.navdy.service.library.events.preferences.ScrollableSide scrollableSide;
    
    static {
        DEFAULT_MIDDLEGAUGE = com.navdy.service.library.events.preferences.MiddleGauge.SPEEDOMETER;
        DEFAULT_SCROLLABLESIDE = com.navdy.service.library.events.preferences.ScrollableSide.LEFT;
    }
    
    private DashboardPreferences(com.navdy.service.library.events.preferences.DashboardPreferences$Builder a) {
        this(a.middleGauge, a.scrollableSide, a.rightGaugeId, a.leftGaugeId);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    DashboardPreferences(com.navdy.service.library.events.preferences.DashboardPreferences$Builder a, com.navdy.service.library.events.preferences.DashboardPreferences$1 a0) {
        this(a);
    }
    
    public DashboardPreferences(com.navdy.service.library.events.preferences.MiddleGauge a, com.navdy.service.library.events.preferences.ScrollableSide a0, String s, String s0) {
        this.middleGauge = a;
        this.scrollableSide = a0;
        this.rightGaugeId = s;
        this.leftGaugeId = s0;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.preferences.DashboardPreferences) {
                com.navdy.service.library.events.preferences.DashboardPreferences a0 = (com.navdy.service.library.events.preferences.DashboardPreferences)a;
                boolean b0 = this.equals(this.middleGauge, a0.middleGauge);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.scrollableSide, a0.scrollableSide)) {
                        break label1;
                    }
                    if (!this.equals(this.rightGaugeId, a0.rightGaugeId)) {
                        break label1;
                    }
                    if (this.equals(this.leftGaugeId, a0.leftGaugeId)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((((this.middleGauge == null) ? 0 : this.middleGauge.hashCode()) * 37 + ((this.scrollableSide == null) ? 0 : this.scrollableSide.hashCode())) * 37 + ((this.rightGaugeId == null) ? 0 : this.rightGaugeId.hashCode())) * 37 + ((this.leftGaugeId == null) ? 0 : this.leftGaugeId.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
