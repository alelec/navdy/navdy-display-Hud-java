package com.navdy.service.library.device.discovery;

abstract public interface RemoteDeviceBroadcaster {
    abstract public boolean start();
    
    
    abstract public boolean stop();
}
