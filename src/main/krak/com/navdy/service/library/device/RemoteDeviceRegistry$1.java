package com.navdy.service.library.device;

class RemoteDeviceRegistry$1 implements com.navdy.service.library.device.RemoteDeviceRegistry$DeviceListEventDispatcher {
    final com.navdy.service.library.device.RemoteDeviceRegistry this$0;
    
    RemoteDeviceRegistry$1(com.navdy.service.library.device.RemoteDeviceRegistry a) {
        super();
        this.this$0 = a;
    }
    
    public void dispatchEvent(com.navdy.service.library.device.RemoteDeviceRegistry a, com.navdy.service.library.device.RemoteDeviceRegistry$DeviceListUpdatedListener a0) {
        a0.onDeviceListChanged((java.util.Set)new java.util.HashSet((java.util.Collection)this.this$0.mKnownConnectionInfo));
    }
    
    public void dispatchEvent(com.navdy.service.library.util.Listenable a, com.navdy.service.library.util.Listenable$Listener a0) {
        this.dispatchEvent((com.navdy.service.library.device.RemoteDeviceRegistry)a, (com.navdy.service.library.device.RemoteDeviceRegistry$DeviceListUpdatedListener)a0);
    }
}
