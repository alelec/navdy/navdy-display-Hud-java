package com.navdy.service.library.device;


public enum NavdyDeviceId$Type {
    UNK(0),
    BT(1),
    EMU(2),
    EA(3);

    private int value;
    NavdyDeviceId$Type(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class NavdyDeviceId$Type extends Enum {
//    final private static com.navdy.service.library.device.NavdyDeviceId$Type[] $VALUES;
//    final public static com.navdy.service.library.device.NavdyDeviceId$Type BT;
//    final public static com.navdy.service.library.device.NavdyDeviceId$Type EA;
//    final public static com.navdy.service.library.device.NavdyDeviceId$Type EMU;
//    final public static com.navdy.service.library.device.NavdyDeviceId$Type UNK;
//    
//    static {
//        UNK = new com.navdy.service.library.device.NavdyDeviceId$Type("UNK", 0);
//        BT = new com.navdy.service.library.device.NavdyDeviceId$Type("BT", 1);
//        EMU = new com.navdy.service.library.device.NavdyDeviceId$Type("EMU", 2);
//        EA = new com.navdy.service.library.device.NavdyDeviceId$Type("EA", 3);
//        com.navdy.service.library.device.NavdyDeviceId$Type[] a = new com.navdy.service.library.device.NavdyDeviceId$Type[4];
//        a[0] = UNK;
//        a[1] = BT;
//        a[2] = EMU;
//        a[3] = EA;
//        $VALUES = a;
//    }
//    
//    private NavdyDeviceId$Type(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.service.library.device.NavdyDeviceId$Type valueOf(String s) {
//        return (com.navdy.service.library.device.NavdyDeviceId$Type)Enum.valueOf(com.navdy.service.library.device.NavdyDeviceId$Type.class, s);
//    }
//    
//    public static com.navdy.service.library.device.NavdyDeviceId$Type[] values() {
//        return $VALUES.clone();
//    }
//}
//