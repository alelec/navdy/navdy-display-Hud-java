package com.navdy.service.library.device;

public class NavdyDeviceId$Adapter extends com.google.gson.TypeAdapter {
    public NavdyDeviceId$Adapter() {
    }
    
    public Object read(com.google.gson.stream.JsonReader a) {
        return new com.navdy.service.library.device.NavdyDeviceId(a.nextString());
    }
    
    public void write(com.google.gson.stream.JsonWriter a, Object a0) {
        a.value(a0.toString());
    }
}
