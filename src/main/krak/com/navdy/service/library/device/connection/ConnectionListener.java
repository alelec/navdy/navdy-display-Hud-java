package com.navdy.service.library.device.connection;

abstract public class ConnectionListener extends com.navdy.service.library.util.Listenable {
    final protected com.navdy.service.library.log.Logger logger;
    com.navdy.service.library.device.connection.ConnectionListener$AcceptThread mAcceptThread;
    final protected android.content.Context mContext;
    
    public ConnectionListener(android.content.Context a, String s) {
        this.mContext = a;
        this.logger = new com.navdy.service.library.log.Logger(s);
    }
    
    public void dispatchConnected(com.navdy.service.library.device.connection.Connection a) {
        this.dispatchToListeners((com.navdy.service.library.util.Listenable$EventDispatcher)new com.navdy.service.library.device.connection.ConnectionListener$4(this, a));
    }
    
    public void dispatchConnectionFailed() {
        this.dispatchToListeners((com.navdy.service.library.util.Listenable$EventDispatcher)new com.navdy.service.library.device.connection.ConnectionListener$5(this));
    }
    
    public void dispatchStartFailure() {
        this.dispatchToListeners((com.navdy.service.library.util.Listenable$EventDispatcher)new com.navdy.service.library.device.connection.ConnectionListener$2(this));
    }
    
    public void dispatchStarted() {
        this.dispatchToListeners((com.navdy.service.library.util.Listenable$EventDispatcher)new com.navdy.service.library.device.connection.ConnectionListener$1(this));
    }
    
    public void dispatchStopped() {
        this.dispatchToListeners((com.navdy.service.library.util.Listenable$EventDispatcher)new com.navdy.service.library.device.connection.ConnectionListener$3(this));
    }
    
    abstract protected com.navdy.service.library.device.connection.ConnectionListener$AcceptThread getNewAcceptThread();
    
    
    abstract public com.navdy.service.library.device.connection.ConnectionType getType();
    
    
    public boolean start() {
        boolean b = false;
        label1: synchronized(this) {
            com.navdy.service.library.device.connection.ConnectionListener$AcceptThread a = this.mAcceptThread;
            label3: {
                if (a == null) {
                    break label3;
                }
                boolean b0 = this.mAcceptThread.isAlive();
                label2: {
                    if (b0) {
                        break label2;
                    }
                    this.logger.e("clearing existing accept thread");
                    this.mAcceptThread.cancel();
                    this.mAcceptThread = null;
                    break label3;
                }
                this.logger.e("Already running");
                b = false;
                break label1;
            }
            label0: {
                Throwable a0 = null;
                try {
                    this.mAcceptThread = this.getNewAcceptThread();
                    break label0;
                } catch(Throwable a1) {
                    a0 = a1;
                }
                this.logger.e("Unable to start accept thread: ", a0);
                this.dispatchStartFailure();
                b = false;
                break label1;
            }
            if (this.mAcceptThread != null) {
                this.mAcceptThread.start();
            }
            b = true;
        }
        /*monexit(this)*/;
        return b;
    }
    
    public boolean stop() {
        boolean b = false;
        synchronized(this) {
            if (this.mAcceptThread != null) {
                this.mAcceptThread.cancel();
                this.mAcceptThread = null;
                b = true;
            } else {
                this.logger.e("Already stopped.");
                b = false;
            }
        }
        /*monexit(this)*/;
        return b;
    }
}
