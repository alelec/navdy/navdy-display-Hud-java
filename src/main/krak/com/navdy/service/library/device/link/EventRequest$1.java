package com.navdy.service.library.device.link;

class EventRequest$1 implements Runnable {
    final com.navdy.service.library.device.link.EventRequest this$0;
    final com.navdy.service.library.device.RemoteDevice$PostEventStatus val$status;
    
    EventRequest$1(com.navdy.service.library.device.link.EventRequest a, com.navdy.service.library.device.RemoteDevice$PostEventStatus a0) {
        super();
        this.this$0 = a;
        this.val$status = a0;
    }
    
    public void run() {
        if (this.this$0.eventCompleteHandler != null) {
            this.this$0.eventCompleteHandler.onComplete(this.val$status);
        }
    }
}
