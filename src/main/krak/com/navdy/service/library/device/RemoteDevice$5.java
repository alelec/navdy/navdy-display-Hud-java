package com.navdy.service.library.device;

class RemoteDevice$5 implements com.navdy.service.library.device.RemoteDevice$EventDispatcher {
    final com.navdy.service.library.device.RemoteDevice this$0;
    final byte[] val$eventData;
    
    RemoteDevice$5(com.navdy.service.library.device.RemoteDevice a, byte[] a0) {
        super();
        this.this$0 = a;
        this.val$eventData = a0;
    }
    
    public void dispatchEvent(com.navdy.service.library.device.RemoteDevice a, com.navdy.service.library.device.RemoteDevice$Listener a0) {
        a0.onNavdyEventReceived(a, this.val$eventData);
    }
    
    public void dispatchEvent(com.navdy.service.library.util.Listenable a, com.navdy.service.library.util.Listenable$Listener a0) {
        this.dispatchEvent((com.navdy.service.library.device.RemoteDevice)a, (com.navdy.service.library.device.RemoteDevice$Listener)a0);
    }
}
