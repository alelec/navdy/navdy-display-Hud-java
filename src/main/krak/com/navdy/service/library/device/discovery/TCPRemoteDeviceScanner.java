package com.navdy.service.library.device.discovery;

public class TCPRemoteDeviceScanner extends com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner {
    final public static String TAG = "TCPRemoteDeviceScanner";
    
    public TCPRemoteDeviceScanner(android.content.Context a) {
        super(a, com.navdy.service.library.device.connection.ConnectionType.TCP_PROTOBUF.getServiceType());
    }
}
