package com.navdy.service.library.device.connection;

abstract public interface Connection$ConnectionFactory {
    abstract public com.navdy.service.library.device.connection.Connection build(android.content.Context arg, com.navdy.service.library.device.connection.ConnectionInfo arg0);
}
