package com.navdy.service.library.device.connection.tunnel;

class TransferThread extends Thread {
    final static com.navdy.service.library.log.Logger sLogger;
    final com.navdy.service.library.util.NetworkActivityTracker activityTracker;
    private volatile boolean canceled;
    final java.io.InputStream inStream;
    final java.io.OutputStream outStream;
    final com.navdy.service.library.device.connection.tunnel.Tunnel parentThread;
    final boolean sending;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.device.connection.tunnel.TransferThread.class);
    }
    
    public TransferThread(com.navdy.service.library.device.connection.tunnel.Tunnel a, java.io.InputStream a0, java.io.OutputStream a1, boolean b) {
        sLogger.v(new StringBuilder().append("new transfer thread (").append(this).append("): ").append(a0).append(" -> ").append(a1).toString());
        this.setName(com.navdy.service.library.device.connection.tunnel.TransferThread.class.getSimpleName());
        this.parentThread = a;
        this.inStream = a0;
        this.outStream = a1;
        this.sending = b;
        a.transferThreads.add(this);
        this.activityTracker = com.navdy.service.library.util.NetworkActivityTracker.getInstance();
    }
    
    public void cancel() {
        this.canceled = true;
        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)this.inStream);
        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)this.outStream);
        this.parentThread.transferThreads.remove(this);
    }
    
    public void run() {
        byte[] a = new byte[16384];
        try {
            while(true) {
                int i = this.inStream.read(a);
                if (i < 0) {
                    break;
                }
                if (sLogger.isLoggable(2)) {
                    java.util.Arrays.copyOf(a, i);
                    com.navdy.service.library.log.Logger a0 = sLogger;
                    Object[] a1 = new Object[4];
                    a1[0] = this.inStream;
                    a1[1] = this.outStream;
                    a1[2] = Integer.valueOf(i);
                    a1[3] = "";
                    a0.v(String.format("transfer (%s -> %s): %d bytes%s", a1));
                }
                this.outStream.write(a, 0, i);
                this.outStream.flush();
                if (this.sending) {
                    this.activityTracker.addBytesSent(i);
                } else {
                    this.activityTracker.addBytesReceived(i);
                }
            }
            sLogger.d("socket was closed");
        } catch(Throwable a2) {
            if (!this.canceled) {
                sLogger.e("Exception", a2);
            }
        }
        this.cancel();
    }
}
