package com.navdy.service.library.device;


public enum RemoteDevice$PostEventStatus {
    SUCCESS(0),
    DISCONNECTED(1),
    SEND_FAILED(2),
    UNKNOWN_FAILURE(3);

    private int value;
    RemoteDevice$PostEventStatus(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class RemoteDevice$PostEventStatus extends Enum {
//    final private static com.navdy.service.library.device.RemoteDevice$PostEventStatus[] $VALUES;
//    final public static com.navdy.service.library.device.RemoteDevice$PostEventStatus DISCONNECTED;
//    final public static com.navdy.service.library.device.RemoteDevice$PostEventStatus SEND_FAILED;
//    final public static com.navdy.service.library.device.RemoteDevice$PostEventStatus SUCCESS;
//    final public static com.navdy.service.library.device.RemoteDevice$PostEventStatus UNKNOWN_FAILURE;
//    
//    static {
//        SUCCESS = new com.navdy.service.library.device.RemoteDevice$PostEventStatus("SUCCESS", 0);
//        DISCONNECTED = new com.navdy.service.library.device.RemoteDevice$PostEventStatus("DISCONNECTED", 1);
//        SEND_FAILED = new com.navdy.service.library.device.RemoteDevice$PostEventStatus("SEND_FAILED", 2);
//        UNKNOWN_FAILURE = new com.navdy.service.library.device.RemoteDevice$PostEventStatus("UNKNOWN_FAILURE", 3);
//        com.navdy.service.library.device.RemoteDevice$PostEventStatus[] a = new com.navdy.service.library.device.RemoteDevice$PostEventStatus[4];
//        a[0] = SUCCESS;
//        a[1] = DISCONNECTED;
//        a[2] = SEND_FAILED;
//        a[3] = UNKNOWN_FAILURE;
//        $VALUES = a;
//    }
//    
//    private RemoteDevice$PostEventStatus(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.service.library.device.RemoteDevice$PostEventStatus valueOf(String s) {
//        return (com.navdy.service.library.device.RemoteDevice$PostEventStatus)Enum.valueOf(com.navdy.service.library.device.RemoteDevice$PostEventStatus.class, s);
//    }
//    
//    public static com.navdy.service.library.device.RemoteDevice$PostEventStatus[] values() {
//        return $VALUES.clone();
//    }
//}
//