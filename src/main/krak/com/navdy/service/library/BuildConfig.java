package com.navdy.service.library;

final public class BuildConfig {
    final public static String APPLICATION_ID = "com.navdy.service.library";
    final public static String BUILD_TYPE = "release";
    final public static boolean DEBUG = false;
    final public static String FLAVOR = "";
    final public static int VERSION_CODE = 1;
    final public static String VERSION_NAME = "1.0";
    
    public BuildConfig() {
    }
}
