package com.navdy.obd;

class IObdService$Stub$Proxy implements com.navdy.obd.IObdService {
    private android.os.IBinder mRemote;
    
    IObdService$Stub$Proxy(android.os.IBinder a) {
        this.mRemote = a;
    }
    
    public void addListener(com.navdy.obd.IObdServiceListener a) {
        android.os.Parcel a0 = android.os.Parcel.obtain();
        android.os.Parcel a1 = android.os.Parcel.obtain();
        try {
            a0.writeInterfaceToken("com.navdy.obd.IObdService");
            android.os.IBinder a2 = (a == null) ? null : a.asBinder();
            a0.writeStrongBinder(a2);
            this.mRemote.transact(12, a0, a1, 0);
            a1.readException();
        } catch(Throwable a3) {
            a1.recycle();
            a0.recycle();
            throw a3;
        }
        a1.recycle();
        a0.recycle();
    }
    
    public android.os.IBinder asBinder() {
        return this.mRemote;
    }
    
    public void connect(String s, com.navdy.obd.IObdServiceListener a) {
        android.os.Parcel a0 = android.os.Parcel.obtain();
        android.os.Parcel a1 = android.os.Parcel.obtain();
        try {
            a0.writeInterfaceToken("com.navdy.obd.IObdService");
            a0.writeString(s);
            android.os.IBinder a2 = (a == null) ? null : a.asBinder();
            a0.writeStrongBinder(a2);
            this.mRemote.transact(1, a0, a1, 0);
            a1.readException();
        } catch(Throwable a3) {
            a1.recycle();
            a0.recycle();
            throw a3;
        }
        a1.recycle();
        a0.recycle();
    }
    
    public void disconnect() {
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.IObdService");
            this.mRemote.transact(2, a, a0, 0);
            a0.readException();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
    }
    
    public String getFirmwareVersion() {
        String s = null;
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.IObdService");
            this.mRemote.transact(4, a, a0, 0);
            a0.readException();
            s = a0.readString();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
        return s;
    }
    
    public String getInterfaceDescriptor() {
        return "com.navdy.obd.IObdService";
    }
    
    public int getState() {
        int i = 0;
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.IObdService");
            this.mRemote.transact(3, a, a0, 0);
            a0.readException();
            i = a0.readInt();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
        return i;
    }
    
    public String readPid(int i) {
        String s = null;
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.IObdService");
            a.writeInt(i);
            this.mRemote.transact(7, a, a0, 0);
            a0.readException();
            s = a0.readString();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
        return s;
    }
    
    public void removeListener(com.navdy.obd.IObdServiceListener a) {
        android.os.Parcel a0 = android.os.Parcel.obtain();
        android.os.Parcel a1 = android.os.Parcel.obtain();
        try {
            a0.writeInterfaceToken("com.navdy.obd.IObdService");
            android.os.IBinder a2 = (a == null) ? null : a.asBinder();
            a0.writeStrongBinder(a2);
            this.mRemote.transact(13, a0, a1, 0);
            a1.readException();
        } catch(Throwable a3) {
            a1.recycle();
            a0.recycle();
            throw a3;
        }
        a1.recycle();
        a0.recycle();
    }
    
    public void reset() {
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.IObdService");
            this.mRemote.transact(6, a, a0, 0);
            a0.readException();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
    }
    
    public void scanPids(java.util.List a, int i) {
        android.os.Parcel a0 = android.os.Parcel.obtain();
        android.os.Parcel a1 = android.os.Parcel.obtain();
        try {
            a0.writeInterfaceToken("com.navdy.obd.IObdService");
            a0.writeTypedList(a);
            a0.writeInt(i);
            this.mRemote.transact(10, a0, a1, 0);
            a1.readException();
        } catch(Throwable a2) {
            a1.recycle();
            a0.recycle();
            throw a2;
        }
        a1.recycle();
        a0.recycle();
    }
    
    public void scanSupportedPids() {
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.IObdService");
            this.mRemote.transact(8, a, a0, 0);
            a0.readException();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
    }
    
    public void scanVIN() {
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.IObdService");
            this.mRemote.transact(9, a, a0, 0);
            a0.readException();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
    }
    
    public void startRawScan() {
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.IObdService");
            this.mRemote.transact(11, a, a0, 0);
            a0.readException();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
    }
    
    public boolean updateDeviceFirmware(String s, String s0) {
        int i = 0;
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.IObdService");
            a.writeString(s);
            a.writeString(s0);
            this.mRemote.transact(5, a, a0, 0);
            a0.readException();
            i = a0.readInt();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        boolean b = i != 0;
        a0.recycle();
        a.recycle();
        return b;
    }
}
