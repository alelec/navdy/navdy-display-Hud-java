package com.navdy.obd;

final class PidSet$1 implements android.os.Parcelable$Creator {
    PidSet$1() {
    }
    
    public com.navdy.obd.PidSet createFromParcel(android.os.Parcel a) {
        return new com.navdy.obd.PidSet(a);
    }
    
    public Object createFromParcel(android.os.Parcel a) {
        return this.createFromParcel(a);
    }
    
    public com.navdy.obd.PidSet[] newArray(int i) {
        return new com.navdy.obd.PidSet[i];
    }
    
    public Object[] newArray(int i) {
        return this.newArray(i);
    }
}
