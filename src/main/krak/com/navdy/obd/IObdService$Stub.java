package com.navdy.obd;

abstract public class IObdService$Stub extends android.os.Binder implements com.navdy.obd.IObdService {
    final private static String DESCRIPTOR = "com.navdy.obd.IObdService";
    final static int TRANSACTION_addListener = 12;
    final static int TRANSACTION_connect = 1;
    final static int TRANSACTION_disconnect = 2;
    final static int TRANSACTION_getFirmwareVersion = 4;
    final static int TRANSACTION_getState = 3;
    final static int TRANSACTION_readPid = 7;
    final static int TRANSACTION_removeListener = 13;
    final static int TRANSACTION_reset = 6;
    final static int TRANSACTION_scanPids = 10;
    final static int TRANSACTION_scanSupportedPids = 8;
    final static int TRANSACTION_scanVIN = 9;
    final static int TRANSACTION_startRawScan = 11;
    final static int TRANSACTION_updateDeviceFirmware = 5;
    
    public IObdService$Stub() {
        this.attachInterface((android.os.IInterface)this, "com.navdy.obd.IObdService");
    }
    
    public static com.navdy.obd.IObdService asInterface(android.os.IBinder a) {
        Object a0 = null;
        label1: if (a != null) {
            android.os.IInterface a1 = a.queryLocalInterface("com.navdy.obd.IObdService");
            label0: {
                if (a1 == null) {
                    break label0;
                }
                if (!(a1 instanceof com.navdy.obd.IObdService)) {
                    break label0;
                }
                a0 = a1;
                break label1;
            }
            a0 = new com.navdy.obd.IObdService$Stub$Proxy(a);
        } else {
            a0 = null;
        }
        return (com.navdy.obd.IObdService)a0;
    }
    
    public android.os.IBinder asBinder() {
        return (android.os.IBinder)this;
    }
    
    public boolean onTransact(int i, android.os.Parcel a, android.os.Parcel a0, int i0) {
        boolean b = false;
        switch(i) {
            case 1598968902: {
                a0.writeString("com.navdy.obd.IObdService");
                b = true;
                break;
            }
            case 13: {
                a.enforceInterface("com.navdy.obd.IObdService");
                this.removeListener(com.navdy.obd.IObdServiceListener$Stub.asInterface(a.readStrongBinder()));
                a0.writeNoException();
                b = true;
                break;
            }
            case 12: {
                a.enforceInterface("com.navdy.obd.IObdService");
                this.addListener(com.navdy.obd.IObdServiceListener$Stub.asInterface(a.readStrongBinder()));
                a0.writeNoException();
                b = true;
                break;
            }
            case 11: {
                a.enforceInterface("com.navdy.obd.IObdService");
                this.startRawScan();
                a0.writeNoException();
                b = true;
                break;
            }
            case 10: {
                a.enforceInterface("com.navdy.obd.IObdService");
                this.scanPids((java.util.List)a.createTypedArrayList(com.navdy.obd.Pid.CREATOR), a.readInt());
                a0.writeNoException();
                b = true;
                break;
            }
            case 9: {
                a.enforceInterface("com.navdy.obd.IObdService");
                this.scanVIN();
                a0.writeNoException();
                b = true;
                break;
            }
            case 8: {
                a.enforceInterface("com.navdy.obd.IObdService");
                this.scanSupportedPids();
                a0.writeNoException();
                b = true;
                break;
            }
            case 7: {
                a.enforceInterface("com.navdy.obd.IObdService");
                String s = this.readPid(a.readInt());
                a0.writeNoException();
                a0.writeString(s);
                b = true;
                break;
            }
            case 6: {
                a.enforceInterface("com.navdy.obd.IObdService");
                this.reset();
                a0.writeNoException();
                b = true;
                break;
            }
            case 5: {
                a.enforceInterface("com.navdy.obd.IObdService");
                boolean b0 = this.updateDeviceFirmware(a.readString(), a.readString());
                a0.writeNoException();
                a0.writeInt(b0 ? 1 : 0);
                b = true;
                break;
            }
            case 4: {
                a.enforceInterface("com.navdy.obd.IObdService");
                String s0 = this.getFirmwareVersion();
                a0.writeNoException();
                a0.writeString(s0);
                b = true;
                break;
            }
            case 3: {
                a.enforceInterface("com.navdy.obd.IObdService");
                int i1 = this.getState();
                a0.writeNoException();
                a0.writeInt(i1);
                b = true;
                break;
            }
            case 2: {
                a.enforceInterface("com.navdy.obd.IObdService");
                this.disconnect();
                a0.writeNoException();
                b = true;
                break;
            }
            case 1: {
                a.enforceInterface("com.navdy.obd.IObdService");
                this.connect(a.readString(), com.navdy.obd.IObdServiceListener$Stub.asInterface(a.readStrongBinder()));
                a0.writeNoException();
                b = true;
                break;
            }
            default: {
                b = super.onTransact(i, a, a0, i0);
            }
        }
        return b;
    }
}
