package com.navdy.obd;

class ICarService$Stub$Proxy implements com.navdy.obd.ICarService {
    private android.os.IBinder mRemote;
    
    ICarService$Stub$Proxy(android.os.IBinder a) {
        this.mRemote = a;
    }
    
    public void addListener(java.util.List a, com.navdy.obd.IPidListener a0) {
        android.os.Parcel a1 = android.os.Parcel.obtain();
        android.os.Parcel a2 = android.os.Parcel.obtain();
        try {
            a1.writeInterfaceToken("com.navdy.obd.ICarService");
            a1.writeTypedList(a);
            android.os.IBinder a3 = (a0 == null) ? null : a0.asBinder();
            a1.writeStrongBinder(a3);
            this.mRemote.transact(6, a1, a2, 0);
            a2.readException();
        } catch(Throwable a4) {
            a2.recycle();
            a1.recycle();
            throw a4;
        }
        a2.recycle();
        a1.recycle();
    }
    
    public boolean applyConfiguration(String s) {
        int i = 0;
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.ICarService");
            a.writeString(s);
            this.mRemote.transact(10, a, a0, 0);
            a0.readException();
            i = a0.readInt();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        boolean b = i != 0;
        a0.recycle();
        a.recycle();
        return b;
    }
    
    public android.os.IBinder asBinder() {
        return this.mRemote;
    }
    
    public double getBatteryVoltage() {
        double d = 0.0;
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.ICarService");
            this.mRemote.transact(13, a, a0, 0);
            a0.readException();
            d = a0.readDouble();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
        return d;
    }
    
    public int getConnectionState() {
        int i = 0;
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.ICarService");
            this.mRemote.transact(1, a, a0, 0);
            a0.readException();
            i = a0.readInt();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
        return i;
    }
    
    public String getCurrentConfigurationName() {
        String s = null;
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.ICarService");
            this.mRemote.transact(9, a, a0, 0);
            a0.readException();
            s = a0.readString();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
        return s;
    }
    
    public java.util.List getEcus() {
        java.util.ArrayList a = null;
        android.os.Parcel a0 = android.os.Parcel.obtain();
        android.os.Parcel a1 = android.os.Parcel.obtain();
        try {
            a0.writeInterfaceToken("com.navdy.obd.ICarService");
            this.mRemote.transact(3, a0, a1, 0);
            a1.readException();
            a = a1.createTypedArrayList(com.navdy.obd.ECU.CREATOR);
        } catch(Throwable a2) {
            a1.recycle();
            a0.recycle();
            throw a2;
        }
        a1.recycle();
        a0.recycle();
        return (java.util.List)a;
    }
    
    public String getInterfaceDescriptor() {
        return "com.navdy.obd.ICarService";
    }
    
    public int getMode() {
        int i = 0;
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.ICarService");
            this.mRemote.transact(21, a, a0, 0);
            a0.readException();
            i = a0.readInt();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
        return i;
    }
    
    public String getObdChipFirmwareVersion() {
        String s = null;
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.ICarService");
            this.mRemote.transact(19, a, a0, 0);
            a0.readException();
            s = a0.readString();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
        return s;
    }
    
    public String getProtocol() {
        String s = null;
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.ICarService");
            this.mRemote.transact(4, a, a0, 0);
            a0.readException();
            s = a0.readString();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
        return s;
    }
    
    public java.util.List getReadings(java.util.List a) {
        java.util.ArrayList a0 = null;
        android.os.Parcel a1 = android.os.Parcel.obtain();
        android.os.Parcel a2 = android.os.Parcel.obtain();
        try {
            a1.writeInterfaceToken("com.navdy.obd.ICarService");
            a1.writeTypedList(a);
            this.mRemote.transact(5, a1, a2, 0);
            a2.readException();
            a0 = a2.createTypedArrayList(com.navdy.obd.Pid.CREATOR);
        } catch(Throwable a3) {
            a2.recycle();
            a1.recycle();
            throw a3;
        }
        a2.recycle();
        a1.recycle();
        return (java.util.List)a0;
    }
    
    public java.util.List getSupportedPids() {
        java.util.ArrayList a = null;
        android.os.Parcel a0 = android.os.Parcel.obtain();
        android.os.Parcel a1 = android.os.Parcel.obtain();
        try {
            a0.writeInterfaceToken("com.navdy.obd.ICarService");
            this.mRemote.transact(2, a0, a1, 0);
            a1.readException();
            a = a1.createTypedArrayList(com.navdy.obd.Pid.CREATOR);
        } catch(Throwable a2) {
            a1.recycle();
            a0.recycle();
            throw a2;
        }
        a1.recycle();
        a0.recycle();
        return (java.util.List)a;
    }
    
    public java.util.List getTroubleCodes() {
        java.util.ArrayList a = null;
        android.os.Parcel a0 = android.os.Parcel.obtain();
        android.os.Parcel a1 = android.os.Parcel.obtain();
        try {
            a0.writeInterfaceToken("com.navdy.obd.ICarService");
            this.mRemote.transact(27, a0, a1, 0);
            a1.readException();
            a = a1.createStringArrayList();
        } catch(Throwable a2) {
            a1.recycle();
            a0.recycle();
            throw a2;
        }
        a1.recycle();
        a0.recycle();
        return (java.util.List)a;
    }
    
    public String getVIN() {
        String s = null;
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.ICarService");
            this.mRemote.transact(8, a, a0, 0);
            a0.readException();
            s = a0.readString();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
        return s;
    }
    
    public boolean isCheckEngineLightOn() {
        int i = 0;
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.ICarService");
            this.mRemote.transact(26, a, a0, 0);
            a0.readException();
            i = a0.readInt();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        boolean b = i != 0;
        a0.recycle();
        a.recycle();
        return b;
    }
    
    public boolean isObdPidsScanningEnabled() {
        int i = 0;
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.ICarService");
            this.mRemote.transact(15, a, a0, 0);
            a0.readException();
            i = a0.readInt();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        boolean b = i != 0;
        a0.recycle();
        a.recycle();
        return b;
    }
    
    public void removeListener(com.navdy.obd.IPidListener a) {
        android.os.Parcel a0 = android.os.Parcel.obtain();
        android.os.Parcel a1 = android.os.Parcel.obtain();
        try {
            a0.writeInterfaceToken("com.navdy.obd.ICarService");
            android.os.IBinder a2 = (a == null) ? null : a.asBinder();
            a0.writeStrongBinder(a2);
            this.mRemote.transact(7, a0, a1, 0);
            a1.readException();
        } catch(Throwable a3) {
            a1.recycle();
            a0.recycle();
            throw a3;
        }
        a1.recycle();
        a0.recycle();
    }
    
    public void rescan() {
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.ICarService");
            this.mRemote.transact(12, a, a0, 0);
            a0.readException();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
    }
    
    public void setCANBusMonitoringListener(com.navdy.obd.ICanBusMonitoringListener a) {
        android.os.Parcel a0 = android.os.Parcel.obtain();
        android.os.Parcel a1 = android.os.Parcel.obtain();
        try {
            a0.writeInterfaceToken("com.navdy.obd.ICarService");
            android.os.IBinder a2 = (a == null) ? null : a.asBinder();
            a0.writeStrongBinder(a2);
            this.mRemote.transact(23, a0, a1, 0);
            a1.readException();
        } catch(Throwable a3) {
            a1.recycle();
            a0.recycle();
            throw a3;
        }
        a1.recycle();
        a0.recycle();
    }
    
    public void setMode(int i, boolean b) {
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.ICarService");
            a.writeInt(i);
            a.writeInt(b ? 1 : 0);
            this.mRemote.transact(22, a, a0, 0);
            a0.readException();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
    }
    
    public void setObdPidsScanningEnabled(boolean b) {
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.ICarService");
            a.writeInt(b ? 1 : 0);
            this.mRemote.transact(14, a, a0, 0);
            a0.readException();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
    }
    
    public void setVoltageSettings(com.navdy.obd.VoltageSettings a) {
        android.os.Parcel a0 = android.os.Parcel.obtain();
        android.os.Parcel a1 = android.os.Parcel.obtain();
        try {
            a0.writeInterfaceToken("com.navdy.obd.ICarService");
            if (a == null) {
                a0.writeInt(0);
            } else {
                a0.writeInt(1);
                a.writeToParcel(a0, 0);
            }
            this.mRemote.transact(18, a0, a1, 0);
            a1.readException();
        } catch(Throwable a2) {
            a1.recycle();
            a0.recycle();
            throw a2;
        }
        a1.recycle();
        a0.recycle();
    }
    
    public void sleep(boolean b) {
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.ICarService");
            a.writeInt(b ? 1 : 0);
            this.mRemote.transact(16, a, a0, 0);
            a0.readException();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
    }
    
    public void startCanBusMonitoring() {
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.ICarService");
            this.mRemote.transact(24, a, a0, 0);
            a0.readException();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
    }
    
    public void stopCanBusMonitoring() {
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.ICarService");
            this.mRemote.transact(25, a, a0, 0);
            a0.readException();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
    }
    
    public void updateFirmware(String s, String s0) {
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.ICarService");
            a.writeString(s);
            a.writeString(s0);
            this.mRemote.transact(20, a, a0, 0);
            a0.readException();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
    }
    
    public void updateScan(com.navdy.obd.ScanSchedule a, com.navdy.obd.IPidListener a0) {
        android.os.Parcel a1 = android.os.Parcel.obtain();
        android.os.Parcel a2 = android.os.Parcel.obtain();
        try {
            a1.writeInterfaceToken("com.navdy.obd.ICarService");
            if (a == null) {
                a1.writeInt(0);
            } else {
                a1.writeInt(1);
                a.writeToParcel(a1, 0);
            }
            android.os.IBinder a3 = (a0 == null) ? null : a0.asBinder();
            a1.writeStrongBinder(a3);
            this.mRemote.transact(11, a1, a2, 0);
            a2.readException();
        } catch(Throwable a4) {
            a2.recycle();
            a1.recycle();
            throw a4;
        }
        a2.recycle();
        a1.recycle();
    }
    
    public void wakeup() {
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.ICarService");
            this.mRemote.transact(17, a, a0, 0);
            a0.readException();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
    }
}
