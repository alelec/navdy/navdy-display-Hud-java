package com.navdy.obd;

abstract public class IObdServiceListener$Stub extends android.os.Binder implements com.navdy.obd.IObdServiceListener {
    final private static String DESCRIPTOR = "com.navdy.obd.IObdServiceListener";
    final static int TRANSACTION_onConnectionStateChange = 1;
    final static int TRANSACTION_onRawData = 3;
    final static int TRANSACTION_onStatusMessage = 2;
    final static int TRANSACTION_scannedPids = 4;
    final static int TRANSACTION_scannedVIN = 6;
    final static int TRANSACTION_supportedPids = 5;
    
    public IObdServiceListener$Stub() {
        this.attachInterface((android.os.IInterface)this, "com.navdy.obd.IObdServiceListener");
    }
    
    public static com.navdy.obd.IObdServiceListener asInterface(android.os.IBinder a) {
        Object a0 = null;
        label1: if (a != null) {
            android.os.IInterface a1 = a.queryLocalInterface("com.navdy.obd.IObdServiceListener");
            label0: {
                if (a1 == null) {
                    break label0;
                }
                if (!(a1 instanceof com.navdy.obd.IObdServiceListener)) {
                    break label0;
                }
                a0 = a1;
                break label1;
            }
            a0 = new com.navdy.obd.IObdServiceListener$Stub$Proxy(a);
        } else {
            a0 = null;
        }
        return (com.navdy.obd.IObdServiceListener)a0;
    }
    
    public android.os.IBinder asBinder() {
        return (android.os.IBinder)this;
    }
    
    public boolean onTransact(int i, android.os.Parcel a, android.os.Parcel a0, int i0) {
        boolean b = false;
        switch(i) {
            case 1598968902: {
                a0.writeString("com.navdy.obd.IObdServiceListener");
                b = true;
                break;
            }
            case 6: {
                a.enforceInterface("com.navdy.obd.IObdServiceListener");
                this.scannedVIN(a.readString());
                a0.writeNoException();
                b = true;
                break;
            }
            case 5: {
                a.enforceInterface("com.navdy.obd.IObdServiceListener");
                this.supportedPids((java.util.List)a.createTypedArrayList(com.navdy.obd.Pid.CREATOR));
                a0.writeNoException();
                b = true;
                break;
            }
            case 4: {
                a.enforceInterface("com.navdy.obd.IObdServiceListener");
                this.scannedPids((java.util.List)a.createTypedArrayList(com.navdy.obd.Pid.CREATOR));
                a0.writeNoException();
                b = true;
                break;
            }
            case 3: {
                a.enforceInterface("com.navdy.obd.IObdServiceListener");
                this.onRawData(a.readString());
                a0.writeNoException();
                b = true;
                break;
            }
            case 2: {
                a.enforceInterface("com.navdy.obd.IObdServiceListener");
                this.onStatusMessage(a.readString());
                a0.writeNoException();
                b = true;
                break;
            }
            case 1: {
                a.enforceInterface("com.navdy.obd.IObdServiceListener");
                this.onConnectionStateChange(a.readInt());
                a0.writeNoException();
                b = true;
                break;
            }
            default: {
                b = super.onTransact(i, a, a0, i0);
            }
        }
        return b;
    }
}
