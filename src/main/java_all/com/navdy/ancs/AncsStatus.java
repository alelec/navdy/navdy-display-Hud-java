package com.navdy.ancs;

public class AncsStatus
{
    public static final int STATE_BOND_REMOVED = 5;
    public static final int STATE_CONNECTED = 2;
    public static final int STATE_CONNECTING = 1;
    public static final int STATE_DISCONNECTED = 0;
    public static final int STATE_DISCONNECTING = 3;
    public static final int STATE_PAIRING_AUTH_FAILED = 4;
}
