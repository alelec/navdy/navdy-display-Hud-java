package com.navdy.ancs;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import android.util.Log;
import android.os.Parcel;
import java.util.Date;
import android.os.Parcelable.Creator;
import android.os.Parcelable;

public class AppleNotification implements Parcelable
{
    public static final int ACTION_ID_NEGATIVE = 1;
    public static final int ACTION_ID_POSITIVE = 0;
    public static final int ATTRIBUTE_APP_IDENTIFIER = 0;
    public static final int ATTRIBUTE_DATE = 5;
    public static final int ATTRIBUTE_MESSAGE = 3;
    public static final int ATTRIBUTE_MESSAGE_SIZE = 4;
    public static final int ATTRIBUTE_NEGATIVE_ACTION_LABEL = 7;
    public static final int ATTRIBUTE_POSITIVE_ACTION_LABEL = 6;
    public static final int ATTRIBUTE_SUBTITLE = 2;
    public static final int ATTRIBUTE_TITLE = 1;
    public static final int CATEGORY_BUSINESS_AND_FINANCE = 9;
    public static final int CATEGORY_EMAIL = 6;
    public static final int CATEGORY_ENTERTAINMENT = 11;
    public static final int CATEGORY_HEALTH_AND_FITNESS = 8;
    public static final int CATEGORY_INCOMING_CALL = 1;
    public static final int CATEGORY_LOCATION = 10;
    public static final int CATEGORY_MISSED_CALL = 2;
    public static final int CATEGORY_NEWS = 7;
    public static final int CATEGORY_OTHER = 0;
    public static final int CATEGORY_SCHEDULE = 5;
    public static final int CATEGORY_SOCIAL = 4;
    public static final int CATEGORY_VOICE_MAIL = 3;
    public static final Parcelable.Creator<AppleNotification> CREATOR;
    public static final int EVENT_ADDED = 0;
    static final int EVENT_FLAG_IMPORTANT = 2;
    static final int EVENT_FLAG_NEGATIVE_ACTION = 16;
    static final int EVENT_FLAG_POSITIVE_ACTION = 8;
    static final int EVENT_FLAG_PRE_EXISTING = 4;
    static final int EVENT_FLAG_SILENT = 1;
    public static final int EVENT_MODIFIED = 1;
    public static final int EVENT_REMOVED = 2;
    private static final String TAG;
    private String appId;
    private String appName;
    private int categoryCount;
    private int categoryId;
    private Date date;
    private int eventFlags;
    private int eventId;
    private String message;
    private String negativeActionLabel;
    private int notificationUid;
    private String positiveActionLabel;
    private String subTitle;
    private String title;
    
    static {
        TAG = AppleNotification.class.getSimpleName();
        CREATOR = (Parcelable.Creator)new Parcelable.Creator<AppleNotification>() {
            public AppleNotification createFromParcel(final Parcel parcel) {
                return new AppleNotification(parcel);
            }
            
            public AppleNotification[] newArray(final int n) {
                return new AppleNotification[n];
            }
        };
    }
    
    public AppleNotification(final int eventId, final int eventFlags, final int categoryId, final int categoryCount, final int notificationUid) {
        this.eventId = eventId;
        this.categoryId = categoryId;
        this.categoryCount = categoryCount;
        this.eventFlags = eventFlags;
        this.notificationUid = notificationUid;
    }
    
    public AppleNotification(final Parcel parcel) {
        this(parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readInt());
        this.setTitle(parcel.readString());
        this.setSubTitle(parcel.readString());
        this.setMessage(parcel.readString());
        final long long1 = parcel.readLong();
        if (long1 != 0L) {
            this.setDate(new Date(long1));
        }
        this.setAppId(parcel.readString());
        this.setPositiveActionLabel(parcel.readString());
        this.setNegativeActionLabel(parcel.readString());
        this.setAppName(parcel.readString());
    }
    
    public static int byteToInt(final byte b) {
        return b & 0xFF;
    }
    
    public static int bytesToInt(final byte b, final byte b2, final byte b3, final byte b4) {
        return byteToInt(b) + (byteToInt(b2) << 8) + (byteToInt(b3) << 16) + (byteToInt(b4) << 24);
    }
    
    public static int bytesToInt(final byte[] array, final int n) {
        return bytesToInt(array[n], array[n + 1], array[n + 2], array[n + 3]);
    }
    
    public static int bytesToShort(final byte b, final byte b2) {
        return byteToInt(b) + (byteToInt(b2) << 8);
    }
    
    public static int bytesToShort(final byte[] array, final int n) {
        return bytesToShort(array[n], array[n + 1]);
    }
    
    public static AppleNotification parse(final byte[] array) {
        return new AppleNotification(array[0] & 0xFF, array[1] & 0xFF, array[2] & 0xFF, array[3] & 0xFF, bytesToInt(array, 4));
    }
    
    public int describeContents() {
        return 0;
    }
    
    public String getAppId() {
        return this.appId;
    }
    
    public String getAppName() {
        return this.appName;
    }
    
    public int getCategoryCount() {
        return this.categoryCount;
    }
    
    public int getCategoryId() {
        return this.categoryId;
    }
    
    public Date getDate() {
        return this.date;
    }
    
    public int getEventFlags() {
        return this.eventFlags;
    }
    
    public int getEventId() {
        return this.eventId;
    }
    
    public String getMessage() {
        return this.message;
    }
    
    public String getNegativeActionLabel() {
        return this.negativeActionLabel;
    }
    
    public int getNotificationUid() {
        return this.notificationUid;
    }
    
    public String getPositiveActionLabel() {
        return this.positiveActionLabel;
    }
    
    public String getSubTitle() {
        return this.subTitle;
    }
    
    public String getTitle() {
        return this.title;
    }
    
    public boolean hasNegativeAction() {
        return (this.eventFlags & 0x10) != 0x0;
    }
    
    public boolean hasPositiveAction() {
        return (this.eventFlags & 0x8) != 0x0;
    }
    
    public boolean isImportant() {
        return (this.eventFlags & 0x2) != 0x0;
    }
    
    public boolean isPreExisting() {
        return (this.eventFlags & 0x4) != 0x0;
    }
    
    public boolean isSilent() {
        return (this.eventFlags & 0x1) != 0x0;
    }
    
    public void setAppId(final String appId) {
        this.appId = appId;
    }
    
    public void setAppName(final String appName) {
        this.appName = appName;
    }
    
    public void setAttribute(final int n, final String s) {
        switch (n) {
            default:
                Log.d(AppleNotification.TAG, "Unhandled attribute - id:" + n + " val:" + s);
                break;
            case 1:
                this.setTitle(s);
                break;
            case 2:
                this.setSubTitle(s);
                break;
            case 3:
                this.setMessage(s);
                break;
            case 0:
                this.setAppId(s);
                break;
            case 5: {
                final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd'T'HHmmSS", Locale.ENGLISH);
                try {
                    this.setDate(simpleDateFormat.parse(s));
                }
                catch (ParseException ex) {
                    Log.e(AppleNotification.TAG, "Unable to parse date - " + s);
                }
                break;
            }
            case 6:
                this.setPositiveActionLabel(s);
                break;
            case 7:
                this.setNegativeActionLabel(s);
                break;
        }
    }
    
    public void setDate(final Date date) {
        this.date = date;
    }
    
    public void setMessage(final String message) {
        this.message = message;
    }
    
    public void setNegativeActionLabel(final String negativeActionLabel) {
        this.negativeActionLabel = negativeActionLabel;
    }
    
    public void setPositiveActionLabel(final String positiveActionLabel) {
        this.positiveActionLabel = positiveActionLabel;
    }
    
    public void setSubTitle(final String subTitle) {
        this.subTitle = subTitle;
    }
    
    public void setTitle(final String title) {
        this.title = title;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Notification{eventId=" + this.eventId + ", categoryId=" + this.categoryId + ", categoryCount=" + this.categoryCount + ", eventFlags=" + this.eventFlags + ", notificationUid=" + this.notificationUid);
        if (this.appId != null) {
            sb.append(", appId='");
            sb.append(this.appId);
            sb.append('\'');
        }
        if (this.appName != null) {
            sb.append(", appName='");
            sb.append(this.appName);
            sb.append('\'');
        }
        if (this.title != null) {
            sb.append(", title='");
            sb.append(this.title);
            sb.append('\'');
        }
        if (this.subTitle != null) {
            sb.append(", subTitle='");
            sb.append(this.subTitle);
            sb.append('\'');
        }
        if (this.message != null) {
            sb.append(", message='");
            sb.append(this.message);
            sb.append('\'');
        }
        if (this.positiveActionLabel != null) {
            sb.append(", positiveAction='");
            sb.append(this.positiveActionLabel);
            sb.append('\'');
        }
        if (this.negativeActionLabel != null) {
            sb.append(", negativeAction='");
            sb.append(this.negativeActionLabel);
            sb.append('\'');
        }
        if (this.date != null) {
            sb.append(", date='");
            sb.append(this.date);
            sb.append('\'');
        }
        sb.append("}");
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeInt(this.eventId);
        parcel.writeInt(this.eventFlags);
        parcel.writeInt(this.categoryId);
        parcel.writeInt(this.categoryCount);
        parcel.writeInt(this.notificationUid);
        parcel.writeString(this.title);
        parcel.writeString(this.subTitle);
        parcel.writeString(this.message);
        long time;
        if (this.date != null) {
            time = this.date.getTime();
        }
        else {
            time = 0L;
        }
        parcel.writeLong(time);
        parcel.writeString(this.appId);
        parcel.writeString(this.positiveActionLabel);
        parcel.writeString(this.negativeActionLabel);
        parcel.writeString(this.appName);
    }
}
