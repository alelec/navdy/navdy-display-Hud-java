package com.navdy.hud.app.util.picasso;

import com.navdy.service.library.util.IOUtils;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import com.squareup.picasso.Request;
import com.squareup.picasso.RequestHandler;
import com.squareup.picasso.Cache;
import com.navdy.hud.app.storage.PathManager;
import android.content.Context;
import com.navdy.hud.app.HudApplication;
import android.net.Uri;
import android.graphics.Bitmap;
import java.io.File;
import com.navdy.service.library.log.Logger;
import com.squareup.picasso.Picasso;
import com.navdy.hud.app.storage.cache.DiskLruCache;

public class PicassoUtil
{
    private static final String DISK_CACHE_SCHEME = "diskcache://";
    private static final String DISK_CACHE_SCHEME_NAME = "diskcache";
    private static final int DISK_CACHE_SIZE = 10485760;
    public static final int IMAGE_MEMORY_CACHE = 8388608;
    private static DiskLruCache diskLruCache;
    private static volatile boolean initialized;
    private static Object lockObj;
    private static PicassoLruCache lruCache;
    private static Picasso picasso;
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(PicassoUtil.class);
        PicassoUtil.lockObj = new Object();
    }
    
    public static void clearCache() {
        if (PicassoUtil.lruCache != null) {
            PicassoUtil.lruCache.clear();
        }
    }
    
    public static Bitmap getBitmapfromCache(final File file) {
        Bitmap bitmap;
        if (PicassoUtil.lruCache != null) {
            bitmap = PicassoUtil.lruCache.getBitmap("file://" + file.getAbsolutePath());
        }
        else {
            bitmap = null;
        }
        return bitmap;
    }
    
    public static Bitmap getBitmapfromCache(final String s) {
        Bitmap bitmap;
        if (PicassoUtil.lruCache != null) {
            bitmap = PicassoUtil.lruCache.getBitmap(s);
        }
        else {
            bitmap = null;
        }
        return bitmap;
    }
    
    public static DiskLruCache getDiskLruCache() {
        return PicassoUtil.diskLruCache;
    }
    
    public static Uri getDiskcacheUri(final String s) {
        return Uri.parse("diskcache://" + s);
    }
    
    public static Picasso getInstance() {
        if (!PicassoUtil.initialized) {
            initPicasso(HudApplication.getAppContext());
        }
        return PicassoUtil.picasso;
    }
    
    public static void initPicasso(Context memoryCache) {
        if (PicassoUtil.initialized) {
            return;
        }
        final Object lockObj = PicassoUtil.lockObj;
        synchronized (lockObj) {
            if (PicassoUtil.initialized) {
                return;
            }
            try {
                Object imageDiskCacheFolder = new PicassoLruCache(8388608);
                (PicassoUtil.lruCache = (PicassoLruCache)imageDiskCacheFolder).init();
                imageDiskCacheFolder = PathManager.getInstance().getImageDiskCacheFolder();
                while (true) {
                    try {
                        PicassoUtil.diskLruCache = new DiskLruCache("imagecache", (String)imageDiskCacheFolder, 10485760);
                        imageDiskCacheFolder = new Picasso.Builder((Context)memoryCache);
                        memoryCache = (Throwable)((Picasso.Builder)imageDiskCacheFolder).memoryCache(PicassoUtil.lruCache);
                        imageDiskCacheFolder = new RequestHandler() {
                            @Override
                            public boolean canHandleRequest(final Request request) {
                                return "diskcache".equals(request.uri.getScheme());
                            }
                            
                            @Override
                            public Result load(final Request request, final int n) throws IOException {
                                final String host = request.uri.getHost();
                                PicassoUtil.sLogger.v("load:" + host);
                                if (PicassoUtil.diskLruCache == null) {
                                    throw new IOException();
                                }
                                final byte[] value = PicassoUtil.diskLruCache.get(host);
                                if (value == null) {
                                    throw new IOException();
                                }
                                return new Result(new ByteArrayInputStream(value), Picasso.LoadedFrom.DISK);
                            }
                        };
                        Picasso.setSingletonInstance(PicassoUtil.picasso = ((Picasso.Builder)memoryCache).addRequestHandler((RequestHandler)imageDiskCacheFolder).build());
                        PicassoUtil.initialized = true;
                    }
                    catch (Throwable t) {
                        PicassoUtil.sLogger.e("Error creating disk cache: " + t);
                        try {
                            IOUtils.deleteDirectory((Context)memoryCache, new File((String)imageDiskCacheFolder));
                            PicassoUtil.diskLruCache = new DiskLruCache("imagecache", (String)imageDiskCacheFolder, 10485760);
                            continue;
                        }
                        catch (Throwable t2) {
                            final Logger sLogger = PicassoUtil.sLogger;
                            imageDiskCacheFolder = new StringBuilder();
                            sLogger.e(((StringBuilder)imageDiskCacheFolder).append("Error re-creating disk cache: ").append(t2).toString());
                            continue;
                        }
                    }
                    break;
                }
            }
            catch (Throwable memoryCache) {
                PicassoUtil.sLogger.e(memoryCache);
            }
        }
    }
    
    public static boolean isImageAvailableInCache(final File file) {
        return getBitmapfromCache(file) != null;
    }
    
    public static void setBitmapInCache(final String s, final Bitmap bitmap) {
        if (PicassoUtil.lruCache != null) {
            PicassoUtil.lruCache.setBitmap(s, bitmap);
        }
    }
}
