package com.navdy.hud.app.util;

import com.navdy.service.library.util.CredentialUtil;
import mortar.Mortar;
import java.io.Closeable;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.FileReader;
import com.navdy.hud.app.framework.network.NetworkStateManager;
import com.navdy.hud.app.framework.network.NetworkBandwidthController;
import com.navdy.hud.app.profile.DriverProfile;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.service.ConnectionHandler;
import org.json.JSONException;
import com.navdy.hud.app.profile.HudLocale;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.hud.app.ui.component.homescreen.HomeScreen;
import org.json.JSONObject;
import com.navdy.hud.app.event.ShowScreenWithArgs;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2;
import android.os.Bundle;
import android.app.PendingIntent;
import android.os.SystemClock;
import android.app.AlarmManager;
import com.navdy.hud.app.storage.PathManager;
import java.util.Arrays;
import java.io.Serializable;
import android.content.Intent;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.routing.RouteTta;
import com.navdy.hud.app.maps.here.HereNavController;
import com.here.android.mpa.common.RoadElement;
import com.here.android.mpa.common.GeoPosition;
import android.os.Build;
import android.os.Build;
import com.here.android.mpa.routing.Maneuver;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;
import com.navdy.service.library.events.preferences.NavigationPreferences;
import java.util.Date;
import com.navdy.hud.app.analytics.NavigationQualityTracker;
import com.here.android.mpa.routing.Route;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.maps.here.HereMapUtil;
import com.navdy.hud.app.maps.here.HereMapsManager;
import android.text.TextUtils;
import com.google.gson.JsonParser;
import com.google.gson.GsonBuilder;
import android.content.Context;
import com.navdy.service.library.device.NavdyDeviceId;
import java.util.Iterator;
import java.util.List;
import java.io.IOException;
import java.util.ArrayList;
import com.navdy.service.library.util.IOUtils;
import com.navdy.hud.app.HudApplication;
import java.util.concurrent.TimeUnit;
import java.util.Comparator;
import java.util.Locale;
import com.navdy.service.library.network.http.services.JiraClient;
import com.navdy.service.library.network.http.IHttpManager;
import com.navdy.hud.app.gesture.GestureServiceConnector;
import com.navdy.hud.app.debug.DriveRecorder;
import javax.inject.Inject;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.PriorityBlockingQueue;
import java.io.File;
import java.text.SimpleDateFormat;
import android.app.IntentService;

public class ReportIssueService extends IntentService
{
    private static final String ACTION_DUMP = "Dump";
    private static final String ACTION_SNAPSHOT = "Snapshot";
    private static final String ACTION_SUBMIT_JIRA = "Jira";
    private static final String ACTION_SYNC = "Sync";
    public static final String ASSIGNEE = "assignee";
    public static final String ASSIGNEE_EMAIL = "assigneeEmail";
    public static final String ATTACHMENT = "attachment";
    private static final SimpleDateFormat DATE_FORMAT;
    public static final String ENVIRONMENT = "Environment";
    public static final String EXTRA_ISSUE_TYPE = "EXTRA_ISSUE_TYPE";
    public static final String EXTRA_SNAPSHOT_TITLE = "EXTRA_SNAPSHOT_TITLE";
    private static final String HUD_ISSUE_TYPE = "Issue";
    private static final String HUD_PROJECT = "HUD";
    private static final String ISSUE_TYPE = "Task";
    private static final String JIRA_CREDENTIALS_META = "JIRA_CREDENTIALS";
    private static final int MAX_FILES_OUT_STANDING = 10;
    private static final String NAME = "REPORT_ISSUE";
    private static final String PROJECT_NAME = "NP";
    public static final long RETRY_INTERVAL;
    private static final SimpleDateFormat SNAPSHOT_DATE_FORMAT;
    private static final SimpleDateFormat SNAPSHOT_JIRA_TICKET_DATE_FORMAT;
    public static final String SUMMARY = "Summary";
    public static final int SYNC_REQ = 128;
    public static final String TICKET_ID = "ticketId";
    private static File lastSnapShotFile;
    private static boolean mIsInitialized;
    private static PriorityBlockingQueue<File> mIssuesToSync;
    private static long mLastIssueSubmittedAt;
    private static AtomicBoolean mSyncing;
    private static String navigationIssuesFolder;
    private static Logger sLogger;
    @Inject
    Bus bus;
    @Inject
    DriveRecorder driveRecorder;
    @Inject
    GestureServiceConnector gestureService;
    @Inject
    IHttpManager mHttpManager;
    JiraClient mJiraClient;
    
    static {
        DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'_'HH:mm:ss.SSS", Locale.US);
        ReportIssueService.mIssuesToSync = new PriorityBlockingQueue<File>(5, new FilesModifiedTimeComparator());
        RETRY_INTERVAL = TimeUnit.MINUTES.toMillis(3L);
        ReportIssueService.mLastIssueSubmittedAt = 0L;
        ReportIssueService.mIsInitialized = false;
        ReportIssueService.mSyncing = new AtomicBoolean(false);
        ReportIssueService.sLogger = new Logger(ReportIssueService.class);
        SNAPSHOT_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss", Locale.US);
        SNAPSHOT_JIRA_TICKET_DATE_FORMAT = new SimpleDateFormat("MMM,dd hh:mm a", Locale.US);
    }
    
    public ReportIssueService() {
        super("REPORT_ISSUE");
    }
    
    private void attachFilesToTheJiraTicket(final String s, final File file, final JiraClient.ResultCallback resultCallback) {
        if (file.exists() && file.getName().endsWith(".zip")) {
            final File file2 = new File(file.getParentFile(), "Staging");
            ArrayList<File> list = null;
            ArrayList<File> list2 = null;
            Label_0147: {
                if (file2.exists()) {
                    if (!file2.isDirectory()) {
                        break Label_0147;
                    }
                    IOUtils.deleteDirectory(HudApplication.getAppContext(), file2);
                }
                while (true) {
                    file2.mkdir();
                    try {
                        IOUtils.deCompressZipToDirectory(HudApplication.getAppContext(), file, file2);
                        final File[] listFiles = file2.listFiles();
                        list = new ArrayList<File>();
                        list2 = new ArrayList<File>();
                        for (final File file3 : listFiles) {
                            if (file3.getName().endsWith(".png")) {
                                list2.add(file3);
                            }
                            else {
                                list.add(file3);
                            }
                        }
                        break;
                        IOUtils.deleteFile(HudApplication.getAppContext(), file2.getAbsolutePath());
                        continue;
                    }
                    catch (IOException ex) {
                        ReportIssueService.sLogger.e("Error decompressing the zip file to get the attachments", ex);
                    }
                    return;
                }
            }
            final File[] array = list.<File>toArray(new File[list.size()]);
            final File file4 = new File(file2, "logs.zip");
            IOUtils.compressFilesToZip(HudApplication.getAppContext(), array, file4.getAbsolutePath());
            final ArrayList<JiraClient.Attachment> list3 = new ArrayList<JiraClient.Attachment>();
            final JiraClient.Attachment attachment = new JiraClient.Attachment();
            attachment.filePath = file4.getAbsolutePath();
            attachment.mimeType = "application/zip";
            list3.add(attachment);
            for (final File file5 : list2) {
                final JiraClient.Attachment attachment2 = new JiraClient.Attachment();
                attachment2.filePath = file5.getAbsolutePath();
                attachment2.mimeType = "image/png";
                list3.add(attachment2);
            }
            this.mJiraClient.attachFilesToTicket(s, list3, resultCallback);
        }
    }
    
    private String buildDescription() {
        final NavdyDeviceId thisDevice = NavdyDeviceId.getThisDevice((Context)this);
        final StringBuilder sb = new StringBuilder();
        sb.append("\n#############\n");
        sb.append("HERE metadata: ");
        sb.append("\n#############\n\n");
        Object instance = "";
        Object o = DeviceUtil.getCurrentHereSdkVersion();
        Object o2 = DeviceUtil.getHEREMapsDataInfo();
        Object json = instance;
    Label_0103:
        while (true) {
            if (o2 != null) {
                GeoPosition lastGeoPosition;
                RoadElement currentRoadElement;
                Route currentRoute = null;
                HereNavController navController;
                RouteTta tta;
                GeoCoordinate startLocation;
                GeoCoordinate destination;
                long tripStartUtc;
                NavigationQualityTracker instance2;
                int actualDurationSoFar;
                int expectedDuration;
                int expectedDistance;
                long destinationDistance;
                String destinationLabel = null;
                NavigationPreferences navigationPreferences;
                String s;
                int n;
                Iterator<Maneuver> iterator;
                GeoCoordinate coordinate;
                Label_0396_Outer:Label_0444_Outer:Label_0781_Outer:
                while (true) {
                Label_1152:
                    while (true) {
                    Label_1144:
                        while (true) {
                        Label_1133:
                            while (true) {
                            Label_1122:
                                while (true) {
                                    try {
                                        json = new GsonBuilder().setPrettyPrinting().create().toJson(new JsonParser().parse((String)o2));
                                        if (!TextUtils.isEmpty((CharSequence)o)) {
                                            sb.append("HERE mSDK for Android v").append((String)o).append("\n");
                                        }
                                        if (!TextUtils.isEmpty((CharSequence)json)) {
                                            sb.append("HERE Map Data:\n").append((String)o2).append("\n");
                                        }
                                        sb.append("\n#############\n");
                                        sb.append("Navigation details:");
                                        sb.append("\n#############\n\n");
                                        lastGeoPosition = HereMapsManager.getInstance().getLastGeoPosition();
                                        if (lastGeoPosition != null && lastGeoPosition.getCoordinate() != null) {
                                            sb.append("Current Location: ").append(lastGeoPosition.getCoordinate().getLatitude()).append(",").append(lastGeoPosition.getCoordinate().getLongitude()).append("\n");
                                            sb.append("Current Road: ").append(HereMapUtil.getCurrentRoadName()).append("\n");
                                            currentRoadElement = HereMapUtil.getCurrentRoadElement();
                                            if (currentRoadElement != null) {
                                                sb.append("Current road element ID: ").append(currentRoadElement.getIdentifier()).append("\n");
                                            }
                                            o = DriverProfileHelper.getInstance();
                                            o2 = ((DriverProfileHelper)o).getCurrentProfile();
                                            if (!HereMapsManager.getInstance().isNavigationModeOn()) {
                                                break;
                                            }
                                            instance = HereNavigationManager.getInstance();
                                            currentRoute = ((HereNavigationManager)instance).getCurrentRoute();
                                            navController = ((HereNavigationManager)instance).getNavController();
                                            if (currentRoute != null) {
                                                tta = navController.getTta(Route.TrafficPenaltyMode.OPTIMAL, true);
                                                startLocation = ((HereNavigationManager)instance).getStartLocation();
                                                if (startLocation == null) {
                                                    break Label_1122;
                                                }
                                                sb.append("Route Starting point: ").append(startLocation.getLatitude()).append(",").append(startLocation.getLongitude()).append("\n");
                                                destination = currentRoute.getDestination();
                                                if (destination == null) {
                                                    break Label_1133;
                                                }
                                                sb.append("Route Ending point: ").append(destination.getLatitude()).append(",").append(destination.getLongitude()).append("\n");
                                                sb.append("Traffic Used: ").append(((HereNavigationManager)instance).isTrafficConsidered(((HereNavigationManager)instance).getCurrentRouteId())).append("\n");
                                                tripStartUtc = NavigationQualityTracker.getInstance().getTripStartUtc();
                                                if (tripStartUtc > 0L) {
                                                    sb.append("Trip started: ").append(new SimpleDateFormat("MMMM dd HH:mm:ss zzzz yyyy Z", Locale.US).format(new Date(tripStartUtc))).append("\n");
                                                }
                                                instance2 = NavigationQualityTracker.getInstance();
                                                actualDurationSoFar = instance2.getActualDurationSoFar();
                                                expectedDuration = instance2.getExpectedDuration();
                                                expectedDistance = instance2.getExpectedDistance();
                                                destinationDistance = navController.getDestinationDistance();
                                                if (expectedDuration > 0) {
                                                    sb.append("Expected initial ETA according to HERE: ").append(expectedDuration).append(" seconds\n");
                                                }
                                                if (expectedDistance > 0) {
                                                    sb.append("Expected initial distance according to HERE: ").append(expectedDistance).append(" meters\n");
                                                }
                                                if (tta != null) {
                                                    sb.append("Time To Arrival remaining according to HERE: ").append(tta.getDuration()).append(" seconds\n");
                                                }
                                                if (destinationDistance > 0L) {
                                                    sb.append("Distance remaining according to HERE: ").append(destinationDistance).append(" meters\n");
                                                }
                                                if (actualDurationSoFar > 0) {
                                                    sb.append("Duration of the trip so far: ").append(actualDurationSoFar).append(" seconds\n");
                                                }
                                            }
                                            destinationLabel = ((HereNavigationManager)instance).getDestinationLabel();
                                            sb.append("Destination: ").append(destinationLabel).append(", ").append(((HereNavigationManager)instance).getDestinationStreetAddress()).append("\n");
                                            sb.append("\n#############\n");
                                            sb.append("User route preferences:");
                                            sb.append("\n#############\n\n");
                                            navigationPreferences = ((DriverProfile)o2).getNavigationPreferences();
                                            if (navigationPreferences.routingType == NavigationPreferences.RoutingType.ROUTING_FASTEST) {
                                                s = "fastest";
                                                sb.append("Route calculation: ").append(s).append("\n");
                                                sb.append("Highways: ").append(navigationPreferences.allowHighways).append("\n");
                                                sb.append("Toll roads: ").append(navigationPreferences.allowTollRoads).append("\n");
                                                sb.append("Ferries: ").append(navigationPreferences.allowFerries).append("\n");
                                                sb.append("Tunnels: ").append(navigationPreferences.allowTunnels).append("\n");
                                                sb.append("Unpaved Roads: ").append(navigationPreferences.allowUnpavedRoads).append("\n");
                                                sb.append("Auto Trains: ").append(navigationPreferences.allowAutoTrains).append("\n");
                                                sb.append("\n#############\n");
                                                sb.append("Maneuvers (entire route):");
                                                sb.append("\n#############\n\n");
                                                HereMapUtil.printRouteDetails(currentRoute, destinationLabel, null, sb);
                                                sb.append("\n#############\n");
                                                sb.append("Waypoints (entire route):");
                                                sb.append("\n#############\n\n");
                                                n = 0;
                                                iterator = currentRoute.getManeuvers().iterator();
                                                while (iterator.hasNext()) {
                                                    coordinate = iterator.next().getCoordinate();
                                                    if (coordinate != null) {
                                                        sb.append("waypoint").append(n).append(": ").append(coordinate.getLatitude()).append(",").append(coordinate.getLongitude()).append("\n");
                                                        ++n;
                                                    }
                                                }
                                                break Label_1152;
                                            }
                                            break Label_1144;
                                        }
                                    }
                                    catch (Throwable t) {
                                        ReportIssueService.sLogger.e(t);
                                        json = instance;
                                        continue Label_0103;
                                    }
                                    sb.append("Current Location: N/A\n");
                                    continue Label_0396_Outer;
                                }
                                sb.append("Route Starting point: N/A\n");
                                continue Label_0444_Outer;
                            }
                            sb.append("Route Ending point: N/A\n");
                            continue Label_0781_Outer;
                        }
                        s = "shortest";
                        continue;
                    }
                    sb.append("\n#############\n");
                    sb.append("Maneuvers (remaining route):");
                    sb.append("\n#############\n\n");
                    HereMapUtil.printRouteDetails(currentRoute, destinationLabel, null, sb, ((HereNavigationManager)instance).getNextManeuver());
                    break;
                }
                sb.append("\n#############\n");
                sb.append("Navdy info:");
                sb.append("\n#############\n\n");
                sb.append("Device ID: ").append(thisDevice.toString()).append("\n");
                sb.append("HUD app version: 1.3.3051-corona\n");
                sb.append("Serial number: ").append(Build.SERIAL).append("\n");
                sb.append("Model: ").append(Build.MODEL).append("\n");
                sb.append("API Level: ").append(Build$VERSION.SDK_INT).append("\n");
                sb.append("Build type: ").append(Build.TYPE).append("\n");
                String s2;
                String s3;
                if (!((DriverProfile)o2).isDefaultProfile()) {
                    s2 = ((DriverProfile)o2).getDriverName();
                    s3 = ((DriverProfile)o2).getDriverEmail();
                }
                else {
                    s2 = ((DriverProfileHelper)o).getDriverProfileManager().getLastUserName();
                    s3 = ((DriverProfileHelper)o).getDriverProfileManager().getLastUserEmail();
                }
                if (!TextUtils.isEmpty((CharSequence)s2) && !TextUtils.isEmpty((CharSequence)s3)) {
                    sb.append("Username: ").append(s2).append("\n");
                    sb.append("Email: ").append(s3).append("\n");
                }
                return sb.toString();
            }
            continue Label_0103;
        }
    }
    
    public static boolean canReportIssue() {
        return ReportIssueService.mIssuesToSync.size() <= 10;
    }
    
    public static void dispatchReportNewIssue(final IssueType issueType) {
        final Intent intent = new Intent(HudApplication.getAppContext(), (Class)ReportIssueService.class);
        intent.setAction("Dump");
        intent.putExtra("EXTRA_ISSUE_TYPE", (Serializable)issueType);
        HudApplication.getAppContext().startService(intent);
    }
    
    public static void dispatchSync() {
        final Intent intent = new Intent(HudApplication.getAppContext(), (Class)ReportIssueService.class);
        intent.setAction("Sync");
        HudApplication.getAppContext().startService(intent);
    }
    
    private void dumpIssue(final IssueType issueType) {
        if (HereMapsManager.getInstance().isNavigationModeOn()) {
            final StringBuilder sb = new StringBuilder();
            sb.append(issueType.ordinal() + "\n");
            sb.append(this.buildDescription());
            this.saveJiraTicketDescriptionToFile(sb.toString(), false);
            ReportIssueService.mLastIssueSubmittedAt = System.currentTimeMillis();
        }
    }
    
    private void dumpSnapshot() {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: ldc_w           "Creating the snapshot of the HUD for support ticket"
        //     6: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //     9: invokestatic    com/navdy/hud/app/HudApplication.getAppContext:()Landroid/content/Context;
        //    12: invokestatic    com/navdy/hud/app/device/light/LightManager.getInstance:()Lcom/navdy/hud/app/device/light/LightManager;
        //    15: iconst_0       
        //    16: invokestatic    com/navdy/hud/app/device/light/HUDLightUtils.showSnapshotCollection:(Landroid/content/Context;Lcom/navdy/hud/app/device/light/LightManager;Z)V
        //    19: invokestatic    com/navdy/hud/app/storage/PathManager.getInstance:()Lcom/navdy/hud/app/storage/PathManager;
        //    22: invokevirtual   com/navdy/hud/app/storage/PathManager.getNonFatalCrashReportDir:()Ljava/lang/String;
        //    25: astore_1       
        //    26: new             Ljava/lang/StringBuilder;
        //    29: astore_2       
        //    30: aload_2        
        //    31: invokespecial   java/lang/StringBuilder.<init>:()V
        //    34: aload_2        
        //    35: aload_1        
        //    36: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    39: getstatic       java/io/File.separator:Ljava/lang/String;
        //    42: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    45: ldc_w           "screen.png"
        //    48: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    51: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    54: invokestatic    com/navdy/hud/app/util/DeviceUtil.takeDeviceScreenShot:(Ljava/lang/String;)V
        //    57: new             Ljava/util/ArrayList;
        //    60: astore_2       
        //    61: aload_2        
        //    62: invokespecial   java/util/ArrayList.<init>:()V
        //    65: getstatic       android/os/Build.TYPE:Ljava/lang/String;
        //    68: ldc_w           "eng"
        //    71: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //    74: ifeq            85
        //    77: aload_0        
        //    78: getfield        com/navdy/hud/app/util/ReportIssueService.gestureService:Lcom/navdy/hud/app/gesture/GestureServiceConnector;
        //    81: invokevirtual   com/navdy/hud/app/gesture/GestureServiceConnector.dumpRecording:()Ljava/lang/String;
        //    84: pop            
        //    85: aload_0        
        //    86: getfield        com/navdy/hud/app/util/ReportIssueService.driveRecorder:Lcom/navdy/hud/app/debug/DriveRecorder;
        //    89: invokevirtual   com/navdy/hud/app/debug/DriveRecorder.flushRecordings:()V
        //    92: aload_0        
        //    93: getfield        com/navdy/hud/app/util/ReportIssueService.gestureService:Lcom/navdy/hud/app/gesture/GestureServiceConnector;
        //    96: ldc_w           "/data/misc/swiped/camera.png"
        //    99: invokevirtual   com/navdy/hud/app/gesture/GestureServiceConnector.takeSnapShot:(Ljava/lang/String;)V
        //   102: new             Ljava/io/File;
        //   105: astore_3       
        //   106: new             Ljava/lang/StringBuilder;
        //   109: astore          4
        //   111: aload           4
        //   113: invokespecial   java/lang/StringBuilder.<init>:()V
        //   116: aload_3        
        //   117: aload           4
        //   119: aload_1        
        //   120: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   123: getstatic       java/io/File.separator:Ljava/lang/String;
        //   126: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   129: ldc_w           "deviceInfo.txt"
        //   132: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   135: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   138: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //   141: aload_3        
        //   142: invokevirtual   java/io/File.exists:()Z
        //   145: ifeq            157
        //   148: aload_0        
        //   149: aload_3        
        //   150: invokevirtual   java/io/File.getAbsolutePath:()Ljava/lang/String;
        //   153: invokestatic    com/navdy/service/library/util/IOUtils.deleteFile:(Landroid/content/Context;Ljava/lang/String;)Z
        //   156: pop            
        //   157: new             Ljava/io/FileWriter;
        //   160: astore          4
        //   162: aload           4
        //   164: aload_3        
        //   165: invokespecial   java/io/FileWriter.<init>:(Ljava/io/File;)V
        //   168: invokestatic    com/navdy/hud/app/manager/RemoteDeviceManager.getInstance:()Lcom/navdy/hud/app/manager/RemoteDeviceManager;
        //   171: invokevirtual   com/navdy/hud/app/manager/RemoteDeviceManager.getConnectionHandler:()Lcom/navdy/hud/app/service/ConnectionHandler;
        //   174: astore          5
        //   176: aload           5
        //   178: ifnull          203
        //   181: aload           5
        //   183: invokevirtual   com/navdy/hud/app/service/ConnectionHandler.getLastConnectedDeviceInfo:()Lcom/navdy/service/library/events/DeviceInfo;
        //   186: astore          5
        //   188: aload           5
        //   190: ifnull          203
        //   193: aload           4
        //   195: aload           5
        //   197: invokestatic    com/navdy/hud/app/util/CrashReportService.printDeviceInfo:(Lcom/navdy/service/library/events/DeviceInfo;)Ljava/lang/String;
        //   200: invokevirtual   java/io/FileWriter.write:(Ljava/lang/String;)V
        //   203: aload           4
        //   205: invokevirtual   java/io/FileWriter.flush:()V
        //   208: aload           4
        //   210: invokevirtual   java/io/FileWriter.close:()V
        //   213: new             Ljava/io/File;
        //   216: astore          4
        //   218: new             Ljava/lang/StringBuilder;
        //   221: astore          5
        //   223: aload           5
        //   225: invokespecial   java/lang/StringBuilder.<init>:()V
        //   228: aload           4
        //   230: aload           5
        //   232: aload_1        
        //   233: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   236: getstatic       java/io/File.separator:Ljava/lang/String;
        //   239: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   242: ldc_w           "temp"
        //   245: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   248: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   251: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //   254: aload           4
        //   256: invokevirtual   java/io/File.exists:()Z
        //   259: ifeq            268
        //   262: aload_0        
        //   263: aload           4
        //   265: invokestatic    com/navdy/service/library/util/IOUtils.deleteDirectory:(Landroid/content/Context;Ljava/io/File;)V
        //   268: aload           4
        //   270: invokevirtual   java/io/File.mkdirs:()Z
        //   273: ifne            336
        //   276: getstatic       com/navdy/hud/app/util/ReportIssueService.sLogger:Lcom/navdy/service/library/log/Logger;
        //   279: ldc_w           "could not create tempDirectory"
        //   282: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;)V
        //   285: invokestatic    com/navdy/hud/app/HudApplication.getAppContext:()Landroid/content/Context;
        //   288: invokestatic    com/navdy/hud/app/device/light/LightManager.getInstance:()Lcom/navdy/hud/app/device/light/LightManager;
        //   291: iconst_1       
        //   292: invokestatic    com/navdy/hud/app/device/light/HUDLightUtils.showSnapshotCollection:(Landroid/content/Context;Lcom/navdy/hud/app/device/light/LightManager;Z)V
        //   295: return         
        //   296: astore          4
        //   298: getstatic       com/navdy/hud/app/util/ReportIssueService.sLogger:Lcom/navdy/service/library/log/Logger;
        //   301: ldc_w           "Error closing the file writer for log file"
        //   304: aload           4
        //   306: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   309: goto            213
        //   312: astore_1       
        //   313: getstatic       com/navdy/hud/app/util/ReportIssueService.sLogger:Lcom/navdy/service/library/log/Logger;
        //   316: ldc_w           "Error creating a snapshot "
        //   319: aload_1        
        //   320: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   323: invokestatic    com/navdy/hud/app/HudApplication.getAppContext:()Landroid/content/Context;
        //   326: invokestatic    com/navdy/hud/app/device/light/LightManager.getInstance:()Lcom/navdy/hud/app/device/light/LightManager;
        //   329: iconst_1       
        //   330: invokestatic    com/navdy/hud/app/device/light/HUDLightUtils.showSnapshotCollection:(Landroid/content/Context;Lcom/navdy/hud/app/device/light/LightManager;Z)V
        //   333: goto            295
        //   336: aload           4
        //   338: invokevirtual   java/io/File.getAbsolutePath:()Ljava/lang/String;
        //   341: astore          5
        //   343: aload           5
        //   345: invokestatic    com/navdy/service/library/util/LogUtils.copySnapshotSystemLogs:(Ljava/lang/String;)V
        //   348: invokestatic    com/navdy/hud/app/device/gps/GpsDeadReckoningManager.getInstance:()Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;
        //   351: aload           5
        //   353: invokevirtual   com/navdy/hud/app/device/gps/GpsDeadReckoningManager.dumpGpsInfo:(Ljava/lang/String;)V
        //   356: invokestatic    com/navdy/hud/app/storage/PathManager.getInstance:()Lcom/navdy/hud/app/storage/PathManager;
        //   359: aload           5
        //   361: invokevirtual   com/navdy/hud/app/storage/PathManager.collectEnvironmentInfo:(Ljava/lang/String;)V
        //   364: aload           4
        //   366: invokevirtual   java/io/File.listFiles:()[Ljava/io/File;
        //   369: astore          6
        //   371: new             Ljava/io/File;
        //   374: astore          5
        //   376: new             Ljava/lang/StringBuilder;
        //   379: astore          7
        //   381: aload           7
        //   383: invokespecial   java/lang/StringBuilder.<init>:()V
        //   386: aload           5
        //   388: aload           7
        //   390: aload_1        
        //   391: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   394: getstatic       java/io/File.separator:Ljava/lang/String;
        //   397: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   400: ldc_w           "route.log"
        //   403: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   406: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   409: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //   412: aload           5
        //   414: invokevirtual   java/io/File.exists:()Z
        //   417: ifeq            430
        //   420: aload_0        
        //   421: aload           5
        //   423: invokevirtual   java/io/File.getAbsolutePath:()Ljava/lang/String;
        //   426: invokestatic    com/navdy/service/library/util/IOUtils.deleteFile:(Landroid/content/Context;Ljava/lang/String;)Z
        //   429: pop            
        //   430: aload_0        
        //   431: invokespecial   com/navdy/hud/app/util/ReportIssueService.buildDescription:()Ljava/lang/String;
        //   434: astore          8
        //   436: new             Ljava/io/FileWriter;
        //   439: astore          7
        //   441: aload           7
        //   443: aload           5
        //   445: invokespecial   java/io/FileWriter.<init>:(Ljava/io/File;)V
        //   448: aload           7
        //   450: aload           8
        //   452: invokevirtual   java/io/FileWriter.write:(Ljava/lang/String;)V
        //   455: aload           7
        //   457: invokevirtual   java/io/FileWriter.flush:()V
        //   460: aload           7
        //   462: invokevirtual   java/io/FileWriter.close:()V
        //   465: new             Ljava/io/File;
        //   468: astore          7
        //   470: aload           7
        //   472: ldc_w           "/data/misc/swiped/camera.png"
        //   475: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //   478: aload_2        
        //   479: aload           7
        //   481: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //   486: pop            
        //   487: new             Ljava/io/File;
        //   490: astore          7
        //   492: new             Ljava/lang/StringBuilder;
        //   495: astore          8
        //   497: aload           8
        //   499: invokespecial   java/lang/StringBuilder.<init>:()V
        //   502: aload           7
        //   504: aload           8
        //   506: aload_1        
        //   507: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   510: getstatic       java/io/File.separator:Ljava/lang/String;
        //   513: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   516: ldc_w           "screen.png"
        //   519: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   522: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   525: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //   528: aload_2        
        //   529: aload           7
        //   531: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //   536: pop            
        //   537: aload_2        
        //   538: aload           5
        //   540: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //   545: pop            
        //   546: aload_2        
        //   547: aload_3        
        //   548: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //   553: pop            
        //   554: aload           6
        //   556: ifnull          566
        //   559: aload_2        
        //   560: aload           6
        //   562: invokestatic    java/util/Collections.addAll:(Ljava/util/Collection;[Ljava/lang/Object;)Z
        //   565: pop            
        //   566: new             Ljava/util/HashSet;
        //   569: astore_3       
        //   570: aload_3        
        //   571: invokespecial   java/util/HashSet.<init>:()V
        //   574: iconst_1       
        //   575: invokestatic    com/navdy/hud/app/util/ReportIssueService.getLatestDriveLogFiles:(I)Ljava/util/List;
        //   578: astore          5
        //   580: aload           5
        //   582: ifnull          670
        //   585: aload           5
        //   587: invokeinterface java/util/List.size:()I
        //   592: ifle            670
        //   595: aload_2        
        //   596: aload           5
        //   598: invokeinterface java/util/List.addAll:(Ljava/util/Collection;)Z
        //   603: pop            
        //   604: aload           5
        //   606: invokeinterface java/util/List.iterator:()Ljava/util/Iterator;
        //   611: astore          5
        //   613: aload           5
        //   615: invokeinterface java/util/Iterator.hasNext:()Z
        //   620: ifeq            670
        //   623: aload_3        
        //   624: aload           5
        //   626: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //   631: checkcast       Ljava/io/File;
        //   634: invokevirtual   java/util/HashSet.add:(Ljava/lang/Object;)Z
        //   637: pop            
        //   638: goto            613
        //   641: astore_1       
        //   642: invokestatic    com/navdy/hud/app/HudApplication.getAppContext:()Landroid/content/Context;
        //   645: invokestatic    com/navdy/hud/app/device/light/LightManager.getInstance:()Lcom/navdy/hud/app/device/light/LightManager;
        //   648: iconst_1       
        //   649: invokestatic    com/navdy/hud/app/device/light/HUDLightUtils.showSnapshotCollection:(Landroid/content/Context;Lcom/navdy/hud/app/device/light/LightManager;Z)V
        //   652: aload_1        
        //   653: athrow         
        //   654: astore          7
        //   656: getstatic       com/navdy/hud/app/util/ReportIssueService.sLogger:Lcom/navdy/service/library/log/Logger;
        //   659: ldc_w           "Error closing the file writer for route log file"
        //   662: aload           7
        //   664: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   667: goto            465
        //   670: new             Ljava/util/Date;
        //   673: astore          5
        //   675: aload           5
        //   677: invokestatic    java/lang/System.currentTimeMillis:()J
        //   680: invokespecial   java/util/Date.<init>:(J)V
        //   683: getstatic       com/navdy/hud/app/util/ReportIssueService.SNAPSHOT_DATE_FORMAT:Ljava/text/SimpleDateFormat;
        //   686: aload           5
        //   688: invokevirtual   java/text/SimpleDateFormat.format:(Ljava/util/Date;)Ljava/lang/String;
        //   691: astore          6
        //   693: new             Ljava/lang/StringBuilder;
        //   696: astore          5
        //   698: aload           5
        //   700: invokespecial   java/lang/StringBuilder.<init>:()V
        //   703: aload           5
        //   705: ldc_w           "snapshot-"
        //   708: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   711: getstatic       android/os/Build.SERIAL:Ljava/lang/String;
        //   714: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   717: ldc_w           "-"
        //   720: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   723: getstatic       android/os/Build$VERSION.INCREMENTAL:Ljava/lang/String;
        //   726: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   729: ldc_w           "_"
        //   732: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   735: aload           6
        //   737: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   740: ldc             ".zip"
        //   742: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   745: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   748: astore          7
        //   750: new             Ljava/io/File;
        //   753: astore          5
        //   755: new             Ljava/lang/StringBuilder;
        //   758: astore          6
        //   760: aload           6
        //   762: invokespecial   java/lang/StringBuilder.<init>:()V
        //   765: aload           5
        //   767: aload           6
        //   769: aload_1        
        //   770: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   773: getstatic       java/io/File.separator:Ljava/lang/String;
        //   776: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   779: aload           7
        //   781: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   784: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   787: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //   790: aload           5
        //   792: invokevirtual   java/io/File.exists:()Z
        //   795: ifeq            810
        //   798: invokestatic    com/navdy/hud/app/HudApplication.getAppContext:()Landroid/content/Context;
        //   801: aload           5
        //   803: invokevirtual   java/io/File.getAbsolutePath:()Ljava/lang/String;
        //   806: invokestatic    com/navdy/service/library/util/IOUtils.deleteFile:(Landroid/content/Context;Ljava/lang/String;)Z
        //   809: pop            
        //   810: aload_2        
        //   811: invokeinterface java/util/List.size:()I
        //   816: anewarray       Ljava/io/File;
        //   819: astore_1       
        //   820: aload_2        
        //   821: aload_1        
        //   822: invokeinterface java/util/List.toArray:([Ljava/lang/Object;)[Ljava/lang/Object;
        //   827: pop            
        //   828: aload           5
        //   830: invokevirtual   java/io/File.createNewFile:()Z
        //   833: ifne            858
        //   836: getstatic       com/navdy/hud/app/util/ReportIssueService.sLogger:Lcom/navdy/service/library/log/Logger;
        //   839: ldc_w           "snapshot file could not be created"
        //   842: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;)V
        //   845: invokestatic    com/navdy/hud/app/HudApplication.getAppContext:()Landroid/content/Context;
        //   848: invokestatic    com/navdy/hud/app/device/light/LightManager.getInstance:()Lcom/navdy/hud/app/device/light/LightManager;
        //   851: iconst_1       
        //   852: invokestatic    com/navdy/hud/app/device/light/HUDLightUtils.showSnapshotCollection:(Landroid/content/Context;Lcom/navdy/hud/app/device/light/LightManager;Z)V
        //   855: goto            295
        //   858: aload_0        
        //   859: aload_1        
        //   860: aload           5
        //   862: invokevirtual   java/io/File.getAbsolutePath:()Ljava/lang/String;
        //   865: invokestatic    com/navdy/service/library/util/IOUtils.compressFilesToZip:(Landroid/content/Context;[Ljava/io/File;Ljava/lang/String;)V
        //   868: aload_2        
        //   869: invokeinterface java/util/List.iterator:()Ljava/util/Iterator;
        //   874: astore_2       
        //   875: aload_2        
        //   876: invokeinterface java/util/Iterator.hasNext:()Z
        //   881: ifeq            916
        //   884: aload_2        
        //   885: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //   890: checkcast       Ljava/io/File;
        //   893: astore_1       
        //   894: aload_3        
        //   895: aload_1        
        //   896: invokevirtual   java/util/HashSet.contains:(Ljava/lang/Object;)Z
        //   899: ifne            875
        //   902: invokestatic    com/navdy/hud/app/HudApplication.getAppContext:()Landroid/content/Context;
        //   905: aload_1        
        //   906: invokevirtual   java/io/File.getAbsolutePath:()Ljava/lang/String;
        //   909: invokestatic    com/navdy/service/library/util/IOUtils.deleteFile:(Landroid/content/Context;Ljava/lang/String;)Z
        //   912: pop            
        //   913: goto            875
        //   916: aload_0        
        //   917: aload           4
        //   919: invokestatic    com/navdy/service/library/util/IOUtils.deleteDirectory:(Landroid/content/Context;Ljava/io/File;)V
        //   922: aload           5
        //   924: putstatic       com/navdy/hud/app/util/ReportIssueService.lastSnapShotFile:Ljava/io/File;
        //   927: getstatic       com/navdy/hud/app/util/ReportIssueService.sLogger:Lcom/navdy/service/library/log/Logger;
        //   930: astore_1       
        //   931: new             Ljava/lang/StringBuilder;
        //   934: astore_2       
        //   935: aload_2        
        //   936: invokespecial   java/lang/StringBuilder.<init>:()V
        //   939: aload_1        
        //   940: aload_2        
        //   941: ldc_w           "Snapshot File created "
        //   944: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   947: getstatic       com/navdy/hud/app/util/ReportIssueService.lastSnapShotFile:Ljava/io/File;
        //   950: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   953: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   956: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //   959: aload           5
        //   961: invokevirtual   java/io/File.getAbsolutePath:()Ljava/lang/String;
        //   964: invokestatic    com/navdy/hud/app/util/CrashReportService.addSnapshotAsync:(Ljava/lang/String;)V
        //   967: invokestatic    com/navdy/hud/app/HudApplication.getAppContext:()Landroid/content/Context;
        //   970: invokestatic    com/navdy/hud/app/device/light/LightManager.getInstance:()Lcom/navdy/hud/app/device/light/LightManager;
        //   973: iconst_1       
        //   974: invokestatic    com/navdy/hud/app/device/light/HUDLightUtils.showSnapshotCollection:(Landroid/content/Context;Lcom/navdy/hud/app/device/light/LightManager;Z)V
        //   977: goto            295
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  19     85     312    336    Ljava/lang/Throwable;
        //  19     85     641    654    Any
        //  85     157    312    336    Ljava/lang/Throwable;
        //  85     157    641    654    Any
        //  157    176    312    336    Ljava/lang/Throwable;
        //  157    176    641    654    Any
        //  181    188    312    336    Ljava/lang/Throwable;
        //  181    188    641    654    Any
        //  193    203    312    336    Ljava/lang/Throwable;
        //  193    203    641    654    Any
        //  203    208    312    336    Ljava/lang/Throwable;
        //  203    208    641    654    Any
        //  208    213    296    312    Ljava/io/IOException;
        //  208    213    312    336    Ljava/lang/Throwable;
        //  208    213    641    654    Any
        //  213    268    312    336    Ljava/lang/Throwable;
        //  213    268    641    654    Any
        //  268    285    312    336    Ljava/lang/Throwable;
        //  268    285    641    654    Any
        //  298    309    312    336    Ljava/lang/Throwable;
        //  298    309    641    654    Any
        //  313    323    641    654    Any
        //  336    430    312    336    Ljava/lang/Throwable;
        //  336    430    641    654    Any
        //  430    460    312    336    Ljava/lang/Throwable;
        //  430    460    641    654    Any
        //  460    465    654    670    Ljava/io/IOException;
        //  460    465    312    336    Ljava/lang/Throwable;
        //  460    465    641    654    Any
        //  465    554    312    336    Ljava/lang/Throwable;
        //  465    554    641    654    Any
        //  559    566    312    336    Ljava/lang/Throwable;
        //  559    566    641    654    Any
        //  566    580    312    336    Ljava/lang/Throwable;
        //  566    580    641    654    Any
        //  585    613    312    336    Ljava/lang/Throwable;
        //  585    613    641    654    Any
        //  613    638    312    336    Ljava/lang/Throwable;
        //  613    638    641    654    Any
        //  656    667    312    336    Ljava/lang/Throwable;
        //  656    667    641    654    Any
        //  670    810    312    336    Ljava/lang/Throwable;
        //  670    810    641    654    Any
        //  810    845    312    336    Ljava/lang/Throwable;
        //  810    845    641    654    Any
        //  858    875    312    336    Ljava/lang/Throwable;
        //  858    875    641    654    Any
        //  875    913    312    336    Ljava/lang/Throwable;
        //  875    913    641    654    Any
        //  916    967    312    336    Ljava/lang/Throwable;
        //  916    967    641    654    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0465:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static void dumpSnapshotAsync() {
        final Intent intent = new Intent(HudApplication.getAppContext(), (Class)ReportIssueService.class);
        intent.setAction("Snapshot");
        HudApplication.getAppContext().startService(intent);
    }
    
    public static IssueType getIssueTypeForId(final int n) {
        final IssueType[] values = IssueType.values();
        IssueType issueType;
        if (n >= 0 && n < values.length) {
            issueType = values[n];
        }
        else {
            issueType = null;
        }
        return issueType;
    }
    
    public static List<File> getLatestDriveLogFiles(final int n) {
        final List<File> list = null;
        List<File> list2;
        if (n <= 0) {
            list2 = list;
        }
        else {
            final File[] listFiles = DriveRecorder.getDriveLogsDir("").listFiles();
            list2 = list;
            if (listFiles != null) {
                list2 = list;
                if (listFiles.length != 0) {
                    final ArrayList<File> list3 = new ArrayList<File>();
                    Arrays.<File>sort(listFiles, new Comparator<File>() {
                        @Override
                        public int compare(final File file, final File file2) {
                            return Long.compare(file.lastModified(), file2.lastModified());
                        }
                    });
                    int n2 = listFiles.length - 1;
                    while (true) {
                        list2 = list3;
                        if (n2 < 0) {
                            break;
                        }
                        list2 = list3;
                        if (list3.size() >= n * 4) {
                            break;
                        }
                        list3.add(listFiles[n2]);
                        --n2;
                    }
                }
            }
        }
        return list2;
    }
    
    private String getSummary(final IssueType issueType) {
        String s;
        if (issueType.getMessageStringResource() != 0) {
            s = "Navigation issue: [" + issueType.getIssueTypeCode() + "] " + this.getString(issueType.getTitleStringResource()) + ": " + this.getString(issueType.getMessageStringResource());
        }
        else {
            s = "Navigation issue: [" + issueType.getIssueTypeCode() + "] " + this.getString(issueType.getTitleStringResource());
        }
        return s;
    }
    
    public static void initialize() {
        final int n = 0;
        if (!ReportIssueService.mIsInitialized) {
            ReportIssueService.sLogger.d("Initializing the service statically");
            ReportIssueService.navigationIssuesFolder = PathManager.getInstance().getNavigationIssuesDir();
            final File[] listFiles = new File(ReportIssueService.navigationIssuesFolder).listFiles();
            final Logger sLogger = ReportIssueService.sLogger;
            final StringBuilder append = new StringBuilder().append("Number of Files waiting for sync :");
            int length;
            if (listFiles != null) {
                length = listFiles.length;
            }
            else {
                length = 0;
            }
            sLogger.d(append.append(length).toString());
            if (listFiles != null) {
                for (int length2 = listFiles.length, i = n; i < length2; ++i) {
                    final File file = listFiles[i];
                    ReportIssueService.sLogger.d("File " + file.getName());
                    ReportIssueService.mIssuesToSync.add(file);
                    if (ReportIssueService.mIssuesToSync.size() == 10) {
                        final File file2 = ReportIssueService.mIssuesToSync.poll();
                        ReportIssueService.sLogger.d("Deleting the old file " + file2.getName());
                        IOUtils.deleteFile(HudApplication.getAppContext(), file2.getAbsolutePath());
                    }
                }
            }
            ReportIssueService.mIsInitialized = true;
        }
    }
    
    private void onSyncComplete(final File file) {
        if (file != null) {
            ReportIssueService.sLogger.d("Removed the file from list :" + ReportIssueService.mIssuesToSync.remove(file));
            IOUtils.deleteFile((Context)this, file.getAbsolutePath());
            dispatchSync();
        }
        ReportIssueService.mSyncing.set(false);
    }
    
    private void saveJiraTicketDescriptionToFile(final String p0, final boolean p1) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: lstore_3       
        //     4: new             Ljava/lang/StringBuilder;
        //     7: dup            
        //     8: invokespecial   java/lang/StringBuilder.<init>:()V
        //    11: getstatic       com/navdy/hud/app/util/ReportIssueService.navigationIssuesFolder:Ljava/lang/String;
        //    14: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    17: getstatic       java/io/File.separator:Ljava/lang/String;
        //    20: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    23: getstatic       com/navdy/hud/app/util/ReportIssueService.DATE_FORMAT:Ljava/text/SimpleDateFormat;
        //    26: new             Ljava/util/Date;
        //    29: dup            
        //    30: lload_3        
        //    31: invokespecial   java/util/Date.<init>:(J)V
        //    34: invokevirtual   java/text/SimpleDateFormat.format:(Ljava/util/Date;)Ljava/lang/String;
        //    37: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    40: astore          5
        //    42: iload_2        
        //    43: ifeq            164
        //    46: ldc_w           ".json"
        //    49: astore          6
        //    51: new             Ljava/io/File;
        //    54: dup            
        //    55: aload           5
        //    57: aload           6
        //    59: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    62: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    65: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //    68: astore          7
        //    70: aload           7
        //    72: invokevirtual   java/io/File.exists:()Z
        //    75: ifeq            88
        //    78: aload_0        
        //    79: aload           7
        //    81: invokevirtual   java/io/File.getAbsolutePath:()Ljava/lang/String;
        //    84: invokestatic    com/navdy/service/library/util/IOUtils.deleteFile:(Landroid/content/Context;Ljava/lang/String;)Z
        //    87: pop            
        //    88: aconst_null    
        //    89: astore          8
        //    91: aconst_null    
        //    92: astore          9
        //    94: aconst_null    
        //    95: astore          5
        //    97: aload           9
        //    99: astore          6
        //   101: aload           7
        //   103: invokevirtual   java/io/File.createNewFile:()Z
        //   106: ifeq            158
        //   109: aload           9
        //   111: astore          6
        //   113: new             Ljava/io/FileWriter;
        //   116: astore          5
        //   118: aload           9
        //   120: astore          6
        //   122: aload           5
        //   124: aload           7
        //   126: invokespecial   java/io/FileWriter.<init>:(Ljava/io/File;)V
        //   129: aload           5
        //   131: aload_1        
        //   132: invokevirtual   java/io/FileWriter.write:(Ljava/lang/String;)V
        //   135: aload           5
        //   137: invokevirtual   java/io/FileWriter.close:()V
        //   140: getstatic       com/navdy/hud/app/util/ReportIssueService.mIssuesToSync:Ljava/util/concurrent/PriorityBlockingQueue;
        //   143: aload           7
        //   145: invokevirtual   java/util/concurrent/PriorityBlockingQueue.add:(Ljava/lang/Object;)Z
        //   148: pop            
        //   149: aload           5
        //   151: astore_1       
        //   152: invokestatic    com/navdy/hud/app/util/ReportIssueService.dispatchSync:()V
        //   155: aload_1        
        //   156: astore          5
        //   158: aload           5
        //   160: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   163: return         
        //   164: ldc_w           ""
        //   167: astore          6
        //   169: goto            51
        //   172: astore          5
        //   174: aload           8
        //   176: astore_1       
        //   177: aload_1        
        //   178: astore          6
        //   180: getstatic       com/navdy/hud/app/util/ReportIssueService.sLogger:Lcom/navdy/service/library/log/Logger;
        //   183: astore          8
        //   185: aload_1        
        //   186: astore          6
        //   188: new             Ljava/lang/StringBuilder;
        //   191: astore          9
        //   193: aload_1        
        //   194: astore          6
        //   196: aload           9
        //   198: invokespecial   java/lang/StringBuilder.<init>:()V
        //   201: aload_1        
        //   202: astore          6
        //   204: aload           8
        //   206: aload           9
        //   208: ldc_w           "Error while dumping the issue "
        //   211: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   214: aload           5
        //   216: invokevirtual   java/io/IOException.getMessage:()Ljava/lang/String;
        //   219: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   222: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   225: aload           5
        //   227: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   230: aload_1        
        //   231: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   234: goto            163
        //   237: astore_1       
        //   238: aload           6
        //   240: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   243: aload_1        
        //   244: athrow         
        //   245: astore_1       
        //   246: aload           5
        //   248: astore          6
        //   250: goto            238
        //   253: astore_1       
        //   254: aload           5
        //   256: astore          6
        //   258: aload_1        
        //   259: astore          5
        //   261: aload           6
        //   263: astore_1       
        //   264: goto            177
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  101    109    172    177    Ljava/io/IOException;
        //  101    109    237    238    Any
        //  113    118    172    177    Ljava/io/IOException;
        //  113    118    237    238    Any
        //  122    129    172    177    Ljava/io/IOException;
        //  122    129    237    238    Any
        //  129    149    253    267    Ljava/io/IOException;
        //  129    149    245    253    Any
        //  152    155    253    267    Ljava/io/IOException;
        //  152    155    245    253    Any
        //  180    185    237    238    Any
        //  188    193    237    238    Any
        //  196    201    237    238    Any
        //  204    230    237    238    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0158:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static void scheduleSync() {
        final Intent intent = new Intent(HudApplication.getAppContext(), (Class)ReportIssueService.class);
        intent.setAction("Sync");
        ((AlarmManager)HudApplication.getAppContext().getSystemService("alarm")).set(3, SystemClock.elapsedRealtime() + ReportIssueService.RETRY_INTERVAL, PendingIntent.getService(HudApplication.getAppContext(), 128, intent, 268435456));
    }
    
    public static void showSnapshotMenu() {
        final Bundle bundle = new Bundle();
        bundle.putInt("MENU_MODE", MainMenuScreen2.MenuMode.SNAPSHOT_TITLE_PICKER.ordinal());
        RemoteDeviceManager.getInstance().getBus().post(new ShowScreenWithArgs(Screen.SCREEN_MAIN_MENU, bundle, null, false));
    }
    
    private void submitJiraTicket(String s) {
        ReportIssueService.sLogger.d("Submitting jira ticket with title " + s);
        try {
            final Date date = new Date(System.currentTimeMillis());
            final ConnectionHandler connectionHandler = RemoteDeviceManager.getInstance().getConnectionHandler();
            final JSONObject jsonObject = new JSONObject();
            jsonObject.put("attachment", ReportIssueService.lastSnapShotFile.getAbsolutePath());
            final StringBuilder sb = new StringBuilder();
            final UIStateManager uiStateManager = RemoteDeviceManager.getInstance().getUiStateManager();
            final String format = ReportIssueService.SNAPSHOT_JIRA_TICKET_DATE_FORMAT.format(date);
            final DriverProfile currentProfile = DriverProfileHelper.getInstance().getCurrentProfile();
            final StringBuilder append = new StringBuilder().append("[Snapshot ").append(s).append(" ").append(format).append("] ");
            if (HomeScreen.DisplayMode.MAP.equals(uiStateManager.getHomescreenView().getDisplayMode())) {
                s = "Map";
            }
            else {
                s = "Dash";
            }
            sb.append(append.append(s).toString());
            s = HereMapUtil.getCurrentRoadName();
            if (!TextUtils.isEmpty((CharSequence)s)) {
                sb.append(" ").append(s);
            }
            if (!currentProfile.isDefaultProfile()) {
                sb.append(" , From ").append(currentProfile.getDriverEmail());
            }
            jsonObject.put("Summary", sb.toString());
            if (!currentProfile.isDefaultProfile()) {
                s = currentProfile.getDriverName();
                if (!TextUtils.isEmpty((CharSequence)s)) {
                    final String trim = s.split(" ")[0].trim();
                    ReportIssueService.sLogger.d("Assignee name for the snapshot " + trim);
                    jsonObject.put("assignee", trim);
                }
                s = currentProfile.getDriverEmail();
                if (!TextUtils.isEmpty((CharSequence)s)) {
                    jsonObject.put("assigneeEmail", s);
                }
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("HUD " + Build$VERSION.INCREMENTAL + "\n");
            if (connectionHandler != null) {
                final DeviceInfo lastConnectedDeviceInfo = connectionHandler.getLastConnectedDeviceInfo();
                if (lastConnectedDeviceInfo != null) {
                    if (DeviceInfo.Platform.PLATFORM_iOS.equals(lastConnectedDeviceInfo.platform)) {
                        s = "iPhone";
                    }
                    else {
                        s = "Android";
                    }
                    sb2.append(s);
                    sb2.append(" V" + lastConnectedDeviceInfo.clientVersion + "\n");
                }
            }
            sb2.append("Language : " + HudLocale.getCurrentLocale(HudApplication.getAppContext()).getLanguage());
            jsonObject.put("Environment", sb2.toString());
            this.saveJiraTicketDescriptionToFile(jsonObject.toString(), true);
        }
        catch (JSONException ex) {
            ex.printStackTrace();
        }
    }
    
    public static void submitJiraTicketAsync(final String s) {
        final Intent intent = new Intent(HudApplication.getAppContext(), (Class)ReportIssueService.class);
        intent.setAction("Jira");
        intent.putExtra("EXTRA_SNAPSHOT_TITLE", s);
        HudApplication.getAppContext().startService(intent);
    }
    
    private void sync() {
        if (!NetworkBandwidthController.getInstance().isNetworkAccessAllowed(NetworkBandwidthController.Component.JIRA)) {
            scheduleSync();
        }
        else {
            final boolean remoteDeviceConnected = RemoteDeviceManager.getInstance().isRemoteDeviceConnected();
            final boolean connectedToNetwork = NetworkStateManager.isConnectedToNetwork((Context)this);
            ReportIssueService.sLogger.d("Trying to sync , already syncing:" + ReportIssueService.mSyncing.get() + ", Navigation mode on:" + HereMapsManager.getInstance().isNavigationModeOn() + ", Device connected:" + remoteDeviceConnected + ", Internet connected :" + connectedToNetwork);
            Label_0856: {
                if (!remoteDeviceConnected || !connectedToNetwork) {
                    break Label_0856;
                }
                if (!ReportIssueService.mSyncing.compareAndSet(false, true)) {
                    return;
                }
                final File file = ReportIssueService.mIssuesToSync.peek();
                Label_0839: {
                    if (file == null || !file.exists()) {
                        break Label_0839;
                    }
                    if (file.getName().endsWith(".json")) {
                        try {
                            ReportIssueService.sLogger.d("Submitting ticket from JSON file");
                            final JSONObject jsonObject = new JSONObject(IOUtils.convertFileToString(file.getAbsolutePath()));
                            jsonObject.getString("Summary");
                            if (!jsonObject.has("Environment")) {
                                goto Label_0456;
                            }
                            jsonObject.getString("Environment");
                            if (!jsonObject.has("assignee")) {
                                goto Label_0464;
                            }
                            String string = jsonObject.getString("assignee");
                            if (!jsonObject.has("assigneeEmail")) {
                                goto Label_0472;
                            }
                            jsonObject.getString("assigneeEmail");
                            String s = string;
                            if (TextUtils.isEmpty((CharSequence)string)) {
                                final DriverProfile currentProfile = DriverProfileHelper.getInstance().getCurrentProfile();
                                final String driverName = currentProfile.getDriverName();
                                if (!TextUtils.isEmpty((CharSequence)driverName)) {
                                    string = driverName.trim().split(" ")[0];
                                }
                                currentProfile.getDriverEmail();
                                s = string;
                            }
                            ReportIssueService.sLogger.d("Assignee name " + s);
                            if (!jsonObject.has("attachment")) {
                                goto Label_0480;
                            }
                            final String string2 = jsonObject.getString("attachment");
                            ReportIssueService.sLogger.d("Attachment " + string2);
                            if (new File(string2).exists()) {
                                goto Label_0488;
                            }
                            ReportIssueService.sLogger.e("Snapshot file has been deleted, not creating a ticket ");
                            this.onSyncComplete(file);
                        }
                        catch (IOException ex) {
                            ReportIssueService.sLogger.e("Not able to read json file ", ex);
                            this.syncLater();
                        }
                        catch (JSONException ex2) {
                            ReportIssueService.sLogger.e("Not able to read json file ", (Throwable)ex2);
                            this.syncLater();
                        }
                        return;
                    }
                    Serializable s2 = null;
                    while (true) {
                        try {
                            final Object o = new FileReader(file);
                            BufferedReader bufferedReader = null;
                            Label_0744: {
                                try {
                                    bufferedReader = new BufferedReader((Reader)o);
                                    s2 = getIssueTypeForId(Integer.parseInt(bufferedReader.readLine().trim()));
                                    if (s2 == null) {
                                        s2 = new IOException("Bad file format");
                                        throw s2;
                                    }
                                    break Label_0744;
                                }
                                catch (Exception ex3) {}
                                IOUtils.closeStream((Closeable)o);
                                this.onSyncComplete(file);
                                return;
                            }
                            s2 = this.getSummary((IssueType)s2);
                            final StringBuilder sb = new StringBuilder();
                            while (true) {
                                final String line = bufferedReader.readLine();
                                if (line == null) {
                                    break;
                                }
                                sb.append(line).append("\n");
                            }
                            IOUtils.closeStream((Closeable)o);
                            this.mJiraClient.submitTicket("NP", "Task", (String)s2, sb.toString(), (JiraClient.ResultCallback)new JiraClient.ResultCallback() {
                                @Override
                                public void onError(final Throwable t) {
                                    ReportIssueService.sLogger.e("Error during sync " + t);
                                    ReportIssueService.this.syncLater();
                                }
                                
                                @Override
                                public void onSuccess(final Object o) {
                                    if (o instanceof String) {
                                        ReportIssueService.sLogger.d("Issue reported " + o);
                                    }
                                    ReportIssueService.sLogger.d("Sync succeeded");
                                    ReportIssueService.this.onSyncComplete(file);
                                }
                            });
                            return;
                            ReportIssueService.sLogger.d("Ticket file does not exist anymore");
                            this.onSyncComplete(file);
                            return;
                            ReportIssueService.sLogger.d("Already sync is running, scheduling for a later time");
                            scheduleSync();
                        }
                        catch (Exception ex4) {
                            final Object o = s2;
                            continue;
                        }
                        break;
                    }
                }
            }
        }
    }
    
    private void syncLater() {
        ReportIssueService.mSyncing.set(false);
        scheduleSync();
    }
    
    public void onCreate() {
        super.onCreate();
        Mortar.inject(HudApplication.getAppContext(), this);
        ReportIssueService.sLogger.d("ReportIssue service started");
        (this.mJiraClient = new JiraClient(this.mHttpManager.getClientCopy().readTimeout(120L, TimeUnit.SECONDS).connectTimeout(120L, TimeUnit.SECONDS).build())).setEncodedCredentials(CredentialUtil.getCredentials((Context)this, "JIRA_CREDENTIALS"));
    }
    
    protected void onHandleIntent(final Intent intent) {
        while (true) {
            Label_0128: {
                try {
                    ReportIssueService.sLogger.d("onHandleIntent " + intent.toString());
                    if ("Dump".equals(intent.getAction())) {
                        final IssueType issueType = (IssueType)intent.getSerializableExtra("EXTRA_ISSUE_TYPE");
                        ReportIssueService.sLogger.d("Dumping an issue " + issueType);
                        if (issueType != null) {
                            this.dumpIssue(issueType);
                        }
                    }
                    else {
                        if (!"Snapshot".equals(intent.getAction())) {
                            break Label_0128;
                        }
                        this.dumpSnapshot();
                    }
                    return;
                }
                catch (Throwable t) {
                    ReportIssueService.sLogger.e("Exception while handling intent ", t);
                    return;
                }
            }
            if ("Jira".equals(intent.getAction())) {
                String stringExtra;
                if (TextUtils.isEmpty((CharSequence)(stringExtra = intent.getStringExtra("EXTRA_SNAPSHOT_TITLE")))) {
                    stringExtra = "HUD";
                }
                this.submitJiraTicket(stringExtra);
                return;
            }
            ReportIssueService.sLogger.d("Handling Sync request");
            this.sync();
        }
    }
    
    static class FilesModifiedTimeComparator implements Comparator<File>
    {
        @Override
        public int compare(final File file, final File file2) {
            return (int)(file.lastModified() - file2.lastModified());
        }
    }
    
    public enum IssueType
    {
        INEFFICIENT_ROUTE_ETA_TRAFFIC(R.string.eta_inaccurate_traffic, 0, 107), 
        INEFFICIENT_ROUTE_SELECTED(R.string.route_inefficient, 0, 106), 
        OTHER(R.string.other_navigation_issue, 0, 105), 
        ROAD_CLOSED(R.string.road_closed, R.string.road_closed_message, 103), 
        ROAD_CLOSED_PERMANENT(R.string.permanent_closure, 0, 108), 
        ROAD_NAME(R.string.road_name, 0, 104), 
        WRONG_DIRECTION(R.string.wrong_direction, 0, 102);
        
        private int issueTypeCode;
        private int messageStringResource;
        private int titleStringResource;
        
        private IssueType(final int titleStringResource, final int messageStringResource, final int issueTypeCode) {
            this.titleStringResource = titleStringResource;
            this.messageStringResource = messageStringResource;
            this.issueTypeCode = issueTypeCode;
        }
        
        public int getIssueTypeCode() {
            return this.issueTypeCode;
        }
        
        public int getMessageStringResource() {
            return this.messageStringResource;
        }
        
        public int getTitleStringResource() {
            return this.titleStringResource;
        }
    }
}
