package com.navdy.hud.app.util;

import com.google.i18n.phonenumbers.Phonenumber;
import java.util.Locale;
import android.text.TextUtils;
import com.navdy.hud.app.framework.DriverProfileHelper;
import java.util.HashMap;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.navdy.service.library.log.Logger;
import java.util.Map;

public class PhoneUtil
{
    private static final Map<String, Integer> sISOCountryCodeMap;
    private static final Logger sLogger;
    private static final PhoneNumberUtil sPhoneNumberUtil;
    
    static {
        sLogger = new Logger(PhoneUtil.class);
        sPhoneNumberUtil = PhoneNumberUtil.getInstance();
        (sISOCountryCodeMap = new HashMap<String, Integer>(310)).put("US", 1);
        PhoneUtil.sISOCountryCodeMap.put("RU", 7);
        PhoneUtil.sISOCountryCodeMap.put("KZ", 7);
        PhoneUtil.sISOCountryCodeMap.put("EG", 20);
        PhoneUtil.sISOCountryCodeMap.put("ZA", 27);
        PhoneUtil.sISOCountryCodeMap.put("GR", 30);
        PhoneUtil.sISOCountryCodeMap.put("NL", 31);
        PhoneUtil.sISOCountryCodeMap.put("BE", 32);
        PhoneUtil.sISOCountryCodeMap.put("FR", 33);
        PhoneUtil.sISOCountryCodeMap.put("ES", 34);
        PhoneUtil.sISOCountryCodeMap.put("HU", 36);
        PhoneUtil.sISOCountryCodeMap.put("IT", 39);
        PhoneUtil.sISOCountryCodeMap.put("VA", 39);
        PhoneUtil.sISOCountryCodeMap.put("RO", 40);
        PhoneUtil.sISOCountryCodeMap.put("CH", 41);
        PhoneUtil.sISOCountryCodeMap.put("AT", 43);
        PhoneUtil.sISOCountryCodeMap.put("GB", 44);
        PhoneUtil.sISOCountryCodeMap.put("GG", 44);
        PhoneUtil.sISOCountryCodeMap.put("IM", 44);
        PhoneUtil.sISOCountryCodeMap.put("JE", 44);
        PhoneUtil.sISOCountryCodeMap.put("DK", 45);
        PhoneUtil.sISOCountryCodeMap.put("SE", 46);
        PhoneUtil.sISOCountryCodeMap.put("NO", 47);
        PhoneUtil.sISOCountryCodeMap.put("SJ", 47);
        PhoneUtil.sISOCountryCodeMap.put("PL", 48);
        PhoneUtil.sISOCountryCodeMap.put("DE", 49);
        PhoneUtil.sISOCountryCodeMap.put("PE", 51);
        PhoneUtil.sISOCountryCodeMap.put("MX", 52);
        PhoneUtil.sISOCountryCodeMap.put("CU", 53);
        PhoneUtil.sISOCountryCodeMap.put("AR", 54);
        PhoneUtil.sISOCountryCodeMap.put("BR", 55);
        PhoneUtil.sISOCountryCodeMap.put("CL", 56);
        PhoneUtil.sISOCountryCodeMap.put("CO", 57);
        PhoneUtil.sISOCountryCodeMap.put("VE", 58);
        PhoneUtil.sISOCountryCodeMap.put("MY", 60);
        PhoneUtil.sISOCountryCodeMap.put("AU", 61);
        PhoneUtil.sISOCountryCodeMap.put("CC", 61);
        PhoneUtil.sISOCountryCodeMap.put("CX", 61);
        PhoneUtil.sISOCountryCodeMap.put("ID", 62);
        PhoneUtil.sISOCountryCodeMap.put("PH", 63);
        PhoneUtil.sISOCountryCodeMap.put("NZ", 64);
        PhoneUtil.sISOCountryCodeMap.put("SG", 65);
        PhoneUtil.sISOCountryCodeMap.put("TH", 66);
        PhoneUtil.sISOCountryCodeMap.put("JP", 81);
        PhoneUtil.sISOCountryCodeMap.put("KR", 82);
        PhoneUtil.sISOCountryCodeMap.put("VN", 84);
        PhoneUtil.sISOCountryCodeMap.put("CN", 86);
        PhoneUtil.sISOCountryCodeMap.put("TR", 90);
        PhoneUtil.sISOCountryCodeMap.put("IN", 91);
        PhoneUtil.sISOCountryCodeMap.put("IN", 91);
        PhoneUtil.sISOCountryCodeMap.put("IN", 91);
        PhoneUtil.sISOCountryCodeMap.put("IN", 91);
        PhoneUtil.sISOCountryCodeMap.put("PK", 92);
        PhoneUtil.sISOCountryCodeMap.put("AF", 93);
        PhoneUtil.sISOCountryCodeMap.put("LK", 94);
        PhoneUtil.sISOCountryCodeMap.put("MM", 95);
        PhoneUtil.sISOCountryCodeMap.put("IR", 98);
        PhoneUtil.sISOCountryCodeMap.put("SS", 211);
        PhoneUtil.sISOCountryCodeMap.put("MA", 212);
        PhoneUtil.sISOCountryCodeMap.put("EH", 212);
        PhoneUtil.sISOCountryCodeMap.put("DZ", 213);
        PhoneUtil.sISOCountryCodeMap.put("TN", 216);
        PhoneUtil.sISOCountryCodeMap.put("LY", 218);
        PhoneUtil.sISOCountryCodeMap.put("GM", 220);
        PhoneUtil.sISOCountryCodeMap.put("SN", 221);
        PhoneUtil.sISOCountryCodeMap.put("MR", 222);
        PhoneUtil.sISOCountryCodeMap.put("ML", 223);
        PhoneUtil.sISOCountryCodeMap.put("GN", 224);
        PhoneUtil.sISOCountryCodeMap.put("CI", 225);
        PhoneUtil.sISOCountryCodeMap.put("BF", 226);
        PhoneUtil.sISOCountryCodeMap.put("NE", 227);
        PhoneUtil.sISOCountryCodeMap.put("TG", 228);
        PhoneUtil.sISOCountryCodeMap.put("BJ", 229);
        PhoneUtil.sISOCountryCodeMap.put("MU", 230);
        PhoneUtil.sISOCountryCodeMap.put("LR", 231);
        PhoneUtil.sISOCountryCodeMap.put("SL", 232);
        PhoneUtil.sISOCountryCodeMap.put("GH", 233);
        PhoneUtil.sISOCountryCodeMap.put("NG", 234);
        PhoneUtil.sISOCountryCodeMap.put("TD", 235);
        PhoneUtil.sISOCountryCodeMap.put("CF", 236);
        PhoneUtil.sISOCountryCodeMap.put("CM", 237);
        PhoneUtil.sISOCountryCodeMap.put("CV", 238);
        PhoneUtil.sISOCountryCodeMap.put("ST", 239);
        PhoneUtil.sISOCountryCodeMap.put("ST", 239);
        PhoneUtil.sISOCountryCodeMap.put("GQ", 240);
        PhoneUtil.sISOCountryCodeMap.put("GA", 241);
        PhoneUtil.sISOCountryCodeMap.put("CG", 242);
        PhoneUtil.sISOCountryCodeMap.put("CD", 243);
        PhoneUtil.sISOCountryCodeMap.put("AO", 244);
        PhoneUtil.sISOCountryCodeMap.put("GW", 245);
        PhoneUtil.sISOCountryCodeMap.put("IO", 246);
        PhoneUtil.sISOCountryCodeMap.put("AC", 247);
        PhoneUtil.sISOCountryCodeMap.put("SC", 248);
        PhoneUtil.sISOCountryCodeMap.put("SD", 249);
        PhoneUtil.sISOCountryCodeMap.put("RW", 250);
        PhoneUtil.sISOCountryCodeMap.put("ET", 251);
        PhoneUtil.sISOCountryCodeMap.put("SO", 252);
        PhoneUtil.sISOCountryCodeMap.put("DJ", 253);
        PhoneUtil.sISOCountryCodeMap.put("KE", 254);
        PhoneUtil.sISOCountryCodeMap.put("TZ", 255);
        PhoneUtil.sISOCountryCodeMap.put("UG", 256);
        PhoneUtil.sISOCountryCodeMap.put("BI", 257);
        PhoneUtil.sISOCountryCodeMap.put("MZ", 258);
        PhoneUtil.sISOCountryCodeMap.put("ZM", 260);
        PhoneUtil.sISOCountryCodeMap.put("MG", 261);
        PhoneUtil.sISOCountryCodeMap.put("KE", 254);
        PhoneUtil.sISOCountryCodeMap.put("YT", 262);
        PhoneUtil.sISOCountryCodeMap.put("KE", 262);
        PhoneUtil.sISOCountryCodeMap.put("ZW", 263);
        PhoneUtil.sISOCountryCodeMap.put("NA", 264);
        PhoneUtil.sISOCountryCodeMap.put("MW", 265);
        PhoneUtil.sISOCountryCodeMap.put("LS", 266);
        PhoneUtil.sISOCountryCodeMap.put("BW", 267);
        PhoneUtil.sISOCountryCodeMap.put("SZ", 268);
        PhoneUtil.sISOCountryCodeMap.put("KM", 269);
        PhoneUtil.sISOCountryCodeMap.put("SH", 290);
        PhoneUtil.sISOCountryCodeMap.put("TA", 290);
        PhoneUtil.sISOCountryCodeMap.put("ER", 291);
        PhoneUtil.sISOCountryCodeMap.put("AW", 297);
        PhoneUtil.sISOCountryCodeMap.put("FO", 298);
        PhoneUtil.sISOCountryCodeMap.put("GL", 299);
        PhoneUtil.sISOCountryCodeMap.put("GI", 350);
        PhoneUtil.sISOCountryCodeMap.put("PT", 351);
        PhoneUtil.sISOCountryCodeMap.put("LU", 352);
        PhoneUtil.sISOCountryCodeMap.put("IE", 353);
        PhoneUtil.sISOCountryCodeMap.put("IS", 354);
        PhoneUtil.sISOCountryCodeMap.put("AL", 355);
        PhoneUtil.sISOCountryCodeMap.put("MT", 356);
        PhoneUtil.sISOCountryCodeMap.put("CY", 357);
        PhoneUtil.sISOCountryCodeMap.put("FI", 358);
        PhoneUtil.sISOCountryCodeMap.put("AX", 358);
        PhoneUtil.sISOCountryCodeMap.put("BG", 359);
        PhoneUtil.sISOCountryCodeMap.put("LT", 370);
        PhoneUtil.sISOCountryCodeMap.put("LV", 371);
        PhoneUtil.sISOCountryCodeMap.put("EE", 372);
        PhoneUtil.sISOCountryCodeMap.put("MD", 373);
        PhoneUtil.sISOCountryCodeMap.put("AM", 374);
        PhoneUtil.sISOCountryCodeMap.put("BY", 375);
        PhoneUtil.sISOCountryCodeMap.put("AD", 376);
        PhoneUtil.sISOCountryCodeMap.put("MC", 377);
        PhoneUtil.sISOCountryCodeMap.put("SM", 378);
        PhoneUtil.sISOCountryCodeMap.put("UA", 380);
        PhoneUtil.sISOCountryCodeMap.put("RS", 381);
        PhoneUtil.sISOCountryCodeMap.put("ME", 382);
        PhoneUtil.sISOCountryCodeMap.put("HR", 385);
        PhoneUtil.sISOCountryCodeMap.put("SI", 386);
        PhoneUtil.sISOCountryCodeMap.put("BA", 387);
        PhoneUtil.sISOCountryCodeMap.put("MK", 389);
        PhoneUtil.sISOCountryCodeMap.put("CZ", 420);
        PhoneUtil.sISOCountryCodeMap.put("SK", 421);
        PhoneUtil.sISOCountryCodeMap.put("LI", 423);
        PhoneUtil.sISOCountryCodeMap.put("FK", 500);
        PhoneUtil.sISOCountryCodeMap.put("BZ", 501);
        PhoneUtil.sISOCountryCodeMap.put("GT", 502);
        PhoneUtil.sISOCountryCodeMap.put("SV", 503);
        PhoneUtil.sISOCountryCodeMap.put("HN", 504);
        PhoneUtil.sISOCountryCodeMap.put("NI", 505);
        PhoneUtil.sISOCountryCodeMap.put("CR", 506);
        PhoneUtil.sISOCountryCodeMap.put("PA", 507);
        PhoneUtil.sISOCountryCodeMap.put("PM", 508);
        PhoneUtil.sISOCountryCodeMap.put("HT", 509);
        PhoneUtil.sISOCountryCodeMap.put("GP", 590);
        PhoneUtil.sISOCountryCodeMap.put("BL", 590);
        PhoneUtil.sISOCountryCodeMap.put("MF", 590);
        PhoneUtil.sISOCountryCodeMap.put("BO", 591);
        PhoneUtil.sISOCountryCodeMap.put("GY", 592);
        PhoneUtil.sISOCountryCodeMap.put("EC", 593);
        PhoneUtil.sISOCountryCodeMap.put("GF", 594);
        PhoneUtil.sISOCountryCodeMap.put("PY", 595);
        PhoneUtil.sISOCountryCodeMap.put("MQ", 596);
        PhoneUtil.sISOCountryCodeMap.put("SR", 597);
        PhoneUtil.sISOCountryCodeMap.put("UY", 598);
        PhoneUtil.sISOCountryCodeMap.put("CW", 599);
        PhoneUtil.sISOCountryCodeMap.put("BQ", 599);
        PhoneUtil.sISOCountryCodeMap.put("TL", 670);
        PhoneUtil.sISOCountryCodeMap.put("NF", 672);
        PhoneUtil.sISOCountryCodeMap.put("BN", 673);
        PhoneUtil.sISOCountryCodeMap.put("NR", 674);
        PhoneUtil.sISOCountryCodeMap.put("PG", 675);
        PhoneUtil.sISOCountryCodeMap.put("TO", 676);
        PhoneUtil.sISOCountryCodeMap.put("SB", 677);
        PhoneUtil.sISOCountryCodeMap.put("VU", 678);
        PhoneUtil.sISOCountryCodeMap.put("FJ", 679);
        PhoneUtil.sISOCountryCodeMap.put("PW", 680);
        PhoneUtil.sISOCountryCodeMap.put("WF", 681);
        PhoneUtil.sISOCountryCodeMap.put("CK", 682);
        PhoneUtil.sISOCountryCodeMap.put("NU", 683);
        PhoneUtil.sISOCountryCodeMap.put("WS", 685);
        PhoneUtil.sISOCountryCodeMap.put("KI", 686);
        PhoneUtil.sISOCountryCodeMap.put("NC", 687);
        PhoneUtil.sISOCountryCodeMap.put("TV", 688);
        PhoneUtil.sISOCountryCodeMap.put("PF", 689);
        PhoneUtil.sISOCountryCodeMap.put("TK", 690);
        PhoneUtil.sISOCountryCodeMap.put("FM", 691);
        PhoneUtil.sISOCountryCodeMap.put("MH", 692);
        PhoneUtil.sISOCountryCodeMap.put("KP", 850);
        PhoneUtil.sISOCountryCodeMap.put("HK", 852);
        PhoneUtil.sISOCountryCodeMap.put("MO", 853);
        PhoneUtil.sISOCountryCodeMap.put("KH", 855);
        PhoneUtil.sISOCountryCodeMap.put("LA", 856);
        PhoneUtil.sISOCountryCodeMap.put("BD", 880);
        PhoneUtil.sISOCountryCodeMap.put("TW", 886);
        PhoneUtil.sISOCountryCodeMap.put("MV", 960);
        PhoneUtil.sISOCountryCodeMap.put("LB", 961);
        PhoneUtil.sISOCountryCodeMap.put("JO", 962);
        PhoneUtil.sISOCountryCodeMap.put("SY", 963);
        PhoneUtil.sISOCountryCodeMap.put("IQ", 964);
        PhoneUtil.sISOCountryCodeMap.put("KW", 965);
        PhoneUtil.sISOCountryCodeMap.put("SA", 966);
        PhoneUtil.sISOCountryCodeMap.put("YE", 967);
        PhoneUtil.sISOCountryCodeMap.put("OM", 968);
        PhoneUtil.sISOCountryCodeMap.put("PS", 970);
        PhoneUtil.sISOCountryCodeMap.put("AE", 971);
        PhoneUtil.sISOCountryCodeMap.put("IL", 972);
        PhoneUtil.sISOCountryCodeMap.put("BH", 973);
        PhoneUtil.sISOCountryCodeMap.put("QA", 974);
        PhoneUtil.sISOCountryCodeMap.put("BT", 975);
        PhoneUtil.sISOCountryCodeMap.put("MN", 976);
        PhoneUtil.sISOCountryCodeMap.put("NP", 977);
        PhoneUtil.sISOCountryCodeMap.put("TJ", 992);
        PhoneUtil.sISOCountryCodeMap.put("TM", 993);
        PhoneUtil.sISOCountryCodeMap.put("AZ", 994);
        PhoneUtil.sISOCountryCodeMap.put("GE", 995);
        PhoneUtil.sISOCountryCodeMap.put("KG", 996);
        PhoneUtil.sISOCountryCodeMap.put("UZ", 998);
    }
    
    public static String convertToE164Format(String format) {
        try {
            format = PhoneUtil.sPhoneNumberUtil.format(PhoneUtil.sPhoneNumberUtil.parse(format, DriverProfileHelper.getInstance().getCurrentLocale().getCountry()), PhoneNumberUtil.PhoneNumberFormat.E164);
            return format;
        }
        catch (Throwable t) {
            PhoneUtil.sLogger.e(t);
            return format;
        }
    }
    
    public static String formatPhoneNumber(final String s) {
        synchronized (PhoneUtil.class) {
            try {
                String s2;
                if (TextUtils.isEmpty((CharSequence)s)) {
                    s2 = "";
                }
                else {
                    final Locale currentLocale = DriverProfileHelper.getInstance().getCurrentLocale();
                    final int countryPhoneCode = getCountryPhoneCode(currentLocale.getCountry());
                    final Phonenumber.PhoneNumber parse = PhoneUtil.sPhoneNumberUtil.parse(s, currentLocale.getCountry());
                    final boolean b = true;
                    int n2;
                    final int n = n2 = 0;
                    boolean b2 = b;
                    if (countryPhoneCode > 0) {
                        n2 = n;
                        b2 = b;
                        if (parse.hasCountryCode()) {
                            final int countryCode = parse.getCountryCode();
                            if (countryCode != countryPhoneCode) {
                                if (!s.startsWith(String.valueOf(countryCode))) {
                                    s2 = s;
                                    if (!s.startsWith("+")) {
                                        return s2;
                                    }
                                }
                                b2 = false;
                                n2 = n;
                            }
                            else {
                                n2 = 1;
                                b2 = b;
                            }
                        }
                    }
                    boolean b3;
                    if (b3 = b2) {
                        b3 = b2;
                        if (n2 == 0) {
                            b3 = b2;
                            if (s.startsWith("+")) {
                                b3 = false;
                            }
                        }
                    }
                    if (b3) {
                        s2 = PhoneUtil.sPhoneNumberUtil.format(parse, PhoneNumberUtil.PhoneNumberFormat.NATIONAL);
                    }
                    else {
                        s2 = PhoneUtil.sPhoneNumberUtil.format(parse, PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL);
                    }
                }
                return s2;
            }
            catch (Throwable t) {
                String s2 = s;
                if (PhoneUtil.sLogger.isLoggable(2)) {
                    PhoneUtil.sLogger.e(t);
                    s2 = s;
                    return s2;
                }
                return s2;
            }
        }
    }
    
    private static int getCountryPhoneCode(final String s) {
        int intValue = -1;
        if (!TextUtils.isEmpty((CharSequence)s)) {
            final Integer n = PhoneUtil.sISOCountryCodeMap.get(s);
            if (n != null) {
                intValue = n;
            }
        }
        return intValue;
    }
    
    public static String normalizeNumber(String replaceAll) {
        if (!TextUtils.isEmpty((CharSequence)replaceAll)) {
            replaceAll = replaceAll.replaceAll("[^0-9]+", "");
        }
        return replaceAll;
    }
}
