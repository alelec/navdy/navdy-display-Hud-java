package com.navdy.hud.app.util;

import android.util.DisplayMetrics;
import android.app.Activity;
import com.navdy.service.library.log.Logger;

public class ScreenOrientation
{
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(ScreenOrientation.class);
    }
    
    public static int getCurrentOrientation(final Activity activity) {
        final int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        final DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        final int widthPixels = displayMetrics.widthPixels;
        final int heightPixels = displayMetrics.heightPixels;
        int n = 0;
        if (((rotation == 0 || rotation == 2) && heightPixels > widthPixels) || ((rotation == 1 || rotation == 3) && widthPixels > heightPixels)) {
            switch (rotation) {
                default:
                    ScreenOrientation.sLogger.e("Unknown screen orientation. Defaulting to portrait.");
                    n = 1;
                    break;
                case 0:
                    n = 1;
                    break;
                case 1:
                    n = 0;
                    break;
                case 2:
                    n = 9;
                    break;
                case 3:
                    n = 8;
                    break;
            }
        }
        else {
            switch (rotation) {
                default:
                    ScreenOrientation.sLogger.e("Unknown screen orientation. Defaulting to landscape.");
                    n = 0;
                    break;
                case 0:
                    n = 0;
                    break;
                case 1:
                    n = 1;
                    break;
                case 2:
                    n = 8;
                    break;
                case 3:
                    n = 9;
                    break;
            }
        }
        return n;
    }
    
    public static boolean isPortrait(final int n) {
        boolean b = true;
        if (n != 1) {
            b = (n == 9 && b);
        }
        return b;
    }
    
    public static boolean isPortrait(final Activity activity) {
        return isPortrait(getCurrentOrientation(activity));
    }
}
