package com.navdy.hud.app.util;

import com.navdy.service.library.events.notification.NotificationAction;

public class NotificationActionCache
{
    private String handler;
    private int lastId;
    
    public NotificationActionCache(final Class clazz) {
        this.lastId = 1;
        this.handler = clazz.getName();
    }
    
    public NotificationAction buildAction(final int n, final int n2, final int n3) {
        return this.buildAction(n, n2, n3, null);
    }
    
    public NotificationAction buildAction(final int n, final int n2, final int n3, final int n4) {
        return new NotificationAction(this.handler, this.lastId++, n, n2, null, null, n3, n4);
    }
    
    public NotificationAction buildAction(final int n, final int n2, final int n3, final String s) {
        return new NotificationAction(this.handler, this.lastId++, n, n2, n3, s, null, null);
    }
    
    public void markComplete(final NotificationAction notificationAction) {
    }
    
    public boolean validAction(final NotificationAction notificationAction) {
        return notificationAction.handler.equals(this.handler);
    }
}
