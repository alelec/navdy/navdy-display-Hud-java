package com.navdy.hud.app.view;

import android.content.res.TypedArray;
import com.navdy.hud.app.R;
import android.util.AttributeSet;
import android.content.Context;
import butterknife.Optional;
import butterknife.InjectView;
import android.widget.TextView;

public class GaugeView extends DashboardWidgetView
{
    @InjectView(R.id.txt_unit)
    @Optional
    TextView mTvUnit;
    @InjectView(R.id.txt_value)
    TextView mTvValue;
    
    public GaugeView(final Context context) {
        this(context, null);
    }
    
    public GaugeView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public GaugeView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.GaugeView, n, 0);
        if (obtainStyledAttributes != null) {
            this.setValueTextSize(obtainStyledAttributes.getDimensionPixelSize(0, 0));
            obtainStyledAttributes.recycle();
        }
    }
    
    @Override
    public void clear() {
        super.clear();
        this.mTvUnit.setText((CharSequence)null);
        this.mTvValue.setText((CharSequence)null);
    }
    
    public TextView getUnitTextView() {
        return this.mTvUnit;
    }
    
    public TextView getUnitView() {
        return this.mTvUnit;
    }
    
    public TextView getValueTextView() {
        return this.mTvValue;
    }
    
    public TextView getValueView() {
        return this.mTvValue;
    }
    
    protected void onMeasure(final int n, final int n2) {
        super.onMeasure(n, n2);
    }
    
    public void setUnitText(final CharSequence text) {
        if (this.mTvUnit != null) {
            this.mTvUnit.setText(text);
        }
    }
    
    public void setValueText(final CharSequence text) {
        if (this.mTvValue != null) {
            this.mTvValue.setText(text);
        }
    }
    
    public void setValueTextSize(final int n) {
        this.mTvValue.setTextSize(0, (float)n);
    }
}
