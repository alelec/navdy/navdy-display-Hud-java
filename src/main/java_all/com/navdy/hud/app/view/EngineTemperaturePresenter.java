package com.navdy.hud.app.view;

import com.squareup.wire.ProtoEnum;
import com.navdy.hud.app.profile.DriverProfile;
import com.navdy.service.library.events.preferences.DriverProfilePreferences;
import kotlin.NoWhenBranchMatchedException;
import com.navdy.hud.app.ExtensionsKt;
import android.view.View;
import kotlin.TypeCastException;
import android.os.Bundle;
import com.squareup.otto.Subscribe;
import com.navdy.hud.app.manager.SpeedManager;
import android.graphics.drawable.Drawable;
import android.content.res.Resources;
import mortar.Mortar;
import com.navdy.hud.app.HudApplication;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.DefaultConstructorMarker;
import android.widget.TextView;
import org.jetbrains.annotations.Nullable;
import com.navdy.hud.app.view.drawable.EngineTemperatureDrawable;
import com.navdy.hud.app.profile.DriverProfileManager;
import android.content.Context;
import org.jetbrains.annotations.NotNull;
import javax.inject.Inject;
import com.squareup.otto.Bus;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000v\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0006\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 R2\u00020\u0001:\u0001RB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\n\u0010C\u001a\u0004\u0018\u00010DH\u0016J\b\u0010E\u001a\u00020\u0018H\u0016J\b\u0010F\u001a\u00020\u0018H\u0016J\b\u0010G\u001a\u000206H\u0014J\u0010\u0010H\u001a\u00020I2\u0006\u0010J\u001a\u00020KH\u0007J\u001c\u0010L\u001a\u00020I2\b\u0010M\u001a\u0004\u0018\u00010N2\b\u0010O\u001a\u0004\u0018\u00010PH\u0016J\b\u0010Q\u001a\u00020IH\u0014R\u001a\u0010\u0005\u001a\u00020\u0006X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001a\u0010\u000b\u001a\u00020\u0006X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\b\"\u0004\b\r\u0010\nR\u001a\u0010\u000e\u001a\u00020\u0006X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\b\"\u0004\b\u0010\u0010\nR\u001e\u0010\u0011\u001a\u00020\u00128\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\u001a\u0010\u0017\u001a\u00020\u0018X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001cR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u001e\u0010\u001f\u001a\u00020 8\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b!\u0010\"\"\u0004\b#\u0010$R$\u0010'\u001a\u00020&2\u0006\u0010%\u001a\u00020&@FX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b(\u0010)\"\u0004\b*\u0010+R\u001c\u0010,\u001a\u0004\u0018\u00010-X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b.\u0010/\"\u0004\b0\u00101R\u001a\u00102\u001a\u00020\u0018X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b3\u0010\u001a\"\u0004\b4\u0010\u001cR\u000e\u00105\u001a\u000206X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u00107\u001a\u000206X\u0082\u000e¢\u0006\u0002\n\u0000R\u001a\u00108\u001a\u000209X\u0086.¢\u0006\u000e\n\u0000\u001a\u0004\b:\u0010;\"\u0004\b<\u0010=R\u001a\u0010>\u001a\u000209X\u0086.¢\u0006\u000e\n\u0000\u001a\u0004\b?\u0010;\"\u0004\b@\u0010=R\u0011\u0010A\u001a\u00020\u0018¢\u0006\b\n\u0000\u001a\u0004\bB\u0010\u001a¨\u0006S" }, d2 = { "Lcom/navdy/hud/app/view/EngineTemperaturePresenter;", "Lcom/navdy/hud/app/view/DashboardWidgetPresenter;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "STATE_COLD", "", "getSTATE_COLD", "()I", "setSTATE_COLD", "(I)V", "STATE_HOT", "getSTATE_HOT", "setSTATE_HOT", "STATE_NORMAL", "getSTATE_NORMAL", "setSTATE_NORMAL", "bus", "Lcom/squareup/otto/Bus;", "getBus", "()Lcom/squareup/otto/Bus;", "setBus", "(Lcom/squareup/otto/Bus;)V", "celsiusLabel", "", "getCelsiusLabel", "()Ljava/lang/String;", "setCelsiusLabel", "(Ljava/lang/String;)V", "getContext", "()Landroid/content/Context;", "driverProfileManager", "Lcom/navdy/hud/app/profile/DriverProfileManager;", "getDriverProfileManager", "()Lcom/navdy/hud/app/profile/DriverProfileManager;", "setDriverProfileManager", "(Lcom/navdy/hud/app/profile/DriverProfileManager;)V", "value", "", "engineTemperature", "getEngineTemperature", "()D", "setEngineTemperature", "(D)V", "engineTemperatureDrawable", "Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;", "getEngineTemperatureDrawable", "()Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;", "setEngineTemperatureDrawable", "(Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;)V", "fahrenheitLabel", "getFahrenheitLabel", "setFahrenheitLabel", "mLeftOriented", "", "registered", "tempText", "Landroid/widget/TextView;", "getTempText", "()Landroid/widget/TextView;", "setTempText", "(Landroid/widget/TextView;)V", "tempUnitText", "getTempUnitText", "setTempUnitText", "widgetNameString", "getWidgetNameString", "getDrawable", "Landroid/graphics/drawable/Drawable;", "getWidgetIdentifier", "getWidgetName", "isRegisteringToBusRequired", "onSpeedUnitChanged", "", "speedUnitChangedEvent", "Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnitChanged;", "setView", "dashboardWidgetView", "Lcom/navdy/hud/app/view/DashboardWidgetView;", "arguments", "Landroid/os/Bundle;", "updateGauge", "Companion", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
public final class EngineTemperaturePresenter extends DashboardWidgetPresenter
{
    public static final Companion Companion;
    private static final double TEMPERATURE_GAUGE_COLD_THRESHOLD = 45.7;
    private static final double TEMPERATURE_GAUGE_HOT_THRESHOLD = 105.0;
    private static final double TEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS = 38.0;
    private static final double TEMPERATURE_GAUGE_MID_POINT_CELSIUS = 76.5;
    private static final double TEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS = 115.0;
    private int STATE_COLD;
    private int STATE_HOT;
    private int STATE_NORMAL;
    @Inject
    @NotNull
    public Bus bus;
    @NotNull
    private String celsiusLabel;
    @NotNull
    private final Context context;
    @Inject
    @NotNull
    public DriverProfileManager driverProfileManager;
    private double engineTemperature;
    @Nullable
    private EngineTemperatureDrawable engineTemperatureDrawable;
    @NotNull
    private String fahrenheitLabel;
    private boolean mLeftOriented;
    private boolean registered;
    @NotNull
    public TextView tempText;
    @NotNull
    public TextView tempUnitText;
    @NotNull
    private final String widgetNameString;
    
    static {
        Companion = new Companion();
    }
    
    public EngineTemperaturePresenter(@NotNull final Context context) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        this.context = context;
        this.STATE_COLD = 1;
        this.STATE_HOT = 2;
        this.mLeftOriented = true;
        this.fahrenheitLabel = "";
        this.celsiusLabel = "";
        this.engineTemperatureDrawable = new EngineTemperatureDrawable(this.context, R.array.smart_dash_engine_temperature_state_colors);
        final EngineTemperatureDrawable engineTemperatureDrawable = this.engineTemperatureDrawable;
        if (engineTemperatureDrawable != null) {
            engineTemperatureDrawable.setMinValue((float)EngineTemperaturePresenter.Companion.getTEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS());
        }
        final EngineTemperatureDrawable engineTemperatureDrawable2 = this.engineTemperatureDrawable;
        if (engineTemperatureDrawable2 != null) {
            engineTemperatureDrawable2.setMaxGaugeValue((float)EngineTemperaturePresenter.Companion.getTEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS());
        }
        final Resources resources = this.context.getResources();
        final String string = resources.getString(R.string.temperature_unit_fahrenheit);
        Intrinsics.checkExpressionValueIsNotNull(string, "resources.getString(R.st…perature_unit_fahrenheit)");
        this.fahrenheitLabel = string;
        final String string2 = resources.getString(R.string.temperature_unit_celsius);
        Intrinsics.checkExpressionValueIsNotNull(string2, "resources.getString(R.st…temperature_unit_celsius)");
        this.celsiusLabel = string2;
        Mortar.inject(HudApplication.getAppContext(), this);
        final String string3 = resources.getString(R.string.widget_engine_temperature);
        Intrinsics.checkExpressionValueIsNotNull(string3, "resources.getString(R.st…idget_engine_temperature)");
        this.widgetNameString = string3;
    }
    
    public static final /* synthetic */ double access$getTEMPERATURE_GAUGE_COLD_THRESHOLD$cp() {
        return EngineTemperaturePresenter.TEMPERATURE_GAUGE_COLD_THRESHOLD;
    }
    
    public static final /* synthetic */ double access$getTEMPERATURE_GAUGE_HOT_THRESHOLD$cp() {
        return EngineTemperaturePresenter.TEMPERATURE_GAUGE_HOT_THRESHOLD;
    }
    
    public static final /* synthetic */ double access$getTEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS$cp() {
        return EngineTemperaturePresenter.TEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS;
    }
    
    public static final /* synthetic */ double access$getTEMPERATURE_GAUGE_MID_POINT_CELSIUS$cp() {
        return EngineTemperaturePresenter.TEMPERATURE_GAUGE_MID_POINT_CELSIUS;
    }
    
    public static final /* synthetic */ double access$getTEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS$cp() {
        return EngineTemperaturePresenter.TEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS;
    }
    
    @NotNull
    public final Bus getBus() {
        final Bus bus = this.bus;
        if (bus == null) {
            Intrinsics.throwUninitializedPropertyAccessException("bus");
        }
        return bus;
    }
    
    @NotNull
    public final String getCelsiusLabel() {
        return this.celsiusLabel;
    }
    
    @NotNull
    public final Context getContext() {
        return this.context;
    }
    
    @Nullable
    @Override
    public Drawable getDrawable() {
        return this.engineTemperatureDrawable;
    }
    
    @NotNull
    public final DriverProfileManager getDriverProfileManager() {
        final DriverProfileManager driverProfileManager = this.driverProfileManager;
        if (driverProfileManager == null) {
            Intrinsics.throwUninitializedPropertyAccessException("driverProfileManager");
        }
        return driverProfileManager;
    }
    
    public final double getEngineTemperature() {
        return this.engineTemperature;
    }
    
    @Nullable
    public final EngineTemperatureDrawable getEngineTemperatureDrawable() {
        return this.engineTemperatureDrawable;
    }
    
    @NotNull
    public final String getFahrenheitLabel() {
        return this.fahrenheitLabel;
    }
    
    public final int getSTATE_COLD() {
        return this.STATE_COLD;
    }
    
    public final int getSTATE_HOT() {
        return this.STATE_HOT;
    }
    
    public final int getSTATE_NORMAL() {
        return this.STATE_NORMAL;
    }
    
    @NotNull
    public final TextView getTempText() {
        final TextView tempText = this.tempText;
        if (tempText == null) {
            Intrinsics.throwUninitializedPropertyAccessException("tempText");
        }
        return tempText;
    }
    
    @NotNull
    public final TextView getTempUnitText() {
        final TextView tempUnitText = this.tempUnitText;
        if (tempUnitText == null) {
            Intrinsics.throwUninitializedPropertyAccessException("tempUnitText");
        }
        return tempUnitText;
    }
    
    @NotNull
    @Override
    public String getWidgetIdentifier() {
        return "ENGINE_TEMPERATURE_GAUGE_ID";
    }
    
    @NotNull
    @Override
    public String getWidgetName() {
        return this.widgetNameString;
    }
    
    @NotNull
    public final String getWidgetNameString() {
        return this.widgetNameString;
    }
    
    @Override
    protected boolean isRegisteringToBusRequired() {
        return true;
    }
    
    @Subscribe
    public final void onSpeedUnitChanged(@NotNull final SpeedManager.SpeedUnitChanged speedUnitChanged) {
        Intrinsics.checkParameterIsNotNull(speedUnitChanged, "speedUnitChangedEvent");
        this.reDraw();
    }
    
    public final void setBus(@NotNull final Bus bus) {
        Intrinsics.checkParameterIsNotNull(bus, "set");
        this.bus = bus;
    }
    
    public final void setCelsiusLabel(@NotNull final String celsiusLabel) {
        Intrinsics.checkParameterIsNotNull(celsiusLabel, "set");
        this.celsiusLabel = celsiusLabel;
    }
    
    public final void setDriverProfileManager(@NotNull final DriverProfileManager driverProfileManager) {
        Intrinsics.checkParameterIsNotNull(driverProfileManager, "set");
        this.driverProfileManager = driverProfileManager;
    }
    
    public final void setEngineTemperature(final double engineTemperature) {
        this.engineTemperature = engineTemperature;
        this.reDraw();
    }
    
    public final void setEngineTemperatureDrawable(@Nullable final EngineTemperatureDrawable engineTemperatureDrawable) {
        this.engineTemperatureDrawable = engineTemperatureDrawable;
    }
    
    public final void setFahrenheitLabel(@NotNull final String fahrenheitLabel) {
        Intrinsics.checkParameterIsNotNull(fahrenheitLabel, "set");
        this.fahrenheitLabel = fahrenheitLabel;
    }
    
    public final void setSTATE_COLD(final int state_COLD) {
        this.STATE_COLD = state_COLD;
    }
    
    public final void setSTATE_HOT(final int state_HOT) {
        this.STATE_HOT = state_HOT;
    }
    
    public final void setSTATE_NORMAL(final int state_NORMAL) {
        this.STATE_NORMAL = state_NORMAL;
    }
    
    public final void setTempText(@NotNull final TextView tempText) {
        Intrinsics.checkParameterIsNotNull(tempText, "set");
        this.tempText = tempText;
    }
    
    public final void setTempUnitText(@NotNull final TextView tempUnitText) {
        Intrinsics.checkParameterIsNotNull(tempUnitText, "set");
        this.tempUnitText = tempUnitText;
    }
    
    @Override
    public void setView(@Nullable final DashboardWidgetView dashboardWidgetView, @Nullable final Bundle bundle) {
        int contentView;
        final int n = contentView = R.layout.engine_temp_gauge_left;
        if (bundle != null) {
            contentView = n;
            switch (bundle.getInt("EXTRA_GRAVITY")) {
                default:
                    contentView = n;
                    break;
                case 2:
                    contentView = R.layout.engine_temp_gauge_right;
                    this.mLeftOriented = false;
                    break;
                case 0:
                    contentView = R.layout.engine_temp_gauge_left;
                    this.mLeftOriented = true;
                case 1:
                    break;
            }
        }
        if (dashboardWidgetView != null) {
            dashboardWidgetView.setContentView(contentView);
            final View viewById = dashboardWidgetView.findViewById(R.id.txt_value);
            if (viewById == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.widget.TextView");
            }
            this.tempText = (TextView)viewById;
            final View viewById2 = dashboardWidgetView.findViewById(R.id.txt_unit);
            if (viewById2 == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.widget.TextView");
            }
            this.tempUnitText = (TextView)viewById2;
        }
        super.setView(dashboardWidgetView, bundle);
        this.reDraw();
    }
    
    @Override
    protected void updateGauge() {
        if (this.mWidgetView != null) {
            final DriverProfileManager driverProfileManager = this.driverProfileManager;
            if (driverProfileManager == null) {
                Intrinsics.throwUninitializedPropertyAccessException("driverProfileManager");
            }
            while (true) {
                Label_0192: {
                    if (driverProfileManager == null) {
                        break Label_0192;
                    }
                    final DriverProfile currentProfile = driverProfileManager.getCurrentProfile();
                    if (currentProfile == null) {
                        break Label_0192;
                    }
                    final ProtoEnum protoEnum = currentProfile.getUnitSystem();
                    if (protoEnum == null) {
                        break Label_0192;
                    }
                    final EngineTemperatureDrawable engineTemperatureDrawable = this.engineTemperatureDrawable;
                    if (engineTemperatureDrawable != null) {
                        engineTemperatureDrawable.setGaugeValue((float)ExtensionsKt.clamp(this.engineTemperature, EngineTemperaturePresenter.Companion.getTEMPERATURE_GAUGE_COLD_THRESHOLD(), EngineTemperaturePresenter.Companion.getTEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS()));
                    }
                    final EngineTemperatureDrawable engineTemperatureDrawable2 = this.engineTemperatureDrawable;
                    if (engineTemperatureDrawable2 != null) {
                        engineTemperatureDrawable2.setLeftOriented(this.mLeftOriented);
                    }
                    int state = this.STATE_NORMAL;
                    if (this.engineTemperature <= EngineTemperaturePresenter.Companion.getTEMPERATURE_GAUGE_COLD_THRESHOLD()) {
                        state = this.STATE_COLD;
                    }
                    else if (this.engineTemperature > EngineTemperaturePresenter.Companion.getTEMPERATURE_GAUGE_HOT_THRESHOLD()) {
                        state = this.STATE_HOT;
                    }
                    final EngineTemperatureDrawable engineTemperatureDrawable3 = this.engineTemperatureDrawable;
                    if (engineTemperatureDrawable3 != null) {
                        engineTemperatureDrawable3.setState(state);
                    }
                    final TextView tempText = this.tempText;
                    if (tempText == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("tempText");
                    }
                    if (tempText != null) {
                        String text = null;
                        switch (EngineTemperaturePresenter$WhenMappings.$EnumSwitchMapping$0[((Enum)protoEnum).ordinal()]) {
                            default:
                                throw new NoWhenBranchMatchedException();
                            case 1:
                                text = String.valueOf((int)this.engineTemperature);
                                break;
                            case 2:
                                text = String.valueOf((int)ExtensionsKt.celsiusToFahrenheit(this.engineTemperature));
                                break;
                        }
                        tempText.setText((CharSequence)text);
                    }
                    final TextView tempUnitText = this.tempUnitText;
                    if (tempUnitText == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("tempUnitText");
                    }
                    if (tempUnitText != null) {
                        String text2 = null;
                        switch (EngineTemperaturePresenter$WhenMappings.$EnumSwitchMapping$1[((Enum)protoEnum).ordinal()]) {
                            default:
                                throw new NoWhenBranchMatchedException();
                            case 1:
                                text2 = this.celsiusLabel;
                                break;
                            case 2:
                                text2 = this.fahrenheitLabel;
                                break;
                        }
                        tempUnitText.setText((CharSequence)text2);
                        return;
                    }
                    return;
                }
                final ProtoEnum protoEnum = DriverProfilePreferences.UnitSystem.UNIT_SYSTEM_METRIC;
                continue;
            }
        }
    }
    
    @Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0006\n\u0002\b\u000b\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0086D¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0004X\u0086D¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0006R\u0014\u0010\t\u001a\u00020\u0004X\u0086D¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u0006R\u0014\u0010\u000b\u001a\u00020\u0004X\u0086D¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u0006R\u0014\u0010\r\u001a\u00020\u0004X\u0086D¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u0006¨\u0006\u000f" }, d2 = { "Lcom/navdy/hud/app/view/EngineTemperaturePresenter$Companion;", "", "()V", "TEMPERATURE_GAUGE_COLD_THRESHOLD", "", "getTEMPERATURE_GAUGE_COLD_THRESHOLD", "()D", "TEMPERATURE_GAUGE_HOT_THRESHOLD", "getTEMPERATURE_GAUGE_HOT_THRESHOLD", "TEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS", "getTEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS", "TEMPERATURE_GAUGE_MID_POINT_CELSIUS", "getTEMPERATURE_GAUGE_MID_POINT_CELSIUS", "TEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS", "getTEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
    public static final class Companion
    {
        public final double getTEMPERATURE_GAUGE_COLD_THRESHOLD() {
            return EngineTemperaturePresenter.access$getTEMPERATURE_GAUGE_COLD_THRESHOLD$cp();
        }
        
        public final double getTEMPERATURE_GAUGE_HOT_THRESHOLD() {
            return EngineTemperaturePresenter.access$getTEMPERATURE_GAUGE_HOT_THRESHOLD$cp();
        }
        
        public final double getTEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS() {
            return EngineTemperaturePresenter.access$getTEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS$cp();
        }
        
        public final double getTEMPERATURE_GAUGE_MID_POINT_CELSIUS() {
            return EngineTemperaturePresenter.access$getTEMPERATURE_GAUGE_MID_POINT_CELSIUS$cp();
        }
        
        public final double getTEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS() {
            return EngineTemperaturePresenter.access$getTEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS$cp();
        }
    }
}
