package com.navdy.hud.app.view;

import android.animation.Animator;

public interface ICustomAnimator
{
    Animator getCustomAnimator(final MainView.CustomAnimationMode p0);
}
