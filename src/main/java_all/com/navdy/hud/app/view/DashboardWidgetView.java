package com.navdy.hud.app.view;

import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.graphics.drawable.Drawable;
import android.content.res.TypedArray;
import butterknife.ButterKnife;
import com.navdy.hud.app.R;
import android.util.AttributeSet;
import android.content.Context;
import butterknife.Optional;
import butterknife.InjectView;
import android.view.View;
import android.widget.RelativeLayout;

public class DashboardWidgetView extends RelativeLayout
{
    @InjectView(R.id.custom_drawable)
    @Optional
    protected View mCustomView;
    private int mLayout;
    
    public DashboardWidgetView(final Context context) {
        this(context, null);
    }
    
    public DashboardWidgetView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public DashboardWidgetView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.GaugeView, n, 0);
        if (obtainStyledAttributes != null) {
            this.setContentView(obtainStyledAttributes.getResourceId(1, R.layout.small_gauge_view));
            ButterKnife.inject((View)this);
            obtainStyledAttributes.recycle();
        }
    }
    
    public void clear() {
        if (this.mCustomView != null) {
            this.mCustomView.setBackground((Drawable)null);
        }
    }
    
    public View getCustomView() {
        return this.mCustomView;
    }
    
    public boolean setContentView(final int mLayout) {
        boolean b;
        if (mLayout > 0 && mLayout != this.mLayout) {
            this.mLayout = mLayout;
            this.removeAllViews();
            LayoutInflater.from(this.getContext()).inflate(mLayout, (ViewGroup)this);
            ButterKnife.inject((View)this);
            b = true;
        }
        else {
            b = false;
        }
        return b;
    }
    
    public boolean setContentView(final View view) {
        this.mLayout = -1;
        this.removeAllViews();
        boolean b;
        if (view != null) {
            this.addView(view);
            b = true;
        }
        else {
            b = false;
        }
        return b;
    }
}
