package com.navdy.hud.app.view;

import flow.Flow;
import android.view.ViewGroup;
import android.animation.Animator;
import android.view.View;
import mortar.Mortar;
import android.util.AttributeSet;
import android.content.Context;
import javax.inject.Inject;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.util.ScreenConductor;
import com.navdy.hud.app.util.Pauseable;
import mortar.Blueprint;
import com.navdy.hud.app.util.CanShowScreen;
import android.widget.FrameLayout;

public class ContainerView extends FrameLayout implements CanShowScreen<Blueprint>, Pauseable
{
    private ScreenConductor<Blueprint> screenMaestro;
    @Inject
    UIStateManager uiStateManager;
    
    public ContainerView(final Context context, final AttributeSet set) {
        super(context, set);
        if (!this.isInEditMode()) {
            Mortar.inject(context, this);
        }
    }
    
    public void createScreens() {
        this.screenMaestro.createScreens();
    }
    
    public View getCurrentView() {
        return this.screenMaestro.getChildView();
    }
    
    public Animator getCustomContainerAnimator(final MainView.CustomAnimationMode customAnimationMode) {
        final View currentView = this.getCurrentView();
        Animator customAnimator;
        if (currentView != null && currentView instanceof ICustomAnimator) {
            customAnimator = ((ICustomAnimator)currentView).getCustomAnimator(customAnimationMode);
        }
        else {
            customAnimator = null;
        }
        return customAnimator;
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        this.screenMaestro = new ScreenConductor<Blueprint>(this.getContext(), (ViewGroup)this.findViewById(R.id.screenContainer), this.uiStateManager);
    }
    
    public void onPause() {
        final View currentView = this.getCurrentView();
        if (currentView instanceof Pauseable) {
            ((Pauseable)currentView).onPause();
        }
    }
    
    public void onResume() {
        final View currentView = this.getCurrentView();
        if (currentView instanceof Pauseable) {
            ((Pauseable)currentView).onResume();
        }
    }
    
    public void showScreen(final Blueprint blueprint, final Flow.Direction direction, final int n, final int n2) {
        this.screenMaestro.showScreen(blueprint, direction, n, n2);
    }
}
