package com.navdy.hud.app.view;

import android.graphics.Paint;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Canvas;
import android.content.res.Resources;
import com.navdy.hud.app.R;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.Paint;

public class BrightnessControlGauge extends Gauge
{
    private static final int PIVOT_ANGLE = 5;
    private static final int THUMB_ANGLE = 10;
    private int mPivotIndicatorColor;
    private Paint mPivotIndicatorPaint;
    private int mPivotValue;
    private int mThumbColor;
    private Paint mThumbPaint;
    
    public BrightnessControlGauge(final Context context) {
        this(context, null);
    }
    
    public BrightnessControlGauge(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public BrightnessControlGauge(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        final Resources resources = this.getResources();
        this.mPivotValue = context.getTheme().obtainStyledAttributes(set, R.styleable.BrightnessControlGauge, 0, 0).getInteger(0, 0);
        this.mThumbColor = resources.getColor(R.color.thumb_color);
        this.mPivotIndicatorColor = resources.getColor(R.color.pivot_indicator_color);
    }
    
    @Override
    protected void drawGauge(final Canvas canvas) {
        final float n = this.getWidth();
        final float n2 = this.getHeight();
        final float radius = this.getRadius();
        final float deltaToAngle = this.deltaToAngle(this.mPivotValue - this.mMinValue);
        final float n3 = 5.0f / 2.0f;
        final float n4 = n / 2.0f;
        final float n5 = n2 / 2.0f;
        final float n6 = n / 2.0f;
        final float n7 = n2 / 2.0f;
        final RectF rectF = new RectF();
        rectF.set(n4 - radius, n5 - radius, n6 + radius, n7 + radius);
        canvas.drawArc(rectF, this.mStartAngle + (deltaToAngle - n3), 5.0f, false, this.mPivotIndicatorPaint);
        if (this.mValue > this.mPivotValue) {
            this.drawIndicator(canvas, this.mPivotValue, this.mValue);
        }
        else if (this.mValue < this.mPivotValue) {
            this.drawIndicator(canvas, this.mValue, this.mPivotValue);
        }
        canvas.drawArc(rectF, this.mStartAngle + (this.deltaToAngle(this.mValue - this.mMinValue) - 10.0f / 2.0f), 10.0f, false, this.mThumbPaint);
    }
    
    @Override
    protected void initDrawingTools() {
        super.initDrawingTools();
        (this.mPivotIndicatorPaint = new Paint()).setStrokeWidth((float)this.mBackgroundThickness);
        this.mPivotIndicatorPaint.setAntiAlias(true);
        this.mPivotIndicatorPaint.setStrokeCap(Paint$Cap.BUTT);
        this.mPivotIndicatorPaint.setStyle(Paint$Style.STROKE);
        (this.mThumbPaint = new Paint()).setColor(this.mThumbColor);
        this.mThumbPaint.setStrokeWidth((float)this.mThickness);
        this.mThumbPaint.setAntiAlias(true);
        this.mThumbPaint.setStrokeCap(Paint$Cap.BUTT);
        this.mThumbPaint.setStyle(Paint$Style.STROKE);
    }
}
