package com.navdy.hud.app.view;

import android.os.Bundle;
import com.navdy.hud.app.HudApplication;
import android.graphics.drawable.Drawable;
import android.view.View;
import com.navdy.service.library.log.Logger;

public abstract class DashboardWidgetPresenter
{
    public static final String EXTRA_GRAVITY = "EXTRA_GRAVITY";
    public static final String EXTRA_IS_ACTIVE = "EXTRA_IS_ACTIVE";
    public static final int GRAVITY_CENTER = 1;
    public static final int GRAVITY_LEFT = 0;
    public static final int GRAVITY_RIGHT = 2;
    protected boolean isDashActive;
    protected boolean isRegistered;
    protected boolean isWidgetVisibleToUser;
    protected Logger logger;
    protected View mCustomView;
    protected DashboardWidgetView mWidgetView;
    
    public DashboardWidgetPresenter() {
        this.logger = new Logger(this.getClass());
        this.isRegistered = false;
    }
    
    public boolean canDraw() {
        return this.isDashActive && this.isWidgetVisibleToUser && this.mWidgetView != null;
    }
    
    public abstract Drawable getDrawable();
    
    public abstract String getWidgetIdentifier();
    
    public abstract String getWidgetName();
    
    public DashboardWidgetView getWidgetView() {
        return this.mWidgetView;
    }
    
    protected boolean isRegisteringToBusRequired() {
        return false;
    }
    
    public boolean isWidgetActive() {
        return this.mWidgetView != null;
    }
    
    public void onPause() {
        this.isDashActive = false;
        if (this.canDraw()) {
            this.logger.v("widget::onPause:");
        }
    }
    
    public void onResume() {
        this.isDashActive = true;
        if (this.canDraw()) {
            this.logger.v("widget::onResume:");
            this.reDraw();
        }
    }
    
    public void reDraw() {
        if (this.canDraw()) {
            this.updateGauge();
            if (this.mCustomView != null) {
                this.mCustomView.invalidate();
            }
        }
    }
    
    protected void registerToBus() {
        HudApplication.getApplication().getBus().register(this);
    }
    
    public void setView(final DashboardWidgetView mWidgetView) {
        this.mWidgetView = mWidgetView;
        while (true) {
            Label_0080: {
                if (this.mWidgetView == null) {
                    break Label_0080;
                }
                this.mCustomView = mWidgetView.getCustomView();
                if (this.mCustomView != null) {
                    this.mCustomView.setBackground(this.getDrawable());
                }
                if (!this.isRegisteringToBusRequired() || this.isRegistered) {
                    break Label_0061;
                }
                try {
                    this.registerToBus();
                    this.isRegistered = true;
                    this.reDraw();
                    return;
                }
                catch (RuntimeException ex) {
                    this.logger.e("Runtime exception registering to the bus ", ex);
                    continue;
                }
            }
            if (this.isRegistered) {
                try {
                    this.unregisterToBus();
                    this.isRegistered = false;
                    continue;
                }
                catch (RuntimeException ex2) {
                    this.logger.e("Runtime exception unregistering from the bus ", ex2);
                    continue;
                }
                continue;
            }
            continue;
        }
    }
    
    public void setView(final DashboardWidgetView view, final Bundle bundle) {
        this.setView(view);
    }
    
    public void setWidgetVisibleToUser(final boolean isWidgetVisibleToUser) {
        this.isWidgetVisibleToUser = isWidgetVisibleToUser;
        if (this.canDraw()) {
            this.logger.v("widget::visible:reDraw");
            this.reDraw();
        }
    }
    
    protected void unregisterToBus() {
        HudApplication.getApplication().getBus().unregister(this);
    }
    
    protected abstract void updateGauge();
}
