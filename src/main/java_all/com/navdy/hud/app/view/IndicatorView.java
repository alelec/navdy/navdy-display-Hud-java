package com.navdy.hud.app.view;

import android.graphics.Canvas;
import android.content.res.TypedArray;
import com.navdy.hud.app.R;
import android.graphics.Shader;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.content.Context;
import com.navdy.hud.app.util.CustomDimension;
import android.graphics.Path;
import android.graphics.Paint;
import android.view.View;

public class IndicatorView extends View
{
    private Paint indicatorPaint;
    private Path indicatorPath;
    private float mCurveRadius;
    private int mIndicatorHeight;
    private CustomDimension mIndicatorHeightAttribute;
    private float mIndicatorStrokeWidth;
    private int mIndicatorWidth;
    private CustomDimension mIndicatorWidthAttribute;
    private float mValue;
    
    public IndicatorView(final Context context) {
        this(context, null);
    }
    
    public IndicatorView(final Context context, final AttributeSet set) {
        super(context, set);
        this.initFromAttributes(context, set);
    }
    
    public static void drawIndicatorPath(final Path path, final float n, final float n2, final float n3, final float n4, final float n5, final float n6) {
        drawIndicatorPath(path, 0.5f, n, n2, n3, n4, n5, n6);
    }
    
    public static void drawIndicatorPath(final Path path, float n, final float n2, final float n3, final float n4, final float n5, final float n6, final float n7) {
        path.moveTo(n2, n3);
        final double atan = Math.atan(n6 / (n5 / 2.0f));
        final float n8 = (float)(Math.sin(atan) * n4);
        final float n9 = (float)(Math.cos(atan) * n4);
        final float n10 = n5 / 2.0f;
        n = n2 + n10 + (n7 - n5) * n;
        path.lineTo(n - n10 - n4, n3);
        path.quadTo(n - n10, n3, n - n10 + n9, n3 - n8);
        path.lineTo(n, n3 - n6);
        path.lineTo(n + n10 - n9, n3 - n8);
        path.quadTo(n + n10, n3, n + n10 + n4, n3);
        path.lineTo(n2 + n7, n3);
    }
    
    private void initDrawingTools() {
        this.indicatorPath = new Path();
        (this.indicatorPaint = new Paint()).setColor(-1);
        this.indicatorPaint.setAntiAlias(true);
        this.indicatorPaint.setStyle(Paint$Style.STROKE);
        this.indicatorPaint.setStrokeWidth(this.mIndicatorStrokeWidth);
        final int paddingLeft = this.getPaddingLeft();
        this.indicatorPaint.setShader((Shader)new LinearGradient((float)paddingLeft, 0.0f, (float)(this.getWidth() - this.getPaddingRight()), 0.0f, new int[] { 0, -1, -1, 0 }, new float[] { 0.0f, 0.45f, 0.55f, 1.0f }, Shader$TileMode.CLAMP));
        drawIndicatorPath(this.indicatorPath, this.mValue, paddingLeft, this.getHeight() - this.mIndicatorStrokeWidth, this.mCurveRadius, this.mIndicatorWidth, this.mIndicatorHeight, this.getWidth() - this.getPaddingRight() - this.getPaddingLeft());
    }
    
    private void initFromAttributes(Context obtainStyledAttributes, final AttributeSet set) {
        obtainStyledAttributes = (Context)obtainStyledAttributes.getTheme().obtainStyledAttributes(set, R.styleable.Indicator, 0, 0);
        try {
            this.mIndicatorWidthAttribute = CustomDimension.getDimension(this, (TypedArray)obtainStyledAttributes, 0, 0.0f);
            this.mIndicatorHeightAttribute = CustomDimension.getDimension(this, (TypedArray)obtainStyledAttributes, 1, 0.0f);
            this.mCurveRadius = ((TypedArray)obtainStyledAttributes).getDimension(2, 10.0f);
            this.mIndicatorStrokeWidth = ((TypedArray)obtainStyledAttributes).getDimension(3, 4.0f);
            this.mValue = ((TypedArray)obtainStyledAttributes).getFloat(4, 0.5f);
        }
        finally {
            ((TypedArray)obtainStyledAttributes).recycle();
        }
    }
    
    public void evaluateDimensions(final int n, final int n2) {
        this.mIndicatorHeight = (int)this.mIndicatorHeightAttribute.getSize(this, n, 0.0f);
        this.mIndicatorWidth = (int)this.mIndicatorWidthAttribute.getSize(this, n, 0.0f);
    }
    
    public float getCurveRadius() {
        return this.mCurveRadius;
    }
    
    public int getIndicatorHeight() {
        return this.mIndicatorHeight;
    }
    
    public int getIndicatorWidth() {
        return this.mIndicatorWidth;
    }
    
    public CustomDimension getIndicatorWidthAttribute() {
        return this.mIndicatorWidthAttribute;
    }
    
    public float getStrokeWidth() {
        return this.mIndicatorStrokeWidth;
    }
    
    public float getValue() {
        return this.mValue;
    }
    
    protected void onDraw(final Canvas canvas) {
        canvas.drawPath(this.indicatorPath, this.indicatorPaint);
    }
    
    protected void onSizeChanged(final int n, final int n2, final int n3, final int n4) {
        this.evaluateDimensions(n, n2);
        this.initDrawingTools();
    }
    
    public void setValue(final float mValue) {
        if (mValue != this.mValue) {
            this.mValue = mValue;
            this.initDrawingTools();
            this.invalidate();
        }
    }
}
