package com.navdy.hud.app.view;

import android.view.View;
import butterknife.ButterKnife;
import android.os.Bundle;
import android.graphics.drawable.Drawable;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.animation.ValueAnimator;
import android.os.Handler;
import butterknife.InjectView;
import android.widget.TextView;

public class EmptyGaugePresenter extends DashboardWidgetPresenter
{
    public static final int ALPHA_ANIMATION_DURATION = 2000;
    public static final int ANIMATION_DELAY = 1000;
    private String emptyGaugeName;
    private Runnable mAnimationRunnable;
    @InjectView(R.id.txt_empty_gauge)
    TextView mEmptyGaugeText;
    private Handler mHandler;
    private boolean mPlayedAnimation;
    private ValueAnimator mTextAlphaAnimator;
    
    public EmptyGaugePresenter(final Context context) {
        this.mPlayedAnimation = false;
        this.mHandler = new Handler();
        (this.mTextAlphaAnimator = new ValueAnimator()).setFloatValues(new float[] { 1.0f, 0.0f });
        this.mTextAlphaAnimator.setDuration(2000L);
        this.emptyGaugeName = context.getString(R.string.widget_empty);
        this.mTextAlphaAnimator.addUpdateListener((ValueAnimator.AnimatorUpdateListener)new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                final float floatValue = (float)valueAnimator.getAnimatedValue();
                if (EmptyGaugePresenter.this.mEmptyGaugeText != null) {
                    EmptyGaugePresenter.this.mEmptyGaugeText.setAlpha(floatValue);
                }
            }
        });
        this.mAnimationRunnable = new Runnable() {
            @Override
            public void run() {
                EmptyGaugePresenter.this.mTextAlphaAnimator.start();
            }
        };
    }
    
    @Override
    public Drawable getDrawable() {
        return null;
    }
    
    @Override
    public String getWidgetIdentifier() {
        return "EMPTY_WIDGET";
    }
    
    @Override
    public String getWidgetName() {
        return this.emptyGaugeName;
    }
    
    @Override
    public void setView(final DashboardWidgetView dashboardWidgetView, final Bundle bundle) {
        if (dashboardWidgetView != null) {
            dashboardWidgetView.setContentView(R.layout.empty_gauge_layout);
            ButterKnife.inject(this, (View)dashboardWidgetView);
            if (bundle != null) {
                if (bundle.getBoolean("EXTRA_IS_ACTIVE", false)) {
                    if (!this.mPlayedAnimation) {
                        this.mPlayedAnimation = true;
                        this.mTextAlphaAnimator.end();
                        this.mHandler.removeCallbacks(this.mAnimationRunnable);
                        this.mEmptyGaugeText.setAlpha(1.0f);
                        this.mHandler.postDelayed(this.mAnimationRunnable, 1000L);
                    }
                }
                else {
                    this.mPlayedAnimation = false;
                    this.mTextAlphaAnimator.end();
                    this.mHandler.removeCallbacks(this.mAnimationRunnable);
                    this.mEmptyGaugeText.setAlpha(1.0f);
                }
            }
            super.setView(dashboardWidgetView, bundle);
        }
        else {
            this.mPlayedAnimation = false;
            super.setView(dashboardWidgetView, bundle);
        }
    }
    
    @Override
    protected void updateGauge() {
    }
}
