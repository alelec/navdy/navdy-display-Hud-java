package com.navdy.hud.app.view;

import android.content.res.TypedArray;
import com.navdy.service.library.events.input.Gesture;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.hud.app.device.dial.DialFirmwareUpdater;
import android.view.ViewGroup.LayoutParams;
import com.navdy.hud.app.util.ViewUtil;
import com.navdy.hud.app.device.dial.DialManager;
import android.widget.LinearLayout;
import android.view.View;
import butterknife.ButterKnife;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.toast.ToastManager;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.animation.TimeInterpolator;
import android.view.animation.LinearInterpolator;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import java.util.List;
import java.util.ArrayList;
import com.navdy.hud.app.R;
import mortar.Mortar;
import android.util.AttributeSet;
import android.content.Context;
import com.navdy.hud.app.event.Shutdown;
import android.animation.ValueAnimator;
import javax.inject.Inject;
import com.navdy.hud.app.screen.ShutDownScreen;
import android.widget.TextView;
import android.widget.ImageView;
import butterknife.InjectView;
import android.os.Handler;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.hud.app.ui.component.ChoiceLayout;
import android.widget.RelativeLayout;

public class ShutDownConfirmationView extends RelativeLayout implements IListener, IInputHandler
{
    public static final int DIAL_OTA = 2;
    public static final int INACTIVITY = 0;
    private static final int INACTIVITY_SHUTDOWN_TIMEOUT = 60000;
    private static final int INSTALL_UPDATE_AND_SHUTDOWN_TIMEOUT = 30000;
    public static final int OTA = 1;
    private static final int SHUTDOWN_MESSAGE_MAX_WIDTH = 360;
    private static final int SHUTDOWN_RESET_TIMEOUT = 30000;
    private static final int SHUTDOWN_TIMEOUT = 5000;
    private static final int TAG_DO_NOT_SHUT_DOWN = 2;
    private static final int TAG_INSTALL_AND_SHUT_DOWN = 1;
    private static final int TAG_INSTALL_DIAL_UPDATE = 3;
    private static final int TAG_SHUT_DOWN = 0;
    private static final Logger sLogger;
    private boolean gesturesEnabled;
    private Handler handler;
    private int initialState;
    @InjectView(R.id.choiceLayout)
    ChoiceLayout mChoiceLayout;
    @InjectView(R.id.image)
    ImageView mIcon;
    @InjectView(R.id.title3)
    TextView mInfoText;
    boolean mIsUpdateAvailable;
    @InjectView(R.id.leftSwipe)
    ImageView mLefttSwipe;
    @InjectView(R.id.title2)
    TextView mMainTitleText;
    @Inject
    ShutDownScreen.Presenter mPresenter;
    @InjectView(R.id.rightSwipe)
    ImageView mRightSwipe;
    @InjectView(R.id.mainTitle)
    TextView mScreenTitleText;
    ValueAnimator mSecondsAnimator;
    @InjectView(R.id.title4)
    TextView mSummaryText;
    @InjectView(R.id.title1)
    TextView mTextView1;
    private Shutdown.Reason shutDownCause;
    private int swipeLeftAction;
    private int swipeRightAction;
    private Runnable timeout;
    String timerMessage;
    private String updateTargetVersion;
    
    static {
        sLogger = new Logger(ShutDownConfirmationView.class);
    }
    
    public ShutDownConfirmationView(final Context context) {
        this(context, null);
    }
    
    public ShutDownConfirmationView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public ShutDownConfirmationView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mIsUpdateAvailable = false;
        this.handler = new Handler();
        this.gesturesEnabled = false;
        this.swipeLeftAction = -1;
        this.swipeRightAction = -1;
        this.timerMessage = null;
        this.updateTargetVersion = "1.3.2884";
        this.shutDownCause = Shutdown.Reason.INACTIVITY;
        this.initialState = 0;
        if (!this.isInEditMode()) {
            Mortar.inject(context, this);
        }
        this.initFromAttributes(context, set);
    }
    
    private void cancelTimeout() {
        ShutDownConfirmationView.sLogger.v("cancelTimeout");
        if (this.timeout != null) {
            this.handler.removeCallbacks(this.timeout);
        }
        if (this.mSecondsAnimator != null) {
            this.mSecondsAnimator.removeAllUpdateListeners();
            this.mSecondsAnimator.removeAllListeners();
            this.mSecondsAnimator.cancel();
        }
    }
    
    private int getTimeout(final Shutdown.Reason reason) {
        int n = 0;
        switch (reason) {
            default:
                n = 5000;
                break;
            case INACTIVITY:
                n = 60000;
                break;
        }
        return n;
    }
    
    private void initFromAttributes(Context obtainStyledAttributes, final AttributeSet set) {
        obtainStyledAttributes = (Context)obtainStyledAttributes.getTheme().obtainStyledAttributes(set, R.styleable.ShutdownScreen, 0, 0);
        try {
            this.initialState = ((TypedArray)obtainStyledAttributes).getInt(0, 0);
        }
        finally {
            ((TypedArray)obtainStyledAttributes).recycle();
        }
    }
    
    private void performAction(final int n) {
        switch (n) {
            case 0:
                this.recordUpdateSelection(false);
                this.mPresenter.shutDown();
                break;
            case 2:
                this.recordUpdateSelection(false);
                this.mPresenter.finish();
                break;
            case 1:
                if (this.mPresenter.isSoftwareUpdatePending()) {
                    this.recordUpdateSelection(true);
                    this.mPresenter.installAndShutDown();
                    break;
                }
                break;
            case 3:
                if (this.mPresenter.isDialFirmwareUpdatePending()) {
                    this.recordUpdateSelection(true);
                    this.mScreenTitleText.setText(R.string.dial_update_started);
                    this.mMainTitleText.setText((CharSequence)"");
                    this.mInfoText.setVisibility(View.VISIBLE);
                    final ArrayList<Choice> list = new ArrayList<Choice>();
                    list.add(new ChoiceLayout.Choice(this.getContext().getString(R.string.shutdown), 0));
                    this.mChoiceLayout.setChoices(Mode.LABEL, list, 0, (ChoiceLayout.IListener)this);
                    this.mPresenter.installDialUpdateAndShutDown();
                    break;
                }
                break;
        }
    }
    
    private void recordUpdateSelection(final boolean b) {
        if (this.mIsUpdateAvailable) {
            AnalyticsSupport.recordUpdatePrompt(b, !this.mPresenter.isSoftwareUpdatePending(), this.updateTargetVersion);
        }
    }
    
    private void startCountDown() {
        ShutDownConfirmationView.sLogger.v("startCountDown");
        if (this.timeout != null) {
            this.handler.removeCallbacks(this.timeout);
        }
        if (this.mSecondsAnimator != null) {
            this.mSecondsAnimator.removeAllUpdateListeners();
            this.mSecondsAnimator.removeAllListeners();
            this.mSecondsAnimator.cancel();
        }
        if (this.initialState == 1) {
            if (this.timeout == null) {
                this.timeout = new Runnable() {
                    @Override
                    public void run() {
                        ShutDownConfirmationView.this.performAction(1);
                    }
                };
            }
            (this.mSecondsAnimator = ValueAnimator.ofInt(new int[] { 30, 0 })).setDuration(30000L);
            this.mSecondsAnimator.setInterpolator((TimeInterpolator)new LinearInterpolator());
            this.mSecondsAnimator.addUpdateListener((ValueAnimator.AnimatorUpdateListener)new ValueAnimator.AnimatorUpdateListener() {
                public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                    ShutDownConfirmationView.this.mInfoText.setText((CharSequence)ShutDownConfirmationView.this.getResources().getString(R.string.update_installation_will_begin_in, new Object[] { (int)valueAnimator.getAnimatedValue() }));
                }
            });
            this.handler.postDelayed(this.timeout, 30000L);
            this.mSecondsAnimator.start();
        }
        else if (this.initialState == 2) {
            if (this.timeout == null) {
                this.timeout = new Runnable() {
                    @Override
                    public void run() {
                        ShutDownConfirmationView.this.performAction(3);
                    }
                };
            }
            (this.mSecondsAnimator = ValueAnimator.ofInt(new int[] { 30, 0 })).setDuration(30000L);
            this.mSecondsAnimator.setInterpolator((TimeInterpolator)new LinearInterpolator());
            this.mSecondsAnimator.addUpdateListener((ValueAnimator.AnimatorUpdateListener)new ValueAnimator.AnimatorUpdateListener() {
                public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                    ShutDownConfirmationView.this.mInfoText.setText((CharSequence)ShutDownConfirmationView.this.getResources().getString(R.string.dial_update_installation_will_begin_in, new Object[] { (int)valueAnimator.getAnimatedValue() }));
                }
            });
            this.handler.postDelayed(this.timeout, 30000L);
            this.mSecondsAnimator.start();
        }
        else {
            int timeout = 30000;
            if (this.mSecondsAnimator == null) {
                timeout = this.getTimeout(this.shutDownCause);
            }
            if (this.timeout == null) {
                this.timeout = new Runnable() {
                    @Override
                    public void run() {
                        ShutDownConfirmationView.this.performAction(0);
                    }
                };
            }
            (this.mSecondsAnimator = ValueAnimator.ofInt(new int[] { timeout / 1000, 0 })).setDuration((long)timeout);
            this.mSecondsAnimator.setInterpolator((TimeInterpolator)new LinearInterpolator());
            this.mSecondsAnimator.addUpdateListener((ValueAnimator.AnimatorUpdateListener)new ValueAnimator.AnimatorUpdateListener() {
                public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                    ShutDownConfirmationView.this.mSummaryText.setText((CharSequence)ShutDownConfirmationView.this.getResources().getString(R.string.powering_off, new Object[] { (int)valueAnimator.getAnimatedValue() }));
                }
            });
            this.handler.postDelayed(this.timeout, (long)timeout);
            this.mSecondsAnimator.start();
        }
    }
    
    public void executeItem(final int n, final int n2) {
        this.performAction(n2);
    }
    
    public void itemSelected(final int n, final int n2) {
    }
    
    public IInputHandler nextHandler() {
        return null;
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.cancelTimeout();
        if (this.mPresenter != null) {
            this.mPresenter.dropView(this);
            ToastManager.getInstance().disableToasts(false);
            NotificationManager.getInstance().enableNotifications(true);
        }
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View)this);
        this.mTextView1.setVisibility(GONE);
        final LinearLayout linearLayout = (LinearLayout)this.findViewById(R.id.infoContainer);
        final ViewGroup.LayoutParams layoutParams = linearLayout.getLayoutParams();
        ((MaxWidthLinearLayout)linearLayout).setMaxWidth(360);
        layoutParams.width = -2;
        linearLayout.setLayoutParams(layoutParams);
        String s2;
        final String s = s2 = "1.2.2872";
        if (this.mPresenter != null) {
            this.mPresenter.takeView(this);
            ToastManager.getInstance().disableToasts(true);
            NotificationManager.getInstance().enableNotifications(false);
            if (this.mPresenter.isSoftwareUpdatePending()) {
                this.initialState = 1;
                this.updateTargetVersion = this.mPresenter.getUpdateVersion();
                s2 = this.mPresenter.getCurrentVersion();
            }
            else if (this.mPresenter.isDialFirmwareUpdatePending()) {
                this.initialState = 2;
                final DialFirmwareUpdater.Versions versions = DialManager.getInstance().getDialFirmwareUpdater().getVersions();
                this.updateTargetVersion = "1.0." + versions.local.incrementalVersion;
                s2 = "1.0." + versions.dial.incrementalVersion;
            }
            else {
                this.shutDownCause = this.mPresenter.getShutDownCause();
                this.initialState = 0;
                s2 = s;
            }
        }
        this.gesturesEnabled = false;
        this.swipeLeftAction = -1;
        this.swipeRightAction = -1;
        if (this.initialState == 1) {
            this.mIsUpdateAvailable = true;
            this.mScreenTitleText.setVisibility(GONE);
            this.mMainTitleText.setText(R.string.update_available);
            this.timerMessage = this.getResources().getString(R.string.update_installation_will_begin_in, new Object[] { 30 });
            this.mInfoText.setVisibility(View.VISIBLE);
            this.mInfoText.setText((CharSequence)this.timerMessage);
            final String format = String.format(this.getResources().getString(R.string.update_navdy_will_upgrade_from_to), this.updateTargetVersion, s2);
            this.mSummaryText.setVisibility(View.VISIBLE);
            this.mSummaryText.setText((CharSequence)format);
            this.mIcon.setImageResource(R.drawable.icon_software_update);
            final ArrayList<Choice> list = new ArrayList<Choice>();
            list.add(new ChoiceLayout.Choice(this.getContext().getString(R.string.install), 1));
            list.add(new ChoiceLayout.Choice(this.getContext().getString(R.string.shutdown), 0));
            list.add(new ChoiceLayout.Choice(this.getContext().getString(R.string.dont_shutdown), 2));
            this.mChoiceLayout.setChoices(Mode.LABEL, list, 0, (ChoiceLayout.IListener)this);
            this.swipeLeftAction = 1;
            this.swipeRightAction = 2;
            this.gesturesEnabled = true;
            this.mRightSwipe.setVisibility(View.VISIBLE);
            this.mLefttSwipe.setVisibility(View.VISIBLE);
            this.startCountDown();
        }
        else if (this.initialState == 2) {
            this.mIsUpdateAvailable = true;
            this.mScreenTitleText.setText((CharSequence)"");
            this.mMainTitleText.setText(R.string.dial_update_available);
            this.timerMessage = this.getResources().getString(R.string.dial_update_installation_will_begin_in, new Object[] { 30 });
            this.mInfoText.setVisibility(View.VISIBLE);
            this.mInfoText.setText((CharSequence)this.timerMessage);
            this.mSummaryText.setText((CharSequence)String.format(this.getResources().getString(R.string.dial_update_will_upgrade_to_from), this.updateTargetVersion, s2));
            this.mSummaryText.setVisibility(View.VISIBLE);
            this.mIcon.setImageResource(R.drawable.icon_dial_update);
            final ArrayList<Choice> list2 = new ArrayList<Choice>();
            list2.add(new ChoiceLayout.Choice(this.getContext().getString(R.string.install), 3));
            list2.add(new ChoiceLayout.Choice(this.getContext().getString(R.string.shutdown), 0));
            list2.add(new ChoiceLayout.Choice(this.getContext().getString(R.string.dont_shutdown), 2));
            this.mChoiceLayout.setChoices(Mode.LABEL, list2, 0, (ChoiceLayout.IListener)this);
            this.swipeLeftAction = 3;
            this.swipeRightAction = 2;
            this.gesturesEnabled = true;
            this.mRightSwipe.setVisibility(View.VISIBLE);
            this.mLefttSwipe.setVisibility(View.VISIBLE);
            this.startCountDown();
        }
        else {
            this.mIsUpdateAvailable = false;
            this.mScreenTitleText.setText((CharSequence)null);
            this.mMainTitleText.setText(R.string.shutting_down);
            this.mInfoText.setVisibility(View.VISIBLE);
            this.setShutdownCause(this.shutDownCause);
            this.mSummaryText.setText((CharSequence)this.getResources().getString(R.string.powering_off, new Object[] { 5 }));
            this.mSummaryText.setVisibility(View.VISIBLE);
            this.mIcon.setImageResource(R.drawable.icon_power);
            final ArrayList<Choice> list3 = new ArrayList<Choice>();
            list3.add(new ChoiceLayout.Choice(this.getContext().getString(R.string.shutdown), 0));
            list3.add(new ChoiceLayout.Choice(this.getContext().getString(R.string.dont_shutdown), 2));
            this.mChoiceLayout.setChoices(Mode.LABEL, list3, 0, (ChoiceLayout.IListener)this);
            ShutDownConfirmationView.sLogger.v("shutdown cause is " + this.shutDownCause + " time is " + this.getTimeout(this.shutDownCause));
            this.mRightSwipe.setVisibility(GONE);
            this.mLefttSwipe.setVisibility(GONE);
            this.startCountDown();
        }
        ViewUtil.autosize(this.mMainTitleText, 2, 360, R.array.title_sizes);
    }
    
    public boolean onGesture(final GestureEvent gestureEvent) {
        boolean b = true;
        if (this.gesturesEnabled && gestureEvent.gesture != null) {
            switch (gestureEvent.gesture) {
                case GESTURE_SWIPE_LEFT:
                    if (this.swipeLeftAction != -1) {
                        this.executeItem(0, this.swipeLeftAction);
                        return b;
                    }
                    break;
                case GESTURE_SWIPE_RIGHT:
                    if (this.swipeRightAction != -1) {
                        this.executeItem(0, this.swipeRightAction);
                        return b;
                    }
                    break;
            }
        }
        b = false;
        return b;
    }
    
    public boolean onKey(final CustomKeyEvent customKeyEvent) {
        boolean b = true;
        if (this.mChoiceLayout != null && this.mChoiceLayout.getVisibility() == 0) {
            switch (customKeyEvent) {
                case LEFT:
                    this.startCountDown();
                    this.mChoiceLayout.moveSelectionLeft();
                    break;
                case RIGHT:
                    this.startCountDown();
                    this.mChoiceLayout.moveSelectionRight();
                    break;
                case SELECT:
                    this.cancelTimeout();
                    this.mChoiceLayout.executeSelectedItem(true);
                    break;
                case POWER_BUTTON_DOUBLE_CLICK:
                    ShutDownConfirmationView.sLogger.v("power btn dlb-click");
                    this.cancelTimeout();
                    this.mPresenter.finish();
                    break;
                case POWER_BUTTON_CLICK:
                    ShutDownConfirmationView.sLogger.v("power btn click");
                    this.cancelTimeout();
                    this.mPresenter.finish();
                    break;
            }
        }
        else {
            b = false;
        }
        return b;
    }
    
    public void setShutdownCause(final Shutdown.Reason reason) {
        final Logger sLogger = ShutDownConfirmationView.sLogger;
        final StringBuilder append = new StringBuilder().append("Update is ");
        String s;
        if (this.mIsUpdateAvailable) {
            s = "";
        }
        else {
            s = "not ";
        }
        sLogger.d(append.append(s).append("available, shut down cause:").append(reason).toString());
        if (!this.mIsUpdateAvailable) {
            switch (reason) {
                default:
                    this.mInfoText.setVisibility(GONE);
                    break;
                case ENGINE_OFF:
                case ACCELERATE_SHUTDOWN:
                case INACTIVITY:
                    this.mInfoText.setVisibility(View.VISIBLE);
                    this.mInfoText.setText((CharSequence)this.getResources().getString(R.string.navdy_inactivity));
                    break;
            }
        }
        else {
            this.mInfoText.setVisibility(View.VISIBLE);
        }
    }
}
