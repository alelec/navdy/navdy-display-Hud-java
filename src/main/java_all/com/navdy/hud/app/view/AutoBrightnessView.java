package com.navdy.hud.app.view;

import android.text.style.StyleSpan;
import android.text.style.TypefaceSpan;
import android.text.SpannableStringBuilder;
import android.graphics.drawable.Drawable;
import java.util.List;
import com.navdy.service.library.events.input.GestureEvent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.view.ViewGroup.MarginLayoutParams;
import butterknife.ButterKnife;
import android.view.View;
import mortar.Mortar;
import android.util.AttributeSet;
import android.content.Context;
import javax.inject.Inject;
import com.navdy.hud.app.screen.AutoBrightnessScreen;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ImageView;
import butterknife.InjectView;
import com.navdy.hud.app.ui.component.ChoiceLayout;
import com.navdy.hud.app.manager.InputManager;
import android.widget.FrameLayout;

public class AutoBrightnessView extends FrameLayout implements IInputHandler
{
    @InjectView(R.id.choiceLayout)
    ChoiceLayout autoBrightnessChoices;
    @InjectView(R.id.image)
    ImageView autoBrightnessIcon;
    @InjectView(R.id.mainTitle)
    TextView autoBrightnessTitle;
    @InjectView(R.id.title2)
    TextView brightnessTitle;
    @InjectView(R.id.title3)
    TextView brightnessTitleDesc;
    @InjectView(R.id.infoContainer)
    LinearLayout infoContainer;
    @Inject
    AutoBrightnessScreen.Presenter presenter;
    String text;
    @InjectView(R.id.title1)
    TextView title1;
    
    public AutoBrightnessView(final Context context) {
        this(context, null);
    }
    
    public AutoBrightnessView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public AutoBrightnessView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        if (!this.isInEditMode()) {
            Mortar.inject(context, this);
        }
    }
    
    public void executeSelectedItem() {
        this.autoBrightnessChoices.executeSelectedItem(true);
    }
    
    public void moveSelectionLeft() {
        this.autoBrightnessChoices.moveSelectionLeft();
    }
    
    public void moveSelectionRight() {
        this.autoBrightnessChoices.moveSelectionRight();
    }
    
    public IInputHandler nextHandler() {
        return InputManager.nextContainingHandler((View)this);
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.presenter != null) {
            this.presenter.dropView(this);
        }
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View)this);
        final Resources resources = this.getResources();
        final int n = (int)resources.getDimension(R.dimen.auto_brightness_section_width);
        ((ViewGroup.MarginLayoutParams)this.infoContainer.getLayoutParams()).width = n;
        this.infoContainer.setMinimumWidth(n);
        ((ViewGroup.MarginLayoutParams)this.autoBrightnessChoices.getLayoutParams()).bottomMargin = (int)resources.getDimension(R.dimen.auto_brightness_choice_margin_bottom);
        this.autoBrightnessTitle.setTypeface(Typeface.create("sans-serif-medium", 0));
        this.title1.setVisibility(GONE);
        this.autoBrightnessTitle.setTypeface(Typeface.create("sans-serif-light", 0));
        this.autoBrightnessTitle.setTypeface(Typeface.create("sans-serif", 0));
        this.brightnessTitleDesc.setSingleLine(false);
        this.brightnessTitleDesc.setMaxLines(3);
        this.brightnessTitleDesc.setText((CharSequence)resources.getString(R.string.auto_brightness_description_text));
        this.text = resources.getString(R.string.auto_brightness_status_label_text);
    }
    
    public boolean onGesture(final GestureEvent gestureEvent) {
        return false;
    }
    
    public boolean onKey(final CustomKeyEvent customKeyEvent) {
        return this.presenter.handleKey(customKeyEvent);
    }
    
    public void setChoices(final List<ChoiceLayout.Choice> list, final ChoiceLayout.IListener listener) {
        this.autoBrightnessChoices.setChoices(ChoiceLayout.Mode.LABEL, list, 0, listener);
    }
    
    public void setIcon(final Drawable imageDrawable) {
        this.autoBrightnessIcon.setImageDrawable(imageDrawable);
    }
    
    public void setStatusLabel(final String s) {
        final SpannableStringBuilder text = new SpannableStringBuilder();
        text.append((CharSequence)this.text);
        text.setSpan(new TypefaceSpan("sans-serif-light"), 0, text.length(), 33);
        final int length = text.length();
        text.append((CharSequence)s);
        text.setSpan(new StyleSpan(1), length, text.length(), 33);
        this.brightnessTitle.setText((CharSequence)text);
    }
    
    public void setTitle(final String text) {
        this.autoBrightnessTitle.setText((CharSequence)text);
    }
}
