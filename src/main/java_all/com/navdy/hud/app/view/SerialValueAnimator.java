package com.navdy.hud.app.view;

import android.animation.Animator.AnimatorListener;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.animation.TimeInterpolator;
import android.view.animation.LinearInterpolator;
import java.util.LinkedList;
import android.animation.Animator;
import java.util.Queue;
import android.animation.ValueAnimator;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;

public class SerialValueAnimator
{
    private DefaultAnimationListener animationListener;
    private SerialValueAnimatorAdapter mAdapter;
    private boolean mAnimationRunning;
    private ValueAnimator mAnimator;
    private int mDuration;
    private Queue<Float> mValueQueue;
    
    public SerialValueAnimator(final SerialValueAnimatorAdapter mAdapter, final int mDuration) {
        this.animationListener = new DefaultAnimationListener() {
            @Override
            public void onAnimationEnd(final Animator animator) {
                if (animator instanceof ValueAnimator) {
                    SerialValueAnimator.this.mAdapter.animationComplete((float)((ValueAnimator)animator).getAnimatedValue());
                }
                if (SerialValueAnimator.this.mValueQueue.size() == 0) {
                    SerialValueAnimator.this.mAnimationRunning = false;
                    SerialValueAnimator.this.mAnimator = null;
                }
                else {
                    SerialValueAnimator.this.animate(SerialValueAnimator.this.mValueQueue.remove());
                }
            }
        };
        this.mValueQueue = new LinkedList<Float>();
        this.mAdapter = mAdapter;
        this.mDuration = mDuration;
        if (mAdapter == null) {
            throw new IllegalArgumentException("SerialValueAnimator cannot run without the adapter");
        }
    }
    
    private void animate(final float n) {
        (this.mAnimator = ValueAnimator.ofFloat(new float[] { this.mAdapter.getValue(), n })).setDuration((long)this.mDuration);
        this.mAnimator.setInterpolator((TimeInterpolator)new LinearInterpolator());
        this.mAnimator.addUpdateListener((ValueAnimator.AnimatorUpdateListener)new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                SerialValueAnimator.this.mAdapter.setValue((float)valueAnimator.getAnimatedValue());
            }
        });
        this.mAnimator.addListener((Animator.AnimatorListener)this.animationListener);
        this.mAnimator.start();
    }
    
    public void release() {
        this.mValueQueue.clear();
        if (this.mAnimator != null) {
            this.mAnimator.removeAllListeners();
            this.mAnimator.removeAllUpdateListeners();
            this.mAnimator.cancel();
        }
        this.mAnimator = null;
        this.mAnimationRunning = false;
    }
    
    public void setDuration(final int mDuration) {
        this.mDuration = mDuration;
    }
    
    public void setValue(final float value) {
        if (this.mAdapter.getValue() != value) {
            if (this.mAnimationRunning) {
                this.mValueQueue.add(value);
            }
            else {
                this.mAnimationRunning = true;
                this.animate(value);
            }
        }
        else {
            this.mAdapter.setValue(value);
            this.mAdapter.animationComplete(value);
        }
    }
    
    interface SerialValueAnimatorAdapter
    {
        void animationComplete(final float p0);
        
        float getValue();
        
        void setValue(final float p0);
    }
}
