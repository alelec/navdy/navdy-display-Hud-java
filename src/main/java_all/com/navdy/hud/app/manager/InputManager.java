package com.navdy.hud.app.manager;

import com.squareup.otto.Subscribe;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import android.view.ViewParent;
import android.view.View;
import com.navdy.hud.app.ui.activity.Main;
import com.navdy.hud.app.service.ShutdownMonitor;
import android.os.SystemClock;
import com.navdy.hud.app.device.light.HUDLightUtils;
import com.navdy.hud.app.device.light.LightManager;
import com.navdy.hud.app.HudApplication;
import android.view.KeyEvent;
import com.navdy.service.library.events.input.GestureEvent;
import android.os.Looper;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.device.PowerManager;
import android.os.Handler;
import com.navdy.hud.app.device.dial.DialManager;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;

public class InputManager
{
    private static final Logger sLogger;
    private Bus bus;
    private IInputHandler defaultHandler;
    private DialManager dialManager;
    private Handler handler;
    private IInputHandler inputHandler;
    private boolean isLongPressDetected;
    private long lastInputEventTime;
    private PowerManager powerManager;
    private UIStateManager uiStateManager;
    
    static {
        sLogger = new Logger(InputManager.class);
    }
    
    public InputManager(final Bus bus, final PowerManager powerManager, final UIStateManager uiStateManager) {
        this.lastInputEventTime = 0L;
        this.handler = new Handler(Looper.getMainLooper());
        this.powerManager = powerManager;
        (this.bus = bus).register(this);
        this.uiStateManager = uiStateManager;
    }
    
    private CustomKeyEvent convertKeyToCustomEvent(final int n, final KeyEvent keyEvent) {
        final CustomKeyEvent customKeyEvent = null;
        CustomKeyEvent customKeyEvent2 = null;
        switch (n) {
            default:
                customKeyEvent2 = customKeyEvent;
                break;
            case 9:
            case 20:
            case 39:
            case 47:
            case 51:
            case 52:
            case 66:
                if (keyEvent.isLongPress()) {
                    customKeyEvent2 = CustomKeyEvent.LONG_PRESS;
                    break;
                }
                customKeyEvent2 = CustomKeyEvent.SELECT;
                break;
            case 8:
            case 21:
            case 29:
            case 38:
            case 45:
            case 54:
                customKeyEvent2 = CustomKeyEvent.LEFT;
                break;
            case 10:
            case 22:
            case 31:
            case 32:
            case 33:
            case 40:
                customKeyEvent2 = CustomKeyEvent.RIGHT;
                break;
        }
        return customKeyEvent2;
    }
    
    private boolean invokeHandler(final GestureEvent gestureEvent, final CustomKeyEvent customKeyEvent) {
        boolean b;
        if (gestureEvent == null && customKeyEvent == null) {
            b = false;
        }
        else {
            boolean b3;
            final boolean b2 = b3 = false;
            Label_0077: {
                if (this.inputHandler != null) {
                    IInputHandler inputHandler = this.inputHandler;
                    while (true) {
                        b3 = b2;
                        if (inputHandler == null) {
                            break Label_0077;
                        }
                        if (gestureEvent != null) {
                            if (inputHandler.onGesture(gestureEvent)) {
                                HUDLightUtils.showGestureDetected(HudApplication.getAppContext(), LightManager.getInstance());
                                b3 = true;
                                break Label_0077;
                            }
                            inputHandler = inputHandler.nextHandler();
                        }
                        else {
                            if (inputHandler.onKey(customKeyEvent)) {
                                break;
                            }
                            inputHandler = inputHandler.nextHandler();
                        }
                    }
                    b3 = true;
                }
            }
            boolean onKey = b3;
            if (!b3) {
                onKey = b3;
                if (customKeyEvent != null) {
                    switch (customKeyEvent) {
                        default:
                            onKey = b3;
                            break;
                        case LONG_PRESS:
                            InputManager.sLogger.v("long press not handled");
                            if (this.defaultHandler == null) {
                                final Main rootScreen = this.uiStateManager.getRootScreen();
                                if (rootScreen != null) {
                                    this.defaultHandler = rootScreen.getInputHandler();
                                }
                            }
                            onKey = b3;
                            if (this.defaultHandler != null) {
                                onKey = this.defaultHandler.onKey(customKeyEvent);
                                break;
                            }
                            break;
                    }
                }
            }
            if (b = onKey) {
                this.lastInputEventTime = SystemClock.elapsedRealtime();
                ShutdownMonitor.getInstance().recordInputEvent();
                b = onKey;
            }
        }
        return b;
    }
    
    public static boolean isCenterKey(final int n) {
        return n == 47 || n == 52 || n == 51 || n == 66 || n == 20 || n == 39 || n == 9;
    }
    
    public static boolean isLeftKey(final int n) {
        return n == 29 || n == 54 || n == 45 || n == 21 || n == 38 || n == 8;
    }
    
    public static boolean isRightKey(final int n) {
        return n == 32 || n == 31 || n == 33 || n == 22 || n == 40 || n == 10;
    }
    
    public static IInputHandler nextContainingHandler(final View view) {
        ViewParent viewParent;
        for (viewParent = view.getParent(); viewParent != null && !(viewParent instanceof IInputHandler); viewParent = viewParent.getParent()) {}
        IInputHandler inputHandler;
        if (viewParent != null) {
            inputHandler = (IInputHandler)viewParent;
        }
        else {
            inputHandler = null;
        }
        return inputHandler;
    }
    
    public IInputHandler getFocus() {
        return this.inputHandler;
    }
    
    public long getLastInputEventTime() {
        return this.lastInputEventTime;
    }
    
    public void injectKey(final CustomKeyEvent customKeyEvent) {
        this.powerManager.wakeUp(AnalyticsSupport.WakeupReason.POWERBUTTON);
        this.handler.post((Runnable)new Runnable() {
            @Override
            public void run() {
                if (customKeyEvent != null) {
                    InputManager.sLogger.v("inject key event:" + customKeyEvent);
                    if (!InputManager.this.invokeHandler(null, customKeyEvent)) {
                        final Main rootScreen = InputManager.this.uiStateManager.getRootScreen();
                        if (rootScreen != null) {
                            rootScreen.handleKey(customKeyEvent);
                        }
                    }
                }
            }
        });
    }
    
    @Subscribe
    public void onGestureEvent(final GestureEvent gestureEvent) {
        this.invokeHandler(gestureEvent, null);
    }
    
    public boolean onKeyDown(final int n, final KeyEvent keyEvent) {
        final boolean b = true;
        if (InputManager.sLogger.isLoggable(3)) {
            InputManager.sLogger.v("onKeyDown:" + n + " isLongPress:" + keyEvent.isLongPress() + " " + keyEvent);
        }
        this.bus.post(keyEvent);
        if (this.dialManager == null) {
            this.dialManager = DialManager.getInstance();
        }
        if (this.dialManager.reportInputEvent(keyEvent)) {
            HUDLightUtils.dialActionDetected(HudApplication.getAppContext(), LightManager.getInstance());
        }
        boolean invokeHandler = b;
        if (!keyEvent.isLongPress()) {
            if (this.isLongPressDetected) {
                invokeHandler = b;
            }
            else {
                final CustomKeyEvent convertKeyToCustomEvent = this.convertKeyToCustomEvent(n, keyEvent);
                if (convertKeyToCustomEvent != null) {
                    if (convertKeyToCustomEvent == CustomKeyEvent.SELECT) {
                        keyEvent.startTracking();
                        invokeHandler = b;
                    }
                    else {
                        invokeHandler = this.invokeHandler(null, convertKeyToCustomEvent);
                    }
                }
                else {
                    invokeHandler = false;
                }
            }
        }
        return invokeHandler;
    }
    
    public boolean onKeyLongPress(final int n, final KeyEvent keyEvent) {
        if (InputManager.sLogger.isLoggable(3)) {
            InputManager.sLogger.v("onKeyLongPress:" + n + " " + keyEvent);
        }
        this.isLongPressDetected = true;
        return this.invokeHandler(null, this.convertKeyToCustomEvent(n, keyEvent));
    }
    
    public boolean onKeyUp(final int n, final KeyEvent keyEvent) {
        final boolean b = false;
        if (InputManager.sLogger.isLoggable(3)) {
            InputManager.sLogger.v("onKeyUp:" + n + " isLongPress:" + keyEvent.isLongPress() + " " + keyEvent);
        }
        this.bus.post(keyEvent);
        boolean invokeHandler;
        if (this.isLongPressDetected) {
            this.isLongPressDetected = false;
            invokeHandler = true;
        }
        else {
            final CustomKeyEvent convertKeyToCustomEvent = this.convertKeyToCustomEvent(n, keyEvent);
            invokeHandler = b;
            if (convertKeyToCustomEvent != null) {
                switch (convertKeyToCustomEvent) {
                    default:
                        invokeHandler = b;
                        break;
                    case SELECT:
                    case LONG_PRESS:
                        invokeHandler = this.invokeHandler(null, convertKeyToCustomEvent);
                        break;
                }
            }
        }
        return invokeHandler;
    }
    
    public void setFocus(final IInputHandler inputHandler) {
        this.inputHandler = inputHandler;
    }
    
    public enum CustomKeyEvent
    {
        LEFT, 
        LONG_PRESS, 
        POWER_BUTTON_CLICK, 
        POWER_BUTTON_DOUBLE_CLICK, 
        POWER_BUTTON_LONG_PRESS, 
        RIGHT, 
        SELECT;
    }
    
    public interface IInputHandler
    {
        IInputHandler nextHandler();
        
        boolean onGesture(final GestureEvent p0);
        
        boolean onKey(final CustomKeyEvent p0);
    }
}
