package com.navdy.hud.app.debug;

import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;
import com.navdy.service.library.log.Logger;

public class BusLeakDetector
{
    private static final String EMPTY = "";
    private static final Logger sLogger;
    private static final BusLeakDetector singleton;
    private HashMap<Class, Integer> registrationMap;
    
    static {
        sLogger = new Logger(BusLeakDetector.class);
        singleton = new BusLeakDetector();
    }
    
    public BusLeakDetector() {
        this.registrationMap = new HashMap<Class, Integer>(100);
        BusLeakDetector.sLogger.v("::ctor::");
    }
    
    public static BusLeakDetector getInstance() {
        return BusLeakDetector.singleton;
    }
    
    public void addReference(final Class clazz) {
        synchronized (this) {
            final Integer n = this.registrationMap.get(clazz);
            if (n == null) {
                this.registrationMap.put(clazz, 1);
            }
            else {
                this.registrationMap.put(clazz, n + 1);
            }
        }
    }
    
    public void printReferences() {
    Label_0127_Outer:
        while (true) {
            while (true) {
                Label_0208: {
                    Label_0202: {
                        Label_0196: {
                            Label_0190: {
                                synchronized (this) {
                                    BusLeakDetector.sLogger.v("::printReferences: [" + this.registrationMap.size() + "]");
                                    for (final Map.Entry<Class, Integer> entry : this.registrationMap.entrySet()) {
                                        final int intValue = entry.getValue();
                                        switch (intValue) {
                                            default: {
                                                final String s = " ****************";
                                                BusLeakDetector.sLogger.v(entry.getKey().getName() + " = " + intValue + s);
                                                continue Label_0127_Outer;
                                            }
                                            case 1:
                                                break Label_0190;
                                            case 2:
                                                break Label_0196;
                                            case 3:
                                                break Label_0202;
                                            case 4:
                                                break Label_0208;
                                        }
                                    }
                                    break;
                                }
                            }
                            final String s = "";
                            continue;
                        }
                        final String s = " **";
                        continue;
                    }
                    final String s = " ****";
                    continue;
                }
                final String s = " ********";
                continue;
            }
        }
    }
    // monitorexit(this)
    
    public void removeReference(final Class clazz) {
        synchronized (this) {
            final Integer n = this.registrationMap.get(clazz);
            if (n != null) {
                final Integer value = n - 1;
                if (value == 0) {
                    this.registrationMap.remove(clazz);
                }
                else {
                    this.registrationMap.put(clazz, value);
                }
            }
        }
    }
}
