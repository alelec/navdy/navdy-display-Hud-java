package com.navdy.hud.app.debug;

import kotlin.jvm.JvmOverloads;
import java.io.Writer;
import java.io.OutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import java.io.BufferedWriter;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 1 }, d1 = { "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0006\u0010\r\u001a\u00020\u000eJ\u0006\u0010\u000f\u001a\u00020\u000eJ\u001a\u0010\u0010\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u00032\b\b\u0002\u0010\u0012\u001a\u00020\u0013H\u0007R\u0011\u0010\t\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014" }, d2 = { "Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;", "", "filePath", "", "executor", "Lcom/navdy/hud/app/debug/SerialExecutor;", "bufferSize", "", "(Ljava/lang/String;Lcom/navdy/hud/app/debug/SerialExecutor;I)V", "bufferedWriter", "Ljava/io/BufferedWriter;", "getBufferedWriter", "()Ljava/io/BufferedWriter;", "flush", "", "flushAndClose", "write", "data", "forceToDisk", "", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
public final class AsyncBufferedFileWriter
{
    @NotNull
    private final BufferedWriter bufferedWriter;
    private final SerialExecutor executor;
    
    public AsyncBufferedFileWriter(@NotNull final String s, @NotNull final SerialExecutor executor, final int n) {
        Intrinsics.checkParameterIsNotNull(s, "filePath");
        Intrinsics.checkParameterIsNotNull(executor, "executor");
        this.executor = executor;
        this.bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(s)), "utf-8"), n);
    }
    
    public final void flush() {
        this.executor.execute((Runnable)new AsyncBufferedFileWriter$flush.AsyncBufferedFileWriter$flush$1(this));
    }
    
    public final void flushAndClose() {
        this.executor.execute((Runnable)new AsyncBufferedFileWriter$flushAndClose.AsyncBufferedFileWriter$flushAndClose$1(this));
    }
    
    @NotNull
    public final BufferedWriter getBufferedWriter() {
        return this.bufferedWriter;
    }
    
    @JvmOverloads
    public final void write(@NotNull final String s) {
        write$default(this, s, false, 2, null);
    }
    
    @JvmOverloads
    public final void write(@NotNull final String s, final boolean b) {
        Intrinsics.checkParameterIsNotNull(s, "data");
        this.executor.execute((Runnable)new AsyncBufferedFileWriter$write.AsyncBufferedFileWriter$write$1(this, s, b));
    }
}
