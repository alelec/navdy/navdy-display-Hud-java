package com.navdy.hud.app.event;

import com.navdy.service.library.events.DeviceInfo;

public class DeviceInfoAvailable
{
    public final DeviceInfo deviceInfo;
    
    public DeviceInfoAvailable(final DeviceInfo deviceInfo) {
        this.deviceInfo = deviceInfo;
    }
}
