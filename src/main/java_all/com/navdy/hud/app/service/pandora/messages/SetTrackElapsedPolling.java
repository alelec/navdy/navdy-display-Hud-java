package com.navdy.hud.app.service.pandora.messages;

import java.io.ByteArrayOutputStream;
import com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException;
import com.navdy.hud.app.service.pandora.exceptions.UnexpectedEndOfStringException;
import com.navdy.hud.app.service.pandora.exceptions.StringOverflowException;
import java.io.IOException;

public class SetTrackElapsedPolling extends BaseOutgoingConstantMessage
{
    public static final SetTrackElapsedPolling DISABLED;
    public static final SetTrackElapsedPolling ENABLED;
    public boolean isOn;
    
    static {
        ENABLED = new SetTrackElapsedPolling(true);
        DISABLED = new SetTrackElapsedPolling(false);
    }
    
    private SetTrackElapsedPolling(final boolean isOn) {
        this.isOn = isOn;
    }
    
    @Override
    protected ByteArrayOutputStream putThis(final ByteArrayOutputStream byteArrayOutputStream) throws IOException, StringOverflowException, UnexpectedEndOfStringException, MessageWrongWayException {
        BaseOutgoingMessage.putByte(byteArrayOutputStream, (byte)21);
        BaseOutgoingMessage.putBoolean(byteArrayOutputStream, this.isOn);
        return byteArrayOutputStream;
    }
    
    @Override
    public String toString() {
        return "Setting track elapsed polling to: " + this.isOn;
    }
}
