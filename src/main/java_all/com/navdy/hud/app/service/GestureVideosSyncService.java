package com.navdy.hud.app.service;

import mortar.Mortar;
import android.app.PendingIntent;
import android.os.SystemClock;
import android.app.AlarmManager;
import android.content.Intent;
import com.navdy.hud.app.HudApplication;
import java.io.File;
import javax.inject.Inject;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;
import java.util.concurrent.atomic.AtomicBoolean;

public class GestureVideosSyncService extends S3FileUploadService
{
    private static final int MAX_GESTURE_VIDEOS_TO_UPLOAD = 100;
    public static final String NAVDY_GESTURE_VIDEOS_BUCKET = "navdy-gesture-videos";
    private static AtomicBoolean isUploading;
    private static boolean mIsInitialized;
    private static Request sCurrentRequest;
    private static String sGestureVideosFolder;
    private static final UploadQueue sGestureVideosUploadQueue;
    private static Logger sLogger;
    @Inject
    Bus bus;
    
    static {
        GestureVideosSyncService.sLogger = new Logger(GestureVideosSyncService.class);
        sGestureVideosUploadQueue = new UploadQueue();
        GestureVideosSyncService.isUploading = new AtomicBoolean(false);
        GestureVideosSyncService.mIsInitialized = false;
    }
    
    public GestureVideosSyncService() {
        super("GESTURE_VIDEOS_SYNC");
    }
    
    public static void addGestureVideoToUploadQueue(final File file, final String s) {
        initializeIfNecessary(file);
        final UploadQueue sGestureVideosUploadQueue = GestureVideosSyncService.sGestureVideosUploadQueue;
        synchronized (sGestureVideosUploadQueue) {
            GestureVideosSyncService.sLogger.d("Add gesture video to upload " + file.getName());
            GestureVideosSyncService.sGestureVideosUploadQueue.add(new Request(file, s));
            GestureVideosSyncService.sLogger.d("Queue size : " + GestureVideosSyncService.sGestureVideosUploadQueue.size());
            if (!GestureVideosSyncService.isUploading.get() && GestureVideosSyncService.sGestureVideosUploadQueue.size() > 100) {
                GestureVideosSyncService.sLogger.d("Number of videos to upload exceeded ");
                GestureVideosSyncService.sGestureVideosUploadQueue.pop();
            }
        }
    }
    
    private static void initializeIfNecessary() {
        initializeIfNecessary(null);
    }
    
    private static void initializeIfNecessary(final File p0) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: astore_1       
        //     4: aload_1        
        //     5: dup            
        //     6: astore_2       
        //     7: monitorenter   
        //     8: getstatic       com/navdy/hud/app/service/GestureVideosSyncService.mIsInitialized:Z
        //    11: ifne            354
        //    14: getstatic       com/navdy/hud/app/service/GestureVideosSyncService.sLogger:Lcom/navdy/service/library/log/Logger;
        //    17: ldc             "Not initialized , initializing now"
        //    19: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //    22: getstatic       com/navdy/hud/app/service/GestureVideosSyncService.mIsInitialized:Z
        //    25: ifne            354
        //    28: invokestatic    com/navdy/hud/app/storage/PathManager.getInstance:()Lcom/navdy/hud/app/storage/PathManager;
        //    31: invokevirtual   com/navdy/hud/app/storage/PathManager.getGestureVideosSyncFolder:()Ljava/lang/String;
        //    34: putstatic       com/navdy/hud/app/service/GestureVideosSyncService.sGestureVideosFolder:Ljava/lang/String;
        //    37: new             Ljava/io/File;
        //    40: astore_3       
        //    41: aload_3        
        //    42: getstatic       com/navdy/hud/app/service/GestureVideosSyncService.sGestureVideosFolder:Ljava/lang/String;
        //    45: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //    48: aload_3        
        //    49: invokevirtual   java/io/File.listFiles:()[Ljava/io/File;
        //    52: astore          4
        //    54: new             Ljava/util/ArrayList;
        //    57: astore_3       
        //    58: aload_3        
        //    59: invokespecial   java/util/ArrayList.<init>:()V
        //    62: aload           4
        //    64: arraylength    
        //    65: istore          5
        //    67: iconst_0       
        //    68: istore          6
        //    70: iload           6
        //    72: iload           5
        //    74: if_icmpge       307
        //    77: aload           4
        //    79: iload           6
        //    81: aaload         
        //    82: astore          7
        //    84: getstatic       com/navdy/hud/app/service/GestureVideosSyncService.sLogger:Lcom/navdy/service/library/log/Logger;
        //    87: astore          8
        //    89: new             Ljava/lang/StringBuilder;
        //    92: astore          9
        //    94: aload           9
        //    96: invokespecial   java/lang/StringBuilder.<init>:()V
        //    99: aload           8
        //   101: aload           9
        //   103: ldc             "Session directory :"
        //   105: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   108: aload           7
        //   110: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   113: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   116: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //   119: aload           7
        //   121: invokevirtual   java/io/File.isDirectory:()Z
        //   124: ifeq            301
        //   127: aload           7
        //   129: invokevirtual   java/io/File.listFiles:()[Ljava/io/File;
        //   132: astore          9
        //   134: aload           9
        //   136: arraylength    
        //   137: ifne            183
        //   140: getstatic       com/navdy/hud/app/service/GestureVideosSyncService.sLogger:Lcom/navdy/service/library/log/Logger;
        //   143: astore          8
        //   145: new             Ljava/lang/StringBuilder;
        //   148: astore          10
        //   150: aload           10
        //   152: invokespecial   java/lang/StringBuilder.<init>:()V
        //   155: aload           8
        //   157: aload           10
        //   159: ldc             "Remove empty directory:"
        //   161: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   164: aload           7
        //   166: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   169: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   172: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //   175: invokestatic    com/navdy/hud/app/HudApplication.getAppContext:()Landroid/content/Context;
        //   178: aload           7
        //   180: invokestatic    com/navdy/service/library/util/IOUtils.deleteDirectory:(Landroid/content/Context;Ljava/io/File;)V
        //   183: aload           9
        //   185: arraylength    
        //   186: istore          11
        //   188: iconst_0       
        //   189: istore          12
        //   191: iload           12
        //   193: iload           11
        //   195: if_icmpge       301
        //   198: aload           9
        //   200: iload           12
        //   202: aaload         
        //   203: astore          8
        //   205: getstatic       com/navdy/hud/app/service/GestureVideosSyncService.sLogger:Lcom/navdy/service/library/log/Logger;
        //   208: astore          10
        //   210: new             Ljava/lang/StringBuilder;
        //   213: astore          7
        //   215: aload           7
        //   217: invokespecial   java/lang/StringBuilder.<init>:()V
        //   220: aload           10
        //   222: aload           7
        //   224: ldc             "Gesture video :"
        //   226: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   229: aload           8
        //   231: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   234: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   237: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //   240: aload           8
        //   242: invokevirtual   java/io/File.isFile:()Z
        //   245: istore          13
        //   247: iload           13
        //   249: ifne            258
        //   252: iinc            12, 1
        //   255: goto            191
        //   258: aload_0        
        //   259: ifnull          281
        //   262: aload           8
        //   264: invokevirtual   java/io/File.getCanonicalPath:()Ljava/lang/String;
        //   267: aload_0        
        //   268: invokevirtual   java/io/File.getCanonicalPath:()Ljava/lang/String;
        //   271: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   274: istore          13
        //   276: iload           13
        //   278: ifne            252
        //   281: aload_3        
        //   282: aload           8
        //   284: invokevirtual   java/util/ArrayList.add:(Ljava/lang/Object;)Z
        //   287: pop            
        //   288: goto            252
        //   291: astore_0       
        //   292: aload_2        
        //   293: monitorexit    
        //   294: aload_0        
        //   295: athrow         
        //   296: astore          7
        //   298: goto            252
        //   301: iinc            6, 1
        //   304: goto            70
        //   307: getstatic       com/navdy/hud/app/service/GestureVideosSyncService.sLogger:Lcom/navdy/service/library/log/Logger;
        //   310: astore          4
        //   312: new             Ljava/lang/StringBuilder;
        //   315: astore_0       
        //   316: aload_0        
        //   317: invokespecial   java/lang/StringBuilder.<init>:()V
        //   320: aload           4
        //   322: aload_0        
        //   323: ldc             "Number of Gesture videos :"
        //   325: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   328: aload_3        
        //   329: invokevirtual   java/util/ArrayList.size:()I
        //   332: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   335: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   338: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //   341: aload_3        
        //   342: getstatic       com/navdy/hud/app/service/GestureVideosSyncService.sGestureVideosUploadQueue:Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;
        //   345: bipush          100
        //   347: invokestatic    com/navdy/hud/app/service/GestureVideosSyncService.populateFilesQueue:(Ljava/util/ArrayList;Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;I)V
        //   350: iconst_1       
        //   351: putstatic       com/navdy/hud/app/service/GestureVideosSyncService.mIsInitialized:Z
        //   354: aload_2        
        //   355: monitorexit    
        //   356: return         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  8      67     291    296    Any
        //  84     183    291    296    Any
        //  183    188    291    296    Any
        //  205    247    291    296    Any
        //  262    276    296    301    Ljava/io/IOException;
        //  262    276    291    296    Any
        //  281    288    291    296    Any
        //  292    294    291    296    Any
        //  307    354    291    296    Any
        //  354    356    291    296    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0281:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static void scheduleWithDelay(final long n) {
        final Intent intent = new Intent(HudApplication.getAppContext(), (Class)GestureVideosSyncService.class);
        intent.setAction("SYNC");
        ((AlarmManager)HudApplication.getAppContext().getSystemService("alarm")).setExact(3, SystemClock.elapsedRealtime() + n, PendingIntent.getService(HudApplication.getAppContext(), 123, intent, 268435456));
    }
    
    public static void syncNow() {
        GestureVideosSyncService.sLogger.d("synNow");
        final Intent intent = new Intent(HudApplication.getAppContext(), (Class)GestureVideosSyncService.class);
        intent.setAction("SYNC");
        HudApplication.getAppContext().startService(intent);
    }
    
    @Override
    public boolean canCompleteRequest(final Request request) {
        return false;
    }
    
    @Override
    protected String getAWSBucket() {
        return "navdy-gesture-videos";
    }
    
    @Override
    protected Request getCurrentRequest() {
        return GestureVideosSyncService.sCurrentRequest;
    }
    
    @Override
    protected AtomicBoolean getIsUploading() {
        return GestureVideosSyncService.isUploading;
    }
    
    @Override
    protected String getKeyPrefix(final File file) {
        return "archives" + File.separator + file.getParentFile().getName();
    }
    
    @Override
    protected Logger getLogger() {
        return GestureVideosSyncService.sLogger;
    }
    
    @Override
    protected UploadQueue getUploadQueue() {
        return GestureVideosSyncService.sGestureVideosUploadQueue;
    }
    
    @Override
    protected void initialize() {
        initializeIfNecessary();
    }
    
    @Override
    public void onCreate() {
        Mortar.inject(HudApplication.getAppContext(), this);
        super.onCreate();
    }
    
    @Override
    public void reSchedule() {
        GestureVideosSyncService.sLogger.d("reschedule");
        scheduleWithDelay(10000L);
    }
    
    @Override
    protected void setCurrentRequest(final Request sCurrentRequest) {
        GestureVideosSyncService.sCurrentRequest = sCurrentRequest;
    }
    
    @Override
    public void sync() {
        syncNow();
    }
    
    @Override
    protected void uploadFinished(final boolean b, final String s, final String s2) {
        this.bus.post(new UploadFinished(b, s, s2));
    }
}
