package com.navdy.hud.app.service.pandora;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.HashMap;
import java.util.Map;

abstract class FrameMessage
{
    protected static final byte[] ACK_PAYLOAD;
    protected static final int ADDITIONAL_BYTES_LENGTH = 6;
    protected static final int CRC_BYTES_LENGTH = 2;
    protected static final int FRAME_TYPE_POSITION = 1;
    protected static final byte PNDR_FRAME_END = 124;
    private static final byte PNDR_FRAME_ESCAPE = 125;
    private static final Map<Byte, Byte> PNDR_FRAME_ESCAPE_MAPPING;
    protected static final byte PNDR_FRAME_SEQUENCE_0 = 0;
    protected static final byte PNDR_FRAME_SEQUENCE_1 = 1;
    protected static final byte PNDR_FRAME_START = 126;
    protected static final byte PNDR_FRAME_TYPE_ACK = 1;
    protected static final byte PNDR_FRAME_TYPE_DATA = 0;
    private static final Map<Byte, Byte> PNDR_FRAME_UNESCAPE_MAPPING;
    protected boolean isSequence0;
    protected byte[] payload;
    
    static {
        ACK_PAYLOAD = new byte[0];
        PNDR_FRAME_ESCAPE_MAPPING = new HashMap<Byte, Byte>() {
            {
                this.put((byte)126, new Byte((byte)94));
                this.put((byte)124, new Byte((byte)92));
                this.put((byte)125, new Byte((byte)93));
            }
        };
        PNDR_FRAME_UNESCAPE_MAPPING = new HashMap<Byte, Byte>(FrameMessage.PNDR_FRAME_ESCAPE_MAPPING.size());
        for (final Map.Entry<Byte, Byte> entry : FrameMessage.PNDR_FRAME_ESCAPE_MAPPING.entrySet()) {
            FrameMessage.PNDR_FRAME_UNESCAPE_MAPPING.put(entry.getValue(), entry.getKey());
        }
    }
    
    protected FrameMessage(final boolean isSequence0, final byte[] payload) {
        this.isSequence0 = isSequence0;
        this.payload = payload;
    }
    
    protected static byte[] buildFrameFromCRCPart(final ByteBuffer byteBuffer) {
        final ByteBuffer calculate = CRC16CCITT.calculate(byteBuffer);
        final byte[] escapeBytes = escapeBytes(byteBuffer);
        final byte[] escapeBytes2 = escapeBytes(calculate);
        return ByteBuffer.allocate(escapeBytes.length + escapeBytes2.length + 2).put((byte)126).put(escapeBytes).put(escapeBytes2).put((byte)124).array();
    }
    
    protected static byte[] escapeBytes(final ByteBuffer byteBuffer) {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        for (int i = 0; i < byteBuffer.limit(); ++i) {
            final byte value = byteBuffer.get(i);
            final Byte b = FrameMessage.PNDR_FRAME_ESCAPE_MAPPING.get(value);
            if (b != null) {
                byteArrayOutputStream.write(125);
                byteArrayOutputStream.write(b);
            }
            else {
                byteArrayOutputStream.write(value);
            }
        }
        return byteArrayOutputStream.toByteArray();
    }
    
    public static FrameMessage parseFrame(final byte[] array) throws IllegalArgumentException {
        final byte b = array[1];
        FrameMessage frameMessage;
        if (b == 1) {
            frameMessage = AckFrameMessage.parseAckFrame(array);
        }
        else {
            if (b != 0) {
                throw new IllegalArgumentException("Unknown message frame type: " + String.format("%02X", b));
            }
            frameMessage = DataFrameMessage.parseDataFrame(array);
        }
        return frameMessage;
    }
    
    protected static ByteBuffer unescapeBytes(final byte[] array) throws IllegalArgumentException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        final int length = array.length;
        int i = 0;
    Label_0065_Outer:
        while (i < length) {
            byte byteValue = array[i];
            int n = i;
            while (true) {
                if (byteValue == 125) {
                    n = i + 1;
                    try {
                        byteValue = FrameMessage.PNDR_FRAME_UNESCAPE_MAPPING.get(array[n]);
                        byteArrayOutputStream.write(byteValue);
                        i = n + 1;
                        continue Label_0065_Outer;
                    }
                    catch (ArrayIndexOutOfBoundsException ex) {
                        throw new IllegalArgumentException("Escape byte not followed by a byte");
                    }
                    catch (NullPointerException ex2) {
                        throw new IllegalArgumentException("Escape byte not followed by a proper byte");
                    }
                    break;
                }
                continue;
            }
        }
        return ByteBuffer.wrap(byteArrayOutputStream.toByteArray());
    }
    
    public abstract byte[] buildFrame();
}
