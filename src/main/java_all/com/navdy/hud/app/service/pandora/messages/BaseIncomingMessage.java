package com.navdy.hud.app.service.pandora.messages;

import com.navdy.hud.app.service.pandora.exceptions.StringOverflowException;
import com.navdy.hud.app.service.pandora.exceptions.UnterminatedStringException;
import java.io.Closeable;
import com.navdy.service.library.util.IOUtils;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException;
import com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException;
import com.navdy.hud.app.service.pandora.exceptions.UnsupportedMessageReceivedException;

public abstract class BaseIncomingMessage extends BaseMessage
{
    public static BaseIncomingMessage buildFromPayload(final byte[] array) throws MessageWrongWayException, CorruptedPayloadException, UnsupportedMessageReceivedException {
        BaseIncomingMessage baseIncomingMessage = null;
        switch (array[0]) {
            default:
                throw new UnsupportedMessageReceivedException(array[0]);
            case -127:
                baseIncomingMessage = UpdateStatus.innerBuildFromPayload(array);
                break;
            case -112:
                baseIncomingMessage = UpdateTrack.innerBuildFromPayload(array);
                break;
            case -98:
                baseIncomingMessage = UpdateTrackCompleted.innerBuildFromPayload(array);
                break;
            case -70:
                baseIncomingMessage = UpdateStationActive.innerBuildFromPayload(array);
                break;
            case -99:
                baseIncomingMessage = ReturnTrackInfoExtended.innerBuildFromPayload(array);
                break;
            case -106:
                baseIncomingMessage = UpdateTrackAlbumArt.innerBuildFromPayload(array);
                break;
            case -107:
                baseIncomingMessage = ReturnTrackAlbumArtSegment.innerBuildFromPayload(array);
                break;
            case -105:
                baseIncomingMessage = UpdateTrackElapsed.innerBuildFromPayload(array);
                break;
        }
        return baseIncomingMessage;
    }
    
    protected static boolean getBoolean(final ByteBuffer byteBuffer) {
        return byteBuffer.get() != 0;
    }
    
    protected static String getFixedLengthASCIIString(final ByteBuffer byteBuffer, final int n) {
        ByteArrayOutputStream byteArrayOutputStream = null;
        ByteArrayOutputStream byteArrayOutputStream2;
        for (int i = 0; i < n; ++i, byteArrayOutputStream = byteArrayOutputStream2) {
            final byte value = byteBuffer.get();
            if (value == 0 && byteArrayOutputStream != null) {
                break;
            }
            byteArrayOutputStream2 = byteArrayOutputStream;
            if (value != 0) {
                if ((byteArrayOutputStream2 = byteArrayOutputStream) == null) {
                    byteArrayOutputStream2 = new ByteArrayOutputStream();
                }
                byteArrayOutputStream2.write(value);
            }
        }
        String s = "";
        if (byteArrayOutputStream != null) {
            s = new String(byteArrayOutputStream.toByteArray(), BaseIncomingMessage.FIXED_LENGTH_STRING_ENCODING);
            IOUtils.closeStream(byteArrayOutputStream);
        }
        return s;
    }
    
    protected static String getString(final ByteBuffer byteBuffer) throws UnterminatedStringException, StringOverflowException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int n = 0;
        for (byte b = byteBuffer.get(); b != 0; b = byteBuffer.get()) {
            if (b < 0) {
                throw new UnterminatedStringException();
            }
            byteArrayOutputStream.write(b);
            if (++n > 247) {
                throw new StringOverflowException();
            }
        }
        return new String(byteArrayOutputStream.toByteArray(), BaseIncomingMessage.STRING_ENCODING);
    }
    
    protected static BaseIncomingMessage innerBuildFromPayload(final byte[] array) throws MessageWrongWayException, CorruptedPayloadException {
        throw new MessageWrongWayException();
    }
}
