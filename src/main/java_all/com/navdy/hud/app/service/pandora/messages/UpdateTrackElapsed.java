package com.navdy.hud.app.service.pandora.messages;

import com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException;
import com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException;
import java.nio.ByteBuffer;

public class UpdateTrackElapsed extends BaseIncomingMessage
{
    public short elapsed;
    public int trackToken;
    
    protected static BaseIncomingMessage innerBuildFromPayload(final byte[] array) throws MessageWrongWayException, CorruptedPayloadException {
        final ByteBuffer wrap = ByteBuffer.wrap(array);
        wrap.get();
        final UpdateTrackElapsed updateTrackElapsed = new UpdateTrackElapsed();
        updateTrackElapsed.trackToken = wrap.getInt();
        updateTrackElapsed.elapsed = wrap.getShort();
        return updateTrackElapsed;
    }
    
    @Override
    public String toString() {
        return "Got track's elapsed time update - seconds: " + this.elapsed;
    }
}
