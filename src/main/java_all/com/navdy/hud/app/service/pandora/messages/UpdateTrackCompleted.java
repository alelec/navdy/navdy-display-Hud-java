package com.navdy.hud.app.service.pandora.messages;

import com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException;
import com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException;

public class UpdateTrackCompleted extends BaseIncomingOneIntMessage
{
    public UpdateTrackCompleted(final int n) {
        super(n);
    }
    
    protected static BaseIncomingMessage innerBuildFromPayload(final byte[] array) throws MessageWrongWayException, CorruptedPayloadException {
        return new UpdateTrackCompleted(BaseIncomingOneIntMessage.parseIntValue(array));
    }
    
    @Override
    public String toString() {
        return "Track ended - token: " + this.value;
    }
}
