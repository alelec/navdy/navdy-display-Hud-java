package com.navdy.hud.app.service.pandora.messages;

import com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException;
import com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException;

public class UpdateTrack extends BaseIncomingOneIntMessage
{
    public UpdateTrack(final int n) {
        super(n);
    }
    
    protected static BaseIncomingMessage innerBuildFromPayload(final byte[] array) throws MessageWrongWayException, CorruptedPayloadException {
        return new UpdateTrack(BaseIncomingOneIntMessage.parseIntValue(array));
    }
    
    @Override
    public String toString() {
        return "New track with token: " + this.value;
    }
}
