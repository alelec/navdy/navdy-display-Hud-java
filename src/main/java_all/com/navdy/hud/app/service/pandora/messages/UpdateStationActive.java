package com.navdy.hud.app.service.pandora.messages;

import com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException;
import com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException;

public class UpdateStationActive extends BaseIncomingOneIntMessage
{
    public UpdateStationActive(final int n) {
        super(n);
    }
    
    protected static BaseIncomingMessage innerBuildFromPayload(final byte[] array) throws MessageWrongWayException, CorruptedPayloadException {
        return new UpdateStationActive(BaseIncomingOneIntMessage.parseIntValue(array));
    }
    
    @Override
    public String toString() {
        return "New active station with token: " + this.value;
    }
}
