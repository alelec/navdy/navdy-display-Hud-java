package com.navdy.hud.app.service;

import com.squareup.otto.Subscribe;
import com.navdy.service.library.events.navigation.NavigationRouteResult;
import com.here.android.mpa.search.Location;
import java.util.Iterator;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.search.Place;
import com.navdy.hud.app.maps.here.HerePlacesManager;
import android.os.SystemClock;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.framework.fuel.FuelRoutingManager;
import com.navdy.service.library.events.glances.FuelConstants;
import com.navdy.hud.app.framework.glance.GlanceHelper;
import com.navdy.service.library.events.glances.CalendarConstants;
import java.util.concurrent.TimeUnit;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.framework.connection.ConnectionNotification;
import com.navdy.hud.app.maps.MapEvents;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.events.ui.ShowScreen;
import com.navdy.hud.app.device.dial.DialNotification;
import com.navdy.service.library.events.glances.MessageConstants;
import com.navdy.service.library.events.callcontrol.PhoneBatteryStatus;
import com.navdy.service.library.events.callcontrol.PhoneStatus;
import com.navdy.service.library.events.callcontrol.PhoneEvent;
import java.util.List;
import java.util.UUID;
import com.navdy.service.library.events.glances.GlanceEvent;
import com.navdy.service.library.events.glances.GlanceIconConstants;
import com.navdy.service.library.events.glances.GenericConstants;
import com.navdy.service.library.events.glances.KeyValue;
import java.util.ArrayList;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.service.library.events.notification.ShowCustomNotification;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;

public class CustomNotificationServiceHandler
{
    private static final Logger sLogger;
    Bus bus;
    
    static {
        sLogger = new Logger(CustomNotificationServiceHandler.class);
    }
    
    public CustomNotificationServiceHandler(final Bus bus) {
        (this.bus = bus).register(this);
    }
    
    @Subscribe
    public void onShowCustomNotification(final ShowCustomNotification showCustomNotification) {
        CustomNotificationServiceHandler.sLogger.v("onShowCustomNotification: " + showCustomNotification.id);
        NotificationManager.getInstance();
        final String id = showCustomNotification.id;
        switch (id) {
            case "GENERIC_1": {
                final ArrayList<KeyValue> list = new ArrayList<KeyValue>();
                list.add(new KeyValue(GenericConstants.GENERIC_TITLE.name(), "John Doe"));
                list.add(new KeyValue(GenericConstants.GENERIC_MESSAGE.name(), "Message Test"));
                list.add(new KeyValue(GenericConstants.GENERIC_MAIN_ICON.name(), GlanceIconConstants.GLANCE_ICON_NAVDY_MAIN.name()));
                list.add(new KeyValue(GenericConstants.GENERIC_SIDE_ICON.name(), GlanceIconConstants.GLANCE_ICON_MESSAGE_SIDE_BLUE.name()));
                this.bus.post(new GlanceEvent.Builder().glanceType(GlanceEvent.GlanceType.GLANCE_TYPE_GENERIC).id(UUID.randomUUID().toString()).postTime(System.currentTimeMillis()).provider("blah").glanceData(list).build());
                break;
            }
            case "GENERIC_2": {
                final ArrayList<KeyValue> list2 = new ArrayList<KeyValue>();
                list2.add(new KeyValue(GenericConstants.GENERIC_TITLE.name(), "Jone Doe"));
                list2.add(new KeyValue(GenericConstants.GENERIC_MESSAGE.name(), "Message Test"));
                this.bus.post(new GlanceEvent.Builder().glanceType(GlanceEvent.GlanceType.GLANCE_TYPE_GENERIC).id(UUID.randomUUID().toString()).postTime(System.currentTimeMillis()).provider("blah").glanceData(list2).build());
                break;
            }
            case "INCOMING_PHONE_CALL_323":
                this.bus.post(new PhoneEvent.Builder().number("323-222-1111").status(PhoneStatus.PHONE_RINGING).contact_name("Al Jazeera Al Bin Al Salaad").build());
                break;
            case "END_PHONE_CALL_323":
                this.bus.post(new PhoneEvent.Builder().number("323-222-1111").status(PhoneStatus.PHONE_IDLE).contact_name("Al Jazeera").build());
                break;
            case "PHONE_BATTERY_LOW":
                this.bus.post(new PhoneBatteryStatus(PhoneBatteryStatus.BatteryStatus.BATTERY_LOW, 12, false));
                break;
            case "PHONE_BATTERY_EXTREMELY_LOW":
                this.bus.post(new PhoneBatteryStatus(PhoneBatteryStatus.BatteryStatus.BATTERY_EXTREMELY_LOW, 4, false));
                break;
            case "PHONE_BATTERY_OK":
                this.bus.post(new PhoneBatteryStatus(PhoneBatteryStatus.BatteryStatus.BATTERY_OK, 36, false));
                break;
            case "TEXT_NOTIFICAION_WITH_REPLY_408": {
                final ArrayList<GlanceEvent.GlanceActions> list3 = new ArrayList<GlanceEvent.GlanceActions>(1);
                list3.add(GlanceEvent.GlanceActions.REPLY);
                final ArrayList<KeyValue> list4 = new ArrayList<KeyValue>();
                list4.add(new KeyValue(MessageConstants.MESSAGE_FROM.name(), "John Doe"));
                list4.add(new KeyValue(MessageConstants.MESSAGE_FROM_NUMBER.name(), "4081111111"));
                list4.add(new KeyValue(MessageConstants.MESSAGE_BODY.name(), "Hi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing"));
                list4.add(new KeyValue(MessageConstants.MESSAGE_IS_SMS.name(), ""));
                this.bus.post(new GlanceEvent.Builder().glanceType(GlanceEvent.GlanceType.GLANCE_TYPE_MESSAGE).id(UUID.randomUUID().toString()).postTime(System.currentTimeMillis()).provider("com.navdy.sms").actions(list3).glanceData(list4).build());
                break;
            }
            case "TEXT_NOTIFICAION_WITH_NO_REPLY_510": {
                final ArrayList<KeyValue> list5 = new ArrayList<KeyValue>();
                list5.add(new KeyValue(MessageConstants.MESSAGE_FROM.name(), "Santa Singh"));
                list5.add(new KeyValue(MessageConstants.MESSAGE_FROM_NUMBER.name(), "999-999-0999"));
                list5.add(new KeyValue(MessageConstants.MESSAGE_BODY.name(), "Hi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doinghow are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doinghow are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing"));
                list5.add(new KeyValue(MessageConstants.MESSAGE_IS_SMS.name(), ""));
                this.bus.post(new GlanceEvent.Builder().glanceType(GlanceEvent.GlanceType.GLANCE_TYPE_MESSAGE).id(UUID.randomUUID().toString()).postTime(System.currentTimeMillis()).provider("com.navdy.sms").glanceData(list5).build());
                break;
            }
            case "TEXT_NOTIFICAION_WITH_REPLY_999": {
                final ArrayList<GlanceEvent.GlanceActions> list6 = new ArrayList<GlanceEvent.GlanceActions>(1);
                list6.add(GlanceEvent.GlanceActions.REPLY);
                final ArrayList<KeyValue> list7 = new ArrayList<KeyValue>();
                list7.add(new KeyValue(MessageConstants.MESSAGE_FROM.name(), "Barack Obama"));
                list7.add(new KeyValue(MessageConstants.MESSAGE_FROM_NUMBER.name(), "9999999999"));
                list7.add(new KeyValue(MessageConstants.MESSAGE_BODY.name(), "Hi how are u doing"));
                list7.add(new KeyValue(MessageConstants.MESSAGE_IS_SMS.name(), ""));
                this.bus.post(new GlanceEvent.Builder().glanceType(GlanceEvent.GlanceType.GLANCE_TYPE_MESSAGE).id(UUID.randomUUID().toString()).postTime(System.currentTimeMillis()).provider("com.navdy.sms").actions(list6).glanceData(list7).build());
                break;
            }
            case "DIAL_CONNECTED":
                DialNotification.showConnectedToast();
                break;
            case "DIAL_DISCONNECTED":
                DialNotification.showDisconnectedToast("Navdy Dial (test)");
                break;
            case "DIAL_FORGOTTEN_SINGLE":
                DialNotification.showForgottenToast(false, "Navdy Dial (test)");
                break;
            case "DIAL_FORGOTTEN_MULTIPLE":
                DialNotification.showForgottenToast(true, "Navdy Dial (AAAA), Navdy Dial (BBBB)");
                break;
            case "DIAL_BATTERY_LOW":
                DialNotification.showLowBatteryToast();
                break;
            case "DIAL_BATTERY_EXTREMELY_LOW":
                DialNotification.showExtremelyLowBatteryToast();
                break;
            case "DIAL_BATTERY_VERY_LOW":
                DialNotification.showVeryLowBatteryToast();
                break;
            case "DIAL_BATTERY_OK":
                DialNotification.dismissAllBatteryToasts();
                break;
            case "MUSIC":
                this.bus.post(new ShowScreen.Builder().screen(Screen.SCREEN_MUSIC).build());
                break;
            case "VOICE_ASSIST":
                this.bus.post(new ShowScreen.Builder().screen(Screen.SCREEN_VOICE_CONTROL).build());
                break;
            case "BRIGHTNESS":
                this.bus.post(new ShowScreen.Builder().screen(Screen.SCREEN_BRIGHTNESS).build());
                break;
            case "TRAFFIC_REROUTE":
                this.bus.post(new MapEvents.TrafficRerouteEvent("Heaven", "Heaven, Hell", 1500L, System.currentTimeMillis() + 300000L, 10000L, 0L));
                break;
            case "CLEAR_TRAFFIC_REROUTE":
                this.bus.post(new MapEvents.TrafficRerouteDismissEvent());
                break;
            case "TRAFFIC_JAM":
                this.bus.post(new MapEvents.TrafficJamProgressEvent(900));
                break;
            case "CLEAR_TRAFFIC_JAM":
                this.bus.post(new MapEvents.TrafficJamDismissEvent());
                break;
            case "TRAFFIC_INCIDENT":
                this.bus.post(new MapEvents.LiveTrafficEvent(MapEvents.LiveTrafficEvent.Type.INCIDENT, MapEvents.LiveTrafficEvent.Severity.HIGH));
                break;
            case "CLEAR_TRAFFIC_INCIDENT":
                this.bus.post(new MapEvents.LiveTrafficDismissEvent());
                break;
            case "TRAFFIC_DELAY":
                this.bus.post(new MapEvents.TrafficDelayEvent(420L));
                break;
            case "CLEAR_TRAFFIC_DELAY":
                this.bus.post(new MapEvents.TrafficDelayDismissEvent());
                break;
            case "PHONE_DISCONNECTED":
                ConnectionNotification.showDisconnectedToast(false);
                break;
            case "PHONE_CONNECTED":
                ConnectionNotification.showConnectedToast();
                break;
            case "PHONE_APP_DISCONNECTED":
                ConnectionNotification.showDisconnectedToast(true);
                break;
            case "CLEAR_CURRENT_TOAST":
                ToastManager.getInstance().dismissCurrentToast();
                break;
            case "CLEAR_ALL_TOAST":
                ToastManager.getInstance().clearAllPendingToast();
                break;
            case "CLEAR_ALL_TOAST_AND_CURRENT":
                ToastManager.getInstance().clearAllPendingToast();
                ToastManager.getInstance().dismissCurrentToast();
                break;
            case "GOOGLE_CALENDAR_1": {
                final long currentTimeMillis = System.currentTimeMillis();
                final long millis = TimeUnit.MINUTES.toMillis(10L);
                final ArrayList<KeyValue> list8 = new ArrayList<KeyValue>();
                list8.add(new KeyValue(CalendarConstants.CALENDAR_TITLE.name(), "Meeting with Obama"));
                list8.add(new KeyValue(CalendarConstants.CALENDAR_TIME_STR.name(), "\u200e\u202a12:00 – 12:10 PM\u202c\u200e"));
                list8.add(new KeyValue(CalendarConstants.CALENDAR_LOCATION.name(), "575 7th Street, San Francisco, CA 94103"));
                this.bus.post(new GlanceEvent.Builder().glanceType(GlanceEvent.GlanceType.GLANCE_TYPE_CALENDAR).id(UUID.randomUUID().toString()).postTime(currentTimeMillis + millis).provider("com.google.android.calendar").glanceData(list8).build());
                break;
            }
            case "APPLE_CALENDAR_1": {
                final long currentTimeMillis2 = System.currentTimeMillis();
                final ArrayList<KeyValue> list9 = new ArrayList<KeyValue>();
                list9.add(new KeyValue(CalendarConstants.CALENDAR_TITLE.name(), "Huddle"));
                list9.add(new KeyValue(CalendarConstants.CALENDAR_TIME.name(), String.valueOf(currentTimeMillis2)));
                list9.add(new KeyValue(CalendarConstants.CALENDAR_TIME_STR.name(), "1:30 pm - 1:45 pm"));
                this.bus.post(new GlanceEvent.Builder().glanceType(GlanceEvent.GlanceType.GLANCE_TYPE_CALENDAR).id(UUID.randomUUID().toString()).postTime(currentTimeMillis2).provider("com.apple.mobilecal").glanceData(list9).build());
                break;
            }
            case "LOW_FUEL_LEVEL":
                if (GlanceHelper.isFuelNotificationEnabled()) {
                    final ArrayList<KeyValue> list10 = new ArrayList<KeyValue>();
                    list10.add(new KeyValue(FuelConstants.FUEL_LEVEL.name(), "14"));
                    this.bus.post(new GlanceEvent.Builder().glanceType(GlanceEvent.GlanceType.GLANCE_TYPE_FUEL).id(UUID.randomUUID().toString()).postTime(System.currentTimeMillis()).provider("com.navdy.fuel").glanceData(list10).build());
                    break;
                }
                break;
            case "LOW_FUEL_LEVEL_NOCHECK": {
                final ArrayList<KeyValue> list11 = new ArrayList<KeyValue>();
                list11.add(new KeyValue(FuelConstants.FUEL_LEVEL.name(), "14"));
                this.bus.post(new GlanceEvent.Builder().glanceType(GlanceEvent.GlanceType.GLANCE_TYPE_FUEL).id(UUID.randomUUID().toString()).postTime(System.currentTimeMillis()).provider("com.navdy.fuel").glanceData(list11).build());
                break;
            }
            case "LOW_FUEL_LEVEL_CLEAR":
                NotificationManager.getInstance().removeNotification(FuelRoutingManager.LOW_FUEL_ID);
                break;
            case "OBD_LOW_FUEL_LEVEL":
                this.bus.post(new FuelRoutingManager.TestObdLowFuelLevel());
                break;
            case "CLEAR_OBD_LOW_FUEL_LEVEL":
                this.bus.post(new FuelRoutingManager.ClearTestObdLowFuelLevel());
                break;
            case "FUEL_ADDED_TEST":
                this.bus.post(new FuelRoutingManager.FuelAddedTestEvent());
                break;
            case "FIND_GAS_STATION":
                Label_3147: {
                    try {
                        if (HereMapsManager.getInstance().isInitialized()) {
                            break Label_3147;
                        }
                        CustomNotificationServiceHandler.sLogger.v("FIND_GAS_STATION: here maps not initialized");
                    }
                    catch (Throwable t) {
                        CustomNotificationServiceHandler.sLogger.e("FIND_GAS_STATION", t);
                    }
                    break;
                }
                HerePlacesManager.handleCategoriesRequest(FuelRoutingManager.GAS_CATEGORY, 3, (HerePlacesManager.OnCategoriesSearchListener)new HerePlacesManager.OnCategoriesSearchListener() {
                    final /* synthetic */ long val$t1 = SystemClock.elapsedRealtime();
                    
                    @Override
                    public void onCompleted(final List<Place> list) {
                        while (true) {
                            int n;
                            GeoCoordinate geoCoordinate;
                            try {
                                CustomNotificationServiceHandler.sLogger.v("FIND_GAS_STATION: found gas station (" + list.size() + ") time to find [" + (SystemClock.elapsedRealtime() - this.val$t1) + "]");
                                n = 1;
                                geoCoordinate = HereMapsManager.getInstance().getRouteStartPoint();
                                if (geoCoordinate == null) {
                                    geoCoordinate = HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
                                }
                                else {
                                    CustomNotificationServiceHandler.sLogger.v("using debug start point:" + geoCoordinate);
                                }
                                if (geoCoordinate == null) {
                                    CustomNotificationServiceHandler.sLogger.e("FIND_GAS_STATION no current position");
                                    return;
                                }
                            }
                            catch (Throwable t) {
                                CustomNotificationServiceHandler.sLogger.e("FIND_GAS_STATION", t);
                                return;
                            }
                            for (final Place place : list) {
                                final Location location = place.getLocation();
                                final GeoCoordinate placeEntry = HerePlacesManager.getPlaceEntry(place);
                                CustomNotificationServiceHandler.sLogger.v("FIND_GAS_STATION [" + n + "]" + " name [" + place.getName() + "]" + " address [" + location.getAddress().toString().replace("<br/>", ", ").replace("\n", "") + "]" + " distance [" + (int)geoCoordinate.distanceTo(placeEntry) + "] meters" + " displayPos [" + location.getCoordinate() + "]" + " navPos [" + placeEntry + "]");
                                ++n;
                            }
                        }
                    }
                    
                    @Override
                    public void onError(final Error error) {
                        CustomNotificationServiceHandler.sLogger.v("FIND_GAS_STATION: could not find gas station:" + error + " time to error [" + (SystemClock.elapsedRealtime() - this.val$t1) + "]");
                    }
                });
                break;
            case "FIND_ROUTE_TO_CLOSEST_GAS_STATION":
                Label_3210: {
                    try {
                        if (HereMapsManager.getInstance().isInitialized()) {
                            break Label_3210;
                        }
                        CustomNotificationServiceHandler.sLogger.v("FIND_ROUTE_TO_CLOSEST_GAS_STATION: here maps not initialized");
                    }
                    catch (Throwable t2) {
                        CustomNotificationServiceHandler.sLogger.e("FIND_ROUTE_TO_CLOSEST_GAS_STATION", t2);
                    }
                    break;
                }
                FuelRoutingManager.getInstance().findNearestGasStation((FuelRoutingManager.OnNearestGasStationCallback)new FuelRoutingManager.OnNearestGasStationCallback() {
                    final /* synthetic */ long val$t1 = SystemClock.elapsedRealtime();
                    
                    @Override
                    public void onComplete(final NavigationRouteResult navigationRouteResult) {
                        CustomNotificationServiceHandler.sLogger.v("FIND_ROUTE_TO_CLOSEST_GAS_STATION: success: time  [" + (SystemClock.elapsedRealtime() - this.val$t1) + "]");
                        final Logger access$000 = CustomNotificationServiceHandler.sLogger;
                        final StringBuilder append = new StringBuilder().append("FIND_ROUTE_TO_CLOSEST_GAS_STATION route id[").append(navigationRouteResult.routeId).append("] label[").append(navigationRouteResult.label).append("] via[").append(navigationRouteResult.via).append("] address[");
                        String replace;
                        if (navigationRouteResult.address != null) {
                            replace = navigationRouteResult.address.replace("\n", "");
                        }
                        else {
                            replace = "null";
                        }
                        access$000.v(append.append(replace).append("] length[").append(navigationRouteResult.length).append("] duration[").append(navigationRouteResult.duration_traffic).append("] freeFlowDuration[").append(navigationRouteResult.duration).append("]").toString());
                    }
                    
                    @Override
                    public void onError(final Error error) {
                        CustomNotificationServiceHandler.sLogger.v("FIND_ROUTE_TO_CLOSEST_GAS_STATION: failed:" + error + " time to error [" + (SystemClock.elapsedRealtime() - this.val$t1) + "]");
                    }
                });
                break;
        }
    }
    
    public void start() {
    }
}
