package com.navdy.hud.app.storage.db.helper;

import java.util.Iterator;
import android.database.sqlite.SQLiteStatement;
import com.navdy.service.library.events.photo.PhotoType;
import android.text.TextUtils;
import com.navdy.hud.app.framework.contacts.PhoneImageDownloader;
import com.navdy.hud.app.framework.contacts.FavoriteContactsManager;
import android.database.Cursor;
import com.navdy.hud.app.framework.contacts.NumberType;
import java.util.ArrayList;
import com.navdy.hud.app.framework.contacts.ContactImageHelper;
import java.io.Closeable;
import com.navdy.service.library.util.IOUtils;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.util.GenericUtil;
import android.database.sqlite.SQLiteDatabase;
import com.navdy.hud.app.storage.db.DatabaseUtil;
import com.navdy.hud.app.storage.db.HudDatabase;
import java.util.LinkedList;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.framework.contacts.Contact;
import java.util.List;

public class FavoriteContactsPersistenceHelper
{
    private static final String BULK_INSERT_SQL = "INSERT INTO fav_contacts(device_id,name,number,number_type,def_image,number_numeric) VALUES (?,?,?,?,?,?)";
    private static final int DEFAULT_IMAGE_INDEX_ORDINAL = 3;
    private static final List<Contact> EMPTY_LIST;
    private static final String[] FAV_CONTACT_ARGS;
    private static final String FAV_CONTACT_ORDER_BY = "rowid";
    private static final String[] FAV_CONTACT_PROJECTION;
    private static final String FAV_CONTACT_WHERE = "device_id=?";
    private static final int NAME_ORDINAL = 0;
    private static final int NUMBER_NUMERIC_ORDINAL = 4;
    private static final int NUMBER_ORDINAL = 1;
    private static final int NUMBER_TYPE_ORDINAL = 2;
    private static final Object lockObj;
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(FavoriteContactsPersistenceHelper.class);
        lockObj = new Object();
        EMPTY_LIST = new LinkedList<Contact>();
        FAV_CONTACT_PROJECTION = new String[] { "name", "number", "number_type", "def_image", "number_numeric" };
        FAV_CONTACT_ARGS = new String[1];
    }
    
    private static void deleteFavoriteContacts(final String s) {
        SQLiteDatabase writableDatabase = null;
        Label_0043: {
            try {
                final Object lockObj = FavoriteContactsPersistenceHelper.lockObj;
                synchronized (lockObj) {
                    writableDatabase = HudDatabase.getInstance().getWritableDatabase();
                    if (writableDatabase == null) {
                        throw new DatabaseUtil.DatabaseNotAvailable();
                    }
                    break Label_0043;
                }
            }
            catch (Throwable t) {
                FavoriteContactsPersistenceHelper.sLogger.e(t);
            }
            return;
        }
        final String s2;
        FavoriteContactsPersistenceHelper.FAV_CONTACT_ARGS[0] = s2;
        FavoriteContactsPersistenceHelper.sLogger.v("fav-contact rows deleted:" + writableDatabase.delete("fav_contacts", "device_id=?", FavoriteContactsPersistenceHelper.FAV_CONTACT_ARGS));
    }
    // monitorexit(o)
    
    public static List<Contact> getFavoriteContacts(final String s) {
        GenericUtil.checkNotOnMainThread();
        final List<Contact> empty_LIST;
        if (DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile()) {
            empty_LIST = FavoriteContactsPersistenceHelper.EMPTY_LIST;
        }
        else {
            final Object lockObj = FavoriteContactsPersistenceHelper.lockObj;
            final SQLiteDatabase writableDatabase;
            synchronized (lockObj) {
                writableDatabase = HudDatabase.getInstance().getWritableDatabase();
                if (writableDatabase == null) {
                    throw new DatabaseUtil.DatabaseNotAvailable();
                }
            }
            final String s2;
            FavoriteContactsPersistenceHelper.FAV_CONTACT_ARGS[0] = s2;
            final Cursor query = writableDatabase.query("fav_contacts", FavoriteContactsPersistenceHelper.FAV_CONTACT_PROJECTION, "device_id=?", FavoriteContactsPersistenceHelper.FAV_CONTACT_ARGS, (String)null, (String)null, "rowid");
            Label_0093: {
                if (query == null) {
                    break Label_0093;
                }
                try {
                    if (!query.moveToFirst()) {
                        final List<Contact> empty_LIST2 = FavoriteContactsPersistenceHelper.EMPTY_LIST;
                        IOUtils.closeStream((Closeable)query);
                    }
                    // monitorexit(o)
                    else {
                        ContactImageHelper.getInstance();
                        final ArrayList<Contact> list = new ArrayList<Contact>();
                        do {
                            list.add(new Contact(query.getString(0), query.getString(1), NumberType.buildFromValue(query.getInt(2)), query.getInt(3), query.getLong(4)));
                        } while (query.moveToNext());
                        IOUtils.closeStream((Closeable)query);
                    }
                    // monitorexit(o)
                }
                finally {
                    IOUtils.closeStream((Closeable)query);
                }
            }
        }
        return empty_LIST;
    }
    
    public static void storeFavoriteContacts(final String s, final List<Contact> favoriteContacts, final boolean b) {
        GenericUtil.checkNotOnMainThread();
        FavoriteContactsPersistenceHelper.sLogger.v("fav-contacts passed [" + favoriteContacts.size() + "]");
        final FavoriteContactsManager instance = FavoriteContactsManager.getInstance();
        deleteFavoriteContacts(s);
        if (favoriteContacts.size() == 0) {
            instance.setFavoriteContacts(null);
        }
        else {
            PhoneImageDownloader.getInstance().clearAllPhotoCheckEntries();
            final Object lockObj = FavoriteContactsPersistenceHelper.lockObj;
            final SQLiteDatabase writableDatabase;
            synchronized (lockObj) {
                writableDatabase = HudDatabase.getInstance().getWritableDatabase();
                if (writableDatabase == null) {
                    throw new DatabaseUtil.DatabaseNotAvailable();
                }
            }
            int n = 0;
        Label_0191_Outer:
            while (true) {
                final SQLiteStatement compileStatement = writableDatabase.compileStatement("INSERT INTO fav_contacts(device_id,name,number,number_type,def_image,number_numeric) VALUES (?,?,?,?,?,?)");
                n = 0;
                while (true) {
                    Label_0312: {
                        try {
                            final ContactImageHelper instance2 = ContactImageHelper.getInstance();
                            writableDatabase.beginTransaction();
                            for (final Contact contact : favoriteContacts) {
                                compileStatement.clearBindings();
                                final String s2;
                                compileStatement.bindString(1, s2);
                                if (TextUtils.isEmpty((CharSequence)contact.name)) {
                                    break Label_0312;
                                }
                                compileStatement.bindString(2, contact.name);
                                compileStatement.bindString(3, contact.number);
                                compileStatement.bindLong(4, (long)contact.numberType.getValue());
                                contact.defaultImageIndex = instance2.getContactImageIndex(contact.number);
                                compileStatement.bindLong(5, (long)contact.defaultImageIndex);
                                compileStatement.bindLong(6, contact.numericNumber);
                                compileStatement.execute();
                                final int n2 = ++n;
                                if (!b) {
                                    continue Label_0191_Outer;
                                }
                                PhoneImageDownloader.getInstance().submitDownload(contact.number, PhoneImageDownloader.Priority.NORMAL, PhotoType.PHOTO_CONTACT, contact.name);
                                n = n2;
                            }
                            break;
                        }
                        finally {
                            writableDatabase.endTransaction();
                        }
                    }
                    compileStatement.bindNull(2);
                    continue;
                }
            }
            writableDatabase.setTransactionSuccessful();
            FavoriteContactsManager.getInstance().setFavoriteContacts(favoriteContacts);
            writableDatabase.endTransaction();
            FavoriteContactsPersistenceHelper.sLogger.v("fav-contacts rows added:" + n);
        }
        // monitorexit(o)
    }
}
