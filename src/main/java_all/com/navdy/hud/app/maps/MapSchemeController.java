package com.navdy.hud.app.maps;

import android.hardware.SensorEvent;
import java.util.List;
import android.os.Message;
import android.os.Looper;
import mortar.Mortar;
import com.navdy.hud.app.HudApplication;
import javax.inject.Inject;
import android.content.SharedPreferences;
import android.hardware.SensorManager;
import android.os.Handler;
import android.hardware.Sensor;
import android.content.Context;
import com.navdy.service.library.log.Logger;
import android.hardware.SensorEventListener;

public class MapSchemeController implements SensorEventListener
{
    private static final int AVERAGING_PERIOD = 300;
    private static final long DELAY_MILLIS = 10000L;
    private static final float HI_THRESHOLD = 600.0f;
    private static final float LO_THRESHOLD = 200.0f;
    public static final String MAP_SCHEME_CARNAV_DAY = "carnav.day";
    public static final String MAP_SCHEME_CARNAV_NIGHT = "carnav.day";
    public static final int MESSAGE_PERIODIC = 0;
    public static final Logger sLogger;
    private static final MapSchemeController singleton;
    private Context context;
    private int currentIdx;
    private float lastSensorValue;
    private Sensor light;
    private Handler myHandler;
    private boolean ringBufferFull;
    private SensorManager sensorManager;
    private float[] sensorValues;
    @Inject
    SharedPreferences sharedPreferences;
    
    static {
        sLogger = new Logger(MapSchemeController.class);
        singleton = new MapSchemeController();
    }
    
    public MapSchemeController() {
        this.lastSensorValue = 400.0f;
        this.sensorValues = new float[30];
        this.currentIdx = 0;
        this.ringBufferFull = false;
        Mortar.inject(this.context = HudApplication.getAppContext(), this);
        this.sensorManager = (SensorManager)this.context.getSystemService("sensor");
        final List sensorList = this.sensorManager.getSensorList(-1);
        for (int i = 0; i < sensorList.size(); ++i) {
            final Sensor light = sensorList.get(i);
            MapSchemeController.sLogger.d("sensor: " + light.toString());
            if (light.getName().equals("tsl2572")) {
                this.light = light;
            }
        }
        MapSchemeController.sLogger.d("light sensor selected: " + this.light);
        this.sensorManager.registerListener((SensorEventListener)this, this.light, 3);
        (this.myHandler = new Handler(Looper.getMainLooper()) {
            public void handleMessage(final Message message) {
                if (message.what == 0) {
                    MapSchemeController.this.collectLightSensorSample();
                    MapSchemeController.this.evaluateCollectedSamples();
                    this.sendEmptyMessageDelayed(0, 10000L);
                }
            }
        }).sendEmptyMessageDelayed(0, 10000L);
    }
    
    private void collectLightSensorSample() {
        this.sensorValues[this.currentIdx] = this.lastSensorValue;
        if (this.currentIdx == this.sensorValues.length - 1) {
            this.ringBufferFull = true;
        }
        this.currentIdx = (this.currentIdx + 1) % this.sensorValues.length;
    }
    
    private void evaluateCollectedSamples() {
        float n = 0.0f;
        int n2;
        if (this.ringBufferFull) {
            n2 = this.sensorValues.length;
        }
        else {
            n2 = this.currentIdx;
        }
        for (int i = 0; i < n2; ++i) {
            n += this.sensorValues[i];
        }
        final float n3 = n / n2;
        MapSchemeController.sLogger.d("evaluateCollectedSamples: avg lux: " + n3);
        if (this.getActiveScheme() == MapScheme.DAY && n3 < 200.0f) {
            this.switchMapScheme(MapScheme.NIGHT);
        }
        else if (this.getActiveScheme() == MapScheme.NIGHT && n3 > 600.0f) {
            this.switchMapScheme(MapScheme.DAY);
        }
    }
    
    public static MapSchemeController getInstance() {
        return MapSchemeController.singleton;
    }
    
    private void switchMapScheme(final MapScheme mapScheme) {
        final String string = this.sharedPreferences.getString("map.scheme", "");
        String s;
        if (string.equals("carnav.day") && mapScheme == MapScheme.NIGHT) {
            s = "carnav.day";
        }
        else {
            s = string;
            if (string.equals("carnav.day")) {
                s = string;
                if (mapScheme == MapScheme.DAY) {
                    s = "carnav.day";
                }
            }
        }
        MapSchemeController.sLogger.i("changing map scheme to " + s);
        this.sharedPreferences.edit().putString("map.scheme", s).apply();
    }
    
    MapScheme getActiveScheme() {
        MapScheme mapScheme;
        if (this.sharedPreferences.getString("map.scheme", "").equals("carnav.day")) {
            mapScheme = MapScheme.DAY;
        }
        else {
            mapScheme = MapScheme.NIGHT;
        }
        return mapScheme;
    }
    
    public void onAccuracyChanged(final Sensor sensor, final int n) {
        MapSchemeController.sLogger.d("onAccuracyChanged: accuracy: " + n);
    }
    
    public final void onSensorChanged(final SensorEvent sensorEvent) {
        final float lastSensorValue = sensorEvent.values[0];
        if (MapSchemeController.sLogger.isLoggable(2)) {
            MapSchemeController.sLogger.d("onSensorChanged: lux: " + lastSensorValue);
        }
        this.lastSensorValue = lastSensorValue;
    }
    
    enum MapScheme
    {
        DAY, 
        NIGHT;
    }
}
