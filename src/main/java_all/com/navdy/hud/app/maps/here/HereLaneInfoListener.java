package com.navdy.hud.app.maps.here;

import java.util.AbstractCollection;
import java.util.Iterator;
import java.util.Collection;
import com.here.android.mpa.common.RoadElement;
import java.util.List;
import com.here.android.mpa.routing.Maneuver;
import java.util.EnumSet;
import java.lang.ref.WeakReference;
import android.graphics.drawable.Drawable;
import com.here.android.mpa.guidance.LaneInformation;
import java.util.ArrayList;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.maps.MapEvents;
import com.here.android.mpa.guidance.NavigationManager;

public class HereLaneInfoListener extends LaneInformationListener
{
    private static final MapEvents.LaneData EMPTY_LANE_DATA;
    private static final MapEvents.HideTrafficLaneInfo HIDE_LANE_INFO;
    private static Logger sLogger;
    private Bus bus;
    private MapEvents.DisplayTrafficLaneInfo lastLanesData;
    private HereNavController navController;
    private volatile boolean running;
    private Runnable startRunnable;
    private Runnable stopRunnable;
    private TaskManager taskManager;
    private ArrayList<LaneInformation.Direction> tempDirections;
    private ArrayList<LaneInformation.Direction> tempDirections2;
    private ArrayList<MapEvents.LaneData> tempLanes;
    
    static {
        HereLaneInfoListener.sLogger = new Logger(HereLaneInfoListener.class);
        HIDE_LANE_INFO = new MapEvents.HideTrafficLaneInfo();
        EMPTY_LANE_DATA = new MapEvents.LaneData(MapEvents.LaneData.Position.OFF_ROUTE, null);
    }
    
    HereLaneInfoListener(final Bus bus, final HereNavController navController) {
        this.taskManager = TaskManager.getInstance();
        this.tempDirections = new ArrayList<LaneInformation.Direction>();
        this.tempDirections2 = new ArrayList<LaneInformation.Direction>();
        this.tempLanes = new ArrayList<MapEvents.LaneData>();
        this.startRunnable = new Runnable() {
            @Override
            public void run() {
                HereLaneInfoListener.this.navController.addLaneInfoListener(new WeakReference<LaneInformationListener>(HereLaneInfoListener.this));
                HereLaneInfoListener.sLogger.v("added lane info listener");
            }
        };
        this.stopRunnable = new Runnable() {
            @Override
            public void run() {
                HereLaneInfoListener.this.navController.removeLaneInfoListener(HereLaneInfoListener.this);
                HereLaneInfoListener.sLogger.v("removed lane info listener");
            }
        };
        HereLaneInfoListener.sLogger.v("ctor");
        this.bus = bus;
        this.navController = navController;
    }
    
    private LaneInformation.Direction getOnRouteDirectionFromManeuver(final EnumSet<LaneInformation.Direction> set) {
        final LaneInformation.Direction direction = null;
        Enum<LaneInformation.Direction> enum1;
        try {
            final Maneuver nextManeuver = HereNavigationManager.getInstance().getNextManeuver();
            if (nextManeuver == null) {
                enum1 = direction;
            }
            else {
                final Maneuver.Turn turn = nextManeuver.getTurn();
                enum1 = direction;
                if (turn != null) {
                    switch (turn) {
                        default:
                            enum1 = direction;
                            break;
                        case KEEP_RIGHT:
                            if (set.contains(LaneInformation.Direction.RIGHT)) {
                                enum1 = LaneInformation.Direction.RIGHT;
                                break;
                            }
                            enum1 = direction;
                            if (HereLaneInfoBuilder.getNumDirections(set, HereLaneInfoBuilder.DirectionType.RIGHT) == 1) {
                                enum1 = HereLaneInfoBuilder.getDirection(set, HereLaneInfoBuilder.DirectionType.RIGHT);
                                break;
                            }
                            break;
                        case LIGHT_RIGHT:
                            if (set.contains(LaneInformation.Direction.SLIGHTLY_RIGHT)) {
                                enum1 = LaneInformation.Direction.SLIGHTLY_RIGHT;
                                break;
                            }
                            enum1 = direction;
                            if (HereLaneInfoBuilder.getNumDirections(set, HereLaneInfoBuilder.DirectionType.RIGHT) == 1) {
                                enum1 = HereLaneInfoBuilder.getDirection(set, HereLaneInfoBuilder.DirectionType.RIGHT);
                                break;
                            }
                            break;
                        case QUITE_RIGHT:
                        case HEAVY_RIGHT:
                            if (set.contains(LaneInformation.Direction.SHARP_RIGHT)) {
                                enum1 = LaneInformation.Direction.SHARP_RIGHT;
                                break;
                            }
                            enum1 = direction;
                            if (HereLaneInfoBuilder.getNumDirections(set, HereLaneInfoBuilder.DirectionType.RIGHT) == 1) {
                                enum1 = HereLaneInfoBuilder.getDirection(set, HereLaneInfoBuilder.DirectionType.RIGHT);
                                break;
                            }
                            break;
                        case KEEP_LEFT:
                            if (set.contains(LaneInformation.Direction.LEFT)) {
                                enum1 = LaneInformation.Direction.LEFT;
                                break;
                            }
                            enum1 = direction;
                            if (HereLaneInfoBuilder.getNumDirections(set, HereLaneInfoBuilder.DirectionType.LEFT) == 1) {
                                enum1 = HereLaneInfoBuilder.getDirection(set, HereLaneInfoBuilder.DirectionType.LEFT);
                                break;
                            }
                            break;
                        case LIGHT_LEFT:
                            if (set.contains(LaneInformation.Direction.SLIGHTLY_LEFT)) {
                                enum1 = LaneInformation.Direction.SLIGHTLY_LEFT;
                                break;
                            }
                            enum1 = direction;
                            if (HereLaneInfoBuilder.getNumDirections(set, HereLaneInfoBuilder.DirectionType.LEFT) == 1) {
                                enum1 = HereLaneInfoBuilder.getDirection(set, HereLaneInfoBuilder.DirectionType.LEFT);
                                break;
                            }
                            break;
                        case QUITE_LEFT:
                        case HEAVY_LEFT:
                            if (set.contains(LaneInformation.Direction.SHARP_LEFT)) {
                                enum1 = LaneInformation.Direction.SHARP_LEFT;
                                break;
                            }
                            enum1 = direction;
                            if (HereLaneInfoBuilder.getNumDirections(set, HereLaneInfoBuilder.DirectionType.LEFT) == 1) {
                                enum1 = HereLaneInfoBuilder.getDirection(set, HereLaneInfoBuilder.DirectionType.LEFT);
                                break;
                            }
                            break;
                    }
                }
            }
            return (LaneInformation.Direction)enum1;
        }
        catch (Throwable t) {
            HereLaneInfoListener.sLogger.e(t);
            enum1 = direction;
            return (LaneInformation.Direction)enum1;
        }
        return (LaneInformation.Direction)enum1;
    }
    
    public void hideLaneInfo() {
        HereLaneInfoListener.sLogger.v("hideLaneInfo::");
        this.bus.post(HereLaneInfoListener.HIDE_LANE_INFO);
    }
    
    @Override
    public void onLaneInformation(final List<LaneInformation> list, final RoadElement roadElement) {
        if (this.running) {
            this.taskManager.execute(new Runnable() {
                @Override
                public void run() {
                    if (list == null || list.size() == 0) {
                        if (HereLaneInfoListener.this.lastLanesData != null) {
                            HereLaneInfoListener.this.lastLanesData = null;
                            HereLaneInfoListener.this.hideLaneInfo();
                        }
                    }
                    else {
                        int size;
                        try {
                            size = list.size();
                            if (HereLaneInfoListener.sLogger.isLoggable(2)) {
                                HereLaneInfoListener.sLogger.v("onShowLaneInfo: size=" + size);
                                for (final LaneInformation laneInformation : list) {
                                    HereLaneInfoListener.sLogger.v("onShowLaneInfo:recommended state:" + laneInformation.getRecommendationState().name());
                                    final EnumSet<LaneInformation.Direction> directions = laneInformation.getDirections();
                                    if (directions != null) {
                                        final Iterator<Object> iterator2 = directions.iterator();
                                        while (iterator2.hasNext()) {
                                            HereLaneInfoListener.sLogger.v("onShowLaneInfo:direction:" + iterator2.next().name());
                                        }
                                    }
                                }
                            }
                        }
                        catch (Throwable t) {
                            HereLaneInfoListener.sLogger.e(t);
                            return;
                        }
                        int n = 0;
                        int n2 = 0;
                        int n3 = 0;
                        for (int i = 0; i < size; ++i) {
                            switch (((LaneInformation)list.get(i)).getRecommendationState()) {
                                case NOT_RECOMMENDED:
                                    ++n3;
                                    break;
                                case HIGHLY_RECOMMENDED:
                                    ++n;
                                    break;
                                case RECOMMENDED:
                                    ++n2;
                                    break;
                            }
                        }
                        if (n == 0 && n2 == 0) {
                            if (HereLaneInfoListener.this.lastLanesData != null) {
                                HereLaneInfoListener.this.lastLanesData = null;
                                HereLaneInfoListener.this.bus.post(HereLaneInfoListener.HIDE_LANE_INFO);
                            }
                            if (HereLaneInfoListener.sLogger.isLoggable(2)) {
                                HereLaneInfoListener.sLogger.v("Invalid lane data");
                            }
                        }
                        else {
                            final LaneInformation.Direction direction = null;
                            int n4 = 0;
                            final EnumSet set = null;
                            LaneInformation.Direction direction2 = null;
                            EnumSet set2 = null;
                            LaneInformation.Direction direction3 = null;
                            EnumSet set3 = null;
                            LaneInformation.Direction direction4 = null;
                            LaneInformation.Direction direction5 = null;
                            AbstractCollection<Object> collection = null;
                            EnumSet<Object> set4 = null;
                            int n5 = 0;
                            for (int j = 0; j < size; ++j, n4 = n5, set2 = set4, set3 = (EnumSet)collection, direction2 = direction5, direction3 = direction4) {
                                final LaneInformation laneInformation2 = list.get(j);
                                switch (laneInformation2.getRecommendationState()) {
                                    default:
                                        direction4 = direction3;
                                        direction5 = direction2;
                                        collection = (AbstractCollection<Object>)set3;
                                        set4 = (EnumSet<Object>)set2;
                                        n5 = n4;
                                        break;
                                    case HIGHLY_RECOMMENDED: {
                                        n5 = n4;
                                        set4 = (EnumSet<Object>)set2;
                                        collection = (AbstractCollection<Object>)set3;
                                        direction5 = direction2;
                                        direction4 = direction3;
                                        if (direction2 != null) {
                                            break;
                                        }
                                        final EnumSet<LaneInformation.Direction> directions2 = laneInformation2.getDirections();
                                        n5 = n4;
                                        set4 = (EnumSet<Object>)set2;
                                        collection = (AbstractCollection<Object>)set3;
                                        direction5 = direction2;
                                        direction4 = direction3;
                                        if (directions2 == null) {
                                            break;
                                        }
                                        n5 = n4;
                                        set4 = (EnumSet<Object>)set2;
                                        collection = (AbstractCollection<Object>)set3;
                                        direction5 = direction2;
                                        direction4 = direction3;
                                        if (directions2.size() <= 0) {
                                            break;
                                        }
                                        if (directions2.size() != 1) {
                                            n5 = 1;
                                            set4 = (EnumSet<Object>)directions2;
                                            collection = (AbstractCollection<Object>)set3;
                                            direction5 = direction2;
                                            direction4 = direction3;
                                            break;
                                        }
                                        final LaneInformation.Direction direction6 = directions2.iterator().next();
                                        n5 = n4;
                                        set4 = (EnumSet<Object>)set2;
                                        collection = (AbstractCollection<Object>)set3;
                                        direction5 = direction6;
                                        direction4 = direction3;
                                        if (HereLaneInfoListener.sLogger.isLoggable(2)) {
                                            HereLaneInfoListener.sLogger.v("onRouteDirection:" + null);
                                            n5 = n4;
                                            set4 = (EnumSet<Object>)set2;
                                            collection = (AbstractCollection<Object>)set3;
                                            direction5 = direction6;
                                            direction4 = direction3;
                                            break;
                                        }
                                        break;
                                    }
                                    case RECOMMENDED: {
                                        n5 = n4;
                                        set4 = (EnumSet<Object>)set2;
                                        collection = (AbstractCollection<Object>)set3;
                                        direction5 = direction2;
                                        direction4 = direction3;
                                        if (direction3 != null) {
                                            break;
                                        }
                                        final EnumSet<LaneInformation.Direction> directions3 = laneInformation2.getDirections();
                                        n5 = n4;
                                        set4 = (EnumSet<Object>)set2;
                                        collection = (AbstractCollection<Object>)set3;
                                        direction5 = direction2;
                                        direction4 = direction3;
                                        if (directions3 == null) {
                                            break;
                                        }
                                        n5 = n4;
                                        set4 = (EnumSet<Object>)set2;
                                        collection = (AbstractCollection<Object>)set3;
                                        direction5 = direction2;
                                        direction4 = direction3;
                                        if (directions3.size() <= 0) {
                                            break;
                                        }
                                        if (directions3.size() != 1) {
                                            n5 = 1;
                                            collection = (AbstractCollection<Object>)directions3;
                                            set4 = (EnumSet<Object>)set2;
                                            direction5 = direction2;
                                            direction4 = direction3;
                                            break;
                                        }
                                        final LaneInformation.Direction direction7 = directions3.iterator().next();
                                        n5 = n4;
                                        set4 = (EnumSet<Object>)set2;
                                        collection = (AbstractCollection<Object>)set3;
                                        direction5 = direction2;
                                        direction4 = direction7;
                                        if (HereLaneInfoListener.sLogger.isLoggable(2)) {
                                            HereLaneInfoListener.sLogger.v("onRouteDirection:" + null);
                                            n5 = n4;
                                            set4 = (EnumSet<Object>)set2;
                                            collection = (AbstractCollection<Object>)set3;
                                            direction5 = direction2;
                                            direction4 = direction7;
                                            break;
                                        }
                                        break;
                                    }
                                }
                            }
                            if (n > 0) {
                                if (direction2 != null) {
                                    direction3 = direction2;
                                    set3 = set;
                                }
                                else if (direction3 != null && set2.contains(direction3)) {
                                    set3 = set;
                                }
                                else {
                                    set3 = set2;
                                    direction3 = direction;
                                }
                            }
                            else if (direction3 != null) {
                                set3 = set;
                            }
                            else {
                                direction3 = direction;
                            }
                            LaneInformation.Direction direction8 = direction3;
                            if (direction3 == null) {
                                direction8 = direction3;
                                if (n4 != 0) {
                                    if (HereLaneInfoListener.sLogger.isLoggable(2)) {
                                        HereLaneInfoListener.sLogger.v("onRouteDirection is needed, but not available");
                                    }
                                    final LaneInformation.Direction access$500 = HereLaneInfoListener.this.getOnRouteDirectionFromManeuver(set3);
                                    if (access$500 == null) {
                                        if (HereLaneInfoListener.sLogger.isLoggable(2)) {
                                            HereLaneInfoListener.sLogger.v("onRouteDirection:could not get onroute direction from maneuver");
                                        }
                                        if (HereLaneInfoListener.this.lastLanesData != null) {
                                            HereLaneInfoListener.this.lastLanesData = null;
                                            HereLaneInfoListener.this.bus.post(HereLaneInfoListener.HIDE_LANE_INFO);
                                        }
                                        return;
                                    }
                                    else {
                                        direction8 = access$500;
                                        if (HereLaneInfoListener.sLogger.isLoggable(2)) {
                                            HereLaneInfoListener.sLogger.v("onRouteDirection:got on route direction:" + access$500);
                                            direction8 = access$500;
                                        }
                                    }
                                }
                            }
                            HereLaneInfoListener.this.tempLanes.clear();
                            for (int k = 0; k < size; ++k) {
                                final LaneInformation laneInformation3 = list.get(k);
                                final EnumSet<LaneInformation.Direction> directions4 = laneInformation3.getDirections();
                                if (directions4 == null || directions4.size() == 0) {
                                    HereLaneInfoListener.this.tempLanes.add(HereLaneInfoListener.EMPTY_LANE_DATA);
                                }
                                else {
                                    HereLaneInfoListener.this.tempDirections.clear();
                                    for (final LaneInformation.Direction direction9 : directions4) {
                                        switch (direction9) {
                                            case UNDEFINED:
                                                continue;
                                            default:
                                                HereLaneInfoListener.this.tempDirections.add(direction9);
                                                continue;
                                        }
                                    }
                                    if (HereLaneInfoListener.this.tempDirections.size() == 0) {
                                        HereLaneInfoListener.this.tempLanes.add(HereLaneInfoListener.EMPTY_LANE_DATA);
                                    }
                                    else {
                                        switch (laneInformation3.getRecommendationState()) {
                                            case NOT_RECOMMENDED:
                                                HereLaneInfoListener.this.tempLanes.add(new MapEvents.LaneData(MapEvents.LaneData.Position.OFF_ROUTE, HereLaneInfoBuilder.getDrawable(HereLaneInfoListener.this.tempDirections, MapEvents.LaneData.Position.OFF_ROUTE, null)));
                                                break;
                                            case HIGHLY_RECOMMENDED:
                                                if (direction8 != null && HereLaneInfoListener.this.tempDirections.size() > 1) {
                                                    HereLaneInfoListener.this.tempDirections2.clear();
                                                    final Iterator iterator4 = HereLaneInfoListener.this.tempDirections.iterator();
                                                    while (iterator4.hasNext()) {
                                                        final LaneInformation.Direction direction10 = iterator4.next();
                                                        if (direction10 == direction8) {
                                                            HereLaneInfoListener.this.tempDirections2.add(direction10);
                                                            iterator4.remove();
                                                        }
                                                    }
                                                    HereLaneInfoListener.this.tempDirections.addAll(HereLaneInfoListener.this.tempDirections2);
                                                }
                                                HereLaneInfoListener.this.tempLanes.add(new MapEvents.LaneData(MapEvents.LaneData.Position.ON_ROUTE, HereLaneInfoBuilder.getDrawable(HereLaneInfoListener.this.tempDirections, MapEvents.LaneData.Position.ON_ROUTE, direction8)));
                                                break;
                                            case RECOMMENDED:
                                                if (direction8 != null && HereLaneInfoListener.this.tempDirections.size() > 1) {
                                                    HereLaneInfoListener.this.tempDirections2.clear();
                                                    final Iterator iterator5 = HereLaneInfoListener.this.tempDirections.iterator();
                                                    while (iterator5.hasNext()) {
                                                        final LaneInformation.Direction direction11 = iterator5.next();
                                                        if (direction11 == direction8) {
                                                            HereLaneInfoListener.this.tempDirections2.add(direction11);
                                                            iterator5.remove();
                                                        }
                                                    }
                                                    HereLaneInfoListener.this.tempDirections.addAll(HereLaneInfoListener.this.tempDirections2);
                                                }
                                                if (n == 0) {
                                                    HereLaneInfoListener.this.tempLanes.add(new MapEvents.LaneData(MapEvents.LaneData.Position.ON_ROUTE, HereLaneInfoBuilder.getDrawable(HereLaneInfoListener.this.tempDirections, MapEvents.LaneData.Position.ON_ROUTE, direction8)));
                                                    break;
                                                }
                                                HereLaneInfoListener.this.tempLanes.add(new MapEvents.LaneData(MapEvents.LaneData.Position.OFF_ROUTE, HereLaneInfoBuilder.getDrawable(HereLaneInfoListener.this.tempDirections, MapEvents.LaneData.Position.OFF_ROUTE, null)));
                                                break;
                                            case NOT_AVAILABLE:
                                                HereLaneInfoListener.this.tempLanes.add(new MapEvents.LaneData(MapEvents.LaneData.Position.OFF_ROUTE, HereLaneInfoBuilder.getDrawable(HereLaneInfoListener.this.tempDirections, MapEvents.LaneData.Position.OFF_ROUTE, null)));
                                                break;
                                        }
                                    }
                                }
                            }
                            if (HereLaneInfoListener.sLogger.isLoggable(2)) {
                                HereLaneInfoListener.sLogger.v("LaneData: size=" + HereLaneInfoListener.this.tempLanes.size());
                                for (final MapEvents.LaneData laneData : HereLaneInfoListener.this.tempLanes) {
                                    HereLaneInfoListener.sLogger.v("LaneData:position: " + laneData.position);
                                    if (laneData.icons != null) {
                                        for (int l = 0; l < laneData.icons.length; ++l) {
                                            HereLaneInfoListener.sLogger.v("LaneData:icons" + (l + 1) + " = " + laneData.icons[l]);
                                        }
                                    }
                                    else {
                                        HereLaneInfoListener.sLogger.v("LaneData:icons: null");
                                    }
                                }
                            }
                            if (HereLaneInfoListener.this.lastLanesData != null && HereLaneInfoBuilder.compareLaneData(HereLaneInfoListener.this.lastLanesData.laneData, HereLaneInfoListener.this.tempLanes)) {
                                if (HereLaneInfoListener.sLogger.isLoggable(2)) {
                                    HereLaneInfoListener.sLogger.v("LaneData: lanedata is same as last");
                                }
                                HereLaneInfoListener.this.tempLanes.clear();
                            }
                            else {
                                final ArrayList list = new ArrayList<MapEvents.LaneData>(HereLaneInfoListener.this.tempLanes);
                                HereLaneInfoListener.this.lastLanesData = new MapEvents.DisplayTrafficLaneInfo(list);
                                HereLaneInfoListener.this.tempLanes.clear();
                                HereLaneInfoListener.sLogger.v("onShowLaneInfo::" + list.size());
                                if (HereLaneInfoListener.this.running) {
                                    HereLaneInfoListener.this.bus.post(HereLaneInfoListener.this.lastLanesData);
                                }
                            }
                        }
                    }
                }
            }, 15);
        }
    }
    
    void start() {
        synchronized (this) {
            if (this.running) {
                HereLaneInfoListener.sLogger.v("already running");
            }
            else {
                HereLaneInfoListener.sLogger.v("start");
                this.taskManager.execute(this.startRunnable, 15);
                this.running = true;
            }
        }
    }
    
    void stop() {
        synchronized (this) {
            if (!this.running) {
                HereLaneInfoListener.sLogger.v("not running");
            }
            else {
                HereLaneInfoListener.sLogger.v("stop");
                this.taskManager.execute(this.stopRunnable, 15);
                this.running = false;
                this.bus.post(HereLaneInfoListener.HIDE_LANE_INFO);
                this.lastLanesData = null;
            }
        }
    }
}
