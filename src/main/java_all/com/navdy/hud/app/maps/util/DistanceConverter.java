package com.navdy.hud.app.maps.util;

import android.content.res.Resources;
import com.navdy.hud.app.HudApplication;
import com.navdy.service.library.events.navigation.DistanceUnit;
import com.navdy.hud.app.maps.here.HereMapUtil;
import com.navdy.hud.app.manager.SpeedManager;

public class DistanceConverter
{
    public static void convertToDistance(final SpeedManager.SpeedUnit speedUnit, final float value, final Distance distance) {
        distance.clear();
        switch (speedUnit) {
            default:
                if (value >= 160.934f) {
                    distance.value = value / 1609.34f;
                    distance.value = (float)HereMapUtil.roundToN(distance.value, 10);
                    distance.unit = DistanceUnit.DISTANCE_MILES;
                    break;
                }
                distance.value = (int)(3.28084f * value);
                distance.unit = DistanceUnit.DISTANCE_FEET;
                break;
            case KILOMETERS_PER_HOUR:
                if (value >= 400.0f) {
                    distance.value = value / 1000.0f;
                    distance.value = (float)HereMapUtil.roundToN(distance.value, 10);
                    distance.unit = DistanceUnit.DISTANCE_KMS;
                    break;
                }
                distance.value = value;
                distance.unit = DistanceUnit.DISTANCE_METERS;
                break;
        }
    }
    
    public static float convertToMeters(final float n, final DistanceUnit distanceUnit) {
        float n2 = n;
        switch (distanceUnit) {
            default:
                n2 = n;
                return n2;
            case DISTANCE_FEET:
                n2 = n / 3.28084f;
                return n2;
            case DISTANCE_MILES:
                n2 = n * 1609.34f;
                return n2;
            case DISTANCE_KMS:
                n2 = n * 1000.0f;
            case DISTANCE_METERS:
                return n2;
        }
    }
    
    public static class Distance
    {
        public DistanceUnit unit;
        public float value;
        
        void clear() {
            this.value = 0.0f;
            this.unit = null;
        }
        
        public String getFormattedExtendedDistanceUnit() {
            final Resources resources = HudApplication.getAppContext().getResources();
            String s = null;
            switch (this.unit) {
                default:
                    s = "";
                    break;
                case DISTANCE_METERS:
                    if (this.value == 1.0f) {
                        s = resources.getString(R.string.unit_meters_ext_singular);
                        break;
                    }
                    s = resources.getString(R.string.unit_meters_ext);
                    break;
                case DISTANCE_FEET:
                    if (this.value == 1.0f) {
                        s = resources.getString(R.string.unit_feet_ext_singular);
                        break;
                    }
                    s = resources.getString(R.string.unit_feet_ext);
                    break;
                case DISTANCE_KMS:
                    if (this.value == 1.0f) {
                        s = resources.getString(R.string.unit_kilometers_ext_singular);
                        break;
                    }
                    s = resources.getString(R.string.unit_kilometers_ext);
                    break;
                case DISTANCE_MILES:
                    if (this.value == 1.0f) {
                        s = resources.getString(R.string.unit_miles_ext_singular);
                        break;
                    }
                    s = resources.getString(R.string.unit_miles_ext);
                    break;
            }
            return s;
        }
    }
}
