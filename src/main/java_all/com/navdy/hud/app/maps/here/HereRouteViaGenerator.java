package com.navdy.hud.app.maps.here;

import java.io.ObjectInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import com.here.android.mpa.routing.Route;
import android.text.TextUtils;
import java.util.HashSet;
import android.os.SystemClock;
import java.util.Iterator;
import java.util.Collections;
import com.here.android.mpa.common.RoadElement;
import java.util.Collection;
import com.here.android.mpa.routing.RouteElement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;
import com.here.android.mpa.routing.Maneuver;
import com.here.android.mpa.routing.RouteResult;
import java.util.List;
import com.navdy.service.library.log.Logger;

public class HereRouteViaGenerator
{
    private static final String COMMA = ", ";
    private static final String EMPTY = "";
    private static final Logger sLogger;
    private static final StringBuilder viaBuilder;
    
    static {
        sLogger = new Logger(HereRouteViaGenerator.class);
        viaBuilder = new StringBuilder();
    }
    
    private static List<DistanceContainer> buildDistanceListDesc(final List<RouteResult> list, final List<Maneuver> list2, final AtomicInteger atomicInteger, final String s) {
        final HashMap<String, DistanceContainer> hashMap = new HashMap<String, DistanceContainer>();
        final ArrayList<Comparable> list3 = new ArrayList<Comparable>();
        int n = 0;
        Label_0221: {
            if (list != null) {
                final Iterator<RouteResult> iterator = list.iterator();
                while (iterator.hasNext()) {
                    final List<RouteElement> elements = iterator.next().getRoute().getRouteElements().getElements();
                    int n2 = 1;
                    final Iterator<RouteElement> iterator2 = elements.iterator();
                    while (iterator2.hasNext()) {
                        processRoadElement(iterator2.next().getRoadElement(), hashMap, n, atomicInteger);
                        if (n2 % 5 == 0 && s != null && isRouteCancelled(s)) {
                            return (List<DistanceContainer>)list3;
                        }
                        ++n2;
                    }
                    list3.addAll(hashMap.values());
                    hashMap.clear();
                    ++n;
                }
                break Label_0221;
            }
            final Iterator<Maneuver> iterator3 = list2.iterator();
            while (iterator3.hasNext()) {
                final Iterator<RoadElement> iterator4 = iterator3.next().getRoadElements().iterator();
                while (iterator4.hasNext()) {
                    processRoadElement(iterator4.next(), hashMap, 0, atomicInteger);
                }
            }
            list3.addAll(hashMap.values());
            break Label_0221;
        }
        Collections.<Comparable>sort(list3);
        return (List<DistanceContainer>)list3;
    }
    
    public static String[] generateVia(final List<RouteResult> list, final String s) {
        Object iterator;
        long elapsedRealtime;
        int n;
        String[] array = null;
        double[] array2;
        List<DistanceContainer> buildDistanceListDesc;
        DistanceContainer distanceContainer;
        Logger sLogger;
        Object o = null;
        int index = 0;
        int n2;
        final List<DistanceContainer> list2;
        int n3 = 0;
        Iterator<DistanceContainer> iterator2;
        int j = 0;
        DistanceContainer distanceContainer2;
        Label_0507_Outer:Label_0583_Outer:
        while (true) {
            HereRouteViaGenerator.sLogger.v("generating via for " + list.size() + " routes");
            iterator = new AtomicInteger(0);
            elapsedRealtime = SystemClock.elapsedRealtime();
            n = list.size();
            array = new String[n];
            array2 = new double[n];
            while (true) {
            Label_0583:
                while (true) {
                Label_0481:
                    while (true) {
                        Label_0507:Block_8_Outer:
                        while (true) {
                            Label_0318: {
                                try {
                                    buildDistanceListDesc = buildDistanceListDesc(list, null, (AtomicInteger)iterator, s);
                                    if (HereRouteViaGenerator.sLogger.isLoggable(2)) {
                                        iterator = buildDistanceListDesc.iterator();
                                        while (((Iterator)iterator).hasNext()) {
                                            distanceContainer = ((Iterator<DistanceContainer>)iterator).next();
                                            sLogger = HereRouteViaGenerator.sLogger;
                                            o = new StringBuilder();
                                            sLogger.v(((StringBuilder)o).append("[").append(distanceContainer.index).append("] road[").append(distanceContainer.via).append("] distance[").append(distanceContainer.val).append("]").toString());
                                        }
                                    }
                                    break Label_0318;
                                }
                                catch (Throwable t) {
                                    HereRouteViaGenerator.sLogger.e(t);
                                    HereRouteViaGenerator.sLogger.v("via algo took[" + (SystemClock.elapsedRealtime() - elapsedRealtime) + "] len[" + array.length + "]");
                                    for (int i = 0; i < array.length; ++i) {
                                        HereRouteViaGenerator.sLogger.v("via[" + (i + 1) + "] [" + array[i] + "][" + array2[i] + "]");
                                    }
                                    break;
                                    // iftrue(Label_0751:, n3 != -1)
                                    // iftrue(Label_0860:, !iterator2.hasNext())
                                    // iftrue(Label_0347:, j == n || isRouteCancelled(s))
                                    // iftrue(Label_0866:, n2 >= array.length)
                                    // iftrue(Label_0591:, (DistanceContainer)iterator2.next().index != j)
                                    // iftrue(Label_0821:, !iterator.contains(o.via))
                                    // iftrue(Label_0811:, n + 1 != list2.size())
                                    // iftrue(Label_0347:, list2.size() <= 0)
                                Block_13:
                                    while (true) {
                                    Block_10:
                                        while (true) {
                                            Block_14: {
                                                while (true) {
                                                    Label_0395: {
                                                        while (true) {
                                                            index = ((DistanceContainer)o).index;
                                                            break Label_0507;
                                                            Label_0347: {
                                                                HereRouteViaGenerator.sLogger.v("via algo took[" + (SystemClock.elapsedRealtime() - elapsedRealtime) + "] len[" + array.length + "]");
                                                            }
                                                            n2 = 0;
                                                            break Label_0395;
                                                            o = list2.get(n);
                                                            continue Label_0507_Outer;
                                                        }
                                                        break Block_14;
                                                        HereRouteViaGenerator.sLogger.v("via[" + (n2 + 1) + "] [" + array[n2] + "][" + array2[n2] + "]");
                                                        ++n2;
                                                        break Label_0395;
                                                        n = array.length;
                                                        break Block_10;
                                                    }
                                                    continue Block_8_Outer;
                                                }
                                            }
                                            Block_15: {
                                                break Block_15;
                                                HereRouteViaGenerator.sLogger.v("already seen[" + ((DistanceContainer)o).via + "]");
                                                break Block_13;
                                                iterator2 = list2.iterator();
                                                continue Block_8_Outer;
                                            }
                                            iterator2.remove();
                                            continue Block_8_Outer;
                                        }
                                        n = 0;
                                        n3 = -1;
                                        continue Label_0481;
                                        iterator = new HashSet<DistanceContainer>();
                                        continue Label_0583_Outer;
                                    }
                                    HereRouteViaGenerator.sLogger.i("no more roadelements,use current");
                                    continue Label_0583;
                                }
                                finally {
                                    HereRouteViaGenerator.sLogger.v("via algo took[" + (SystemClock.elapsedRealtime() - elapsedRealtime) + "] len[" + array.length + "]");
                                    for (j = 0; j < array.length; ++j) {
                                        HereRouteViaGenerator.sLogger.v("via[" + (j + 1) + "] [" + array[j] + "][" + array2[j] + "]");
                                    }
                                    break;
                                }
                            }
                            Label_0751: {
                                if ((index = n3) != ((DistanceContainer)o).index) {
                                    HereRouteViaGenerator.sLogger.i("no more roadelements,go back");
                                    distanceContainer2 = list2.get(n - 1);
                                    array[j] = distanceContainer2.via;
                                    array2[j] = distanceContainer2.val;
                                    continue Label_0583;
                                }
                            }
                            continue Label_0507;
                        }
                        Label_0811: {
                            ++n;
                        }
                        n3 = index;
                        continue Label_0481;
                    }
                    Label_0821: {
                        array[((DistanceContainer)o).index] = ((DistanceContainer)o).via;
                    }
                    array2[((DistanceContainer)o).index] = ((DistanceContainer)o).val;
                    ((HashSet<DistanceContainer>)iterator).add((DistanceContainer)((DistanceContainer)o).via);
                    continue Label_0583;
                }
                Label_0860: {
                    ++j;
                }
                continue;
            }
        }
        Label_0866: {
            return array;
        }
    }
    
    private static String getDisplayString(final RoadElement roadElement) {
        String s;
        if (roadElement == null) {
            s = null;
        }
        else if (TextUtils.isEmpty((CharSequence)(s = roadElement.getRouteName()))) {
            s = roadElement.getRoadName();
        }
        return s;
    }
    
    public static String getViaString(final Route route) {
        return getViaString(route, 1);
    }
    
    public static String getViaString(final Route route, int n) {
        final AtomicInteger atomicInteger = new AtomicInteger(0);
        final StringBuilder viaBuilder = HereRouteViaGenerator.viaBuilder;
        synchronized (viaBuilder) {
            HereRouteViaGenerator.viaBuilder.setLength(0);
            if (route != null) {
                final List<DistanceContainer> buildDistanceListDesc = buildDistanceListDesc(null, route.getManeuvers(), atomicInteger, null);
                if (buildDistanceListDesc != null && buildDistanceListDesc.size() > 0) {
                    for (int size = buildDistanceListDesc.size(), i = 0; i < size; ++i) {
                        if (HereRouteViaGenerator.viaBuilder.length() > 0) {
                            HereRouteViaGenerator.viaBuilder.append(", ");
                        }
                        HereRouteViaGenerator.viaBuilder.append(buildDistanceListDesc.get(i).via);
                        if (--n == 0) {
                            break;
                        }
                    }
                }
            }
            final String string = HereRouteViaGenerator.viaBuilder.toString();
            HereRouteViaGenerator.viaBuilder.setLength(0);
            HereRouteViaGenerator.sLogger.v("getViaString[" + string + "]");
            return string;
        }
    }
    
    private static boolean isRouteCancelled(final String s) {
        boolean b = false;
        if (s != null) {
            final String activeRouteCalcId = HereRouteManager.getActiveRouteCalcId();
            if (!TextUtils.equals((CharSequence)s, (CharSequence)activeRouteCalcId)) {
                HereRouteViaGenerator.sLogger.v("route request [" + s + "] is not active anymore, current [" + activeRouteCalcId + "]");
                b = true;
            }
        }
        return b;
    }
    
    private static void processRoadElement(final RoadElement roadElement, final HashMap<String, DistanceContainer> hashMap, final int n, final AtomicInteger atomicInteger) {
        final String displayString = getDisplayString(roadElement);
        if (!TextUtils.isEmpty((CharSequence)displayString)) {
            final DistanceContainer distanceContainer = hashMap.get(displayString);
            if (distanceContainer == null) {
                hashMap.put(displayString, new DistanceContainer(roadElement.getGeometryLength(), n, atomicInteger.getAndIncrement(), displayString));
            }
            else {
                distanceContainer.val += roadElement.getGeometryLength();
            }
        }
    }
    
    private static class DistanceContainer implements Comparable<DistanceContainer>
    {
        int index;
        long order;
        double val;
        String via;
        
        DistanceContainer(final double val, final int index, final long order, final String via) {
            this.val = val;
            this.index = index;
            this.order = order;
            this.via = via;
        }
        
        @Override
        public int compareTo(final DistanceContainer distanceContainer) {
            int n = this.index - distanceContainer.index;
            if (n == 0) {
                n = (int)distanceContainer.val - (int)this.val;
                if (n == 0) {
                    n = (int)(this.order - distanceContainer.order);
                }
            }
            return n;
        }
    }
}
