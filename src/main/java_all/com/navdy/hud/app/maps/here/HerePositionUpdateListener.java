package com.navdy.hud.app.maps.here;

import com.here.android.mpa.common.MatchedGeoPosition;
import com.navdy.service.library.task.TaskManager;
import com.here.android.mpa.routing.RouteTta;
import com.navdy.hud.app.framework.trips.TripManager;
import com.here.android.mpa.routing.Route;
import com.navdy.hud.app.util.CrashReporter;
import android.os.SystemClock;
import com.navdy.service.library.log.Logger;
import com.here.android.mpa.common.GeoPosition;
import com.squareup.otto.Bus;
import com.here.android.mpa.guidance.NavigationManager;

public class HerePositionUpdateListener extends PositionListener
{
    private final Bus bus;
    private HereNavigationManager hereNavigationManager;
    private GeoPosition lastGeoPosition;
    private long lastGeoPositionTime;
    private Logger logger;
    private HereNavController navController;
    private String tag;
    
    HerePositionUpdateListener(final String tag, final HereNavController navController, final HereNavigationManager hereNavigationManager, final Bus bus) {
        this.logger = new Logger("HerePositionListener");
        this.tag = tag;
        this.navController = navController;
        this.hereNavigationManager = hereNavigationManager;
        (this.bus = bus).register(this);
    }
    
    private void handlePositionUpdate(final GeoPosition lastGeoPosition) {
        while (true) {
            Label_0129: {
                try {
                    if (this.navController.isInitialized() && lastGeoPosition != null) {
                        if (this.lastGeoPosition == null) {
                            break Label_0129;
                        }
                        final long n = SystemClock.elapsedRealtime() - this.lastGeoPositionTime;
                        if (n >= 500L || !HereMapUtil.isCoordinateEqual(this.lastGeoPosition.getCoordinate(), lastGeoPosition.getCoordinate())) {
                            break Label_0129;
                        }
                        if (this.logger.isLoggable(2)) {
                            this.logger.v(this.tag + "GEO-Here-Nav same pos as last:" + n);
                        }
                    }
                    return;
                }
                catch (Throwable t) {
                    this.logger.e(t);
                    CrashReporter.getInstance().reportNonFatalException(t);
                    return;
                }
            }
            this.lastGeoPosition = lastGeoPosition;
            this.lastGeoPositionTime = SystemClock.elapsedRealtime();
            if (this.logger.isLoggable(2)) {
                this.logger.v(this.tag + " onPosUpdated: road name =" + HereMapUtil.getCurrentRoadName());
            }
            this.postTrackingEvent(lastGeoPosition);
            this.hereNavigationManager.refreshNavigationInfo();
        }
    }
    
    private void postTrackingEvent(final GeoPosition geoPosition) {
        if (this.hereNavigationManager.isNavigationModeOn()) {
            final RouteTta tta = this.navController.getTta(Route.TrafficPenaltyMode.OPTIMAL, true);
            if (tta != null) {
                this.bus.post(new TripManager.TrackingEvent(geoPosition, this.hereNavigationManager.getCurrentDestinationIdentifier(), tta.getDuration(), (int)this.navController.getDestinationDistance()));
            }
            else {
                this.bus.post(new TripManager.TrackingEvent(geoPosition));
            }
        }
        else {
            this.bus.post(new TripManager.TrackingEvent(geoPosition));
        }
    }
    
    @Override
    public void onPositionUpdated(final GeoPosition geoPosition) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                HerePositionUpdateListener.this.handlePositionUpdate(geoPosition);
            }
        }, 4);
    }
    
    public static class MapMatchEvent
    {
        public final MatchedGeoPosition mapMatch;
        
        public MapMatchEvent(final MatchedGeoPosition mapMatch) {
            this.mapMatch = mapMatch;
        }
    }
}
