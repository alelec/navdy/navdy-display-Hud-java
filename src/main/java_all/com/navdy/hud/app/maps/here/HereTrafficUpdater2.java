package com.navdy.hud.app.maps.here;

import com.navdy.hud.app.framework.network.NetworkBandwidthController;
import com.navdy.service.library.util.SystemUtils;
import com.navdy.hud.app.HudApplication;
import com.here.android.mpa.routing.Maneuver;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.RoadElement;
import java.util.ArrayList;
import com.navdy.hud.app.util.GenericUtil;
import java.util.Iterator;
import com.here.android.mpa.mapping.TrafficEvent;
import com.here.android.mpa.routing.RouteElement;
import java.util.List;
import android.os.SystemClock;
import com.navdy.service.library.task.TaskManager;
import com.navdy.hud.app.maps.MapSettings;
import android.os.Looper;
import java.util.concurrent.TimeUnit;
import com.here.android.mpa.routing.Route;
import android.os.Handler;
import com.here.android.mpa.guidance.TrafficUpdater;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.maps.MapEvents;

public class HereTrafficUpdater2
{
    private static final MapEvents.DisplayTrafficIncident FAILED_EVENT;
    private static final MapEvents.DisplayTrafficIncident INACTIVE_EVENT;
    private static final MapEvents.DisplayTrafficIncident NORMAL_EVENT;
    private static final int STALE_REQUEST_TIME;
    private static final String TAG = "HereTrafficUpdater2";
    private static final int TRAFFIC_DATA_CHECK_TIME_INTERVAL;
    private static final int TRAFFIC_DATA_REFRESH_INTERVAL;
    private static final int TRAFFIC_DATA_REFRESH_INTERVAL_LIMITED_BANDWIDTH;
    private static final Logger sLogger;
    private Bus bus;
    private Runnable checkRunnable;
    private TrafficUpdater.RequestInfo currentRequestInfo;
    private Handler handler;
    private long lastRequestTime;
    private TrafficUpdater.GetEventsListener onGetEventsListener;
    private TrafficUpdater.Listener onRequestListener;
    private Runnable refreshRunnable;
    private Route route;
    private TrafficUpdater trafficUpdater;
    
    static {
        sLogger = new Logger("HereTrafficUpdater2");
        TRAFFIC_DATA_REFRESH_INTERVAL = (int)TimeUnit.MINUTES.toMillis(5L);
        TRAFFIC_DATA_REFRESH_INTERVAL_LIMITED_BANDWIDTH = (int)TimeUnit.MINUTES.toMillis(15L);
        TRAFFIC_DATA_CHECK_TIME_INTERVAL = (int)TimeUnit.SECONDS.toMillis(30L);
        STALE_REQUEST_TIME = (int)TimeUnit.MINUTES.toMillis(2L);
        NORMAL_EVENT = new MapEvents.DisplayTrafficIncident(MapEvents.DisplayTrafficIncident.Type.NORMAL, MapEvents.DisplayTrafficIncident.Category.UNDEFINED);
        FAILED_EVENT = new MapEvents.DisplayTrafficIncident(MapEvents.DisplayTrafficIncident.Type.FAILED, MapEvents.DisplayTrafficIncident.Category.UNDEFINED);
        INACTIVE_EVENT = new MapEvents.DisplayTrafficIncident(MapEvents.DisplayTrafficIncident.Type.INACTIVE, MapEvents.DisplayTrafficIncident.Category.UNDEFINED);
    }
    
    public HereTrafficUpdater2(final Bus bus) {
        this.handler = new Handler(Looper.getMainLooper());
        this.onRequestListener = new TrafficUpdater.Listener() {
            @Override
            public void onStatusChanged(final RequestState requestState) {
                final int n = 0;
                int refereshInterval;
                int n3;
                int n2 = n3 = (refereshInterval = HereTrafficUpdater2.this.getRefereshInterval());
                try {
                    final Logger access$000 = HereTrafficUpdater2.sLogger;
                    refereshInterval = n2;
                    n3 = n2;
                    refereshInterval = n2;
                    n3 = n2;
                    final StringBuilder sb = new StringBuilder();
                    refereshInterval = n2;
                    n3 = n2;
                    access$000.i(sb.append("onRequestListener::").append(requestState).toString());
                    refereshInterval = n2;
                    n3 = n2;
                    Label_0280: {
                        if (requestState != RequestState.DONE) {
                            refereshInterval = n2;
                            n3 = n2;
                            HereTrafficUpdater2.sLogger.i("onRequestListener:: traffic data download failed");
                            refereshInterval = n2;
                            n3 = n2;
                            n2 = (n3 = (refereshInterval = HereTrafficUpdater2.this.getRefereshInterval() / 2));
                            HereTrafficUpdater2.this.bus.post(HereTrafficUpdater2.FAILED_EVENT);
                            n3 = 1;
                            break Label_0280;
                        }
                        refereshInterval = n2;
                        n3 = n2;
                        if (HereTrafficUpdater2.this.route == null) {
                            refereshInterval = n2;
                            n3 = n2;
                            HereTrafficUpdater2.sLogger.i("onRequestListener:traffic downloaded for open map");
                            if (!MapSettings.isTrafficDashWidgetsEnabled()) {
                                HereTrafficUpdater2.this.currentRequestInfo = null;
                                HereTrafficUpdater2.this.lastRequestTime = 0L;
                            }
                            if (true) {
                                HereTrafficUpdater2.this.reset(n2);
                            }
                        }
                        else {
                            refereshInterval = n2;
                            n3 = n2;
                            HereTrafficUpdater2.sLogger.i("onRequestListener:traffic downloaded for route");
                            refereshInterval = n2;
                            n3 = n2;
                            if (MapSettings.isTrafficDashWidgetsEnabled()) {
                                refereshInterval = n2;
                                n3 = n2;
                                HereTrafficUpdater2.sLogger.i("onRequestListener:call getEvents");
                                refereshInterval = n2;
                                n3 = n2;
                                final TaskManager instance = TaskManager.getInstance();
                                refereshInterval = n2;
                                n3 = n2;
                                refereshInterval = n2;
                                n3 = n2;
                                final Runnable runnable = new Runnable() {
                                    @Override
                                    public void run() {
                                        final long elapsedRealtime = SystemClock.elapsedRealtime();
                                        while (true) {
                                            try {
                                                final HereNavigationManager instance = HereNavigationManager.getInstance();
                                                if (!instance.isNavigationModeOn()) {
                                                    HereTrafficUpdater2.sLogger.v("nav is off");
                                                }
                                                else {
                                                    final List<RouteElement> aheadRouteElements = HereMapUtil.getAheadRouteElements(HereTrafficUpdater2.this.route, HereMapUtil.getCurrentRoadElement(), instance.getPrevManeuver(), instance.getNextManeuver());
                                                    if (aheadRouteElements == null || aheadRouteElements.size() == 0) {
                                                        HereTrafficUpdater2.sLogger.i("onRequestListener: no ahead road elements");
                                                        HereTrafficUpdater2.this.currentRequestInfo = null;
                                                        HereTrafficUpdater2.this.lastRequestTime = 0L;
                                                        HereTrafficUpdater2.this.reset(HereTrafficUpdater2.this.getRefereshInterval());
                                                    }
                                                    else {
                                                        HereTrafficUpdater2.sLogger.i("onRequestListener: calling getevent size:" + aheadRouteElements.size());
                                                        HereTrafficUpdater2.this.trafficUpdater.getEvents(aheadRouteElements, HereTrafficUpdater2.this.onGetEventsListener);
                                                    }
                                                    HereTrafficUpdater2.sLogger.v("ahead route build time [" + (SystemClock.elapsedRealtime() - elapsedRealtime) + "]");
                                                }
                                                return;
                                            }
                                            catch (Throwable t) {
                                                HereTrafficUpdater2.this.currentRequestInfo = null;
                                                HereTrafficUpdater2.this.lastRequestTime = 0L;
                                                HereTrafficUpdater2.this.reset(HereTrafficUpdater2.this.getRefereshInterval() / 2);
                                                HereTrafficUpdater2.sLogger.e("onRequestListener", t);
                                                continue;
                                            }
                                            continue;
                                        }
                                    }
                                };
                                refereshInterval = n2;
                                n3 = n2;
                                instance.execute(runnable, 2);
                                n3 = n;
                                break Label_0280;
                            }
                            if (!MapSettings.isTrafficDashWidgetsEnabled()) {
                                HereTrafficUpdater2.this.currentRequestInfo = null;
                                HereTrafficUpdater2.this.lastRequestTime = 0L;
                            }
                            if (true) {
                                HereTrafficUpdater2.this.reset(n2);
                            }
                        }
                        return;
                    }
                    if (!MapSettings.isTrafficDashWidgetsEnabled()) {
                        HereTrafficUpdater2.this.currentRequestInfo = null;
                        HereTrafficUpdater2.this.lastRequestTime = 0L;
                    }
                    if (n3 != 0) {
                        HereTrafficUpdater2.this.reset(n2);
                    }
                }
                catch (Throwable t) {
                    n3 = refereshInterval;
                    HereTrafficUpdater2.this.bus.post(HereTrafficUpdater2.FAILED_EVENT);
                    n3 = refereshInterval;
                    HereTrafficUpdater2.sLogger.e("onRequestListener", t);
                    if (!MapSettings.isTrafficDashWidgetsEnabled()) {
                        HereTrafficUpdater2.this.currentRequestInfo = null;
                        HereTrafficUpdater2.this.lastRequestTime = 0L;
                    }
                    if (true) {
                        HereTrafficUpdater2.this.reset(refereshInterval);
                    }
                }
                finally {
                    if (!MapSettings.isTrafficDashWidgetsEnabled()) {
                        HereTrafficUpdater2.this.currentRequestInfo = null;
                        HereTrafficUpdater2.this.lastRequestTime = 0L;
                    }
                    if (false) {
                        HereTrafficUpdater2.this.reset(n3);
                    }
                }
            }
        };
        this.onGetEventsListener = new TrafficUpdater.GetEventsListener() {
            @Override
            public void onComplete(final List<TrafficEvent> list, Error instance) {
                while (true) {
                    int n2;
                    int n = n2 = 0;
                    while (true) {
                        Label_0258: {
                            try {
                                final Logger access$000 = HereTrafficUpdater2.sLogger;
                                n2 = n;
                                n2 = n;
                                final StringBuilder sb = new StringBuilder();
                                n2 = n;
                                access$000.i(sb.append("onGetEventsListener::").append(instance).toString());
                                n2 = n;
                                final Route access$2 = HereTrafficUpdater2.this.route;
                                if (access$2 == null) {
                                    n2 = n;
                                    HereTrafficUpdater2.sLogger.i("onGetEventsListener: route is null");
                                    if (false) {
                                        HereTrafficUpdater2.this.reset(HereTrafficUpdater2.this.getRefereshInterval());
                                    }
                                }
                                else {
                                    n = (n2 = 1);
                                    HereTrafficUpdater2.this.currentRequestInfo = null;
                                    n2 = n;
                                    HereTrafficUpdater2.this.lastRequestTime = 0L;
                                    n2 = n;
                                    if (instance != Error.NONE) {
                                        break Label_0258;
                                    }
                                    if (list != null) {
                                        n2 = n;
                                        if (list.size() != 0) {
                                            n2 = n;
                                            instance = (Error)TaskManager.getInstance();
                                            n2 = n;
                                            n2 = n;
                                            final Runnable runnable = new Runnable() {
                                                @Override
                                                public void run() {
                                                    if (HereTrafficUpdater2.this.route == null) {
                                                        HereTrafficUpdater2.sLogger.w("no current route, cannot process traffic events");
                                                    }
                                                    else {
                                                        HereTrafficUpdater2.this.processEvents(access$2, list, HereNavigationManager.getInstance().getNavController().getDestinationDistance());
                                                    }
                                                }
                                            };
                                            n2 = n;
                                            ((TaskManager)instance).execute(runnable, 2);
                                            break Label_0174;
                                        }
                                    }
                                    n2 = n;
                                    HereTrafficUpdater2.sLogger.i("onGetEventsListener: no events");
                                    n2 = n;
                                    HereTrafficUpdater2.this.bus.post(HereTrafficUpdater2.FAILED_EVENT);
                                    if (true) {
                                        HereTrafficUpdater2.this.reset(HereTrafficUpdater2.this.getRefereshInterval());
                                    }
                                }
                                return;
                            }
                            finally {
                                if (n2 != 0) {
                                    HereTrafficUpdater2.this.reset(HereTrafficUpdater2.this.getRefereshInterval());
                                }
                            }
                        }
                        HereTrafficUpdater2.sLogger.i("onGetEventsListener: error " + instance);
                        n2 = n;
                        HereTrafficUpdater2.this.bus.post(HereTrafficUpdater2.FAILED_EVENT);
                        continue;
                    }
                }
            }
        };
        this.refreshRunnable = new Runnable() {
            @Override
            public void run() {
                HereTrafficUpdater2.this.startRequest();
            }
        };
        this.checkRunnable = new Runnable() {
            @Override
            public void run() {
                final boolean b = false;
                boolean b2 = b;
                try {
                    HereTrafficUpdater2.sLogger.v("checkRunnable");
                    b2 = b;
                    if (HereTrafficUpdater2.this.currentRequestInfo != null) {
                        b2 = b;
                        if (HereTrafficUpdater2.this.lastRequestTime > 0L) {
                            b2 = b;
                            final long n = SystemClock.elapsedRealtime() - HereTrafficUpdater2.this.lastRequestTime;
                            b2 = b;
                            final Logger access$000 = HereTrafficUpdater2.sLogger;
                            b2 = b;
                            b2 = b;
                            final StringBuilder sb = new StringBuilder();
                            b2 = b;
                            access$000.v(sb.append("checkRunnable:").append(n).toString());
                            b2 = b;
                            if (n >= HereTrafficUpdater2.STALE_REQUEST_TIME) {
                                b2 = b;
                                HereTrafficUpdater2.sLogger.v("checkRunnable: request stale");
                                b2 = b;
                                HereTrafficUpdater2.this.reset(5000);
                            }
                            else {
                                b2 = true;
                                HereTrafficUpdater2.sLogger.v("checkRunnable: request not stale");
                            }
                        }
                    }
                }
                finally {
                    if (b2) {
                        HereTrafficUpdater2.this.handler.postDelayed((Runnable)this, (long)HereTrafficUpdater2.TRAFFIC_DATA_CHECK_TIME_INTERVAL);
                    }
                }
            }
        };
        HereTrafficUpdater2.sLogger.i("ctor");
        this.bus = bus;
        this.trafficUpdater = TrafficUpdater.getInstance();
    }
    
    private MapEvents.DisplayTrafficIncident buildEvent(final MapEvents.DisplayTrafficIncident.Type type, final List<HereTrafficEvent> list) {
        HereTrafficEvent hereTrafficEvent = null;
        HereTrafficEvent hereTrafficEvent2 = null;
        HereTrafficEvent hereTrafficEvent3 = null;
        HereTrafficEvent hereTrafficEvent4 = null;
        HereTrafficEvent hereTrafficEvent5 = null;
        HereTrafficEvent hereTrafficEvent6 = null;
        HereTrafficEvent hereTrafficEvent7 = null;
        for (final HereTrafficEvent hereTrafficEvent8 : list) {
            final String shortText = hereTrafficEvent8.trafficEvent.getShortText();
            switch (shortText) {
                default:
                    if (hereTrafficEvent7 == null) {
                        hereTrafficEvent7 = hereTrafficEvent8;
                        continue;
                    }
                    if (hereTrafficEvent7.distance > hereTrafficEvent8.distance) {
                        hereTrafficEvent7 = hereTrafficEvent8;
                        continue;
                    }
                    continue;
                case "ACCIDENT":
                    if (hereTrafficEvent == null) {
                        hereTrafficEvent = hereTrafficEvent8;
                        continue;
                    }
                    if (hereTrafficEvent.distance > hereTrafficEvent8.distance) {
                        hereTrafficEvent = hereTrafficEvent8;
                        continue;
                    }
                    continue;
                case "CLOSURE":
                    if (hereTrafficEvent2 == null) {
                        hereTrafficEvent2 = hereTrafficEvent8;
                        continue;
                    }
                    if (hereTrafficEvent2.distance > hereTrafficEvent8.distance) {
                        hereTrafficEvent2 = hereTrafficEvent8;
                        continue;
                    }
                    continue;
                case "ROADWORKS":
                    if (hereTrafficEvent3 == null) {
                        hereTrafficEvent3 = hereTrafficEvent8;
                        continue;
                    }
                    if (hereTrafficEvent3.distance > hereTrafficEvent8.distance) {
                        hereTrafficEvent3 = hereTrafficEvent8;
                        continue;
                    }
                    continue;
                case "CONGESTION":
                    if (hereTrafficEvent4 == null) {
                        hereTrafficEvent4 = hereTrafficEvent8;
                        continue;
                    }
                    if (hereTrafficEvent4.distance > hereTrafficEvent8.distance) {
                        hereTrafficEvent4 = hereTrafficEvent8;
                        continue;
                    }
                    continue;
                case "FLOW":
                    if (hereTrafficEvent5 == null) {
                        hereTrafficEvent5 = hereTrafficEvent8;
                        continue;
                    }
                    if (hereTrafficEvent5.distance > hereTrafficEvent8.distance) {
                        hereTrafficEvent5 = hereTrafficEvent8;
                        continue;
                    }
                    continue;
                case "OTHER":
                    if (hereTrafficEvent6 == null) {
                        hereTrafficEvent6 = hereTrafficEvent8;
                        continue;
                    }
                    if (hereTrafficEvent6.distance > hereTrafficEvent8.distance) {
                        hereTrafficEvent6 = hereTrafficEvent8;
                        continue;
                    }
                    continue;
            }
        }
        HereTrafficEvent hereTrafficEvent9 = null;
        if (hereTrafficEvent != null) {
            hereTrafficEvent9 = hereTrafficEvent;
        }
        else if (hereTrafficEvent2 != null) {
            hereTrafficEvent9 = hereTrafficEvent2;
        }
        else if (hereTrafficEvent3 != null) {
            hereTrafficEvent9 = hereTrafficEvent3;
        }
        else if (hereTrafficEvent4 != null) {
            hereTrafficEvent9 = hereTrafficEvent4;
        }
        else if (hereTrafficEvent5 != null) {
            hereTrafficEvent9 = hereTrafficEvent5;
        }
        else if (hereTrafficEvent6 != null) {
            hereTrafficEvent9 = hereTrafficEvent6;
        }
        else if (hereTrafficEvent7 != null) {
            hereTrafficEvent9 = hereTrafficEvent7;
        }
        final String firstAffectedStreet = hereTrafficEvent9.trafficEvent.getFirstAffectedStreet();
        final String shortText2 = hereTrafficEvent9.trafficEvent.getShortText();
        int n2 = -1;
        switch (shortText2.hashCode()) {
            case -1360575985:
                if (shortText2.equals("ACCIDENT")) {
                    n2 = 0;
                    break;
                }
                break;
            case 1584535067:
                if (shortText2.equals("CLOSURE")) {
                    n2 = 1;
                    break;
                }
                break;
            case 1943412802:
                if (shortText2.equals("ROADWORKS")) {
                    n2 = 2;
                    break;
                }
                break;
            case 2101625895:
                if (shortText2.equals("CONGESTION")) {
                    n2 = 3;
                    break;
                }
                break;
            case 2160942:
                if (shortText2.equals("FLOW")) {
                    n2 = 4;
                    break;
                }
                break;
            case 75532016:
                if (shortText2.equals("OTHER")) {
                    n2 = 5;
                    break;
                }
                break;
            case 1748463920:
                if (shortText2.equals("UNDEFINED")) {
                    n2 = 6;
                    break;
                }
                break;
        }
        String string = null;
        String s = null;
        MapEvents.DisplayTrafficIncident.Category category = null;
        switch (n2) {
            default:
                string = "Undefined:" + hereTrafficEvent9.trafficEvent.getShortText();
                s = hereTrafficEvent9.trafficEvent.getEventText();
                category = MapEvents.DisplayTrafficIncident.Category.UNDEFINED;
                break;
            case 0:
                string = "Accident";
                s = hereTrafficEvent9.trafficEvent.getEventText();
                category = MapEvents.DisplayTrafficIncident.Category.ACCIDENT;
                break;
            case 1:
                string = "Closure";
                s = hereTrafficEvent9.trafficEvent.getEventText();
                category = MapEvents.DisplayTrafficIncident.Category.CLOSURE;
                break;
            case 2:
                string = "RoadWork";
                s = hereTrafficEvent9.trafficEvent.getEventText();
                category = MapEvents.DisplayTrafficIncident.Category.ROADWORKS;
                break;
            case 3:
                string = "Congestion";
                s = hereTrafficEvent9.trafficEvent.getEventText();
                category = MapEvents.DisplayTrafficIncident.Category.CONGESTION;
                break;
            case 4:
                string = "Congestion";
                s = "";
                category = MapEvents.DisplayTrafficIncident.Category.FLOW;
                break;
            case 5:
                string = "Other";
                s = hereTrafficEvent9.trafficEvent.getEventText();
                category = MapEvents.DisplayTrafficIncident.Category.OTHER;
                break;
        }
        return new MapEvents.DisplayTrafficIncident(type, category, string, s, firstAffectedStreet, hereTrafficEvent9.distance, hereTrafficEvent9.trafficEvent.getActivationDate(), hereTrafficEvent9.trafficEvent.getActivationDate(), hereTrafficEvent9.trafficEvent.getIconOnRoute());
    }
    
    private void cancelRequest() {
        while (true) {
            try {
                if (this.currentRequestInfo != null) {
                    if (this.currentRequestInfo.getError() == TrafficUpdater.Error.NONE) {
                        this.trafficUpdater.cancelRequest(this.currentRequestInfo.getRequestId());
                        this.lastRequestTime = 0L;
                        HereTrafficUpdater2.sLogger.v("cancelRequest: called cancel");
                    }
                    this.currentRequestInfo = null;
                }
            }
            catch (Throwable t) {
                HereTrafficUpdater2.sLogger.e(t);
                continue;
            }
            break;
        }
    }
    
    private void processEvents(final Route route, final List<TrafficEvent> list, long elapsedRealtime) {
        final long elapsedRealtime2 = SystemClock.elapsedRealtime();
        HereTrafficUpdater2.sLogger.v("processEvents size:" + list.size());
        GenericUtil.checkNotOnMainThread();
        route.getDestination();
        int n = 0;
        int n2 = 0;
        int n3 = 0;
        final ArrayList<HereTrafficEvent> list2 = new ArrayList<HereTrafficEvent>();
        final ArrayList<HereTrafficEvent> list3 = new ArrayList<HereTrafficEvent>();
        final ArrayList<HereTrafficEvent> list4 = new ArrayList<HereTrafficEvent>();
        final RoadElement currentRoadElement = HereMapUtil.getCurrentRoadElement();
        if (currentRoadElement == null) {
            HereTrafficUpdater2.sLogger.w("no current road element");
        }
        else {
            final GeoCoordinate lastGeoCoordinate = HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
            if (lastGeoCoordinate == null) {
                HereTrafficUpdater2.sLogger.w("no current coordinate");
            }
            else {
                final List<Maneuver> maneuvers = route.getManeuvers();
                final RoadElement roadElement = null;
                final Maneuver lastManeuver = HereMapUtil.getLastManeuver(maneuvers);
                RoadElement roadElement2 = roadElement;
                if (lastManeuver != null) {
                    final List<RoadElement> roadElements = lastManeuver.getRoadElements();
                    roadElement2 = roadElement;
                    if (roadElements != null) {
                        roadElement2 = roadElement;
                        if (roadElements.size() > 0) {
                            roadElement2 = roadElements.get(roadElements.size() - 1);
                        }
                    }
                }
                for (final TrafficEvent trafficEvent : list) {
                    if (!trafficEvent.isOnRoute(route)) {
                        HereTrafficUpdater2.sLogger.v("[NOT_ON_ROUTE] event is not on route");
                    }
                    else {
                        final List<RoadElement> affectedRoadElements = trafficEvent.getAffectedRoadElements();
                        if (affectedRoadElements == null || affectedRoadElements.size() == 0) {
                            HereTrafficUpdater2.sLogger.v("no road elements for event");
                        }
                        else {
                            int n4 = 0;
                            final TrafficEvent.Severity severity = trafficEvent.getSeverity();
                            switch (severity) {
                                case BLOCKING:
                                case VERY_HIGH:
                                case HIGH:
                                    n4 = 1;
                                    break;
                            }
                            long distanceToRoadElement = -1L;
                            if (n4 != 0) {
                                distanceToRoadElement = HereMapUtil.getDistanceToRoadElement(currentRoadElement, affectedRoadElements.get(0), maneuvers);
                                final long distanceToRoadElement2 = HereMapUtil.getDistanceToRoadElement(affectedRoadElements.get(0), roadElement2, maneuvers);
                                if (distanceToRoadElement2 == -1L) {
                                    HereTrafficUpdater2.sLogger.v("could not get distance");
                                    continue;
                                }
                                HereTrafficUpdater2.sLogger.v("[EVENT] distanceToEvent:" + distanceToRoadElement + " eventToDestinationDistance:" + distanceToRoadElement2 + " distanceToDest:" + elapsedRealtime + " total:" + (distanceToRoadElement + distanceToRoadElement2) + " distanceToEventHaversine:" + (long)trafficEvent.getDistanceTo(lastGeoCoordinate));
                            }
                            else {
                                HereTrafficUpdater2.sLogger.v("[EVENT] n/a severity = " + severity);
                            }
                            switch (severity) {
                                default:
                                    continue;
                                case BLOCKING:
                                    list2.add(new HereTrafficEvent(trafficEvent, distanceToRoadElement));
                                    ++n;
                                    continue;
                                case VERY_HIGH:
                                    list3.add(new HereTrafficEvent(trafficEvent, distanceToRoadElement));
                                    ++n2;
                                    continue;
                                case HIGH:
                                    list4.add(new HereTrafficEvent(trafficEvent, distanceToRoadElement));
                                    ++n3;
                                    continue;
                            }
                        }
                    }
                }
                HereTrafficUpdater2.sLogger.v("blocking:" + n + " very high:" + n2 + " high:" + n3);
                if (n > 0) {
                    this.bus.post(this.buildEvent(MapEvents.DisplayTrafficIncident.Type.BLOCKING, list2));
                }
                else if (n2 > 0) {
                    this.bus.post(this.buildEvent(MapEvents.DisplayTrafficIncident.Type.VERY_HIGH, list3));
                }
                else if (n3 > 0) {
                    this.bus.post(this.buildEvent(MapEvents.DisplayTrafficIncident.Type.HIGH, list4));
                }
                else {
                    this.bus.post(HereTrafficUpdater2.NORMAL_EVENT);
                }
                elapsedRealtime = SystemClock.elapsedRealtime();
                HereTrafficUpdater2.sLogger.v("processEvents took [" + (elapsedRealtime - elapsedRealtime2) + "]");
            }
        }
    }
    
    private void reset(final int n) {
        HereTrafficUpdater2.sLogger.v("reset");
        this.cancelRequest();
        this.handler.removeCallbacks(this.refreshRunnable);
        this.handler.postDelayed(this.refreshRunnable, (long)n);
        this.handler.removeCallbacks(this.checkRunnable);
    }
    
    private void startRequest() {
        while (true) {
            Label_0101: {
                try {
                    if (!SystemUtils.isConnectedToNetwork(HudApplication.getAppContext())) {
                        HereTrafficUpdater2.sLogger.i("startRequest: not connected to n/w, retry");
                        this.reset(this.getRefereshInterval() / 2);
                        this.bus.post(HereTrafficUpdater2.FAILED_EVENT);
                        this.lastRequestTime = 0L;
                    }
                    else {
                        if (!HereNavigationManager.getInstance().hasArrived()) {
                            break Label_0101;
                        }
                        HereTrafficUpdater2.sLogger.i("startRequest: arrived, not making request");
                        this.bus.post(HereTrafficUpdater2.INACTIVE_EVENT);
                        this.lastRequestTime = 0L;
                    }
                    return;
                }
                catch (Throwable t) {
                    HereTrafficUpdater2.sLogger.e(t);
                    this.reset(this.getRefereshInterval() / 2);
                    return;
                }
            }
            if (this.route != null) {
                HereTrafficUpdater2.sLogger.i("startRequest: getting route traffic");
                this.currentRequestInfo = this.trafficUpdater.request(this.route, this.onRequestListener);
            }
            else {
                final GeoCoordinate lastGeoCoordinate = HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
                if (lastGeoCoordinate == null) {
                    HereTrafficUpdater2.sLogger.i("startRequest: getting area traffic, no current coordinate");
                    this.lastRequestTime = 0L;
                    this.bus.post(HereTrafficUpdater2.FAILED_EVENT);
                    this.reset(this.getRefereshInterval() / 2);
                    return;
                }
                HereTrafficUpdater2.sLogger.i("startRequest: getting area traffic");
                this.currentRequestInfo = this.trafficUpdater.request(lastGeoCoordinate, this.onRequestListener);
            }
            final TrafficUpdater.Error error = this.currentRequestInfo.getError();
            HereTrafficUpdater2.sLogger.i("startRequest: returned:" + error);
            switch (error) {
                default:
                    this.lastRequestTime = 0L;
                    this.bus.post(HereTrafficUpdater2.FAILED_EVENT);
                    this.reset(this.getRefereshInterval() / 2);
                case NONE:
                    this.handler.removeCallbacks(this.checkRunnable);
                    this.handler.postDelayed(this.checkRunnable, (long)HereTrafficUpdater2.TRAFFIC_DATA_CHECK_TIME_INTERVAL);
                    this.lastRequestTime = SystemClock.elapsedRealtime();
            }
        }
    }
    
    int getRefereshInterval() {
        int n;
        if (NetworkBandwidthController.getInstance().isLimitBandwidthModeOn()) {
            n = HereTrafficUpdater2.TRAFFIC_DATA_REFRESH_INTERVAL_LIMITED_BANDWIDTH;
        }
        else {
            n = HereTrafficUpdater2.TRAFFIC_DATA_REFRESH_INTERVAL;
        }
        return n;
    }
    
    boolean isRunning() {
        return this.route != null;
    }
    
    public void setRoute(final Route route) {
        HereTrafficUpdater2.sLogger.i("setRoute:" + route + " id=" + System.identityHashCode(route));
        this.cancelRequest();
        this.route = route;
        this.startRequest();
    }
    
    public void start() {
        HereTrafficUpdater2.sLogger.i("start");
        this.startRequest();
    }
    
    private static class HereTrafficEvent
    {
        public long distance;
        public TrafficEvent trafficEvent;
        
        public HereTrafficEvent(final TrafficEvent trafficEvent, final long distance) {
            this.trafficEvent = trafficEvent;
            this.distance = distance;
        }
    }
}
