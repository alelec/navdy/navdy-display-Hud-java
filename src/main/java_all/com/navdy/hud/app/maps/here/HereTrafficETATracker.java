package com.navdy.hud.app.maps.here;

import com.navdy.hud.app.maps.MapEvents;
import android.os.Looper;
import java.util.Date;
import com.here.android.mpa.routing.Route;
import com.navdy.service.library.task.TaskManager;
import java.util.concurrent.TimeUnit;
import com.navdy.service.library.log.Logger;
import android.os.Handler;
import com.squareup.otto.Bus;
import com.here.android.mpa.guidance.NavigationManager;

public class HereTrafficETATracker extends NavigationManagerEventListener
{
    private static final long ETA_THRESHOLD;
    private static final long INVALID = -1L;
    private static final long REFRESH_ETA_INTERVAL_MILLIS;
    private long baseEta;
    private final Bus bus;
    private Runnable dismissEtaRunnable;
    private final Handler handler;
    private final HereNavController navController;
    private Runnable navigationModeChangeBkRunnable;
    private Runnable refreshEtaBkRunnable;
    private Runnable refreshEtaRunnable;
    private final Logger sLogger;
    
    static {
        REFRESH_ETA_INTERVAL_MILLIS = TimeUnit.SECONDS.toMillis(60L);
        ETA_THRESHOLD = TimeUnit.MINUTES.toMillis(10L);
    }
    
    public HereTrafficETATracker(final HereNavController navController, final Bus bus) {
        this.sLogger = new Logger(HereTrafficETATracker.class);
        this.refreshEtaRunnable = new Runnable() {
            @Override
            public void run() {
                TaskManager.getInstance().execute(HereTrafficETATracker.this.refreshEtaBkRunnable, 3);
            }
        };
        this.refreshEtaBkRunnable = new Runnable() {
            @Override
            public void run() {
                HereTrafficETATracker.this.etaUpdate();
            }
        };
        this.dismissEtaRunnable = new Runnable() {
            @Override
            public void run() {
                HereTrafficETATracker.this.sendTrafficDelayDismissEvent();
            }
        };
        this.navigationModeChangeBkRunnable = new Runnable() {
            @Override
            public void run() {
                final HereNavController.State state = HereTrafficETATracker.this.navController.getState();
                if (state == HereNavController.State.NAVIGATING) {
                    final Date eta = HereTrafficETATracker.this.navController.getEta(true, Route.TrafficPenaltyMode.OPTIMAL);
                    if (HereMapUtil.isValidEtaDate(eta)) {
                        HereTrafficETATracker.this.baseEta = eta.getTime();
                        HereTrafficETATracker.this.sLogger.v("got baseEta from eta:" + HereTrafficETATracker.this.baseEta);
                    }
                    else {
                        final Date ttaDate = HereTrafficETATracker.this.navController.getTtaDate(true, Route.TrafficPenaltyMode.OPTIMAL);
                        if (!HereMapUtil.isValidEtaDate(ttaDate)) {
                            HereTrafficETATracker.this.baseEta = -1L;
                            HereTrafficETATracker.this.sendTrafficDelayDismissEvent();
                            HereTrafficETATracker.this.sLogger.i("no baseEta");
                        }
                        else {
                            HereTrafficETATracker.this.baseEta = ttaDate.getTime();
                            HereTrafficETATracker.this.sLogger.v("got baseEta from eta--tta:" + HereTrafficETATracker.this.baseEta);
                        }
                    }
                    HereTrafficETATracker.this.refreshEta();
                }
                else {
                    HereTrafficETATracker.this.sLogger.i("no baseEta, not navigation:" + state);
                    HereTrafficETATracker.this.baseEta = -1L;
                    HereTrafficETATracker.this.handler.removeCallbacks(HereTrafficETATracker.this.refreshEtaRunnable);
                    HereTrafficETATracker.this.handler.removeCallbacks(HereTrafficETATracker.this.dismissEtaRunnable);
                    HereTrafficETATracker.this.sendTrafficDelayDismissEvent();
                }
            }
        };
        this.navController = navController;
        this.bus = bus;
        this.handler = new Handler(Looper.getMainLooper());
        this.sLogger.v("ctor");
    }
    
    private void etaUpdate() {
        this.sLogger.v("etaUpdate");
        if (this.baseEta == -1L) {
            this.sLogger.v("base ETA is invalid");
        }
        else {
            final Date eta = this.navController.getEta(true, Route.TrafficPenaltyMode.OPTIMAL);
            if (HereMapUtil.isValidEtaDate(eta)) {
                final long time = eta.getTime();
                final long n = time - this.baseEta;
                if (n > HereTrafficETATracker.ETA_THRESHOLD) {
                    this.sLogger.v("ETA threshold delay, triggering delay:" + n);
                    this.baseEta = time;
                    this.bus.post(new MapEvents.TrafficDelayEvent(n / 1000L));
                    this.handler.removeCallbacks(this.dismissEtaRunnable);
                    this.handler.postDelayed(this.dismissEtaRunnable, HereTrafficETATracker.ETA_THRESHOLD);
                }
                else {
                    this.sLogger.v("ETA threshold ok:" + n);
                }
            }
            this.refreshEta();
        }
    }
    
    private void refreshEta() {
        this.sLogger.v("posting refresh ETA in 60 sec...");
        this.handler.removeCallbacks(this.refreshEtaRunnable);
        this.handler.postDelayed(this.refreshEtaRunnable, HereTrafficETATracker.REFRESH_ETA_INTERVAL_MILLIS);
    }
    
    private void sendTrafficDelayDismissEvent() {
        this.bus.post(new MapEvents.TrafficDelayDismissEvent());
    }
    
    @Override
    public void onNavigationModeChanged() {
        TaskManager.getInstance().execute(this.navigationModeChangeBkRunnable, 3);
    }
    
    @Override
    public void onRouteUpdated(final Route route) {
        this.sendTrafficDelayDismissEvent();
    }
}
