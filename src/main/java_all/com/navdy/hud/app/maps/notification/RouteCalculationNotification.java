package com.navdy.hud.app.maps.notification;

import java.io.ObjectInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import com.navdy.service.library.events.input.Gesture;
import com.here.android.mpa.routing.RouteOptions;
import android.graphics.Color;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import android.view.ViewGroup.MarginLayoutParams;
import com.navdy.hud.app.ui.component.HaloView;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.events.places.DestinationSelectedResponse;
import com.squareup.otto.Subscribe;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.hud.app.ui.activity.Main;
import android.animation.AnimatorSet;
import android.view.LayoutInflater;
import com.navdy.hud.app.framework.notifications.NotificationType;
import android.content.Context;
import android.animation.Animator.AnimatorListener;
import android.animation.Animator;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import android.animation.TimeInterpolator;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.View;
import com.navdy.service.library.events.places.PlaceType;
import java.util.Date;
import com.navdy.hud.app.common.TimeHelper;
import com.here.android.mpa.common.GeoCoordinate;
import com.navdy.service.library.events.ui.Screen;
import android.os.Parcelable;
import com.navdy.hud.app.maps.util.RouteUtils;
import com.navdy.hud.app.maps.here.HereMapUtil;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.service.library.events.navigation.NavigationRouteResult;
import com.navdy.hud.app.maps.here.HereRouteCache;
import com.navdy.hud.app.ui.component.destination.DestinationParcelable;
import com.navdy.hud.app.maps.here.HereMapsManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.graphics.Shader;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.service.library.events.audio.SpeechRequest;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.hud.app.framework.destinations.DestinationsManager;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.navigation.NavigationRouteResponse;
import android.text.TextUtils;
import com.navdy.hud.app.maps.here.HereRouteManager;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import android.os.Looper;
import java.util.ArrayList;
import com.navdy.hud.app.HudApplication;
import com.squareup.wire.Message;
import com.navdy.service.library.events.audio.CancelSpeechRequest;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import com.navdy.hud.app.ui.framework.UIStateManager;
import android.text.SpannableStringBuilder;
import android.widget.TextView;
import com.navdy.service.library.events.places.DestinationSelectedRequest;
import com.navdy.service.library.events.destination.Destination;
import android.animation.ObjectAnimator;
import android.widget.ImageView;
import com.navdy.hud.app.ui.component.image.InitialsImageView;
import com.navdy.hud.app.ui.component.image.IconColorImageView;
import android.os.Handler;
import com.navdy.hud.app.maps.MapEvents;
import com.navdy.hud.app.framework.notifications.INotificationController;
import android.view.ViewGroup;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;
import android.content.res.Resources;
import com.navdy.hud.app.ui.component.ChoiceLayout2;
import java.util.List;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.hud.app.framework.notifications.INotification;

public class RouteCalculationNotification implements INotification
{
    public static final RemoteEvent CANCEL_CALC_TTS;
    public static final RemoteEvent CANCEL_TBT_TTS;
    private static final float ICON_SCALE_FACTOR = 1.38f;
    private static final int NAV_LOOK_UP_TIMEOUT;
    public static final String ROUTE_CALC_TTS_ID;
    public static final String ROUTE_PICKER = "ROUTE_PICKER";
    public static final String ROUTE_TBT_TTS_ID;
    private static final int TAG_CANCEL = 1;
    private static final int TAG_DISMISS = 3;
    private static final int TAG_RETRY = 4;
    private static final int TAG_ROUTES = 2;
    private static final int TIMEOUT;
    private static List<ChoiceLayout2.Choice> cancelRouteCalc;
    public static final String fastestRoute;
    public static final int notifBkColor;
    private static final Resources resources;
    private static List<ChoiceLayout2.Choice> routeError;
    private static final String routeFailed;
    private static final Logger sLogger;
    public static final String shortestRoute;
    private static final int showRouteBkColor;
    private static List<ChoiceLayout2.Choice> showRoutes;
    private static final int startTripImageContainerSize;
    private static final int startTripImageSize;
    private static final int startTripLeftMargin;
    private static final int startTripTextLeftMargin;
    private Bus bus;
    private ChoiceLayout2 choiceLayoutView;
    private List<ChoiceLayout2.Choice> choices;
    private ViewGroup containerView;
    private INotificationController controller;
    private String destinationAddress;
    private String destinationDistance;
    private String destinationLabel;
    private boolean dismissedbyUser;
    private MapEvents.RouteCalculationEvent event;
    private Handler handler;
    private int icon;
    private int iconBkColor;
    private IconColorImageView iconColorImageView;
    private InitialsImageView iconInitialsImageView;
    private int iconSide;
    private ImageView iconSpinnerView;
    private String initials;
    private ChoiceLayout2.IListener listener;
    private ObjectAnimator loadingAnimator;
    private Destination lookupDestination;
    private DestinationSelectedRequest lookupRequest;
    private int notifColor;
    private boolean registered;
    private RouteCalculationEventHandler routeCalculationEventHandler;
    private String routeId;
    private TextView routeTtaView;
    private long startTime;
    private String subTitle;
    private TextView subTitleView;
    private Runnable timeoutRunnable;
    private String title;
    private TextView titleView;
    private SpannableStringBuilder tripDuration;
    private String tts;
    private boolean ttsPlayed;
    private UIStateManager uiStateManager;
    private Runnable waitForNavLookupRunnable;
    
    static {
        sLogger = new Logger(RouteCalculationNotification.class);
        TIMEOUT = (int)TimeUnit.SECONDS.toMillis(30L);
        NAV_LOOK_UP_TIMEOUT = (int)TimeUnit.SECONDS.toMillis(10L);
        ROUTE_CALC_TTS_ID = "route_calc_" + UUID.randomUUID().toString();
        ROUTE_TBT_TTS_ID = "route_tbt_" + UUID.randomUUID().toString();
        CANCEL_CALC_TTS = new RemoteEvent(new CancelSpeechRequest(RouteCalculationNotification.ROUTE_CALC_TTS_ID));
        CANCEL_TBT_TTS = new RemoteEvent(new CancelSpeechRequest(RouteCalculationNotification.ROUTE_TBT_TTS_ID));
        resources = HudApplication.getAppContext().getResources();
        final int color = RouteCalculationNotification.resources.getColor(R.color.glance_dismiss);
        final int color2 = RouteCalculationNotification.resources.getColor(R.color.glance_ok_blue);
        final int color3 = RouteCalculationNotification.resources.getColor(R.color.glance_ok_blue);
        final int color4 = RouteCalculationNotification.resources.getColor(R.color.glance_ok_go);
        final String string = RouteCalculationNotification.resources.getString(R.string.more_routes);
        (RouteCalculationNotification.cancelRouteCalc = new ArrayList<ChoiceLayout2.Choice>(1)).add(new ChoiceLayout2.Choice(1, R.drawable.icon_glances_dismiss, color, R.drawable.icon_glances_dismiss, -16777216, RouteCalculationNotification.resources.getString(R.string.cancel), color));
        (RouteCalculationNotification.showRoutes = new ArrayList<ChoiceLayout2.Choice>(2)).add(new ChoiceLayout2.Choice(2, R.drawable.icon_glances_read, color3, R.drawable.icon_glances_read, -16777216, string, color3));
        RouteCalculationNotification.showRoutes.add(new ChoiceLayout2.Choice(3, R.drawable.icon_glances_ok_strong, color4, R.drawable.icon_glances_ok_strong, -16777216, RouteCalculationNotification.resources.getString(R.string.route_go), color4));
        (RouteCalculationNotification.routeError = new ArrayList<ChoiceLayout2.Choice>(2)).add(new ChoiceLayout2.Choice(4, R.drawable.icon_glances_retry, color2, R.drawable.icon_glances_retry, -16777216, RouteCalculationNotification.resources.getString(R.string.retry), color2));
        RouteCalculationNotification.routeError.add(new ChoiceLayout2.Choice(3, R.drawable.icon_glances_dismiss, color, R.drawable.icon_glances_dismiss, -16777216, RouteCalculationNotification.resources.getString(R.string.dismiss), color));
        routeFailed = RouteCalculationNotification.resources.getString(R.string.route_failed);
        fastestRoute = RouteCalculationNotification.resources.getString(R.string.fastest_route);
        shortestRoute = RouteCalculationNotification.resources.getString(R.string.shortest_route);
        showRouteBkColor = RouteCalculationNotification.resources.getColor(R.color.route_unsel);
        notifBkColor = RouteCalculationNotification.resources.getColor(R.color.route_sel);
        startTripLeftMargin = RouteCalculationNotification.resources.getDimensionPixelSize(R.dimen.start_trip_left_margin);
        startTripImageContainerSize = RouteCalculationNotification.resources.getDimensionPixelSize(R.dimen.start_trip_image_container_size);
        startTripImageSize = RouteCalculationNotification.resources.getDimensionPixelSize(R.dimen.start_trip_image_size);
        startTripTextLeftMargin = RouteCalculationNotification.resources.getDimensionPixelSize(R.dimen.start_trip_text_left_margin);
    }
    
    public RouteCalculationNotification(final RouteCalculationEventHandler routeCalculationEventHandler, final Bus bus) {
        this.iconBkColor = 0;
        this.notifColor = 0;
        this.handler = new Handler(Looper.getMainLooper());
        this.waitForNavLookupRunnable = new Runnable() {
            @Override
            public void run() {
                if (RouteCalculationNotification.this.controller != null && RouteCalculationNotification.this.lookupRequest != null) {
                    RouteCalculationNotification.sLogger.v("nav lookup expired");
                    RouteCalculationNotification.this.lookupRequest = null;
                    RouteCalculationNotification.this.launchOriginalDestination();
                }
            }
        };
        this.timeoutRunnable = new Runnable() {
            @Override
            public void run() {
                RouteCalculationNotification.sLogger.v("timeoutRunnable");
                RouteCalculationNotification.this.dismissNotification();
            }
        };
        this.listener = new ChoiceLayout2.IListener() {
            @Override
            public void executeItem(final Selection selection) {
                if (RouteCalculationNotification.this.controller != null) {
                    switch (selection.id) {
                        case 1:
                            RouteCalculationNotification.sLogger.v("cancel");
                            RouteCalculationNotification.this.dismissedbyUser = true;
                            AnalyticsSupport.recordRouteSelectionCancelled();
                            if (RouteCalculationNotification.this.lookupRequest == null) {
                                RouteCalculationNotification.sLogger.v("cancel route");
                                if (!HereRouteManager.cancelActiveRouteRequest()) {
                                    RouteCalculationNotification.sLogger.v("route calc was not cancelled, may be it is not running");
                                    if (!TextUtils.isEmpty((CharSequence)RouteCalculationNotification.this.routeId)) {
                                        RouteCalculationNotification.this.bus.post(new RemoteEvent(new NavigationRouteResponse.Builder().requestId(RouteCalculationNotification.this.routeId).status(RequestStatus.REQUEST_CANCELLED).destination(new Coordinate.Builder().latitude(0.0).longitude(0.0).build()).build()));
                                        RouteCalculationNotification.sLogger.v("route calc event sent:" + RouteCalculationNotification.this.routeId);
                                    }
                                    else {
                                        RouteCalculationNotification.sLogger.v("route calc was not cancelled,no route id");
                                    }
                                }
                                else {
                                    RouteCalculationNotification.sLogger.v("route calc was cancelled");
                                }
                            }
                            RouteCalculationNotification.this.dismissNotification();
                            break;
                        case 2:
                            RouteCalculationNotification.sLogger.v("show route picker: choice");
                            RouteCalculationNotification.this.dismissedbyUser = true;
                            RouteCalculationNotification.this.showRoutePicker();
                            break;
                        case 3:
                            RouteCalculationNotification.sLogger.v("dismiss route");
                            RouteCalculationNotification.this.dismissedbyUser = true;
                            RouteCalculationNotification.this.dismissNotification();
                            break;
                        case 4: {
                            RouteCalculationNotification.sLogger.v("retry");
                            final MapEvents.RouteCalculationEvent currentRouteCalcEvent = RouteCalculationNotification.this.routeCalculationEventHandler.getCurrentRouteCalcEvent();
                            if (currentRouteCalcEvent != null && currentRouteCalcEvent.request != null) {
                                RouteCalculationNotification.this.bus.post(currentRouteCalcEvent.request);
                                RouteCalculationNotification.this.bus.post(new RemoteEvent(currentRouteCalcEvent.request));
                                break;
                            }
                            RouteCalculationNotification.sLogger.v("retry: request not found");
                            RouteCalculationNotification.this.dismissNotification();
                            break;
                        }
                    }
                }
            }
            
            @Override
            public void itemSelected(final Selection selection) {
            }
        };
        this.uiStateManager = RemoteDeviceManager.getInstance().getUiStateManager();
        this.routeCalculationEventHandler = routeCalculationEventHandler;
        this.bus = bus;
    }
    
    private void dismissNotification() {
        this.routeCalculationEventHandler.clearCurrentRouteCalcEvent();
        NotificationManager.getInstance().removeNotification("navdy#route#calc#notif");
    }
    
    private void launchOriginalDestination() {
        if (this.lookupDestination != null) {
            final com.navdy.hud.app.framework.destinations.Destination transformToInternalDestination = DestinationsManager.getInstance().transformToInternalDestination(this.lookupDestination);
            DestinationsManager.getInstance().requestNavigation(transformToInternalDestination);
            this.lookupDestination = null;
            RouteCalculationNotification.sLogger.v("original destination launched:" + transformToInternalDestination);
        }
        else {
            RouteCalculationNotification.sLogger.v("original destination not launched");
        }
    }
    
    private void playTts(final String s) {
        RouteCalculationNotification.sLogger.v("tts[" + s + "]");
        if (HereNavigationManager.getInstance().getNavigationSessionPreference().spokenTurnByTurn) {
            this.bus.post(RouteCalculationNotification.CANCEL_CALC_TTS);
            TTSUtils.sendSpeechRequest(s, SpeechRequest.Category.SPEECH_TURN_BY_TURN, RouteCalculationNotification.ROUTE_CALC_TTS_ID);
        }
    }
    
    private void setIcon() {
        if (this.iconBkColor != 0) {
            this.iconColorImageView.setIcon(this.icon, this.iconBkColor, null, 1.38f);
            this.iconColorImageView.setVisibility(View.VISIBLE);
            this.iconInitialsImageView.setVisibility(GONE);
        }
        else {
            this.iconInitialsImageView.setImage(this.icon, this.initials, InitialsImageView.Style.LARGE);
            this.iconInitialsImageView.setVisibility(View.VISIBLE);
            this.iconColorImageView.setVisibility(GONE);
        }
    }
    
    private void setUI() {
        this.startTime = SystemClock.elapsedRealtime();
        this.titleView.setText((CharSequence)this.title);
        this.subTitleView.setText((CharSequence)this.subTitle);
        this.iconSpinnerView.setImageResource(this.iconSide);
        final int n = 0;
        int n2;
        if (this.choices == RouteCalculationNotification.cancelRouteCalc) {
            this.routeTtaView.setVisibility(GONE);
            this.setIcon();
            this.startLoadingAnimation();
            if (this.tts != null && !this.ttsPlayed) {
                this.playTts(this.tts);
                this.tts = null;
                this.ttsPlayed = true;
            }
            n2 = n;
            if (this.lookupDestination != null) {
                this.lookupRequest = new DestinationSelectedRequest.Builder().request_id(UUID.randomUUID().toString()).destination(this.lookupDestination).build();
                this.handler.removeCallbacks(this.waitForNavLookupRunnable);
                this.handler.postDelayed(this.waitForNavLookupRunnable, (long)RouteCalculationNotification.NAV_LOOK_UP_TIMEOUT);
                this.bus.post(new RemoteEvent(this.lookupRequest));
                RouteCalculationNotification.sLogger.v("launched nav lookup request:" + this.lookupRequest.request_id + " dest:" + this.lookupDestination + " placeid:" + this.lookupDestination.place_id);
                n2 = n;
            }
        }
        else if (this.choices == RouteCalculationNotification.showRoutes) {
            n2 = 1;
            this.setIcon();
            this.stopLoadingAnimation();
            this.routeTtaView.setText((CharSequence)this.tripDuration);
            this.routeTtaView.setVisibility(View.VISIBLE);
        }
        else {
            n2 = n;
            if (this.choices == RouteCalculationNotification.routeError) {
                this.routeTtaView.setVisibility(GONE);
                this.setIcon();
                this.stopLoadingAnimation();
                n2 = n;
            }
        }
        this.choiceLayoutView.setChoices(this.choices, n2, this.listener);
        this.controller.startTimeout(this.getTimeout());
    }
    
    private void showRoutePicker() {
        if (this.event == null || this.event.response == null || this.event.request == null) {
            RouteCalculationNotification.sLogger.v("showRoutePicker invalid state");
            this.dismissNotification();
        }
        else {
            final Bundle bundle = new Bundle();
            bundle.putString("PICKER_LEFT_TITLE", RouteCalculationNotification.resources.getString(R.string.routes));
            bundle.putInt("PICKER_LEFT_ICON", R.drawable.icon_route);
            bundle.putInt("PICKER_LEFT_ICON_BKCOLOR", 0);
            bundle.putString("PICKER_TITLE", RouteCalculationNotification.resources.getString(R.string.pick_route));
            bundle.putBoolean("PICKER_HIDE", true);
            bundle.putBoolean("ROUTE_PICKER", true);
            bundle.putBoolean("PICKER_SHOW_ROUTE_MAP", true);
            final GeoCoordinate lastGeoCoordinate = HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
            bundle.putDouble("PICKER_MAP_START_LAT", lastGeoCoordinate.getLatitude());
            bundle.putDouble("PICKER_MAP_START_LNG", lastGeoCoordinate.getLongitude());
            bundle.putDouble("PICKER_MAP_END_LAT", (double)this.event.request.destination.latitude);
            bundle.putDouble("PICKER_MAP_END_LNG", (double)this.event.request.destination.longitude);
            final int size = this.event.response.results.size();
            final DestinationParcelable[] array = new DestinationParcelable[size];
            double doubleValue = 0.0;
            double doubleValue2 = 0.0;
            double doubleValue3 = 0.0;
            double doubleValue4 = 0.0;
            if (this.event.request.destination != null) {
                doubleValue3 = this.event.request.destination.latitude;
                doubleValue4 = this.event.request.destination.longitude;
            }
            if (this.event.request.destinationDisplay != null) {
                doubleValue = this.event.request.destinationDisplay.latitude;
                doubleValue2 = this.event.request.destinationDisplay.longitude;
            }
            final StringBuilder sb = new StringBuilder();
            final HereRouteCache instance = HereRouteCache.getInstance();
            final TimeHelper timeHelper = RemoteDeviceManager.getInstance().getTimeHelper();
            final MapEvents.ManeuverDisplay maneuverDisplay = new MapEvents.ManeuverDisplay();
            final Destination requestDestination = this.event.request.requestDestination;
            for (int i = 0; i < size; ++i) {
                final String s = "";
                final String s2 = "";
                final NavigationRouteResult navigationRouteResult = this.event.response.results.get(i);
                HereManeuverDisplayBuilder.setNavigationDistance(navigationRouteResult.length, maneuverDisplay, false, true);
                final HereRouteCache.RouteInfo route = instance.getRoute(navigationRouteResult.routeId);
                String string = s2;
                String formatEtaMinutes = s;
                if (route != null) {
                    final Date routeTtaDate = HereMapUtil.getRouteTtaDate(route.route);
                    string = s2;
                    formatEtaMinutes = s;
                    if (routeTtaDate != null) {
                        string = timeHelper.formatTime12Hour(routeTtaDate, sb, false) + " " + sb.toString();
                        final long n = routeTtaDate.getTime() - System.currentTimeMillis();
                        long minutes;
                        if (n < 0L) {
                            minutes = 0L;
                        }
                        else {
                            minutes = TimeUnit.MILLISECONDS.toMinutes(n);
                        }
                        formatEtaMinutes = RouteUtils.formatEtaMinutes(RouteCalculationNotification.resources, (int)minutes);
                    }
                }
                final String string2 = RouteCalculationNotification.resources.getString(R.string.route_title, new Object[] { formatEtaMinutes, navigationRouteResult.via });
                final String string3 = maneuverDisplay.distanceToPendingRoadText + " " + string;
                final String address = navigationRouteResult.address;
                final int notifBkColor = RouteCalculationNotification.notifBkColor;
                final DestinationParcelable.DestinationType destination = DestinationParcelable.DestinationType.DESTINATION;
                PlaceType place_type;
                if (requestDestination != null) {
                    place_type = requestDestination.place_type;
                }
                else {
                    place_type = null;
                }
                (array[i] = new DestinationParcelable(0, string2, string3, false, null, false, address, doubleValue3, doubleValue4, doubleValue, doubleValue2, R.drawable.icon_route, R.drawable.icon_route_sm, notifBkColor, 0, destination, place_type)).setRouteId(this.event.response.results.get(i).routeId);
                if (requestDestination != null) {
                    array[i].setIdentifier(requestDestination.identifier);
                    array[i].setPlaceId(requestDestination.place_id);
                }
                sb.setLength(0);
            }
            bundle.putParcelableArray("PICKER_DESTINATIONS", (Parcelable[])array);
            NotificationManager.getInstance().removeNotification("navdy#route#calc#notif", true, Screen.SCREEN_DESTINATION_PICKER, bundle, new RouteCalcPickerHandler(this.event));
            this.event = null;
        }
    }
    
    private void startLoadingAnimation() {
        if (this.loadingAnimator == null) {
            (this.loadingAnimator = ObjectAnimator.ofFloat(this.iconSpinnerView, View.ROTATION, new float[] { 360.0f })).setDuration(500L);
            this.loadingAnimator.setInterpolator((TimeInterpolator)new AccelerateDecelerateInterpolator());
            this.loadingAnimator.addListener((Animator.AnimatorListener)new DefaultAnimationListener() {
                @Override
                public void onAnimationEnd(final Animator animator) {
                    if (RouteCalculationNotification.this.loadingAnimator != null) {
                        RouteCalculationNotification.this.loadingAnimator.setStartDelay(33L);
                        RouteCalculationNotification.this.loadingAnimator.start();
                    }
                    else {
                        RouteCalculationNotification.sLogger.v("abandon loading animation");
                    }
                }
            });
        }
        if (!this.loadingAnimator.isRunning()) {
            RouteCalculationNotification.sLogger.v("started loading animation");
            this.loadingAnimator.start();
        }
    }
    
    private void stopLoadingAnimation() {
        if (this.loadingAnimator != null) {
            RouteCalculationNotification.sLogger.v("cancelled loading animation");
            this.loadingAnimator.removeAllListeners();
            this.loadingAnimator.cancel();
            this.iconSpinnerView.setRotation(0.0f);
            this.loadingAnimator = null;
        }
    }
    
    @Override
    public boolean canAddToStackIfCurrentExists() {
        return true;
    }
    
    @Override
    public boolean expandNotification() {
        return false;
    }
    
    @Override
    public int getColor() {
        return RouteCalculationNotification.notifBkColor;
    }
    
    @Override
    public View getExpandedView(final Context context, final Object o) {
        return null;
    }
    
    @Override
    public int getExpandedViewIndicatorColor() {
        return 0;
    }
    
    @Override
    public String getId() {
        return "navdy#route#calc#notif";
    }
    
    @Override
    public int getTimeout() {
        int n = 0;
        if (this.choices != RouteCalculationNotification.cancelRouteCalc) {
            if (this.choices == RouteCalculationNotification.routeError) {
                n = RouteCalculationNotification.TIMEOUT;
            }
            else if (this.choices == RouteCalculationNotification.showRoutes) {
                n = RouteCalculationNotification.TIMEOUT;
            }
        }
        return n;
    }
    
    @Override
    public NotificationType getType() {
        return NotificationType.ROUTE_CALC;
    }
    
    @Override
    public View getView(final Context context) {
        if (this.containerView == null) {
            this.containerView = (ViewGroup)LayoutInflater.from(context).inflate(R.layout.notification_route_calc, (ViewGroup)null);
            this.titleView = (TextView)this.containerView.findViewById(R.id.title);
            this.subTitleView = (TextView)this.containerView.findViewById(R.id.subTitle);
            this.iconColorImageView = (IconColorImageView)this.containerView.findViewById(R.id.iconColorView);
            this.iconInitialsImageView = (InitialsImageView)this.containerView.findViewById(R.id.iconInitialsView);
            this.iconSpinnerView = (ImageView)this.containerView.findViewById(R.id.iconSpinner);
            this.choiceLayoutView = (ChoiceLayout2)this.containerView.findViewById(R.id.choiceLayout);
            this.routeTtaView = (TextView)this.containerView.findViewById(R.id.routeTtaView);
        }
        return (View)this.containerView;
    }
    
    @Override
    public AnimatorSet getViewSwitchAnimation(final boolean b) {
        return null;
    }
    
    public void hideStartTrip() {
        final Main rootScreen = this.uiStateManager.getRootScreen();
        if (rootScreen != null) {
            rootScreen.ejectMainLowerView();
        }
    }
    
    @Override
    public boolean isAlive() {
        boolean b = false;
        String s = null;
        if (this.choices == RouteCalculationNotification.cancelRouteCalc) {
            b = (!this.dismissedbyUser && this.routeCalculationEventHandler.getCurrentRouteCalcEvent() != null);
            s = "cancelRouteCalc";
        }
        else if (this.choices == RouteCalculationNotification.routeError) {
            b = false;
            s = "routeError";
        }
        else if (this.choices == RouteCalculationNotification.showRoutes) {
            if (this.dismissedbyUser) {
                b = false;
            }
            else {
                final long n = SystemClock.elapsedRealtime() - this.startTime;
                final boolean b2 = n >= RouteCalculationNotification.TIMEOUT;
                b = (this.routeCalculationEventHandler.getCurrentRouteCalcEvent() != null && !b2);
                RouteCalculationNotification.sLogger.v("timedOut=" + n + " = " + b2);
            }
            s = "showRoutes";
        }
        RouteCalculationNotification.sLogger.v("alive=" + b + " type=" + s);
        return b;
    }
    
    @Override
    public boolean isPurgeable() {
        return !this.isAlive();
    }
    
    public boolean isStarting() {
        return this.choices == RouteCalculationNotification.cancelRouteCalc;
    }
    
    @Override
    public IInputHandler nextHandler() {
        return null;
    }
    
    @Override
    public void onClick() {
    }
    
    @Subscribe
    public void onConnectionStateChange(final ConnectionStateChange connectionStateChange) {
        if (this.controller != null) {
            switch (connectionStateChange.state) {
                case CONNECTION_DISCONNECTED:
                    if (this.lookupRequest != null) {
                        RouteCalculationNotification.sLogger.v("nav lookup device disconnected");
                        this.lookupRequest = null;
                        this.launchOriginalDestination();
                        break;
                    }
                    break;
            }
        }
    }
    
    @Subscribe
    public void onDestinationLookup(final DestinationSelectedResponse destinationSelectedResponse) {
        if (this.controller != null) {
            if (this.lookupRequest == null) {
                RouteCalculationNotification.sLogger.v("receive destination lookup:" + destinationSelectedResponse.request_id + " , not in lookup mode, ignore");
            }
            else if (!TextUtils.equals((CharSequence)destinationSelectedResponse.request_id, (CharSequence)this.lookupRequest.request_id)) {
                RouteCalculationNotification.sLogger.v("receive destination lookup, mismatch id recv[" + destinationSelectedResponse.request_id + "] lookup[" + this.lookupRequest.request_id + "]");
            }
            else {
                if (destinationSelectedResponse.request_status != null && destinationSelectedResponse.request_status == RequestStatus.REQUEST_SUCCESS) {
                    RouteCalculationNotification.sLogger.v("nav lookup suceeded:" + destinationSelectedResponse.destination);
                    DestinationsManager.getInstance().requestNavigation(DestinationsManager.getInstance().transformToInternalDestination(destinationSelectedResponse.destination));
                }
                else {
                    RouteCalculationNotification.sLogger.v("nav lookup failed:" + destinationSelectedResponse.request_status);
                    this.launchOriginalDestination();
                }
                this.lookupRequest = null;
            }
        }
    }
    
    @Override
    public void onExpandedNotificationEvent(final UIStateManager.Mode mode) {
    }
    
    @Override
    public void onExpandedNotificationSwitched() {
    }
    
    @Override
    public boolean onGesture(final GestureEvent gestureEvent) {
        final boolean b = false;
        boolean b2;
        if (this.controller == null) {
            b2 = b;
        }
        else {
            b2 = b;
            if (this.choices == RouteCalculationNotification.showRoutes) {
                switch (gestureEvent.gesture) {
                    default:
                        b2 = b;
                        break;
                    case GESTURE_SWIPE_LEFT:
                        RouteCalculationNotification.sLogger.v("show route picker:gesture");
                        this.dismissedbyUser = true;
                        this.showRoutePicker();
                        b2 = true;
                        break;
                }
            }
        }
        return b2;
    }
    
    @Override
    public boolean onKey(final CustomKeyEvent customKeyEvent) {
        boolean b = false;
        if (this.controller != null) {
            switch (customKeyEvent) {
                case LEFT:
                    this.controller.resetTimeout();
                    this.choiceLayoutView.moveSelectionLeft();
                    b = true;
                    break;
                case RIGHT:
                    this.controller.resetTimeout();
                    this.choiceLayoutView.moveSelectionRight();
                    b = true;
                    break;
                case SELECT:
                    this.choiceLayoutView.executeSelectedItem();
                    b = true;
                    break;
            }
        }
        return b;
    }
    
    @Override
    public void onNotificationEvent(final UIStateManager.Mode mode) {
    }
    
    @Override
    public void onStart(final INotificationController controller) {
        RouteCalculationNotification.sLogger.v("onStart");
        this.controller = controller;
        this.handler.removeCallbacks(this.timeoutRunnable);
        if (!this.registered) {
            this.bus.register(this);
            this.registered = true;
        }
        this.setUI();
    }
    
    @Override
    public void onStop() {
        RouteCalculationNotification.sLogger.v("onStop");
        this.hideStartTrip();
        this.stopLoadingAnimation();
        this.handler.removeCallbacks(this.waitForNavLookupRunnable);
        this.controller = null;
        this.ttsPlayed = false;
        if (this.choices == RouteCalculationNotification.cancelRouteCalc) {
            RouteCalculationNotification.sLogger.v("cancel tts");
            this.bus.post(RouteCalculationNotification.CANCEL_CALC_TTS);
        }
        if ((this.choices == RouteCalculationNotification.showRoutes || this.choices == RouteCalculationNotification.routeError) && this.isAlive()) {
            this.handler.removeCallbacks(this.timeoutRunnable);
            this.handler.postDelayed(this.timeoutRunnable, (long)RouteCalculationNotification.TIMEOUT);
            RouteCalculationNotification.sLogger.v("expire notif in " + RouteCalculationNotification.TIMEOUT);
        }
        if (this.registered) {
            this.registered = false;
            this.bus.unregister(this);
        }
    }
    
    @Override
    public void onTrackHand(final float n) {
    }
    
    @Override
    public void onUpdate() {
    }
    
    public void showError() {
        this.title = RouteCalculationNotification.routeFailed;
        this.choices = RouteCalculationNotification.routeError;
        this.iconSide = R.drawable.icon_badge_alert;
        this.event = null;
        this.tts = null;
        if (this.controller != null) {
            this.setUI();
        }
    }
    
    public void showRouteSearch(final String title, final String subTitle, final int icon, final int iconBkColor, final int notifColor, final String initials, final String tts, final Destination lookupDestination, final String routeId, final String destinationLabel, final String destinationAddress, final String destinationDistance) {
        RouteCalculationNotification.sLogger.v("showRouteSearch: navlookup:" + (lookupDestination != null));
        boolean b = false;
        if (this.lookupDestination != null) {
            b = b;
            if (TextUtils.equals((CharSequence)this.title, (CharSequence)title)) {
                b = true;
                RouteCalculationNotification.sLogger.v("showRouteSearch: nav lookup transition");
            }
        }
        this.title = title;
        this.subTitle = subTitle;
        this.routeId = routeId;
        if (!b) {
            this.icon = icon;
            this.iconBkColor = iconBkColor;
            this.notifColor = notifColor;
            this.initials = initials;
        }
        this.choices = RouteCalculationNotification.cancelRouteCalc;
        this.iconSide = R.drawable.loader_circle;
        this.event = null;
        this.tts = tts;
        this.lookupDestination = lookupDestination;
        this.destinationLabel = destinationLabel;
        this.destinationAddress = destinationAddress;
        this.destinationDistance = destinationDistance;
        if (this.controller != null) {
            this.setUI();
        }
    }
    
    public void showRoutes(final MapEvents.RouteCalculationEvent event) {
        switch (event.routeOptions.getRouteType()) {
            case FASTEST:
                this.title = RouteCalculationNotification.fastestRoute;
                break;
            case SHORTEST:
                this.title = RouteCalculationNotification.shortestRoute;
                break;
        }
        this.subTitle = RouteCalculationNotification.resources.getString(R.string.via_desc, new Object[] { event.response.results.get(0).via });
        this.choices = RouteCalculationNotification.showRoutes;
        this.icon = 0;
        this.iconBkColor = RouteCalculationNotification.showRouteBkColor;
        this.iconSide = R.drawable.icon_route;
        this.event = event;
        this.tripDuration = null;
        this.tts = null;
        final NavigationRouteResult navigationRouteResult = event.response.results.get(0);
        final HereRouteCache.RouteInfo route = HereRouteCache.getInstance().getRoute(navigationRouteResult.routeId);
        if (route != null) {
            this.tripDuration = HereMapUtil.getEtaString(route.route, navigationRouteResult.routeId, false);
        }
        else {
            this.tripDuration = new SpannableStringBuilder();
        }
        if (this.controller != null) {
            this.setUI();
        }
    }
    
    public void showStartTrip() {
        if (this.controller == null) {
            RouteCalculationNotification.sLogger.v("showStartTrip:null");
        }
        else {
            final Main rootScreen = RemoteDeviceManager.getInstance().getUiStateManager().getRootScreen();
            if (rootScreen != null) {
                final Context uiContext = this.controller.getUIContext();
                final ViewGroup viewGroup = (ViewGroup)LayoutInflater.from(uiContext).inflate(R.layout.vlist_item, (ViewGroup)null);
                final HaloView haloView = (HaloView)viewGroup.findViewById(R.id.halo);
                haloView.setVisibility(GONE);
                final ViewGroup.MarginLayoutParams ViewGroup.MarginLayoutParams = (ViewGroup.MarginLayoutParams)((ViewGroup)viewGroup.findViewById(R.id.imageContainer)).getLayoutParams();
                ViewGroup.MarginLayoutParams.width = RouteCalculationNotification.startTripImageContainerSize;
                ViewGroup.MarginLayoutParams.height = RouteCalculationNotification.startTripImageContainerSize;
                ViewGroup.MarginLayoutParams.leftMargin = RouteCalculationNotification.startTripLeftMargin;
                final ViewGroup viewGroup2 = (ViewGroup)viewGroup.findViewById(R.id.iconContainer);
                final ViewGroup.MarginLayoutParams ViewGroup.MarginLayoutParams2 = (ViewGroup.MarginLayoutParams)viewGroup2.getLayoutParams();
                ViewGroup.MarginLayoutParams2.width = RouteCalculationNotification.startTripImageSize;
                ViewGroup.MarginLayoutParams2.height = RouteCalculationNotification.startTripImageSize;
                ((ViewGroup.MarginLayoutParams)((ViewGroup)viewGroup.findViewById(R.id.textContainer)).getLayoutParams()).leftMargin = RouteCalculationNotification.startTripTextLeftMargin;
                final TextView textView = (TextView)viewGroup.findViewById(R.id.title);
                final TextView textView2 = (TextView)viewGroup.findViewById(R.id.subTitle);
                final TextView textView3 = (TextView)viewGroup.findViewById(R.id.subTitle2);
                final VerticalList.Model buildModel = IconBkColorViewHolder.buildModel(0, 0, 0, 0, 0, this.destinationLabel, this.destinationAddress, this.destinationDistance, null);
                VerticalList.setFontSize(buildModel, false);
                ((ViewGroup.MarginLayoutParams)textView.getLayoutParams()).topMargin = (int)buildModel.fontInfo.titleFontTopMargin;
                ((ViewGroup.MarginLayoutParams)textView2.getLayoutParams()).topMargin = (int)buildModel.fontInfo.subTitleFontTopMargin;
                ((ViewGroup.MarginLayoutParams)textView3.getLayoutParams()).topMargin = (int)buildModel.fontInfo.subTitle2FontTopMargin;
                textView.setTextSize(buildModel.fontInfo.titleFontSize);
                textView.setSingleLine(true);
                textView.setText((CharSequence)this.destinationLabel);
                if (!TextUtils.isEmpty((CharSequence)this.destinationAddress)) {
                    textView2.setTextSize(buildModel.fontInfo.subTitleFontSize);
                    textView2.setText((CharSequence)this.destinationAddress);
                }
                else {
                    textView2.setVisibility(GONE);
                }
                if (!TextUtils.isEmpty((CharSequence)this.destinationDistance)) {
                    textView3.setTextSize(buildModel.fontInfo.subTitle2FontSize);
                    textView3.setText((CharSequence)this.destinationDistance);
                    textView3.setVisibility(View.VISIBLE);
                }
                else {
                    textView3.setVisibility(GONE);
                }
                if (this.iconBkColor != 0) {
                    final IconColorImageView iconColorImageView = new IconColorImageView(uiContext);
                    viewGroup2.addView((View)iconColorImageView);
                    haloView.setStrokeColor(Color.argb(153, Color.red(this.iconBkColor), Color.green(this.iconBkColor), Color.blue(this.iconBkColor)));
                    iconColorImageView.setIcon(this.icon, this.iconBkColor, null, 0.83f);
                }
                else {
                    final InitialsImageView initialsImageView = new InitialsImageView(uiContext);
                    viewGroup2.addView((View)initialsImageView);
                    haloView.setStrokeColor(Color.argb(153, Color.red(this.notifColor), Color.green(this.notifColor), Color.blue(this.notifColor)));
                    initialsImageView.setImage(this.icon, this.initials, InitialsImageView.Style.MEDIUM);
                }
                rootScreen.injectMainLowerView((View)viewGroup);
            }
        }
    }
    
    @Override
    public boolean supportScroll() {
        return false;
    }
}
