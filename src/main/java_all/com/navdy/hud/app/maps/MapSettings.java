package com.navdy.hud.app.maps;

import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.hud.app.framework.glympse.GlympseManager;
import java.util.concurrent.TimeUnit;
import com.navdy.hud.app.util.os.SystemProperties;

public class MapSettings
{
    private static final String CUSTOM_ANIMATION = "persist.sys.custom_animation";
    private static final String DEBUG_HERE_LOC = "persist.sys.dbg_here_loc";
    private static final int DEFAULT_MAP_FPS = 5;
    private static final String FULL_CUSTOM_ANIMATION = "persist.sys.full_animation";
    private static final String GENERATE_ROUTE_ICONS = "persist.sys.route_icons";
    private static final String GLYMPSE_DURATION = "persist.sys.glympse_dur";
    private static final String LANE_GUIDANCE_ENABLED = "persist.sys.lane_info_enabled";
    private static final String MAP_FPS = "persist.sys.map_fps";
    private static final String NO_TURN_TEXT_IN_TBT = "persist.sys.no_turn_text";
    private static final String TBT_ONTO_DISABLED = "persist.sys.map.tbt.disableonto";
    private static final String TRAFFIC_WIDGETS_ENABLED = "persist.sys.map.traffic.widgets";
    private static final String TTS_RECALCULATION_DISABLED = "persist.sys.map.tts.norecalc";
    private static final boolean customAnimationEnabled;
    private static final boolean debugHereLocation;
    private static final boolean dontShowTurnText;
    private static final int fps;
    private static final boolean fullCustomAnimatonEnabled;
    private static final boolean generateRouteIcons;
    private static final int glympseDuration;
    private static final boolean laneGuidanceEnabled;
    private static int simulationSpeed;
    private static final boolean tbtOntoDisabled;
    private static final boolean trafficDashWidgetsEnabled;
    private static final boolean ttsDisableRecalculating;
    
    static {
        trafficDashWidgetsEnabled = SystemProperties.getBoolean("persist.sys.map.traffic.widgets", false);
        tbtOntoDisabled = SystemProperties.getBoolean("persist.sys.map.tbt.disableonto", false);
        ttsDisableRecalculating = SystemProperties.getBoolean("persist.sys.map.tts.norecalc", false);
        customAnimationEnabled = SystemProperties.getBoolean("persist.sys.custom_animation", false);
        fullCustomAnimatonEnabled = SystemProperties.getBoolean("persist.sys.full_animation", false);
        fps = SystemProperties.getInt("persist.sys.map_fps", 5);
        laneGuidanceEnabled = SystemProperties.getBoolean("persist.sys.lane_info_enabled", false);
        generateRouteIcons = SystemProperties.getBoolean("persist.sys.route_icons", false);
        debugHereLocation = SystemProperties.getBoolean("persist.sys.dbg_here_loc", false);
        dontShowTurnText = SystemProperties.getBoolean("persist.sys.no_turn_text", false);
        final int int1 = SystemProperties.getInt("persist.sys.glympse_dur", 0);
        if (int1 > 0 && int1 <= 1440) {
            glympseDuration = (int)TimeUnit.MINUTES.toMillis(int1);
        }
        else {
            glympseDuration = 0;
        }
    }
    
    public static boolean doNotShowTurnTextInTBT() {
        return true;
    }
    
    public static int getGlympseDuration() {
        int n;
        if (MapSettings.glympseDuration > 0) {
            n = MapSettings.glympseDuration;
        }
        else {
            n = GlympseManager.GLYMPSE_DURATION;
        }
        return n;
    }
    
    public static int getMapFps() {
        return MapSettings.fps;
    }
    
    public static int getSimulationSpeed() {
        return MapSettings.simulationSpeed;
    }
    
    public static boolean isCustomAnimationEnabled() {
        return MapSettings.customAnimationEnabled;
    }
    
    public static boolean isDebugHereLocation() {
        return MapSettings.debugHereLocation;
    }
    
    public static boolean isFullCustomAnimatonEnabled() {
        return MapSettings.fullCustomAnimatonEnabled;
    }
    
    public static boolean isGenerateRouteIcons() {
        return MapSettings.generateRouteIcons;
    }
    
    public static boolean isLaneGuidanceEnabled() {
        return MapSettings.laneGuidanceEnabled;
    }
    
    public static boolean isTbtOntoDisabled() {
        return MapSettings.tbtOntoDisabled || !DeviceUtil.isUserBuild();
    }
    
    public static boolean isTrafficDashWidgetsEnabled() {
        return MapSettings.trafficDashWidgetsEnabled;
    }
    
    public static boolean isTtsRecalculationDisabled() {
        return MapSettings.ttsDisableRecalculating;
    }
    
    public static void setSimulationSpeed(final int simulationSpeed) {
        MapSettings.simulationSpeed = simulationSpeed;
    }
}
