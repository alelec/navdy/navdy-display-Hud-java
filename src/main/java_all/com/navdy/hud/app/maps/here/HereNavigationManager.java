package com.navdy.hud.app.maps.here;

import java.util.UUID;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.profile.DriverProfileManager;
import com.navdy.service.library.events.navigation.NavigationSessionResponse;
import com.navdy.service.library.events.navigation.NavigationSessionStatusEvent;
import com.navdy.hud.app.framework.glance.GlanceHelper;
import com.navdy.service.library.events.preferences.NotificationPreferences;
import com.squareup.otto.Subscribe;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.here.android.mpa.routing.RouteOptions;
import com.navdy.hud.app.maps.NavSessionPreferences;
import com.squareup.wire.Message;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.events.navigation.NavigationSessionRouteChange;
import com.navdy.service.library.events.navigation.NavigationRouteResult;
import com.here.android.mpa.mapping.MapRoute;
import com.here.android.mpa.common.RoadElement;
import java.util.Date;
import com.here.android.mpa.common.GeoPosition;
import com.here.android.mpa.routing.Route;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.events.location.Coordinate;
import java.util.ArrayList;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.hud.app.maps.notification.RouteCalculationNotification;
import com.here.android.mpa.guidance.VoiceCatalog;
import com.navdy.hud.app.profile.HudLocale;
import com.navdy.service.library.events.preferences.NavigationPreferences;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.maps.MapSettings;
import com.navdy.hud.app.util.DeviceUtil;
import java.lang.ref.WeakReference;
import android.os.SystemClock;
import android.text.TextUtils;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.hud.app.util.GenericUtil;
import java.util.Iterator;
import com.here.android.mpa.guidance.VoiceSkin;
import java.util.List;
import com.here.android.mpa.mapping.MapObject;
import com.here.android.mpa.mapping.MapMarker;
import com.here.android.mpa.common.Image;
import com.here.android.mpa.common.GeoCoordinate;
import com.navdy.service.library.events.navigation.NavigationSessionRequest;
import android.content.res.Resources;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.service.library.events.audio.SpeechRequest;
import java.util.Locale;
import android.os.Looper;
import com.navdy.hud.app.HudApplication;
import java.util.HashMap;
import com.navdy.hud.app.common.TimeHelper;
import android.content.SharedPreferences;
import com.navdy.hud.app.ui.component.homescreen.NavigationView;
import com.here.android.mpa.guidance.NavigationManager;
import com.here.android.mpa.mapping.MapView;
import android.os.Handler;
import com.navdy.hud.app.profile.DriverSessionPreferences;
import com.navdy.service.library.events.navigation.NavigationSessionState;
import com.navdy.hud.app.maps.NavigationMode;
import com.here.android.mpa.routing.Maneuver;
import com.squareup.otto.Bus;
import com.navdy.hud.app.framework.network.NetworkBandwidthController;
import com.here.android.mpa.guidance.AudioPlayerDelegate;
import com.navdy.hud.app.event.LocalSpeechRequest;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.maps.MapsEventHandler;
import android.content.Context;
import java.util.Map;
import com.navdy.hud.app.maps.MapEvents;

public class HereNavigationManager
{
    private static final MapEvents.ArrivalEvent ARRIVAL_EVENT;
    private static final int ARRIVAL_GEO_FENCE_THRESHOLD = 804;
    static final String EMPTY_STR = "";
    private static final int ETA_CALCULATION_INTERVAL = 30000;
    private static final int ETA_UPDATE_THRESHOLD = 60000;
    private static Map<String, String> LANGUAGE_HIERARCHY;
    static final MapEvents.SpeedWarning SPEED_EXCEEDED;
    static final MapEvents.SpeedWarning SPEED_NORMAL;
    private static final String TAG_AUDIO_CALLBACK = "[CB-AUDIO]";
    private static final String TAG_GPS_CALLBACK = "[CB-GPS]";
    private static final String TAG_NAV_MANAGER_CALLBACK = "[CB-NAVM]";
    private static final String TAG_NEW_MANEUVER_CALLBACK = "[CB-NEWM]";
    private static final String TAG_POS_UPDATE_CALLBACK = "[CB-POS-UPDATE]";
    private static final String TAG_REROUTE_CALLBACK = "[CB-REROUTE]";
    private static final String TAG_SAFETY_CALLBACK = "[CB-SAFE]";
    private static final String TAG_SHOWLANE_INFO_CALLBACK = "[CB-LANEINFO]";
    private static final String TAG_SPEED_CALLBACK = "[CB-SPD]";
    private static final String TAG_TRAFFIC_ETA_TRACKER = "[CB-TRAFFIC-ETA]";
    private static final String TAG_TRAFFIC_REROUTE_CALLBACK = "[CB-TRAFFIC-REROUTE]";
    private static final String TAG_TRAFFIC_UPDATE = "[CB-TRAFFIC-UPDATE]";
    private static final String TAG_TRAFFIC_WARN = "[CB-TRAFFIC-WARN]";
    private static final String TAG_TTS = "[CB-TTS]";
    private static final String TBT_LANGUAGE_CODE = "en-US";
    private static final boolean VERBOSE = false;
    private static Context context;
    private static final HereMapsManager hereMapsManager;
    private static final MapsEventHandler mapsEventHandler;
    static final Logger sLogger;
    private static final HereNavigationManager sSingleton;
    private static final SpeedManager speedManager;
    private static final Object trafficRerouteLock;
    final MapEvents.GpsStatusChange BUS_GPS_SIGNAL_LOST;
    final MapEvents.GpsStatusChange BUS_GPS_SIGNAL_RESTORED;
    private final String HERE_TBT_AUDIO_DESTINATION_REACHED_PATTERN;
    private String INVALID_ROUTE_ID;
    private String INVALID_STATE;
    private String NO_CURRENT_POSITION;
    String TTS_KMS;
    String TTS_METERS;
    String TTS_MILES;
    LocalSpeechRequest TTS_REROUTING;
    LocalSpeechRequest TTS_REROUTING_FAILED;
    private AudioPlayerDelegate audioPlayerDelegate;
    private NetworkBandwidthController bandwidthController;
    private Bus bus;
    private Maneuver currentManeuver;
    private HereNavigationInfo currentNavigationInfo;
    private NavigationMode currentNavigationMode;
    private NavigationSessionState currentSessionState;
    private String debugManeuverDistance;
    private int debugManeuverIcon;
    private String debugManeuverInstruction;
    private HereManeuverDisplayBuilder.ManeuverState debugManeuverState;
    private int debugNextManeuverIcon;
    private DriverSessionPreferences driverSessionPreferences;
    private Runnable etaCalcRunnable;
    private Boolean generatePhoneticTTS;
    private HereGpsSignalListener gpsSignalListener;
    private Handler handler;
    private HereNavController hereNavController;
    private HereRealisticViewListener hereRealisticViewListener;
    private HereLaneInfoListener laneInfoListener;
    private Maneuver maneuverAfterCurrent;
    private HereMapController mapController;
    private MapView mapView;
    private NavdyTrafficRerouteManager navdyTrafficRerouteManager;
    private NavigationManager navigationManager;
    private HereNavigationEventListener navigationManagerEventListener;
    private NavigationView navigationView;
    private HereNewManeuverListener newManeuverEventListener;
    private HerePositionUpdateListener positionUpdateListener;
    private Maneuver prevManeuver;
    private HereRerouteListener reRouteListener;
    private HereSafetySpotListener safetySpotListener;
    private SharedPreferences sharedPreferences;
    private HereSpeedWarningManager speedWarningManager;
    private StringBuilder stringBuilder;
    private boolean switchingToNewRoute;
    private TimeHelper timeHelper;
    private HereTrafficETATracker trafficEtaTracker;
    private HereTrafficRerouteListener trafficRerouteListener;
    private HereTrafficUpdater2 trafficUpdater;
    private boolean trafficUpdaterStarted;
    
    static {
        sLogger = new Logger(HereNavigationManager.class);
        (HereNavigationManager.LANGUAGE_HIERARCHY = new HashMap<String, String>()).put("en-AU", "en-GB");
        HereNavigationManager.LANGUAGE_HIERARCHY.put("en-GB", "en");
        HereNavigationManager.LANGUAGE_HIERARCHY.put("en-US", "en");
        HereNavigationManager.context = HudApplication.getAppContext();
        hereMapsManager = HereMapsManager.getInstance();
        mapsEventHandler = MapsEventHandler.getInstance();
        speedManager = SpeedManager.getInstance();
        trafficRerouteLock = new Object();
        sSingleton = new HereNavigationManager();
        ARRIVAL_EVENT = new MapEvents.ArrivalEvent();
        SPEED_EXCEEDED = new MapEvents.SpeedWarning(true);
        SPEED_NORMAL = new MapEvents.SpeedWarning(false);
    }
    
    private HereNavigationManager() {
        this.currentNavigationInfo = new HereNavigationInfo();
        this.bus = MapsEventHandler.getInstance().getBus();
        this.sharedPreferences = MapsEventHandler.getInstance().getSharedPreferences();
        this.BUS_GPS_SIGNAL_LOST = new MapEvents.GpsStatusChange(false);
        this.BUS_GPS_SIGNAL_RESTORED = new MapEvents.GpsStatusChange(true);
        this.stringBuilder = new StringBuilder();
        this.etaCalcRunnable = new Runnable() {
            @Override
            public void run() {
                if (HereNavigationManager.this.currentNavigationMode != NavigationMode.MAP) {
                    final long n = System.currentTimeMillis() - HereNavigationManager.this.currentNavigationInfo.lastManeuverPostTime;
                    if (n < 0L) {
                        HereNavigationManager.this.handler.postDelayed((Runnable)this, 30000L);
                    }
                    else if (n < 60000L) {
                        if (HereNavigationManager.sLogger.isLoggable(2)) {
                            HereNavigationManager.sLogger.v("etaCalc threshold not met:" + n);
                        }
                        HereNavigationManager.this.handler.postDelayed((Runnable)this, 30000L);
                    }
                    else {
                        HereNavigationManager.this.refreshNavigationInfo();
                        if (HereNavigationManager.sLogger.isLoggable(2)) {
                            HereNavigationManager.sLogger.v("etaCalc updated maneuver:" + n);
                        }
                        HereNavigationManager.this.handler.postDelayed((Runnable)this, 30000L);
                    }
                }
            }
        };
        this.bandwidthController = NetworkBandwidthController.getInstance();
        this.handler = new Handler(Looper.getMainLooper());
        final String language = Locale.getDefault().getLanguage();
        final Resources resources = HudApplication.getAppContext().getResources();
        this.TTS_REROUTING = new LocalSpeechRequest(new SpeechRequest.Builder().words(resources.getString(R.string.tts_recalculate_route)).category(SpeechRequest.Category.SPEECH_REROUTE).language(language).build());
        this.TTS_REROUTING_FAILED = new LocalSpeechRequest(new SpeechRequest.Builder().words(resources.getString(R.string.tts_recalculate_route_failed)).category(SpeechRequest.Category.SPEECH_REROUTE).language(language).build());
        this.TTS_MILES = resources.getString(R.string.tts_miles_per_hour);
        this.TTS_KMS = resources.getString(R.string.tts_kms_per_hour);
        this.TTS_METERS = resources.getString(R.string.tts_meters_per_second);
        this.INVALID_STATE = resources.getString(R.string.invalid_state);
        this.NO_CURRENT_POSITION = resources.getString(R.string.no_current_position);
        this.INVALID_ROUTE_ID = resources.getString(R.string.invalid_routeid);
        this.HERE_TBT_AUDIO_DESTINATION_REACHED_PATTERN = resources.getString(R.string.tbt_audio_destination_reached_pattern).toLowerCase();
        this.currentSessionState = NavigationSessionState.NAV_SESSION_ENGINE_NOT_READY;
        this.timeHelper = RemoteDeviceManager.getInstance().getTimeHelper();
        this.initNavigationManager();
    }
    
    private void addDestinationMarker(final GeoCoordinate geoCoordinate, final int imageResource) {
        try {
            if (this.currentNavigationInfo.mapDestinationMarker != null) {
                this.removeDestinationMarker();
                this.currentNavigationInfo.mapDestinationMarker = null;
            }
            final Image image = new Image();
            image.setImageResource(imageResource);
            this.currentNavigationInfo.mapDestinationMarker = new MapMarker(geoCoordinate, image);
            this.mapController.addMapObject(this.currentNavigationInfo.mapDestinationMarker);
            HereNavigationManager.hereMapsManager.getLocationFixManager().addMarkers(this.mapController);
            HereNavigationManager.sLogger.v("added destination marker");
        }
        catch (Throwable t) {
            HereNavigationManager.sLogger.e(t);
        }
    }
    
    private void dumpVoiceSkinInfo(final List<VoiceSkin> list) {
        final StringBuffer sb = new StringBuffer("Supported voice skins: ");
        for (final VoiceSkin voiceSkin : list) {
            sb.append("[language:");
            sb.append(voiceSkin.getLanguage());
            sb.append(",code:");
            sb.append(voiceSkin.getLanguageCode());
            sb.append("],");
        }
        HereNavigationManager.sLogger.i(sb.toString());
    }
    
    private VoiceSkin findSkinSupporting(final List<VoiceSkin> list, String lowerCase) {
        VoiceSkin voiceSkin;
        if (list == null) {
            voiceSkin = null;
        }
        else if (lowerCase == null) {
            voiceSkin = null;
        }
        else {
            lowerCase = lowerCase.toLowerCase(Locale.US);
            final Iterator<VoiceSkin> iterator = list.iterator();
            while (iterator.hasNext()) {
                voiceSkin = iterator.next();
                if (voiceSkin.getLanguageCode().toLowerCase(Locale.US).startsWith(lowerCase)) {
                    return voiceSkin;
                }
            }
            voiceSkin = null;
        }
        return voiceSkin;
    }
    
    private static NavigationManager.MapUpdateMode getHereMapUpdateMode() {
        return NavigationManager.MapUpdateMode.NONE;
    }
    
    public static HereNavigationManager getInstance() {
        return HereNavigationManager.sSingleton;
    }
    
    private void handleNavigationSessionRequestInternal(final NavigationSessionRequest navigationSessionRequest) {
        while (true) {
            Label_0196: {
                synchronized (this) {
                    GenericUtil.checkNotOnMainThread();
                    try {
                        HereNavigationManager.sLogger.v("NavigationSessionRequest id[" + navigationSessionRequest.routeId + "] newState[" + navigationSessionRequest.newState + "] label[" + navigationSessionRequest.label + "] sim-speed[" + navigationSessionRequest.simulationSpeed + "] currentState[" + this.currentSessionState + "]");
                        if (!HereMapUtil.isValidNavigationState(navigationSessionRequest.newState)) {
                            this.returnResponse(RequestStatus.REQUEST_INVALID_REQUEST, this.INVALID_STATE, null, navigationSessionRequest.routeId);
                        }
                        else {
                            if (HereNavigationManager.hereMapsManager.getLocationFixManager().getLastGeoCoordinate() != null) {
                                break Label_0196;
                            }
                            this.returnResponse(RequestStatus.REQUEST_NO_LOCATION_SERVICE, this.NO_CURRENT_POSITION, null, navigationSessionRequest.routeId);
                        }
                    }
                    catch (Throwable t) {
                        HereNavigationManager.sLogger.e("handleNavigationSessionRequest", t);
                        this.returnResponse(RequestStatus.REQUEST_SERVICE_ERROR, t.toString(), null, navigationSessionRequest.routeId);
                    }
                    return;
                }
            }
            Object route = null;
            boolean b = false;
            if (navigationSessionRequest.routeId != null) {
                b = true;
                route = HereRouteCache.getInstance().getRoute(navigationSessionRequest.routeId);
            }
            if (this.isNavigationModeOn()) {
                HereNavigationManager.sLogger.v("navigation is active");
                switch (navigationSessionRequest.newState) {
                    default:
                        return;
                    case NAV_SESSION_PAUSED:
                        if (this.currentSessionState == NavigationSessionState.NAV_SESSION_STARTED) {
                            HereNavigationManager.sLogger.v("pausing navigation");
                            this.hereNavController.pause();
                            this.currentSessionState = NavigationSessionState.NAV_SESSION_PAUSED;
                            this.returnResponse(RequestStatus.REQUEST_SUCCESS, "", NavigationSessionState.NAV_SESSION_PAUSED, navigationSessionRequest.routeId);
                            this.postNavigationSessionStatusEvent(false);
                            return;
                        }
                        HereNavigationManager.sLogger.v("already paused");
                        this.returnResponse(RequestStatus.REQUEST_SUCCESS, "", NavigationSessionState.NAV_SESSION_PAUSED, null);
                        return;
                    case NAV_SESSION_STOPPED: {
                        HereNavigationManager.sLogger.v("stopping navigation");
                        final StringBuilder sb = new StringBuilder();
                        if (this.setNavigationMode(NavigationMode.MAP, null, sb)) {
                            this.navigationView.resetMapOverview();
                            this.returnResponse(RequestStatus.REQUEST_SUCCESS, "", this.currentSessionState, navigationSessionRequest.routeId);
                            return;
                        }
                        this.returnResponse(RequestStatus.REQUEST_SERVICE_ERROR, sb.toString(), null, navigationSessionRequest.routeId);
                        return;
                    }
                    case NAV_SESSION_STARTED: {
                        if (route == null && b) {
                            this.returnResponse(RequestStatus.REQUEST_INVALID_REQUEST, this.INVALID_ROUTE_ID, null, navigationSessionRequest.routeId);
                            return;
                        }
                        int n;
                        if (navigationSessionRequest.routeId == null || TextUtils.equals((CharSequence)navigationSessionRequest.routeId, (CharSequence)this.currentNavigationInfo.routeId)) {
                            n = 1;
                        }
                        else {
                            n = 0;
                        }
                        if (n == 0) {
                            HereNavigationManager.sLogger.v("different route: switching to new route");
                            final StringBuilder sb2 = new StringBuilder();
                            HereNavigationManager.sLogger.v("stop navigation");
                            try {
                                final long elapsedRealtime = SystemClock.elapsedRealtime();
                                this.switchingToNewRoute = true;
                                if (this.setNavigationMode(NavigationMode.MAP, null, sb2)) {
                                    HereNavigationManager.sLogger.v("start navigation");
                                    this.startNavigation(navigationSessionRequest, (HereRouteCache.RouteInfo)route);
                                    HereNavigationManager.sLogger.v("navigation took[" + (SystemClock.elapsedRealtime() - elapsedRealtime) + "]");
                                    return;
                                }
                                this.returnResponse(RequestStatus.REQUEST_SERVICE_ERROR, sb2.toString(), null, navigationSessionRequest.routeId);
                                return;
                            }
                            catch (Throwable t2) {
                                throw t2;
                            }
                            finally {
                                this.switchingToNewRoute = false;
                            }
                            break;
                        }
                        HereNavigationManager.sLogger.v("same route");
                        if (this.currentSessionState == NavigationSessionState.NAV_SESSION_STARTED) {
                            this.returnResponse(RequestStatus.REQUEST_SUCCESS, "", NavigationSessionState.NAV_SESSION_STARTED, navigationSessionRequest.routeId);
                            return;
                        }
                        HereNavigationManager.sLogger.v("resume navigation");
                        final NavigationManager.Error resume = this.hereNavController.resume();
                        if (resume == NavigationManager.Error.NONE) {
                            this.currentSessionState = NavigationSessionState.NAV_SESSION_STARTED;
                            this.returnResponse(RequestStatus.REQUEST_SUCCESS, "", NavigationSessionState.NAV_SESSION_STARTED, navigationSessionRequest.routeId);
                            this.postNavigationSessionStatusEvent(false);
                            return;
                        }
                        HereNavigationManager.sLogger.e("cannot resume navigation:" + resume);
                        this.returnResponse(RequestStatus.REQUEST_SERVICE_ERROR, HereNavigationManager.context.getString(R.string.unable_to_resume, new Object[] { resume }), NavigationSessionState.NAV_SESSION_STARTED, navigationSessionRequest.routeId);
                        if (!this.setNavigationMode(NavigationMode.MAP, null, null)) {
                            HereNavigationManager.sLogger.w("cannot even go to tracking mode");
                        }
                        return;
                    }
                }
            }
            HereNavigationManager.sLogger.v("navigation is not active");
            switch (navigationSessionRequest.newState) {
                default:
                case NAV_SESSION_PAUSED:
                    HereNavigationManager.sLogger.e("cannot pause navigation current state:" + this.currentSessionState);
                    this.returnResponse(RequestStatus.REQUEST_INVALID_STATE, HereNavigationManager.context.getString(R.string.cannot_move_state, new Object[] { this.currentSessionState.name(), navigationSessionRequest.newState.name() }), null, navigationSessionRequest.routeId);
                case NAV_SESSION_STARTED:
                    if (route == null) {
                        HereNavigationManager.sLogger.e("valid route id required");
                        this.returnResponse(RequestStatus.REQUEST_INVALID_REQUEST, this.INVALID_ROUTE_ID, null, navigationSessionRequest.routeId);
                        return;
                    }
                    try {
                        final long elapsedRealtime2 = SystemClock.elapsedRealtime();
                        this.startNavigation(navigationSessionRequest, (HereRouteCache.RouteInfo)route);
                        HereNavigationManager.sLogger.v("navigation took[" + (SystemClock.elapsedRealtime() - elapsedRealtime2) + "]");
                        return;
                    }
                    catch (Throwable t3) {
                        throw t3;
                    }
                case NAV_SESSION_STOPPED:
                    HereNavigationManager.sLogger.v("already stopped");
                    this.returnResponse(RequestStatus.REQUEST_SUCCESS, "", NavigationSessionState.NAV_SESSION_STOPPED, null);
            }
        }
    }
    
    private void initNavigationManager() {
        this.navigationManager = NavigationManager.getInstance();
        this.hereNavController = new HereNavController(this.navigationManager, this.bus);
        this.mapController = HereNavigationManager.hereMapsManager.getMapController();
        this.navigationManagerEventListener = new HereNavigationEventListener(HereNavigationManager.sLogger, "[CB-NAVM]", this);
        this.navigationManager.addNavigationManagerEventListener(new WeakReference<NavigationManager.NavigationManagerEventListener>((NavigationManager.NavigationManagerEventListener)this.navigationManagerEventListener));
        this.newManeuverEventListener = new HereNewManeuverListener(HereNavigationManager.sLogger, "[CB-NEWM]", false, this.hereNavController, this, this.bus);
        this.navigationManager.addNewInstructionEventListener(new WeakReference<NavigationManager.NewInstructionEventListener>((NavigationManager.NewInstructionEventListener)this.newManeuverEventListener));
        this.gpsSignalListener = new HereGpsSignalListener(HereNavigationManager.sLogger, "[CB-GPS]", this.bus, HereNavigationManager.mapsEventHandler, this);
        this.navigationManager.addGpsSignalListener(new WeakReference<NavigationManager.GpsSignalListener>((NavigationManager.GpsSignalListener)this.gpsSignalListener));
        this.speedWarningManager = new HereSpeedWarningManager("[CB-SPD]", this.bus, HereNavigationManager.mapsEventHandler, this);
        this.safetySpotListener = new HereSafetySpotListener(this.bus, this.mapController);
        this.navigationManager.addSafetySpotListener(new WeakReference<NavigationManager.SafetySpotListener>((NavigationManager.SafetySpotListener)this.safetySpotListener));
        this.reRouteListener = new HereRerouteListener(HereNavigationManager.sLogger, "[CB-REROUTE]", this, this.bus);
        this.navigationManager.addRerouteListener(new WeakReference<NavigationManager.RerouteListener>((NavigationManager.RerouteListener)this.reRouteListener));
        if (!DeviceUtil.isUserBuild() && MapSettings.isLaneGuidanceEnabled()) {
            this.laneInfoListener = new HereLaneInfoListener(this.bus, this.hereNavController);
        }
        this.hereRealisticViewListener = new HereRealisticViewListener(this.bus, this.hereNavController);
        this.positionUpdateListener = new HerePositionUpdateListener("[CB-POS-UPDATE]", this.hereNavController, this, this.bus);
        this.navigationManager.addPositionListener(new WeakReference<NavigationManager.PositionListener>((NavigationManager.PositionListener)this.positionUpdateListener));
        this.trafficRerouteListener = new HereTrafficRerouteListener("[CB-TRAFFIC-REROUTE]", true, this.hereNavController, this, this.bus);
        this.navdyTrafficRerouteManager = new NavdyTrafficRerouteManager(this, this.trafficRerouteListener, this.bus);
        this.trafficRerouteListener.setNavdyTrafficRerouteManager(this.navdyTrafficRerouteManager);
        this.trafficUpdater = new HereTrafficUpdater2(this.bus);
        this.trafficEtaTracker = new HereTrafficETATracker(this.hereNavController, this.bus);
        this.navigationManager.addNavigationManagerEventListener(new WeakReference<NavigationManager.NavigationManagerEventListener>((NavigationManager.NavigationManagerEventListener)this.trafficEtaTracker));
        this.setMapUpdateMode();
        this.setupNavigationTTS();
        this.setNavigationManagerUnit();
        this.driverSessionPreferences = DriverProfileHelper.getInstance().getDriverProfileManager().getSessionPreferences();
        if (this.bandwidthController.isLimitBandwidthModeOn()) {
            HereNavigationManager.sLogger.v("limitbandwidth: initial on");
            this.setBandwidthPreferences();
        }
        else {
            this.setTrafficRerouteMode(HereNavigationManager.mapsEventHandler.getNavigationPreferences().rerouteForTraffic);
        }
        this.setGeneratePhoneticTTS(Boolean.TRUE.equals(HereNavigationManager.mapsEventHandler.getNavigationPreferences().phoneticTurnByTurn));
        this.bus.register(this);
        HereNavigationManager.sLogger.v("start speed warning manager");
        this.speedWarningManager.start();
        if (!this.setNavigationMode(NavigationMode.MAP, null, null)) {
            HereNavigationManager.sLogger.e("navigation mode cannot be switched to map");
        }
    }
    
    private void postArrivedManeuver() {
        MapEvents.ManeuverDisplay arrivedManeuverDisplay;
        if ((arrivedManeuverDisplay = this.currentNavigationInfo.arrivedManeuverDisplay) == null) {
            arrivedManeuverDisplay = HereManeuverDisplayBuilder.getArrivedManeuverDisplay();
            this.currentNavigationInfo.arrivedManeuverDisplay = arrivedManeuverDisplay;
            HereNavigationManager.sLogger.v("arrival maneuver created");
        }
        this.currentNavigationInfo.lastManeuverPostTime = System.currentTimeMillis();
        this.bus.post(arrivedManeuverDisplay);
    }
    
    private void removeDestinationMarker() {
        if (this.currentNavigationInfo.mapDestinationMarker != null) {
            HereNavigationManager.sLogger.v("removed destination marker");
            this.mapController.removeMapObject(this.currentNavigationInfo.mapDestinationMarker);
            HereNavigationManager.hereMapsManager.getLocationFixManager().removeMarkers(this.mapController);
        }
    }
    
    private void setGeneratePhoneticTTS(final boolean b) {
        this.generatePhoneticTTS = b;
        NavigationManager.TtsOutputFormat ttsOutputFormat;
        if (b) {
            ttsOutputFormat = NavigationManager.TtsOutputFormat.NUANCE;
        }
        else {
            ttsOutputFormat = NavigationManager.TtsOutputFormat.RAW;
        }
        this.navigationManager.setTtsOutputFormat(ttsOutputFormat);
    }
    
    private void setNavigationManagerUnit() {
        switch (HereNavigationManager.speedManager.getSpeedUnit()) {
            default:
                this.hereNavController.setDistanceUnit(NavigationManager.UnitSystem.IMPERIAL_US);
                break;
            case KILOMETERS_PER_HOUR:
                this.hereNavController.setDistanceUnit(NavigationManager.UnitSystem.METRIC);
                break;
        }
    }
    
    private void setTrafficOverlay() {
        HereNavigationManager.sLogger.v("enableTraffic on map:" + true);
        if (true) {
            HereNavigationManager.hereMapsManager.setTrafficOverlay(this.currentNavigationMode);
        }
        else {
            HereNavigationManager.hereMapsManager.clearTrafficOverlay();
        }
    }
    
    private void setTrafficRerouteMode(final NavigationPreferences.RerouteForTraffic rerouteForTraffic) {
        this.navigationManager.setTrafficAvoidanceMode(NavigationManager.TrafficAvoidanceMode.MANUAL);
    }
    
    private void setupNavigationTTS() {
        final Locale currentLocale = HudLocale.getCurrentLocale(HudApplication.getAppContext());
        String languageTag = currentLocale.toLanguageTag();
        final String baseLanguage = HudLocale.getBaseLanguage(languageTag);
        final List<VoiceSkin> localVoiceSkins = VoiceCatalog.getInstance().getLocalVoiceSkins();
        VoiceSkin skinSupporting = null;
        final VoiceSkin voiceSkin = null;
        if (localVoiceSkins != null) {
            VoiceSkin skinSupporting2;
            for (skinSupporting2 = voiceSkin; languageTag != null && skinSupporting2 == null; skinSupporting2 = this.findSkinSupporting(localVoiceSkins, languageTag), languageTag = HereNavigationManager.LANGUAGE_HIERARCHY.get(languageTag)) {}
            VoiceSkin skinSupporting3;
            if ((skinSupporting3 = skinSupporting2) == null) {
                skinSupporting3 = this.findSkinSupporting(localVoiceSkins, baseLanguage);
            }
            if ((skinSupporting = skinSupporting3) == null) {
                skinSupporting = this.findSkinSupporting(localVoiceSkins, "en-US");
            }
        }
        if (skinSupporting == null) {
            HereNavigationManager.sLogger.e("No voice skin found for default locale:" + HereMapUtil.TBT_ISO3_LANG_CODE);
        }
        else {
            HereNavigationManager.sLogger.d("Found voice skin lang[" + skinSupporting.getLanguage() + " code[" + skinSupporting.getLanguageCode() + "] for locale:" + currentLocale);
            this.navigationManager.setVoiceSkin(skinSupporting);
            HereNavigationManager.hereMapsManager.setVoiceSkinsLoaded();
            this.audioPlayerDelegate = new AudioPlayerDelegate() {
                @Override
                public boolean playFiles(final String[] array) {
                    return true;
                }
                
                @Override
                public boolean playText(String s) {
                    HereNavigationManager.sLogger.v("[CB-TTS] [" + s + "]");
                    if (HereNavigationManager.this.currentNavigationMode == NavigationMode.MAP) {
                        HereNavigationManager.sLogger.i("[CB-TTS] not navigating");
                    }
                    else {
                        boolean b2;
                        final boolean b = b2 = false;
                        if (s != null) {
                            b2 = b;
                            if (s.toLowerCase().contains(HereNavigationManager.this.HERE_TBT_AUDIO_DESTINATION_REACHED_PATTERN)) {
                                b2 = true;
                            }
                        }
                        if (b2 && !HereNavigationManager.this.currentNavigationInfo.hasArrived) {
                            HereNavigationManager.sLogger.v("last maneuver marked arrived tts:" + HereNavigationManager.this.currentNavigationInfo.lastManeuver);
                            if (!HereNavigationManager.this.currentNavigationInfo.lastManeuver) {
                                HereNavigationManager.this.setLastManeuver();
                            }
                        }
                        if (HereNavigationManager.this.getNavigationSessionPreference().spokenTurnByTurn && !HereNavigationManager.this.currentNavigationInfo.hasArrived) {
                            String string = s;
                            if (HereNavigationManager.this.currentNavigationInfo.lastManeuver) {
                                string = s;
                                if (HereNavigationManager.this.currentNavigationInfo.destinationDirection != null) {
                                    string = s;
                                    if (HereNavigationManager.this.currentNavigationInfo.destinationDirection != MapEvents.DestinationDirection.UNKNOWN) {
                                        string = s;
                                        if (b2) {
                                            HereNavigationManager.sLogger.v("Found destination reached tts[" + s + "]");
                                            final Context access$400 = HereNavigationManager.context;
                                            if (HereNavigationManager.this.currentNavigationInfo.destinationDirection == MapEvents.DestinationDirection.LEFT) {
                                                s = HereManeuverDisplayBuilder.DESTINATION_LEFT;
                                            }
                                            else {
                                                s = HereManeuverDisplayBuilder.DESTINATION_RIGHT;
                                            }
                                            string = access$400.getString(R.string.tbt_audio_destination_reached, new Object[] { s });
                                            HereNavigationManager.sLogger.v("Converted destination reached tts[" + string + "]");
                                        }
                                    }
                                }
                            }
                            HereNavigationManager.this.bus.post(RouteCalculationNotification.CANCEL_TBT_TTS);
                            TTSUtils.sendSpeechRequest(string, SpeechRequest.Category.SPEECH_TURN_BY_TURN, RouteCalculationNotification.ROUTE_TBT_TTS_ID);
                        }
                    }
                    return true;
                }
            };
            this.navigationManager.getAudioPlayer().setDelegate(this.audioPlayerDelegate);
        }
    }
    
    private void startNavigation(final NavigationSessionRequest navigationSessionRequest, final HereRouteCache.RouteInfo routeInfo) {
        final HereNavigationInfo hereNavigationInfo = new HereNavigationInfo();
        hereNavigationInfo.navigationRouteRequest = routeInfo.routeRequest;
        hereNavigationInfo.startLocation = routeInfo.routeStartPoint;
        hereNavigationInfo.routeOptions = HereNavigationManager.hereMapsManager.getRouteOptions();
        hereNavigationInfo.waypoints = new ArrayList<GeoCoordinate>();
        for (final Coordinate coordinate : routeInfo.routeRequest.waypoints) {
            hereNavigationInfo.waypoints.add(new GeoCoordinate(coordinate.latitude, coordinate.longitude));
        }
        hereNavigationInfo.destination = new GeoCoordinate(routeInfo.routeRequest.destination.latitude, routeInfo.routeRequest.destination.longitude);
        hereNavigationInfo.route = routeInfo.route;
        hereNavigationInfo.simulationSpeed = navigationSessionRequest.simulationSpeed;
        hereNavigationInfo.routeId = navigationSessionRequest.routeId;
        hereNavigationInfo.deviceId = RemoteDeviceManager.getInstance().getDeviceId();
        final NavdyDeviceId lastKnownDeviceId = HereMapUtil.getLastKnownDeviceId();
        if (hereNavigationInfo.deviceId == null) {
            hereNavigationInfo.deviceId = lastKnownDeviceId;
            HereNavigationManager.sLogger.v("lastknown device id = " + hereNavigationInfo.deviceId);
        }
        if (hereNavigationInfo.deviceId == null) {
            final DeviceInfo lastConnectedDeviceInfo = RemoteDeviceManager.getInstance().getConnectionHandler().getLastConnectedDeviceInfo();
            if (lastConnectedDeviceInfo != null) {
                hereNavigationInfo.deviceId = new NavdyDeviceId(lastConnectedDeviceInfo.deviceId);
                HereNavigationManager.sLogger.v("device id is null , adding last device:" + lastConnectedDeviceInfo.deviceId);
            }
        }
        if (routeInfo.routeRequest.destination_identifier != null) {
            hereNavigationInfo.destinationIdentifier = routeInfo.routeRequest.destination_identifier;
        }
        String streetAddress = null;
        final NavigationRouteRequest navigationRouteRequest = hereNavigationInfo.navigationRouteRequest;
        if (navigationRouteRequest != null) {
            streetAddress = navigationRouteRequest.streetAddress;
            hereNavigationInfo.destinationLabel = navigationRouteRequest.label;
        }
        hereNavigationInfo.streetAddress = HereMapUtil.parseStreetAddress(streetAddress);
        final StringBuilder sb = new StringBuilder();
        if (this.setNavigationMode(NavigationMode.MAP_ON_ROUTE, hereNavigationInfo, sb)) {
            this.returnResponse(RequestStatus.REQUEST_SUCCESS, null, this.currentSessionState, navigationSessionRequest.routeId);
        }
        else {
            this.returnResponse(RequestStatus.REQUEST_SERVICE_ERROR, sb.toString(), this.currentSessionState, navigationSessionRequest.routeId);
        }
    }
    
    private void tearDownCurrentRoute() {
        this.removeCurrentRoute();
        this.removeDestinationMarker();
        this.currentNavigationInfo.mapDestinationMarker = null;
        this.trafficUpdater.setRoute(null);
        this.trafficRerouteListener.dismissReroute();
        this.bus.post(new MapEvents.TrafficRerouteDismissEvent());
        this.bus.post(new MapEvents.LiveTrafficDismissEvent());
    }
    
    private void updateNavigationInfoInternal(Maneuver currentManeuver, Maneuver maneuver, Maneuver prevManeuver, boolean currentRoadHighway) {
        while (true) {
            // monitorenter(this)
            boolean b = true;
            try {
                if (this.currentNavigationInfo.hasArrived) {
                    if (this.currentNavigationInfo.ignoreArrived) {
                        HereNavigationManager.sLogger.v("ignore arrived");
                    }
                    else {
                        try {
                            final GeoPosition lastGeoPosition = HereNavigationManager.hereMapsManager.getLastGeoPosition();
                            if (lastGeoPosition != null) {
                                final int n = (int)lastGeoPosition.getCoordinate().distanceTo(this.currentNavigationInfo.destination);
                                if (n >= 804) {
                                    final Logger sLogger = HereNavigationManager.sLogger;
                                    maneuver = (Maneuver)new StringBuilder();
                                    sLogger.v(((StringBuilder)maneuver).append("we have exceeded geo threshold:").append(n).append(" , stopping nav").toString());
                                    this.stopNavigation();
                                    return;
                                }
                            }
                        }
                        catch (Throwable t) {
                            HereNavigationManager.sLogger.e(t);
                        }
                        this.postArrivedManeuver();
                    }
                    return;
                }
            }
            finally {
            }
            // monitorexit(this)
            if (this.currentSessionState == NavigationSessionState.NAV_SESSION_STARTED || this.currentSessionState == NavigationSessionState.NAV_SESSION_PAUSED) {
                final Maneuver currentManeuver2;
                if (currentManeuver2 == null) {
                    currentManeuver = this.currentManeuver;
                    maneuver = this.maneuverAfterCurrent;
                    maneuver = this.prevManeuver;
                    currentRoadHighway = b;
                    maneuver = currentManeuver;
                    if (this.maneuverAfterCurrent != null) {
                        currentRoadHighway = b;
                        maneuver = currentManeuver;
                        if (this.currentNavigationInfo.maneuverAfterCurrent == this.maneuverAfterCurrent) {
                            currentRoadHighway = b;
                            maneuver = currentManeuver;
                            if (this.currentNavigationInfo.maneuverState != null) {
                                currentRoadHighway = b;
                                maneuver = currentManeuver;
                                if (this.currentNavigationInfo.maneuverState != HereManeuverDisplayBuilder.ManeuverState.STAY) {
                                    currentRoadHighway = false;
                                    maneuver = currentManeuver;
                                }
                            }
                        }
                    }
                }
                else {
                    if (currentRoadHighway) {
                        HereNavigationManager.sLogger.v("setNewRoute: cleared");
                        this.prevManeuver = null;
                    }
                    else {
                        this.prevManeuver = this.currentManeuver;
                    }
                    this.currentManeuver = currentManeuver2;
                    this.maneuverAfterCurrent = maneuver;
                    this.currentNavigationInfo.maneuverAfterCurrent = maneuver;
                    this.currentNavigationInfo.maneuverAfterCurrentIconid = -1;
                    this.currentNavigationInfo.maneuverState = null;
                    currentRoadHighway = b;
                    maneuver = currentManeuver2;
                }
                if (maneuver != null) {
                    b = false;
                    if (maneuver.getAction() == Maneuver.Action.END || maneuver.getIcon() == Maneuver.Icon.END) {
                        b = true;
                    }
                    final boolean b2 = false;
                    int n3;
                    final int n2 = n3 = 0;
                    boolean b3 = b2;
                    if (b) {
                        if (this.currentNavigationInfo.destinationDirection != null) {
                            b3 = true;
                            n3 = n2;
                        }
                        else {
                            n3 = 1;
                            b3 = b2;
                        }
                    }
                    final long nextManeuverDistance = this.hereNavController.getNextManeuverDistance();
                    final Maneuver currentManeuver3 = this.currentManeuver;
                    prevManeuver = this.prevManeuver;
                    NavigationRouteRequest navigationRouteRequest;
                    if (n3 != 0) {
                        navigationRouteRequest = this.currentNavigationInfo.navigationRouteRequest;
                    }
                    else {
                        navigationRouteRequest = null;
                    }
                    final MapEvents.ManeuverDisplay maneuverDisplay = HereManeuverDisplayBuilder.getManeuverDisplay(currentManeuver3, b, nextManeuverDistance, null, prevManeuver, navigationRouteRequest, this.maneuverAfterCurrent, currentRoadHighway, true, this.currentNavigationInfo.destinationDirection);
                    if (!b) {
                        if (this.debugManeuverState != null) {
                            maneuverDisplay.maneuverState = this.debugManeuverState;
                        }
                        if (this.debugManeuverInstruction != null) {
                            maneuverDisplay.pendingRoad = this.debugManeuverInstruction;
                        }
                        if (this.debugManeuverIcon != 0) {
                            maneuverDisplay.turnIconId = this.debugManeuverIcon;
                        }
                        if (this.debugManeuverDistance != null) {
                            maneuverDisplay.distanceToPendingRoadText = this.debugManeuverDistance;
                        }
                        if (this.debugNextManeuverIcon != 0) {
                            maneuverDisplay.nextTurnIconId = this.debugNextManeuverIcon;
                        }
                    }
                    if (b && !this.currentNavigationInfo.lastManeuver) {
                        HereNavigationManager.sLogger.v("last maneuver marked distance:" + nextManeuverDistance);
                        this.setLastManeuver();
                    }
                    if (b) {
                        if (b3) {
                            if (this.currentNavigationInfo.destinationDirection != MapEvents.DestinationDirection.UNKNOWN) {
                                maneuverDisplay.pendingRoad = this.currentNavigationInfo.destinationDirectionStr;
                                maneuverDisplay.destinationIconId = this.currentNavigationInfo.destinationIconId;
                            }
                        }
                        else {
                            this.currentNavigationInfo.destinationDirection = maneuverDisplay.direction;
                            this.currentNavigationInfo.destinationDirectionStr = maneuverDisplay.pendingRoad;
                            this.currentNavigationInfo.destinationIconId = maneuverDisplay.destinationIconId;
                            if (this.currentNavigationInfo.destinationDirection != MapEvents.DestinationDirection.UNKNOWN) {
                                HereNavigationManager.sLogger.v("we have destination marker info: " + this.currentNavigationInfo.destinationDirection);
                                this.removeDestinationMarker();
                                this.currentNavigationInfo.mapDestinationMarker = null;
                                if (this.currentNavigationInfo.route != null) {
                                    this.addDestinationMarker(this.currentNavigationInfo.route.getDestination(), this.currentNavigationInfo.destinationIconId);
                                }
                            }
                        }
                    }
                    if (this.currentNavigationInfo.maneuverAfterCurrentIconid == -1) {
                        this.currentNavigationInfo.maneuverAfterCurrentIconid = maneuverDisplay.nextTurnIconId;
                        this.currentNavigationInfo.maneuverState = maneuverDisplay.maneuverState;
                    }
                    else {
                        maneuverDisplay.nextTurnIconId = this.currentNavigationInfo.maneuverAfterCurrentIconid;
                    }
                    Date etaDate;
                    if (!HereMapUtil.isValidEtaDate(etaDate = this.hereNavController.getEta(true, Route.TrafficPenaltyMode.OPTIMAL))) {
                        etaDate = this.hereNavController.getTtaDate(true, Route.TrafficPenaltyMode.OPTIMAL);
                        if (!HereMapUtil.isValidEtaDate(etaDate)) {
                            HereNavigationManager.sLogger.v("intermediate maneuver eta-tta date invalid = " + etaDate);
                            etaDate = null;
                        }
                        else {
                            HereNavigationManager.sLogger.v("intermediate maneuver eta-tta date = " + etaDate);
                        }
                    }
                    if (etaDate != null) {
                        maneuverDisplay.eta = this.timeHelper.formatTime(etaDate, this.stringBuilder);
                        maneuverDisplay.etaAmPm = this.stringBuilder.toString();
                        maneuverDisplay.etaDate = etaDate;
                    }
                    currentRoadHighway = false;
                    final RoadElement currentRoadElement = HereMapUtil.getCurrentRoadElement();
                    if (currentRoadElement != null) {
                        currentRoadHighway = HereMapUtil.isCurrentRoadHighway(currentRoadElement);
                    }
                    if (HereManeuverDisplayBuilder.getManeuverState(this.hereNavController.getNextManeuverDistance(), currentRoadHighway) == HereManeuverDisplayBuilder.ManeuverState.SOON) {
                        this.bus.post(new MapEvents.ManeuverSoonEvent(maneuver));
                    }
                    if (HereNavigationManager.sLogger.isLoggable(2)) {
                        HereNavigationManager.sLogger.d("ManeuverDisplay-nav:" + this.getHereNavigationState() + " :" + maneuverDisplay.toString());
                        final Logger sLogger2 = HereNavigationManager.sLogger;
                        maneuver = (Maneuver)new StringBuilder();
                        sLogger2.v(((StringBuilder)maneuver).append("[ManeuverDisplay-nav-short] ").append(maneuverDisplay.pendingTurn).append(" ").append(maneuverDisplay.pendingRoad).toString());
                    }
                    this.currentNavigationInfo.lastManeuverPostTime = System.currentTimeMillis();
                    this.bus.post(maneuverDisplay);
                    return;
                }
                HereNavigationManager.sLogger.w("there is no next maneuver");
            }
            else {
                if (this.currentSessionState == NavigationSessionState.NAV_SESSION_STOPPED) {
                    this.prevManeuver = null;
                    this.currentManeuver = null;
                    this.maneuverAfterCurrent = null;
                    maneuver = (Maneuver)new MapEvents.ManeuverDisplay();
                    ((MapEvents.ManeuverDisplay)maneuver).currentRoad = HereMapUtil.getCurrentRoadName();
                    ((MapEvents.ManeuverDisplay)maneuver).currentSpeedLimit = HereMapUtil.getCurrentSpeedLimit();
                    if (HereNavigationManager.sLogger.isLoggable(2)) {
                        HereNavigationManager.sLogger.d("ManeuverDisplay:" + this.getHereNavigationState() + " :" + ((MapEvents.ManeuverDisplay)maneuver).toString());
                    }
                    this.bus.post(maneuver);
                    return;
                }
                this.prevManeuver = null;
                this.currentManeuver = null;
                this.maneuverAfterCurrent = null;
                final Logger sLogger3 = HereNavigationManager.sLogger;
                maneuver = (Maneuver)new StringBuilder();
                final Maneuver currentManeuver2;
                sLogger3.e(((StringBuilder)maneuver).append("Invalid state:").append(this.currentSessionState).append(" maneuver=").append(currentManeuver2).toString());
            }
        }
    }
    
    public void addCurrentRoute(final MapRoute mapRoute) {
        synchronized (this) {
            try {
                if (this.currentNavigationInfo.mapRoute != null) {
                    this.removeCurrentRoute();
                }
                if (mapRoute != null) {
                    this.currentNavigationInfo.mapRoute = mapRoute;
                    this.mapController.addMapObject(mapRoute);
                    HereNavigationManager.sLogger.v("addCurrentRoute:" + mapRoute);
                }
            }
            catch (Throwable t) {
                HereNavigationManager.sLogger.e(t);
            }
        }
    }
    
    public NavigationManager.Error addNewArrivalRoute(final ArrayList<NavigationRouteResult> list) {
        GenericUtil.checkNotOnMainThread();
        return this.addNewRoute(list, NavigationSessionRouteChange.RerouteReason.NAV_SESSION_ARRIVAL_REROUTE, null);
    }
    
    public NavigationManager.Error addNewRoute(final ArrayList<NavigationRouteResult> list, final NavigationSessionRouteChange.RerouteReason rerouteReason, final NavigationRouteRequest navigationRouteRequest) {
        GenericUtil.checkNotOnMainThread();
        return this.addNewRoute(list, rerouteReason, navigationRouteRequest, -1L);
    }
    
    public NavigationManager.Error addNewRoute(final ArrayList<NavigationRouteResult> list, final NavigationSessionRouteChange.RerouteReason rerouteReason, final NavigationRouteRequest navigationRouteRequest, final long n) {
        GenericUtil.checkNotOnMainThread();
        Enum<NavigationManager.Error> enum1;
        if (list != null && list.size() > 0) {
            HereNavigationManager.sLogger.v("addNewRoute calculated=" + rerouteReason);
            final String routeId = list.get(0).routeId;
            final HereRouteCache.RouteInfo route = HereRouteCache.getInstance().getRoute(routeId);
            if (route != null) {
                this.updateMapRoute(route.route, rerouteReason, navigationRouteRequest, null, null, false, route.traffic);
                HereNavigationManager.sLogger.i("setNavigationMode: set transition mode");
                enum1 = this.hereNavController.startNavigation(route.route, (int)n);
                if (enum1 == NavigationManager.Error.NONE) {
                    HereNavigationManager.sLogger.v("addNewRoute navigation started");
                }
                else {
                    HereNavigationManager.sLogger.e("addNewRoute navigation started failed:" + enum1);
                    this.stopNavigation();
                }
            }
            else {
                HereNavigationManager.sLogger.e("addNewRoute calc could not found route:" + routeId);
                this.stopNavigation();
                enum1 = NavigationManager.Error.NOT_FOUND;
            }
        }
        else {
            HereNavigationManager.sLogger.e("addNewRoute calc no result, end trip:");
            this.stopNavigation();
            enum1 = NavigationManager.Error.NOT_FOUND;
        }
        return (NavigationManager.Error)enum1;
    }
    
    public void arrived() {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    Label_0286: {
                        synchronized (this) {
                            if (HereNavigationManager.this.isNavigationModeOn()) {
                                HereNavigationManager.sLogger.v("arrived = true");
                                HereNavigationManager.this.hereNavController.arrivedAtDestination();
                                HereMapUtil.removeRouteInfo(HereNavigationManager.sLogger, false);
                                if (!HereNavigationManager.this.currentNavigationInfo.navigationRouteRequest.routeAttributes.contains(NavigationRouteRequest.RouteAttribute.ROUTE_ATTRIBUTE_GAS)) {
                                    HereNavigationManager.sLogger.v("arrived : regular");
                                    if (!HereNavigationManager.this.currentNavigationInfo.hasArrived) {
                                        HereNavigationManager.this.bus.post(new MapEvents.ManeuverEvent(MapEvents.ManeuverEvent.Type.LAST, null));
                                    }
                                    HereNavigationManager.this.currentNavigationInfo.hasArrived = true;
                                    HereNavigationManager.this.currentNavigationInfo.ignoreArrived = false;
                                    HereNavigationManager.this.bus.post(HereNavigationManager.ARRIVAL_EVENT);
                                    HereNavigationManager.this.postArrivedManeuver();
                                    HereNavigationManager.this.removeCurrentRoute();
                                    HereNavigationManager.this.currentSessionState = NavigationSessionState.NAV_SESSION_STOPPED;
                                    HereNavigationManager.sLogger.v("arrived: session stopped");
                                    HereNavigationManager.this.postNavigationSessionStatusEvent(NavigationSessionState.NAV_SESSION_ARRIVED, false);
                                    HereNavigationManager.this.postNavigationSessionStatusEvent(false);
                                    HereNavigationManager.sLogger.v("arrived: posted arrival maneuver");
                                }
                                else {
                                    HereNavigationManager.sLogger.v("arrived : gas");
                                    final NavigationRouteRequest navigationRouteRequest = HereMapUtil.getSavedRouteData(HereNavigationManager.sLogger).navigationRouteRequest;
                                    HereNavigationManager.this.setNavigationMode(NavigationMode.MAP, null, null);
                                    if (navigationRouteRequest == null) {
                                        break Label_0286;
                                    }
                                    HereNavigationManager.sLogger.v("arrived : gas, has next route");
                                    HereNavigationManager.this.handler.post((Runnable)new Runnable() {
                                        @Override
                                        public void run() {
                                            HereNavigationManager.this.bus.post(navigationRouteRequest);
                                            HereNavigationManager.this.bus.post(new RemoteEvent(navigationRouteRequest));
                                        }
                                    });
                                }
                            }
                            return;
                        }
                    }
                    HereNavigationManager.sLogger.v("arrived : gas, no next route");
                }
            }
        }, 20);
    }
    
    public void clearNavManeuver() {
        this.newManeuverEventListener.clearNavManeuver();
    }
    
    public String getCurrentDestinationIdentifier() {
        return this.currentNavigationInfo.destinationIdentifier;
    }
    
    public MapRoute getCurrentMapRoute() {
        return this.currentNavigationInfo.mapRoute;
    }
    
    public NavdyDeviceId getCurrentNavigationDeviceId() {
        return this.currentNavigationInfo.deviceId;
    }
    
    public NavigationMode getCurrentNavigationMode() {
        return this.currentNavigationMode;
    }
    
    public NavigationRouteRequest getCurrentNavigationRouteRequest() {
        return this.currentNavigationInfo.navigationRouteRequest;
    }
    
    public NavigationSessionState getCurrentNavigationState() {
        return this.currentSessionState;
    }
    
    public NavigationSessionRouteChange.RerouteReason getCurrentRerouteReason() {
        return this.currentNavigationInfo.currentRerouteReason;
    }
    
    public Route getCurrentRoute() {
        return this.currentNavigationInfo.route;
    }
    
    public String getCurrentRouteId() {
        return this.currentNavigationInfo.routeId;
    }
    
    public int getCurrentTrafficRerouteCount() {
        return this.currentNavigationInfo.getTrafficReRouteCount();
    }
    
    public String getCurrentVia() {
        final String s = null;
        final String currentRouteId = this.getCurrentRouteId();
        String via = s;
        if (currentRouteId != null) {
            final HereRouteCache.RouteInfo route = HereRouteCache.getInstance().getRoute(currentRouteId);
            via = s;
            if (route != null) {
                via = s;
                if (route.routeResult != null) {
                    via = route.routeResult.via;
                }
            }
        }
        return via;
    }
    
    public String getDestinationLabel() {
        return this.currentNavigationInfo.destinationLabel;
    }
    
    public MapMarker getDestinationMarker() {
        return this.currentNavigationInfo.mapDestinationMarker;
    }
    
    public String getDestinationStreetAddress() {
        return this.currentNavigationInfo.streetAddress;
    }
    
    public long getFirstManeuverShowntime() {
        return this.currentNavigationInfo.firstManeuverShownTime;
    }
    
    public String getFullStreetAddress() {
        final NavigationRouteRequest navigationRouteRequest = this.currentNavigationInfo.navigationRouteRequest;
        String streetAddress;
        if (navigationRouteRequest != null) {
            streetAddress = navigationRouteRequest.streetAddress;
        }
        else {
            streetAddress = null;
        }
        return streetAddress;
    }
    
    public Boolean getGeneratePhoneticTTS() {
        return this.generatePhoneticTTS;
    }
    
    public NavigationManager.NavigationMode getHereNavigationState() {
        GenericUtil.checkNotOnMainThread();
        return this.navigationManager.getNavigationMode();
    }
    
    public long getLastTrafficRerouteTime() {
        return this.currentNavigationInfo.getLastTrafficRerouteTime();
    }
    
    public HereNavController getNavController() {
        return this.hereNavController;
    }
    
    public Maneuver getNavManeuver() {
        return this.newManeuverEventListener.getNavManeuver();
    }
    
    public NavSessionPreferences getNavigationSessionPreference() {
        return this.driverSessionPreferences.getNavigationSessionPreference();
    }
    
    public HereNavController.State getNavigationState() {
        return this.hereNavController.getState();
    }
    
    public NavigationView getNavigationView() {
        return this.navigationView;
    }
    
    public Maneuver getNextManeuver() {
        Maneuver currentManeuver;
        if (this.isNavigationModeOn()) {
            currentManeuver = this.currentManeuver;
        }
        else {
            currentManeuver = null;
        }
        return currentManeuver;
    }
    
    public Maneuver getPrevManeuver() {
        Maneuver prevManeuver;
        if (this.isNavigationModeOn()) {
            prevManeuver = this.prevManeuver;
        }
        else {
            prevManeuver = null;
        }
        return prevManeuver;
    }
    
    public int getRerouteInterval() {
        int n;
        if (this.bandwidthController.isLimitBandwidthModeOn()) {
            n = HereTrafficRerouteListener.ROUTE_CHECK_INTERVAL_LIMIT_BANDWIDTH;
        }
        else {
            n = HereTrafficRerouteListener.ROUTE_CHECK_INTERVAL;
        }
        return n;
    }
    
    public RouteOptions getRouteOptions() {
        return this.currentNavigationInfo.routeOptions;
    }
    
    public HereSafetySpotListener getSafetySpotListener() {
        return this.safetySpotListener;
    }
    
    public GeoCoordinate getStartLocation() {
        return this.currentNavigationInfo.startLocation;
    }
    
    public void handleConnectionState(final boolean b) {
        if (!b) {
            HereNavigationManager.sLogger.i("client not connected");
        }
        else {
            HereNavigationManager.sLogger.i("client connected");
            if (this.isNavigationModeOn()) {
                final NavdyDeviceId deviceId = RemoteDeviceManager.getInstance().getDeviceId();
                if (deviceId != null && !deviceId.equals(this.currentNavigationInfo.deviceId)) {
                    TaskManager.getInstance().execute(new Runnable() {
                        @Override
                        public void run() {
                            HereNavigationManager.sLogger.i("stop navigation, device id has changed from[" + HereNavigationManager.this.currentNavigationInfo.deviceId + "] to [" + deviceId + "]");
                            HereNavigationManager.this.setNavigationMode(NavigationMode.MAP, null, null);
                        }
                    }, 20);
                }
                else {
                    HereNavigationManager.sLogger.i("navigation still on, device id has not changed");
                }
            }
        }
    }
    
    public void handleNavigationSessionRequest(final NavigationSessionRequest navigationSessionRequest) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                HereNavigationManager.this.handleNavigationSessionRequestInternal(navigationSessionRequest);
            }
        }, 20);
    }
    
    public boolean hasArrived() {
        return this.isNavigationModeOn() && this.currentNavigationInfo.hasArrived;
    }
    
    public boolean hasTrafficRerouteOnce() {
        return this.currentNavigationInfo.trafficRerouteOnce;
    }
    
    public void incrCurrentTrafficRerouteCount() {
        this.currentNavigationInfo.incrTrafficRerouteCount();
    }
    
    public boolean isLastManeuver() {
        return this.currentNavigationInfo.lastManeuver;
    }
    
    public boolean isNavigationModeOn() {
        return this.hereNavController.getState() == HereNavController.State.NAVIGATING;
    }
    
    public boolean isOnGasRoute() {
        boolean contains;
        final boolean b = contains = false;
        if (this.isNavigationModeOn()) {
            final NavigationRouteRequest navigationRouteRequest = this.currentNavigationInfo.navigationRouteRequest;
            if (navigationRouteRequest == null) {
                contains = b;
            }
            else {
                contains = b;
                if (navigationRouteRequest.routeAttributes != null) {
                    contains = navigationRouteRequest.routeAttributes.contains(NavigationRouteRequest.RouteAttribute.ROUTE_ATTRIBUTE_GAS);
                }
            }
        }
        return contains;
    }
    
    public boolean isRerouting() {
        return this.reRouteListener.isRecalculating();
    }
    
    public boolean isShownFirstManeuver() {
        return this.currentNavigationInfo.firstManeuverShown;
    }
    
    public boolean isSwitchingToNewRoute() {
        return this.switchingToNewRoute;
    }
    
    public boolean isTrafficConsidered(final String s) {
        while (true) {
            try {
                final HereRouteCache.RouteInfo route = HereRouteCache.getInstance().getRoute(s);
                if (route != null) {
                    return route.traffic;
                }
            }
            catch (Throwable t) {
                HereNavigationManager.sLogger.e(t);
            }
            return false;
        }
    }
    
    public boolean isTrafficUpdaterRunning() {
        return this.trafficUpdater.isRunning();
    }
    
    @Subscribe
    public void onDriverProfileChanged(final DriverProfileChanged driverProfileChanged) {
        HereNavigationManager.sLogger.v("driver profile changed");
    }
    
    @Subscribe
    public void onNavigationPreferences(final NavigationPreferences navigationPreferences) {
        this.setGeneratePhoneticTTS(Boolean.TRUE.equals(navigationPreferences.phoneticTurnByTurn));
    }
    
    @Subscribe
    public void onNotificationPreference(final NotificationPreferences notificationPreferences) {
        if (notificationPreferences.enabled) {
            if (GlanceHelper.isTrafficNotificationEnabled()) {
                HereNavigationManager.sLogger.v("glances, enabled, turn traffic notif on if not on");
                this.startTrafficRerouteListener();
            }
            else {
                HereNavigationManager.sLogger.v("traffic glances disabled, turn traffic notif off");
                this.resetTrafficRerouteListener();
                this.stopTrafficRerouteListener();
            }
        }
        else {
            HereNavigationManager.sLogger.v("glances, disabled, turn traffic notif off");
            this.resetTrafficRerouteListener();
            this.stopTrafficRerouteListener();
        }
    }
    
    @Subscribe
    public void onSpeedUnitChanged(final SpeedManager.SpeedUnitChanged speedUnitChanged) {
        HereNavigationManager.sLogger.i("speed unit setting has changed to " + HereNavigationManager.speedManager.getSpeedUnit().name() + ", refreshing");
        this.refreshNavigationInfo();
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                HereNavigationManager.this.setNavigationManagerUnit();
            }
        }, 3);
    }
    
    @Subscribe
    public void onTimeSettingsChange(final TimeHelper.UpdateClock updateClock) {
        HereNavigationManager.sLogger.v("time setting changed");
        if (this.isNavigationModeOn()) {
            this.refreshNavigationInfo();
        }
    }
    
    @Subscribe
    public void onTrafficRerouteConfirm(final MapEvents.TrafficRerouteAction trafficRerouteAction) {
        if (trafficRerouteAction == MapEvents.TrafficRerouteAction.REROUTE) {
            this.trafficRerouteListener.confirmReroute();
        }
        else {
            this.trafficRerouteListener.dismissReroute();
        }
    }
    
    public void postManeuverDisplay() {
        this.refreshNavigationInfo();
    }
    
    public void postNavigationSessionStatusEvent(final NavigationSessionState navigationSessionState, final boolean b) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                String s = null;
                final NavigationRouteResult navigationRouteResult = null;
                String s2 = null;
                final NavigationRouteRequest navigationRouteRequest = HereNavigationManager.this.currentNavigationInfo.navigationRouteRequest;
                NavigationRouteResult build = navigationRouteResult;
                if (navigationRouteRequest != null) {
                    final String label = navigationRouteRequest.label;
                    final String routeId = HereNavigationManager.this.currentNavigationInfo.routeId;
                    s = label;
                    s2 = routeId;
                    build = navigationRouteResult;
                    if (b) {
                        final HereRouteCache.RouteInfo route = HereRouteCache.getInstance().getRoute(routeId);
                        s = label;
                        s2 = routeId;
                        build = navigationRouteResult;
                        if (route != null) {
                            final NavigationRouteResult routeResult = route.routeResult;
                            int intValue = routeResult.duration;
                            int intValue2 = routeResult.duration_traffic;
                            final Date eta = HereNavigationManager.this.hereNavController.getEta(true, Route.TrafficPenaltyMode.DISABLED);
                            final Date eta2 = HereNavigationManager.this.hereNavController.getEta(true, Route.TrafficPenaltyMode.OPTIMAL);
                            final long currentTimeMillis = System.currentTimeMillis();
                            if (HereMapUtil.isValidEtaDate(eta)) {
                                intValue = (int)(eta.getTime() - currentTimeMillis) / 1000;
                            }
                            if (HereMapUtil.isValidEtaDate(eta2)) {
                                intValue2 = (int)(eta2.getTime() - currentTimeMillis) / 1000;
                            }
                            final int n = (int)HereNavigationManager.this.hereNavController.getDestinationDistance();
                            build = new NavigationRouteResult.Builder(routeResult).duration(intValue).duration_traffic(intValue2).length(n).build();
                            HereNavigationManager.sLogger.v("NavigationSessionStatusEvent new data duration[" + intValue + "] traffic[" + intValue2 + "] length[" + n + "]");
                            s2 = routeId;
                            s = label;
                        }
                    }
                }
                String label2 = s;
                String lastRouteId;
                if ((lastRouteId = s2) == null) {
                    label2 = s;
                    lastRouteId = s2;
                    if (navigationSessionState == NavigationSessionState.NAV_SESSION_STOPPED) {
                        final NavigationRouteRequest lastNavigationRequest = HereNavigationManager.this.currentNavigationInfo.getLastNavigationRequest();
                        label2 = s;
                        lastRouteId = s2;
                        if (lastNavigationRequest != null) {
                            label2 = lastNavigationRequest.label;
                            lastRouteId = HereNavigationManager.this.currentNavigationInfo.getLastRouteId();
                        }
                    }
                }
                final NavigationSessionStatusEvent navigationSessionStatusEvent = new NavigationSessionStatusEvent(navigationSessionState, label2, lastRouteId, build, HereNavigationManager.this.currentNavigationInfo.destinationIdentifier);
                HereNavigationManager.mapsEventHandler.sendEventToClient(navigationSessionStatusEvent);
                final Logger sLogger = HereNavigationManager.sLogger;
                final StringBuilder append = new StringBuilder().append("posted NavigationSessionStatusEvent state[").append(navigationSessionStatusEvent.sessionState).append("] label[").append(navigationSessionStatusEvent.label).append("] routeId[").append(navigationSessionStatusEvent.routeId).append("]").append("] result[");
                String via;
                if (build == null) {
                    via = "null";
                }
                else {
                    via = build.via;
                }
                sLogger.v(append.append(via).append("]").toString());
            }
        }, 2);
    }
    
    public void postNavigationSessionStatusEvent(final boolean b) {
        this.postNavigationSessionStatusEvent(this.currentSessionState, b);
    }
    
    void refreshNavigationInfo() {
        this.updateNavigationInfo(null, null, null, false);
    }
    
    public boolean removeCurrentRoute() {
        synchronized (this) {
            while (true) {
                try {
                    final MapRoute mapRoute = this.currentNavigationInfo.mapRoute;
                    if (mapRoute != null) {
                        HereNavigationManager.sLogger.v("removed map route:" + mapRoute);
                        this.currentNavigationInfo.mapRoute = null;
                        this.mapController.removeMapObject(mapRoute);
                        return true;
                    }
                }
                catch (Throwable t) {
                    HereNavigationManager.sLogger.e(t);
                }
                return false;
            }
        }
    }
    
    public void resetTrafficRerouteListener() {
        final Object trafficRerouteLock = HereNavigationManager.trafficRerouteLock;
        synchronized (trafficRerouteLock) {
            this.navdyTrafficRerouteManager.reset();
        }
    }
    
    protected void returnResponse(final RequestStatus requestStatus, final String s, final NavigationSessionState navigationSessionState, final String s2) {
        if (requestStatus != RequestStatus.REQUEST_SUCCESS) {
            HereNavigationManager.sLogger.e(requestStatus + ": " + s);
        }
        else {
            HereNavigationManager.sLogger.i("Success");
        }
        HereNavigationManager.mapsEventHandler.sendEventToClient(new NavigationSessionResponse(requestStatus, s, navigationSessionState, s2));
    }
    
    public void sendStartManeuver(final Route route) {
        GenericUtil.checkNotOnMainThread();
        if (route != null) {
            final List<Maneuver> maneuvers = route.getManeuvers();
            if (maneuvers.size() >= 2) {
                final MapEvents.ManeuverDisplay startManeuverDisplay = HereManeuverDisplayBuilder.getStartManeuverDisplay(maneuvers.get(0), maneuvers.get(1));
                final Date eta = this.hereNavController.getEta(true, Route.TrafficPenaltyMode.OPTIMAL);
                Date routeTtaDate;
                if (!HereMapUtil.isValidEtaDate(eta)) {
                    final Date ttaDate = this.hereNavController.getTtaDate(true, Route.TrafficPenaltyMode.OPTIMAL);
                    if (HereMapUtil.isValidEtaDate(ttaDate)) {
                        HereNavigationManager.sLogger.v("start maneuver eta-tta date = " + ttaDate);
                        routeTtaDate = ttaDate;
                    }
                    else {
                        routeTtaDate = HereMapUtil.getRouteTtaDate(route);
                        HereNavigationManager.sLogger.v("start maneuver tta date = " + routeTtaDate);
                    }
                }
                else {
                    HereNavigationManager.sLogger.v("start maneuver eta date = " + eta);
                    routeTtaDate = eta;
                }
                if (routeTtaDate != null) {
                    startManeuverDisplay.etaDate = routeTtaDate;
                    startManeuverDisplay.eta = this.timeHelper.formatTime(routeTtaDate, this.stringBuilder);
                    startManeuverDisplay.etaAmPm = this.stringBuilder.toString();
                }
                if (HereNavigationManager.sLogger.isLoggable(2)) {
                    HereNavigationManager.sLogger.d("ManeuverDisplay:START:" + this.getHereNavigationState() + " :" + startManeuverDisplay.toString());
                }
                this.currentNavigationInfo.lastManeuverPostTime = System.currentTimeMillis();
                this.bus.post(startManeuverDisplay);
            }
            else {
                HereNavigationManager.sLogger.i("start maneuver not sent");
            }
        }
    }
    
    void setBandwidthPreferences() {
        final boolean limitBandwidthModeOn = this.bandwidthController.isLimitBandwidthModeOn();
        final DriverProfileManager driverProfileManager = DriverProfileHelper.getInstance().getDriverProfileManager();
        Label_0097: {
            if (!limitBandwidthModeOn) {
                break Label_0097;
            }
            HereNavigationManager.sLogger.v("limitbandwidth: on");
            HereNavigationManager.sLogger.v("limitbandwidth: turn map traffic off");
            HereNavigationManager.hereMapsManager.clearMapTraffic();
            driverProfileManager.disableTraffic();
            HereNavigationManager.sLogger.v("limitbandwidth: turn HERE traffic listener off");
            final Object trafficRerouteLock = HereNavigationManager.trafficRerouteLock;
            synchronized (trafficRerouteLock) {
                this.trafficRerouteListener.stop();
                // monitorexit(trafficRerouteLock)
                TaskManager.getInstance().execute(new Runnable() {
                    @Override
                    public void run() {
                        HereNavigationManager.sLogger.v("limitbandwidth: turn off HERE traffic avoidance mode");
                        HereNavigationManager.this.hereNavController.setTrafficAvoidanceMode(NavigationManager.TrafficAvoidanceMode.DISABLE);
                    }
                }, 3);
                return;
            }
        }
        HereNavigationManager.sLogger.v("limitbandwidth: off");
        HereNavigationManager.sLogger.v("limitbandwidth: turn map traffic on");
        HereNavigationManager.hereMapsManager.setTrafficOverlay(this.getCurrentNavigationMode());
        driverProfileManager.enableTraffic();
        final Object trafficRerouteLock2 = HereNavigationManager.trafficRerouteLock;
        synchronized (trafficRerouteLock2) {
            if (GlanceHelper.isTrafficNotificationEnabled()) {
                HereNavigationManager.sLogger.v("limitbandwidth: turn HERE traffic listener on");
                this.trafficRerouteListener.start();
            }
            else {
                HereNavigationManager.sLogger.v("limitbandwidth: HERE traffic listener off, not enabled");
            }
            // monitorexit(trafficRerouteLock2)
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    HereNavigationManager.sLogger.v("limitbandwidth: turn on HERE traffic avoidance mode");
                    HereNavigationManager.this.hereNavController.setTrafficAvoidanceMode(NavigationManager.TrafficAvoidanceMode.MANUAL);
                }
            }, 3);
        }
    }
    
    public void setDebugManeuverDistance(final String debugManeuverDistance) {
        HereNavigationManager.sLogger.v("debugManeuverDistance:" + debugManeuverDistance);
        this.debugManeuverDistance = debugManeuverDistance;
    }
    
    public void setDebugManeuverIcon(final int debugManeuverIcon) {
        HereNavigationManager.sLogger.v("debugManeuverIcon:" + debugManeuverIcon);
        this.debugManeuverIcon = debugManeuverIcon;
    }
    
    public void setDebugManeuverInstruction(final String debugManeuverInstruction) {
        HereNavigationManager.sLogger.v("debugManeuverInstruction:" + debugManeuverInstruction);
        this.debugManeuverInstruction = debugManeuverInstruction;
    }
    
    public void setDebugManeuverState(final HereManeuverDisplayBuilder.ManeuverState debugManeuverState) {
        HereNavigationManager.sLogger.v("debugManeuverState:" + debugManeuverState);
        this.debugManeuverState = debugManeuverState;
    }
    
    public void setDebugNextManeuverIcon(final int debugNextManeuverIcon) {
        HereNavigationManager.sLogger.v("debugNextManeuverIcon:" + debugNextManeuverIcon);
        if (debugNextManeuverIcon == 0) {
            this.currentNavigationInfo.maneuverAfterCurrentIconid = -1;
        }
        this.debugNextManeuverIcon = debugNextManeuverIcon;
    }
    
    public void setIgnoreArrived(final boolean ignoreArrived) {
        if (this.isNavigationModeOn() && this.currentNavigationInfo.hasArrived) {
            HereNavigationManager.sLogger.v("ignore arrived:" + ignoreArrived);
            this.currentNavigationInfo.ignoreArrived = ignoreArrived;
        }
    }
    
    public void setLastManeuver() {
        this.currentNavigationInfo.lastManeuver = true;
    }
    
    public void setLastTrafficRerouteTime(final long lastTrafficRerouteTime) {
        this.currentNavigationInfo.setLastTrafficRerouteTime(lastTrafficRerouteTime);
    }
    
    public void setMapUpdateMode() {
        synchronized (this) {
            GenericUtil.checkNotOnMainThread();
            final NavigationManager.MapUpdateMode hereMapUpdateMode = getHereMapUpdateMode();
            HereNavigationManager.sLogger.v("setting here map update mode to[" + hereMapUpdateMode + "]");
            this.navigationManager.setMapUpdateMode(hereMapUpdateMode);
        }
    }
    
    public void setMapView(final MapView mapView, final NavigationView navigationView) {
        this.mapView = mapView;
        this.navigationView = navigationView;
    }
    
    boolean setNavigationMode(final NavigationMode navigationMode, final HereNavigationInfo hereNavigationInfo, final StringBuilder sb) {
        while (true) {
            while (true) {
                Label_0638: {
                    synchronized (this) {
                        GenericUtil.checkNotOnMainThread();
                        HereNavigationManager.sLogger.i("setNavigationMode:" + navigationMode);
                        boolean b;
                        if (this.currentNavigationMode == navigationMode) {
                            HereNavigationManager.sLogger.i("setNavigationMode: already in " + navigationMode);
                            b = true;
                        }
                        else if (hereNavigationInfo == null && navigationMode != NavigationMode.MAP) {
                            HereNavigationManager.sLogger.e("setNavigationMode: navigation info cannot be null");
                            if (sb != null) {
                                sb.append(HereNavigationManager.context.getString(R.string.invalid_param));
                            }
                            b = false;
                        }
                        else {
                            final NavigationSessionState currentSessionState = this.currentSessionState;
                            final boolean b2 = true;
                            final boolean b3 = false;
                            boolean b4 = b2;
                            NavigationSessionState currentSessionState2 = currentSessionState;
                            if (this.currentNavigationMode != null) {
                                if ((this.currentNavigationMode == NavigationMode.MAP_ON_ROUTE && navigationMode == NavigationMode.TBT_ON_ROUTE) || (this.currentNavigationMode == NavigationMode.TBT_ON_ROUTE && navigationMode == NavigationMode.MAP_ON_ROUTE)) {
                                    HereNavigationManager.sLogger.v("navigation switch not reqd from= " + this.currentNavigationMode + " to=" + navigationMode);
                                    this.currentNavigationMode = navigationMode;
                                    b4 = false;
                                    currentSessionState2 = currentSessionState;
                                }
                                else {
                                    final boolean navigationModeOn = this.isNavigationModeOn();
                                    HereNavigationManager.sLogger.v("remove route before calling nav:stop navon=" + navigationModeOn);
                                    if (navigationModeOn) {
                                        HereNavigationManager.sLogger.v("navigation stopped from:" + this.currentSessionState);
                                        this.updateMapRoute(null, null, null, null, false, false);
                                        HereMapUtil.removeRouteInfo(HereNavigationManager.sLogger, false);
                                    }
                                    this.hereNavController.stopNavigation(true);
                                    this.currentSessionState = NavigationSessionState.NAV_SESSION_STOPPED;
                                    if (currentSessionState != NavigationSessionState.NAV_SESSION_STOPPED) {
                                        this.postNavigationSessionStatusEvent(false);
                                    }
                                    currentSessionState2 = this.currentSessionState;
                                    b4 = b2;
                                }
                            }
                            this.handler.removeCallbacks(this.etaCalcRunnable);
                            this.currentNavigationInfo.clear();
                            this.debugManeuverState = null;
                            int n = b3 ? 1 : 0;
                            if (b4) {
                                switch (navigationMode) {
                                    default:
                                        n = (b3 ? 1 : 0);
                                        break;
                                    case MAP:
                                        this.hereNavController.stopNavigation(false);
                                        this.bus.post(new MapEvents.GPSSpeedEvent());
                                        if (!DeviceUtil.isUserBuild() && MapSettings.isLaneGuidanceEnabled()) {
                                            this.laneInfoListener.stop();
                                        }
                                        this.hereRealisticViewListener.stop();
                                        this.currentNavigationInfo.copy(hereNavigationInfo);
                                        this.currentNavigationMode = NavigationMode.MAP;
                                        this.currentSessionState = NavigationSessionState.NAV_SESSION_STOPPED;
                                        this.mapController.setMapScheme(HereNavigationManager.hereMapsManager.getTrackingMapScheme());
                                        n = (b3 ? 1 : 0);
                                        break;
                                    case MAP_ON_ROUTE:
                                    case TBT_ON_ROUTE:
                                        break Label_0638;
                                }
                            }
                            this.setTrafficOverlay();
                            this.bus.post(new MapEvents.NavigationModeChange(this.currentNavigationMode));
                            if (n != 0) {
                                HereNavigationManager.sLogger.v("send start maneuver");
                                this.sendStartManeuver(this.currentNavigationInfo.route);
                            }
                            if (currentSessionState2 != this.currentSessionState) {
                                this.postNavigationSessionStatusEvent(false);
                            }
                            b = true;
                        }
                        return b;
                    }
                }
                this.updateMapRoute(hereNavigationInfo.route, null, null, null, false, this.isTrafficConsidered(hereNavigationInfo.routeId));
                final NavigationManager.Error startNavigation = this.hereNavController.startNavigation(hereNavigationInfo.route, hereNavigationInfo.simulationSpeed);
                if (startNavigation != NavigationManager.Error.NONE) {
                    this.updateMapRoute(null, null, null, null, false, false);
                    this.currentSessionState = NavigationSessionState.NAV_SESSION_ERROR;
                    final Logger sLogger = HereNavigationManager.sLogger;
                    final StringBuilder append = new StringBuilder().append("cannot switch navigation mode from:").append(this.currentNavigationMode).append(" to=").append(navigationMode).append(" error=").append(startNavigation).append(" simulating=");
                    String s;
                    if (hereNavigationInfo.simulationSpeed > 0) {
                        s = "true";
                    }
                    else {
                        s = "false";
                    }
                    sLogger.e(append.append(s).toString());
                    this.postNavigationSessionStatusEvent(false);
                    if (sb != null) {
                        sb.append(startNavigation.name());
                    }
                    return false;
                }
                final Logger sLogger2 = HereNavigationManager.sLogger;
                final StringBuilder append2 = new StringBuilder().append("switched navigation mode from:").append(this.currentNavigationMode).append(" to=").append(navigationMode).append(" simulating=");
                String s2;
                if (hereNavigationInfo.simulationSpeed > 0) {
                    s2 = "true";
                }
                else {
                    s2 = "false";
                }
                sLogger2.v(append2.append(s2).toString());
                this.currentNavigationInfo.copy(hereNavigationInfo);
                this.currentNavigationMode = navigationMode;
                this.currentSessionState = NavigationSessionState.NAV_SESSION_STARTED;
                if (!DeviceUtil.isUserBuild() && MapSettings.isLaneGuidanceEnabled()) {
                    this.laneInfoListener.start();
                }
                this.hereRealisticViewListener.start();
                this.mapController.setMapScheme(HereNavigationManager.hereMapsManager.getEnrouteMapScheme());
                int n = 1;
                this.handler.postDelayed(this.etaCalcRunnable, 30000L);
                if (this.currentNavigationInfo.navigationRouteRequest.routeAttributes.contains(NavigationRouteRequest.RouteAttribute.ROUTE_ATTRIBUTE_GAS)) {
                    HereMapUtil.saveGasRouteInfo(hereNavigationInfo.navigationRouteRequest, hereNavigationInfo.deviceId, HereNavigationManager.sLogger);
                    continue;
                }
                HereMapUtil.saveRouteInfo(hereNavigationInfo.navigationRouteRequest, hereNavigationInfo.deviceId, HereNavigationManager.sLogger);
                continue;
            }
        }
    }
    
    public void setReroute(final Route route, final NavigationSessionRouteChange.RerouteReason rerouteReason, final String s, final String s2, final boolean b, final boolean b2) {
        while (true) {
            Object o = null;
            Label_0114: {
                synchronized (this) {
                    o = HereNavigationManager.sLogger;
                    ((Logger)o).v("setReroute:" + b);
                    if (!this.isNavigationModeOn()) {
                        HereNavigationManager.sLogger.v("setReroute: nav mode ended");
                    }
                    else {
                        this.clearNavManeuver();
                        o = this.hereNavController.setRoute(route);
                        if (o != NavigationManager.Error.NONE) {
                            break Label_0114;
                        }
                        this.updateMapRoute(route, rerouteReason, s, s2, b, b2);
                        HereNavigationManager.sLogger.i("setReroute: done");
                    }
                    return;
                }
            }
            HereNavigationManager.sLogger.e("setReroute: route could not be set:" + o);
        }
    }
    
    public void setShownFirstManeuver(final boolean firstManeuverShown) {
        this.currentNavigationInfo.firstManeuverShown = firstManeuverShown;
        this.currentNavigationInfo.firstManeuverShownTime = SystemClock.elapsedRealtime();
    }
    
    public void setTrafficToMode() {
        while (true) {
            GenericUtil.checkNotOnMainThread();
            Label_0138: {
                try {
                    final HereMapController.State state = this.mapController.getState();
                    switch (state) {
                        default:
                            HereNavigationManager.sLogger.v("setTrafficToMode: " + state + "::  no-op");
                            break;
                        case AR_MODE:
                            HereNavigationManager.sLogger.v("setTrafficToMode: " + state);
                            if (!DriverProfileHelper.getInstance().getDriverProfileManager().isTrafficEnabled()) {
                                HereNavigationManager.sLogger.v("setTrafficToMode: traffic is not enabled");
                                break;
                            }
                            break Label_0138;
                    }
                    return;
                }
                catch (Throwable t) {
                    HereNavigationManager.sLogger.e(t);
                    return;
                }
                return;
            }
            this.mapController.setTrafficInfoVisible(true);
            HereNavigationManager.sLogger.v("setTrafficToMode: map traffic enabled");
            final MapRoute currentMapRoute = this.getCurrentMapRoute();
            if (currentMapRoute != null) {
                currentMapRoute.setTrafficEnabled(true);
                HereNavigationManager.sLogger.v("setTrafficToMode: route traffic enabled");
            }
        }
    }
    
    public void startTrafficRerouteListener() {
        final Object trafficRerouteLock = HereNavigationManager.trafficRerouteLock;
        synchronized (trafficRerouteLock) {
            this.navdyTrafficRerouteManager.start();
            if (NetworkBandwidthController.getInstance().isLimitBandwidthModeOn()) {
                HereNavigationManager.sLogger.v("startTrafficRerouteListener: limit b/w not starting here traffic reroute");
            }
            else {
                this.trafficRerouteListener.start();
            }
        }
    }
    
    void startTrafficUpdater() {
        if (this.trafficUpdater != null && !this.trafficUpdaterStarted) {
            this.trafficUpdaterStarted = true;
            this.trafficUpdater.start();
        }
    }
    
    public void stopAndRemoveAllRoutes() {
        HereNavigationManager.sLogger.v("stopAndRemoveAllRoutes");
        GenericUtil.checkNotOnMainThread();
        HereMapUtil.removeRouteInfo(HereNavigationManager.sLogger, true);
        this.stopNavigation();
        AnalyticsSupport.recordNavigationCancelled();
    }
    
    public void stopNavigation() {
        if (this.isNavigationModeOn()) {
            HereNavigationManager.sLogger.v("stopNavigation: nav mode is on");
            String label = null;
            final NavigationRouteRequest navigationRouteRequest = this.currentNavigationInfo.navigationRouteRequest;
            if (navigationRouteRequest != null) {
                label = navigationRouteRequest.label;
            }
            this.handleNavigationSessionRequest(new NavigationSessionRequest(NavigationSessionState.NAV_SESSION_STOPPED, label, this.currentNavigationInfo.routeId, 0, true));
        }
        else {
            HereNavigationManager.sLogger.v("stopNavigation: nav mode not on");
        }
    }
    
    public void stopTrafficRerouteListener() {
        final Object trafficRerouteLock = HereNavigationManager.trafficRerouteLock;
        synchronized (trafficRerouteLock) {
            this.navdyTrafficRerouteManager.stop();
            this.trafficRerouteListener.stop();
        }
    }
    
    public void updateMapRoute(final Route route, final NavigationSessionRouteChange.RerouteReason currentRerouteReason, final NavigationRouteRequest navigationRouteRequest, String string, final String s, final boolean b, final boolean b2) {
        while (true) {
            // monitorenter(this)
            final boolean b3 = false;
            while (true) {
                try {
                    boolean b4 = false;
                    Label_0107: {
                        if (this.currentNavigationInfo.mapRoute == null) {
                            b4 = b3;
                            if (this.currentNavigationInfo.mapDestinationMarker == null) {
                                break Label_0107;
                            }
                        }
                        this.tearDownCurrentRoute();
                        this.currentNavigationInfo.destinationDirection = null;
                        this.currentNavigationInfo.destinationDirectionStr = null;
                        this.currentNavigationInfo.destinationIconId = -1;
                        if (route != null) {
                            b4 = b3;
                            if (currentRerouteReason != null) {
                                break Label_0107;
                            }
                        }
                        b4 = true;
                        this.currentNavigationInfo.hasArrived = false;
                        this.currentNavigationInfo.ignoreArrived = false;
                        this.currentNavigationInfo.lastManeuver = false;
                        this.currentNavigationInfo.arrivedManeuverDisplay = null;
                    }
                    if (route != null) {
                        final MapRoute mapRoute = new MapRoute(route);
                        mapRoute.setTrafficEnabled(true);
                        this.addCurrentRoute(mapRoute);
                        this.currentNavigationInfo.mapRoute.setTrafficEnabled(true);
                        this.bus.post(new MapEvents.NewRouteAdded(currentRerouteReason));
                        this.newManeuverEventListener.setNewRoute();
                        if (b) {
                            this.sendStartManeuver(route);
                            HereNavigationManager.sLogger.v("updateMapRoute:sending start maneuver");
                        }
                        HereNavigationManager.sLogger.v("map route added:" + this.currentNavigationInfo.mapRoute);
                        this.removeDestinationMarker();
                        this.currentNavigationInfo.mapDestinationMarker = null;
                        this.currentNavigationInfo.currentRerouteReason = currentRerouteReason;
                        this.addDestinationMarker(route.getDestination(), R.drawable.icon_pin_dot_destination);
                        this.trafficUpdater.setRoute(this.currentNavigationInfo.mapRoute.getRoute());
                        if (currentRerouteReason != null) {
                            if (string == null) {
                                string = UUID.randomUUID().toString();
                            }
                            final String routeId = this.currentNavigationInfo.routeId;
                            final NavigationRouteRequest navigationRouteRequest2 = this.currentNavigationInfo.navigationRouteRequest;
                            if (currentRerouteReason == NavigationSessionRouteChange.RerouteReason.NAV_SESSION_FUEL_REROUTE) {
                                this.currentNavigationInfo.navigationRouteRequest = navigationRouteRequest;
                                this.currentNavigationInfo.routeOptions = HereNavigationManager.hereMapsManager.getRouteOptions();
                                this.currentNavigationInfo.streetAddress = HereMapUtil.parseStreetAddress(navigationRouteRequest.streetAddress);
                                this.currentNavigationInfo.destinationLabel = null;
                            }
                            else if (currentRerouteReason == NavigationSessionRouteChange.RerouteReason.NAV_SESSION_TRAFFIC_REROUTE) {
                                this.currentNavigationInfo.trafficRerouteOnce = true;
                            }
                            TaskManager.getInstance().execute(new Runnable() {
                                @Override
                                public void run() {
                                    final HereRouteCalculator hereRouteCalculator = new HereRouteCalculator(HereNavigationManager.sLogger, false);
                                    String s;
                                    if (TextUtils.isEmpty((CharSequence)(s = s)) && TextUtils.isEmpty((CharSequence)(s = HereRouteViaGenerator.getViaString(route)))) {
                                        s = "";
                                    }
                                    String label = null;
                                    String streetAddress = null;
                                    List<NavigationRouteRequest.RouteAttribute> routeAttributes = null;
                                    final NavigationRouteRequest navigationRouteRequest = HereNavigationManager.this.currentNavigationInfo.navigationRouteRequest;
                                    if (navigationRouteRequest != null) {
                                        label = navigationRouteRequest.label;
                                        streetAddress = navigationRouteRequest.streetAddress;
                                        routeAttributes = navigationRouteRequest.routeAttributes;
                                    }
                                    final NavigationRouteResult routeResultFromRoute = hereRouteCalculator.getRouteResultFromRoute(string, route, s, label, streetAddress, true);
                                    HereNavigationManager.this.currentNavigationInfo.routeId = string;
                                    HereNavigationManager.this.currentNavigationInfo.route = route;
                                    HereRouteCache.getInstance().addRoute(string, new HereRouteCache.RouteInfo(route, navigationRouteRequest, routeResultFromRoute, b2, HereNavigationManager.hereMapsManager.getLocationFixManager().getLastGeoCoordinate()));
                                    HereNavigationManager.sLogger.v("new route added to cache");
                                    HereNavigationManager.this.postNavigationSessionStatusEvent(NavigationSessionState.NAV_SESSION_REROUTED, false);
                                    final NavigationSessionRouteChange navigationSessionRouteChange = new NavigationSessionRouteChange(routeId, routeResultFromRoute, currentRerouteReason);
                                    if (routeAttributes != null && routeAttributes.contains(NavigationRouteRequest.RouteAttribute.ROUTE_ATTRIBUTE_GAS)) {
                                        HereMapUtil.saveGasRouteInfo(navigationRouteRequest, HereNavigationManager.this.currentNavigationInfo.deviceId, HereNavigationManager.sLogger);
                                    }
                                    HereNavigationManager.this.bus.post(navigationSessionRouteChange);
                                    HereNavigationManager.this.bus.post(new RemoteEvent(navigationSessionRouteChange));
                                    HereNavigationManager.sLogger.v("NavigationSessionRouteChange sent to client oldRoute[" + routeId + "] newRoute[" + string + "] reason[" + currentRerouteReason + "] via[" + s + "]");
                                    if (HereNavigationManager.sLogger.isLoggable(2)) {
                                        HereMapUtil.printRouteDetails(route, navigationRouteRequest2.streetAddress, navigationRouteRequest2, null);
                                    }
                                }
                            }, 2);
                        }
                        if (b4) {
                            this.refreshNavigationInfo();
                        }
                        return;
                    }
                }
                finally {
                }
                // monitorexit(this)
                HereNavigationManager.hereMapsManager.getLocationFixManager().addMarkers(this.mapController);
                continue;
            }
        }
    }
    
    public void updateMapRoute(final Route route, final NavigationSessionRouteChange.RerouteReason rerouteReason, final String s, final String s2, final boolean b, final boolean b2) {
        this.updateMapRoute(route, rerouteReason, this.currentNavigationInfo.navigationRouteRequest, s, s2, b, b2);
    }
    
    void updateNavigationInfo(final Maneuver maneuver, final Maneuver maneuver2, final Maneuver maneuver3, final boolean b) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    HereNavigationManager.this.updateNavigationInfoInternal(maneuver, maneuver2, maneuver3, b);
                }
                catch (Throwable t) {
                    HereNavigationManager.sLogger.e("updateNavigationInfoInternal", t);
                }
            }
        }, 4);
    }
}
