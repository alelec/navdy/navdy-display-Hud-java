package com.navdy.hud.app.maps;

import com.navdy.service.library.events.preferences.NavigationPreferences;

public class NavSessionPreferences
{
    public boolean spokenTurnByTurn;
    
    public NavSessionPreferences(final NavigationPreferences default1) {
        this.setDefault(default1);
    }
    
    public void setDefault(final NavigationPreferences navigationPreferences) {
        this.spokenTurnByTurn = Boolean.TRUE.equals(navigationPreferences.spokenTurnByTurn);
    }
}
