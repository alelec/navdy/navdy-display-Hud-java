package com.navdy.hud.app.maps.here;

import com.squareup.otto.Subscribe;
import com.here.android.mpa.common.GeoPosition;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.service.library.events.audio.SpeechRequest;
import com.navdy.hud.app.framework.phonecall.CallUtils;
import com.here.android.mpa.common.RoadElement;
import android.os.Looper;
import android.os.SystemClock;
import com.navdy.service.library.task.TaskManager;
import com.navdy.hud.app.HudApplication;
import java.util.concurrent.TimeUnit;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.maps.MapsEventHandler;
import android.os.Handler;
import android.content.Context;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;

public class HereSpeedWarningManager
{
    private static final int BUFFER_SPEED = 7;
    private static final long DISCARD_INTERVAL;
    private static final int PERIODIC_CHECK_INTERVAL = 5000;
    private static final Logger sLogger;
    private Bus bus;
    private Runnable checkSpeedRunnable;
    private Context context;
    private Handler handler;
    private HereNavigationManager hereNavigationManager;
    private long lastSpeedWarningTime;
    private long lastUpdate;
    private MapsEventHandler mapsEventHandler;
    private Runnable periodicCheckRunnable;
    private Runnable periodicRunnable;
    private boolean registered;
    private SpeedManager speedManager;
    private volatile boolean speedWarningOn;
    private String tag;
    private int warningSpeed;
    
    static {
        sLogger = new Logger(HereSpeedWarningManager.class);
        DISCARD_INTERVAL = TimeUnit.SECONDS.toMillis(10L);
    }
    
    HereSpeedWarningManager(final String tag, final Bus bus, final MapsEventHandler mapsEventHandler, final HereNavigationManager hereNavigationManager) {
        this.context = HudApplication.getAppContext();
        this.speedManager = SpeedManager.getInstance();
        this.periodicRunnable = new Runnable() {
            @Override
            public void run() {
                TaskManager.getInstance().execute(HereSpeedWarningManager.this.periodicCheckRunnable, 12);
            }
        };
        this.periodicCheckRunnable = new Runnable() {
            @Override
            public void run() {
                try {
                    if (HereSpeedWarningManager.this.registered) {
                        if (SystemClock.elapsedRealtime() - HereSpeedWarningManager.this.lastUpdate < 5000L) {
                            if (HereSpeedWarningManager.this.registered) {
                                HereSpeedWarningManager.this.handler.postDelayed(HereSpeedWarningManager.this.periodicRunnable, 5000L);
                            }
                        }
                        else {
                            HereSpeedWarningManager.this.checkSpeed();
                            if (HereSpeedWarningManager.this.registered) {
                                HereSpeedWarningManager.this.handler.postDelayed(HereSpeedWarningManager.this.periodicRunnable, 5000L);
                            }
                        }
                    }
                }
                catch (Throwable t) {
                    HereSpeedWarningManager.sLogger.e(t);
                    if (HereSpeedWarningManager.this.registered) {
                        HereSpeedWarningManager.this.handler.postDelayed(HereSpeedWarningManager.this.periodicRunnable, 5000L);
                    }
                }
                finally {
                    if (HereSpeedWarningManager.this.registered) {
                        HereSpeedWarningManager.this.handler.postDelayed(HereSpeedWarningManager.this.periodicRunnable, 5000L);
                    }
                }
            }
        };
        this.checkSpeedRunnable = new Runnable() {
            @Override
            public void run() {
                try {
                    if (SystemClock.elapsedRealtime() - HereSpeedWarningManager.this.lastUpdate >= 1000L) {
                        HereSpeedWarningManager.this.checkSpeed();
                    }
                }
                catch (Throwable t) {
                    HereSpeedWarningManager.sLogger.e(t);
                }
            }
        };
        this.tag = tag;
        this.bus = bus;
        this.mapsEventHandler = mapsEventHandler;
        this.hereNavigationManager = hereNavigationManager;
        this.handler = new Handler(Looper.getMainLooper());
    }
    
    private void checkSpeed() {
        final float n = this.speedManager.getCurrentSpeed();
        if (n == -1.0f) {
            if (this.speedWarningOn) {
                HereSpeedWarningManager.sLogger.i("lost speed info while warning on");
                this.onSpeedExceededEnd(null, 0, 0);
            }
        }
        else {
            final RoadElement currentRoadElement = HereMapUtil.getCurrentRoadElement();
            if (currentRoadElement != null) {
                this.lastUpdate = SystemClock.elapsedRealtime();
                final int convert = SpeedManager.convert(currentRoadElement.getSpeedLimit(), SpeedManager.SpeedUnit.METERS_PER_SECOND, this.speedManager.getSpeedUnit());
                if (convert <= 0) {
                    if (this.speedWarningOn) {
                        HereSpeedWarningManager.sLogger.v("clear speed warning, no speedlimit info avail");
                        this.onSpeedExceededEnd(currentRoadElement.getRoadName(), convert, (int)n);
                    }
                }
                else if (n > convert) {
                    if (!this.speedWarningOn || this.warningSpeed == -1 || (this.warningSpeed != -1 && n > this.warningSpeed)) {
                        this.onSpeedExceeded(currentRoadElement.getRoadName(), convert, (int)n);
                    }
                }
                else if (this.speedWarningOn) {
                    this.onSpeedExceededEnd(currentRoadElement.getRoadName(), convert, (int)n);
                }
            }
        }
    }
    
    private void onSpeedExceeded(String s, final int n, final int warningSpeed) {
        this.bus.post(HereNavigationManager.SPEED_EXCEEDED);
        if (!this.speedWarningOn) {
            this.warningSpeed = -1;
        }
        final boolean speedWarningOn = this.speedWarningOn;
        this.speedWarningOn = true;
        if (!Boolean.TRUE.equals(this.mapsEventHandler.getNavigationPreferences().spokenSpeedLimitWarnings) || CallUtils.isPhoneCallInProgress()) {
            if (!speedWarningOn) {
                HereSpeedWarningManager.sLogger.w(this.tag + " speed exceeded:" + s + " limit=" + n + " current=" + warningSpeed);
            }
        }
        else {
            final SpeedManager.SpeedUnit speedUnit = this.speedManager.getSpeedUnit();
            int n2;
            if (this.warningSpeed == -1) {
                n2 = n + 7;
            }
            else {
                n2 = this.warningSpeed + this.warningSpeed / 10;
            }
            if (warningSpeed >= n2) {
                HereSpeedWarningManager.sLogger.w(this.tag + " speed exceeded-notify current[" + warningSpeed + "] threshold[" + n2 + "] allowed[" + n + "] " + speedUnit.name());
                this.warningSpeed = warningSpeed;
                switch (speedUnit) {
                    case MILES_PER_HOUR:
                        s = this.hereNavigationManager.TTS_MILES;
                        break;
                    case KILOMETERS_PER_HOUR:
                        s = this.hereNavigationManager.TTS_KMS;
                        break;
                    case METERS_PER_SECOND:
                        s = this.hereNavigationManager.TTS_METERS;
                        break;
                }
                final long elapsedRealtime = SystemClock.elapsedRealtime();
                final long n3 = elapsedRealtime - this.lastSpeedWarningTime;
                if (n3 <= HereSpeedWarningManager.DISCARD_INTERVAL) {
                    HereSpeedWarningManager.sLogger.v("don't post speed tts:" + n3);
                }
                else {
                    s = this.context.getString(R.string.tts_speed_warning, new Object[] { n });
                    this.lastSpeedWarningTime = elapsedRealtime;
                    TTSUtils.sendSpeechRequest(s, SpeechRequest.Category.SPEECH_SPEED_WARNING, null);
                }
            }
            else if (!speedWarningOn) {
                HereSpeedWarningManager.sLogger.w(this.tag + " speed exceeded current[" + warningSpeed + "] threshold[" + n2 + "] allowed[" + n + "] " + speedUnit.name());
            }
        }
    }
    
    private void onSpeedExceededEnd(final String s, final int n, final int n2) {
        this.speedWarningOn = false;
        this.warningSpeed = -1;
        this.bus.post(HereNavigationManager.SPEED_NORMAL);
        HereSpeedWarningManager.sLogger.i(this.tag + " speed normal:" + s + " limit=" + n + " current=" + n2);
    }
    
    @Subscribe
    public void onPositionUpdated(final GeoPosition geoPosition) {
        TaskManager.getInstance().execute(this.checkSpeedRunnable, 12);
    }
    
    public void start() {
        if (!this.registered) {
            this.registered = true;
            this.bus.register(this);
            this.handler.removeCallbacks(this.periodicRunnable);
            this.handler.postDelayed(this.periodicRunnable, 5000L);
            HereSpeedWarningManager.sLogger.v("started");
        }
    }
    
    public void stop() {
        if (this.registered) {
            this.registered = false;
            this.bus.unregister(this);
            this.handler.removeCallbacks(this.periodicRunnable);
            HereSpeedWarningManager.sLogger.v("stopped");
        }
    }
}
