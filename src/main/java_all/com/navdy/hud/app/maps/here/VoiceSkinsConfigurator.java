package com.navdy.hud.app.maps.here;

import android.content.Context;
import android.os.SystemClock;
import com.navdy.service.library.util.IOUtils;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.storage.PathManager;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.util.PackagedResource;

class VoiceSkinsConfigurator
{
    private static final PackagedResource resource;
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(VoiceSkinsConfigurator.class);
        resource = new PackagedResource("voiceskins", PathManager.getInstance().getHereVoiceSkinsPath());
    }
    
    static void updateVoiceSkins() {
        try {
            final Context appContext = HudApplication.getAppContext();
            IOUtils.checkIntegrity(appContext, PathManager.getInstance().getHereVoiceSkinsPath(), R.raw.here_voices_integrity);
            VoiceSkinsConfigurator.sLogger.i("Voice skins update: starting");
            final long elapsedRealtime = SystemClock.elapsedRealtime();
            VoiceSkinsConfigurator.resource.updateFromResources(appContext, R.raw.here_voices, R.raw.here_voices_md5, true);
            VoiceSkinsConfigurator.sLogger.i("Voice skins update: time took " + (SystemClock.elapsedRealtime() - elapsedRealtime) + " ms");
        }
        catch (Throwable t) {
            VoiceSkinsConfigurator.sLogger.e("Voice skins error", t);
        }
    }
}
