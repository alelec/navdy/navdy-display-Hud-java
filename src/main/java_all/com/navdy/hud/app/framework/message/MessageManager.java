package com.navdy.hud.app.framework.message;

import com.squareup.otto.Subscribe;
import java.util.List;
import com.navdy.service.library.events.glances.GlanceEvent;
import com.navdy.service.library.events.glances.MessageConstants;
import com.navdy.service.library.events.glances.KeyValue;
import java.util.ArrayList;
import com.navdy.hud.app.framework.glance.GlanceHelper;
import com.navdy.hud.app.framework.contacts.ContactUtil;
import android.text.TextUtils;
import com.navdy.hud.app.framework.recentcall.RecentCallManager;
import java.util.Date;
import com.navdy.hud.app.framework.contacts.NumberType;
import com.navdy.hud.app.framework.recentcall.RecentCall;
import com.navdy.service.library.events.notification.NotificationCategory;
import com.navdy.service.library.events.notification.NotificationEvent;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;

public class MessageManager
{
    private static final String EMPTY = "";
    private static final MessageManager sInstance;
    public static final Logger sLogger;
    private Bus bus;
    
    static {
        sLogger = new Logger(MessageManager.class);
        sInstance = new MessageManager();
    }
    
    private MessageManager() {
        (this.bus = RemoteDeviceManager.getInstance().getBus()).register(this);
    }
    
    public static MessageManager getInstance() {
        return MessageManager.sInstance;
    }
    
    @Subscribe
    public void onMessage(final NotificationEvent notificationEvent) {
        MessageManager.sLogger.v("old notification --> convert to glance");
        if (notificationEvent.sourceIdentifier == null) {
            MessageManager.sLogger.w("Ignoring notification with null sourceIdentifier" + notificationEvent);
        }
        else {
            RecentCall recentCall = null;
            if (notificationEvent.category == NotificationCategory.CATEGORY_SOCIAL) {
                recentCall = new RecentCall(null, RecentCall.Category.MESSAGE, notificationEvent.sourceIdentifier, NumberType.OTHER, new Date(), RecentCall.CallType.INCOMING, -1, 0L);
                RecentCallManager.getInstance().handleNewCall(recentCall);
            }
            String s;
            if (TextUtils.isEmpty((CharSequence)notificationEvent.title)) {
                s = notificationEvent.sourceIdentifier;
            }
            else {
                s = notificationEvent.title;
            }
            final String message = notificationEvent.message;
            final String s2 = null;
            boolean equals = Boolean.TRUE.equals(notificationEvent.cannotReplyBack);
            final boolean b = false;
            String s3;
            int n;
            if (ContactUtil.isValidNumber(notificationEvent.sourceIdentifier)) {
                s3 = notificationEvent.sourceIdentifier;
                n = 1;
            }
            else {
                s3 = s2;
                n = (b ? 1 : 0);
                if (recentCall != null) {
                    s3 = s2;
                    n = (b ? 1 : 0);
                    if (ContactUtil.isValidNumber(recentCall.number)) {
                        s3 = recentCall.number;
                        n = 1;
                    }
                }
            }
            if (n == 0) {
                equals = true;
            }
            final String id = GlanceHelper.getId();
            final ArrayList<KeyValue> list = new ArrayList<KeyValue>();
            final List<GlanceEvent.GlanceActions> list2 = null;
            list.add(new KeyValue(MessageConstants.MESSAGE_FROM.name(), s));
            List<GlanceEvent.GlanceActions> list3 = list2;
            if (s3 != null) {
                list.add(new KeyValue(MessageConstants.MESSAGE_FROM_NUMBER.name(), s3));
                list3 = list2;
                if (!equals) {
                    list3 = new ArrayList<GlanceEvent.GlanceActions>(1);
                    list3.add(GlanceEvent.GlanceActions.REPLY);
                    list.add(new KeyValue(MessageConstants.MESSAGE_IS_SMS.name(), s));
                }
            }
            list.add(new KeyValue(MessageConstants.MESSAGE_BODY.name(), message));
            final GlanceEvent.Builder glanceData = new GlanceEvent.Builder().glanceType(GlanceEvent.GlanceType.GLANCE_TYPE_MESSAGE).provider("com.navdy.sms").id(id).postTime(System.currentTimeMillis()).glanceData(list);
            if (list3 != null) {
                glanceData.actions(list3);
            }
            this.bus.post(glanceData.build());
        }
    }
}
