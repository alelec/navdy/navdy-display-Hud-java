package com.navdy.hud.app.framework.music;

import com.navdy.service.library.util.MusicDataUtils;
import java.util.Set;
import android.support.annotation.NonNull;
import com.squareup.otto.Subscribe;
import android.view.KeyEvent;
import com.navdy.hud.app.ui.component.ChoiceLayout;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.ui.component.mainmenu.IMenu;
import android.os.Bundle;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.HudApplication;
import com.navdy.service.library.task.TaskManager;
import android.support.annotation.Nullable;
import okio.ByteString;
import com.navdy.hud.app.manager.InputManager;
import android.animation.AnimatorSet;
import android.view.LayoutInflater;
import com.navdy.hud.app.framework.notifications.NotificationType;
import android.view.View;
import android.content.Context;
import android.graphics.Bitmap;
import com.navdy.service.library.events.audio.MusicTrackInfo;
import android.widget.TextView;
import android.widget.ProgressBar;
import java.util.concurrent.atomic.AtomicBoolean;
import android.os.Handler;
import com.navdy.hud.app.framework.notifications.INotificationController;
import android.view.ViewGroup;
import com.squareup.otto.Bus;
import com.navdy.service.library.util.ScalingUtilities;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.manager.MusicManager;
import com.navdy.hud.app.framework.notifications.INotification;

public class MusicNotification implements INotification, MusicUpdateListener
{
    private static final float ARTWORK_ALPHA_NOT_PLAYING = 0.5f;
    private static final float ARTWORK_ALPHA_PLAYING = 1.0f;
    private static final String EMPTY = "";
    private static final int MUSIC_TIMEOUT = 5000;
    private static final Logger sLogger;
    private final ScalingUtilities.ScalingLogic FIT;
    private Bus bus;
    private MediaControllerLayout choiceLayout;
    private ViewGroup container;
    private INotificationController controller;
    private Handler handler;
    private AlbumArtImageView image;
    private AtomicBoolean isKeyPressedDown;
    private String lastEventIdentifier;
    private MusicManager musicManager;
    private ProgressBar progressBar;
    private TextView subTitle;
    private TextView title;
    private MusicTrackInfo trackInfo;
    
    static {
        sLogger = new Logger(MusicNotification.class);
    }
    
    public MusicNotification(final MusicManager musicManager, final Bus bus) {
        this.FIT = ScalingUtilities.ScalingLogic.FIT;
        this.isKeyPressedDown = new AtomicBoolean(false);
        this.musicManager = musicManager;
        this.bus = bus;
        this.handler = new Handler();
    }
    
    private void setDefaultImage(final boolean b) {
        this.handler.post((Runnable)new Runnable() {
            @Override
            public void run() {
                MusicNotification.this.image.setArtworkBitmap(null, b);
            }
        });
    }
    
    private void setImage(final Bitmap bitmap, final boolean b) {
        this.handler.post((Runnable)new Runnable() {
            @Override
            public void run() {
                MusicNotification.this.image.setArtworkBitmap(bitmap, b);
            }
        });
    }
    
    @Override
    public boolean canAddToStackIfCurrentExists() {
        return false;
    }
    
    @Override
    public boolean expandNotification() {
        return false;
    }
    
    @Override
    public int getColor() {
        return 0;
    }
    
    @Override
    public View getExpandedView(final Context context, final Object o) {
        return null;
    }
    
    @Override
    public int getExpandedViewIndicatorColor() {
        return 0;
    }
    
    @Override
    public String getId() {
        return "navdy#music#notif";
    }
    
    @Override
    public int getTimeout() {
        return 5000;
    }
    
    @Override
    public NotificationType getType() {
        return NotificationType.MUSIC;
    }
    
    @Override
    public View getView(final Context context) {
        if (this.container == null) {
            this.container = (ViewGroup)LayoutInflater.from(context).inflate(R.layout.notification_music, (ViewGroup)null);
            this.title = (TextView)this.container.findViewById(R.id.title);
            this.subTitle = (TextView)this.container.findViewById(R.id.subTitle);
            this.image = (AlbumArtImageView)this.container.findViewById(R.id.image);
            this.progressBar = (ProgressBar)this.container.findViewById(R.id.music_progress);
            this.choiceLayout = (MediaControllerLayout)this.container.findViewById(R.id.choiceLayout);
        }
        return (View)this.container;
    }
    
    @Override
    public AnimatorSet getViewSwitchAnimation(final boolean b) {
        return null;
    }
    
    @Override
    public boolean isAlive() {
        return false;
    }
    
    @Override
    public boolean isPurgeable() {
        return false;
    }
    
    @Override
    public IInputHandler nextHandler() {
        return null;
    }
    
    @Override
    public void onAlbumArtUpdate(@Nullable final ByteString byteString, final boolean defaultImage) {
        if (byteString == null) {
            MusicNotification.sLogger.v("ByteString image to set in notification is null");
            this.setDefaultImage(defaultImage);
        }
        else {
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    MusicNotification.sLogger.d("PhotoUpdate runnable " + byteString);
                    final byte[] byteArray = byteString.toByteArray();
                    if (byteArray == null || byteArray.length <= 0) {
                        MusicNotification.sLogger.i("Received photo has null or empty byte array");
                        MusicNotification.this.setDefaultImage(true);
                    }
                    else {
                        final int dimensionPixelSize = HudApplication.getAppContext().getResources().getDimensionPixelSize(R.dimen.music_notification_image_size);
                        Bitmap bitmap = null;
                        Bitmap decodeByteArray = null;
                        final Bitmap bitmap2 = null;
                        Bitmap bitmap4;
                        final Bitmap bitmap3 = bitmap4 = null;
                        Bitmap bitmap5 = bitmap2;
                        try {
                            final Bitmap bitmap6 = decodeByteArray = ScalingUtilities.decodeByteArray(byteArray, dimensionPixelSize, dimensionPixelSize, MusicNotification.this.FIT);
                            bitmap4 = bitmap3;
                            bitmap = bitmap6;
                            bitmap5 = bitmap2;
                            final Bitmap scaledBitmap = ScalingUtilities.createScaledBitmap(bitmap6, dimensionPixelSize, dimensionPixelSize, MusicNotification.this.FIT);
                            decodeByteArray = bitmap6;
                            bitmap4 = scaledBitmap;
                            bitmap = bitmap6;
                            bitmap5 = scaledBitmap;
                            MusicNotification.this.setImage(scaledBitmap, defaultImage);
                        }
                        catch (Exception ex) {
                            bitmap = decodeByteArray;
                            bitmap5 = bitmap4;
                            MusicNotification.sLogger.e("Error updating the artwork received ", ex);
                            bitmap = decodeByteArray;
                            bitmap5 = bitmap4;
                            MusicNotification.this.setDefaultImage(defaultImage);
                        }
                        finally {
                            if (bitmap != null && bitmap != bitmap5) {
                                bitmap.recycle();
                            }
                        }
                    }
                }
            }, 1);
        }
    }
    
    @Override
    public void onClick() {
    }
    
    @Override
    public void onExpandedNotificationEvent(final UIStateManager.Mode mode) {
    }
    
    @Override
    public void onExpandedNotificationSwitched() {
    }
    
    @Override
    public boolean onGesture(final GestureEvent gestureEvent) {
        return false;
    }
    
    @Override
    public boolean onKey(final CustomKeyEvent customKeyEvent) {
        boolean b = true;
        if (this.controller == null) {
            b = false;
        }
        else {
            if (this.choiceLayout != null) {
                switch (customKeyEvent) {
                    case LEFT:
                        this.controller.resetTimeout();
                        this.choiceLayout.moveSelectionLeft();
                        return b;
                    case RIGHT:
                        this.controller.resetTimeout();
                        this.choiceLayout.moveSelectionRight();
                        return b;
                    case SELECT: {
                        this.controller.resetTimeout();
                        final ChoiceLayout.Choice selectedItem = this.choiceLayout.getSelectedItem();
                        if (selectedItem == null) {
                            MusicNotification.sLogger.w("Choice layout selected item is null, returning");
                            b = false;
                            return b;
                        }
                        final MusicManager.MediaControl mediaControl = MusicManager.CONTROLS[selectedItem.id];
                        switch (mediaControl) {
                            default:
                                b = false;
                                return b;
                            case PLAY:
                            case PAUSE:
                                if (this.trackInfo != null) {
                                    this.musicManager.executeMediaControl(mediaControl, this.isKeyPressedDown.get());
                                }
                                this.choiceLayout.executeSelectedItem(true);
                                return b;
                            case MUSIC_MENU: {
                                this.choiceLayout.executeSelectedItem(true);
                                final Bundle bundle = new Bundle();
                                String s;
                                if ((s = this.musicManager.getMusicMenuPath()) == null) {
                                    s = "/" + IMenu.Menu.MUSIC.name();
                                }
                                bundle.putString("MENU_PATH", s);
                                NotificationManager.getInstance().removeNotification(this.getId(), true, Screen.SCREEN_MAIN_MENU, bundle, null);
                                return b;
                            }
                        }
                        break;
                    }
                    case LONG_PRESS:
                        this.controller.resetTimeout();
                        return b;
                }
            }
            b = false;
        }
        return b;
    }
    
    @Subscribe
    public void onKeyEvent(final KeyEvent keyEvent) {
        if (this.controller != null && this.choiceLayout != null && InputManager.isCenterKey(keyEvent.getKeyCode())) {
            final ChoiceLayout.Choice selectedItem = this.choiceLayout.getSelectedItem();
            if (selectedItem == null) {
                MusicNotification.sLogger.w("Choice layout selected item is null, returning");
            }
            else {
                final MusicManager.MediaControl mediaControl = MusicManager.CONTROLS[selectedItem.id];
                if (mediaControl == MediaControl.NEXT || mediaControl == MediaControl.PREVIOUS) {
                    this.controller.resetTimeout();
                    if (this.trackInfo != null) {
                        this.musicManager.handleKeyEvent(keyEvent, mediaControl, this.isKeyPressedDown.get());
                    }
                    if (keyEvent.getAction() == 0 && this.isKeyPressedDown.compareAndSet(false, true)) {
                        this.choiceLayout.keyDownSelectedItem();
                    }
                    else if (keyEvent.getAction() == 1 && this.isKeyPressedDown.compareAndSet(true, false)) {
                        this.choiceLayout.keyUpSelectedItem();
                    }
                }
            }
        }
    }
    
    @Override
    public void onNotificationEvent(final UIStateManager.Mode mode) {
    }
    
    @Override
    public void onStart(final INotificationController controller) {
        this.controller = controller;
        this.bus.register(this);
        this.musicManager.addMusicUpdateListener((MusicManager.MusicUpdateListener)this);
        this.updateProgressBar(this.musicManager.getCurrentPosition());
    }
    
    @Override
    public void onStop() {
        this.musicManager.removeMusicUpdateListener((MusicManager.MusicUpdateListener)this);
        this.bus.unregister(this);
        this.controller = null;
    }
    
    @Override
    public void onTrackHand(final float n) {
    }
    
    @Override
    public void onTrackUpdated(@NonNull final MusicTrackInfo trackInfo, final Set<MediaControl> set, final boolean b) {
        this.trackInfo = trackInfo;
        if (this.controller != null) {
            final String songIdentifierFromTrackInfo = MusicDataUtils.songIdentifierFromTrackInfo(trackInfo);
            if (this.lastEventIdentifier != null && !this.lastEventIdentifier.equals(songIdentifierFromTrackInfo)) {
                this.controller.resetTimeout();
            }
            this.lastEventIdentifier = songIdentifierFromTrackInfo;
            if (trackInfo.name != null) {
                this.subTitle.setText((CharSequence)trackInfo.name);
            }
            else {
                this.subTitle.setText((CharSequence)"");
            }
            if (trackInfo.author != null) {
                this.title.setText((CharSequence)trackInfo.author);
            }
            else {
                this.title.setText((CharSequence)"");
            }
            if (trackInfo.duration != null && trackInfo.currentPosition != null) {
                this.progressBar.setMax((int)trackInfo.duration);
                this.progressBar.setSecondaryProgress((int)trackInfo.duration);
            }
            if (MusicManager.tryingToPlay(trackInfo.playbackState)) {
                this.image.setAlpha(1.0f);
            }
            else {
                this.image.setAlpha(0.5f);
            }
            this.choiceLayout.updateControls(set);
        }
    }
    
    @Override
    public void onUpdate() {
        this.onTrackUpdated(this.musicManager.getCurrentTrack(), this.musicManager.getCurrentControls(), false);
        this.updateProgressBar(this.musicManager.getCurrentPosition());
    }
    
    @Override
    public boolean supportScroll() {
        return false;
    }
    
    public void updateProgressBar(final int progress) {
        if (this.controller != null) {
            this.progressBar.setProgress(progress);
        }
    }
}
