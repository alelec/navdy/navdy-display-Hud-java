package com.navdy.hud.app.framework.glance;

import android.os.SystemClock;
import java.util.ArrayList;
import com.navdy.service.library.events.glances.GlanceEvent;
import java.util.Iterator;
import android.text.TextUtils;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.List;
import java.util.HashMap;
import com.navdy.service.library.log.Logger;

class GlanceTracker
{
    private static final long NOTIFICATION_SEEN_THRESHOLD;
    private static final Logger sLogger;
    private HashMap<GlanceApp, List<GlanceInfo>> notificationSavedState;
    private HashMap<GlanceApp, List<GlanceInfo>> notificationSeen;
    
    static {
        sLogger = new Logger(GlanceTracker.class);
        NOTIFICATION_SEEN_THRESHOLD = TimeUnit.MINUTES.toMillis(60L);
    }
    
    GlanceTracker() {
        this.notificationSeen = new HashMap<GlanceApp, List<GlanceInfo>>();
    }
    
    private boolean isNotificationSame(final GlanceInfo glanceInfo, final GlanceApp glanceApp, final Map<String, String> map) {
        boolean b;
        if (map.size() != glanceInfo.data.size()) {
            b = false;
        }
        else {
            for (final Map.Entry<String, String> entry : glanceInfo.data.entrySet()) {
                final String s = map.get(entry.getKey());
                if (s == null) {
                    b = false;
                    return b;
                }
                if (!TextUtils.equals((CharSequence)s, (CharSequence)entry.getValue())) {
                    b = false;
                    return b;
                }
            }
            b = true;
        }
        return b;
    }
    
    void clearState() {
        synchronized (this) {
            GlanceTracker.sLogger.v("clearState");
            this.notificationSavedState = null;
        }
    }
    
    long isNotificationSeen(final GlanceEvent glanceEvent, final GlanceApp glanceApp, Map<String, String> sb) {
        while (true) {
            long time = 0L;
            Object o = null;
            Label_0177: {
                synchronized (this) {
                    switch (glanceApp) {
                        case GOOGLE_CALENDAR:
                        case APPLE_CALENDAR:
                        case GENERIC_CALENDAR:
                        case GOOGLE_MAIL:
                        case GOOGLE_INBOX:
                        case APPLE_MAIL:
                        case GENERIC_MAIL:
                            o = this.notificationSeen.get(glanceApp);
                            if (o == null) {
                                final ArrayList<GlanceInfo> list = new ArrayList<GlanceInfo>();
                                o = new GlanceInfo(SystemClock.elapsedRealtime(), glanceEvent, (Map<String, String>)sb);
                                list.add((List<Object>)o);
                                this.notificationSeen.put(glanceApp, list);
                                final Logger sLogger = GlanceTracker.sLogger;
                                sb = new StringBuilder();
                                sLogger.v(sb.append("first notification[").append(glanceApp).append("]").toString());
                                break;
                            }
                            break Label_0177;
                    }
                    return time;
                }
            }
            final Iterator<GlanceInfo> iterator = ((List<GlanceInfo>)o).iterator();
            while (iterator.hasNext()) {
                final GlanceInfo glanceInfo = iterator.next();
                if (SystemClock.elapsedRealtime() - glanceInfo.time > GlanceTracker.NOTIFICATION_SEEN_THRESHOLD) {
                    iterator.remove();
                    GlanceTracker.sLogger.v("expired notification removed [" + glanceApp + "]");
                }
                else {
                    if (this.isNotificationSame(glanceInfo, glanceApp, (Map<String, String>)sb)) {
                        time = glanceInfo.time;
                        glanceInfo.time = SystemClock.elapsedRealtime();
                        final Logger sLogger2 = GlanceTracker.sLogger;
                        sb = new StringBuilder();
                        sLogger2.v(sb.append("notification has been seen [").append(glanceApp).append("] time [").append(time).append("]").toString());
                        return time;
                    }
                    continue;
                }
            }
            final GlanceEvent glanceEvent2;
            ((List<GlanceInfo>)o).add(new GlanceInfo(SystemClock.elapsedRealtime(), glanceEvent2, (Map<String, String>)sb));
            final Logger sLogger3 = GlanceTracker.sLogger;
            sb = new StringBuilder();
            sLogger3.v(sb.append("new notification [").append(glanceApp).append("]").toString());
            return time;
        }
    }
    
    void restoreState() {
        synchronized (this) {
            GlanceTracker.sLogger.v("restoreState");
            this.notificationSeen = this.notificationSavedState;
            this.notificationSavedState = null;
            if (this.notificationSeen == null) {
                this.notificationSeen = new HashMap<GlanceApp, List<GlanceInfo>>();
            }
        }
    }
    
    void saveState() {
        synchronized (this) {
            GlanceTracker.sLogger.v("saveState");
            this.notificationSavedState = this.notificationSeen;
            this.notificationSeen = new HashMap<GlanceApp, List<GlanceInfo>>();
        }
    }
    
    private static class GlanceInfo
    {
        Map<String, String> data;
        GlanceEvent event;
        long time;
        
        GlanceInfo(final long time, final GlanceEvent event, final Map<String, String> data) {
            this.time = time;
            this.event = event;
            this.data = data;
        }
    }
}
