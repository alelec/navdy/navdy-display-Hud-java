package com.navdy.hud.app.framework.twilio;

import com.navdy.service.library.util.SystemUtils;
import okhttp3.Response;
import java.io.IOException;
import okhttp3.RequestBody;
import com.navdy.hud.app.util.PhoneUtil;
import okhttp3.FormBody;
import okhttp3.Request;
import com.navdy.service.library.task.TaskManager;
import com.navdy.hud.app.profile.DriverProfile;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import android.text.TextUtils;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import android.content.Context;
import okhttp3.Credentials;
import com.navdy.service.library.util.CredentialUtil;
import com.navdy.hud.app.HudApplication;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import com.navdy.hud.app.profile.DriverProfileManager;
import com.navdy.service.library.log.Logger;

public class TwilioSmsManager
{
    private static final String ACCOUNT_SID;
    private static final String AUTH_TOKEN;
    private static final int CONNECTION_TIMEOUT;
    private static final String CREDENTIALS;
    private static final String MESSAGE_SERVICE_ID;
    private static final int READ_TIMEOUT;
    private static final String SMS_ENDPOINT;
    private static final int WRITE_TIMEOUT;
    private static TwilioSmsManager instance;
    private static final Logger sLogger;
    private final DriverProfileManager driverProfileManager;
    private final OkHttpClient okHttpClient;
    
    static {
        sLogger = new Logger(TwilioSmsManager.class);
        CONNECTION_TIMEOUT = (int)TimeUnit.SECONDS.toMillis(15L);
        READ_TIMEOUT = (int)TimeUnit.SECONDS.toMillis(30L);
        WRITE_TIMEOUT = (int)TimeUnit.SECONDS.toMillis(30L);
        final Context appContext = HudApplication.getAppContext();
        ACCOUNT_SID = CredentialUtil.getCredentials(appContext, "TWILIO_ACCOUNT_SID");
        AUTH_TOKEN = CredentialUtil.getCredentials(appContext, "TWILIO_AUTH_TOKEN");
        MESSAGE_SERVICE_ID = CredentialUtil.getCredentials(appContext, "TWILIO_MSG_SERVICE_ID");
        SMS_ENDPOINT = "https://api.twilio.com/2010-04-01/Accounts/" + TwilioSmsManager.ACCOUNT_SID + "/Messages.json";
        CREDENTIALS = Credentials.basic(TwilioSmsManager.ACCOUNT_SID, TwilioSmsManager.AUTH_TOKEN);
        TwilioSmsManager.instance = new TwilioSmsManager();
    }
    
    private TwilioSmsManager() {
        this.okHttpClient = RemoteDeviceManager.getInstance().getHttpManager().getClient().newBuilder().readTimeout(TwilioSmsManager.READ_TIMEOUT, TimeUnit.MILLISECONDS).writeTimeout(TwilioSmsManager.WRITE_TIMEOUT, TimeUnit.MILLISECONDS).connectTimeout(TwilioSmsManager.CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS).build();
        this.driverProfileManager = DriverProfileHelper.getInstance().getDriverProfileManager();
    }
    
    public static TwilioSmsManager getInstance() {
        return TwilioSmsManager.instance;
    }
    
    private void sendResult(final ErrorCode errorCode, final String s, final Callback callback) {
        if (callback != null) {
            final DriverProfile currentProfile = this.driverProfileManager.getCurrentProfile();
            final String profileName = currentProfile.getProfileName();
            if (currentProfile.isDefaultProfile() || TextUtils.equals((CharSequence)s, (CharSequence)profileName)) {
                switch (errorCode) {
                    default:
                        AnalyticsSupport.recordSmsSent(false, "iOS", GlanceConstants.areMessageCanned());
                        break;
                    case SUCCESS:
                        AnalyticsSupport.recordSmsSent(true, "iOS", GlanceConstants.areMessageCanned());
                        break;
                }
                callback.result(errorCode);
            }
            else {
                TwilioSmsManager.sLogger.v("profile changed:" + s + "," + profileName);
            }
        }
    }
    
    public ErrorCode sendSms(final String s, final String s2, final Callback callback) {
        Label_0014: {
            if (s != null) {
                if (s2 != null) {
                    break Label_0014;
                }
            }
            while (true) {
                ErrorCode errorCode;
                try {
                    errorCode = ErrorCode.INVALID_PARAMETER;
                    return errorCode;
                    errorCode = ErrorCode.NETWORK_ERROR;
                    return errorCode;
                    Label_0030: {
                        TaskManager.getInstance().execute(new Runnable() {
                            final /* synthetic */ String val$profileId = TwilioSmsManager.this.driverProfileManager.getCurrentProfile().getProfileName();
                            
                            @Override
                            public void run() {
                                try {
                                    TwilioSmsManager.sLogger.v("sendSms-start");
                                    final String firstName = DriverProfileHelper.getInstance().getCurrentProfile().getFirstName();
                                    String s;
                                    if (!TextUtils.isEmpty((CharSequence)firstName)) {
                                        s = HudApplication.getAppContext().getString(R.string.ios_sms_format, new Object[] { firstName, s2 });
                                    }
                                    else {
                                        s = s2;
                                    }
                                    final Response execute = TwilioSmsManager.this.okHttpClient.newCall(new Request.Builder().url(TwilioSmsManager.SMS_ENDPOINT).header("Authorization", TwilioSmsManager.CREDENTIALS).post(new FormBody.Builder().add("To", PhoneUtil.convertToE164Format(s)).add("MessagingServiceSid", TwilioSmsManager.MESSAGE_SERVICE_ID).add("Body", s).build()).build()).execute();
                                    if (!execute.isSuccessful()) {
                                        TwilioSmsManager.sLogger.e("sendSms-end-err:" + execute.code() + "," + execute.message());
                                        TwilioSmsManager.this.sendResult(ErrorCode.TWILIO_SERVER_ERROR, this.val$profileId, callback);
                                    }
                                    else {
                                        TwilioSmsManager.sLogger.v("sendSms-end-suc:" + execute.code());
                                        TwilioSmsManager.this.sendResult(ErrorCode.SUCCESS, this.val$profileId, callback);
                                    }
                                }
                                catch (Throwable t) {
                                    TwilioSmsManager.sLogger.e("sendSms-end-err", t);
                                    if (t instanceof IOException) {
                                        TwilioSmsManager.this.sendResult(ErrorCode.NETWORK_ERROR, this.val$profileId, callback);
                                        return;
                                    }
                                    TwilioSmsManager.this.sendResult(ErrorCode.INTERNAL_ERROR, this.val$profileId, callback);
                                }
                            }
                        }, 23);
                    }
                    errorCode = ErrorCode.SUCCESS;
                    return errorCode;
                    // iftrue(Label_0030:, SystemUtils.isConnectedToNetwork(HudApplication.getAppContext()))
                    return ErrorCode.NETWORK_ERROR;
                }
                catch (Throwable t) {
                    TwilioSmsManager.sLogger.e("sendSms", t);
                    errorCode = ErrorCode.INTERNAL_ERROR;
                    return errorCode;
                }
                return errorCode;
            }
        }
    }
    
    public interface Callback
    {
        void result(final ErrorCode p0);
    }
    
    public enum ErrorCode
    {
        INTERNAL_ERROR, 
        INVALID_PARAMETER, 
        NETWORK_ERROR, 
        SUCCESS, 
        TWILIO_SERVER_ERROR;
    }
}
