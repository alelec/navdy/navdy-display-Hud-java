package com.navdy.hud.app.framework.glance;

import com.navdy.hud.app.framework.message.SmsNotification;
import com.squareup.wire.Message;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.service.library.events.messaging.SmsMessageRequest;
import java.util.UUID;
import com.navdy.hud.app.framework.twilio.TwilioSmsManager;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.messaging.SmsMessageResponse;
import com.squareup.otto.Subscribe;
import com.navdy.service.library.task.TaskManager;
import java.util.Iterator;
import java.util.Map;
import com.navdy.hud.app.framework.notifications.INotification;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.contacts.Contact;
import android.os.SystemClock;
import com.navdy.service.library.events.glances.CalendarConstants;
import com.navdy.service.library.events.glances.EmailConstants;
import com.navdy.service.library.events.glances.GenericConstants;
import com.navdy.service.library.events.glances.SocialConstants;
import java.util.List;
import com.navdy.service.library.events.glances.KeyValue;
import java.util.ArrayList;
import com.navdy.hud.app.framework.recentcall.RecentCallManager;
import java.util.Date;
import com.navdy.hud.app.framework.contacts.NumberType;
import com.navdy.hud.app.framework.recentcall.RecentCall;
import com.navdy.hud.app.framework.contacts.ContactUtil;
import com.navdy.service.library.events.glances.MessageConstants;
import android.text.TextUtils;
import com.navdy.service.library.events.glances.GlanceEvent;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import java.util.HashMap;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;
import java.text.SimpleDateFormat;

public class GlanceHandler
{
    private static final String EMPTY = "";
    private static final SimpleDateFormat dateFormat1;
    private static final SimpleDateFormat dateFormat2;
    private static final SimpleDateFormat dateFormat3;
    private static final SimpleDateFormat dateFormat4;
    private static final SimpleDateFormat dateFormat5;
    private static final Object lockObj;
    private static final GlanceHandler sInstance;
    public static final Logger sLogger;
    private Bus bus;
    private GlanceTracker glanceTracker;
    private HashMap<String, String> messagesSent;
    
    static {
        sLogger = new Logger(GlanceHandler.class);
        sInstance = new GlanceHandler();
        lockObj = new Object();
        dateFormat1 = new SimpleDateFormat("hh:mm a");
        dateFormat2 = new SimpleDateFormat("hh:mm");
        dateFormat3 = new SimpleDateFormat("MMM d, hh:mm a");
        dateFormat4 = new SimpleDateFormat("h");
        dateFormat5 = new SimpleDateFormat("h a");
    }
    
    private GlanceHandler() {
        this.glanceTracker = new GlanceTracker();
        this.messagesSent = new HashMap<String, String>();
        (this.bus = RemoteDeviceManager.getInstance().getBus()).register(this);
    }
    
    private void clearAllMessage() {
        final HashMap<String, String> messagesSent = this.messagesSent;
        synchronized (messagesSent) {
            this.messagesSent.clear();
        }
    }
    
    private long extractTime(final String p0) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore_2       
        //     2: aload_1        
        //     3: ldc             "\u2013"
        //     5: invokevirtual   java/lang/String.indexOf:(Ljava/lang/String;)I
        //     8: istore_3       
        //     9: iload_3        
        //    10: iconst_m1      
        //    11: if_icmpeq       200
        //    14: aload_1        
        //    15: iload_3        
        //    16: iconst_1       
        //    17: iadd           
        //    18: invokevirtual   java/lang/String.substring:(I)Ljava/lang/String;
        //    21: invokevirtual   java/lang/String.trim:()Ljava/lang/String;
        //    24: astore_2       
        //    25: aload_1        
        //    26: iconst_0       
        //    27: iload_3        
        //    28: invokevirtual   java/lang/String.substring:(II)Ljava/lang/String;
        //    31: invokevirtual   java/lang/String.trim:()Ljava/lang/String;
        //    34: astore_1       
        //    35: aload_2        
        //    36: invokestatic    com/navdy/hud/app/framework/contacts/ContactUtil.sanitizeString:(Ljava/lang/String;)Ljava/lang/String;
        //    39: astore_2       
        //    40: aload_1        
        //    41: invokestatic    com/navdy/hud/app/framework/contacts/ContactUtil.sanitizeString:(Ljava/lang/String;)Ljava/lang/String;
        //    44: astore_1       
        //    45: getstatic       com/navdy/hud/app/framework/glance/GlanceHandler.lockObj:Ljava/lang/Object;
        //    48: astore          4
        //    50: aload           4
        //    52: dup            
        //    53: astore          5
        //    55: monitorenter   
        //    56: invokestatic    java/util/Calendar.getInstance:()Ljava/util/Calendar;
        //    59: astore          6
        //    61: invokestatic    java/util/Calendar.getInstance:()Ljava/util/Calendar;
        //    64: astore          7
        //    66: aload           7
        //    68: getstatic       com/navdy/hud/app/framework/glance/GlanceHandler.dateFormat1:Ljava/text/SimpleDateFormat;
        //    71: aload_1        
        //    72: invokevirtual   java/text/SimpleDateFormat.parse:(Ljava/lang/String;)Ljava/util/Date;
        //    75: invokevirtual   java/util/Calendar.setTime:(Ljava/util/Date;)V
        //    78: aload           6
        //    80: bipush          10
        //    82: aload           7
        //    84: bipush          10
        //    86: invokevirtual   java/util/Calendar.get:(I)I
        //    89: invokevirtual   java/util/Calendar.set:(II)V
        //    92: aload           6
        //    94: bipush          12
        //    96: aload           7
        //    98: bipush          12
        //   100: invokevirtual   java/util/Calendar.get:(I)I
        //   103: invokevirtual   java/util/Calendar.set:(II)V
        //   106: aload           6
        //   108: bipush          13
        //   110: aload           7
        //   112: bipush          13
        //   114: invokevirtual   java/util/Calendar.get:(I)I
        //   117: invokevirtual   java/util/Calendar.set:(II)V
        //   120: aload           6
        //   122: bipush          14
        //   124: iconst_0       
        //   125: invokevirtual   java/util/Calendar.set:(II)V
        //   128: aload           6
        //   130: bipush          9
        //   132: aload           7
        //   134: bipush          9
        //   136: invokevirtual   java/util/Calendar.get:(I)I
        //   139: invokevirtual   java/util/Calendar.set:(II)V
        //   142: aload           6
        //   144: invokevirtual   java/util/Calendar.getTime:()Ljava/util/Date;
        //   147: astore          7
        //   149: getstatic       com/navdy/hud/app/framework/glance/GlanceHandler.sLogger:Lcom/navdy/service/library/log/Logger;
        //   152: astore          8
        //   154: new             Ljava/lang/StringBuilder;
        //   157: astore          9
        //   159: aload           9
        //   161: invokespecial   java/lang/StringBuilder.<init>:()V
        //   164: aload           8
        //   166: aload           9
        //   168: ldc             "event-time-date = "
        //   170: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   173: aload           7
        //   175: invokevirtual   java/util/Date.toString:()Ljava/lang/String;
        //   178: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   181: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   184: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   187: aload           7
        //   189: invokevirtual   java/util/Date.getTime:()J
        //   192: lstore          10
        //   194: aload           5
        //   196: monitorexit    
        //   197: lload           10
        //   199: lreturn        
        //   200: aload_1        
        //   201: invokestatic    com/navdy/hud/app/framework/contacts/ContactUtil.sanitizeString:(Ljava/lang/String;)Ljava/lang/String;
        //   204: astore_1       
        //   205: goto            45
        //   208: astore          7
        //   210: invokestatic    java/util/Calendar.getInstance:()Ljava/util/Calendar;
        //   213: astore          7
        //   215: aload           7
        //   217: getstatic       com/navdy/hud/app/framework/glance/GlanceHandler.dateFormat2:Ljava/text/SimpleDateFormat;
        //   220: aload_1        
        //   221: invokevirtual   java/text/SimpleDateFormat.parse:(Ljava/lang/String;)Ljava/util/Date;
        //   224: invokevirtual   java/util/Calendar.setTime:(Ljava/util/Date;)V
        //   227: aload           6
        //   229: bipush          10
        //   231: aload           7
        //   233: bipush          10
        //   235: invokevirtual   java/util/Calendar.get:(I)I
        //   238: invokevirtual   java/util/Calendar.set:(II)V
        //   241: aload           6
        //   243: bipush          12
        //   245: aload           7
        //   247: bipush          12
        //   249: invokevirtual   java/util/Calendar.get:(I)I
        //   252: invokevirtual   java/util/Calendar.set:(II)V
        //   255: aload           6
        //   257: bipush          13
        //   259: aload           7
        //   261: bipush          13
        //   263: invokevirtual   java/util/Calendar.get:(I)I
        //   266: invokevirtual   java/util/Calendar.set:(II)V
        //   269: aload           6
        //   271: bipush          14
        //   273: iconst_0       
        //   274: invokevirtual   java/util/Calendar.set:(II)V
        //   277: aload_2        
        //   278: ifnull          312
        //   281: invokestatic    java/util/Calendar.getInstance:()Ljava/util/Calendar;
        //   284: astore          7
        //   286: aload           7
        //   288: getstatic       com/navdy/hud/app/framework/glance/GlanceHandler.dateFormat1:Ljava/text/SimpleDateFormat;
        //   291: aload_2        
        //   292: invokevirtual   java/text/SimpleDateFormat.parse:(Ljava/lang/String;)Ljava/util/Date;
        //   295: invokevirtual   java/util/Calendar.setTime:(Ljava/util/Date;)V
        //   298: aload           6
        //   300: bipush          9
        //   302: aload           7
        //   304: bipush          9
        //   306: invokevirtual   java/util/Calendar.get:(I)I
        //   309: invokevirtual   java/util/Calendar.set:(II)V
        //   312: aload           6
        //   314: invokevirtual   java/util/Calendar.getTime:()Ljava/util/Date;
        //   317: astore          8
        //   319: getstatic       com/navdy/hud/app/framework/glance/GlanceHandler.sLogger:Lcom/navdy/service/library/log/Logger;
        //   322: astore          7
        //   324: new             Ljava/lang/StringBuilder;
        //   327: astore          9
        //   329: aload           9
        //   331: invokespecial   java/lang/StringBuilder.<init>:()V
        //   334: aload           7
        //   336: aload           9
        //   338: ldc             "event-time-date = "
        //   340: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   343: aload           8
        //   345: invokevirtual   java/util/Date.toString:()Ljava/lang/String;
        //   348: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   351: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   354: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   357: aload           8
        //   359: invokevirtual   java/util/Date.getTime:()J
        //   362: lstore          10
        //   364: aload           5
        //   366: monitorexit    
        //   367: goto            197
        //   370: astore_1       
        //   371: aload           5
        //   373: monitorexit    
        //   374: aload_1        
        //   375: athrow         
        //   376: astore          7
        //   378: invokestatic    java/util/Calendar.getInstance:()Ljava/util/Calendar;
        //   381: astore          7
        //   383: aload           7
        //   385: getstatic       com/navdy/hud/app/framework/glance/GlanceHandler.dateFormat3:Ljava/text/SimpleDateFormat;
        //   388: aload_1        
        //   389: invokevirtual   java/text/SimpleDateFormat.parse:(Ljava/lang/String;)Ljava/util/Date;
        //   392: invokevirtual   java/util/Calendar.setTime:(Ljava/util/Date;)V
        //   395: aload           6
        //   397: bipush          10
        //   399: aload           7
        //   401: bipush          10
        //   403: invokevirtual   java/util/Calendar.get:(I)I
        //   406: invokevirtual   java/util/Calendar.set:(II)V
        //   409: aload           6
        //   411: bipush          12
        //   413: aload           7
        //   415: bipush          12
        //   417: invokevirtual   java/util/Calendar.get:(I)I
        //   420: invokevirtual   java/util/Calendar.set:(II)V
        //   423: aload           6
        //   425: bipush          13
        //   427: aload           7
        //   429: bipush          13
        //   431: invokevirtual   java/util/Calendar.get:(I)I
        //   434: invokevirtual   java/util/Calendar.set:(II)V
        //   437: aload           6
        //   439: bipush          14
        //   441: iconst_0       
        //   442: invokevirtual   java/util/Calendar.set:(II)V
        //   445: aload           6
        //   447: bipush          9
        //   449: aload           7
        //   451: bipush          9
        //   453: invokevirtual   java/util/Calendar.get:(I)I
        //   456: invokevirtual   java/util/Calendar.set:(II)V
        //   459: aload           6
        //   461: invokevirtual   java/util/Calendar.getTime:()Ljava/util/Date;
        //   464: astore          8
        //   466: getstatic       com/navdy/hud/app/framework/glance/GlanceHandler.sLogger:Lcom/navdy/service/library/log/Logger;
        //   469: astore          7
        //   471: new             Ljava/lang/StringBuilder;
        //   474: astore          9
        //   476: aload           9
        //   478: invokespecial   java/lang/StringBuilder.<init>:()V
        //   481: aload           7
        //   483: aload           9
        //   485: ldc             "event-time-date = "
        //   487: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   490: aload           8
        //   492: invokevirtual   java/util/Date.toString:()Ljava/lang/String;
        //   495: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   498: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   501: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   504: aload           8
        //   506: invokevirtual   java/util/Date.getTime:()J
        //   509: lstore          10
        //   511: aload           5
        //   513: monitorexit    
        //   514: goto            197
        //   517: astore          7
        //   519: invokestatic    java/util/Calendar.getInstance:()Ljava/util/Calendar;
        //   522: astore          7
        //   524: aload           7
        //   526: getstatic       com/navdy/hud/app/framework/glance/GlanceHandler.dateFormat4:Ljava/text/SimpleDateFormat;
        //   529: aload_1        
        //   530: invokevirtual   java/text/SimpleDateFormat.parse:(Ljava/lang/String;)Ljava/util/Date;
        //   533: invokevirtual   java/util/Calendar.setTime:(Ljava/util/Date;)V
        //   536: aload           6
        //   538: bipush          10
        //   540: aload           7
        //   542: bipush          10
        //   544: invokevirtual   java/util/Calendar.get:(I)I
        //   547: invokevirtual   java/util/Calendar.set:(II)V
        //   550: aload           6
        //   552: bipush          12
        //   554: iconst_0       
        //   555: invokevirtual   java/util/Calendar.set:(II)V
        //   558: aload           6
        //   560: bipush          13
        //   562: iconst_0       
        //   563: invokevirtual   java/util/Calendar.set:(II)V
        //   566: aload           6
        //   568: bipush          14
        //   570: iconst_0       
        //   571: invokevirtual   java/util/Calendar.set:(II)V
        //   574: aload_2        
        //   575: ifnull          609
        //   578: invokestatic    java/util/Calendar.getInstance:()Ljava/util/Calendar;
        //   581: astore          7
        //   583: aload           7
        //   585: getstatic       com/navdy/hud/app/framework/glance/GlanceHandler.dateFormat5:Ljava/text/SimpleDateFormat;
        //   588: aload_2        
        //   589: invokevirtual   java/text/SimpleDateFormat.parse:(Ljava/lang/String;)Ljava/util/Date;
        //   592: invokevirtual   java/util/Calendar.setTime:(Ljava/util/Date;)V
        //   595: aload           6
        //   597: bipush          9
        //   599: aload           7
        //   601: bipush          9
        //   603: invokevirtual   java/util/Calendar.get:(I)I
        //   606: invokevirtual   java/util/Calendar.set:(II)V
        //   609: aload           6
        //   611: invokevirtual   java/util/Calendar.getTime:()Ljava/util/Date;
        //   614: astore          7
        //   616: getstatic       com/navdy/hud/app/framework/glance/GlanceHandler.sLogger:Lcom/navdy/service/library/log/Logger;
        //   619: astore_2       
        //   620: new             Ljava/lang/StringBuilder;
        //   623: astore          9
        //   625: aload           9
        //   627: invokespecial   java/lang/StringBuilder.<init>:()V
        //   630: aload_2        
        //   631: aload           9
        //   633: ldc             "event-time-date = "
        //   635: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   638: aload           7
        //   640: invokevirtual   java/util/Date.toString:()Ljava/lang/String;
        //   643: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   646: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   649: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   652: aload           7
        //   654: invokevirtual   java/util/Date.getTime:()J
        //   657: lstore          10
        //   659: aload           5
        //   661: monitorexit    
        //   662: goto            197
        //   665: astore_2       
        //   666: invokestatic    java/util/Calendar.getInstance:()Ljava/util/Calendar;
        //   669: astore_2       
        //   670: aload_2        
        //   671: getstatic       com/navdy/hud/app/framework/glance/GlanceHandler.dateFormat5:Ljava/text/SimpleDateFormat;
        //   674: aload_1        
        //   675: invokevirtual   java/text/SimpleDateFormat.parse:(Ljava/lang/String;)Ljava/util/Date;
        //   678: invokevirtual   java/util/Calendar.setTime:(Ljava/util/Date;)V
        //   681: aload           6
        //   683: bipush          10
        //   685: aload_2        
        //   686: bipush          10
        //   688: invokevirtual   java/util/Calendar.get:(I)I
        //   691: invokevirtual   java/util/Calendar.set:(II)V
        //   694: aload           6
        //   696: bipush          12
        //   698: aload_2        
        //   699: bipush          12
        //   701: invokevirtual   java/util/Calendar.get:(I)I
        //   704: invokevirtual   java/util/Calendar.set:(II)V
        //   707: aload           6
        //   709: bipush          13
        //   711: aload_2        
        //   712: bipush          13
        //   714: invokevirtual   java/util/Calendar.get:(I)I
        //   717: invokevirtual   java/util/Calendar.set:(II)V
        //   720: aload           6
        //   722: bipush          14
        //   724: iconst_0       
        //   725: invokevirtual   java/util/Calendar.set:(II)V
        //   728: aload           6
        //   730: bipush          9
        //   732: aload_2        
        //   733: bipush          9
        //   735: invokevirtual   java/util/Calendar.get:(I)I
        //   738: invokevirtual   java/util/Calendar.set:(II)V
        //   741: aload           6
        //   743: invokevirtual   java/util/Calendar.getTime:()Ljava/util/Date;
        //   746: astore          6
        //   748: getstatic       com/navdy/hud/app/framework/glance/GlanceHandler.sLogger:Lcom/navdy/service/library/log/Logger;
        //   751: astore_1       
        //   752: new             Ljava/lang/StringBuilder;
        //   755: astore_2       
        //   756: aload_2        
        //   757: invokespecial   java/lang/StringBuilder.<init>:()V
        //   760: aload_1        
        //   761: aload_2        
        //   762: ldc             "event-time-date = "
        //   764: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   767: aload           6
        //   769: invokevirtual   java/util/Date.toString:()Ljava/lang/String;
        //   772: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   775: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   778: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   781: aload           6
        //   783: invokevirtual   java/util/Date.getTime:()J
        //   786: lstore          10
        //   788: aload           5
        //   790: monitorexit    
        //   791: goto            197
        //   794: astore_1       
        //   795: aload           5
        //   797: monitorexit    
        //   798: lconst_0       
        //   799: lstore          10
        //   801: goto            197
        //   804: astore_2       
        //   805: goto            609
        //   808: astore          7
        //   810: goto            312
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  56     61     370    376    Any
        //  61     194    208    813    Ljava/lang/Throwable;
        //  61     194    370    376    Any
        //  194    197    370    376    Any
        //  210    277    376    808    Ljava/lang/Throwable;
        //  210    277    370    376    Any
        //  281    312    808    813    Ljava/lang/Throwable;
        //  281    312    370    376    Any
        //  312    364    376    808    Ljava/lang/Throwable;
        //  312    364    370    376    Any
        //  364    367    370    376    Any
        //  371    374    370    376    Any
        //  378    511    517    808    Ljava/lang/Throwable;
        //  378    511    370    376    Any
        //  511    514    370    376    Any
        //  519    574    665    804    Ljava/lang/Throwable;
        //  519    574    370    376    Any
        //  578    609    804    808    Ljava/lang/Throwable;
        //  578    609    370    376    Any
        //  609    659    665    804    Ljava/lang/Throwable;
        //  609    659    370    376    Any
        //  659    662    370    376    Any
        //  666    788    794    804    Ljava/lang/Throwable;
        //  666    788    370    376    Any
        //  788    791    370    376    Any
        //  795    798    370    376    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index: 369, Size: 369
        //     at java.util.ArrayList.rangeCheck(Unknown Source)
        //     at java.util.ArrayList.get(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static GlanceHandler getInstance() {
        return GlanceHandler.sInstance;
    }
    
    private void handleGlancEventInternal(final GlanceEvent glanceEvent) {
        printGlance(glanceEvent);
        if (TextUtils.isEmpty((CharSequence)glanceEvent.id) || TextUtils.isEmpty((CharSequence)glanceEvent.provider) || glanceEvent.glanceType == null || glanceEvent.glanceData == null || glanceEvent.glanceData.size() == 0) {
            GlanceHandler.sLogger.v("invalid glance event");
        }
        else {
            GlanceApp glanceApp;
            if ((glanceApp = GlanceHelper.getGlancesApp(glanceEvent)) == null) {
                glanceApp = GlanceApp.GENERIC;
            }
            final Map<String, String> buildDataMap = GlanceHelper.buildDataMap(glanceEvent);
            final List<com.navdy.service.library.events.contacts.Contact> list = null;
            final List<com.navdy.service.library.events.contacts.Contact> list2 = null;
            GlanceEvent build = null;
            Map<String, String> buildDataMap2 = null;
            List<com.navdy.service.library.events.contacts.Contact> list3 = null;
            switch (glanceApp) {
                default:
                    build = glanceEvent;
                    buildDataMap2 = buildDataMap;
                    list3 = list2;
                    break;
                case SMS:
                case IMESSAGE:
                case GOOGLE_HANGOUT: {
                    boolean b = false;
                    if (TextUtils.isEmpty((CharSequence)buildDataMap.get(MessageConstants.MESSAGE_BODY.name()))) {
                        GlanceHandler.sLogger.v("glance message does not exist:" + glanceApp);
                        return;
                    }
                    final String number = GlanceHelper.getNumber(glanceApp, buildDataMap);
                    final String from = GlanceHelper.getFrom(glanceApp, buildDataMap);
                    String s;
                    String s2;
                    if (ContactUtil.isValidNumber(number)) {
                        final boolean b2 = true;
                        GlanceHandler.sLogger.v("hasNumber:" + number);
                        s = from;
                        s2 = number;
                        b = b2;
                        if (from == null) {
                            s = number;
                            b = b2;
                            s2 = number;
                        }
                    }
                    else {
                        s = null;
                        s2 = from;
                    }
                    list3 = list2;
                    buildDataMap2 = buildDataMap;
                    build = glanceEvent;
                    if (s2 == null) {
                        break;
                    }
                    final RecentCall recentCall = new RecentCall(s, RecentCall.Category.MESSAGE, s2, NumberType.OTHER, new Date(), RecentCall.CallType.INCOMING, -1, 0L);
                    final RecentCallManager instance = RecentCallManager.getInstance();
                    final boolean handleNewCall = instance.handleNewCall(recentCall);
                    List<com.navdy.service.library.events.contacts.Contact> contactsFromId = list;
                    if (!handleNewCall) {
                        contactsFromId = instance.getContactsFromId(s2);
                        GlanceHandler.sLogger.v("recent call contact list found [" + s2 + "]");
                    }
                    GlanceHandler.sLogger.v("recent call id[" + s2 + "]");
                    list3 = contactsFromId;
                    buildDataMap2 = buildDataMap;
                    build = glanceEvent;
                    if (b) {
                        break;
                    }
                    if (handleNewCall) {
                        GlanceHandler.sLogger.v("got Number:" + recentCall.number);
                        final ArrayList<KeyValue> list4 = new ArrayList<KeyValue>();
                        for (final KeyValue keyValue : glanceEvent.glanceData) {
                            if (!TextUtils.equals((CharSequence)keyValue.key, (CharSequence)MessageConstants.MESSAGE_FROM_NUMBER.name())) {
                                list4.add(keyValue);
                            }
                        }
                        list4.add(new KeyValue(MessageConstants.MESSAGE_FROM_NUMBER.name(), recentCall.number));
                        build = new GlanceEvent.Builder().id(glanceEvent.id).actions(glanceEvent.actions).postTime(glanceEvent.postTime).glanceType(glanceEvent.glanceType).provider(glanceEvent.provider).glanceData(list4).build();
                        buildDataMap2 = GlanceHelper.buildDataMap(build);
                        list3 = contactsFromId;
                        break;
                    }
                    GlanceHandler.sLogger.v("no Number yet:" + s2);
                    list3 = contactsFromId;
                    buildDataMap2 = buildDataMap;
                    build = glanceEvent;
                    break;
                }
                case SLACK:
                case WHATS_APP:
                case FACEBOOK_MESSENGER:
                case GENERIC_MESSAGE:
                    list3 = list2;
                    buildDataMap2 = buildDataMap;
                    build = glanceEvent;
                    if (TextUtils.isEmpty((CharSequence)buildDataMap.get(MessageConstants.MESSAGE_BODY.name()))) {
                        GlanceHandler.sLogger.v("glance message does not exist:" + glanceApp);
                        return;
                    }
                    break;
                case FACEBOOK:
                case TWITTER:
                case GENERIC_SOCIAL:
                    list3 = list2;
                    buildDataMap2 = buildDataMap;
                    build = glanceEvent;
                    if (TextUtils.isEmpty((CharSequence)buildDataMap.get(SocialConstants.SOCIAL_MESSAGE.name()))) {
                        GlanceHandler.sLogger.v("glance message does not exist:" + glanceApp);
                        return;
                    }
                    break;
                case GENERIC:
                    list3 = list2;
                    buildDataMap2 = buildDataMap;
                    build = glanceEvent;
                    if (TextUtils.isEmpty((CharSequence)buildDataMap.get(GenericConstants.GENERIC_MESSAGE.name()))) {
                        GlanceHandler.sLogger.v("glance message does not exist:" + glanceApp);
                        return;
                    }
                    break;
                case GOOGLE_MAIL:
                case GOOGLE_INBOX:
                case APPLE_MAIL:
                case GENERIC_MAIL: {
                    final String s3 = buildDataMap.get(EmailConstants.EMAIL_BODY.name());
                    final String s4 = buildDataMap.get(EmailConstants.EMAIL_SUBJECT.name());
                    list3 = list2;
                    buildDataMap2 = buildDataMap;
                    build = glanceEvent;
                    if (!TextUtils.isEmpty((CharSequence)s3)) {
                        break;
                    }
                    list3 = list2;
                    buildDataMap2 = buildDataMap;
                    build = glanceEvent;
                    if (TextUtils.isEmpty((CharSequence)s4)) {
                        GlanceHandler.sLogger.v("glance message does not exist:" + glanceApp);
                        return;
                    }
                    break;
                }
            }
            GlanceNotification glanceNotification = null;
            String s5 = null;
            Label_0260: {
                if (GlanceHelper.usesMessageLayout(glanceApp)) {
                    final boolean needsScrollLayout = GlanceHelper.needsScrollLayout(GlanceHelper.getGlanceMessage(glanceApp, buildDataMap2));
                    if (needsScrollLayout) {
                        glanceNotification = new GlanceNotification(build, glanceApp, GlanceViewCache.ViewType.BIG_GLANCE_MESSAGE, buildDataMap2);
                    }
                    else {
                        glanceNotification = new GlanceNotification(build, glanceApp, buildDataMap2);
                    }
                    s5 = "posted glance [" + build.id + "] scroll[" + needsScrollLayout + "]";
                }
                else {
                    if (GlanceHelper.isCalendarApp(glanceApp)) {
                        final String s6 = buildDataMap2.get(CalendarConstants.CALENDAR_TIME.name());
                        long n2;
                        long n = n2 = 0L;
                        while (true) {
                            if (s6 == null) {
                                break Label_1226;
                            }
                            try {
                                n2 = Long.parseLong(s6);
                                n = n2;
                                if (n2 == 0L) {
                                    final String s7 = buildDataMap2.get(CalendarConstants.CALENDAR_TIME_STR.name());
                                    GlanceHandler.sLogger.v("event-time-str =" + s7);
                                    if (!TextUtils.isEmpty((CharSequence)s7)) {
                                        n2 = this.extractTime(s7);
                                    }
                                    n = n2;
                                    if (n2 != 0L) {
                                        buildDataMap2.put(CalendarConstants.CALENDAR_TIME.name(), String.valueOf(n2));
                                        n = n2;
                                    }
                                }
                                glanceNotification = new GlanceNotification(build, glanceApp, buildDataMap2);
                                s5 = "posted glance [" + build.id + "] time[" + n + "]";
                                break Label_0260;
                            }
                            catch (Throwable t) {
                                GlanceHandler.sLogger.e(t);
                                n2 = n;
                                continue;
                            }
                            break;
                        }
                    }
                    glanceNotification = new GlanceNotification(build, glanceApp, buildDataMap2);
                    s5 = "posted glance [" + build.id + "]";
                }
            }
            final long notificationSeen = this.glanceTracker.isNotificationSeen(build, glanceApp, buildDataMap2);
            if (notificationSeen > 0L) {
                GlanceHandler.sLogger.v("glance already seen [" + build.id + "] at [" + notificationSeen + "] now [" + SystemClock.elapsedRealtime() + "]");
            }
            else {
                if (list3 != null) {
                    final ArrayList<Contact> contactList = new ArrayList<Contact>();
                    final Iterator<com.navdy.service.library.events.contacts.Contact> iterator2 = list3.iterator();
                    while (iterator2.hasNext()) {
                        contactList.add(new Contact(iterator2.next()));
                    }
                    glanceNotification.setContactList(contactList);
                }
                NotificationManager.getInstance().addNotification(glanceNotification);
                GlanceHandler.sLogger.v(s5);
            }
        }
    }
    
    private static void printGlance(final GlanceEvent glanceEvent) {
        final int n = 0;
        if (glanceEvent == null) {
            GlanceHandler.sLogger.v("null glance event");
        }
        else {
            final Logger sLogger = GlanceHandler.sLogger;
            final StringBuilder append = new StringBuilder().append("[glance-event] id[").append(glanceEvent.id).append("] type[").append(glanceEvent.glanceType).append("] provider[").append(glanceEvent.provider).append("] data-size [");
            int size;
            if (glanceEvent.glanceData == null) {
                size = 0;
            }
            else {
                size = glanceEvent.glanceData.size();
            }
            final StringBuilder append2 = append.append(size).append("] action-size [");
            int size2;
            if (glanceEvent.actions == null) {
                size2 = n;
            }
            else {
                size2 = glanceEvent.actions.size();
            }
            sLogger.v(append2.append(size2).append("]").toString());
            if (glanceEvent.glanceData != null && glanceEvent.glanceData.size() > 0) {
                for (final KeyValue keyValue : glanceEvent.glanceData) {
                    GlanceHandler.sLogger.v("[glance-event] data key[" + keyValue.key + "] val[" + keyValue.value + "]");
                }
            }
            if (glanceEvent.actions != null && glanceEvent.actions.size() > 0) {
                final Iterator<GlanceEvent.GlanceActions> iterator2 = glanceEvent.actions.iterator();
                while (iterator2.hasNext()) {
                    GlanceHandler.sLogger.v("[glance-event] action[" + iterator2.next() + "]");
                }
            }
        }
    }
    
    public void addMessage(final String s, final String s2) {
        if (s != null && s2 != null) {
            final HashMap<String, String> messagesSent = this.messagesSent;
            synchronized (messagesSent) {
                this.messagesSent.put(s, s2);
            }
        }
    }
    
    public void clearState() {
        this.glanceTracker.clearState();
        this.clearAllMessage();
    }
    
    @Subscribe
    public void onGlanceEvent(final GlanceEvent glanceEvent) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    GlanceHandler.this.handleGlancEventInternal(glanceEvent);
                }
                catch (Throwable t) {
                    GlanceHandler.sLogger.e("handleGlancEventInternal", t);
                }
            }
        }, 14);
    }
    
    @Subscribe
    public void onMessage(final SmsMessageResponse smsMessageResponse) {
        while (true) {
            Label_0073: {
                try {
                    if (smsMessageResponse.status == RequestStatus.REQUEST_SUCCESS) {
                        AnalyticsSupport.recordSmsSent(true, "Android", GlanceConstants.areMessageCanned());
                    }
                    else {
                        AnalyticsSupport.recordSmsSent(false, "Android", GlanceConstants.areMessageCanned());
                        final String removeMessage = this.removeMessage(smsMessageResponse.id);
                        if (removeMessage == null) {
                            break Label_0073;
                        }
                        getInstance().sendSmsFailedNotification(smsMessageResponse.number, removeMessage, smsMessageResponse.name);
                    }
                    return;
                }
                catch (Throwable t) {
                    GlanceHandler.sLogger.e(t);
                    return;
                }
            }
            GlanceHandler.sLogger.e("sms message with id:" + smsMessageResponse.id + " not found");
        }
    }
    
    public String removeMessage(String s) {
        final String s2;
        if (s == null) {
            s2 = null;
        }
        else {
            final HashMap<String, String> messagesSent = this.messagesSent;
            synchronized (messagesSent) {
                s = this.messagesSent.remove(s);
            }
        }
        return s2;
    }
    
    public void restoreState() {
        this.glanceTracker.restoreState();
    }
    
    public void saveState() {
        this.glanceTracker.saveState();
    }
    
    public boolean sendMessage(final String s, final String s2, final String s3) {
        boolean b = true;
        try {
            if (GenericUtil.isClientiOS()) {
                if (TwilioSmsManager.getInstance().sendSms(s, s2, (TwilioSmsManager.Callback)new TwilioSmsManager.Callback() {
                    @Override
                    public void result(final ErrorCode errorCode) {
                        if (errorCode != ErrorCode.SUCCESS) {
                            GlanceHandler.this.sendSmsFailedNotification(s, s2, s3);
                        }
                    }
                }) == TwilioSmsManager.ErrorCode.SUCCESS) {
                    GlanceHandler.sLogger.v("sms-twilio queued");
                }
                else {
                    AnalyticsSupport.recordSmsSent(false, "iOS", GlanceConstants.areMessageCanned());
                    b = false;
                }
            }
            else {
                final String string = UUID.randomUUID().toString();
                this.addMessage(string, s2);
                this.bus.post(new RemoteEvent(new SmsMessageRequest(s, s2, s3, string)));
                GlanceHandler.sLogger.v("sms sent");
            }
            return b;
        }
        catch (Throwable t) {
            GlanceHandler.sLogger.e(t);
            b = false;
            return b;
        }
        return b;
    }
    
    public void sendSmsFailedNotification(final String s, final String s2, final String s3) {
        NotificationManager.getInstance().addNotification(new SmsNotification(SmsNotification.Mode.Failed, UUID.randomUUID().toString(), s, s2, s3));
    }
    
    public void sendSmsSuccessNotification(final String s, final String s2, final String s3, final String s4) {
        NotificationManager.getInstance().addNotification(new SmsNotification(SmsNotification.Mode.Success, s, s2, s3, s4));
    }
}
