package com.navdy.hud.app.framework.destinations;

import android.content.res.Resources;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.hud.app.HudApplication;
import com.navdy.service.library.events.places.PlaceType;
import com.navdy.hud.app.framework.contacts.Contact;
import java.util.List;

public class Destination
{
    public final List<Contact> contacts;
    public final int destinationIcon;
    public final int destinationIconBkColor;
    public final String destinationPlaceId;
    public final String destinationSubtitle;
    public final String destinationTitle;
    public final DestinationType destinationType;
    public final double displayPositionLatitude;
    public final double displayPositionLongitude;
    public final String distanceStr;
    public final FavoriteDestinationType favoriteDestinationType;
    public final String fullAddress;
    public final String identifier;
    public final String initials;
    public boolean isInitialNumber;
    public final double navigationPositionLatitude;
    public final double navigationPositionLongitude;
    public boolean needShortTitleFont;
    public final List<Contact> phoneNumbers;
    public final PlaceCategory placeCategory;
    public final PlaceType placeType;
    public final String recentTimeLabel;
    public final int recentTimeLabelColor;
    public final boolean recommendation;
    
    public Destination(final double navigationPositionLatitude, final double navigationPositionLongitude, final double displayPositionLatitude, final double displayPositionLongitude, final String fullAddress, final String destinationTitle, final String destinationSubtitle, final String identifier, final FavoriteDestinationType favoriteDestinationType, final DestinationType destinationType, final String initials, final PlaceCategory placeCategory, final String recentTimeLabel, final int recentTimeLabelColor, final boolean recommendation, final int destinationIcon, final int destinationIconBkColor, final String destinationPlaceId, final PlaceType placeType, final String distanceStr, final List<Contact> phoneNumbers, final List<Contact> contacts) {
        this.navigationPositionLatitude = navigationPositionLatitude;
        this.navigationPositionLongitude = navigationPositionLongitude;
        this.displayPositionLatitude = displayPositionLatitude;
        this.displayPositionLongitude = displayPositionLongitude;
        this.fullAddress = fullAddress;
        this.destinationTitle = destinationTitle;
        this.destinationSubtitle = destinationSubtitle;
        this.identifier = identifier;
        this.favoriteDestinationType = favoriteDestinationType;
        this.destinationType = destinationType;
        this.initials = initials;
        this.placeCategory = placeCategory;
        this.recentTimeLabel = recentTimeLabel;
        this.recentTimeLabelColor = recentTimeLabelColor;
        this.recommendation = recommendation;
        this.destinationIcon = destinationIcon;
        this.destinationIconBkColor = destinationIconBkColor;
        this.destinationPlaceId = destinationPlaceId;
        this.placeType = placeType;
        this.distanceStr = distanceStr;
        this.phoneNumbers = phoneNumbers;
        this.contacts = contacts;
        try {
            Integer.parseInt(initials);
            this.isInitialNumber = true;
        }
        catch (Throwable t) {
            this.isInitialNumber = false;
        }
    }
    
    private Destination(final Builder builder) {
        this(builder.navigationPositionLatitude, builder.navigationPositionLongitude, builder.displayPositionLatitude, builder.displayPositionLongitude, builder.fullAddress, builder.destinationTitle, builder.destinationSubtitle, builder.identifier, builder.favoriteDestinationType, builder.destinationType, builder.initials, builder.placeCategory, builder.recentTimeLabel, builder.recentTimeLabelColor, builder.recommendation, builder.destinationIcon, builder.destinationIconBkColor, builder.destinationPlaceId, builder.placeType, builder.distanceStr, builder.phoneNumbers, builder.contacts);
    }
    
    public static Destination getGasDestination() {
        final Resources resources = HudApplication.getAppContext().getResources();
        String s;
        String s2;
        if (HereNavigationManager.getInstance().isNavigationModeOn()) {
            s = resources.getString(R.string.suggested_dest_find_gas_on_route_title);
            s2 = resources.getString(R.string.suggested_dest_find_gas_on_route_desc);
        }
        else {
            s = resources.getString(R.string.suggested_dest_find_gas_title);
            s2 = resources.getString(R.string.suggested_dest_find_gas_desc);
        }
        return new Builder().destinationTitle(s).destinationSubtitle(s2).favoriteDestinationType(FavoriteDestinationType.FAVORITE_NONE).destinationType(DestinationType.FIND_GAS).build();
    }
    
    @Override
    public String toString() {
        return "id[" + this.identifier + "] placeid[" + this.destinationPlaceId + "] nav_pos[" + this.navigationPositionLatitude + "," + this.navigationPositionLongitude + "]" + " display_pos[" + this.displayPositionLatitude + "," + this.displayPositionLongitude + "] title[" + this.destinationTitle + "] subTitle[" + this.destinationSubtitle + "] address[" + this.fullAddress + "] place_type[" + this.placeType + "]";
    }
    
    public static class Builder
    {
        private List<Contact> contacts;
        private int destinationIcon;
        private int destinationIconBkColor;
        private String destinationPlaceId;
        private String destinationSubtitle;
        private String destinationTitle;
        private DestinationType destinationType;
        private double displayPositionLatitude;
        private double displayPositionLongitude;
        private String distanceStr;
        private FavoriteDestinationType favoriteDestinationType;
        private String fullAddress;
        private String identifier;
        private String initials;
        private double navigationPositionLatitude;
        private double navigationPositionLongitude;
        private List<Contact> phoneNumbers;
        private PlaceCategory placeCategory;
        private PlaceType placeType;
        private String recentTimeLabel;
        private int recentTimeLabelColor;
        private boolean recommendation;
        
        public Builder() {
            this.destinationIconBkColor = 0;
        }
        
        public Builder(final Destination destination) {
            this.destinationIconBkColor = 0;
            this.navigationPositionLatitude = destination.navigationPositionLatitude;
            this.navigationPositionLongitude = destination.navigationPositionLongitude;
            this.displayPositionLatitude = destination.displayPositionLatitude;
            this.displayPositionLongitude = destination.displayPositionLongitude;
            this.favoriteDestinationType = destination.favoriteDestinationType;
            this.fullAddress = destination.fullAddress;
            this.destinationTitle = destination.destinationTitle;
            this.destinationSubtitle = destination.destinationSubtitle;
            this.identifier = destination.identifier;
            this.destinationType = destination.destinationType;
            this.initials = destination.initials;
            this.placeCategory = destination.placeCategory;
            this.recentTimeLabel = destination.recentTimeLabel;
            this.recentTimeLabelColor = destination.recentTimeLabelColor;
            this.recommendation = destination.recommendation;
            this.destinationIcon = destination.destinationIcon;
            this.destinationIconBkColor = destination.destinationIconBkColor;
            this.destinationPlaceId = destination.destinationPlaceId;
            this.distanceStr = destination.distanceStr;
            this.phoneNumbers = destination.phoneNumbers;
            this.contacts = destination.contacts;
        }
        
        public Destination build() {
            return new Destination(this, null);
        }
        
        public Builder contacts(final List<Contact> contacts) {
            this.contacts = contacts;
            return this;
        }
        
        public Builder destinationIcon(final int destinationIcon) {
            this.destinationIcon = destinationIcon;
            return this;
        }
        
        public Builder destinationIconBkColor(final int destinationIconBkColor) {
            this.destinationIconBkColor = destinationIconBkColor;
            return this;
        }
        
        public Builder destinationPlaceId(final String destinationPlaceId) {
            this.destinationPlaceId = destinationPlaceId;
            return this;
        }
        
        public Builder destinationSubtitle(final String destinationSubtitle) {
            this.destinationSubtitle = destinationSubtitle;
            return this;
        }
        
        public Builder destinationTitle(final String destinationTitle) {
            this.destinationTitle = destinationTitle;
            return this;
        }
        
        public Builder destinationType(final DestinationType destinationType) {
            this.destinationType = destinationType;
            return this;
        }
        
        public Builder displayPositionLatitude(final double displayPositionLatitude) {
            this.displayPositionLatitude = displayPositionLatitude;
            return this;
        }
        
        public Builder displayPositionLongitude(final double displayPositionLongitude) {
            this.displayPositionLongitude = displayPositionLongitude;
            return this;
        }
        
        public Builder distanceStr(final String distanceStr) {
            this.distanceStr = distanceStr;
            return this;
        }
        
        public Builder favoriteDestinationType(final FavoriteDestinationType favoriteDestinationType) {
            this.favoriteDestinationType = favoriteDestinationType;
            return this;
        }
        
        public Builder fullAddress(final String fullAddress) {
            this.fullAddress = fullAddress;
            return this;
        }
        
        public Builder identifier(final String identifier) {
            this.identifier = identifier;
            return this;
        }
        
        public Builder initials(final String initials) {
            this.initials = initials;
            return this;
        }
        
        public Builder navigationPositionLatitude(final double navigationPositionLatitude) {
            this.navigationPositionLatitude = navigationPositionLatitude;
            return this;
        }
        
        public Builder navigationPositionLongitude(final double navigationPositionLongitude) {
            this.navigationPositionLongitude = navigationPositionLongitude;
            return this;
        }
        
        public Builder phoneNumbers(final List<Contact> phoneNumbers) {
            this.phoneNumbers = phoneNumbers;
            return this;
        }
        
        public Builder placeCategory(final PlaceCategory placeCategory) {
            this.placeCategory = placeCategory;
            return this;
        }
        
        public Builder placeType(final PlaceType placeType) {
            this.placeType = placeType;
            return this;
        }
        
        public Builder recentTimeLabel(final String recentTimeLabel) {
            this.recentTimeLabel = recentTimeLabel;
            return this;
        }
        
        public Builder recentTimeLabelColor(final int recentTimeLabelColor) {
            this.recentTimeLabelColor = recentTimeLabelColor;
            return this;
        }
        
        public Builder recommendation(final boolean recommendation) {
            this.recommendation = recommendation;
            return this;
        }
    }
    
    public enum DestinationType
    {
        DEFAULT, 
        FIND_GAS;
    }
    
    public enum FavoriteDestinationType
    {
        FAVORITE_CALENDAR(4), 
        FAVORITE_CONTACT(3), 
        FAVORITE_CUSTOM(10), 
        FAVORITE_HOME(1), 
        FAVORITE_NONE(0), 
        FAVORITE_WORK(2);
        
        public final int value;
        
        private FavoriteDestinationType(final int value) {
            this.value = value;
        }
        
        public static FavoriteDestinationType buildFromValue(final int n) {
            for (final FavoriteDestinationType favorite_NONE : values()) {
                if (n == favorite_NONE.value) {
                    return favorite_NONE;
                }
            }
            return FavoriteDestinationType.FAVORITE_NONE;
        }
    }
    
    public enum PlaceCategory
    {
        RECENT, 
        SUGGESTED, 
        SUGGESTED_RECENT;
    }
}
