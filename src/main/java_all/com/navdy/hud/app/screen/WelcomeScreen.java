package com.navdy.hud.app.screen;

import mortar.Presenter;
import com.navdy.service.library.events.photo.PhotoType;
import com.navdy.hud.app.framework.contacts.PhoneImageDownloader;
import android.os.Bundle;
import com.navdy.hud.app.event.DriverProfileUpdated;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.events.connection.ConnectionStatus;
import com.squareup.otto.Subscribe;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.service.library.events.ui.ShowScreen;
import com.navdy.hud.app.device.light.HUDLightUtils;
import com.navdy.hud.app.device.light.LightManager;
import android.bluetooth.BluetoothAdapter;
import com.navdy.hud.app.event.Disconnect;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.service.library.events.audio.SpeechRequest;
import java.util.Iterator;
import com.navdy.service.library.device.connection.ConnectionInfo;
import java.util.ArrayList;
import java.util.List;
import com.navdy.hud.app.framework.toast.ToastManager;
import android.text.TextUtils;
import com.navdy.hud.app.framework.contacts.ContactImageHelper;
import android.content.res.Resources;
import java.util.HashMap;
import com.navdy.hud.app.ui.component.carousel.Carousel;
import com.navdy.hud.app.framework.connection.ConnectionNotification;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.profile.DriverProfileManager;
import com.navdy.hud.app.manager.PairingManager;
import java.util.concurrent.atomic.AtomicBoolean;
import android.os.Handler;
import com.navdy.hud.app.gesture.GestureServiceConnector;
import com.navdy.service.library.device.RemoteDeviceRegistry;
import com.navdy.hud.app.service.ConnectionHandler;
import javax.inject.Inject;
import com.squareup.otto.Bus;
import javax.inject.Singleton;
import com.navdy.hud.app.ui.framework.BasePresenter;
import com.navdy.hud.app.view.WelcomeView;
import com.navdy.hud.app.ui.activity.Main;
import dagger.Module;
import com.navdy.hud.app.profile.DriverProfile;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.events.ui.Screen;
import flow.Flow;
import com.navdy.service.library.log.Logger;
import flow.Layout;

@Layout(R.layout.screen_welcome)
public class WelcomeScreen extends BaseScreen
{
    public static String ACTION_RECONNECT;
    public static String ACTION_SWITCH_PHONE;
    private static final int APP_LAUNCH_TIMEOUT = 12000;
    public static String ARG_ACTION;
    private static final int CONNECTION_TIMEOUT = 5000;
    private static final Logger logger;
    
    static {
        logger = new Logger(WelcomeScreen.class);
        WelcomeScreen.ARG_ACTION = "action";
        WelcomeScreen.ACTION_SWITCH_PHONE = "switch";
        WelcomeScreen.ACTION_RECONNECT = "reconnect";
    }
    
    @Override
    public int getAnimationIn(final Flow.Direction direction) {
        return 17432576;
    }
    
    @Override
    public int getAnimationOut(final Flow.Direction direction) {
        return 17432577;
    }
    
    @Override
    public Object getDaggerModule() {
        return new Module();
    }
    
    @Override
    public String getMortarScopeName() {
        return this.getClass().getName();
    }
    
    @Override
    public Screen getScreen() {
        return Screen.SCREEN_WELCOME;
    }
    
    public static class DeviceMetadata
    {
        public final NavdyDeviceId deviceId;
        public final DriverProfile driverProfile;
        
        public DeviceMetadata(final NavdyDeviceId deviceId, final DriverProfile driverProfile) {
            this.deviceId = deviceId;
            this.driverProfile = driverProfile;
        }
        
        @Override
        public String toString() {
            return "DeviceMetadata{deviceId=" + this.deviceId + ", driverProfile=" + this.driverProfile + '}';
        }
    }
    
    @dagger.Module(addsTo = Main.Module.class, injects = { WelcomeView.class })
    public class Module
    {
    }
    
    @Singleton
    public static class Presenter extends BasePresenter<WelcomeView>
    {
        private static final int MESSAGE_TIMEOUT = 2500;
        public static final int MISSING_DEVICES_OFFSET = 100;
        private boolean appConnected;
        private Runnable appLaunchTimeout;
        private boolean appLaunched;
        private Runnable appWaitTimeout;
        @Inject
        Bus bus;
        private boolean connected;
        private NavdyDeviceId connectingDevice;
        @Inject
        ConnectionHandler connectionHandler;
        private int currentItem;
        private Runnable deviceConnectTimeout;
        private RemoteDeviceRegistry deviceRegistry;
        private boolean dirty;
        private NavdyDeviceId failedConnection;
        private boolean firstUpdate;
        @Inject
        GestureServiceConnector gestureServiceConnector;
        private Handler handler;
        private boolean ledUpdated;
        private AtomicBoolean mGreetingPending;
        @Inject
        PairingManager pairingManager;
        @Inject
        DriverProfileManager profileManager;
        private boolean searching;
        private State state;
        private Runnable stateTimeout;
        @Inject
        UIStateManager uiStateManager;
        
        public Presenter() {
            this.deviceRegistry = RemoteDeviceRegistry.getInstance(HudApplication.getAppContext());
            this.searching = false;
            this.state = State.UNKNOWN;
            this.handler = new Handler();
            this.dirty = false;
            this.ledUpdated = false;
            this.mGreetingPending = new AtomicBoolean(false);
            this.deviceConnectTimeout = new Runnable() {
                @Override
                public void run() {
                    WelcomeScreen.logger.i("checking connection timeout - connected:" + Presenter.this.connected);
                    if (!Presenter.this.connected) {
                        Presenter.this.setState(State.CONNECTION_FAILED);
                        Presenter.this.updateView();
                    }
                }
            };
            this.appLaunchTimeout = new Runnable() {
                @Override
                public void run() {
                    WelcomeScreen.logger.v("appLaunch: " + Presenter.this.appConnected);
                    if (!Presenter.this.appConnected && Main.mProtocolStatus == Main.ProtocolStatus.PROTOCOL_VALID) {
                        WelcomeScreen.logger.v("showing app dis-connected toast");
                        ConnectionNotification.showDisconnectedToast(true);
                        Presenter.this.appLaunched = true;
                    }
                }
            };
            this.appWaitTimeout = new Runnable() {
                @Override
                public void run() {
                    if (Presenter.this.connected && !Presenter.this.appConnected) {
                        ConnectionNotification.showDisconnectedToast(true);
                    }
                }
            };
            this.stateTimeout = new Runnable() {
                @Override
                public void run() {
                    final WelcomeView welcomeView = (WelcomeView)mortar.Presenter.this.getView();
                    WelcomeScreen.logger.i("State timeout fired - current state:" + Presenter.this.state + " current view:" + welcomeView);
                    if (welcomeView != null) {
                        switch (Presenter.this.state) {
                            case CONNECTION_FAILED:
                                Presenter.this.setState(State.PICKING);
                                Presenter.this.updateView();
                                break;
                            case WELCOME:
                                WelcomeScreen.logger.i("Welcome timed out - going back");
                                Presenter.this.connectingDevice = null;
                                Presenter.this.finish();
                                break;
                        }
                    }
                }
            };
        }
        
        private Carousel.Model buildAddDriverModel(final boolean b) {
            final Resources resources = this.getView().getResources();
            int n;
            if (b) {
                n = R.string.welcome_who_is_driving;
            }
            else {
                n = R.string.welcome_no_drivers_found;
            }
            final Carousel.Model model = new Carousel.Model();
            model.id = R.id.welcome_menu_add_driver;
            model.smallImageRes = R.drawable.icon_add_driver;
            model.largeImageRes = R.drawable.icon_add_driver;
            (model.infoMap = new HashMap<Integer, String>(1)).put(R.id.title, resources.getString(n));
            model.infoMap.put(R.id.subTitle, resources.getString(R.string.welcome_add_driver));
            return model;
        }
        
        private Carousel.Model buildDriverModel(final NavdyDeviceId navdyDeviceId, final int n) {
            return this.buildDriverModel(navdyDeviceId, n, 0, 0);
        }
        
        private Carousel.Model buildDriverModel(final NavdyDeviceId navdyDeviceId, final int n, final int n2) {
            return this.buildDriverModel(navdyDeviceId, n, n2, 0);
        }
        
        private Carousel.Model buildDriverModel(final NavdyDeviceId navdyDeviceId, int driverImageResId, final int n, final int n2) {
            final Resources resources = this.getView().getResources();
            final Carousel.Model model = new Carousel.Model();
            model.id = driverImageResId;
            final String deviceName = navdyDeviceId.getDeviceName();
            driverImageResId = ContactImageHelper.getInstance().getDriverImageResId(deviceName);
            model.smallImageRes = driverImageResId;
            model.largeImageRes = driverImageResId;
            final DeviceMetadata extras = new DeviceMetadata(navdyDeviceId, this.profileManager.getProfileForId(navdyDeviceId));
            model.extras = extras;
            (model.infoMap = new HashMap<Integer, String>(4)).put(R.id.title, resources.getString(n));
            String driverName;
            if (extras.driverProfile != null && !TextUtils.isEmpty((CharSequence)extras.driverProfile.getDriverName())) {
                driverName = extras.driverProfile.getDriverName();
            }
            else {
                driverName = deviceName;
            }
            model.infoMap.put(R.id.subTitle, driverName);
            if (n2 != 0) {
                model.infoMap.put(R.id.message, resources.getString(n2));
            }
            return model;
        }
        
        private Carousel.Model buildSearchingModel() {
            final Resources resources = this.getView().getResources();
            final Carousel.Model model = new Carousel.Model();
            model.id = R.id.welcome_menu_searching;
            model.smallImageRes = R.drawable.icon_bluetooth_connecting;
            model.largeImageRes = R.drawable.icon_bluetooth_connecting;
            (model.infoMap = new HashMap<Integer, String>(1)).put(R.id.title, resources.getString(R.string.welcome_looking_for_drivers));
            model.infoMap.put(R.id.subTitle, "");
            return model;
        }
        
        private void clearCallbacks() {
            this.handler.removeCallbacks(this.deviceConnectTimeout);
            this.handler.removeCallbacks(this.appLaunchTimeout);
            this.handler.removeCallbacks(this.appWaitTimeout);
        }
        
        private void clearLaunchNotification() {
            this.appLaunched = false;
            this.clearCallbacks();
            final ToastManager instance = ToastManager.getInstance();
            instance.dismissCurrentToast("disconnection#toast");
            instance.dismissCurrentToast("connection#toast");
        }
        
        private boolean hasPaired() {
            return this.deviceRegistry.hasPaired();
        }
        
        private List<NavdyDeviceId> knownDevices() {
            final List<ConnectionInfo> pairedConnections = RemoteDeviceRegistry.getInstance(HudApplication.getAppContext()).getPairedConnections();
            final ArrayList<NavdyDeviceId> list = new ArrayList<NavdyDeviceId>();
            final Iterator<ConnectionInfo> iterator = pairedConnections.iterator();
            while (iterator.hasNext()) {
                list.add(iterator.next().getDeviceId());
            }
            return list;
        }
        
        private void sayWelcome(final DriverProfile driverProfile) {
            if (this.mGreetingPending.compareAndSet(true, false)) {
                TTSUtils.sendSpeechRequest(HudApplication.getAppContext().getResources().getString(R.string.welcome_welcome_driver, new Object[] { driverProfile.getFirstName() }), SpeechRequest.Category.SPEECH_WELCOME_MESSAGE, null);
            }
        }
        
        private void selectDevice(final NavdyDeviceId device) {
            WelcomeScreen.logger.i("Trying to select " + device);
            final RemoteDevice remoteDevice = this.connectionHandler.getRemoteDevice();
            if (remoteDevice != null && remoteDevice.getDeviceId().equals(device)) {
                this.setState(State.WELCOME);
            }
            else {
                this.setState(State.CONNECTING);
                this.handler.post((Runnable)new Runnable() {
                    @Override
                    public void run() {
                        Presenter.this.connectionHandler.connectToDevice(device);
                    }
                });
            }
            this.setDevice(device);
            this.updateView();
        }
        
        private void setDevice(final NavdyDeviceId connectingDevice) {
            this.connectingDevice = connectingDevice;
            this.failedConnection = null;
            this.dirty = true;
            this.currentItem = 0;
        }
        
        private void setSearching(final boolean searching) {
            if (this.searching != searching) {
                this.searching = searching;
                this.dirty = true;
            }
        }
        
        private void setState(final State state) {
            if (this.state != state) {
                this.state = state;
                this.pairingManager.setAutoPairing(false);
                this.clearCallbacks();
                this.dirty = true;
                WelcomeScreen.logger.i("switching to state:" + state);
                switch (state) {
                    case CONNECTION_FAILED:
                        this.handler.removeCallbacks(this.stateTimeout);
                        this.handler.postDelayed(this.stateTimeout, 2500L);
                        break;
                    case WELCOME:
                        this.mGreetingPending.set(true);
                        this.handler.removeCallbacks(this.stateTimeout);
                        this.handler.postDelayed(this.stateTimeout, 2500L);
                        break;
                    case DOWNLOAD_APP:
                        this.connectionHandler.stopSearch();
                        this.bus.post(new Disconnect());
                        this.pairingManager.setAutoPairing(true);
                        break;
                    case CONNECTING:
                        this.handler.postDelayed(this.deviceConnectTimeout, 5000L);
                        break;
                    case LAUNCHING_APP:
                        this.handler.postDelayed(this.appLaunchTimeout, 12000L);
                        break;
                    case SEARCHING:
                    case PICKING:
                        if (this.connectingDevice != null) {
                            this.failedConnection = this.connectingDevice;
                            this.connectingDevice = null;
                            break;
                        }
                        break;
                }
                this.updateViewState();
            }
        }
        
        private void startSearchIfNeeded() {
            if (this.state == State.SEARCHING && this.connectionHandler.serviceConnected() && BluetoothAdapter.getDefaultAdapter().isEnabled()) {
                this.connectionHandler.searchForDevices();
            }
        }
        
        private void updatePairingStatusOnLED(final boolean b) {
            if (!this.ledUpdated && b) {
                HUDLightUtils.showPairing(HudApplication.getAppContext(), LightManager.getInstance(), true);
                this.ledUpdated = true;
            }
            else if (this.ledUpdated) {
                HUDLightUtils.showPairing(HudApplication.getAppContext(), LightManager.getInstance(), false);
                this.ledUpdated = false;
            }
        }
        
        private void updateViewState() {
            final WelcomeView welcomeView = this.getView();
            if (welcomeView != null) {
                int state;
                final int n = state = -1;
                Label_0070: {
                    switch (this.state) {
                        default:
                            state = n;
                            break Label_0070;
                        case WELCOME:
                        case CONNECTING:
                        case LAUNCHING_APP:
                        case SEARCHING:
                            state = 3;
                            break Label_0070;
                        case PICKING:
                            state = 2;
                            break Label_0070;
                        case DOWNLOAD_APP:
                            state = 1;
                        case CONNECTION_FAILED:
                            if (state != -1) {
                                welcomeView.setState(state);
                                break;
                            }
                            break;
                    }
                }
            }
        }
        
        public void cancel() {
            this.connectionHandler.stopSearch();
            this.finish();
        }
        
        public void executeItem(final int n, final int n2) {
            WelcomeScreen.logger.i("execute: id:" + n + " pos:" + n2);
            if (n != R.id.welcome_menu_searching && n != R.id.welcome_menu_connecting) {
                if (n == R.id.welcome_menu_add_driver) {
                    this.setState(State.DOWNLOAD_APP);
                }
                else {
                    final WelcomeView welcomeView = this.getView();
                    if (welcomeView != null) {
                        final Carousel.Model model = welcomeView.carousel.getModel(n2);
                        try {
                            this.selectDevice(((DeviceMetadata)model.extras).deviceId);
                        }
                        catch (Exception ex) {
                            WelcomeScreen.logger.e("Failed to select device at pos " + n2);
                        }
                    }
                }
            }
        }
        
        public void finish() {
            final BaseScreen currentScreen = this.uiStateManager.getCurrentScreen();
            if (currentScreen != null && currentScreen.getScreen() != Screen.SCREEN_WELCOME) {
                WelcomeScreen.logger.v("finish: not switching: " + currentScreen.getScreen());
            }
            else {
                WelcomeScreen.logger.v("finish: switching: " + currentScreen.getScreen());
                this.bus.post(new ShowScreen.Builder().screen(this.uiStateManager.getDefaultMainActiveScreen()).build());
            }
        }
        
        @Subscribe
        public void onConnectionStateChange(final ConnectionStateChange connectionStateChange) {
            NavdyDeviceId navdyDeviceId = null;
            while (true) {
                try {
                    navdyDeviceId = new NavdyDeviceId(connectionStateChange.remoteDeviceId);
                    WelcomeScreen.logger.v("connection state change:" + connectionStateChange.state + " for: " + navdyDeviceId);
                    switch (connectionStateChange.state) {
                        default:
                            return;
                        case CONNECTION_LINK_LOST:
                            this.connected = false;
                            if (navdyDeviceId == null) {
                                this.startSearchIfNeeded();
                                break;
                            }
                            if (navdyDeviceId.equals(this.connectingDevice)) {
                                this.clearLaunchNotification();
                                this.setState(State.PICKING);
                                break;
                            }
                            break;
                        case CONNECTION_CONNECTED:
                            this.connectingDevice = navdyDeviceId;
                            break;
                        case CONNECTION_VERIFIED:
                            this.appConnected = true;
                            this.connectingDevice = navdyDeviceId;
                            this.setState(State.WELCOME);
                            break;
                        case CONNECTION_DISCONNECTED:
                            if (navdyDeviceId != null && this.connectingDevice != null && this.connectingDevice.equals(navdyDeviceId)) {
                                this.setState(State.PICKING);
                                break;
                            }
                            break;
                        case CONNECTION_LINK_ESTABLISHED:
                            this.connectingDevice = navdyDeviceId;
                            this.connected = true;
                            this.updatePairingStatusOnLED(this.appConnected = false);
                            this.setDevice(navdyDeviceId);
                            this.setState(State.LAUNCHING_APP);
                            break;
                    }
                    this.updateView();
                }
                catch (Exception ex) {
                    continue;
                }
                break;
            }
        }
        
        @Subscribe
        public void onConnectionStatus(final ConnectionStatus connectionStatus) {
            NavdyDeviceId navdyDeviceId = null;
            while (true) {
                try {
                    navdyDeviceId = new NavdyDeviceId(connectionStatus.remoteDeviceId);
                    WelcomeScreen.logger.i("ConnectionStatus: " + connectionStatus);
                    while (true) {
                        switch (connectionStatus.status) {
                            case CONNECTION_SEARCH_STARTED:
                                this.setSearching(true);
                                this.updatePairingStatusOnLED(true);
                            case CONNECTION_LOST:
                                this.updateView();
                                break;
                            case CONNECTION_SEARCH_FINISHED:
                                this.setSearching(false);
                                this.updatePairingStatusOnLED(false);
                                if (this.state == State.SEARCHING) {
                                    this.connectionHandler.stopSearch();
                                    this.finish();
                                }
                                continue;
                            case CONNECTION_FOUND:
                                if (this.state == State.SEARCHING) {
                                    this.selectDevice(navdyDeviceId);
                                }
                                continue;
                            case CONNECTION_PAIRED_DEVICES_CHANGED:
                                TaskManager.getInstance().execute(new Runnable() {
                                    @Override
                                    public void run() {
                                        Presenter.this.deviceRegistry.refresh();
                                    }
                                }, 1);
                                break;
                        }
                        break;
                    }
                }
                catch (Exception ex) {
                    continue;
                }
                break;
            }
        }
        
        public void onCurrentItemChanged(final int currentItem, final int n) {
            WelcomeScreen.logger.i("onCurrentItemChanged id:" + n + " pos:" + currentItem);
            this.currentItem = currentItem;
        }
        
        @Subscribe
        public void onDisconnect(final Disconnect disconnect) {
            this.appLaunched = false;
            this.clearCallbacks();
            if (this.state != State.DOWNLOAD_APP) {
                this.finish();
            }
        }
        
        @Subscribe
        public void onDismissToast(final ToastManager.DismissedToast dismissedToast) {
            if (this.appLaunched && TextUtils.equals((CharSequence)"disconnection#toast", (CharSequence)dismissedToast.name)) {
                WelcomeScreen.logger.v("launching app timeout");
                this.handler.removeCallbacks(this.appLaunchTimeout);
                this.handler.removeCallbacks(this.appWaitTimeout);
                this.handler.postDelayed(this.appWaitTimeout, 5000L);
            }
        }
        
        @Subscribe
        public void onDriverProfileUpdated(final DriverProfileUpdated driverProfileUpdated) {
            final DriverProfile currentProfile = this.profileManager.getCurrentProfile();
            if (this.state == State.WELCOME) {
                WelcomeScreen.logger.v("sayWelcome profile[" + currentProfile.getProfileName() + "] FN[" + currentProfile.getFirstName() + "]");
                this.updateView();
                this.sayWelcome(currentProfile);
                if (driverProfileUpdated.state == DriverProfileUpdated.State.UPDATED) {
                    this.handler.removeCallbacks(this.stateTimeout);
                    this.handler.postDelayed(this.stateTimeout, 2500L);
                }
            }
        }
        
        @Override
        public void onLoad(final Bundle bundle) {
            super.onLoad(bundle);
            this.uiStateManager.enableNotificationColor(false);
            String string = null;
            if (bundle != null) {
                string = bundle.getString(WelcomeScreen.ARG_ACTION);
            }
            WelcomeScreen.logger.i("onLoad - action:" + string);
            this.firstUpdate = true;
            this.bus.register(this);
            this.currentItem = 0;
            this.dirty = true;
            if (WelcomeScreen.ACTION_SWITCH_PHONE.equals(string)) {
                this.connectingDevice = null;
                if (this.hasPaired()) {
                    this.setState(State.PICKING);
                }
                else {
                    this.setState(State.DOWNLOAD_APP);
                }
            }
            else if (WelcomeScreen.ACTION_RECONNECT.equals(string) || this.state == State.UNKNOWN) {
                if (this.hasPaired()) {
                    this.setSearching(true);
                    this.setState(State.SEARCHING);
                    this.startSearchIfNeeded();
                }
                else {
                    this.setState(State.DOWNLOAD_APP);
                }
            }
            else {
                switch (this.state) {
                    case WELCOME:
                    case CONNECTING:
                    case LAUNCHING_APP:
                    case CONNECTION_FAILED:
                        if (this.connectingDevice == null) {
                            this.finish();
                            return;
                        }
                        this.handler.removeCallbacks(this.stateTimeout);
                        this.handler.postDelayed(this.stateTimeout, 2500L);
                        break;
                    case SEARCHING:
                        this.startSearchIfNeeded();
                        break;
                }
            }
            this.updateView();
        }
        
        @Subscribe
        public void onPhotoDownload(final PhoneImageDownloader.PhotoDownloadStatus photoDownloadStatus) {
            if (photoDownloadStatus.photoType == PhotoType.PHOTO_DRIVER_PROFILE && !photoDownloadStatus.alreadyDownloaded) {
                final WelcomeView welcomeView = this.getView();
                if (welcomeView != null) {
                    welcomeView.carousel.reload();
                }
            }
        }
        
        @Override
        protected void onUnload() {
            this.updatePairingStatusOnLED(false);
            this.uiStateManager.enableNotificationColor(true);
        }
        
        protected void updateView() {
            int currentItem = 1;
            final WelcomeView welcomeView = this.getView();
            if (welcomeView != null) {
                this.updateViewState();
                if (this.dirty) {
                    if (this.firstUpdate) {
                        this.firstUpdate = false;
                        this.handler.postDelayed((Runnable)new Runnable() {
                            @Override
                            public void run() {
                                final WelcomeView welcomeView = (WelcomeView)mortar.Presenter.this.getView();
                                if (welcomeView != null) {
                                    welcomeView.setSearching(Presenter.this.searching);
                                }
                            }
                        }, 1000L);
                    }
                    else {
                        welcomeView.setSearching(this.searching);
                    }
                    this.dirty = false;
                    final ArrayList<Carousel.Model> list = new ArrayList<Carousel.Model>();
                    switch (this.state) {
                        case CONNECTING:
                        case LAUNCHING_APP:
                            list.add(this.buildDriverModel(this.connectingDevice, R.id.welcome_menu_connecting, R.string.welcome_connecting));
                            break;
                        case CONNECTION_FAILED:
                            list.add(this.buildDriverModel(this.connectingDevice, R.id.welcome_menu_connecting, R.string.welcome_cant_connect, R.string.welcome_bluetooth_disabled));
                            break;
                        case WELCOME:
                            list.add(this.buildDriverModel(this.connectingDevice, R.id.welcome_menu_connecting, R.string.welcome_welcome));
                            break;
                        case SEARCHING:
                            list.add(this.buildSearchingModel());
                            break;
                        case PICKING: {
                            list.add(this.buildAddDriverModel(true));
                            int currentItem2 = 1;
                            final List<NavdyDeviceId> knownDevices = this.knownDevices();
                            final NavdyDeviceId connectedDevice = this.connectionHandler.getConnectedDevice();
                            if (knownDevices.size() <= 0) {
                                currentItem = 0;
                            }
                            this.currentItem = currentItem;
                            for (final NavdyDeviceId navdyDeviceId : knownDevices) {
                                int n = R.string.welcome_who_is_driving;
                                final int n2 = 0;
                                int n3;
                                if (navdyDeviceId.equals(this.failedConnection)) {
                                    this.currentItem = currentItem2;
                                    n = R.string.welcome_cant_connect;
                                    n3 = R.string.welcome_bluetooth_disabled;
                                }
                                else {
                                    n3 = n2;
                                    if (navdyDeviceId.equals(connectedDevice)) {
                                        n = R.string.welcome_current_driver;
                                        n3 = n2;
                                    }
                                }
                                list.add(this.buildDriverModel(navdyDeviceId, currentItem2, n, n3));
                                ++currentItem2;
                            }
                            break;
                        }
                    }
                    welcomeView.carousel.setModel(list, this.currentItem, false);
                }
            }
        }
    }
    
    enum State
    {
        CONNECTING, 
        CONNECTION_FAILED, 
        DOWNLOAD_APP, 
        LAUNCHING_APP, 
        PICKING, 
        SEARCHING, 
        UNKNOWN, 
        WELCOME;
    }
}
