package com.navdy.hud.app.screen;

import com.navdy.hud.app.event.Shutdown;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import android.os.Bundle;
import android.content.Intent;
import com.navdy.hud.app.util.OTAUpdateService;
import com.navdy.hud.app.HudApplication;
import com.navdy.service.library.events.ui.ShowScreen;
import com.navdy.hud.app.ui.framework.UIStateManager;
import android.content.SharedPreferences;
import com.squareup.otto.Bus;
import javax.inject.Inject;
import com.navdy.hud.app.gesture.GestureServiceConnector;
import com.navdy.hud.app.ui.framework.BasePresenter;
import dagger.Provides;
import com.navdy.hud.app.view.ForceUpdateView;
import com.navdy.hud.app.ui.activity.Main;
import dagger.Module;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.log.Logger;
import flow.Layout;

@Layout(R.layout.force_update_lyt)
public class ForceUpdateScreen extends BaseScreen
{
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(ForceUpdateScreen.class);
    }
    
    @Override
    public Object getDaggerModule() {
        return new Module();
    }
    
    @Override
    public String getMortarScopeName() {
        return ForceUpdateScreen.class.getName();
    }
    
    @Override
    public Screen getScreen() {
        return Screen.SCREEN_FORCE_UPDATE;
    }
    
    @dagger.Module(addsTo = Main.Module.class, injects = { ForceUpdateView.class })
    public class Module
    {
        @Provides
        ForceUpdateScreen provideScreen() {
            return ForceUpdateScreen.this;
        }
    }
    
    public static class Presenter extends BasePresenter<ForceUpdateView>
    {
        @Inject
        GestureServiceConnector gestureServiceConnector;
        @Inject
        Bus mBus;
        @Inject
        SharedPreferences mPreferences;
        @Inject
        ForceUpdateScreen mScreen;
        @Inject
        UIStateManager uiStateManager;
        
        public void dismiss() {
            this.mBus.post(new ShowScreen.Builder().screen(this.uiStateManager.getDefaultMainActiveScreen()).build());
        }
        
        public Bus getBus() {
            return this.mBus;
        }
        
        public void install() {
            final Intent intent = new Intent(HudApplication.getAppContext(), (Class)OTAUpdateService.class);
            intent.putExtra("COMMAND", "INSTALL_UPDATE");
            HudApplication.getAppContext().startService(intent);
        }
        
        public boolean isSoftwareUpdatePending() {
            return OTAUpdateService.isUpdateAvailable();
        }
        
        @Override
        public void onLoad(final Bundle bundle) {
            super.onLoad(bundle);
            this.uiStateManager.enableSystemTray(false);
            this.uiStateManager.enableNotificationColor(false);
            NotificationManager.getInstance().enableNotifications(false);
        }
        
        @Override
        protected void onUnload() {
            this.uiStateManager.enableSystemTray(true);
            this.uiStateManager.enableNotificationColor(true);
            NotificationManager.getInstance().enableNotifications(true);
            super.onUnload();
        }
        
        public void shutDown() {
            this.mBus.post(new Shutdown(Shutdown.Reason.FORCED_UPDATE));
        }
    }
}
