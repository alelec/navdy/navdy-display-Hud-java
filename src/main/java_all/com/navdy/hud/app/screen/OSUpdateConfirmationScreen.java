package com.navdy.hud.app.screen;

import android.os.Bundle;
import com.navdy.service.library.events.ui.ShowScreen;
import android.content.Intent;
import com.navdy.hud.app.util.OTAUpdateService;
import com.navdy.hud.app.HudApplication;
import android.content.SharedPreferences;
import javax.inject.Inject;
import com.squareup.otto.Bus;
import javax.inject.Singleton;
import com.navdy.hud.app.ui.framework.BasePresenter;
import com.navdy.hud.app.view.UpdateConfirmationView;
import com.navdy.hud.app.ui.activity.Main;
import dagger.Module;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.log.Logger;
import flow.Layout;

@Layout(R.layout.screen_update_confirmation)
public class OSUpdateConfirmationScreen extends BaseScreen
{
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(OSUpdateConfirmationScreen.class);
    }
    
    @Override
    public Object getDaggerModule() {
        return new Module();
    }
    
    @Override
    public String getMortarScopeName() {
        return OSUpdateConfirmationScreen.class.getName();
    }
    
    @Override
    public Screen getScreen() {
        return Screen.SCREEN_OTA_CONFIRMATION;
    }
    
    @dagger.Module(addsTo = Main.Module.class, injects = { UpdateConfirmationView.class })
    public class Module
    {
    }
    
    @Singleton
    public static class Presenter extends BasePresenter<UpdateConfirmationView>
    {
        private boolean isReminder;
        @Inject
        Bus mBus;
        @Inject
        SharedPreferences mPreferences;
        
        public void doNotPrompt() {
            final Intent intent = new Intent(HudApplication.getAppContext(), (Class)OTAUpdateService.class);
            intent.putExtra("COMMAND", "DO_NOT_PROMPT");
            HudApplication.getAppContext().startService(intent);
        }
        
        public void finish() {
            this.mBus.post(new ShowScreen.Builder().screen(Screen.SCREEN_BACK).build());
        }
        
        public String getCurrentVersion() {
            return OTAUpdateService.getCurrentVersion();
        }
        
        public String getUpdateVersion() {
            return OTAUpdateService.getSWUpdateVersion();
        }
        
        public void install() {
            final Intent intent = new Intent(HudApplication.getAppContext(), (Class)OTAUpdateService.class);
            intent.putExtra("COMMAND", "INSTALL_UPDATE");
            HudApplication.getAppContext().startService(intent);
        }
        
        public boolean isReminder() {
            return this.isReminder;
        }
        
        @Override
        public void onLoad(final Bundle bundle) {
            super.onLoad(bundle);
            if (bundle != null) {
                this.isReminder = bundle.getBoolean("UPDATE_REMINDER", false);
            }
            else {
                OSUpdateConfirmationScreen.sLogger.e("null bundle passed to onLoad()");
                this.isReminder = false;
            }
        }
    }
}
