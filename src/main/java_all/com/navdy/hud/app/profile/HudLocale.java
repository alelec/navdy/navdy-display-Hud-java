package com.navdy.hud.app.profile;

import android.annotation.TargetApi;
import android.content.res.Configuration;
import com.navdy.hud.app.util.GenericUtil;
import android.os.Handler;
import android.os.Looper;
import com.navdy.hud.app.ui.component.ConfirmationLayout;
import android.animation.Animator.AnimatorListener;
import android.animation.Animator;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import android.animation.TimeInterpolator;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.View;
import android.widget.FrameLayout.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import com.navdy.hud.app.view.ToastView;
import com.navdy.hud.app.manager.InputManager;
import android.content.res.Resources;
import android.animation.ObjectAnimator;
import com.navdy.hud.app.framework.toast.IToastCallback;
import android.os.Bundle;
import android.os.Build;
import android.content.SharedPreferences;
import java.util.Iterator;
import android.os.Process;
import android.app.ActivityManager;
import com.navdy.hud.app.HudApplication;
import android.app.ActivityManager;
import android.text.TextUtils;
import android.util.Log;
import android.preference.PreferenceManager;
import java.util.Locale;
import android.content.Context;
import com.navdy.hud.app.framework.toast.ToastManager;
import java.util.HashSet;

public class HudLocale
{
    public static final String DEFAULT_LANGUAGE = "en";
    public static final String DEFAULT_LOCALE_ID = "en_US";
    private static final String LOCALE_SEPARATOR = "_";
    private static final String LOCALE_TOAST_ID = "#locale#toast";
    private static String MAP_ENGINE_PROCESS_NAME;
    private static final String SELECTED_LOCALE = "Locale.Helper.Selected.Language";
    private static final String TAG = "[HUD-locale]";
    private static final HashSet<String> hudLanguages;
    
    static {
        (hudLanguages = new HashSet<String>()).add("en");
        HudLocale.hudLanguages.add("fr");
        HudLocale.hudLanguages.add("de");
        HudLocale.hudLanguages.add("it");
        HudLocale.hudLanguages.add("es");
        HudLocale.MAP_ENGINE_PROCESS_NAME = "global.Here.Map.Service.v2";
    }
    
    public static void dismissToast() {
        ToastManager.getInstance().dismissToast("#locale#toast");
    }
    
    public static String getBaseLanguage(final String s) {
        int i;
        for (i = 0; i < s.length(); ++i) {
            final char char1 = s.charAt(i);
            if (char1 == '_' || char1 == '-') {
                break;
            }
        }
        String substring = s;
        if (i > 0) {
            substring = s;
            if (i < s.length()) {
                substring = s.substring(0, i);
            }
        }
        return substring;
    }
    
    public static Locale getCurrentLocale(final Context context) {
        try {
            String string;
            if ((string = PreferenceManager.getDefaultSharedPreferences(context).getString("Locale.Helper.Selected.Language", "en_US")).equals("en")) {
                string = "en_US";
            }
            return getLocaleForID(string);
        }
        catch (Throwable t) {
            Log.e("[HUD-locale]", "getCurrentLocale", t);
            return getLocaleForID("en_US");
        }
    }
    
    public static Locale getLocaleForID(String substring) {
        Locale locale;
        if (TextUtils.isEmpty((CharSequence)substring)) {
            locale = null;
        }
        else {
            final int index = substring.indexOf("_");
            if (index >= 0) {
                final String substring2 = substring.substring(0, index);
                substring = substring.substring(index + 1);
                final int index2 = substring.indexOf("_");
                if (index2 > 0) {
                    locale = new Locale(substring2, substring.substring(0, index2), substring.substring(index2 + 1));
                }
                else {
                    locale = new Locale(substring2, substring);
                }
            }
            else {
                locale = new Locale(substring);
            }
        }
        return locale;
    }
    
    public static boolean isLocaleSupported(final Locale locale) {
        return locale != null && HudLocale.hudLanguages.contains(getBaseLanguage(locale.getLanguage()));
    }
    
    private static void killSelfAndMapEngine() {
        for (final ActivityManager$RunningAppProcessInfo activityManager$RunningAppProcessInfo : ((ActivityManager)HudApplication.getAppContext().getSystemService("activity")).getRunningAppProcesses()) {
            if (activityManager$RunningAppProcessInfo.processName.equals(HudLocale.MAP_ENGINE_PROCESS_NAME)) {
                Process.killProcess(activityManager$RunningAppProcessInfo.pid);
                break;
            }
        }
        System.exit(0);
    }
    
    public static Context onAttach(final Context context) {
        return setLocale(context, getCurrentLocale(context));
    }
    
    private static void setCurrentLocale(final Context context, final Locale locale) {
        final SharedPreferences$Editor edit = PreferenceManager.getDefaultSharedPreferences(context).edit();
        edit.putString("Locale.Helper.Selected.Language", locale.toString());
        edit.commit();
    }
    
    private static Context setLocale(Context context, final Locale locale) {
        Log.e("[HUD-locale]", "setLocale [" + locale + "]");
        if (Build$VERSION.SDK_INT >= 24) {
            context = updateResources(context, locale);
        }
        else {
            context = updateResourcesLegacy(context, locale);
        }
        return context;
    }
    
    public static void showLocaleChangeToast() {
        final ToastManager instance = ToastManager.getInstance();
        final Resources resources = HudApplication.getAppContext().getResources();
        final Bundle bundle = new Bundle();
        instance.clearAllPendingToast();
        instance.disableToasts(false);
        instance.dismissCurrentToast("#locale#toast");
        bundle.putInt("8", R.drawable.icon_sm_spinner);
        bundle.putString("4", resources.getString(R.string.updating_language));
        bundle.putInt("5", R.style.Glances_1);
        instance.addToast(new ToastManager.ToastParams("#locale#toast", bundle, new IToastCallback() {
            ObjectAnimator animator;
            
            @Override
            public void executeChoiceItem(final int n, final int n2) {
            }
            
            @Override
            public boolean onKey(final InputManager.CustomKeyEvent customKeyEvent) {
                return false;
            }
            
            @Override
            public void onStart(final ToastView toastView) {
                final ConfirmationLayout view = toastView.getView();
                final ViewGroup.MarginLayoutParams ViewGroup.MarginLayoutParams = (ViewGroup.MarginLayoutParams)view.screenImage.getLayoutParams();
                ViewGroup.MarginLayoutParams.width = resources.getDimensionPixelSize(R.dimen.locale_change_icon_size);
                ViewGroup.MarginLayoutParams.height = ViewGroup.MarginLayoutParams.width;
                view.title1.setVisibility(GONE);
                view.title3.setVisibility(GONE);
                view.title4.setVisibility(GONE);
                ((FrameLayout.LayoutParams)view.screenImage.getLayoutParams()).gravity = 17;
                view.findViewById(R.id.infoContainer).setPadding(0, 0, 0, 0);
                if (this.animator == null) {
                    (this.animator = ObjectAnimator.ofFloat(view.screenImage, View.ROTATION, new float[] { 360.0f })).setDuration(500L);
                    this.animator.setInterpolator((TimeInterpolator)new AccelerateDecelerateInterpolator());
                    this.animator.addListener((Animator.AnimatorListener)new DefaultAnimationListener() {
                        @Override
                        public void onAnimationEnd(final Animator animator) {
                            if (IToastCallback.this.animator != null) {
                                IToastCallback.this.animator.setStartDelay(33L);
                                IToastCallback.this.animator.start();
                            }
                        }
                    });
                }
                if (!this.animator.isRunning()) {
                    this.animator.start();
                }
            }
            
            @Override
            public void onStop() {
                if (this.animator != null) {
                    this.animator.removeAllListeners();
                    this.animator.cancel();
                }
            }
        }, true, true));
        new Handler(Looper.getMainLooper()).postDelayed((Runnable)new Runnable() {
            @Override
            public void run() {
                killSelfAndMapEngine();
            }
        }, 2000L);
    }
    
    public static void showLocaleNotSupportedToast(final String s) {
        dismissToast();
        final Resources resources = HudApplication.getAppContext().getResources();
        final Bundle bundle = new Bundle();
        bundle.putBoolean("12", true);
        bundle.putInt("8", R.drawable.icon_not_supported);
        bundle.putString("4", resources.getString(R.string.locale_not_supported, new Object[] { s }));
        bundle.putInt("5", R.style.Glances_1);
        ToastManager.getInstance().addToast(new ToastManager.ToastParams("#locale#toast", bundle, null, true, true));
    }
    
    public static boolean switchLocale(final Context context, final Locale locale) {
        while (true) {
            boolean b = false;
            Locale currentLocale = null;
            Label_0100: {
                try {
                    GenericUtil.checkNotOnMainThread();
                    if (!isLocaleSupported(locale)) {
                        Log.e("[HUD-locale]", "switchLocale hud does not support locale:" + locale);
                    }
                    else {
                        currentLocale = getCurrentLocale(context);
                        if (!locale.equals(currentLocale)) {
                            break Label_0100;
                        }
                        Log.e("[HUD-locale]", "switchLocale language not changed:" + locale);
                    }
                    return b;
                }
                catch (Throwable t) {
                    Log.e("[HUD-locale]", "switchLocale", t);
                    return b;
                }
                return b;
            }
            setCurrentLocale(context, locale);
            Log.e("[HUD-locale]", "switchLocale changed language from [" + currentLocale + "] to [" + locale + "], restarting hud app");
            b = true;
            return b;
        }
    }
    
    @TargetApi(24)
    private static Context updateResources(final Context context, final Locale locale) {
        Locale.setDefault(locale);
        final Configuration configuration = context.getResources().getConfiguration();
        configuration.setLocale(locale);
        return context.createConfigurationContext(configuration);
    }
    
    private static Context updateResourcesLegacy(final Context context, final Locale locale) {
        Locale.setDefault(locale);
        final Resources resources = context.getResources();
        final Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        return context;
    }
}
