package com.navdy.hud.app.ui.component.vlist;

import com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.ScrollableViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.LoadingViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconOneViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.TitleSubtitleViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.TitleViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.BlankViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State;

import android.view.ViewGroup;
import android.view.View;
import android.os.Looper;
import java.util.HashSet;
import java.util.HashMap;
import android.os.Handler;
import java.util.List;
import com.navdy.service.library.log.Logger;
import android.support.v7.widget.RecyclerView;


public class VerticalAdapter extends RecyclerView.Adapter<VerticalViewHolder>
{
    private static final Logger sLogger;
    private List<VerticalList.Model> data;
    private Handler handler;
    private int highlightIndex;
    private HashMap<VerticalViewHolder, Integer> holderToPosMap;
    private boolean initialState;
    private VerticalList.ModelState modelState;
    private HashMap<Integer, VerticalViewHolder> posToHolderMap;
    private HashSet<Integer> viewHolderCache;
    private VerticalList vlist;
    
    static {
        sLogger = new Logger(VerticalAdapter.class);
    }
    
    public VerticalAdapter(final List<VerticalList.Model> data, final VerticalList vlist) {
        this.handler = new Handler(Looper.getMainLooper());
        this.modelState = new VerticalList.ModelState();
        this.highlightIndex = -1;
        this.viewHolderCache = new HashSet<Integer>();
        this.holderToPosMap = new HashMap<VerticalViewHolder, Integer>();
        this.posToHolderMap = new HashMap<Integer, VerticalViewHolder>();
        this.data = data;
        this.vlist = vlist;
    }
    
    private void clearPrevPos(final VerticalViewHolder verticalViewHolder) {
        final Integer n = this.holderToPosMap.remove(verticalViewHolder);
        if (n != null) {
            VerticalAdapter.sLogger.v("clearPrevPos removed:" + n);
            this.posToHolderMap.remove(n);
        }
    }
    
    public VerticalViewHolder getHolderForPos(final int n) {
        return this.posToHolderMap.get(n);
    }
    
    @Override
    public int getItemCount() {
        return this.data.size();
    }
    
    @Override
    public long getItemId(final int n) {
        return n;
    }
    
    @Override
    public int getItemViewType(final int n) {
        return this.data.get(n).type.ordinal();
    }
    
    public VerticalList.Model getModel(final int n) {
        VerticalList.Model model;
        if (n < 0 || n >= this.data.size()) {
            model = null;
        }
        else {
            model = this.data.get(n);
        }
        return model;
    }
    
    public void onBindViewHolder(final VerticalViewHolder verticalViewHolder, final int pos) {
        if (VerticalAdapter.sLogger.isLoggable(2)) {
            VerticalAdapter.sLogger.v("onBindViewHolder: {" + pos + "}");
        }
        boolean b = false;
        final boolean b2 = false;
        this.clearPrevPos(verticalViewHolder);
        if (this.highlightIndex != -1) {
            b = b2;
            if (this.highlightIndex == pos) {
                b = true;
            }
            this.highlightIndex = -1;
        }
        if (this.viewHolderCache.contains(pos)) {
            this.holderToPosMap.put(verticalViewHolder, pos);
            this.posToHolderMap.put(pos, verticalViewHolder);
        }
        final VerticalList.Model model = this.data.get(pos);
        if (pos == verticalViewHolder.getPos() && !model.needsRebind) {
            VerticalAdapter.sLogger.i("onBindViewHolder: already bind unselect it");
            verticalViewHolder.setState(VerticalViewHolder.State.UNSELECTED, VerticalViewHolder.AnimationType.NONE, 0);
        }
        else {
            final boolean b3 = false;
            final VerticalList.ModelType modelType = verticalViewHolder.getModelType();
            boolean b4 = b3;
            switch (modelType) {
                case BLANK:
                    break;
                case ICON_BKCOLOR:
                case TWO_ICONS:
                case ICON:
                case SCROLL_CONTENT:
                    b4 = true;
                default:
                    b4 = b3;
                case TITLE:
                case TITLE_SUBTITLE:
                case LOADING:
                case ICON_OPTIONS: {
                    if (!model.fontSizeCheckDone) {
                        VerticalList.setFontSize(model, this.vlist.allowsTwoLineTitles());
                    }
                    final boolean needsRebind = model.needsRebind;
                    final boolean dontStartFluctuator = model.dontStartFluctuator;
                    model.needsRebind = false;
                    model.dontStartFluctuator = false;
                    if (!needsRebind) {
                        verticalViewHolder.clearAnimation();
                    }
                    verticalViewHolder.setExtras(model.extras);
                    verticalViewHolder.setPos(pos);
                    this.modelState.reset();
                    verticalViewHolder.preBind(model, this.modelState);
                    if (this.vlist.bindCallbacks && b4) {
                        int n;
                        if (this.vlist.firstEntryBlank) {
                            n = pos - 1;
                        }
                        else {
                            n = pos;
                        }
                        this.vlist.callback.onBindToView(this.data.get(pos), (View)verticalViewHolder.layout, n, this.modelState);
                    }
                    verticalViewHolder.bind(model, this.modelState);
                    if (!this.initialState) {
                        if (this.vlist.getRawPosition() == pos) {
                            this.initialState = true;
                            verticalViewHolder.setState(VerticalViewHolder.State.SELECTED, VerticalViewHolder.AnimationType.INIT, 100);
                            VerticalAdapter.sLogger.v("initial state: position:" + pos);
                            break;
                        }
                        verticalViewHolder.setState(VerticalViewHolder.State.UNSELECTED, VerticalViewHolder.AnimationType.INIT, 100);
                        break;
                    }
                    else {
                        if (b || (needsRebind && this.vlist.getRawPosition() == pos)) {
                            verticalViewHolder.setState(VerticalViewHolder.State.SELECTED, VerticalViewHolder.AnimationType.NONE, 0, !dontStartFluctuator);
                            break;
                        }
                        verticalViewHolder.setState(VerticalViewHolder.State.UNSELECTED, VerticalViewHolder.AnimationType.NONE, 0);
                        break;
                    }
                    break;
                }
            }
        }
    }
    
    public VerticalViewHolder onCreateViewHolder(final ViewGroup viewGroup, final int n) {
        final VerticalList.ModelType modelType = VerticalList.ModelType.values()[n];
        if (VerticalAdapter.sLogger.isLoggable(2)) {
            VerticalAdapter.sLogger.v("onCreateViewHolder: " + modelType);
        }
        VerticalViewHolder verticalViewHolder = null;
        switch (modelType) {
            default:
                throw new RuntimeException("implement the model:" + modelType);
            case BLANK:
                verticalViewHolder = BlankViewHolder.buildViewHolder(viewGroup, this.vlist, this.handler);
                break;
            case TITLE:
                verticalViewHolder = TitleViewHolder.buildViewHolder(viewGroup, this.vlist, this.handler);
                break;
            case TITLE_SUBTITLE:
                verticalViewHolder = TitleSubtitleViewHolder.buildViewHolder(viewGroup, this.vlist, this.handler);
                break;
            case ICON_BKCOLOR:
                verticalViewHolder = IconBkColorViewHolder.buildViewHolder(viewGroup, this.vlist, this.handler);
                break;
            case TWO_ICONS:
                verticalViewHolder = IconsTwoViewHolder.buildViewHolder(viewGroup, this.vlist, this.handler);
                break;
            case ICON:
                verticalViewHolder = IconOneViewHolder.buildViewHolder(viewGroup, this.vlist, this.handler);
                break;
            case LOADING:
                verticalViewHolder = LoadingViewHolder.buildViewHolder(viewGroup, this.vlist, this.handler);
                break;
            case ICON_OPTIONS:
                verticalViewHolder = IconOptionsViewHolder.buildViewHolder(viewGroup, this.vlist, this.handler);
                break;
            case SCROLL_CONTENT:
                verticalViewHolder = ScrollableViewHolder.buildViewHolder(viewGroup, this.vlist, this.handler);
                break;
            case LOADING_CONTENT:
                verticalViewHolder = ContentLoadingViewHolder.buildViewHolder(viewGroup, this.vlist, this.handler);
                break;
            case SWITCH:
                verticalViewHolder = SwitchViewHolder.Companion.buildViewHolder(viewGroup, this.vlist, this.handler);
                break;
        }
        return verticalViewHolder;
    }
    
    public boolean onFailedToRecycleView(final VerticalViewHolder verticalViewHolder) {
        verticalViewHolder.clearAnimation();
        this.clearPrevPos(verticalViewHolder);
        return super.onFailedToRecycleView(verticalViewHolder);
    }
    
    public void onViewRecycled(final VerticalViewHolder verticalViewHolder) {
        verticalViewHolder.clearAnimation();
        this.clearPrevPos(verticalViewHolder);
    }
    
    public void setHighlightIndex(final int highlightIndex) {
        this.highlightIndex = highlightIndex;
    }
    
    public void setInitialState(final boolean initialState) {
        this.initialState = initialState;
    }
    
    public void setViewHolderCacheIndex(final int[] array) {
        this.viewHolderCache.clear();
        if (array != null) {
            for (int i = 0; i < array.length; ++i) {
                this.viewHolderCache.add(array[i]);
            }
        }
    }
    
    public void updateModel(final int n, final VerticalList.Model model) {
        if (n >= 0 && n < this.data.size()) {
            this.data.set(n, model);
            VerticalAdapter.sLogger.v("updated model:" + n);
        }
    }
}
