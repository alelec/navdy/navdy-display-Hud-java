package com.navdy.hud.app.ui.component.vlist.viewholder;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.animation.AnimatorSet;
import android.view.ViewGroup;
import com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils;
import android.widget.ImageView;
import android.text.Html;
import android.text.TextUtils;
import android.graphics.Color;
import android.view.View;
import android.view.LayoutInflater;
import android.animation.Animator;
import android.os.Handler;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import android.widget.TextView;
import android.view.ViewGroup;
import com.navdy.hud.app.ui.component.HaloView;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import com.navdy.hud.app.ui.component.image.CrossFadeImageView;
import android.animation.AnimatorSet;
import com.navdy.service.library.log.Logger;

public abstract class IconBaseViewHolder extends VerticalViewHolder
{
    public static final int FLUCTUATOR_OPACITY_ALPHA = 153;
    public static final int HALO_DELAY_START_DURATION = 100;
    private static final int[] location;
    private static final Logger sLogger;
    protected AnimatorSet.Builder animatorSetBuilder;
    protected CrossFadeImageView crossFadeImageView;
    private Runnable fluctuatorRunnable;
    private DefaultAnimationListener fluctuatorStartListener;
    protected HaloView haloView;
    private boolean hasIconFluctuatorColor;
    private boolean hasSubTitle;
    private boolean hasSubTitle2;
    protected ViewGroup iconContainer;
    protected boolean iconScaleAnimationDisabled;
    protected ViewGroup imageContainer;
    protected TextView subTitle;
    protected TextView subTitle2;
    protected boolean textAnimationDisabled;
    protected TextView title;
    private float titleSelectedTopMargin;
    private float titleUnselectedScale;
    
    static {
        sLogger = VerticalViewHolder.sLogger;
        location = new int[2];
    }
    
    public IconBaseViewHolder(final ViewGroup viewGroup, final VerticalList list, final Handler handler) {
        super(viewGroup, list, handler);
        this.fluctuatorStartListener = new DefaultAnimationListener() {
            @Override
            public void onAnimationEnd(final Animator animator) {
                IconBaseViewHolder.this.itemAnimatorSet = null;
                IconBaseViewHolder.this.handler.removeCallbacks(IconBaseViewHolder.this.fluctuatorRunnable);
                IconBaseViewHolder.this.handler.postDelayed(IconBaseViewHolder.this.fluctuatorRunnable, 100L);
            }
        };
        this.fluctuatorRunnable = new Runnable() {
            @Override
            public void run() {
                IconBaseViewHolder.this.startFluctuator();
            }
        };
        this.imageContainer = (ViewGroup)viewGroup.findViewById(R.id.imageContainer);
        this.iconContainer = (ViewGroup)viewGroup.findViewById(R.id.iconContainer);
        (this.haloView = (HaloView)viewGroup.findViewById(R.id.halo)).setVisibility(GONE);
        this.crossFadeImageView = (CrossFadeImageView)viewGroup.findViewById(R.id.vlist_image);
        this.title = (TextView)viewGroup.findViewById(R.id.title);
        this.subTitle = (TextView)viewGroup.findViewById(R.id.subTitle);
        this.subTitle2 = (TextView)viewGroup.findViewById(R.id.subTitle2);
    }
    
    static ViewGroup getLayout(ViewGroup viewGroup, final int n, final int n2) {
        final LayoutInflater from = LayoutInflater.from(viewGroup.getContext());
        viewGroup = (ViewGroup)from.inflate(n, viewGroup, false);
        final ViewGroup viewGroup2 = (ViewGroup)viewGroup.findViewById(R.id.iconContainer);
        final CrossFadeImageView crossFadeImageView = (CrossFadeImageView)from.inflate(n2, viewGroup2, false);
        crossFadeImageView.inject(CrossFadeImageView.Mode.SMALL);
        crossFadeImageView.setId(R.id.vlist_image);
        viewGroup2.addView((View)crossFadeImageView);
        return viewGroup;
    }
    
    private int getSubTitle2Color() {
        int textColor;
        if (this.extras == null) {
            textColor = 0;
        }
        else {
            textColor = this.getTextColor("SUBTITLE_2_COLOR");
        }
        return textColor;
    }
    
    private int getSubTitleColor() {
        int textColor;
        if (this.extras == null) {
            textColor = 0;
        }
        else {
            textColor = this.getTextColor("SUBTITLE_COLOR");
        }
        return textColor;
    }
    
    private int getTextColor(String s) {
        final boolean b = false;
        int int1;
        if (this.extras == null) {
            int1 = (b ? 1 : 0);
        }
        else {
            s = this.extras.get(s);
            int1 = (b ? 1 : 0);
            if (s != null) {
                try {
                    int1 = Integer.parseInt(s);
                }
                catch (NumberFormatException ex) {
                    int1 = (b ? 1 : 0);
                }
            }
        }
        return int1;
    }
    
    private void setIconFluctuatorColor(int argb) {
        if (argb != 0) {
            argb = Color.argb(153, Color.red(argb), Color.green(argb), Color.blue(argb));
            this.hasIconFluctuatorColor = true;
            this.haloView.setStrokeColor(argb);
        }
        else {
            this.hasIconFluctuatorColor = false;
        }
    }
    
    private void setMultiLineStyles(final TextView textView, final boolean singleLine, final int n) {
        if (n != -1) {
            textView.setTextAppearance(textView.getContext(), n);
        }
        textView.setSingleLine(singleLine);
        if (singleLine) {
            textView.setEllipsize((TextUtils$TruncateAt)null);
        }
        else {
            textView.setMaxLines(2);
            textView.setEllipsize(TextUtils$TruncateAt.END);
        }
    }
    
    private void setSubTitle(final String text, final boolean b) {
        if (text == null) {
            this.subTitle.setText((CharSequence)"");
            this.hasSubTitle = false;
        }
        else {
            final int subTitleColor = this.getSubTitleColor();
            if (subTitleColor == 0) {
                this.subTitle.setTextColor(IconBaseViewHolder.subTitleColor);
            }
            else {
                this.subTitle.setTextColor(subTitleColor);
            }
            if (b) {
                this.subTitle.setText((CharSequence)Html.fromHtml(text));
            }
            else {
                this.subTitle.setText((CharSequence)text);
            }
            this.hasSubTitle = true;
        }
    }
    
    private void setSubTitle2(final String text, final boolean b) {
        if (text == null) {
            this.subTitle2.setText((CharSequence)"");
            this.subTitle2.setVisibility(GONE);
            this.hasSubTitle2 = false;
        }
        else {
            final int subTitle2Color = this.getSubTitle2Color();
            if (subTitle2Color == 0) {
                this.subTitle2.setTextColor(IconBaseViewHolder.subTitle2Color);
            }
            else {
                this.subTitle2.setTextColor(subTitle2Color);
            }
            if (b) {
                this.subTitle2.setText((CharSequence)Html.fromHtml(text));
            }
            else {
                this.subTitle2.setText((CharSequence)text);
            }
            this.subTitle2.setVisibility(View.VISIBLE);
            this.hasSubTitle2 = true;
        }
    }
    
    private void setTitle(final String text) {
        this.title.setText((CharSequence)text);
    }
    
    private void stopFluctuator() {
        if (this.hasIconFluctuatorColor) {
            this.handler.removeCallbacks(this.fluctuatorRunnable);
            this.haloView.setVisibility(GONE);
            this.haloView.stop();
        }
    }
    
    @Override
    public void bind(final VerticalList.Model model, final VerticalList.ModelState modelState) {
        if (modelState.updateTitle) {
            this.setTitle(model.title);
        }
        if (modelState.updateSubTitle) {
            if (model.subTitle != null) {
                this.setSubTitle(model.subTitle, model.subTitleFormatted);
            }
            else {
                this.setSubTitle(null, false);
            }
        }
        if (modelState.updateSubTitle2) {
            if (model.subTitle2 != null) {
                this.setSubTitle2(model.subTitle2, model.subTitle2Formatted);
            }
            else {
                this.setSubTitle2(null, false);
            }
        }
        this.textAnimationDisabled = model.noTextAnimation;
        this.iconScaleAnimationDisabled = model.noImageScaleAnimation;
        this.setIconFluctuatorColor(model.iconFluctuatorColor);
    }
    
    @Override
    public void clearAnimation() {
        this.stopFluctuator();
        this.stopAnimation();
        this.currentState = null;
        this.layout.setVisibility(View.VISIBLE);
        this.layout.setAlpha(1.0f);
    }
    
    @Override
    public void copyAndPosition(final ImageView imageView, final TextView textView, final TextView textView2, final TextView textView3, final boolean b) {
        final View big = this.crossFadeImageView.getBig();
        if (b) {
            VerticalAnimationUtils.copyImage((ImageView)big, imageView);
        }
        imageView.setX((float)IconBaseViewHolder.selectedImageX);
        imageView.setY((float)IconBaseViewHolder.selectedImageY);
        textView.setText(this.title.getText());
        textView.setX((float)IconBaseViewHolder.selectedTextX);
        this.title.getLocationOnScreen(IconBaseViewHolder.location);
        textView.setY((float)(IconBaseViewHolder.location[1] - IconBaseViewHolder.rootTopOffset));
        if (this.hasSubTitle) {
            textView2.setText(this.subTitle.getText());
            textView2.setX((float)IconBaseViewHolder.selectedTextX);
            this.subTitle.getLocationOnScreen(IconBaseViewHolder.location);
            textView2.setY((float)(IconBaseViewHolder.location[1] - IconBaseViewHolder.rootTopOffset));
        }
        else {
            textView2.setText((CharSequence)"");
        }
        if (this.hasSubTitle2) {
            textView3.setText(this.subTitle2.getText());
            textView3.setX((float)IconBaseViewHolder.selectedTextX);
            this.subTitle2.getLocationOnScreen(IconBaseViewHolder.location);
            textView3.setY((float)(IconBaseViewHolder.location[1] - IconBaseViewHolder.rootTopOffset));
        }
        else {
            textView3.setText((CharSequence)"");
        }
    }
    
    @Override
    public void preBind(final VerticalList.Model model, final VerticalList.ModelState modelState) {
        ((ViewGroup.MarginLayoutParams)this.title.getLayoutParams()).topMargin = (int)model.fontInfo.titleFontTopMargin;
        this.title.setTextSize(model.fontInfo.titleFontSize);
        this.setMultiLineStyles(this.title, model.fontInfo.titleSingleLine, -1);
        this.title.setLineSpacing(0.0f, 0.9f);
        this.titleSelectedTopMargin = (int)model.fontInfo.titleFontTopMargin;
        ((ViewGroup.MarginLayoutParams)this.subTitle.getLayoutParams()).topMargin = (int)model.fontInfo.subTitleFontTopMargin;
        final TextView subTitle = this.subTitle;
        final boolean b = !model.subTitle_2Lines;
        int n;
        if (model.subTitle_2Lines) {
            n = R.style.vlist_subtitle_2_line;
        }
        else {
            n = R.style.vlist_subtitle;
        }
        this.setMultiLineStyles(subTitle, b, n);
        this.subTitle.setTextSize(model.fontInfo.subTitleFontSize);
        ((ViewGroup.MarginLayoutParams)this.subTitle2.getLayoutParams()).topMargin = (int)model.fontInfo.subTitle2FontTopMargin;
        this.subTitle2.setTextSize(model.fontInfo.subTitle2FontSize);
        this.titleUnselectedScale = model.fontInfo.titleScale;
        this.crossFadeImageView.setSmallAlpha(-1.0f);
    }
    
    @Override
    public void select(final VerticalList.Model model, final int n, final int n2) {
        if (this.hasIconFluctuatorColor) {
            this.haloView.setVisibility(INVISIBLE);
            this.haloView.stop();
        }
        VerticalAnimationUtils.performClick((View)this.iconContainer, n2, new Runnable() {
            @Override
            public void run() {
                final VerticalList.ItemSelectionState itemSelectionState = IconBaseViewHolder.this.vlist.getItemSelectionState();
                itemSelectionState.set(model, model.id, n, -1, -1);
                IconBaseViewHolder.this.vlist.performSelectAction(itemSelectionState);
            }
        });
    }
    
    @Override
    public void setItemState(State selected, AnimationType none, final int n, final boolean b) {
        float titleUnselectedScale = 0.0f;
        float titleSelectedTopMargin = 0.0f;
        float alpha = 0.0f;
        float titleUnselectedScale2 = 0.0f;
        float alpha2 = 0.0f;
        float titleUnselectedScale3 = 0.0f;
        this.animatorSetBuilder = null;
        Label_0078: {
            if (!this.textAnimationDisabled) {
                break Label_0078;
            }
            if (none != AnimationType.MOVE) {
                selected = State.SELECTED;
                break Label_0078;
            }
            this.itemAnimatorSet = new AnimatorSet();
            this.animatorSetBuilder = this.itemAnimatorSet.play((Animator)ValueAnimator.ofFloat(new float[] { 0.0f, 1.0f }));
            return;
        }
        switch (selected) {
            case SELECTED:
                titleUnselectedScale = 1.0f;
                titleSelectedTopMargin = this.titleSelectedTopMargin;
                alpha = 1.0f;
                titleUnselectedScale2 = 1.0f;
                alpha2 = 1.0f;
                titleUnselectedScale3 = 1.0f;
                break;
            case UNSELECTED:
                titleUnselectedScale = this.titleUnselectedScale;
                titleSelectedTopMargin = 0.0f;
                alpha = 0.0f;
                titleUnselectedScale2 = this.titleUnselectedScale;
                alpha2 = 0.0f;
                titleUnselectedScale3 = this.titleUnselectedScale;
                break;
        }
        if (!this.hasSubTitle) {
            titleSelectedTopMargin = 0.0f;
            alpha = 0.0f;
            alpha2 = 0.0f;
        }
        this.title.setPivotX(0.0f);
        this.title.setPivotY(IconBaseViewHolder.titleHeight / 2.0f);
        switch (none) {
            case INIT:
            case NONE:
                this.title.setScaleX(titleUnselectedScale);
                this.title.setScaleY(titleUnselectedScale);
                ((ViewGroup.MarginLayoutParams)this.title.getLayoutParams()).topMargin = (int)titleSelectedTopMargin;
                this.title.requestLayout();
                break;
            case MOVE:
                this.itemAnimatorSet = new AnimatorSet();
                (this.animatorSetBuilder = this.itemAnimatorSet.play((Animator)ObjectAnimator.ofPropertyValuesHolder(this.title, new PropertyValuesHolder[] { PropertyValuesHolder.ofFloat(View.SCALE_X, new float[] { titleUnselectedScale }), PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[] { titleUnselectedScale }) }))).with(VerticalAnimationUtils.animateMargin((View)this.title, (int)titleSelectedTopMargin));
                break;
        }
        this.subTitle.setPivotX(0.0f);
        this.subTitle.setPivotY(IconBaseViewHolder.subTitleHeight / 2.0f);
        Enum<AnimationType> none2 = none;
        if (!this.hasSubTitle) {
            none2 = AnimationType.NONE;
        }
        switch (none2) {
            case INIT:
            case NONE:
                this.subTitle.setAlpha(alpha);
                this.subTitle.setScaleX(titleUnselectedScale2);
                this.subTitle.setScaleY(titleUnselectedScale2);
                break;
            case MOVE:
                this.animatorSetBuilder.with((Animator)ObjectAnimator.ofPropertyValuesHolder(this.subTitle, new PropertyValuesHolder[] { PropertyValuesHolder.ofFloat(View.SCALE_X, new float[] { titleUnselectedScale2 }), PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[] { titleUnselectedScale2 }), PropertyValuesHolder.ofFloat(View.ALPHA, new float[] { alpha }) }));
                break;
        }
        this.subTitle2.setPivotX(0.0f);
        this.subTitle2.setPivotY(IconBaseViewHolder.subTitleHeight / 2.0f);
        if (!this.hasSubTitle2) {
            none = AnimationType.NONE;
        }
        switch (none) {
            case INIT:
            case NONE:
                this.subTitle2.setAlpha(alpha2);
                this.subTitle2.setScaleX(titleUnselectedScale3);
                this.subTitle2.setScaleY(titleUnselectedScale3);
                break;
            case MOVE:
                this.animatorSetBuilder.with((Animator)ObjectAnimator.ofPropertyValuesHolder(this.subTitle2, new PropertyValuesHolder[] { PropertyValuesHolder.ofFloat(View.SCALE_X, new float[] { titleUnselectedScale3 }), PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[] { titleUnselectedScale3 }), PropertyValuesHolder.ofFloat(View.ALPHA, new float[] { alpha }) }));
                break;
        }
        if (selected != State.SELECTED || !b) {
            return;
        }
        if (this.itemAnimatorSet != null) {
            this.itemAnimatorSet.addListener((Animator.AnimatorListener)this.fluctuatorStartListener);
            return;
        }
        this.startFluctuator();
    }
    
    @Override
    public void startFluctuator() {
        if (this.hasIconFluctuatorColor) {
            this.haloView.setVisibility(View.VISIBLE);
            this.haloView.start();
        }
    }
}
