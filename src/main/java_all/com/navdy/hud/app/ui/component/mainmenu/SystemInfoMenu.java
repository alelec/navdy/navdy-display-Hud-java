package com.navdy.hud.app.ui.component.mainmenu;

import java.io.ObjectInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import com.navdy.hud.app.ui.component.image.InitialsImageView;
import com.navdy.hud.app.ui.component.homescreen.HomeScreen;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import java.util.Arrays;
import com.navdy.hud.app.device.gps.GpsUtils;
import com.navdy.hud.app.event.DriverProfileUpdated;
import com.navdy.hud.app.ui.component.UISettings;
import butterknife.ButterKnife;
import android.view.View;
import com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex;
import java.util.ArrayList;
import com.squareup.otto.Subscribe;
import com.navdy.hud.app.device.dial.DialFirmwareUpdater;
import android.bluetooth.BluetoothAdapter;
import com.navdy.hud.app.device.dial.DialManager;
import com.navdy.hud.app.util.OTAUpdateService;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.service.library.events.preferences.NavigationPreferences;
import com.navdy.hud.app.maps.here.HereOfflineMapsVersion;
import android.text.TextUtils;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.service.library.events.preferences.DriverProfilePreferences;
import com.navdy.hud.app.obd.ObdDeviceConfigurationManager;
import com.navdy.service.library.events.preferences.InputPreferences;
import com.navdy.hud.app.profile.DriverProfile;
import com.navdy.hud.app.framework.DriverProfileHelper;
import java.util.Iterator;
import com.navdy.obd.PidSet;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.maps.util.DistanceConverter;
import com.navdy.hud.app.maps.MapEvents;
import android.os.Looper;
import com.navdy.hud.app.ui.component.vlist.viewholder.ScrollableViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.ui.component.systeminfo.GpsSignalView;
import com.navdy.hud.app.obd.ObdManager;
import com.navdy.hud.app.maps.here.HereLocationFixManager;
import android.os.Handler;
import android.view.ViewGroup;
import java.util.List;
import com.squareup.otto.Bus;
import butterknife.InjectView;
import android.widget.TextView;
import com.navdy.service.library.log.Logger;
import android.content.res.Resources;
import java.text.SimpleDateFormat;
import com.navdy.hud.app.ui.component.vlist.VerticalList;

public class SystemInfoMenu implements IMenu
{
    private static final int UPDATE_FREQUENCY = 2000;
    private static final VerticalList.Model back;
    private static final SimpleDateFormat dateFormat;
    private static String fuelUnitText;
    private static final VerticalList.Model infoModel;
    private static final String infoTitle;
    private static final String noData;
    private static final String noFixStr;
    private static final String off;
    private static final String on;
    private static String pressureUnitText;
    private static final Resources resources;
    private static final Logger sLogger;
    @InjectView(R.id.si_auto_on)
    protected TextView autoOn;
    private int backSelection;
    private Bus bus;
    private List<VerticalList.Model> cachedList;
    @InjectView(R.id.si_model)
    protected TextView carModel;
    @InjectView(R.id.si_check_engine_light)
    protected TextView checkEngineLight;
    @InjectView(R.id.si_device_mileage)
    protected TextView deviceMileage;
    @InjectView(R.id.si_dial_bt)
    protected TextView dialBt;
    @InjectView(R.id.si_dial_fw)
    protected TextView dialFw;
    @InjectView(R.id.si_display_bt)
    protected TextView displayBt;
    @InjectView(R.id.si_display_fw)
    protected TextView displayFw;
    @InjectView(R.id.si_eng_oil_pressure)
    protected TextView engineOilPressure;
    @InjectView(R.id.si_engine_oil_pressure_lyt)
    protected ViewGroup engineOilPressureLayout;
    @InjectView(R.id.si_eng_temp)
    protected TextView engineTemp;
    @InjectView(R.id.si_engine_trouble_codes)
    protected TextView engineTroubleCodes;
    @InjectView(R.id.si_fix)
    protected TextView fix;
    @InjectView(R.id.si_fuel)
    protected TextView fuel;
    @InjectView(R.id.si_gestures)
    protected TextView gestures;
    @InjectView(R.id.si_gps)
    protected TextView gps;
    @InjectView(R.id.si_gps_speed)
    protected TextView gpsSpeed;
    private Handler handler;
    private HereLocationFixManager hereLocationFixManager;
    @InjectView(R.id.si_maps_data_verified)
    protected TextView hereMapVerified;
    @InjectView(R.id.si_map_version)
    protected TextView hereMapsVersion;
    @InjectView(R.id.si_offline_maps)
    protected TextView hereOfflineMaps;
    @InjectView(R.id.si_here_sdk_version)
    protected TextView hereSdk;
    @InjectView(R.id.si_updated)
    protected TextView hereUpdated;
    @InjectView(R.id.si_maps_voices_verified)
    protected TextView hereVoiceVerified;
    @InjectView(R.id.si_maf)
    protected TextView maf;
    @InjectView(R.id.si_make)
    protected TextView make;
    @InjectView(R.id.si_obd_data)
    protected TextView obdData;
    @InjectView(R.id.obd_data_lyt)
    protected ViewGroup obdDataLayout;
    @InjectView(R.id.si_obd_fw)
    protected TextView obdFw;
    private ObdManager obdManager;
    @InjectView(R.id.si_odometer)
    protected TextView odo;
    @InjectView(R.id.si_odometer_lyt)
    protected ViewGroup odometerLayout;
    private IMenu parent;
    private MainMenuScreen2.Presenter presenter;
    protected boolean registered;
    @InjectView(R.id.si_route_calc)
    protected TextView routeCalc;
    @InjectView(R.id.si_route_pref)
    protected TextView routePref;
    @InjectView(R.id.si_rpm)
    protected TextView rpm;
    @InjectView(R.id.gps_signal)
    GpsSignalView satelliteView;
    @InjectView(R.id.si_satellites)
    protected TextView satellites;
    @InjectView(R.id.si_special_pids_lyt)
    protected ViewGroup specialPidsLayout;
    @InjectView(R.id.si_speed)
    protected TextView speed;
    private SpeedManager speedManager;
    @InjectView(R.id.si_temperature)
    protected TextView temperature;
    @InjectView(R.id.si_eng_trip_fuel)
    protected TextView tripFuel;
    @InjectView(R.id.si_trip_fuel_layout)
    protected ViewGroup tripFuelLayout;
    protected String ubloxFixType;
    @InjectView(R.id.si_ui_scaling)
    protected TextView uiScaling;
    private Runnable updateRunnable;
    @InjectView(R.id.si_voltage)
    protected TextView voltage;
    private VerticalMenuComponent vscrollComponent;
    @InjectView(R.id.si_year)
    protected TextView year;
    
    static {
        sLogger = new Logger(SystemInfoMenu.class);
        dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        resources = HudApplication.getAppContext().getResources();
        noFixStr = SystemInfoMenu.resources.getString(R.string.gps_no_fix);
        noData = SystemInfoMenu.resources.getString(R.string.si_no_data);
        on = SystemInfoMenu.resources.getString(R.string.si_on);
        off = SystemInfoMenu.resources.getString(R.string.si_off);
        final int color = SystemInfoMenu.resources.getColor(R.color.mm_back);
        infoTitle = SystemInfoMenu.resources.getString(R.string.carousel_menu_system_info_title);
        SystemInfoMenu.pressureUnitText = SystemInfoMenu.resources.getString(R.string.pressure_unit);
        SystemInfoMenu.fuelUnitText = SystemInfoMenu.resources.getString(R.string.litre);
        back = IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, color, MainMenu.bkColorUnselected, color, SystemInfoMenu.resources.getString(R.string.back), null);
        infoModel = ScrollableViewHolder.buildModel(R.layout.system_info_lyt);
    }
    
    SystemInfoMenu(final Bus bus, final VerticalMenuComponent vscrollComponent, final MainMenuScreen2.Presenter presenter, final IMenu parent) {
        this.updateRunnable = new Runnable() {
            @Override
            public void run() {
                if (SystemInfoMenu.this.isMenuLoaded()) {
                    SystemInfoMenu.this.updateTemperature();
                    SystemInfoMenu.this.updateVoltage();
                    SystemInfoMenu.this.setFix();
                    SystemInfoMenu.this.handler.removeCallbacks((Runnable)this);
                    SystemInfoMenu.this.handler.postDelayed((Runnable)this, 2000L);
                }
            }
        };
        this.bus = bus;
        this.vscrollComponent = vscrollComponent;
        this.presenter = presenter;
        this.parent = parent;
        this.handler = new Handler(Looper.getMainLooper());
        this.obdManager = ObdManager.getInstance();
        this.speedManager = SpeedManager.getInstance();
    }
    
    private void formatRoutePref(final StringBuilder sb, final String s) {
        int n = 0;
        if (sb.length() > 0) {
            sb.append(",");
            sb.append(" ");
        }
        else {
            n = 1;
        }
        String string = s;
        if (n != 0) {
            string = Character.toUpperCase(s.charAt(0)) + s.substring(1);
        }
        sb.append(string);
    }
    
    private static String getFixTypeDescription(final int n) {
        String s = null;
        switch (n) {
            default:
                s = "Unknown";
                break;
            case 0:
                s = "No Fix";
                break;
            case 1:
                s = "Standard 2D/3D";
                break;
            case 2:
                s = "Differential";
                break;
            case 4:
                s = "RTK Fixed";
                break;
            case 5:
                s = "RTK Float";
                break;
            case 6:
                s = "DR Fix";
                break;
        }
        return s;
    }
    
    private boolean isMenuLoaded() {
        return this.dialFw != null;
    }
    
    private String removeBrackets(final String s) {
        return s.replace("(", "").replace(")", "");
    }
    
    private void setFix() {
        if (this.hereLocationFixManager != null) {
            final Resources resources = HudApplication.getAppContext().getResources();
            final MapEvents.LocationFix locationFixEvent = this.hereLocationFixManager.getLocationFixEvent();
            SystemInfoMenu.sLogger.v("fix [" + locationFixEvent.locationAvailable + "] phone[" + locationFixEvent.usingPhoneLocation + "] u-blox[" + locationFixEvent.usingLocalGpsLocation + "] type[" + this.ubloxFixType + "]");
            if (!locationFixEvent.locationAvailable) {
                this.fix.setText((CharSequence)SystemInfoMenu.noFixStr);
            }
            else if (locationFixEvent.usingPhoneLocation) {
                this.fix.setText(resources.getText(R.string.gps_phone));
            }
            else if (locationFixEvent.usingLocalGpsLocation) {
                final TextView fix = this.fix;
                CharSequence text;
                if (this.ubloxFixType != null) {
                    text = this.ubloxFixType;
                }
                else {
                    text = resources.getText(R.string.gps_ublox_navdy);
                }
                fix.setText(text);
            }
            else {
                this.fix.setText((CharSequence)SystemInfoMenu.noFixStr);
            }
        }
    }
    
    private void update() {
        this.updateVersions();
        this.updateStats();
        this.updateHere();
        this.updateVehicleInfo();
        this.updateGenericPref();
        this.updateNavPref();
    }
    
    private void updateDeviceMileage() {
        if (this.presenter != null) {
            final long totalDistanceTravelledWithNavdy = this.presenter.tripManager.getTotalDistanceTravelledWithNavdy();
            SystemInfoMenu.sLogger.v("navdy distance travelled:" + totalDistanceTravelledWithNavdy);
            if (totalDistanceTravelledWithNavdy > 0L) {
                final DistanceConverter.Distance distance = new DistanceConverter.Distance();
                DistanceConverter.convertToDistance(SpeedManager.getInstance().getSpeedUnit(), totalDistanceTravelledWithNavdy, distance);
                this.deviceMileage.setText((CharSequence)HereManeuverDisplayBuilder.getFormattedDistance(distance.value, distance.unit, false));
            }
        }
    }
    
    private void updateDynamicVehicleInfo() {
        final boolean connected = this.obdManager.isConnected();
        final ObdManager.ConnectionType connectionType = this.obdManager.getConnectionType();
        final ViewGroup obdDataLayout = this.obdDataLayout;
        int visibility;
        if (connectionType == ObdManager.ConnectionType.POWER_ONLY) {
            visibility = 8;
        }
        else {
            visibility = 0;
        }
        obdDataLayout.setVisibility(visibility);
        if (connected) {
            this.updateFuel();
            this.updateRPM();
            this.updateSpeed();
            this.updateMAF();
            this.updateEngineTemperature();
            this.updateVoltage();
            this.updateOdometer();
            final PidSet supportedPids = this.obdManager.getSupportedPids();
            int n;
            if (supportedPids.contains(258) || supportedPids.contains(259)) {
                n = 1;
            }
            else {
                n = 0;
            }
            if (n == 0) {
                this.specialPidsLayout.setVisibility(GONE);
            }
            else {
                this.specialPidsLayout.setVisibility(View.VISIBLE);
                this.updateEngineFuelPressure();
                this.updateEngineTripFuel();
            }
            final TextView checkEngineLight = this.checkEngineLight;
            String text;
            if (this.obdManager.isCheckEngineLightOn()) {
                text = SystemInfoMenu.on;
            }
            else {
                text = SystemInfoMenu.off;
            }
            checkEngineLight.setText((CharSequence)text);
            final List<String> troubleCodes = this.obdManager.getTroubleCodes();
            if (troubleCodes != null && troubleCodes.size() > 0) {
                final StringBuilder sb = new StringBuilder();
                final Iterator<String> iterator = troubleCodes.iterator();
                while (iterator.hasNext()) {
                    sb.append(iterator.next() + ",");
                }
                sb.deleteCharAt(sb.length() - 1);
                this.engineTroubleCodes.setText((CharSequence)sb.toString());
            }
            else {
                this.engineTroubleCodes.setText((CharSequence)SystemInfoMenu.noData);
            }
        }
        else {
            this.specialPidsLayout.setVisibility(GONE);
            this.odometerLayout.setVisibility(GONE);
            this.checkEngineLight.setText((CharSequence)SystemInfoMenu.noData);
            this.engineTroubleCodes.setText((CharSequence)SystemInfoMenu.noData);
        }
    }
    
    private void updateEngineFuelPressure() {
        final int n = (int)this.obdManager.getPidValue(258);
        if (n > 0) {
            this.engineOilPressure.setText((CharSequence)(n + " " + SystemInfoMenu.pressureUnitText));
        }
        else {
            this.engineOilPressure.setText((CharSequence)SystemInfoMenu.noData);
        }
    }
    
    private void updateEngineTemperature() {
        final DriverProfile currentProfile = DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile();
        final double pidValue = this.obdManager.getPidValue(5);
        if (pidValue > 0.0) {
            int n = 0;
            int n2 = 0;
            switch (currentProfile.getUnitSystem()) {
                default:
                    n = R.string.si_fahrenheit;
                    n2 = (int)Math.round(1.8 * pidValue + 32.0);
                    break;
                case UNIT_SYSTEM_METRIC:
                    n = R.string.si_celsius;
                    n2 = (int)Math.round(pidValue);
                    break;
            }
            this.engineTemp.setText((CharSequence)(String.format("%d", n2) + " " + '°' + SystemInfoMenu.resources.getString(n)));
        }
        else {
            this.engineTemp.setText((CharSequence)SystemInfoMenu.noData);
        }
    }
    
    private void updateEngineTripFuel() {
        final int n = (int)this.obdManager.getPidValue(259);
        if (n > 0) {
            this.tripFuel.setText((CharSequence)(n + " " + SystemInfoMenu.fuelUnitText));
        }
        else {
            this.tripFuel.setText((CharSequence)SystemInfoMenu.noData);
        }
    }
    
    private void updateFuel() {
        final int fuelLevel = this.obdManager.getFuelLevel();
        if (fuelLevel != -1) {
            this.fuel.setText((CharSequence)SystemInfoMenu.resources.getString(R.string.si_fuel_level, new Object[] { fuelLevel }));
        }
        else {
            this.fuel.setText((CharSequence)SystemInfoMenu.noData);
        }
    }
    
    private void updateGPSSpeed() {
        final int gpsSpeed = this.speedManager.getGpsSpeed();
        if (gpsSpeed != -1) {
            this.gpsSpeed.setText((CharSequence)String.valueOf(gpsSpeed));
        }
        else {
            this.gpsSpeed.setText((CharSequence)SystemInfoMenu.noData);
        }
    }
    
    private void updateGenericPref() {
        final DriverProfile currentProfile = DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile();
        final InputPreferences inputPreferences = currentProfile.getInputPreferences();
        if (inputPreferences != null) {
            if (Boolean.TRUE.equals(inputPreferences.use_gestures)) {
                this.gestures.setText((CharSequence)SystemInfoMenu.resources.getString(R.string.si_enabled));
            }
            else {
                this.gestures.setText((CharSequence)SystemInfoMenu.resources.getString(R.string.si_disabled));
            }
        }
        final ObdDeviceConfigurationManager obdDeviceConfigurationManager = this.obdManager.getObdDeviceConfigurationManager();
        if (obdDeviceConfigurationManager != null) {
            if (obdDeviceConfigurationManager.isAutoOnEnabled()) {
                this.autoOn.setText((CharSequence)SystemInfoMenu.resources.getString(R.string.si_enabled));
            }
            else {
                this.autoOn.setText((CharSequence)SystemInfoMenu.resources.getString(R.string.si_disabled));
            }
        }
        final DriverProfilePreferences.ObdScanSetting obdScanSetting = currentProfile.getObdScanSetting();
        if (obdScanSetting != null) {
            switch (obdScanSetting) {
                default:
                    this.obdData.setText((CharSequence)SystemInfoMenu.resources.getString(R.string.si_off));
                    break;
                case SCAN_ON:
                case SCAN_DEFAULT_ON:
                    this.obdData.setText((CharSequence)SystemInfoMenu.resources.getString(R.string.si_on));
                    break;
            }
        }
        final DriverProfilePreferences.DisplayFormat displayFormat = currentProfile.getDisplayFormat();
        if (displayFormat != null) {
            switch (displayFormat) {
                case DISPLAY_FORMAT_COMPACT:
                    this.uiScaling.setText((CharSequence)SystemInfoMenu.resources.getString(R.string.si_compact));
                    break;
                case DISPLAY_FORMAT_NORMAL:
                    this.uiScaling.setText((CharSequence)SystemInfoMenu.resources.getString(R.string.si_normal));
                    break;
            }
        }
    }
    
    private void updateHere() {
        final HereMapsManager instance = HereMapsManager.getInstance();
        if (!instance.isInitializing()) {
            if (instance.isInitialized()) {
                this.hereSdk.setText((CharSequence)instance.getVersion());
                if (instance.isVoiceSkinsLoaded()) {
                    this.hereVoiceVerified.setText((CharSequence)SystemInfoMenu.resources.getString(R.string.si_yes));
                }
                else {
                    this.hereVoiceVerified.setText((CharSequence)SystemInfoMenu.resources.getString(R.string.si_no));
                }
                if (instance.isMapDataVerified()) {
                    SystemInfoMenu.sLogger.v("map package installed count:" + instance.getMapPackageCount());
                    this.hereMapVerified.setText((CharSequence)SystemInfoMenu.resources.getString(R.string.si_yes));
                }
                else {
                    this.hereMapVerified.setText((CharSequence)SystemInfoMenu.resources.getString(R.string.si_no));
                }
                final HereOfflineMapsVersion offlineMapsVersion = HereMapsManager.getInstance().getOfflineMapsVersion();
                if (offlineMapsVersion != null) {
                    CharSequence string = null;
                    final List<String> packages = offlineMapsVersion.getPackages();
                    if (packages != null) {
                        final int size = packages.size();
                        final StringBuilder sb = new StringBuilder();
                        for (int i = 0; i < size; ++i) {
                            if (i != 0) {
                                sb.append(",");
                                sb.append(" ");
                            }
                            sb.append(packages.get(i));
                        }
                        string = sb.toString();
                    }
                    String format;
                    if (offlineMapsVersion.getDate() != null) {
                        format = SystemInfoMenu.dateFormat.format(offlineMapsVersion.getDate());
                    }
                    else {
                        format = null;
                    }
                    final String version = offlineMapsVersion.getVersion();
                    if (!TextUtils.isEmpty((CharSequence)format)) {
                        this.hereUpdated.setText((CharSequence)format);
                    }
                    if (!TextUtils.isEmpty(string)) {
                        this.hereOfflineMaps.setText(string);
                    }
                    if (!TextUtils.isEmpty((CharSequence)version)) {
                        this.hereMapsVersion.setText((CharSequence)version);
                    }
                }
            }
            else {
                this.hereSdk.setText((CharSequence)instance.getError().name());
            }
        }
    }
    
    private void updateMAF() {
        final int n = (int)this.obdManager.getInstantFuelConsumption();
        if (n > 0) {
            this.maf.setText((CharSequence)String.valueOf(n));
        }
        else {
            this.maf.setText((CharSequence)SystemInfoMenu.noData);
        }
    }
    
    private void updateNavPref() {
        final NavigationPreferences navigationPreferences = DriverProfileHelper.getInstance().getCurrentProfile().getNavigationPreferences();
        if (navigationPreferences != null) {
            switch (navigationPreferences.routingType) {
                case ROUTING_FASTEST:
                    this.routeCalc.setText((CharSequence)SystemInfoMenu.resources.getString(R.string.si_fastest));
                    break;
                case ROUTING_SHORTEST:
                    this.routeCalc.setText((CharSequence)SystemInfoMenu.resources.getString(R.string.si_shortest));
                    break;
            }
            final StringBuilder sb = new StringBuilder();
            if (Boolean.TRUE.equals(navigationPreferences.allowHighways)) {
                sb.append(SystemInfoMenu.resources.getString(R.string.si_highways));
            }
            if (Boolean.TRUE.equals(navigationPreferences.allowTollRoads)) {
                this.formatRoutePref(sb, SystemInfoMenu.resources.getString(R.string.si_tollroads));
            }
            if (Boolean.TRUE.equals(navigationPreferences.allowFerries)) {
                this.formatRoutePref(sb, SystemInfoMenu.resources.getString(R.string.si_ferries));
            }
            if (Boolean.TRUE.equals(navigationPreferences.allowTunnels)) {
                this.formatRoutePref(sb, SystemInfoMenu.resources.getString(R.string.si_tunnels));
            }
            if (Boolean.TRUE.equals(navigationPreferences.allowUnpavedRoads)) {
                this.formatRoutePref(sb, SystemInfoMenu.resources.getString(R.string.si_unpaved_roads));
            }
            if (Boolean.TRUE.equals(navigationPreferences.allowAutoTrains)) {
                this.formatRoutePref(sb, SystemInfoMenu.resources.getString(R.string.si_auto_trains));
            }
            sb.append("\n");
            this.routePref.setText((CharSequence)sb.toString());
        }
    }
    
    private void updateOdometer() {
        if (this.obdManager.getSupportedPids().contains(260)) {
            this.odometerLayout.setVisibility(View.VISIBLE);
            final int n = (int)this.obdManager.getPidValue(260);
            if (n > 0) {
                final DistanceConverter.Distance distance = new DistanceConverter.Distance();
                DistanceConverter.convertToDistance(this.speedManager.getSpeedUnit(), n, distance);
                this.odo.setText((CharSequence)HereManeuverDisplayBuilder.getFormattedDistance(distance.value, distance.unit, false));
            }
            else {
                this.odo.setText((CharSequence)SystemInfoMenu.noData);
            }
        }
        else {
            this.odometerLayout.setVisibility(GONE);
        }
    }
    
    private void updateRPM() {
        final int engineRpm = this.obdManager.getEngineRpm();
        if (engineRpm != -1) {
            this.rpm.setText((CharSequence)String.valueOf(engineRpm));
        }
        else {
            this.rpm.setText((CharSequence)SystemInfoMenu.noData);
        }
    }
    
    private void updateSpeed() {
        final int currentSpeed = this.speedManager.getCurrentSpeed();
        if (currentSpeed != -1) {
            this.speed.setText((CharSequence)String.valueOf(currentSpeed));
        }
        else {
            this.speed.setText((CharSequence)SystemInfoMenu.noData);
        }
    }
    
    private void updateStats() {
        this.updateTemperature();
        this.updateDeviceMileage();
        this.updateGPSSpeed();
        final HereMapsManager instance = HereMapsManager.getInstance();
        if (instance.isInitialized()) {
            this.hereLocationFixManager = instance.getLocationFixManager();
        }
        this.setFix();
    }
    
    private void updateTemperature() {
        final int currentTemperature = AnalyticsSupport.getCurrentTemperature();
        if (currentTemperature != -1) {
            this.temperature.setText((CharSequence)(String.valueOf(currentTemperature) + " " + '°' + SystemInfoMenu.resources.getString(R.string.si_celsius)));
        }
    }
    
    private void updateVehicleInfo() {
        final DriverProfile currentProfile = DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile();
        final String carMake = currentProfile.getCarMake();
        if (!TextUtils.isEmpty((CharSequence)carMake)) {
            this.make.setText((CharSequence)carMake);
        }
        final String carModel = currentProfile.getCarModel();
        if (!TextUtils.isEmpty((CharSequence)carModel)) {
            this.carModel.setText((CharSequence)carModel);
        }
        final String carYear = currentProfile.getCarYear();
        if (!TextUtils.isEmpty((CharSequence)carYear)) {
            this.year.setText((CharSequence)carYear);
        }
        this.updateDynamicVehicleInfo();
    }
    
    private void updateVersions() {
        String shortVersion = "1.3.3051-corona";
        if (DeviceUtil.isUserBuild()) {
            shortVersion = OTAUpdateService.shortVersion("1.3.3051-corona");
        }
        this.displayFw.setText((CharSequence)shortVersion);
        final DialManager instance = DialManager.getInstance();
        Label_0110: {
            if (!instance.isDialConnected()) {
                break Label_0110;
            }
            while (true) {
                try {
                    final DialFirmwareUpdater dialFirmwareUpdater = instance.getDialFirmwareUpdater();
                    CharSequence value = null;
                    if (dialFirmwareUpdater != null) {
                        final int incrementalVersion = dialFirmwareUpdater.getVersions().dial.incrementalVersion;
                        if (incrementalVersion != -1) {
                            value = String.valueOf(incrementalVersion);
                        }
                        if (!TextUtils.isEmpty(value)) {
                            this.dialFw.setText(value);
                        }
                    }
                    final String dialName = instance.getDialName();
                    if (!TextUtils.isEmpty((CharSequence)dialName)) {
                        this.dialBt.setText((CharSequence)this.removeBrackets(dialName));
                    }
                    final String name = BluetoothAdapter.getDefaultAdapter().getName();
                    if (!TextUtils.isEmpty((CharSequence)name)) {
                        this.displayBt.setText((CharSequence)this.removeBrackets(name));
                    }
                    final String obdChipFirmwareVersion = this.obdManager.getObdChipFirmwareVersion();
                    if (!TextUtils.isEmpty((CharSequence)obdChipFirmwareVersion)) {
                        this.obdFw.setText((CharSequence)obdChipFirmwareVersion);
                    }
                }
                catch (Throwable t) {
                    SystemInfoMenu.sLogger.e(t);
                    continue;
                }
                break;
            }
        }
    }
    
    private void updateVoltage() {
        final double batteryVoltage = this.obdManager.getBatteryVoltage();
        if (batteryVoltage > 0.0) {
            this.voltage.setText((CharSequence)(String.format("%.1f", batteryVoltage) + SystemInfoMenu.resources.getString(R.string.si_volt)));
        }
        else {
            this.voltage.setText((CharSequence)SystemInfoMenu.noData);
        }
    }
    
    @Subscribe
    public void GPSSpeedChangeEvent(final MapEvents.GPSSpeedEvent gpsSpeedEvent) {
        if (this.isMenuLoaded()) {
            this.updateSpeed();
            this.updateGPSSpeed();
        }
    }
    
    @Subscribe
    public void ObdStateChangeEvent(final ObdManager.ObdConnectionStatusEvent obdConnectionStatusEvent) {
        if (this.isMenuLoaded()) {
            this.updateSpeed();
            this.updateDynamicVehicleInfo();
        }
    }
    
    @Override
    public IMenu getChildMenu(final IMenu menu, final String s, final String s2) {
        return null;
    }
    
    @Override
    public int getInitialSelection() {
        return 1;
    }
    
    @Override
    public List<VerticalList.Model> getItems() {
        final ArrayList<VerticalList.Model> cachedList = new ArrayList<VerticalList.Model>();
        cachedList.add(SystemInfoMenu.back);
        cachedList.add(SystemInfoMenu.infoModel);
        return this.cachedList = cachedList;
    }
    
    @Override
    public VerticalList.Model getModelfromPos(final int n) {
        VerticalList.Model model;
        if (this.cachedList != null && this.cachedList.size() > n) {
            model = this.cachedList.get(n);
        }
        else {
            model = null;
        }
        return model;
    }
    
    @Override
    public VerticalFastScrollIndex getScrollIndex() {
        return null;
    }
    
    @Override
    public Menu getType() {
        return Menu.SYSTEM_INFO;
    }
    
    @Override
    public boolean isBindCallsEnabled() {
        return true;
    }
    
    @Override
    public boolean isFirstItemEmpty() {
        return true;
    }
    
    @Override
    public boolean isItemClickable(final int n, final int n2) {
        return n2 == 0;
    }
    
    @Override
    public void onBindToView(final VerticalList.Model model, final View view, int visibility, final VerticalList.ModelState modelState) {
        if (visibility == 1 && ((ViewGroup)view).getChildCount() > 0) {
            final ViewGroup viewGroup = (ViewGroup)((ViewGroup)view).getChildAt(0);
            ButterKnife.inject(this, view);
            final GpsSignalView satelliteView = this.satelliteView;
            if (UISettings.advancedGpsStatsEnabled()) {
                visibility = 0;
            }
            else {
                visibility = 8;
            }
            satelliteView.setVisibility(visibility);
            this.update();
            if (!this.registered) {
                this.bus.register(this);
                this.registered = true;
                ObdManager.getInstance().enableInstantaneousMode(true);
                SystemInfoMenu.sLogger.v("registered, enabled instantaneous mode");
            }
            this.handler.removeCallbacks(this.updateRunnable);
            this.handler.postDelayed(this.updateRunnable, 2000L);
        }
    }
    
    @Subscribe
    public void onDriverProfilePrefChangeEvent(final DriverProfileUpdated driverProfileUpdated) {
        if (this.isMenuLoaded() && driverProfileUpdated.state == DriverProfileUpdated.State.UPDATED) {
            this.updateGenericPref();
        }
    }
    
    @Override
    public void onFastScrollEnd() {
    }
    
    @Override
    public void onFastScrollStart() {
    }
    
    @Subscribe
    public void onGpsSatelliteData(final GpsUtils.GpsSatelliteData gpsSatelliteData) {
        while (true) {
            int int1 = 0;
            int int2 = 0;
            int int3 = 0;
            Label_0353: {
                GpsSignalView.SatelliteData[] satelliteData = null;
            Label_0339:
                while (true) {
                    try {
                        if (!this.isMenuLoaded()) {
                            return;
                        }
                        int1 = gpsSatelliteData.data.getInt("SAT_SEEN", 0);
                        int2 = gpsSatelliteData.data.getInt("SAT_USED", 0);
                        if (int1 > 0 || int2 > 0) {
                            this.satellites.setText((CharSequence)SystemInfoMenu.resources.getString(R.string.si_seen, new Object[] { int2, int1 }));
                        }
                        else {
                            this.satellites.setText((CharSequence)SystemInfoMenu.noData);
                        }
                        this.ubloxFixType = getFixTypeDescription(gpsSatelliteData.data.getInt("SAT_FIX_TYPE", 0));
                        int3 = gpsSatelliteData.data.getInt("SAT_MAX_DB", 0);
                        if (int3 > 0) {
                            this.gps.setText((CharSequence)SystemInfoMenu.resources.getString(R.string.si_gps_signal, new Object[] { int3 }));
                            if (UISettings.advancedGpsStatsEnabled()) {
                                satelliteData = new GpsSignalView.SatelliteData[int1];
                                for (int i = 0; i < int1; ++i) {
                                    satelliteData[i] = new GpsSignalView.SatelliteData(gpsSatelliteData.data.getInt("SAT_ID_" + (i + 1)), gpsSatelliteData.data.getInt("SAT_DB_" + (i + 1)), gpsSatelliteData.data.getString("SAT_PROVIDER_" + (i + 1)));
                                }
                                break Label_0339;
                            }
                            break Label_0353;
                        }
                    }
                    catch (Throwable t) {
                        SystemInfoMenu.sLogger.e(t);
                        return;
                    }
                    this.gps.setText((CharSequence)SystemInfoMenu.noData);
                    continue;
                }
                Arrays.sort(satelliteData);
                this.satelliteView.setSatelliteData(satelliteData);
            }
            SystemInfoMenu.sLogger.v("satellite data { seen=" + int1 + " used=" + int2 + " fixType=" + this.ubloxFixType + " maxDB=" + int3);
        }
    }
    
    @Override
    public void onItemSelected(final VerticalList.ItemSelectionState itemSelectionState) {
    }
    
    @Subscribe
    public void onNavigationPrefChangeEvent(final NavigationPreferences navigationPreferences) {
        if (this.isMenuLoaded()) {
            this.updateNavPref();
        }
    }
    
    @Subscribe
    public void onPidChangeEvent(final ObdManager.ObdPidChangeEvent obdPidChangeEvent) {
        if (this.isMenuLoaded()) {
            switch (obdPidChangeEvent.pid.getId()) {
                case 5:
                    this.updateEngineTemperature();
                    break;
                case 47:
                    this.updateFuel();
                    break;
                case 12:
                    this.updateRPM();
                    break;
                case 256:
                    this.updateMAF();
                    break;
                case 260:
                    this.updateOdometer();
                    break;
                case 13:
                    this.updateSpeed();
                    break;
                case 258:
                    this.updateEngineFuelPressure();
                    break;
                case 259:
                    this.updateEngineTripFuel();
                    break;
            }
        }
    }
    
    @Override
    public void onScrollIdle() {
    }
    
    @Subscribe
    public void onSpeedDataExpired(final SpeedManager.SpeedDataExpired speedDataExpired) {
        if (this.isMenuLoaded()) {
            this.updateSpeed();
            this.updateGPSSpeed();
        }
    }
    
    @Subscribe
    public void onSpeedUnitChanged(final SpeedManager.SpeedUnitChanged speedUnitChanged) {
        if (this.isMenuLoaded()) {
            this.updateSpeed();
            this.updateGPSSpeed();
        }
    }
    
    @Subscribe
    public void onSupportedPidsChanged(final ObdManager.ObdSupportedPidsChangedEvent obdSupportedPidsChangedEvent) {
        if (this.isMenuLoaded()) {
            this.updateSpeed();
            this.updateGPSSpeed();
            this.updateDynamicVehicleInfo();
        }
    }
    
    @Override
    public void onUnload(final MenuLevel menuLevel) {
        if (this.registered) {
            if (RemoteDeviceManager.getInstance().getUiStateManager().getHomescreenView().getDisplayMode() != HomeScreen.DisplayMode.SMART_DASH) {
                SystemInfoMenu.sLogger.v("onUnload: disable instantaneous mode");
                ObdManager.getInstance().enableInstantaneousMode(false);
            }
            else {
                SystemInfoMenu.sLogger.v("onUnload: instantaneous mode still enabled");
            }
            this.bus.unregister(this);
            this.registered = false;
            SystemInfoMenu.sLogger.v("unregistered");
        }
        this.displayFw = null;
        this.dialFw = null;
        this.obdFw = null;
        this.dialBt = null;
        this.displayBt = null;
        this.gps = null;
        this.satellites = null;
        this.fix = null;
        this.temperature = null;
        this.deviceMileage = null;
        this.hereSdk = null;
        this.hereUpdated = null;
        this.hereMapsVersion = null;
        this.hereOfflineMaps = null;
        this.hereMapVerified = null;
        this.hereVoiceVerified = null;
        this.make = null;
        this.carModel = null;
        this.year = null;
        this.fuel = null;
        this.rpm = null;
        this.speed = null;
        this.odo = null;
        this.maf = null;
        this.engineTemp = null;
        this.voltage = null;
        this.gestures = null;
        this.autoOn = null;
        this.obdData = null;
        this.uiScaling = null;
        this.routeCalc = null;
        this.routePref = null;
    }
    
    @Override
    public boolean selectItem(final VerticalList.ItemSelectionState itemSelectionState) {
        SystemInfoMenu.sLogger.v("select id:" + itemSelectionState.id + " pos:" + itemSelectionState.pos);
        switch (itemSelectionState.id) {
            case R.id.menu_back:
                SystemInfoMenu.sLogger.v("back");
                AnalyticsSupport.recordMenuSelection("back");
                this.presenter.loadMenu(this.parent, MenuLevel.BACK_TO_PARENT, this.backSelection, 0);
                break;
        }
        return true;
    }
    
    @Override
    public void setBackSelectionId(final int n) {
    }
    
    @Override
    public void setBackSelectionPos(final int backSelection) {
        this.backSelection = backSelection;
    }
    
    @Override
    public void setSelectedIcon() {
        this.vscrollComponent.setSelectedIconImage(R.drawable.icon_settings_navdy_data, null, InitialsImageView.Style.DEFAULT);
        this.vscrollComponent.selectedText.setText((CharSequence)SystemInfoMenu.infoTitle);
    }
    
    @Override
    public void showToolTip() {
    }
}
