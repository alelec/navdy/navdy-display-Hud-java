package com.navdy.hud.app.ui.component.carousel;

import com.navdy.hud.app.ui.component.image.CrossFadeImageView;
import android.view.View;
import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.AnimatorSet;
import android.animation.Animator.AnimatorListener;
import com.navdy.service.library.log.Logger;

public class CarouselAnimator implements AnimationStrategy
{
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(CarouselAnimator.class);
    }
    
    @Override
    public AnimatorSet buildLayoutAnimation(final Animator.AnimatorListener animatorListener, final CarouselLayout carouselLayout, final int n, final int n2) {
        return null;
    }
    
    @Override
    public AnimatorSet createHiddenViewAnimation(final CarouselLayout carouselLayout, final Direction direction) {
        final AnimatorSet set = new AnimatorSet();
        View view;
        View view2;
        if (direction == Direction.RIGHT) {
            view = carouselLayout.newLeftView;
            view2 = carouselLayout.leftView;
        }
        else {
            view = carouselLayout.newRightView;
            view2 = carouselLayout.rightView;
        }
        set.play((Animator)ObjectAnimator.ofFloat(view, "x", new float[] { view2.getX() }));
        return set;
    }
    
    @Override
    public AnimatorSet createMiddleLeftViewAnimation(final CarouselLayout carouselLayout, final Direction direction) {
        final AnimatorSet set = new AnimatorSet();
        View view;
        if (direction == Direction.LEFT) {
            view = carouselLayout.leftView;
        }
        else {
            view = carouselLayout.rightView;
        }
        float n;
        if (carouselLayout.middleLeftView.getScaleX() == 1.0f) {
            n = carouselLayout.sideImageSize / carouselLayout.mainImageSize;
        }
        else {
            n = 1.0f;
        }
        final ObjectAnimator ofFloat = ObjectAnimator.ofFloat(carouselLayout.middleLeftView, "scaleX", new float[] { n });
        final ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(carouselLayout.middleLeftView, "scaleY", new float[] { n });
        final ObjectAnimator ofFloat3 = ObjectAnimator.ofFloat(carouselLayout.middleLeftView, "x", new float[] { view.getX() });
        final ObjectAnimator ofFloat4 = ObjectAnimator.ofFloat(carouselLayout.middleLeftView, "y", new float[] { view.getY() });
        final AnimatorSet crossFadeAnimator = ((CrossFadeImageView)carouselLayout.middleLeftView).getCrossFadeAnimator();
        carouselLayout.middleLeftView.setPivotX(0.5f);
        carouselLayout.middleLeftView.setPivotY(0.5f);
        set.playTogether(new Animator[] { ofFloat, ofFloat2, ofFloat3, ofFloat4, crossFadeAnimator });
        return set;
    }
    
    @Override
    public AnimatorSet createMiddleRightViewAnimation(final CarouselLayout carouselLayout, final Direction direction) {
        final AnimatorSet set = new AnimatorSet();
        final float n = carouselLayout.sideImageSize / carouselLayout.middleRightView.getMeasuredHeight();
        View view;
        if (direction == Direction.LEFT) {
            view = carouselLayout.leftView;
        }
        else {
            view = carouselLayout.rightView;
        }
        set.playTogether(new Animator[] { ObjectAnimator.ofFloat(carouselLayout.middleRightView, "scaleX", new float[] { n }), ObjectAnimator.ofFloat(carouselLayout.middleRightView, "scaleY", new float[] { n }), ObjectAnimator.ofFloat(carouselLayout.middleRightView, "x", new float[] { view.getX() + (carouselLayout.mainViewDividerPadding + carouselLayout.sideImageSize) / 2 }), ObjectAnimator.ofFloat(carouselLayout.middleRightView, "y", new float[] { carouselLayout.middleRightView.getY() }), ObjectAnimator.ofFloat(carouselLayout.middleRightView, "alpha", new float[] { 0.0f }) });
        return set;
    }
    
    @Override
    public AnimatorSet createNewMiddleRightViewAnimation(final CarouselLayout carouselLayout, final Direction direction) {
        final AnimatorSet set = new AnimatorSet();
        final ObjectAnimator ofFloat = ObjectAnimator.ofFloat(carouselLayout.newMiddleRightView, "x", new float[] { carouselLayout.middleLeftView.getX() + carouselLayout.mainViewDividerPadding + carouselLayout.mainImageSize });
        if (direction == Direction.RIGHT) {
            set.playTogether(new Animator[] { ofFloat, ObjectAnimator.ofFloat(carouselLayout.newMiddleRightView, "alpha", new float[] { 1.0f }) });
        }
        else {
            set.playTogether(new Animator[] { ofFloat });
        }
        return set;
    }
    
    @Override
    public AnimatorSet createSideViewToMiddleAnimation(final CarouselLayout carouselLayout, final Direction direction) {
        final AnimatorSet set = new AnimatorSet();
        View view;
        if (direction == Direction.RIGHT) {
            view = carouselLayout.leftView;
        }
        else {
            view = carouselLayout.rightView;
        }
        float n;
        if (view.getScaleX() == 1.0f) {
            n = carouselLayout.mainImageSize / carouselLayout.sideImageSize;
        }
        else {
            n = 1.0f;
        }
        final ObjectAnimator ofFloat = ObjectAnimator.ofFloat(view, "scaleX", new float[] { n });
        final ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(view, "scaleY", new float[] { n });
        final ObjectAnimator ofFloat3 = ObjectAnimator.ofFloat(view, "x", new float[] { carouselLayout.middleLeftView.getX() });
        final ObjectAnimator ofFloat4 = ObjectAnimator.ofFloat(view, "y", new float[] { carouselLayout.middleLeftView.getY() });
        final AnimatorSet crossFadeAnimator = ((CrossFadeImageView)view).getCrossFadeAnimator();
        view.setPivotX(0.5f);
        view.setPivotY(0.5f);
        set.playTogether(new Animator[] { ofFloat, ofFloat2, ofFloat3, ofFloat4, crossFadeAnimator });
        return set;
    }
    
    @Override
    public Animator createViewOutAnimation(final CarouselLayout carouselLayout, final Direction direction) {
        ObjectAnimator objectAnimator;
        if (direction == Direction.RIGHT) {
            objectAnimator = ObjectAnimator.ofFloat(carouselLayout.rightView, "x", new float[] { carouselLayout.rightView.getX() + carouselLayout.getMeasuredWidth() });
        }
        else {
            objectAnimator = ObjectAnimator.ofFloat(carouselLayout.leftView, "x", new float[] { -(carouselLayout.leftView.getX() + carouselLayout.leftView.getMeasuredWidth()) });
        }
        return (Animator)objectAnimator;
    }
}
