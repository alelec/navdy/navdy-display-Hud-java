package com.navdy.hud.app.ui.component.vlist.viewholder;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.view.View;
import android.animation.PropertyValuesHolder;
import com.navdy.hud.app.ui.component.image.CrossFadeImageView;
import com.navdy.hud.app.ui.component.image.InitialsImageView;
import android.os.Handler;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import android.view.ViewGroup;
import com.navdy.service.library.log.Logger;

public class IconOneViewHolder extends IconBaseViewHolder
{
    private static final Logger sLogger;
    
    static {
        sLogger = VerticalViewHolder.sLogger;
    }
    
    private IconOneViewHolder(final ViewGroup viewGroup, final VerticalList list, final Handler handler) {
        super(viewGroup, list, handler);
    }
    
    public static VerticalList.Model buildModel(final int n, final int n2, final int n3, final String s, final String s2) {
        return buildModel(n, n2, n3, s, s2, null);
    }
    
    public static VerticalList.Model buildModel(final int id, final int icon, final int iconFluctuatorColor, final String title, final String subTitle, final String subTitle2) {
        final VerticalList.Model model = new VerticalList.Model();
        model.type = VerticalList.ModelType.ICON;
        model.id = id;
        model.icon = icon;
        model.iconFluctuatorColor = iconFluctuatorColor;
        model.title = title;
        model.subTitle = subTitle;
        model.subTitle2 = subTitle2;
        return model;
    }
    
    public static IconOneViewHolder buildViewHolder(final ViewGroup viewGroup, final VerticalList list, final Handler handler) {
        return new IconOneViewHolder(IconBaseViewHolder.getLayout(viewGroup, R.layout.vlist_item, R.layout.crossfade_image_lyt), list, handler);
    }
    
    private void setIcon(final int n, final boolean b, final boolean b2) {
        String s = null;
        if (this.extras != null) {
            s = this.extras.get("INITIAL");
        }
        if (b) {
            InitialsImageView.Style style;
            if (s != null) {
                style = InitialsImageView.Style.MEDIUM;
            }
            else {
                style = InitialsImageView.Style.DEFAULT;
            }
            ((InitialsImageView)this.crossFadeImageView.getBig()).setImage(n, null, style);
        }
        if (b2) {
            InitialsImageView.Style style2;
            if (s != null) {
                style2 = InitialsImageView.Style.TINY;
            }
            else {
                style2 = InitialsImageView.Style.DEFAULT;
            }
            final InitialsImageView initialsImageView = (InitialsImageView)this.crossFadeImageView.getSmall();
            initialsImageView.setAlpha(0.5f);
            initialsImageView.setImage(n, s, style2);
        }
    }
    
    @Override
    public void bind(final VerticalList.Model model, final VerticalList.ModelState modelState) {
        super.bind(model, modelState);
        this.setIcon(model.icon, modelState.updateImage, modelState.updateSmallImage);
    }
    
    @Override
    public VerticalList.ModelType getModelType() {
        return VerticalList.ModelType.ICON;
    }
    
    @Override
    public void setItemState(final State state, final AnimationType animationType, final int n, final boolean b) {
        super.setItemState(state, animationType, n, b);
        float n2 = 0.0f;
        float alpha = 0.0f;
        final CrossFadeImageView.Mode mode = null;
        CrossFadeImageView.Mode mode2 = null;
        switch (state) {
            default:
                mode2 = mode;
                break;
            case SELECTED:
                n2 = 1.0f;
                alpha = 1.0f;
                mode2 = CrossFadeImageView.Mode.BIG;
                break;
            case UNSELECTED:
                n2 = 0.6f;
                alpha = 0.5f;
                mode2 = CrossFadeImageView.Mode.SMALL;
                break;
        }
        switch (animationType) {
            case NONE:
            case INIT:
                this.imageContainer.setScaleX(n2);
                this.imageContainer.setScaleY(n2);
                this.imageContainer.setAlpha(alpha);
                this.crossFadeImageView.setMode(mode2);
                break;
            case MOVE:
                this.animatorSetBuilder.with((Animator)ObjectAnimator.ofPropertyValuesHolder(this.imageContainer, new PropertyValuesHolder[] { PropertyValuesHolder.ofFloat(View.SCALE_X, new float[] { n2 }), PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[] { n2 }), PropertyValuesHolder.ofFloat(View.ALPHA, new float[] { alpha }) }));
                if (this.crossFadeImageView.getMode() != mode2) {
                    this.animatorSetBuilder.with((Animator)this.crossFadeImageView.getCrossFadeAnimator());
                    break;
                }
                break;
        }
    }
}
