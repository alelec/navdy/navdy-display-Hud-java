package com.navdy.hud.app.ui.component.vlist;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.HashMap;
import com.navdy.service.library.log.Logger;
import java.util.Map;

public class VerticalModelCache
{
    private static final Map<VerticalList.ModelType, CacheEntry> map;
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(VerticalModelCache.class);
        (map = new HashMap<VerticalList.ModelType, CacheEntry>()).put(VerticalList.ModelType.ICON_BKCOLOR, new CacheEntry(500, 100));
        VerticalModelCache.map.put(VerticalList.ModelType.TWO_ICONS, new CacheEntry(70, 10));
        VerticalModelCache.map.put(VerticalList.ModelType.LOADING_CONTENT, new CacheEntry(500, 100));
    }
    
    public static void addToCache(final VerticalList.ModelType modelType, final VerticalList.Model model) {
        if (model != null && modelType != null) {
            final CacheEntry cacheEntry = VerticalModelCache.map.get(modelType);
            if (cacheEntry != null && cacheEntry.items.size() < cacheEntry.maxItems) {
                model.clear();
                cacheEntry.items.add(model);
            }
        }
    }
    
    public static void addToCache(final List<VerticalList.Model> list) {
        if (list != null) {
            for (final VerticalList.Model model : list) {
                addToCache(model.type, model);
            }
        }
    }
    
    public static VerticalList.Model getFromCache(final VerticalList.ModelType modelType) {
        final VerticalList.Model model = null;
        VerticalList.Model model2;
        if (modelType == null) {
            model2 = model;
        }
        else {
            final CacheEntry cacheEntry = VerticalModelCache.map.get(modelType);
            model2 = model;
            if (cacheEntry != null) {
                model2 = model;
                if (cacheEntry.items.size() > 0) {
                    model2 = cacheEntry.items.remove(0);
                }
            }
        }
        return model2;
    }
    
    private static class CacheEntry
    {
        ArrayList<VerticalList.Model> items;
        int maxItems;
        
        CacheEntry(int i, final int n) {
            this.maxItems = i;
            this.items = new ArrayList<VerticalList.Model>(i);
            if (n > 0) {
                for (i = 0; i < n; ++i) {
                    this.items.add(new VerticalList.Model());
                }
            }
        }
    }
}
