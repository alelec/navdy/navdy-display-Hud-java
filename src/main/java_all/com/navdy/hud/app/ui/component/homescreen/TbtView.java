package com.navdy.hud.app.ui.component.homescreen;

import com.squareup.otto.Subscribe;
import android.view.ViewGroup;
import android.animation.LayoutTransition;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import butterknife.ButterKnife;
import android.animation.ValueAnimator;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import android.animation.AnimatorSet;
import android.text.style.StyleSpan;
import android.text.TextUtils;
import android.graphics.drawable.Drawable;
import android.text.SpannableStringBuilder;
import android.support.annotation.Nullable;
import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.Animator;
import android.animation.AnimatorSet;
import android.util.AttributeSet;
import android.content.Context;
import android.view.animation.AccelerateDecelerateInterpolator;
import com.navdy.hud.app.ui.framework.UIStateManager;
import android.widget.ProgressBar;
import android.view.View;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.view.MainView;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.maps.MapEvents;
import com.squareup.otto.Bus;
import android.widget.ImageView;
import butterknife.InjectView;
import android.widget.TextView;
import android.animation.TimeInterpolator;
import android.widget.RelativeLayout;

public class TbtView extends RelativeLayout implements IHomeScreenLifecycle
{
    static final long MANEUVER_PROGRESS_ANIMATION_DURATION = 250L;
    private static final long MANEUVER_SWAP_DURATION = 500L;
    private static final TimeInterpolator maneuverSwapInterpolator;
    @InjectView(R.id.activeMapDistance)
    TextView activeMapDistance;
    @InjectView(R.id.activeMapIcon)
    ImageView activeMapIcon;
    @InjectView(R.id.activeMapInstruction)
    TextView activeMapInstruction;
    private Bus bus;
    private HomeScreenView homeScreenView;
    private long initialProgressBarDistance;
    private String lastDistance;
    private MapEvents.ManeuverDisplay lastEvent;
    private HereManeuverDisplayBuilder.ManeuverState lastManeuverState;
    private MainView.CustomAnimationMode lastMode;
    private String lastRoadText;
    private int lastTurnIconId;
    private String lastTurnText;
    private Logger logger;
    @InjectView(R.id.nextMapIcon)
    ImageView nextMapIcon;
    @InjectView(R.id.nextMapIconContainer)
    View nextMapIconContainer;
    @InjectView(R.id.nextMapInstruction)
    TextView nextMapInstruction;
    @InjectView(R.id.nextMapInstructionContainer)
    View nextMapInstructionContainer;
    @InjectView(R.id.now_icon)
    View nowIcon;
    private boolean paused;
    @InjectView(R.id.progress_bar)
    ProgressBar progressBar;
    private UIStateManager uiStateManager;
    
    static {
        maneuverSwapInterpolator = (TimeInterpolator)new AccelerateDecelerateInterpolator();
    }
    
    public TbtView(final Context context) {
        this(context, null);
    }
    
    public TbtView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public TbtView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.lastTurnIconId = -1;
    }
    
    private void animateSetDistance(final HereManeuverDisplayBuilder.ManeuverState maneuverState) {
        int n;
        if (this.lastManeuverState != HereManeuverDisplayBuilder.ManeuverState.NOW && maneuverState == HereManeuverDisplayBuilder.ManeuverState.NOW) {
            n = 1;
        }
        else {
            n = 0;
        }
        final boolean b = this.lastManeuverState == HereManeuverDisplayBuilder.ManeuverState.NOW && maneuverState != HereManeuverDisplayBuilder.ManeuverState.NOW;
        if (n != 0) {
            final AnimatorSet fadeInAndScaleUpAnimator = HomeScreenUtils.getFadeInAndScaleUpAnimator(this.nowIcon);
            final ObjectAnimator alphaAnimator = HomeScreenUtils.getAlphaAnimator((View)this.activeMapDistance, 0);
            final AnimatorSet set = new AnimatorSet();
            set.play((Animator)fadeInAndScaleUpAnimator).with((Animator)alphaAnimator);
            set.setDuration(250L).start();
        }
        else if (b) {
            final AnimatorSet fadeOutAndScaleDownAnimator = HomeScreenUtils.getFadeOutAndScaleDownAnimator(this.nowIcon);
            final ObjectAnimator alphaAnimator2 = HomeScreenUtils.getAlphaAnimator((View)this.activeMapDistance, 1);
            final AnimatorSet set2 = new AnimatorSet();
            set2.play((Animator)fadeOutAndScaleDownAnimator).with((Animator)alphaAnimator2);
            set2.setDuration(250L).start();
        }
    }
    
    @Nullable
    private AnimatorSet animateSetIcon(final int n) {
        this.nextMapIconContainer.setVisibility(View.VISIBLE);
        final ObjectAnimator translationYPositionAnimator = HomeScreenUtils.getTranslationYPositionAnimator((View)this.activeMapIcon, HomeScreenResourceValues.activeRoadInfoInstructionTranslationYAnimation);
        final ObjectAnimator translationYPositionAnimator2 = HomeScreenUtils.getTranslationYPositionAnimator(this.nextMapIconContainer, HomeScreenResourceValues.activeRoadInfoInstructionTranslationYAnimation);
        translationYPositionAnimator.addListener((Animator.AnimatorListener)new Animator.AnimatorListener() {
            public void onAnimationCancel(final Animator animator) {
            }
            
            public void onAnimationEnd(final Animator animator) {
                TbtView.this.activeMapIcon.setImageResource(n);
                TbtView.this.activeMapIcon.setTranslationY(0.0f);
                TbtView.this.nextMapIconContainer.setTranslationY(0.0f);
                TbtView.this.nextMapIconContainer.setVisibility(GONE);
            }
            
            public void onAnimationRepeat(final Animator animator) {
            }
            
            public void onAnimationStart(final Animator animator) {
            }
        });
        final AnimatorSet set = new AnimatorSet();
        set.play((Animator)translationYPositionAnimator).with((Animator)translationYPositionAnimator2);
        return set;
    }
    
    private AnimatorSet animateSetInstruction(final SpannableStringBuilder text) {
        this.nextMapInstructionContainer.setVisibility(View.VISIBLE);
        this.nextMapInstruction.setText((CharSequence)text);
        final ObjectAnimator translationYPositionAnimator = HomeScreenUtils.getTranslationYPositionAnimator((View)this.activeMapInstruction, HomeScreenResourceValues.activeRoadInfoInstructionTranslationYAnimation);
        final ObjectAnimator translationYPositionAnimator2 = HomeScreenUtils.getTranslationYPositionAnimator(this.nextMapInstructionContainer, HomeScreenResourceValues.activeRoadInfoInstructionTranslationYAnimation);
        translationYPositionAnimator.addListener((Animator.AnimatorListener)new Animator.AnimatorListener() {
            public void onAnimationCancel(final Animator animator) {
            }
            
            public void onAnimationEnd(final Animator animator) {
                TbtView.this.activeMapInstruction.setText((CharSequence)text);
                TbtView.this.activeMapInstruction.setTranslationY(0.0f);
                TbtView.this.nextMapInstructionContainer.setTranslationY(0.0f);
                TbtView.this.nextMapInstructionContainer.setVisibility(GONE);
            }
            
            public void onAnimationRepeat(final Animator animator) {
            }
            
            public void onAnimationStart(final Animator animator) {
            }
        });
        final AnimatorSet set = new AnimatorSet();
        set.play((Animator)translationYPositionAnimator).with((Animator)translationYPositionAnimator2);
        return set;
    }
    
    private void cleanInstructions() {
        this.activeMapDistance.setText((CharSequence)"");
        HomeScreenUtils.getFadeOutAndScaleDownAnimator(this.nowIcon).setDuration(250L).start();
        HomeScreenUtils.setWidth((View)this.progressBar, 0);
        this.activeMapIcon.setImageDrawable((Drawable)null);
        this.nextMapIcon.setImageDrawable((Drawable)null);
        this.activeMapInstruction.setText((CharSequence)"");
        this.nextMapInstruction.setText((CharSequence)"");
    }
    
    private void setDistance(final String s, final HereManeuverDisplayBuilder.ManeuverState maneuverState) {
        if (!TextUtils.equals((CharSequence)s, (CharSequence)this.lastDistance)) {
            this.lastDistance = s;
            this.activeMapDistance.setText((CharSequence)s);
            if (this.activeMapDistance.getAlpha() == 0.0f && this.nowIcon.getAlpha() == 0.0f && !this.uiStateManager.isMainUIShrunk()) {
                this.activeMapDistance.setAlpha(1.0f);
            }
            this.animateSetDistance(maneuverState);
        }
    }
    
    @Nullable
    private AnimatorSet setIcon(final int imageResource, final boolean b) {
        AnimatorSet animateSetIcon = null;
        if (this.lastTurnIconId != imageResource) {
            if ((this.lastTurnIconId = imageResource) == -1) {
                this.activeMapIcon.setImageDrawable((Drawable)null);
            }
            else {
                this.nextMapIcon.setImageResource(imageResource);
                if (b) {
                    this.activeMapIcon.setImageResource(imageResource);
                }
                else {
                    animateSetIcon = this.animateSetIcon(imageResource);
                }
            }
        }
        return animateSetIcon;
    }
    
    private AnimatorSet setInstruction(final String lastTurnText, final String lastRoadText) {
        AnimatorSet animateSetInstruction;
        if (TextUtils.equals((CharSequence)lastTurnText, (CharSequence)this.lastTurnText) && TextUtils.equals((CharSequence)lastRoadText, (CharSequence)this.lastRoadText)) {
            animateSetInstruction = null;
        }
        else {
            this.lastTurnText = lastTurnText;
            this.lastRoadText = lastRoadText;
            final SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
            if (this.lastTurnText != null && HereManeuverDisplayBuilder.canShowTurnText(this.lastTurnText)) {
                spannableStringBuilder.append((CharSequence)this.lastTurnText);
                spannableStringBuilder.append((CharSequence)" ");
            }
            if (this.lastRoadText != null) {
                spannableStringBuilder.setSpan(new StyleSpan(1), 0, spannableStringBuilder.length(), 33);
                spannableStringBuilder.append((CharSequence)this.lastRoadText);
            }
            animateSetInstruction = this.animateSetInstruction(spannableStringBuilder);
        }
        return animateSetInstruction;
    }
    
    private void setProgressBar(final HereManeuverDisplayBuilder.ManeuverState maneuverState, final long initialProgressBarDistance) {
        final boolean b = this.lastManeuverState != HereManeuverDisplayBuilder.ManeuverState.SOON && maneuverState == HereManeuverDisplayBuilder.ManeuverState.SOON;
        int n;
        if (this.lastManeuverState == HereManeuverDisplayBuilder.ManeuverState.SOON && maneuverState != HereManeuverDisplayBuilder.ManeuverState.SOON) {
            n = 1;
        }
        else {
            n = 0;
        }
        if (n != 0) {
            HomeScreenUtils.setWidth((View)this.progressBar, 0);
            this.initialProgressBarDistance = 0L;
        }
        else if (b) {
            HomeScreenUtils.setWidth((View)this.progressBar, HomeScreenResourceValues.activeRoadProgressBarWidth);
            this.initialProgressBarDistance = initialProgressBarDistance;
        }
        else if (maneuverState == HereManeuverDisplayBuilder.ManeuverState.SOON) {
            HomeScreenUtils.getProgressBarAnimator(this.progressBar, (int)(100.0 * (1.0 - initialProgressBarDistance / this.initialProgressBarDistance))).setDuration(250L).start();
        }
    }
    
    public void clearState() {
        this.lastTurnIconId = -1;
        this.lastDistance = null;
        this.lastTurnText = null;
        this.lastRoadText = null;
        this.cleanInstructions();
    }
    
    public void getCustomAnimator(final MainView.CustomAnimationMode lastMode, final AnimatorSet.Builder animatorSet$Builder) {
        this.lastMode = lastMode;
        switch (lastMode) {
            case SHRINK_LEFT: {
                final AnimatorSet set = new AnimatorSet();
                final AnimatorSet.Builder play = set.play((Animator)HomeScreenUtils.getXPositionAnimator((View)this.activeMapIcon, HomeScreenResourceValues.activeRoadInfoIconShrinkLeftX));
                play.with((Animator)HomeScreenUtils.getXPositionAnimator(this.nextMapIconContainer, HomeScreenResourceValues.activeRoadInfoIconShrinkLeftX));
                final ObjectAnimator xPositionAnimator = HomeScreenUtils.getXPositionAnimator((View)this.activeMapInstruction, HomeScreenResourceValues.activeRoadInfoInstructionShrinkLeftX);
                final ValueAnimator widthAnimator = HomeScreenUtils.getWidthAnimator((View)this.activeMapInstruction, HomeScreenResourceValues.activeRoadInfoInstructionShrinkWidth);
                play.with((Animator)xPositionAnimator);
                play.with((Animator)widthAnimator);
                final ObjectAnimator xPositionAnimator2 = HomeScreenUtils.getXPositionAnimator(this.nextMapInstructionContainer, HomeScreenResourceValues.activeRoadInfoInstructionShrinkLeftX);
                final ValueAnimator widthAnimator2 = HomeScreenUtils.getWidthAnimator(this.nextMapInstructionContainer, HomeScreenResourceValues.activeRoadInfoInstructionShrinkWidth);
                play.with((Animator)xPositionAnimator2);
                play.with((Animator)widthAnimator2);
                if (this.lastManeuverState == HereManeuverDisplayBuilder.ManeuverState.NOW) {
                    play.with((Animator)HomeScreenUtils.getFadeOutAndScaleDownAnimator(this.nowIcon));
                }
                else {
                    play.with((Animator)HomeScreenUtils.getAlphaAnimator((View)this.activeMapDistance, 0));
                }
                set.addListener((Animator.AnimatorListener)new DefaultAnimationListener() {
                    @Override
                    public void onAnimationEnd(final Animator animator) {
                    }
                    
                    @Override
                    public void onAnimationStart(final Animator animator) {
                    }
                });
                animatorSet$Builder.with((Animator)set);
                break;
            }
            case EXPAND: {
                final AnimatorSet set2 = new AnimatorSet();
                final AnimatorSet.Builder play2 = set2.play((Animator)HomeScreenUtils.getXPositionAnimator((View)this.activeMapIcon, HomeScreenResourceValues.activeRoadInfoIconX));
                play2.with((Animator)HomeScreenUtils.getXPositionAnimator(this.nextMapIconContainer, HomeScreenResourceValues.activeRoadInfoIconX));
                final ObjectAnimator xPositionAnimator3 = HomeScreenUtils.getXPositionAnimator((View)this.activeMapInstruction, HomeScreenResourceValues.activeRoadInfoInstructionX);
                final ValueAnimator widthAnimator3 = HomeScreenUtils.getWidthAnimator((View)this.activeMapInstruction, HomeScreenResourceValues.activeRoadInfoInstructionWidth);
                play2.with((Animator)xPositionAnimator3);
                play2.with((Animator)widthAnimator3);
                final ObjectAnimator xPositionAnimator4 = HomeScreenUtils.getXPositionAnimator(this.nextMapInstructionContainer, HomeScreenResourceValues.activeRoadInfoInstructionX);
                final ValueAnimator widthAnimator4 = HomeScreenUtils.getWidthAnimator(this.nextMapInstructionContainer, HomeScreenResourceValues.activeRoadInfoInstructionWidth);
                play2.with((Animator)xPositionAnimator4);
                play2.with((Animator)widthAnimator4);
                if (this.lastManeuverState == HereManeuverDisplayBuilder.ManeuverState.NOW) {
                    play2.with((Animator)HomeScreenUtils.getFadeInAndScaleUpAnimator(this.nowIcon));
                }
                else {
                    play2.with((Animator)HomeScreenUtils.getAlphaAnimator((View)this.activeMapDistance, 1));
                }
                set2.addListener((Animator.AnimatorListener)new DefaultAnimationListener() {
                    @Override
                    public void onAnimationEnd(final Animator animator) {
                        if (TbtView.this.homeScreenView.isNavigationActive() && !TbtView.this.homeScreenView.isRecalculating() && TbtView.this.lastManeuverState != HereManeuverDisplayBuilder.ManeuverState.NOW) {
                            TbtView.this.progressBar.setVisibility(View.VISIBLE);
                            TbtView.this.activeMapDistance.setVisibility(View.VISIBLE);
                        }
                    }
                    
                    @Override
                    public void onAnimationStart(final Animator animator) {
                    }
                });
                animatorSet$Builder.with((Animator)set2);
                break;
            }
        }
    }
    
    public void init(final HomeScreenView homeScreenView) {
        this.homeScreenView = homeScreenView;
        this.bus.register(this);
    }
    
    protected void onFinishInflate() {
        this.logger = HomeScreenView.sLogger;
        super.onFinishInflate();
        ButterKnife.inject((View)this);
        final RemoteDeviceManager instance = RemoteDeviceManager.getInstance();
        this.bus = instance.getBus();
        this.uiStateManager = instance.getUiStateManager();
        this.setLayoutTransition(new LayoutTransition());
    }
    
    public void onPause() {
        if (!this.paused) {
            this.paused = true;
            this.logger.v("::onPause:tbt");
        }
    }
    
    public void onResume() {
        if (this.paused) {
            this.paused = false;
            this.logger.v("::onResume:tbt");
            final MapEvents.ManeuverDisplay lastEvent = this.lastEvent;
            if (lastEvent != null) {
                this.lastEvent = null;
                this.logger.v("::onResume:tbt updated last event");
                this.updateDisplay(lastEvent);
            }
        }
    }
    
    public void setView(final MainView.CustomAnimationMode lastMode) {
        this.lastMode = lastMode;
        final ViewGroup.MarginLayoutParams viewGroup$MarginLayoutParams = (ViewGroup.MarginLayoutParams)this.activeMapInstruction.getLayoutParams();
        final ViewGroup.MarginLayoutParams viewGroup$MarginLayoutParams2 = (ViewGroup.MarginLayoutParams)this.nextMapInstructionContainer.getLayoutParams();
        switch (lastMode) {
            case EXPAND:
                if (this.lastManeuverState == HereManeuverDisplayBuilder.ManeuverState.NOW) {
                    this.nowIcon.setScaleX(1.0f);
                    this.nowIcon.setScaleY(1.0f);
                    this.nowIcon.setAlpha(1.0f);
                }
                this.activeMapDistance.setX((float)HomeScreenResourceValues.activeRoadInfoDistanceX);
                this.activeMapIcon.setX((float)HomeScreenResourceValues.activeRoadInfoIconX);
                this.nextMapIconContainer.setX((float)HomeScreenResourceValues.activeRoadInfoIconX);
                this.activeMapInstruction.setX((float)HomeScreenResourceValues.activeRoadInfoInstructionX);
                this.nextMapInstructionContainer.setX((float)HomeScreenResourceValues.activeRoadInfoInstructionX);
                viewGroup$MarginLayoutParams.width = HomeScreenResourceValues.activeRoadInfoInstructionWidth;
                viewGroup$MarginLayoutParams2.width = HomeScreenResourceValues.activeRoadInfoInstructionWidth;
                break;
            case SHRINK_LEFT: {
                this.nowIcon.setScaleX(0.9f);
                this.nowIcon.setScaleY(0.9f);
                this.nowIcon.setAlpha(0.0f);
                this.activeMapDistance.setX((float)HomeScreenResourceValues.activeRoadInfoDistanceX);
                this.activeMapIcon.setX((float)HomeScreenResourceValues.activeRoadInfoIconShrinkLeftX);
                this.nextMapIconContainer.setX((float)HomeScreenResourceValues.activeRoadInfoIconShrinkLeftX);
                this.activeMapInstruction.setX((float)HomeScreenResourceValues.activeRoadInfoInstructionShrinkLeftX);
                this.nextMapInstructionContainer.setX((float)HomeScreenResourceValues.activeRoadInfoInstructionShrinkLeftX);
                final ViewGroup.MarginLayoutParams viewGroup$MarginLayoutParams3 = (ViewGroup.MarginLayoutParams)this.activeMapInstruction.getLayoutParams();
                final ViewGroup.MarginLayoutParams viewGroup$MarginLayoutParams4 = (ViewGroup.MarginLayoutParams)this.nextMapInstructionContainer.getLayoutParams();
                viewGroup$MarginLayoutParams3.width = HomeScreenResourceValues.activeRoadInfoInstructionShrinkWidth;
                viewGroup$MarginLayoutParams4.width = HomeScreenResourceValues.activeRoadInfoInstructionShrinkWidth;
                break;
            }
        }
    }
    
    public void showHideActiveDistance() {
        if (!this.uiStateManager.isMainUIShrunk()) {
            this.activeMapDistance.setVisibility(View.VISIBLE);
            this.progressBar.setVisibility(View.VISIBLE);
        }
        else {
            this.activeMapDistance.setVisibility(INVISIBLE);
            this.progressBar.setVisibility(INVISIBLE);
        }
    }
    
    @Subscribe
    public void updateDisplay(final MapEvents.ManeuverDisplay lastEvent) {
        if (this.homeScreenView.isNavigationActive()) {
            if (lastEvent.empty) {
                this.clearState();
            }
            else if (lastEvent.pendingTurn == null) {
                this.logger.v("null pending turn");
                this.lastEvent = null;
            }
            else {
                this.lastEvent = lastEvent;
                if (!this.paused) {
                    final boolean b = TextUtils.equals((CharSequence)lastEvent.pendingTurn, (CharSequence)this.lastTurnText) && TextUtils.equals((CharSequence)lastEvent.pendingRoad, (CharSequence)this.lastRoadText);
                    this.setDistance(lastEvent.distanceToPendingRoadText, lastEvent.maneuverState);
                    this.setProgressBar(lastEvent.maneuverState, lastEvent.distanceInMeters);
                    Object setIcon = this.setIcon(lastEvent.turnIconId, b);
                    final AnimatorSet setInstruction = this.setInstruction(lastEvent.pendingTurn, lastEvent.pendingRoad);
                    final AnimatorSet set = null;
                    if (setIcon != null && setInstruction != null) {
                        final AnimatorSet set2 = new AnimatorSet();
                        set2.play((Animator)setIcon).with((Animator)setInstruction);
                        setIcon = set2;
                    }
                    else if (setIcon == null) {
                        setIcon = set;
                        if (setInstruction != null) {
                            setIcon = setInstruction;
                        }
                    }
                    if (setIcon != null) {
                        ((AnimatorSet)setIcon).setDuration(500L).setInterpolator(TbtView.maneuverSwapInterpolator);
                        ((AnimatorSet)setIcon).start();
                    }
                    this.lastManeuverState = lastEvent.maneuverState;
                }
            }
        }
    }
}
