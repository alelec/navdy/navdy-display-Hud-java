package com.navdy.hud.app.ui.component.vlist;

import android.util.AttributeSet;
import android.content.Context;
import android.support.v7.widget.RecyclerView;

public class VerticalRecyclerView extends RecyclerView
{
    public VerticalRecyclerView(final Context context) {
        super(context);
    }
    
    public VerticalRecyclerView(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    public VerticalRecyclerView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
    }
}
