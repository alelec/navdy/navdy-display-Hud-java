package com.navdy.hud.app.ui.component;

import android.animation.Animator;
import android.graphics.Paint;
import android.graphics.Canvas;
import android.content.res.TypedArray;
import android.animation.ValueAnimator;
import android.animation.TimeInterpolator;
import com.navdy.hud.app.R;
import android.animation.Animator;
import android.os.Looper;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.Paint;
import android.view.animation.LinearInterpolator;
import android.os.Handler;
import android.animation.AnimatorSet;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import android.animation.ValueAnimator;
import android.view.View;

public class FluctuatorAnimatorView extends View
{
    private ValueAnimator alphaAnimator;
    private int animationDelay;
    private int animationDuration;
    private DefaultAnimationListener animationListener;
    private AnimatorSet animatorSet;
    float currentCircle;
    private float endRadius;
    private int fillColor;
    private boolean fillEnabled;
    private Handler handler;
    private LinearInterpolator interpolator;
    private Paint paint;
    private ValueAnimator radiusAnimator;
    private float startRadius;
    public Runnable startRunnable;
    boolean started;
    private int strokeColor;
    private float strokeWidth;
    
    public FluctuatorAnimatorView(final Context context) {
        this(context, null);
    }
    
    public FluctuatorAnimatorView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public FluctuatorAnimatorView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.interpolator = new LinearInterpolator();
        this.handler = new Handler(Looper.getMainLooper());
        this.animationListener = new DefaultAnimationListener() {
            @Override
            public void onAnimationEnd(final Animator animator) {
                FluctuatorAnimatorView.this.handler.postDelayed(FluctuatorAnimatorView.this.startRunnable, (long)FluctuatorAnimatorView.this.animationDelay);
            }
        };
        this.startRunnable = new Runnable() {
            @Override
            public void run() {
                FluctuatorAnimatorView.this.animatorSet.start();
            }
        };
        this.started = false;
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.FluctuatorAnimatorView, n, 0);
        if (obtainStyledAttributes != null) {
            this.strokeColor = obtainStyledAttributes.getColor(0, -1);
            this.fillColor = obtainStyledAttributes.getColor(1, -1);
            this.startRadius = obtainStyledAttributes.getDimension(2, 0.0f);
            this.endRadius = obtainStyledAttributes.getDimension(3, 0.0f);
            this.strokeWidth = obtainStyledAttributes.getDimension(4, 0.0f);
            this.animationDuration = obtainStyledAttributes.getInteger(5, 0);
            this.animationDelay = obtainStyledAttributes.getInteger(6, 0);
            obtainStyledAttributes.recycle();
        }
        (this.paint = new Paint()).setStrokeWidth(this.strokeWidth);
        this.paint.setAntiAlias(true);
        (this.animatorSet = new AnimatorSet()).setDuration((long)this.animationDuration);
        this.radiusAnimator = ValueAnimator.ofFloat(new float[] { this.startRadius, this.endRadius });
        this.alphaAnimator = ValueAnimator.ofFloat(new float[] { 1.0f, 0.0f });
        this.animatorSet.setInterpolator((TimeInterpolator)this.interpolator);
        this.radiusAnimator.addUpdateListener((ValueAnimator.AnimatorUpdateListener)new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                FluctuatorAnimatorView.this.currentCircle = (float)valueAnimator.getAnimatedValue();
                FluctuatorAnimatorView.this.invalidate();
            }
        });
        this.alphaAnimator.addUpdateListener((ValueAnimator.AnimatorUpdateListener)new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                FluctuatorAnimatorView.this.setAlpha((float)valueAnimator.getAnimatedValue());
            }
        });
        this.animatorSet.playTogether(new Animator[] { this.radiusAnimator, this.alphaAnimator });
    }
    
    public boolean isStarted() {
        return this.started;
    }
    
    public void onDraw(final Canvas canvas) {
        super.onDraw(canvas);
        if (this.fillColor != -1 || this.fillEnabled) {
            this.paint.setStyle(Paint.Style.FILL);
            this.paint.setColor(this.fillColor);
            canvas.drawCircle((float)(this.getWidth() / 2), (float)(this.getHeight() / 2), this.currentCircle, this.paint);
        }
        this.paint.setStyle(Paint.Style.STROKE);
        this.paint.setColor(this.strokeColor);
        canvas.drawCircle((float)(this.getWidth() / 2), (float)(this.getHeight() / 2), this.currentCircle, this.paint);
    }
    
    public void setFillColor(final int fillColor) {
        this.fillColor = fillColor;
        this.fillEnabled = true;
    }
    
    public void setStrokeColor(final int strokeColor) {
        this.strokeColor = strokeColor;
    }
    
    public void start() {
        this.stop();
        this.animatorSet.addListener((Animator.AnimatorListener)this.animationListener);
        this.animatorSet.start();
        this.started = true;
    }
    
    public void stop() {
        this.handler.removeCallbacks(this.startRunnable);
        if (this.animatorSet.isRunning()) {
            this.animatorSet.removeAllListeners();
            this.animatorSet.cancel();
        }
        this.started = false;
    }
}
