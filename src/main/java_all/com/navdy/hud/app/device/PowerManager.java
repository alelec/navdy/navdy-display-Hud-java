package com.navdy.hud.app.device;

import com.navdy.hud.app.event.Wakeup;
import com.squareup.otto.Subscribe;
import android.view.KeyEvent;
import com.navdy.hud.app.device.light.HUDLightUtils;
import com.navdy.hud.app.device.light.LightManager;
import android.annotation.SuppressLint;
import com.navdy.hud.app.device.light.LED;
import com.navdy.hud.app.device.dial.DialManager;
import java.io.IOException;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.service.library.util.IOUtils;
import android.os.SystemClock;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.event.Shutdown;
import com.navdy.service.library.task.TaskManager;
import android.os.Looper;
import android.content.Context;
import java.util.concurrent.TimeUnit;
import com.navdy.hud.app.util.os.SystemProperties;
import android.content.SharedPreferences;
import java.lang.reflect.Method;
import android.os.Handler;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;

public class PowerManager
{
    public static final String BOOT_POWER_MODE;
    private static final String COOLING_DEVICE = "/sys/devices/virtual/thermal/cooling_device0/cur_state";
    private static final long COOLING_MONITOR_INTERVAL;
    private static final long DEFAULT_INSTANT_ON_TIMEOUT;
    private static final String INSTANT_ON = "persist.sys.instanton";
    private static final long INSTANT_ON_TIMEOUT;
    private static final String INSTANT_ON_TIMEOUT_PROPERTRY = "persist.sys.instanton.timeout";
    public static final String LAST_LOW_VOLTAGE_EVENT = "last_low_voltage_event";
    public static final String NORMAL_MODE = "normal";
    public static final String POWER_MODE_PROPERTY = "sys.power.mode";
    public static final String QUIET_MODE = "quiet";
    public static final long RECHARGE_TIME;
    private static final Logger sLogger;
    private boolean awake;
    private Bus bus;
    private Runnable checkCoolingState;
    private Handler handler;
    private long lastLowVoltage;
    private Method mIPowerManagerShutdownMethod;
    private Object mPowerManager;
    private String oldCoolingState;
    private android.os.PowerManager powerManager;
    private SharedPreferences preferences;
    private Runnable quietModeTimeout;
    private RunState runState;
    
    static {
        sLogger = new Logger(PowerManager.class);
        BOOT_POWER_MODE = SystemProperties.get("sys.power.mode");
        COOLING_MONITOR_INTERVAL = TimeUnit.SECONDS.toMillis(15L);
        DEFAULT_INSTANT_ON_TIMEOUT = TimeUnit.HOURS.toMillis(4L);
        INSTANT_ON_TIMEOUT = SystemProperties.getLong("persist.sys.instanton.timeout", PowerManager.DEFAULT_INSTANT_ON_TIMEOUT);
        RECHARGE_TIME = TimeUnit.DAYS.toMillis(7L);
    }
    
    public PowerManager(final Bus bus, final Context context, final SharedPreferences preferences) {
        this.awake = !"quiet".equals(PowerManager.BOOT_POWER_MODE);
        this.handler = new Handler(Looper.getMainLooper());
        this.checkCoolingState = new Runnable() {
            @Override
            public void run() {
                TaskManager.getInstance().execute(new Runnable() {
                    @Override
                    public void run() {
                        if (PowerManager.this.updateCoolingState()) {
                            PowerManager.this.handler.postDelayed(PowerManager.this.checkCoolingState, PowerManager.COOLING_MONITOR_INTERVAL);
                        }
                    }
                }, 10);
            }
        };
        this.quietModeTimeout = new Runnable() {
            @Override
            public void run() {
                PowerManager.sLogger.i("Quiet mode has timed out - forcing full shutdown");
                PowerManager.this.bus.post(new Shutdown(Shutdown.Reason.TIMEOUT));
            }
        };
        this.mIPowerManagerShutdownMethod = null;
        this.mPowerManager = null;
        (this.bus = bus).register(this);
        this.powerManager = (android.os.PowerManager)context.getSystemService("power");
        this.preferences = preferences;
        this.lastLowVoltage = preferences.getLong("last_low_voltage_event", -1L);
        this.setupShutdown();
        String s;
        if (this.inQuietMode()) {
            s = "quiet";
        }
        else {
            s = "normal";
        }
        SystemProperties.set("sys.power.mode", s);
        if (!this.inQuietMode()) {
            this.startOverheatMonitoring(RunState.Booting);
        }
        PowerManager.sLogger.i("quietMode:" + this.inQuietMode());
    }
    
    private void androidSleep() {
        PowerManager.sLogger.d("androidSleep()");
        ToastManager.getInstance().disableToasts(true);
        try {
            android.os.PowerManager.class.getMethod("goToSleep", Long.TYPE).invoke(this.powerManager, SystemClock.uptimeMillis());
        }
        catch (Exception ex) {
            PowerManager.sLogger.e("error invoking PowerManager.goToSleep(): " + ex);
        }
    }
    
    private void androidWakeup() {
        ToastManager.getInstance().disableToasts(false);
        try {
            android.os.PowerManager.class.getMethod("wakeUp", Long.TYPE).invoke(this.powerManager, SystemClock.uptimeMillis());
        }
        catch (Exception ex) {
            PowerManager.sLogger.e("error invoking PowerManager.wakeUp(): " + ex);
        }
    }
    
    public static boolean isAwake() {
        return !SystemProperties.get("sys.power.mode", "").equals("quiet");
    }
    
    private boolean isCoolingActive(final String s) {
        return s != null && !s.equals("0");
    }
    
    private void setupShutdown() {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: invokestatic    java/lang/Class.forName:(Ljava/lang/String;)Ljava/lang/Class;
        //     6: ldc_w           "getService"
        //     9: iconst_1       
        //    10: anewarray       Ljava/lang/Class;
        //    13: dup            
        //    14: iconst_0       
        //    15: ldc             Ljava/lang/String;.class
        //    17: aastore        
        //    18: invokevirtual   java/lang/Class.getDeclaredMethod:(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
        //    21: aconst_null    
        //    22: iconst_1       
        //    23: anewarray       Ljava/lang/Object;
        //    26: dup            
        //    27: iconst_0       
        //    28: ldc             "power"
        //    30: aastore        
        //    31: invokevirtual   java/lang/reflect/Method.invoke:(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
        //    34: checkcast       Landroid/os/IBinder;
        //    37: astore_1       
        //    38: aload_0        
        //    39: ldc_w           "android.os.IPowerManager$Stub"
        //    42: invokestatic    java/lang/Class.forName:(Ljava/lang/String;)Ljava/lang/Class;
        //    45: ldc_w           "asInterface"
        //    48: iconst_1       
        //    49: anewarray       Ljava/lang/Class;
        //    52: dup            
        //    53: iconst_0       
        //    54: ldc_w           Landroid/os/IBinder;.class
        //    57: aastore        
        //    58: invokevirtual   java/lang/Class.getDeclaredMethod:(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
        //    61: aconst_null    
        //    62: iconst_1       
        //    63: anewarray       Ljava/lang/Object;
        //    66: dup            
        //    67: iconst_0       
        //    68: aload_1        
        //    69: aastore        
        //    70: invokevirtual   java/lang/reflect/Method.invoke:(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
        //    73: putfield        com/navdy/hud/app/device/PowerManager.mPowerManager:Ljava/lang/Object;
        //    76: aload_0        
        //    77: ldc_w           "android.os.IPowerManager"
        //    80: invokestatic    java/lang/Class.forName:(Ljava/lang/String;)Ljava/lang/Class;
        //    83: ldc_w           "shutdown"
        //    86: iconst_2       
        //    87: anewarray       Ljava/lang/Class;
        //    90: dup            
        //    91: iconst_0       
        //    92: getstatic       java/lang/Boolean.TYPE:Ljava/lang/Class;
        //    95: aastore        
        //    96: dup            
        //    97: iconst_1       
        //    98: getstatic       java/lang/Boolean.TYPE:Ljava/lang/Class;
        //   101: aastore        
        //   102: invokevirtual   java/lang/Class.getDeclaredMethod:(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
        //   105: putfield        com/navdy/hud/app/device/PowerManager.mIPowerManagerShutdownMethod:Ljava/lang/reflect/Method;
        //   108: return         
        //   109: astore_1       
        //   110: getstatic       com/navdy/hud/app/device/PowerManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   113: ldc_w           "exception invoking IPowerManager.Stub.asInterface()"
        //   116: aload_1        
        //   117: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   120: goto            108
        //   123: astore_1       
        //   124: getstatic       com/navdy/hud/app/device/PowerManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   127: ldc_w           "exception getting IPowerManager.shutdown() method"
        //   130: aload_1        
        //   131: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   134: goto            108
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  0      76     109    123    Ljava/lang/Exception;
        //  76     108    123    137    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0108:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private void startOverheatMonitoring(final RunState runState) {
        this.runState = runState;
        this.handler.removeCallbacks(this.checkCoolingState);
        this.handler.post(this.checkCoolingState);
    }
    
    private boolean updateCoolingState() {
        try {
            final String trim = IOUtils.convertFileToString("/sys/devices/virtual/thermal/cooling_device0/cur_state").trim();
            if (!trim.equals(this.oldCoolingState)) {
                PowerManager.sLogger.i("Cooling state changed from:" + this.oldCoolingState + " to:" + trim);
                if (!this.isCoolingActive(this.oldCoolingState) && this.isCoolingActive(trim)) {
                    AnalyticsSupport.recordCpuOverheat(this.runState.name());
                }
                this.oldCoolingState = trim;
            }
            this.runState = RunState.Running;
            return true;
        }
        catch (IOException ex) {
            PowerManager.sLogger.e("Failed to read cooling device state - stopping monitoring", ex);
            return false;
        }
    }
    
    @SuppressLint({ "ApplySharedPref" })
    public void androidShutdown(final Shutdown.Reason reason, final boolean b) {
        boolean b2 = true;
        PowerManager.sLogger.d("androidShutdown: " + reason + " forceFullShutdown:" + b);
        DialManager.getInstance().requestDialReboot(false);
        if (reason == Shutdown.Reason.LOW_VOLTAGE || reason == Shutdown.Reason.CRITICAL_VOLTAGE) {
            this.preferences.edit().putLong("last_low_voltage_event", System.currentTimeMillis()).commit();
        }
        while (true) {
            while (true) {
                Label_0088: {
                    if (!b) {
                        break Label_0088;
                    }
                    Label_0151: {
                        break Label_0151;
                        while (true) {
                            try {
                                this.mIPowerManagerShutdownMethod.invoke(this.mPowerManager, false, false);
                                return;
                                this.powerManager.reboot("quiet");
                                return;
                                b2 = false;
                                break;
                            }
                            catch (Exception ex) {
                                PowerManager.sLogger.e("exception invoking IPowerManager.shutdown()", ex);
                            }
                            return;
                        }
                    }
                }
                AnalyticsSupport.recordShutdown(reason, b2);
                LED.writeToSysfs("0", "/sys/dlpc/led_enable");
                if (this.mPowerManager == null || this.mIPowerManagerShutdownMethod == null) {
                    PowerManager.sLogger.e("shutdown was not properly initialized");
                    return;
                }
                if (b) {
                    continue;
                }
                break;
            }
            continue;
        }
    }
    
    public void enterSleepMode() {
        this.handler.postDelayed(this.quietModeTimeout, PowerManager.INSTANT_ON_TIMEOUT);
        HUDLightUtils.turnOffFrontLED(LightManager.getInstance());
        this.androidSleep();
    }
    
    public boolean inQuietMode() {
        return !this.awake;
    }
    
    @Subscribe
    public void onKey(final KeyEvent keyEvent) {
        this.wakeUp(AnalyticsSupport.WakeupReason.DIAL);
    }
    
    public boolean quietModeEnabled() {
        boolean b = true;
        final long n = System.currentTimeMillis() - this.lastLowVoltage;
        boolean b2;
        if (this.lastLowVoltage != -1L && n < PowerManager.RECHARGE_TIME) {
            b2 = true;
        }
        else {
            b2 = false;
        }
        if (b2) {
            PowerManager.sLogger.d("disabling quiet mode since we had a low voltage event " + TimeUnit.MILLISECONDS.toHours(n) + " hours ago");
        }
        if (!SystemProperties.getBoolean("persist.sys.instanton", true) || b2) {
            b = false;
        }
        return b;
    }
    
    public void wakeUp(final AnalyticsSupport.WakeupReason wakeupReason) {
        if (!this.awake) {
            PowerManager.sLogger.d("waking up");
            this.awake = true;
            SystemProperties.set("sys.power.mode", "normal");
            this.handler.removeCallbacks(this.quietModeTimeout);
            HUDLightUtils.resetFrontLED(LightManager.getInstance());
            LED.writeToSysfs("1", "/sys/dlpc/led_enable");
            this.androidWakeup();
            this.startOverheatMonitoring(RunState.Waking);
            this.bus.post(new Wakeup(wakeupReason));
        }
    }
    
    private enum RunState
    {
        Booting, 
        Running, 
        Unknown, 
        Waking;
    }
}
