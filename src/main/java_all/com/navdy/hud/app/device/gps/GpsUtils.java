package com.navdy.hud.app.device.gps;

import android.os.Process;
import com.navdy.hud.app.HudApplication;
import android.content.Intent;
import android.os.Bundle;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.config.BooleanSetting;

public class GpsUtils
{
    public static BooleanSetting SHOW_RAW_GPS;
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(GpsUtils.class);
        GpsUtils.SHOW_RAW_GPS = new BooleanSetting("Show Raw GPS", BooleanSetting.Scope.NEVER, "map.raw_gps", "Add map indicators showing raw and map matched GPS");
    }
    
    public static HeadingDirection getHeadingDirection(final double n) {
        HeadingDirection n2 = HeadingDirection.N;
        if (n >= 0.0) {
            if (n > 360.0) {
                n2 = n2;
            }
            else {
                n2 = HeadingDirection.values()[(int)((float)((22.5 + n) % 360.0) / 45.0f)];
            }
        }
        return n2;
    }
    
    public static boolean isDebugRawGpsPosEnabled() {
        return GpsUtils.SHOW_RAW_GPS.isEnabled();
    }
    
    public static void sendEventBroadcast(final String s, final Bundle bundle) {
        try {
            final Intent intent = new Intent(s);
            if (bundle != null) {
                intent.putExtras(bundle);
            }
            HudApplication.getAppContext().sendBroadcastAsUser(intent, Process.myUserHandle());
        }
        catch (Throwable t) {
            GpsUtils.sLogger.e(t);
        }
    }
    
    public static class GpsSatelliteData
    {
        public Bundle data;
        
        public GpsSatelliteData(final Bundle data) {
            this.data = data;
        }
    }
    
    public static class GpsSwitch
    {
        public boolean usingPhone;
        public boolean usingUblox;
        
        public GpsSwitch(final boolean usingPhone, final boolean usingUblox) {
            this.usingPhone = usingPhone;
            this.usingUblox = usingUblox;
        }
        
        @Override
        public String toString() {
            return "GpsSwitch [UsingPhone :" + this.usingPhone + ", UsingUblox : " + this.usingUblox + "]";
        }
    }
    
    public enum HeadingDirection
    {
        E, 
        N, 
        NE, 
        NW, 
        S, 
        SE, 
        SW, 
        W;
    }
}
