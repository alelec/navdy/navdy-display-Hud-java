package com.navdy.hud.app.device.gps;

import android.os.Message;
import java.util.Iterator;
import java.util.Map;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import java.util.HashMap;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import android.os.Handler;

public class GpsNmeaParser
{
    private static final String COLON = ":";
    private static final String COMMA = ",";
    private static final char COMMA_CHAR = ',';
    private static final String DB = "db";
    private static final String EQUAL = "=";
    public static final int FIX_TYPE_2D_3D = 1;
    public static final int FIX_TYPE_DIFFERENTIAL = 2;
    public static final int FIX_TYPE_DR = 6;
    public static final int FIX_TYPE_FRTK = 5;
    public static final int FIX_TYPE_NONE = 0;
    public static final int FIX_TYPE_PPS = 3;
    public static final int FIX_TYPE_RTK = 4;
    private static final String GGA = "GGA";
    private static final String GLL = "GLL";
    private static final String GNS = "GNS";
    private static final String GSV = "GSV";
    private static final String RMC = "RMC";
    private static final int SATELLITE_REPORT_INTERVAL = 5000;
    private static final String SPACE = " ";
    private static final String SVID = "SV-";
    private static final String VTG = "VTG";
    private int fixType;
    private Handler handler;
    private long lastSatelliteReportTime;
    private int messageCount;
    private ArrayList<String> messages;
    private String[] nmeaResult;
    private Logger sLogger;
    private HashMap<String, HashMap<Integer, Integer>> satellitesSeen;
    private HashMap<String, Integer> satellitesUsed;
    
    GpsNmeaParser(final Logger sLogger, final Handler handler) {
        this.satellitesSeen = new HashMap<String, HashMap<Integer, Integer>>();
        this.satellitesUsed = new HashMap<String, Integer>();
        this.messages = new ArrayList<String>();
        this.nmeaResult = new String[200];
        this.sLogger = sLogger;
        this.handler = handler;
    }
    
    private String getGnssProviderName(String s) {
        switch (s) {
            default:
                s = "UNK";
                break;
            case "GP":
                s = "GPS";
                break;
            case "GL":
                s = "GLONASS";
                break;
            case "GA":
                s = "Galileo";
                break;
            case "GB":
                s = "BeiDou";
                break;
            case "GN":
                s = "MultiGNSS";
                break;
        }
        return s;
    }
    
    static NmeaMessage getMessageType(final char c, final char c2, final char c3) {
        NmeaMessage nmeaMessage = null;
        switch (c) {
            default:
                nmeaMessage = NmeaMessage.NOT_SUPPORTED;
                break;
            case 'G':
                if (c2 == 'S' && c3 == 'V') {
                    nmeaMessage = NmeaMessage.GSV;
                    break;
                }
                if (c2 == 'G' && c3 == 'A') {
                    nmeaMessage = NmeaMessage.GGA;
                    break;
                }
                if (c2 == 'L' && c3 == 'L') {
                    nmeaMessage = NmeaMessage.GLL;
                    break;
                }
                if (c2 == 'N' && c3 == 'S') {
                    nmeaMessage = NmeaMessage.GNS;
                    break;
                }
                nmeaMessage = NmeaMessage.NOT_SUPPORTED;
                break;
            case 'R':
                if (c2 == 'M' && c3 == 'C') {
                    nmeaMessage = NmeaMessage.RMC;
                    break;
                }
                nmeaMessage = NmeaMessage.NOT_SUPPORTED;
                break;
            case 'V':
                if (c2 == 'T' && c3 == 'G') {
                    nmeaMessage = NmeaMessage.VTG;
                    break;
                }
                nmeaMessage = NmeaMessage.NOT_SUPPORTED;
                break;
        }
        return nmeaMessage;
    }
    
    static int parseData(final String s, final String[] array) {
        int n = 0;
        int n2 = 0;
        char[] charArray;
        int i;
        int n3;
        int n4;
        for (charArray = s.toCharArray(), i = 0; i < charArray.length; ++i, n = n3, n2 = n4) {
            n3 = n;
            n4 = n2;
            if (charArray[i] == ',') {
                array[n] = new String(charArray, n2, i - n2);
                n4 = i + 1;
                n3 = n + 1;
            }
        }
        array[n] = new String(charArray, n2, i - n2 - 2);
        return n + 1;
    }
    
    private void processGGA(String s) {
        parseData(s, this.nmeaResult);
        s = this.nmeaResult[6];
        try {
            this.fixType = Integer.parseInt(s);
        }
        catch (Throwable t) {
            this.fixType = 0;
        }
    }
    
    private void processGLL(final String s) {
    }
    
    private void processGNS(String s) {
        parseData(s, this.nmeaResult);
        final String substring = this.nmeaResult[0].substring(1, 3);
        s = this.nmeaResult[2];
        final String s2 = this.nmeaResult[4];
        final String s3 = this.nmeaResult[7];
        if (TextUtils.isEmpty((CharSequence)s) || TextUtils.isEmpty((CharSequence)s2) || TextUtils.isEmpty((CharSequence)s3)) {
            this.satellitesUsed.put(substring, 0);
        }
        else {
            this.satellitesUsed.put(substring, Integer.parseInt(s3));
        }
    }
    
    private void processGSV(String s) {
        while (true) {
            if (parseData(s, this.nmeaResult) >= 8) {
                final String substring = this.nmeaResult[0].substring(1, 3);
                final int int1 = Integer.parseInt(this.nmeaResult[1]);
                final int int2 = Integer.parseInt(this.nmeaResult[2]);
                if (int2 == 1) {
                    this.messages.clear();
                    this.messageCount = int1;
                }
                else if (this.messages.size() == 0) {
                    return;
                }
                this.messages.add(s);
                if (int2 == this.messageCount) {
                    s = (String)this.satellitesSeen.get(substring);
                    if (s == null) {
                        s = (String)new HashMap();
                        this.satellitesSeen.put(substring, (HashMap<Integer, Integer>)s);
                    }
                    else {
                        ((HashMap)s).clear();
                    }
                    for (int size = this.messages.size(), i = 0; i < size; ++i) {
                        for (int data = parseData(this.messages.get(i), this.nmeaResult), n = 4; n + 4 < data; n += 4) {
                            final String s2 = this.nmeaResult[n];
                            final String s3 = this.nmeaResult[n + 1];
                            final String s4 = this.nmeaResult[n + 2];
                            final String s5 = this.nmeaResult[n + 3];
                            if (!TextUtils.isEmpty((CharSequence)s2)) {
                                if (TextUtils.isEmpty((CharSequence)s3) || TextUtils.isEmpty((CharSequence)s4) || TextUtils.isEmpty((CharSequence)s5)) {
                                    ((HashMap<Object, Object>)s).remove(s2);
                                }
                                else {
                                    try {
                                        final int int3 = Integer.parseInt(s2);
                                        final String s6 = s5;
                                        final int n2 = Integer.parseInt(s6);
                                        final String s7 = s;
                                        final int n3 = int3;
                                        final Integer n4 = n3;
                                        final int n5 = n2;
                                        final Integer n6 = n5;
                                        ((HashMap<Integer, Integer>)s7).put(n4, n6);
                                    }
                                    catch (NumberFormatException ex) {}
                                }
                            }
                        }
                    }
                    this.messageCount = 0;
                    this.messages.clear();
                    final long elapsedRealtime = SystemClock.elapsedRealtime();
                    if (elapsedRealtime - this.lastSatelliteReportTime >= 5000L) {
                        this.sendGpsSatelliteStatus();
                        this.lastSatelliteReportTime = elapsedRealtime;
                    }
                }
            }
            return;
            try {
                final String s5;
                final String s6 = s5;
                final int n2 = Integer.parseInt(s6);
                final String s7 = s;
                final int int3;
                final int n3 = int3;
                final Integer n4 = n3;
                final int n5 = n2;
                final Integer n6 = n5;
                ((HashMap<Integer, Integer>)s7).put(n4, n6);
            }
            catch (NumberFormatException ex2) {}
            continue;
        }
    }
    
    private void processRMC(final String s) {
    }
    
    private void processVTG(final String s) {
    }
    
    private void sendGpsSatelliteStatus() {
        final Bundle obj = new Bundle();
        int n = 0;
        int n2 = 0;
        for (final Map.Entry<String, HashMap<Integer, Integer>> entry : this.satellitesSeen.entrySet()) {
            final String s = entry.getKey();
            final Iterator<Map.Entry<Integer, Integer>> iterator2 = entry.getValue().entrySet().iterator();
            int n3 = n2;
            int n4 = n;
            while (true) {
                n = n4;
                n2 = n3;
                if (!iterator2.hasNext()) {
                    break;
                }
                final Map.Entry<Integer, Integer> entry2 = iterator2.next();
                final int intValue = entry2.getKey();
                final int intValue2 = entry2.getValue();
                final int n5 = n4 + 1;
                obj.putString("SAT_PROVIDER_" + n5, s);
                obj.putInt("SAT_ID_" + n5, intValue);
                obj.putInt("SAT_DB_" + n5, intValue2);
                n4 = n5;
                if (intValue2 <= n3) {
                    continue;
                }
                n3 = intValue2;
                n4 = n5;
            }
        }
        obj.putInt("SAT_SEEN", n);
        obj.putInt("SAT_MAX_DB", n2);
        int n6 = 0;
        final Iterator<Map.Entry<String, Integer>> iterator3 = this.satellitesUsed.entrySet().iterator();
        while (iterator3.hasNext()) {
            n6 += iterator3.next().getValue();
        }
        obj.putInt("SAT_USED", n6);
        obj.putInt("SAT_FIX_TYPE", this.fixType);
        final Message obtainMessage = this.handler.obtainMessage(2);
        obtainMessage.obj = obj;
        this.handler.sendMessage(obtainMessage);
    }
    
    public void parseNmeaMessage(final String s) {
        if (s != null) {
            final char[] charArray = s.toCharArray();
            if (charArray.length >= 7 && charArray[0] == '$') {
                final NmeaMessage messageType = getMessageType(charArray[3], charArray[4], charArray[5]);
                if (messageType != NmeaMessage.NOT_SUPPORTED) {
                    for (int i = 0; i < this.nmeaResult.length; ++i) {
                        this.nmeaResult[i] = null;
                    }
                    switch (messageType) {
                        case GSV:
                            this.processGSV(s);
                            break;
                        case GNS:
                            this.processGNS(s);
                            break;
                        case GGA:
                            this.processGGA(s);
                    }
                }
            }
        }
    }
    
    enum NmeaMessage
    {
        GGA, 
        GLL, 
        GNS, 
        GSV, 
        NOT_SUPPORTED, 
        RMC, 
        VTG;
    }
}
