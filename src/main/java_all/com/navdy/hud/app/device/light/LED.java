package com.navdy.hud.app.device.light;

import java.io.IOException;
import java.io.FileWriter;
import java.util.concurrent.Executors;
import java.io.File;
import android.graphics.Color;
import java.util.concurrent.ExecutorService;
import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicBoolean;
import com.navdy.service.library.log.Logger;

public class LED implements ILight
{
    private static final String BLINK_ATTRIBUTE = "blink";
    public static final int CLEAR_COLOR = 0;
    private static final String COLOR_ATTRIBUTE = "color";
    public static final int DEFAULT_COLOR;
    private static final Settings DEFAULT_SETTINGS;
    private static final String SLOPE_DOWN_ATTRUTE = "slope_down";
    private static final String SLOPE_UP_ATTRIBUTE = "slope_up";
    private static final Logger sLogger;
    private AtomicBoolean activityBlinkRunning;
    private String blinkPath;
    private String colorPath;
    private LinkedList<LightSettings> mLightSettingsStack;
    private ExecutorService mSerialExecutor;
    private String slopeDownPath;
    private String slopeUpPath;
    
    static {
        sLogger = new Logger(LED.class);
        DEFAULT_COLOR = Color.parseColor("#ff000000");
        DEFAULT_SETTINGS = new Builder().setName("DEFAULT").setColor(LED.DEFAULT_COLOR).setIsBlinking(false).build();
    }
    
    public LED(final String s) {
        this.activityBlinkRunning = new AtomicBoolean(false);
        if (new File(s).exists()) {
            this.colorPath = s + File.separator + "color";
            this.slopeUpPath = s + File.separator + "slope_up";
            this.slopeDownPath = s + File.separator + "slope_down";
            this.blinkPath = s + File.separator + "blink";
            this.mLightSettingsStack = new LinkedList<LightSettings>();
            this.mSerialExecutor = Executors.newSingleThreadExecutor();
            this.mLightSettingsStack.push(LED.DEFAULT_SETTINGS);
        }
        else {
            LED.sLogger.w("Unable to open led at " + s);
        }
    }
    
    private void activityBlink() {
        try {
            this.stopBlinking();
            this.setBlinkFrequency(BlinkFrequency.HIGH);
            this.startBlinking();
            try {
                Thread.sleep(BlinkFrequency.HIGH.blinkDelay / 2);
                this.stopBlinking();
            }
            catch (InterruptedException ex) {
                LED.sLogger.e("Interrupted exception while activity blinking");
            }
        }
        finally {
            this.activityBlinkRunning.set(false);
        }
    }
    
    private void applySettings(final LightSettings lightSettings) {
        this.mSerialExecutor.execute(new Runnable() {
            @Override
            public void run() {
                if (lightSettings instanceof Settings) {
                    final Settings settings = (Settings)lightSettings;
                    if (settings.getActivityBlink()) {
                        LED.this.activityBlink();
                    }
                    else if (settings.isTurnedOn()) {
                        LED.this.setColor(settings.getColor());
                        if (settings.isBlinking()) {
                            if (settings.isBlinkInfinite()) {
                                LED.this.setBlinkFrequency(settings.getBlinkFrequency());
                                LED.this.startBlinking();
                            }
                            else {
                                LED.this.startBlinking(settings.getBlinkPulseCount(), settings.getColor(), settings.getBlinkFrequency());
                            }
                        }
                        else {
                            LED.this.stopBlinking();
                        }
                    }
                    else {
                        LED.this.turnOff();
                    }
                }
            }
        });
    }
    
    private void setBlinkFrequency(final BlinkFrequency blinkFrequency) {
        if (blinkFrequency != null) {
            writeToKernelDevice(String.valueOf(blinkFrequency.getBlinkDelay()), this.slopeUpPath);
            writeToKernelDevice(String.valueOf(blinkFrequency.getBlinkDelay()), this.slopeDownPath);
        }
    }
    
    private void setColor(final int n) {
        writeToKernelDevice(String.format("%08x", n), this.colorPath);
    }
    
    private void startBlinking() {
        writeToKernelDevice("1", this.blinkPath);
    }
    
    private void startBlinking(final int n, final int color, final BlinkFrequency blinkFrequency) {
        this.stopBlinking();
        final int access$000 = blinkFrequency.blinkDelay;
        if (n > 0) {
            int i = 0;
        Label_0049_Outer:
            while (i < n) {
                while (true) {
                    try {
                        this.setColor(color);
                        Thread.sleep(access$000 / 2);
                        this.setColor(0);
                        Thread.sleep(access$000 / 2);
                        ++i;
                        continue Label_0049_Outer;
                    }
                    catch (InterruptedException ex) {
                        LED.sLogger.e("Interrupted exception while blinking");
                        continue;
                    }
                    break;
                }
                break;
            }
        }
    }
    
    private void stopBlinking() {
        if (LED.sLogger.isLoggable(3)) {
            LED.sLogger.d("Stop blinking");
        }
        writeToKernelDevice("0", this.blinkPath);
    }
    
    private static void writeToKernelDevice(final String s, final String s2) {
        try {
            final File file = new File(s2);
            try {
                final FileWriter fileWriter = new FileWriter(file);
                fileWriter.write(s);
                fileWriter.flush();
                fileWriter.close();
            }
            catch (IOException ex) {
                LED.sLogger.e("Error writing to the sysfsfile ", ex);
            }
        }
        catch (Exception ex2) {
            LED.sLogger.e("Exception while trying to write to the sysfs ");
        }
    }
    
    public static void writeToSysfs(final String s, final String s2) {
        LED.sLogger.d("writing '" + s + "' to sysfs file '" + s2 + "'");
        writeToKernelDevice(s, s2);
    }
    
    public boolean isAvailable() {
        return this.mLightSettingsStack != null;
    }
    
    @Override
    public void popSetting() {
        synchronized (this) {
            if (this.mLightSettingsStack.size() > 1) {
                final LightSettings lightSettings = this.mLightSettingsStack.pop();
                if (lightSettings != null) {
                    final Settings settings = (Settings)lightSettings;
                }
            }
            this.applySettings(this.mLightSettingsStack.peek());
        }
    }
    
    @Override
    public void pushSetting(final LightSettings lightSettings) {
        synchronized (this) {
            this.mLightSettingsStack.push(lightSettings);
            this.applySettings(lightSettings);
        }
    }
    
    @Override
    public void removeSetting(final LightSettings lightSettings) {
        synchronized (this) {
            if (this.mLightSettingsStack.peek() == lightSettings) {
                this.popSetting();
            }
            else {
                this.mLightSettingsStack.remove(lightSettings);
            }
        }
    }
    
    @Override
    public void reset() {
        synchronized (this) {
            this.mLightSettingsStack.clear();
            this.pushSetting(LED.DEFAULT_SETTINGS);
        }
    }
    
    public void startActivityBlink() {
        if (this.activityBlinkRunning.compareAndSet(false, true)) {
            final Settings build = new Builder().setName("ActivityBlink").setActivityBlink(true).build();
            this.pushSetting(build);
            this.removeSetting(build);
        }
    }
    
    @Override
    public void turnOff() {
        synchronized (this) {
            this.stopBlinking();
            this.setColor(0);
        }
    }
    
    @Override
    public void turnOn() {
        synchronized (this) {
            this.stopBlinking();
            this.setColor(LED.DEFAULT_COLOR);
        }
    }
    
    enum BlinkFrequency
    {
        HIGH(100), 
        LOW(1000);
        
        private int blinkDelay;
        
        private BlinkFrequency(final int blinkDelay) {
            this.blinkDelay = blinkDelay;
        }
        
        public int getBlinkDelay() {
            return this.blinkDelay;
        }
    }
    
    public static class Settings extends LightSettings
    {
        private boolean activityBlink;
        private BlinkFrequency blinkFrequency;
        private boolean blinkInfinite;
        private int blinkPulseCount;
        private String name;
        
        public Settings(final int n, final boolean b, final boolean b2, final BlinkFrequency blinkFrequency, final boolean blinkInfinite, final int blinkPulseCount, final boolean activityBlink, final String name) {
            super(n, b, b2);
            this.blinkFrequency = blinkFrequency;
            this.blinkInfinite = blinkInfinite;
            this.blinkPulseCount = blinkPulseCount;
            this.activityBlink = activityBlink;
            this.name = name;
        }
        
        public boolean getActivityBlink() {
            return this.activityBlink;
        }
        
        public BlinkFrequency getBlinkFrequency() {
            return this.blinkFrequency;
        }
        
        public int getBlinkPulseCount() {
            return this.blinkPulseCount;
        }
        
        public boolean isBlinkInfinite() {
            return this.blinkInfinite;
        }
        
        public static class Builder
        {
            private boolean activityBlink;
            private BlinkFrequency blinkFrequency;
            private boolean blinkInfinite;
            private int blinkPulseCount;
            private int color;
            private boolean isBlinking;
            private boolean isTurnedOn;
            private String name;
            
            public Builder() {
                this.color = LED.DEFAULT_COLOR;
                this.isTurnedOn = true;
                this.isBlinking = true;
                this.blinkFrequency = BlinkFrequency.LOW;
                this.blinkInfinite = true;
                this.blinkPulseCount = 0;
                this.activityBlink = false;
                this.name = "UNKNOWN";
            }
            
            public Settings build() {
                return new Settings(this.color, this.isTurnedOn, this.isBlinking, this.blinkFrequency, this.blinkInfinite, this.blinkPulseCount, this.activityBlink, this.name);
            }
            
            public Builder setActivityBlink(final boolean activityBlink) {
                this.activityBlink = activityBlink;
                return this;
            }
            
            public Builder setBlinkFrequency(final BlinkFrequency blinkFrequency) {
                this.blinkFrequency = blinkFrequency;
                return this;
            }
            
            public Builder setBlinkInfinite(final boolean blinkInfinite) {
                this.blinkInfinite = blinkInfinite;
                return this;
            }
            
            public Builder setBlinkPulseCount(final int blinkPulseCount) {
                this.blinkPulseCount = blinkPulseCount;
                return this;
            }
            
            public Builder setColor(final int color) {
                this.color = color;
                return this;
            }
            
            public Builder setIsBlinking(final boolean isBlinking) {
                this.isBlinking = isBlinking;
                return this;
            }
            
            public Builder setIsTurnedOn(final boolean isTurnedOn) {
                this.isTurnedOn = isTurnedOn;
                return this;
            }
            
            public Builder setName(final String name) {
                this.name = name;
                return this;
            }
        }
    }
}
