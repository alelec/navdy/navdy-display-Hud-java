package com.navdy.hud.app.device.gps;

import java.nio.ByteOrder;
import java.nio.ByteBuffer;

public class RawSensorData
{
    private static final float ACCEL_SCALE_FACTOR;
    public static final int ACCEL_X = 16;
    public static final int ACCEL_Y = 17;
    public static final int ACCEL_Z = 18;
    private static final float GYRO_SCALE_FACTOR;
    public static final int GYRO_TEMP = 12;
    public static final int GYRO_X = 13;
    public static final int GYRO_Y = 14;
    public static final int GYRO_Z = 5;
    public static final float MAX_ACCEL = 2.0f;
    public final float x;
    public final float y;
    public final float z;
    
    static {
        ACCEL_SCALE_FACTOR = (float)Math.pow(2.0, -10.0);
        GYRO_SCALE_FACTOR = (float)Math.pow(2.0, -12.0);
    }
    
    public RawSensorData(final byte[] array, int n) {
        final ByteBuffer wrap = ByteBuffer.wrap(array, n + 4, array.length - n - 4);
        wrap.order(ByteOrder.LITTLE_ENDIAN);
        n = (wrap.getShort() - 4) / 8;
        wrap.mark();
        this.x = this.clamp(this.average(wrap, n, 16, RawSensorData.ACCEL_SCALE_FACTOR) / 9.8f, -2.0f, 2.0f);
        this.y = this.clamp(this.average(wrap, n, 17, RawSensorData.ACCEL_SCALE_FACTOR) / 9.8f, -2.0f, 2.0f);
        this.z = this.clamp(this.average(wrap, n, 18, RawSensorData.ACCEL_SCALE_FACTOR) / 9.8f, -2.0f, 2.0f);
    }
    
    private float average(final ByteBuffer byteBuffer, final int n, final int n2, float n3) {
        byteBuffer.reset();
        byteBuffer.getInt();
        int n4 = 0;
        float n5 = 0.0f;
        int n7;
        float n8;
        for (int n6 = 0; n6 < n && byteBuffer.position() < byteBuffer.limit(); ++n6, n4 = n7, n5 = n8) {
            final int int1 = byteBuffer.getInt();
            byteBuffer.getInt();
            n7 = n4;
            n8 = n5;
            if (int1 >> 24 == n2) {
                int n10;
                final int n9 = n10 = (int1 & 0xFFFFFF);
                if ((0x800000 & n9) != 0x0) {
                    n10 = (n9 | 0xFF000000);
                }
                n8 = n5 + n10 * n3;
                n7 = n4 + 1;
            }
        }
        if (n4 > 0) {
            n3 = n5 / n4;
        }
        else {
            n3 = 0.0f;
        }
        return n3;
    }
    
    private float clamp(final float n, float n2, final float n3) {
        if (n >= n2) {
            n2 = n;
            if (n > n3) {
                n2 = n3;
            }
        }
        return n2;
    }
}
