package com.navdy.hud.app.device.gps;

import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0007\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0003HÆ\u0003J\t\u0010\r\u001a\u00020\u0003HÆ\u0003J'\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\bR\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\b¨\u0006\u0016" }, d2 = { "Lcom/navdy/hud/app/device/gps/CalibratedGForceData;", "", "xAccel", "", "yAccel", "zAccel", "(FFF)V", "getXAccel", "()F", "getYAccel", "getZAccel", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "", "toString", "", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
public final class CalibratedGForceData
{
    private final float xAccel;
    private final float yAccel;
    private final float zAccel;
    
    public CalibratedGForceData(final float xAccel, final float yAccel, final float zAccel) {
        this.xAccel = xAccel;
        this.yAccel = yAccel;
        this.zAccel = zAccel;
    }
    
    public final float component1() {
        return this.xAccel;
    }
    
    public final float component2() {
        return this.yAccel;
    }
    
    public final float component3() {
        return this.zAccel;
    }
    
    @NotNull
    public final CalibratedGForceData copy(final float n, final float n2, final float n3) {
        return new CalibratedGForceData(n, n2, n3);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this != o) {
            if (o instanceof CalibratedGForceData) {
                final CalibratedGForceData calibratedGForceData = (CalibratedGForceData)o;
                if (Float.compare(this.xAccel, calibratedGForceData.xAccel) == 0 && Float.compare(this.yAccel, calibratedGForceData.yAccel) == 0 && Float.compare(this.zAccel, calibratedGForceData.zAccel) == 0) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }
    
    public final float getXAccel() {
        return this.xAccel;
    }
    
    public final float getYAccel() {
        return this.yAccel;
    }
    
    public final float getZAccel() {
        return this.zAccel;
    }
    
    @Override
    public int hashCode() {
        return (Float.floatToIntBits(this.xAccel) * 31 + Float.floatToIntBits(this.yAccel)) * 31 + Float.floatToIntBits(this.zAccel);
    }
    
    @Override
    public String toString() {
        return "CalibratedGForceData(xAccel=" + this.xAccel + ", yAccel=" + this.yAccel + ", zAccel=" + this.zAccel + ")";
    }
}
