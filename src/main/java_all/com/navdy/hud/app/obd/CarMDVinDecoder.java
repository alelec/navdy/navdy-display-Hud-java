package com.navdy.hud.app.obd;

import okhttp3.Response;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Request;
import android.text.TextUtils;
import com.navdy.service.library.task.TaskManager;
import okio.BufferedSink;
import org.json.JSONException;
import org.json.JSONObject;
import com.navdy.hud.app.framework.network.NetworkStateManager;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import okio.BufferedSource;
import java.io.IOException;
import okio.Okio;
import okhttp3.ResponseBody;
import com.navdy.service.library.util.CredentialUtil;
import java.util.concurrent.TimeUnit;
import java.io.File;
import com.navdy.hud.app.storage.PathManager;
import okhttp3.internal.io.FileSystem;
import mortar.Mortar;
import com.navdy.hud.app.HudApplication;
import javax.inject.Inject;
import com.navdy.service.library.network.http.IHttpManager;
import okhttp3.OkHttpClient;
import okhttp3.internal.DiskLruCache;
import com.navdy.service.library.log.Logger;
import okhttp3.HttpUrl;

public class CarMDVinDecoder
{
    public static final int APP_VERSION = 1;
    private static final HttpUrl CAR_MD_API_URL;
    private static final String DATA = "data";
    public static final long MAX_SIZE = 5000L;
    private static final String META_CAR_MD_AUTH = "CAR_MD_AUTH";
    private static final String META_CAR_MD_TOKEN = "CAR_MD_TOKEN";
    public static final String VIN = "vin";
    private static final Logger sLogger;
    private final String CAR_MD_AUTH;
    private final String CAR_MD_TOKEN;
    private volatile boolean mDecoding;
    private ObdDeviceConfigurationManager mDeviceConfigurationManager;
    private DiskLruCache mDiskLruCache;
    private OkHttpClient mHttpClient;
    @Inject
    IHttpManager mHttpManager;
    
    static {
        CAR_MD_API_URL = HttpUrl.parse("https://api2.carmd.com/v2.0/decode");
        sLogger = new Logger(CarMDVinDecoder.class);
    }
    
    public CarMDVinDecoder(final ObdDeviceConfigurationManager mDeviceConfigurationManager) {
        this.mDecoding = false;
        Mortar.inject(HudApplication.getAppContext(), this);
        this.mDiskLruCache = DiskLruCache.create(FileSystem.SYSTEM, new File(PathManager.getInstance().getCarMdResponseDiskCacheFolder()), 1, 1, 5000L);
        this.mDeviceConfigurationManager = mDeviceConfigurationManager;
        this.mHttpClient = this.mHttpManager.getClientCopy().readTimeout(120L, TimeUnit.SECONDS).connectTimeout(120L, TimeUnit.SECONDS).build();
        this.CAR_MD_AUTH = CredentialUtil.getCredentials(HudApplication.getAppContext(), "CAR_MD_AUTH");
        this.CAR_MD_TOKEN = CredentialUtil.getCredentials(HudApplication.getAppContext(), "CAR_MD_TOKEN");
    }
    
    private String getFromCache(String utf8) {
        try {
            final DiskLruCache.Snapshot value = this.mDiskLruCache.get(utf8.toLowerCase());
            if (value == null) {
                utf8 = null;
            }
            else {
                final BufferedSource buffer = Okio.buffer(value.getSource(0));
                utf8 = buffer.readUtf8();
                buffer.close();
            }
            return utf8;
        }
        catch (IOException ex) {
            CarMDVinDecoder.sLogger.d("Exception while retrieving response from cache", ex);
            utf8 = null;
            return utf8;
        }
    }
    
    private boolean isConnected() {
        final boolean remoteDeviceConnected = RemoteDeviceManager.getInstance().isRemoteDeviceConnected();
        final boolean connectedToNetwork = NetworkStateManager.isConnectedToNetwork(HudApplication.getAppContext());
        return remoteDeviceConnected && connectedToNetwork;
    }
    
    private void parseResponse(final ResponseBody responseBody) {
        if (responseBody == null) {
            return;
        }
        while (true) {
            try {
                final JSONObject jsonObject = new JSONObject(responseBody.string()).getJSONObject("data");
                if (jsonObject != null) {
                    final String string = jsonObject.toString();
                    final CarDetails fromJson = CarDetails.fromJson(string);
                    if (fromJson != null) {
                        this.putToCache(fromJson.vin, string);
                        this.mDeviceConfigurationManager.setDecodedCarDetails(fromJson);
                    }
                    else {
                        CarMDVinDecoder.sLogger.e("Error parsing the CarMD response to car details");
                    }
                }
            }
            catch (JSONException ex) {
                CarMDVinDecoder.sLogger.e("Error parsing the response body ", (Throwable)ex);
            }
            catch (IOException ex2) {
                CarMDVinDecoder.sLogger.e("IO Error parsing the response body ", ex2);
            }
        }
    }
    
    private void putToCache(final String s, final String s2) {
        try {
            final DiskLruCache.Editor edit = this.mDiskLruCache.edit(s.toLowerCase());
            final BufferedSink buffer = Okio.buffer(edit.newSink(0));
            buffer.writeUtf8(s2);
            buffer.close();
            edit.commit();
        }
        catch (IOException ex) {
            CarMDVinDecoder.sLogger.e("Exception while putting into cache ", ex);
        }
    }
    
    public void decodeVin(final String s) {
        synchronized (this) {
            if (!this.mDecoding) {
                this.mDecoding = true;
                TaskManager.getInstance().execute(new Runnable() {
                    @Override
                    public void run() {
                        while (true) {
                            try {
                                final String access$000 = CarMDVinDecoder.this.getFromCache(s);
                                Label_0098: {
                                    if (TextUtils.isEmpty((CharSequence)access$000)) {
                                        break Label_0098;
                                    }
                                    CarMDVinDecoder.sLogger.d("Entry found in the cache for the vinNumber :" + s + ", Data :" + access$000);
                                    final CarDetails fromJson = CarDetails.fromJson(access$000);
                                    if (fromJson == null) {
                                        CarMDVinDecoder.sLogger.d("Error parsing the CarMD response to CarDetails");
                                        break Label_0098;
                                    }
                                    CarMDVinDecoder.this.mDeviceConfigurationManager.setDecodedCarDetails(fromJson);
                                    CarMDVinDecoder.this.mDecoding = false;
                                    return;
                                }
                                if (!CarMDVinDecoder.this.isConnected()) {
                                    CarMDVinDecoder.this.mDecoding = false;
                                    return;
                                }
                            }
                            catch (Exception ex) {
                                CarMDVinDecoder.sLogger.e("Exception while fetching the vin from CarMD");
                                CarMDVinDecoder.this.mDecoding = false;
                                return;
                            }
                            final HttpUrl.Builder builder = CarMDVinDecoder.CAR_MD_API_URL.newBuilder();
                            builder.addQueryParameter("vin", s);
                            final Request.Builder url = new Request.Builder().url(builder.build());
                            if (TextUtils.isEmpty((CharSequence)CarMDVinDecoder.this.CAR_MD_AUTH)) {
                                CarMDVinDecoder.sLogger.e("Missing car md auth credential !");
                                CarMDVinDecoder.this.mDecoding = false;
                                return;
                            }
                            url.addHeader("authorization", "Basic " + CarMDVinDecoder.this.CAR_MD_AUTH);
                            if (!TextUtils.isEmpty((CharSequence)CarMDVinDecoder.this.CAR_MD_TOKEN)) {
                                url.addHeader("partner-token", CarMDVinDecoder.this.CAR_MD_TOKEN);
                                CarMDVinDecoder.this.mHttpClient.newCall(url.build()).enqueue(new Callback() {
                                    @Override
                                    public void onFailure(final Call call, final IOException ex) {
                                        CarMDVinDecoder.sLogger.e("onFailure : Decoding failed ", ex);
                                        CarMDVinDecoder.this.mDecoding = false;
                                    }
                                    
                                    @Override
                                    public void onResponse(final Call call, final Response response) throws IOException {
                                        CarMDVinDecoder.sLogger.d("onResponse : Decoding response received");
                                        CarMDVinDecoder.this.parseResponse(response.body());
                                        CarMDVinDecoder.this.mDecoding = false;
                                    }
                                });
                                return;
                            }
                            CarMDVinDecoder.sLogger.e("Missing car md token credential !");
                            CarMDVinDecoder.this.mDecoding = false;
                        }
                    }
                }, 1);
            }
        }
    }
}
