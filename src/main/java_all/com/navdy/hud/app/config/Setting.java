package com.navdy.hud.app.config;

public class Setting
{
    private static int MAX_PROP_LEN;
    private String description;
    private String name;
    private IObserver observer;
    private String path;
    private String property;
    
    static {
        Setting.MAX_PROP_LEN = 31;
    }
    
    public Setting(final String s, final String s2, final String s3) {
        this(s, s2, s3, "persist.sys." + s2);
    }
    
    public Setting(final String name, final String path, final String description, final String property) {
        this.name = name;
        this.path = path;
        this.description = description;
        if (property != null && property.length() > Setting.MAX_PROP_LEN) {
            throw new IllegalArgumentException("property name (" + property + ") too long");
        }
        this.property = property;
    }
    
    protected void changed() {
        if (this.observer != null) {
            this.observer.onChanged(this);
        }
    }
    
    public String getDescription() {
        return this.description;
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getPath() {
        return this.path;
    }
    
    public String getProperty() {
        return this.property;
    }
    
    public boolean isEnabled() {
        return true;
    }
    
    public void setObserver(final IObserver observer) {
        this.observer = observer;
    }
    
    interface IObserver
    {
        void onChanged(final Setting p0);
    }
}
