package com.navdy.hud.app.bluetooth.obex;

import java.io.IOException;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

public final class PrivateOutputStream extends OutputStream
{
    private ByteArrayOutputStream mArray;
    private int mMaxPacketSize;
    private boolean mOpen;
    private BaseStream mParent;
    
    public PrivateOutputStream(final BaseStream mParent, final int mMaxPacketSize) {
        this.mParent = mParent;
        this.mArray = new ByteArrayOutputStream();
        this.mMaxPacketSize = mMaxPacketSize;
        this.mOpen = true;
    }
    
    private void ensureOpen() throws IOException {
        this.mParent.ensureOpen();
        if (!this.mOpen) {
            throw new IOException("Output stream is closed");
        }
    }
    
    @Override
    public void close() throws IOException {
        this.mOpen = false;
        this.mParent.streamClosed(false);
    }
    
    public boolean isClosed() {
        return !this.mOpen;
    }
    
    public byte[] readBytes(final int n) {
        synchronized (this) {
            byte[] array2;
            if (this.mArray.size() > 0) {
                final byte[] byteArray = this.mArray.toByteArray();
                this.mArray.reset();
                final byte[] array = new byte[n];
                System.arraycopy(byteArray, 0, array, 0, n);
                array2 = array;
                if (byteArray.length != n) {
                    this.mArray.write(byteArray, n, byteArray.length - n);
                    array2 = array;
                }
            }
            else {
                array2 = null;
            }
            return array2;
        }
    }
    
    public int size() {
        return this.mArray.size();
    }
    
    @Override
    public void write(final int n) throws IOException {
        synchronized (this) {
            this.ensureOpen();
            this.mParent.ensureNotDone();
            this.mArray.write(n);
            if (this.mArray.size() == this.mMaxPacketSize) {
                this.mParent.continueOperation(true, false);
            }
        }
    }
    
    @Override
    public void write(final byte[] array) throws IOException {
        this.write(array, 0, array.length);
    }
    
    @Override
    public void write(final byte[] array, int n, int n2) throws IOException {
        // monitorenter(this)
        int n3 = n;
        final int n4 = n2;
        if (array == null) {
            try {
                throw new IOException("buffer is null");
            }
            finally {
            }
            // monitorexit(this)
        }
        if ((n | n2) < 0 || n2 > array.length - n) {
            throw new IndexOutOfBoundsException("index outof bound");
        }
        this.ensureOpen();
        this.mParent.ensureNotDone();
        n = n4;
        while (this.mArray.size() + n >= this.mMaxPacketSize) {
            n2 = this.mMaxPacketSize - this.mArray.size();
            this.mArray.write(array, n3, n2);
            n3 += n2;
            n -= n2;
            this.mParent.continueOperation(true, false);
        }
        if (n > 0) {
            this.mArray.write(array, n3, n);
        }
    }
    // monitorexit(this)
}
