package com.navdy.hud.app.bluetooth.obex;

public final class PasswordAuthentication
{
    private final byte[] mPassword;
    private byte[] mUserName;
    
    public PasswordAuthentication(final byte[] array, final byte[] array2) {
        if (array != null) {
            System.arraycopy(array, 0, this.mUserName = new byte[array.length], 0, array.length);
        }
        System.arraycopy(array2, 0, this.mPassword = new byte[array2.length], 0, array2.length);
    }
    
    public byte[] getPassword() {
        return this.mPassword;
    }
    
    public byte[] getUserName() {
        return this.mUserName;
    }
}
