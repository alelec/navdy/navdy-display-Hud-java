package com.navdy.hud.app.bluetooth.vcard;

public interface VCardEntryHandler
{
    void onEnd();
    
    void onEntryCreated(final VCardEntry p0);
    
    void onStart();
}
