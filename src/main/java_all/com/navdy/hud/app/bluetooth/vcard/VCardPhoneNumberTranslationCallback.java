package com.navdy.hud.app.bluetooth.vcard;

public interface VCardPhoneNumberTranslationCallback
{
    String onValueReceived(final String p0, final int p1, final String p2, final boolean p3);
}
