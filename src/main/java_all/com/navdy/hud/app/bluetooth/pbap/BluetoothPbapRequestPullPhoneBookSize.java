package com.navdy.hud.app.bluetooth.pbap;

import android.util.Log;
import com.navdy.hud.app.bluetooth.obex.HeaderSet;
import com.navdy.hud.app.bluetooth.pbap.utils.ObexAppParameters;

class BluetoothPbapRequestPullPhoneBookSize extends BluetoothPbapRequest
{
    private static final String TAG = "BTPbapReqPullPBookSize";
    private static final String TYPE = "x-bt/phonebook";
    private int mSize;
    
    public BluetoothPbapRequestPullPhoneBookSize(final String s) {
        this.mHeaderSet.setHeader(1, s);
        this.mHeaderSet.setHeader(66, "x-bt/phonebook");
        final ObexAppParameters obexAppParameters = new ObexAppParameters();
        obexAppParameters.add((byte)4, (short)0);
        obexAppParameters.addToHeaderSet(this.mHeaderSet);
    }
    
    public int getSize() {
        return this.mSize;
    }
    
    @Override
    protected void readResponseHeaders(final HeaderSet set) {
        Log.v("BTPbapReqPullPBookSize", "readResponseHeaders");
        this.mSize = ObexAppParameters.fromHeaderSet(set).getShort((byte)8);
    }
}
