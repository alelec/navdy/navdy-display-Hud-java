package com.navdy.hud.app.bluetooth.vcard.exception;

public class VCardException extends Exception
{
    public VCardException() {
    }
    
    public VCardException(final String s) {
        super(s);
    }
}
