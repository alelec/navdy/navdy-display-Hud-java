package com.navdy.hud.app.bluetooth.obex;

import java.io.IOException;

public class ObexSession
{
    private static final String TAG = "ObexSession";
    private static final boolean V = false;
    protected Authenticator mAuthenticator;
    protected byte[] mChallengeDigest;
    
    public boolean handleAuthChall(final HeaderSet set) throws IOException {
        boolean b;
        if (this.mAuthenticator == null) {
            b = false;
        }
        else {
            final byte[] tagValue = ObexHelper.getTagValue((byte)0, set.mAuthChall);
            final byte[] tagValue2 = ObexHelper.getTagValue((byte)1, set.mAuthChall);
            final byte[] tagValue3 = ObexHelper.getTagValue((byte)2, set.mAuthChall);
            String convertToUnicode = null;
            PasswordAuthentication passwordAuthentication2 = null;
        Label_0245:
            while (true) {
                if (tagValue3 == null) {
                    break Label_0135;
                }
                final byte[] array = new byte[tagValue3.length - 1];
                System.arraycopy(tagValue3, 1, array, 0, array.length);
                Label_0228: {
                    switch (tagValue3[0] & 0xFF) {
                        default:
                            throw new IOException("Unsupported Encoding Scheme");
                        case 0:
                        case 1:
                            break;
                        case 255:
                            break Label_0228;
                    }
                    boolean b2;
                    boolean b5;
                    try {
                        convertToUnicode = new String(array, "ISO8859_1");
                        b2 = false;
                        boolean b3 = false;
                        final boolean b4 = b5 = true;
                        if (tagValue2 != null) {
                            if ((tagValue2[0] & 0x1) != 0x0) {
                                b3 = true;
                            }
                            b5 = b4;
                            b2 = b3;
                            if ((tagValue2[0] & 0x2) != 0x0) {
                                b5 = false;
                                b2 = b3;
                            }
                        }
                        set.mAuthChall = null;
                        final ObexSession obexSession = this;
                        final Authenticator authenticator = obexSession.mAuthenticator;
                        final String s = convertToUnicode;
                        final boolean b6 = b2;
                        final boolean b7 = b5;
                        final PasswordAuthentication passwordAuthentication = authenticator.onAuthenticationChallenge(s, b6, b7);
                        final PasswordAuthentication passwordAuthentication3;
                        passwordAuthentication2 = (passwordAuthentication3 = passwordAuthentication);
                        if (passwordAuthentication3 == null) {
                            b = false;
                            return b;
                        }
                        break Label_0245;
                    }
                    catch (Exception ex) {
                        throw new IOException("Unsupported Encoding Scheme");
                    }
                    try {
                        final ObexSession obexSession = this;
                        final Authenticator authenticator = obexSession.mAuthenticator;
                        final String s = convertToUnicode;
                        final boolean b6 = b2;
                        final boolean b7 = b5;
                        final PasswordAuthentication passwordAuthentication = authenticator.onAuthenticationChallenge(s, b6, b7);
                        final PasswordAuthentication passwordAuthentication3;
                        passwordAuthentication2 = (passwordAuthentication3 = passwordAuthentication);
                        if (passwordAuthentication3 == null) {
                            b = false;
                            return b;
                        }
                        break Label_0245;
                        convertToUnicode = ObexHelper.convertToUnicode(array, false);
                        continue;
                    }
                    catch (Exception ex2) {
                        b = false;
                        return b;
                    }
                }
                break;
            }
            final byte[] password = passwordAuthentication2.getPassword();
            if (password == null) {
                b = false;
            }
            else {
                final byte[] userName = passwordAuthentication2.getUserName();
                if (userName != null) {
                    (set.mAuthResp = new byte[userName.length + 38])[36] = 1;
                    set.mAuthResp[37] = (byte)userName.length;
                    System.arraycopy(userName, 0, set.mAuthResp, 38, userName.length);
                }
                else {
                    set.mAuthResp = new byte[36];
                }
                final byte[] array2 = new byte[tagValue.length + password.length + 1];
                System.arraycopy(tagValue, 0, array2, 0, tagValue.length);
                array2[tagValue.length] = 58;
                System.arraycopy(password, 0, array2, tagValue.length + 1, password.length);
                set.mAuthResp[0] = 0;
                set.mAuthResp[1] = 16;
                System.arraycopy(ObexHelper.computeMd5Hash(array2), 0, set.mAuthResp, 2, 16);
                set.mAuthResp[18] = 2;
                set.mAuthResp[19] = 16;
                System.arraycopy(tagValue, 0, set.mAuthResp, 20, 16);
                b = true;
            }
        }
        return b;
    }
    
    public boolean handleAuthResp(byte[] tagValue) {
        final boolean b = false;
        boolean b2;
        if (this.mAuthenticator == null) {
            b2 = b;
        }
        else {
            final byte[] onAuthenticationResponse = this.mAuthenticator.onAuthenticationResponse(ObexHelper.getTagValue((byte)1, tagValue));
            b2 = b;
            if (onAuthenticationResponse != null) {
                final byte[] array = new byte[onAuthenticationResponse.length + 16];
                System.arraycopy(this.mChallengeDigest, 0, array, 0, 16);
                System.arraycopy(onAuthenticationResponse, 0, array, 16, onAuthenticationResponse.length);
                final byte[] computeMd5Hash = ObexHelper.computeMd5Hash(array);
                tagValue = ObexHelper.getTagValue((byte)0, tagValue);
                for (int i = 0; i < 16; ++i) {
                    b2 = b;
                    if (computeMd5Hash[i] != tagValue[i]) {
                        return b2;
                    }
                }
                b2 = true;
            }
        }
        return b2;
    }
}
