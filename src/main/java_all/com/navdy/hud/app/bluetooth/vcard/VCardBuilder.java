package com.navdy.hud.app.bluetooth.vcard;

import java.io.ObjectInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import android.util.Base64;
import java.util.ArrayList;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import android.content.ContentValues;
import java.util.List;
import android.text.TextUtils;
import android.util.Log;
import java.util.HashMap;
import java.util.Collections;
import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;

public class VCardBuilder
{
    public static final int DEFAULT_EMAIL_TYPE = 3;
    public static final int DEFAULT_PHONE_TYPE = 1;
    public static final int DEFAULT_POSTAL_TYPE = 1;
    private static final String LOG_TAG = "vCard";
    private static final String SHIFT_JIS = "SHIFT_JIS";
    private static final String VCARD_DATA_PUBLIC = "PUBLIC";
    private static final String VCARD_DATA_SEPARATOR = ":";
    private static final String VCARD_DATA_VCARD = "VCARD";
    public static final String VCARD_END_OF_LINE = "\r\n";
    private static final String VCARD_ITEM_SEPARATOR = ";";
    private static final String VCARD_PARAM_ENCODING_BASE64_AS_B = "ENCODING=B";
    private static final String VCARD_PARAM_ENCODING_BASE64_V21 = "ENCODING=BASE64";
    private static final String VCARD_PARAM_ENCODING_QP = "ENCODING=QUOTED-PRINTABLE";
    private static final String VCARD_PARAM_EQUAL = "=";
    private static final String VCARD_PARAM_SEPARATOR = ";";
    private static final String VCARD_WS = " ";
    private static final Set<String> sAllowedAndroidPropertySet;
    private static final Map<Integer, Integer> sPostalTypePriorityMap;
    private final boolean mAppendTypeParamName;
    private StringBuilder mBuilder;
    private final String mCharset;
    private boolean mEndAppended;
    private final boolean mIsDoCoMo;
    private final boolean mIsJapaneseMobilePhone;
    private final boolean mIsV30OrV40;
    private final boolean mNeedsToConvertPhoneticString;
    private final boolean mOnlyOneNoteFieldIsAvailable;
    private final boolean mRefrainsQPToNameProperties;
    private final boolean mShouldAppendCharsetParam;
    private final boolean mShouldUseQuotedPrintable;
    private final boolean mUsesAndroidProperty;
    private final boolean mUsesDefactProperty;
    private final String mVCardCharsetParameter;
    private final int mVCardType;
    
    static {
        sAllowedAndroidPropertySet = Collections.<String>unmodifiableSet((Set<? extends String>)new HashSet<String>(Arrays.<String>asList("vnd.android.cursor.item/nickname", "vnd.android.cursor.item/contact_event", "vnd.android.cursor.item/relation")));
        (sPostalTypePriorityMap = new HashMap<Integer, Integer>()).put(1, 0);
        VCardBuilder.sPostalTypePriorityMap.put(2, 1);
        VCardBuilder.sPostalTypePriorityMap.put(3, 2);
        VCardBuilder.sPostalTypePriorityMap.put(0, 3);
    }
    
    public VCardBuilder(final int n) {
        this(n, null);
    }
    
    public VCardBuilder(final int mvCardType, final String mCharset) {
        final boolean b = false;
        this.mVCardType = mvCardType;
        if (VCardConfig.isVersion40(mvCardType)) {
            Log.w("vCard", "Should not use vCard 4.0 when building vCard. It is not officially published yet.");
        }
        this.mIsV30OrV40 = (VCardConfig.isVersion30(mvCardType) || VCardConfig.isVersion40(mvCardType));
        this.mShouldUseQuotedPrintable = VCardConfig.shouldUseQuotedPrintable(mvCardType);
        this.mIsDoCoMo = VCardConfig.isDoCoMo(mvCardType);
        this.mIsJapaneseMobilePhone = VCardConfig.needsToConvertPhoneticString(mvCardType);
        this.mOnlyOneNoteFieldIsAvailable = VCardConfig.onlyOneNoteFieldIsAvailable(mvCardType);
        this.mUsesAndroidProperty = VCardConfig.usesAndroidSpecificProperty(mvCardType);
        this.mUsesDefactProperty = VCardConfig.usesDefactProperty(mvCardType);
        this.mRefrainsQPToNameProperties = VCardConfig.shouldRefrainQPToNameProperties(mvCardType);
        this.mAppendTypeParamName = VCardConfig.appendTypeParamName(mvCardType);
        this.mNeedsToConvertPhoneticString = VCardConfig.needsToConvertPhoneticString(mvCardType);
        boolean mShouldAppendCharsetParam = false;
        Label_0143: {
            if (VCardConfig.isVersion30(mvCardType)) {
                mShouldAppendCharsetParam = b;
                if ("UTF-8".equalsIgnoreCase(mCharset)) {
                    break Label_0143;
                }
            }
            mShouldAppendCharsetParam = true;
        }
        this.mShouldAppendCharsetParam = mShouldAppendCharsetParam;
        if (VCardConfig.isDoCoMo(mvCardType)) {
            if (!"SHIFT_JIS".equalsIgnoreCase(mCharset)) {
                if (TextUtils.isEmpty((CharSequence)mCharset)) {
                    this.mCharset = "SHIFT_JIS";
                }
                else {
                    this.mCharset = mCharset;
                }
            }
            else {
                this.mCharset = mCharset;
            }
            this.mVCardCharsetParameter = "CHARSET=SHIFT_JIS";
        }
        else if (TextUtils.isEmpty((CharSequence)mCharset)) {
            Log.i("vCard", "Use the charset \"UTF-8\" for export.");
            this.mCharset = "UTF-8";
            this.mVCardCharsetParameter = "CHARSET=UTF-8";
        }
        else {
            this.mCharset = mCharset;
            this.mVCardCharsetParameter = "CHARSET=" + mCharset;
        }
        this.clear();
    }
    
    private VCardBuilder appendNamePropertiesV40(final List<ContentValues> list) {
        if (this.mIsDoCoMo || this.mNeedsToConvertPhoneticString) {
            Log.w("vCard", "Invalid flag is used in vCard 4.0 construction. Ignored.");
        }
        if (list == null || list.isEmpty()) {
            this.appendLine("FN", "");
        }
        else {
            final ContentValues primaryContentValueWithStructuredName = this.getPrimaryContentValueWithStructuredName(list);
            final String asString = primaryContentValueWithStructuredName.getAsString("data3");
            final String asString2 = primaryContentValueWithStructuredName.getAsString("data5");
            final String asString3 = primaryContentValueWithStructuredName.getAsString("data2");
            final String asString4 = primaryContentValueWithStructuredName.getAsString("data4");
            final String asString5 = primaryContentValueWithStructuredName.getAsString("data6");
            final String asString6 = primaryContentValueWithStructuredName.getAsString("data1");
            String s = asString;
            if (TextUtils.isEmpty((CharSequence)asString)) {
                s = asString;
                if (TextUtils.isEmpty((CharSequence)asString3)) {
                    s = asString;
                    if (TextUtils.isEmpty((CharSequence)asString2)) {
                        s = asString;
                        if (TextUtils.isEmpty((CharSequence)asString4)) {
                            s = asString;
                            if (TextUtils.isEmpty((CharSequence)asString5)) {
                                if (TextUtils.isEmpty((CharSequence)asString6)) {
                                    this.appendLine("FN", "");
                                    return this;
                                }
                                s = asString6;
                            }
                        }
                    }
                }
            }
            final String asString7 = primaryContentValueWithStructuredName.getAsString("data9");
            final String asString8 = primaryContentValueWithStructuredName.getAsString("data8");
            final String asString9 = primaryContentValueWithStructuredName.getAsString("data7");
            final String escapeCharacters = this.escapeCharacters(s);
            final String escapeCharacters2 = this.escapeCharacters(asString3);
            final String escapeCharacters3 = this.escapeCharacters(asString2);
            final String escapeCharacters4 = this.escapeCharacters(asString4);
            final String escapeCharacters5 = this.escapeCharacters(asString5);
            this.mBuilder.append("N");
            if (!TextUtils.isEmpty((CharSequence)asString7) || !TextUtils.isEmpty((CharSequence)asString8) || !TextUtils.isEmpty((CharSequence)asString9)) {
                this.mBuilder.append(";");
                this.mBuilder.append("SORT-AS=").append(VCardUtils.toStringAsV40ParamValue(this.escapeCharacters(asString7) + ';' + this.escapeCharacters(asString9) + ';' + this.escapeCharacters(asString8)));
            }
            this.mBuilder.append(":");
            this.mBuilder.append(escapeCharacters);
            this.mBuilder.append(";");
            this.mBuilder.append(escapeCharacters2);
            this.mBuilder.append(";");
            this.mBuilder.append(escapeCharacters3);
            this.mBuilder.append(";");
            this.mBuilder.append(escapeCharacters4);
            this.mBuilder.append(";");
            this.mBuilder.append(escapeCharacters5);
            this.mBuilder.append("\r\n");
            if (TextUtils.isEmpty((CharSequence)asString6)) {
                Log.w("vCard", "DISPLAY_NAME is empty.");
                this.appendLine("FN", this.escapeCharacters(VCardUtils.constructNameFromElements(VCardConfig.getNameOrderType(this.mVCardType), s, asString2, asString3, asString4, asString5)));
            }
            else {
                final String escapeCharacters6 = this.escapeCharacters(asString6);
                this.mBuilder.append("FN");
                this.mBuilder.append(":");
                this.mBuilder.append(escapeCharacters6);
                this.mBuilder.append("\r\n");
            }
            this.appendPhoneticNameFields(primaryContentValueWithStructuredName);
        }
        return this;
    }
    
    private void appendPhoneticNameFields(final ContentValues contentValues) {
        final String asString = contentValues.getAsString("data9");
        String s = contentValues.getAsString("data8");
        String s2 = contentValues.getAsString("data7");
        String halfWidthString;
        if (this.mNeedsToConvertPhoneticString) {
            halfWidthString = VCardUtils.toHalfWidthString(asString);
            s = VCardUtils.toHalfWidthString(s);
            s2 = VCardUtils.toHalfWidthString(s2);
        }
        else {
            halfWidthString = asString;
        }
        if (TextUtils.isEmpty((CharSequence)halfWidthString) && TextUtils.isEmpty((CharSequence)s) && TextUtils.isEmpty((CharSequence)s2)) {
            if (this.mIsDoCoMo) {
                this.mBuilder.append("SOUND");
                this.mBuilder.append(";");
                this.mBuilder.append("X-IRMC-N");
                this.mBuilder.append(":");
                this.mBuilder.append(";");
                this.mBuilder.append(";");
                this.mBuilder.append(";");
                this.mBuilder.append(";");
                this.mBuilder.append("\r\n");
            }
        }
        else {
            if (!VCardConfig.isVersion40(this.mVCardType)) {
                if (VCardConfig.isVersion30(this.mVCardType)) {
                    final String constructNameFromElements = VCardUtils.constructNameFromElements(this.mVCardType, halfWidthString, s, s2);
                    this.mBuilder.append("SORT-STRING");
                    if (VCardConfig.isVersion30(this.mVCardType) && this.shouldAppendCharsetParam(constructNameFromElements)) {
                        this.mBuilder.append(";");
                        this.mBuilder.append(this.mVCardCharsetParameter);
                    }
                    this.mBuilder.append(":");
                    this.mBuilder.append(this.escapeCharacters(constructNameFromElements));
                    this.mBuilder.append("\r\n");
                }
                else if (this.mIsJapaneseMobilePhone) {
                    this.mBuilder.append("SOUND");
                    this.mBuilder.append(";");
                    this.mBuilder.append("X-IRMC-N");
                    int n;
                    if (!this.mRefrainsQPToNameProperties && (!VCardUtils.containsOnlyNonCrLfPrintableAscii(halfWidthString) || !VCardUtils.containsOnlyNonCrLfPrintableAscii(s) || !VCardUtils.containsOnlyNonCrLfPrintableAscii(s2))) {
                        n = 1;
                    }
                    else {
                        n = 0;
                    }
                    String s3;
                    String s4;
                    String s5;
                    if (n != 0) {
                        s3 = this.encodeQuotedPrintable(halfWidthString);
                        s4 = this.encodeQuotedPrintable(s);
                        s5 = this.encodeQuotedPrintable(s2);
                    }
                    else {
                        s3 = this.escapeCharacters(halfWidthString);
                        s4 = this.escapeCharacters(s);
                        s5 = this.escapeCharacters(s2);
                    }
                    if (this.shouldAppendCharsetParam(s3, s4, s5)) {
                        this.mBuilder.append(";");
                        this.mBuilder.append(this.mVCardCharsetParameter);
                    }
                    this.mBuilder.append(":");
                    int n2 = 1;
                    if (!TextUtils.isEmpty((CharSequence)s3)) {
                        this.mBuilder.append(s3);
                        n2 = 0;
                    }
                    int n3 = n2;
                    if (!TextUtils.isEmpty((CharSequence)s4)) {
                        if (n2 != 0) {
                            n2 = 0;
                        }
                        else {
                            this.mBuilder.append(' ');
                        }
                        this.mBuilder.append(s4);
                        n3 = n2;
                    }
                    if (!TextUtils.isEmpty((CharSequence)s5)) {
                        if (n3 == 0) {
                            this.mBuilder.append(' ');
                        }
                        this.mBuilder.append(s5);
                    }
                    this.mBuilder.append(";");
                    this.mBuilder.append(";");
                    this.mBuilder.append(";");
                    this.mBuilder.append(";");
                    this.mBuilder.append("\r\n");
                }
            }
            if (this.mUsesDefactProperty) {
                if (!TextUtils.isEmpty((CharSequence)s2)) {
                    boolean b;
                    if (this.mShouldUseQuotedPrintable && !VCardUtils.containsOnlyNonCrLfPrintableAscii(s2)) {
                        b = true;
                    }
                    else {
                        b = false;
                    }
                    String s6;
                    if (b) {
                        s6 = this.encodeQuotedPrintable(s2);
                    }
                    else {
                        s6 = this.escapeCharacters(s2);
                    }
                    this.mBuilder.append("X-PHONETIC-FIRST-NAME");
                    if (this.shouldAppendCharsetParam(s2)) {
                        this.mBuilder.append(";");
                        this.mBuilder.append(this.mVCardCharsetParameter);
                    }
                    if (b) {
                        this.mBuilder.append(";");
                        this.mBuilder.append("ENCODING=QUOTED-PRINTABLE");
                    }
                    this.mBuilder.append(":");
                    this.mBuilder.append(s6);
                    this.mBuilder.append("\r\n");
                }
                if (!TextUtils.isEmpty((CharSequence)s)) {
                    boolean b2;
                    if (this.mShouldUseQuotedPrintable && !VCardUtils.containsOnlyNonCrLfPrintableAscii(s)) {
                        b2 = true;
                    }
                    else {
                        b2 = false;
                    }
                    String s7;
                    if (b2) {
                        s7 = this.encodeQuotedPrintable(s);
                    }
                    else {
                        s7 = this.escapeCharacters(s);
                    }
                    this.mBuilder.append("X-PHONETIC-MIDDLE-NAME");
                    if (this.shouldAppendCharsetParam(s)) {
                        this.mBuilder.append(";");
                        this.mBuilder.append(this.mVCardCharsetParameter);
                    }
                    if (b2) {
                        this.mBuilder.append(";");
                        this.mBuilder.append("ENCODING=QUOTED-PRINTABLE");
                    }
                    this.mBuilder.append(":");
                    this.mBuilder.append(s7);
                    this.mBuilder.append("\r\n");
                }
                if (!TextUtils.isEmpty((CharSequence)halfWidthString)) {
                    boolean b3;
                    if (this.mShouldUseQuotedPrintable && !VCardUtils.containsOnlyNonCrLfPrintableAscii(halfWidthString)) {
                        b3 = true;
                    }
                    else {
                        b3 = false;
                    }
                    String s8;
                    if (b3) {
                        s8 = this.encodeQuotedPrintable(halfWidthString);
                    }
                    else {
                        s8 = this.escapeCharacters(halfWidthString);
                    }
                    this.mBuilder.append("X-PHONETIC-LAST-NAME");
                    if (this.shouldAppendCharsetParam(halfWidthString)) {
                        this.mBuilder.append(";");
                        this.mBuilder.append(this.mVCardCharsetParameter);
                    }
                    if (b3) {
                        this.mBuilder.append(";");
                        this.mBuilder.append("ENCODING=QUOTED-PRINTABLE");
                    }
                    this.mBuilder.append(":");
                    this.mBuilder.append(s8);
                    this.mBuilder.append("\r\n");
                }
            }
        }
    }
    
    private void appendPostalsForDoCoMo(final List<ContentValues> list) {
        int n = Integer.MAX_VALUE;
        int intValue = Integer.MAX_VALUE;
        final ContentValues contentValues = null;
        final Iterator<ContentValues> iterator = list.iterator();
        ContentValues contentValues2 = contentValues;
        int n2;
        ContentValues contentValues3;
        while (true) {
            n2 = intValue;
            contentValues3 = contentValues2;
            if (!iterator.hasNext()) {
                break;
            }
            contentValues3 = iterator.next();
            if (contentValues3 == null) {
                continue;
            }
            final Integer asInteger = contentValues3.getAsInteger("data2");
            final Integer n3 = VCardBuilder.sPostalTypePriorityMap.get(asInteger);
            int intValue2;
            if (n3 != null) {
                intValue2 = n3;
            }
            else {
                intValue2 = Integer.MAX_VALUE;
            }
            if (intValue2 >= n) {
                continue;
            }
            n = intValue2;
            n2 = (intValue = asInteger);
            contentValues2 = contentValues3;
            if (intValue2 == 0) {
                break;
            }
        }
        if (contentValues3 == null) {
            Log.w("vCard", "Should not come here. Must have at least one postal data.");
        }
        else {
            this.appendPostalLine(n2, contentValues3.getAsString("data3"), contentValues3, false, true);
        }
    }
    
    private void appendPostalsForGeneric(final List<ContentValues> list) {
        for (final ContentValues contentValues : list) {
            if (contentValues != null) {
                final Integer asInteger = contentValues.getAsInteger("data2");
                int intValue;
                if (asInteger != null) {
                    intValue = asInteger;
                }
                else {
                    intValue = 1;
                }
                final String asString = contentValues.getAsString("data3");
                final Integer asInteger2 = contentValues.getAsInteger("is_primary");
                this.appendPostalLine(intValue, asString, contentValues, asInteger2 != null && asInteger2 > 0, false);
            }
        }
    }
    
    private void appendTypeParameter(final String s) {
        this.appendTypeParameter(this.mBuilder, s);
    }
    
    private void appendTypeParameter(final StringBuilder sb, final String s) {
        if (VCardConfig.isVersion40(this.mVCardType) || ((VCardConfig.isVersion30(this.mVCardType) || this.mAppendTypeParamName) && !this.mIsDoCoMo)) {
            sb.append("TYPE").append("=");
        }
        sb.append(s);
    }
    
    private void appendTypeParameters(final List<String> list) {
        int n = 1;
        for (final String s : list) {
            if (VCardConfig.isVersion30(this.mVCardType) || VCardConfig.isVersion40(this.mVCardType)) {
                String s2;
                if (VCardConfig.isVersion40(this.mVCardType)) {
                    s2 = VCardUtils.toStringAsV40ParamValue(s);
                }
                else {
                    s2 = VCardUtils.toStringAsV30ParamValue(s);
                }
                if (TextUtils.isEmpty((CharSequence)s2)) {
                    continue;
                }
                if (n != 0) {
                    n = 0;
                }
                else {
                    this.mBuilder.append(";");
                }
                this.appendTypeParameter(s2);
            }
            else {
                if (!VCardUtils.isV21Word(s)) {
                    continue;
                }
                if (n != 0) {
                    n = 0;
                }
                else {
                    this.mBuilder.append(";");
                }
                this.appendTypeParameter(s);
            }
        }
    }
    
    private void appendUncommonPhoneType(final StringBuilder sb, final Integer n) {
        if (this.mIsDoCoMo) {
            sb.append("VOICE");
        }
        else {
            final String phoneTypeString = VCardUtils.getPhoneTypeString(n);
            if (phoneTypeString != null) {
                this.appendTypeParameter(phoneTypeString);
            }
            else {
                Log.e("vCard", "Unknown or unsupported (by vCard) Phone type: " + n);
            }
        }
    }
    
    private void buildSinglePartNameField(final String s, final String s2) {
        boolean b;
        if (!this.mRefrainsQPToNameProperties && !VCardUtils.containsOnlyNonCrLfPrintableAscii(s2)) {
            b = true;
        }
        else {
            b = false;
        }
        String s3;
        if (b) {
            s3 = this.encodeQuotedPrintable(s2);
        }
        else {
            s3 = this.escapeCharacters(s2);
        }
        this.mBuilder.append(s);
        if (this.shouldAppendCharsetParam(s2)) {
            this.mBuilder.append(";");
            this.mBuilder.append(this.mVCardCharsetParameter);
        }
        if (b) {
            this.mBuilder.append(";");
            this.mBuilder.append("ENCODING=QUOTED-PRINTABLE");
        }
        this.mBuilder.append(":");
        this.mBuilder.append(s3);
    }
    
    private boolean containsNonEmptyName(final ContentValues contentValues) {
        final String asString = contentValues.getAsString("data3");
        final String asString2 = contentValues.getAsString("data5");
        final String asString3 = contentValues.getAsString("data2");
        final String asString4 = contentValues.getAsString("data4");
        final String asString5 = contentValues.getAsString("data6");
        final String asString6 = contentValues.getAsString("data9");
        final String asString7 = contentValues.getAsString("data8");
        final String asString8 = contentValues.getAsString("data7");
        final String asString9 = contentValues.getAsString("data1");
        return !TextUtils.isEmpty((CharSequence)asString) || !TextUtils.isEmpty((CharSequence)asString2) || !TextUtils.isEmpty((CharSequence)asString3) || !TextUtils.isEmpty((CharSequence)asString4) || !TextUtils.isEmpty((CharSequence)asString5) || !TextUtils.isEmpty((CharSequence)asString6) || !TextUtils.isEmpty((CharSequence)asString7) || !TextUtils.isEmpty((CharSequence)asString8) || !TextUtils.isEmpty((CharSequence)asString9);
    }
    
    private String encodeQuotedPrintable(String o) {
        if (TextUtils.isEmpty((CharSequence)o)) {
            o = "";
        }
        else {
            final StringBuilder sb = new StringBuilder();
            int i = 0;
            int n = 0;
        Label_0161:
            while (true) {
                try {
                    o = ((String)o).getBytes(this.mCharset);
                    while (i < o.length) {
                        sb.append(String.format("=%02X", Byte.valueOf(o[i])));
                        final int n2 = i + 1;
                        final int n3 = n + 3;
                        i = n2;
                        if ((n = n3) >= 67) {
                            sb.append("=\r\n");
                            n = 0;
                            i = n2;
                        }
                    }
                    break Label_0161;
                }
                catch (UnsupportedEncodingException ex) {
                    Log.e("vCard", "Charset " + this.mCharset + " cannot be used. " + "Try default charset");
                    o = ((String)o).getBytes();
                    continue;
                }
                continue;
            }
            o = sb.toString();
        }
        return (String)o;
    }
    
    private String escapeCharacters(String string) {
        if (TextUtils.isEmpty((CharSequence)string)) {
            string = "";
        }
        else {
            final StringBuilder sb = new StringBuilder();
            for (int length = string.length(), i = 0; i < length; ++i) {
                final char char1 = string.charAt(i);
                Label_0161: {
                    switch (char1) {
                        default:
                            sb.append(char1);
                            break;
                        case 59:
                            sb.append('\\');
                            sb.append(';');
                            break;
                        case 13:
                            if (i + 1 >= length || string.charAt(i) != '\n') {
                                break Label_0161;
                            }
                            break;
                        case 10:
                            sb.append("\\n");
                            break;
                        case 92:
                            if (this.mIsV30OrV40) {
                                sb.append("\\\\");
                                break;
                            }
                        case 60:
                        case 62:
                            if (this.mIsDoCoMo) {
                                sb.append('\\');
                                sb.append(char1);
                                break;
                            }
                            sb.append(char1);
                            break;
                        case 44:
                            if (this.mIsV30OrV40) {
                                sb.append("\\,");
                                break;
                            }
                            sb.append(char1);
                            break;
                    }
                }
            }
            string = sb.toString();
        }
        return string;
    }
    
    private ContentValues getPrimaryContentValueWithStructuredName(final List<ContentValues> list) {
        ContentValues contentValues = null;
        final ContentValues contentValues2 = null;
        final Iterator<ContentValues> iterator = list.iterator();
        ContentValues contentValues3 = contentValues2;
        ContentValues contentValues4;
        while (true) {
            contentValues4 = contentValues;
            if (!iterator.hasNext()) {
                break;
            }
            contentValues4 = iterator.next();
            if (contentValues4 == null) {
                continue;
            }
            final Integer asInteger = contentValues4.getAsInteger("is_super_primary");
            if (asInteger != null && asInteger > 0) {
                break;
            }
            if (contentValues != null) {
                continue;
            }
            final Integer asInteger2 = contentValues4.getAsInteger("is_primary");
            if (asInteger2 != null && asInteger2 > 0 && this.containsNonEmptyName(contentValues4)) {
                contentValues = contentValues4;
            }
            else {
                if (contentValues3 != null || !this.containsNonEmptyName(contentValues4)) {
                    continue;
                }
                contentValues3 = contentValues4;
            }
        }
        ContentValues contentValues5;
        if ((contentValues5 = contentValues4) == null) {
            if (contentValues3 != null) {
                contentValues5 = contentValues3;
            }
            else {
                contentValues5 = new ContentValues();
            }
        }
        return contentValues5;
    }
    
    private boolean shouldAppendCharsetParam(final String... array) {
        final boolean b = false;
        boolean b2;
        if (!this.mShouldAppendCharsetParam) {
            b2 = b;
        }
        else {
            final int length = array.length;
            int n = 0;
            while (true) {
                b2 = b;
                if (n >= length) {
                    return b2;
                }
                if (!VCardUtils.containsOnlyPrintableAscii(array[n])) {
                    break;
                }
                ++n;
            }
            b2 = true;
        }
        return b2;
    }
    
    private List<String> splitPhoneNumbers(final String s) {
        final ArrayList<String> list = new ArrayList<String>();
        StringBuilder sb = new StringBuilder();
        for (int length = s.length(), i = 0; i < length; ++i) {
            final char char1 = s.charAt(i);
            if (char1 == '\n' && sb.length() > 0) {
                list.add(sb.toString());
                sb = new StringBuilder();
            }
            else {
                sb.append(char1);
            }
        }
        if (sb.length() > 0) {
            list.add(sb.toString());
        }
        return list;
    }
    
    private PostalStruct tryConstructPostalStruct(final ContentValues contentValues) {
        final String asString = contentValues.getAsString("data5");
        final String asString2 = contentValues.getAsString("data6");
        final String asString3 = contentValues.getAsString("data4");
        final String asString4 = contentValues.getAsString("data7");
        final String asString5 = contentValues.getAsString("data8");
        final String asString6 = contentValues.getAsString("data9");
        final String asString7 = contentValues.getAsString("data10");
        final String[] array = { asString, asString2, asString3, asString4, asString5, asString6, asString7 };
        PostalStruct postalStruct;
        if (!VCardUtils.areAllEmpty(array)) {
            final boolean b = this.mShouldUseQuotedPrintable && !VCardUtils.containsOnlyNonCrLfPrintableAscii(array);
            final boolean b2 = !VCardUtils.containsOnlyPrintableAscii(array);
            String string;
            if (TextUtils.isEmpty((CharSequence)asString4)) {
                if (TextUtils.isEmpty((CharSequence)asString2)) {
                    string = "";
                }
                else {
                    string = asString2;
                }
            }
            else if (TextUtils.isEmpty((CharSequence)asString2)) {
                string = asString4;
            }
            else {
                string = asString4 + " " + asString2;
            }
            String encodeQuotedPrintable;
            String s;
            String s2;
            String encodeQuotedPrintable2;
            String s3;
            String s4;
            if (b) {
                encodeQuotedPrintable = this.encodeQuotedPrintable(asString);
                s = this.encodeQuotedPrintable(asString3);
                s2 = this.encodeQuotedPrintable(string);
                encodeQuotedPrintable2 = this.encodeQuotedPrintable(asString5);
                s3 = this.encodeQuotedPrintable(asString6);
                s4 = this.encodeQuotedPrintable(asString7);
            }
            else {
                final String escapeCharacters = this.escapeCharacters(asString);
                s = this.escapeCharacters(asString3);
                s2 = this.escapeCharacters(string);
                final String escapeCharacters2 = this.escapeCharacters(asString5);
                s3 = this.escapeCharacters(asString6);
                s4 = this.escapeCharacters(asString7);
                this.escapeCharacters(asString2);
                encodeQuotedPrintable = escapeCharacters;
                encodeQuotedPrintable2 = escapeCharacters2;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append(encodeQuotedPrintable);
            sb.append(";");
            sb.append(";");
            sb.append(s);
            sb.append(";");
            sb.append(s2);
            sb.append(";");
            sb.append(encodeQuotedPrintable2);
            sb.append(";");
            sb.append(s3);
            sb.append(";");
            sb.append(s4);
            postalStruct = new PostalStruct(b, b2, sb.toString());
        }
        else {
            final String asString8 = contentValues.getAsString("data1");
            if (TextUtils.isEmpty((CharSequence)asString8)) {
                postalStruct = null;
            }
            else {
                final boolean b3 = this.mShouldUseQuotedPrintable && !VCardUtils.containsOnlyNonCrLfPrintableAscii(asString8);
                final boolean b4 = !VCardUtils.containsOnlyPrintableAscii(asString8);
                String s5;
                if (b3) {
                    s5 = this.encodeQuotedPrintable(asString8);
                }
                else {
                    s5 = this.escapeCharacters(asString8);
                }
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(";");
                sb2.append(s5);
                sb2.append(";");
                sb2.append(";");
                sb2.append(";");
                sb2.append(";");
                sb2.append(";");
                postalStruct = new PostalStruct(b3, b4, sb2.toString());
            }
        }
        return postalStruct;
    }
    
    public void appendAndroidSpecificProperty(String s, final ContentValues contentValues) {
        if (VCardBuilder.sAllowedAndroidPropertySet.contains(s)) {
            final ArrayList<String> list = new ArrayList<String>();
            for (int i = 1; i <= 15; ++i) {
                String asString;
                if ((asString = contentValues.getAsString("data" + i)) == null) {
                    asString = "";
                }
                list.add(asString);
            }
            int n;
            if (this.mShouldAppendCharsetParam && !VCardUtils.containsOnlyNonCrLfPrintableAscii(list)) {
                n = 1;
            }
            else {
                n = 0;
            }
            boolean b;
            if (this.mShouldUseQuotedPrintable && !VCardUtils.containsOnlyNonCrLfPrintableAscii(list)) {
                b = true;
            }
            else {
                b = false;
            }
            this.mBuilder.append("X-ANDROID-CUSTOM");
            if (n != 0) {
                this.mBuilder.append(";");
                this.mBuilder.append(this.mVCardCharsetParameter);
            }
            if (b) {
                this.mBuilder.append(";");
                this.mBuilder.append("ENCODING=QUOTED-PRINTABLE");
            }
            this.mBuilder.append(":");
            this.mBuilder.append(s);
            final Iterator<Object> iterator = list.iterator();
            while (iterator.hasNext()) {
                s = iterator.next();
                if (b) {
                    s = this.encodeQuotedPrintable(s);
                }
                else {
                    s = this.escapeCharacters(s);
                }
                this.mBuilder.append(";");
                this.mBuilder.append(s);
            }
            this.mBuilder.append("\r\n");
        }
    }
    
    public void appendEmailLine(final int n, String string, final String s, final boolean b) {
        switch (n) {
            default:
                Log.e("vCard", "Unknown Email type: " + n);
                string = null;
                break;
            case 0:
                if (VCardUtils.isMobilePhoneLabel(string)) {
                    string = "CELL";
                    break;
                }
                if (!TextUtils.isEmpty((CharSequence)string) && VCardUtils.containsOnlyAlphaDigitHyphen(string)) {
                    string = "X-" + string;
                    break;
                }
                string = null;
                break;
            case 1:
                string = "HOME";
                break;
            case 2:
                string = "WORK";
                break;
            case 3:
                string = null;
                break;
            case 4:
                string = "CELL";
                break;
        }
        final ArrayList<String> list = new ArrayList<String>();
        if (b) {
            list.add("PREF");
        }
        if (!TextUtils.isEmpty((CharSequence)string)) {
            list.add(string);
        }
        this.appendLineWithCharsetAndQPDetection("EMAIL", list, s);
    }
    
    public VCardBuilder appendEmails(final List<ContentValues> list) {
        int n = 0;
        int n2 = 0;
        if (list != null) {
            final HashSet<String> set = new HashSet<String>();
            final Iterator<ContentValues> iterator = list.iterator();
            while (true) {
                n = n2;
                if (!iterator.hasNext()) {
                    break;
                }
                final ContentValues contentValues = iterator.next();
                final String asString = contentValues.getAsString("data1");
                String trim;
                if ((trim = asString) != null) {
                    trim = asString.trim();
                }
                if (TextUtils.isEmpty((CharSequence)trim)) {
                    continue;
                }
                final Integer asInteger = contentValues.getAsInteger("data2");
                int intValue;
                if (asInteger != null) {
                    intValue = asInteger;
                }
                else {
                    intValue = 3;
                }
                final String asString2 = contentValues.getAsString("data3");
                final Integer asInteger2 = contentValues.getAsInteger("is_primary");
                final boolean b = asInteger2 != null && asInteger2 > 0;
                n2 = 1;
                if (set.contains(trim)) {
                    continue;
                }
                set.add(trim);
                this.appendEmailLine(intValue, asString2, trim, b);
                n2 = n2;
            }
        }
        if (n == 0 && this.mIsDoCoMo) {
            this.appendEmailLine(1, "", "", false);
        }
        return this;
    }
    
    public VCardBuilder appendEvents(final List<ContentValues> list) {
        if (list != null) {
            final String s = null;
            String s2 = null;
            final Iterator<ContentValues> iterator = list.iterator();
            String s3 = s;
            String asString;
            while (true) {
                asString = s3;
                if (!iterator.hasNext()) {
                    break;
                }
                final ContentValues contentValues = iterator.next();
                if (contentValues == null) {
                    continue;
                }
                final Integer asInteger = contentValues.getAsInteger("data2");
                int intValue;
                if (asInteger != null) {
                    intValue = asInteger;
                }
                else {
                    intValue = 2;
                }
                if (intValue == 3) {
                    asString = contentValues.getAsString("data1");
                    if (asString == null) {
                        continue;
                    }
                    final Integer asInteger2 = contentValues.getAsInteger("is_super_primary");
                    int n;
                    if (asInteger2 != null) {
                        if (asInteger2 > 0) {
                            n = 1;
                        }
                        else {
                            n = 0;
                        }
                    }
                    else {
                        n = 0;
                    }
                    if (n != 0) {
                        break;
                    }
                    final Integer asInteger3 = contentValues.getAsInteger("is_primary");
                    int n2;
                    if (asInteger3 != null) {
                        if (asInteger3 > 0) {
                            n2 = 1;
                        }
                        else {
                            n2 = 0;
                        }
                    }
                    else {
                        n2 = 0;
                    }
                    if (n2 != 0) {
                        s3 = asString;
                    }
                    else {
                        if (s2 != null) {
                            continue;
                        }
                        s2 = asString;
                    }
                }
                else {
                    if (!this.mUsesAndroidProperty) {
                        continue;
                    }
                    this.appendAndroidSpecificProperty("vnd.android.cursor.item/contact_event", contentValues);
                }
            }
            if (asString != null) {
                this.appendLineWithCharsetAndQPDetection("BDAY", asString.trim());
            }
            else if (s2 != null) {
                this.appendLineWithCharsetAndQPDetection("BDAY", s2.trim());
            }
        }
        return this;
    }
    
    public VCardBuilder appendIms(final List<ContentValues> list) {
        if (list != null) {
            for (final ContentValues contentValues : list) {
                final Integer asInteger = contentValues.getAsInteger("data5");
                if (asInteger != null) {
                    final String propertyNameForIm = VCardUtils.getPropertyNameForIm(asInteger);
                    if (propertyNameForIm == null) {
                        continue;
                    }
                    final String asString = contentValues.getAsString("data1");
                    String trim;
                    if ((trim = asString) != null) {
                        trim = asString.trim();
                    }
                    if (TextUtils.isEmpty((CharSequence)trim)) {
                        continue;
                    }
                    final Integer asInteger2 = contentValues.getAsInteger("data2");
                    int intValue;
                    if (asInteger2 != null) {
                        intValue = asInteger2;
                    }
                    else {
                        intValue = 3;
                    }
                    String string = null;
                    switch (intValue) {
                        default:
                            string = null;
                            break;
                        case 1:
                            string = "HOME";
                            break;
                        case 2:
                            string = "WORK";
                            break;
                        case 0: {
                            final String asString2 = contentValues.getAsString("data3");
                            if (asString2 != null) {
                                string = "X-" + asString2;
                            }
                            else {
                                string = null;
                            }
                            break;
                        }
                    }
                    final ArrayList<String> list2 = new ArrayList<String>();
                    if (!TextUtils.isEmpty((CharSequence)string)) {
                        list2.add(string);
                    }
                    final Integer asInteger3 = contentValues.getAsInteger("is_primary");
                    int n;
                    if (asInteger3 != null) {
                        if (asInteger3 > 0) {
                            n = 1;
                        }
                        else {
                            n = 0;
                        }
                    }
                    else {
                        n = 0;
                    }
                    if (n != 0) {
                        list2.add("PREF");
                    }
                    this.appendLineWithCharsetAndQPDetection(propertyNameForIm, list2, trim);
                }
            }
        }
        return this;
    }
    
    public void appendLine(final String s, final String s2) {
        this.appendLine(s, s2, false, false);
    }
    
    public void appendLine(final String s, final String s2, final boolean b, final boolean b2) {
        this.appendLine(s, null, s2, b, b2);
    }
    
    public void appendLine(final String s, final List<String> list) {
        this.appendLine(s, list, false, false);
    }
    
    public void appendLine(final String s, final List<String> list, final String s2) {
        this.appendLine(s, list, s2, false, false);
    }
    
    public void appendLine(String s, final List<String> list, final String s2, final boolean b, final boolean b2) {
        this.mBuilder.append(s);
        if (list != null && list.size() > 0) {
            this.mBuilder.append(";");
            this.appendTypeParameters(list);
        }
        if (b) {
            this.mBuilder.append(";");
            this.mBuilder.append(this.mVCardCharsetParameter);
        }
        if (b2) {
            this.mBuilder.append(";");
            this.mBuilder.append("ENCODING=QUOTED-PRINTABLE");
            s = this.encodeQuotedPrintable(s2);
        }
        else {
            s = this.escapeCharacters(s2);
        }
        this.mBuilder.append(":");
        this.mBuilder.append(s);
        this.mBuilder.append("\r\n");
    }
    
    public void appendLine(String s, final List<String> list, final List<String> list2, final boolean b, final boolean b2) {
        this.mBuilder.append(s);
        if (list != null && list.size() > 0) {
            this.mBuilder.append(";");
            this.appendTypeParameters(list);
        }
        if (b) {
            this.mBuilder.append(";");
            this.mBuilder.append(this.mVCardCharsetParameter);
        }
        if (b2) {
            this.mBuilder.append(";");
            this.mBuilder.append("ENCODING=QUOTED-PRINTABLE");
        }
        this.mBuilder.append(":");
        int n = 1;
        final Iterator<String> iterator = list2.iterator();
        while (iterator.hasNext()) {
            s = iterator.next();
            if (b2) {
                s = this.encodeQuotedPrintable(s);
            }
            else {
                s = this.escapeCharacters(s);
            }
            if (n != 0) {
                n = 0;
            }
            else {
                this.mBuilder.append(";");
            }
            this.mBuilder.append(s);
        }
        this.mBuilder.append("\r\n");
    }
    
    public void appendLine(final String s, final List<String> list, final boolean b, final boolean b2) {
        this.appendLine(s, null, list, b, b2);
    }
    
    public void appendLineWithCharsetAndQPDetection(final String s, final String s2) {
        this.appendLineWithCharsetAndQPDetection(s, null, s2);
    }
    
    public void appendLineWithCharsetAndQPDetection(final String s, final List<String> list) {
        this.appendLineWithCharsetAndQPDetection(s, null, list);
    }
    
    public void appendLineWithCharsetAndQPDetection(final String s, final List<String> list, final String s2) {
        this.appendLine(s, list, s2, !VCardUtils.containsOnlyPrintableAscii(s2), this.mShouldUseQuotedPrintable && !VCardUtils.containsOnlyNonCrLfPrintableAscii(s2));
    }
    
    public void appendLineWithCharsetAndQPDetection(final String s, final List<String> list, final List<String> list2) {
        this.appendLine(s, list, list2, this.mShouldAppendCharsetParam && !VCardUtils.containsOnlyNonCrLfPrintableAscii(list2), this.mShouldUseQuotedPrintable && !VCardUtils.containsOnlyNonCrLfPrintableAscii(list2));
    }
    
    public VCardBuilder appendNameProperties(final List<ContentValues> list) {
        VCardBuilder appendNamePropertiesV40;
        if (VCardConfig.isVersion40(this.mVCardType)) {
            appendNamePropertiesV40 = this.appendNamePropertiesV40(list);
        }
        else if (list == null || list.isEmpty()) {
            if (VCardConfig.isVersion30(this.mVCardType)) {
                this.appendLine("N", "");
                this.appendLine("FN", "");
                appendNamePropertiesV40 = this;
            }
            else {
                appendNamePropertiesV40 = this;
                if (this.mIsDoCoMo) {
                    this.appendLine("N", "");
                    appendNamePropertiesV40 = this;
                }
            }
        }
        else {
            final ContentValues primaryContentValueWithStructuredName = this.getPrimaryContentValueWithStructuredName(list);
            final String asString = primaryContentValueWithStructuredName.getAsString("data3");
            final String asString2 = primaryContentValueWithStructuredName.getAsString("data5");
            final String asString3 = primaryContentValueWithStructuredName.getAsString("data2");
            final String asString4 = primaryContentValueWithStructuredName.getAsString("data4");
            final String asString5 = primaryContentValueWithStructuredName.getAsString("data6");
            String s = primaryContentValueWithStructuredName.getAsString("data1");
            if (!TextUtils.isEmpty((CharSequence)asString) || !TextUtils.isEmpty((CharSequence)asString3)) {
                final boolean shouldAppendCharsetParam = this.shouldAppendCharsetParam(asString, asString3, asString2, asString4, asString5);
                boolean b;
                if (!this.mRefrainsQPToNameProperties && (!VCardUtils.containsOnlyNonCrLfPrintableAscii(asString) || !VCardUtils.containsOnlyNonCrLfPrintableAscii(asString3) || !VCardUtils.containsOnlyNonCrLfPrintableAscii(asString2) || !VCardUtils.containsOnlyNonCrLfPrintableAscii(asString4) || !VCardUtils.containsOnlyNonCrLfPrintableAscii(asString5))) {
                    b = true;
                }
                else {
                    b = false;
                }
                if (TextUtils.isEmpty((CharSequence)s)) {
                    s = VCardUtils.constructNameFromElements(VCardConfig.getNameOrderType(this.mVCardType), asString, asString2, asString3, asString4, asString5);
                }
                final boolean shouldAppendCharsetParam2 = this.shouldAppendCharsetParam(s);
                boolean b2;
                if (!this.mRefrainsQPToNameProperties && !VCardUtils.containsOnlyNonCrLfPrintableAscii(s)) {
                    b2 = true;
                }
                else {
                    b2 = false;
                }
                String s2;
                String s3;
                String s4;
                String s5;
                String s6;
                if (b) {
                    s2 = this.encodeQuotedPrintable(asString);
                    s3 = this.encodeQuotedPrintable(asString3);
                    s4 = this.encodeQuotedPrintable(asString2);
                    s5 = this.encodeQuotedPrintable(asString4);
                    s6 = this.encodeQuotedPrintable(asString5);
                }
                else {
                    s2 = this.escapeCharacters(asString);
                    s3 = this.escapeCharacters(asString3);
                    s4 = this.escapeCharacters(asString2);
                    s5 = this.escapeCharacters(asString4);
                    s6 = this.escapeCharacters(asString5);
                }
                String s7;
                if (b2) {
                    s7 = this.encodeQuotedPrintable(s);
                }
                else {
                    s7 = this.escapeCharacters(s);
                }
                this.mBuilder.append("N");
                if (this.mIsDoCoMo) {
                    if (shouldAppendCharsetParam) {
                        this.mBuilder.append(";");
                        this.mBuilder.append(this.mVCardCharsetParameter);
                    }
                    if (b) {
                        this.mBuilder.append(";");
                        this.mBuilder.append("ENCODING=QUOTED-PRINTABLE");
                    }
                    this.mBuilder.append(":");
                    this.mBuilder.append(s);
                    this.mBuilder.append(";");
                    this.mBuilder.append(";");
                    this.mBuilder.append(";");
                    this.mBuilder.append(";");
                }
                else {
                    if (shouldAppendCharsetParam) {
                        this.mBuilder.append(";");
                        this.mBuilder.append(this.mVCardCharsetParameter);
                    }
                    if (b) {
                        this.mBuilder.append(";");
                        this.mBuilder.append("ENCODING=QUOTED-PRINTABLE");
                    }
                    this.mBuilder.append(":");
                    this.mBuilder.append(s2);
                    this.mBuilder.append(";");
                    this.mBuilder.append(s3);
                    this.mBuilder.append(";");
                    this.mBuilder.append(s4);
                    this.mBuilder.append(";");
                    this.mBuilder.append(s5);
                    this.mBuilder.append(";");
                    this.mBuilder.append(s6);
                }
                this.mBuilder.append("\r\n");
                this.mBuilder.append("FN");
                if (shouldAppendCharsetParam2) {
                    this.mBuilder.append(";");
                    this.mBuilder.append(this.mVCardCharsetParameter);
                }
                if (b2) {
                    this.mBuilder.append(";");
                    this.mBuilder.append("ENCODING=QUOTED-PRINTABLE");
                }
                this.mBuilder.append(":");
                this.mBuilder.append(s7);
                this.mBuilder.append("\r\n");
            }
            else if (!TextUtils.isEmpty((CharSequence)s)) {
                this.buildSinglePartNameField("N", s);
                this.mBuilder.append(";");
                this.mBuilder.append(";");
                this.mBuilder.append(";");
                this.mBuilder.append(";");
                this.mBuilder.append("\r\n");
                this.buildSinglePartNameField("FN", s);
                this.mBuilder.append("\r\n");
            }
            else if (VCardConfig.isVersion30(this.mVCardType)) {
                this.appendLine("N", "");
                this.appendLine("FN", "");
            }
            else if (this.mIsDoCoMo) {
                this.appendLine("N", "");
            }
            this.appendPhoneticNameFields(primaryContentValueWithStructuredName);
            appendNamePropertiesV40 = this;
        }
        return appendNamePropertiesV40;
    }
    
    public VCardBuilder appendNickNames(final List<ContentValues> list) {
        int n;
        if (this.mIsV30OrV40) {
            n = 0;
        }
        else {
            if (!this.mUsesAndroidProperty) {
                return this;
            }
            n = 1;
        }
        if (list != null) {
            for (final ContentValues contentValues : list) {
                final String asString = contentValues.getAsString("data1");
                if (!TextUtils.isEmpty((CharSequence)asString)) {
                    if (n != 0) {
                        this.appendAndroidSpecificProperty("vnd.android.cursor.item/nickname", contentValues);
                    }
                    else {
                        this.appendLineWithCharsetAndQPDetection("NICKNAME", asString);
                    }
                }
            }
        }
        return this;
    }
    
    public VCardBuilder appendNotes(final List<ContentValues> list) {
        if (list != null) {
            if (this.mOnlyOneNoteFieldIsAvailable) {
                final StringBuilder sb = new StringBuilder();
                int n = 1;
                final Iterator<ContentValues> iterator = list.iterator();
                while (iterator.hasNext()) {
                    String asString;
                    if ((asString = iterator.next().getAsString("data1")) == null) {
                        asString = "";
                    }
                    if (asString.length() > 0) {
                        if (n != 0) {
                            n = 0;
                        }
                        else {
                            sb.append('\n');
                        }
                        sb.append(asString);
                    }
                }
                final String string = sb.toString();
                this.appendLine("NOTE", string, !VCardUtils.containsOnlyPrintableAscii(string), this.mShouldUseQuotedPrintable && !VCardUtils.containsOnlyNonCrLfPrintableAscii(string));
            }
            else {
                final Iterator<ContentValues> iterator2 = list.iterator();
                while (iterator2.hasNext()) {
                    final String asString2 = iterator2.next().getAsString("data1");
                    if (!TextUtils.isEmpty((CharSequence)asString2)) {
                        this.appendLine("NOTE", asString2, !VCardUtils.containsOnlyPrintableAscii(asString2), this.mShouldUseQuotedPrintable && !VCardUtils.containsOnlyNonCrLfPrintableAscii(asString2));
                    }
                }
            }
        }
        return this;
    }
    
    public VCardBuilder appendOrganizations(final List<ContentValues> list) {
        if (list != null) {
            for (final ContentValues contentValues : list) {
                final String asString = contentValues.getAsString("data1");
                String trim;
                if ((trim = asString) != null) {
                    trim = asString.trim();
                }
                final String asString2 = contentValues.getAsString("data5");
                String trim2;
                if ((trim2 = asString2) != null) {
                    trim2 = asString2.trim();
                }
                final String asString3 = contentValues.getAsString("data4");
                String trim3;
                if ((trim3 = asString3) != null) {
                    trim3 = asString3.trim();
                }
                final StringBuilder sb = new StringBuilder();
                if (!TextUtils.isEmpty((CharSequence)trim)) {
                    sb.append(trim);
                }
                if (!TextUtils.isEmpty((CharSequence)trim2)) {
                    if (sb.length() > 0) {
                        sb.append(';');
                    }
                    sb.append(trim2);
                }
                final String string = sb.toString();
                this.appendLine("ORG", string, !VCardUtils.containsOnlyPrintableAscii(string), this.mShouldUseQuotedPrintable && !VCardUtils.containsOnlyNonCrLfPrintableAscii(string));
                if (!TextUtils.isEmpty((CharSequence)trim3)) {
                    this.appendLine("TITLE", trim3, !VCardUtils.containsOnlyPrintableAscii(trim3), this.mShouldUseQuotedPrintable && !VCardUtils.containsOnlyNonCrLfPrintableAscii(trim3));
                }
            }
        }
        return this;
    }
    
    public VCardBuilder appendPhones(final List<ContentValues> list, final VCardPhoneNumberTranslationCallback vCardPhoneNumberTranslationCallback) {
        int n = 0;
        int n2 = 0;
        if (list != null) {
            final HashSet<String> set = new HashSet<String>();
            final Iterator<ContentValues> iterator = list.iterator();
            while (true) {
                n = n2;
                if (!iterator.hasNext()) {
                    break;
                }
                final ContentValues contentValues = iterator.next();
                final Integer asInteger = contentValues.getAsInteger("data2");
                final String asString = contentValues.getAsString("data3");
                final Integer asInteger2 = contentValues.getAsInteger("is_primary");
                final boolean b = asInteger2 != null && asInteger2 > 0;
                final String asString2 = contentValues.getAsString("data1");
                String trim;
                if ((trim = asString2) != null) {
                    trim = asString2.trim();
                }
                if (TextUtils.isEmpty((CharSequence)trim)) {
                    continue;
                }
                int intValue;
                if (asInteger != null) {
                    intValue = asInteger;
                }
                else {
                    intValue = 1;
                }
                if (vCardPhoneNumberTranslationCallback != null) {
                    final String onValueReceived = vCardPhoneNumberTranslationCallback.onValueReceived(trim, intValue, asString, b);
                    if (set.contains(onValueReceived)) {
                        continue;
                    }
                    set.add(onValueReceived);
                    this.appendTelLine(intValue, asString, onValueReceived, b);
                }
                else if (intValue == 6 || VCardConfig.refrainPhoneNumberFormatting(this.mVCardType)) {
                    n2 = 1;
                    if (set.contains(trim)) {
                        continue;
                    }
                    set.add(trim);
                    this.appendTelLine(intValue, asString, trim, b);
                    n2 = n2;
                }
                else {
                    final List<String> splitPhoneNumbers = this.splitPhoneNumbers(trim);
                    if (splitPhoneNumbers.isEmpty()) {
                        continue;
                    }
                    final boolean b2 = true;
                    final Iterator<String> iterator2 = splitPhoneNumbers.iterator();
                    while (true) {
                        n2 = (b2 ? 1 : 0);
                        if (!iterator2.hasNext()) {
                            break;
                        }
                        final String s = iterator2.next();
                        if (set.contains(s)) {
                            continue;
                        }
                        String s2 = s.replace(',', 'p').replace(';', 'w');
                        if (TextUtils.equals((CharSequence)s2, (CharSequence)s)) {
                            final StringBuilder sb = new StringBuilder();
                            for (int length = s.length(), i = 0; i < length; ++i) {
                                final char char1 = s.charAt(i);
                                if (Character.isDigit(char1) || char1 == '+') {
                                    sb.append(char1);
                                }
                            }
                            s2 = VCardUtils.PhoneNumberUtilsPort.formatNumber(sb.toString(), VCardUtils.getPhoneNumberFormat(this.mVCardType));
                        }
                        String string = s2;
                        if (VCardConfig.isVersion40(this.mVCardType)) {
                            string = s2;
                            if (!TextUtils.isEmpty((CharSequence)s2)) {
                                string = s2;
                                if (!s2.startsWith("tel:")) {
                                    string = "tel:" + s2;
                                }
                            }
                        }
                        set.add(s);
                        this.appendTelLine(intValue, asString, string, b);
                    }
                }
            }
        }
        if (n == 0 && this.mIsDoCoMo) {
            this.appendTelLine(1, "", "", false);
        }
        return this;
    }
    
    public void appendPhotoLine(final String s, String string) {
        final StringBuilder sb = new StringBuilder();
        sb.append("PHOTO");
        sb.append(";");
        if (this.mIsV30OrV40) {
            sb.append("ENCODING=B");
        }
        else {
            sb.append("ENCODING=BASE64");
        }
        sb.append(";");
        this.appendTypeParameter(sb, string);
        sb.append(":");
        sb.append(s);
        string = sb.toString();
        final StringBuilder sb2 = new StringBuilder();
        int n = 0;
        final int length = string.length();
        final int n2 = 75 - "\r\n".length();
        final int length2 = " ".length();
        int n3 = n2;
        int n4;
        for (int i = 0; i < length; ++i, n3 = n4) {
            sb2.append(string.charAt(i));
            if (++n > (n4 = n3)) {
                sb2.append("\r\n");
                sb2.append(" ");
                n4 = n2 - length2;
                n = 0;
            }
        }
        this.mBuilder.append(sb2.toString());
        this.mBuilder.append("\r\n");
        this.mBuilder.append("\r\n");
    }
    
    public VCardBuilder appendPhotos(final List<ContentValues> list) {
        if (list != null) {
            for (final ContentValues contentValues : list) {
                if (contentValues != null) {
                    final byte[] asByteArray = contentValues.getAsByteArray("data15");
                    if (asByteArray == null) {
                        continue;
                    }
                    final String guessImageType = VCardUtils.guessImageType(asByteArray);
                    if (guessImageType == null) {
                        Log.d("vCard", "Unknown photo type. Ignored.");
                    }
                    else {
                        final String s = new String(Base64.encode(asByteArray, 2));
                        if (TextUtils.isEmpty((CharSequence)s)) {
                            continue;
                        }
                        this.appendPhotoLine(s, guessImageType);
                    }
                }
            }
        }
        return this;
    }
    
    public void appendPostalLine(final int n, final String s, final ContentValues contentValues, final boolean b, final boolean b2) {
        final PostalStruct tryConstructPostalStruct = this.tryConstructPostalStruct(contentValues);
        boolean reallyUseQuotedPrintable;
        boolean appendCharset;
        String addressData;
        if (tryConstructPostalStruct == null) {
            if (!b2) {
                return;
            }
            reallyUseQuotedPrintable = false;
            appendCharset = false;
            addressData = "";
        }
        else {
            reallyUseQuotedPrintable = tryConstructPostalStruct.reallyUseQuotedPrintable;
            appendCharset = tryConstructPostalStruct.appendCharset;
            addressData = tryConstructPostalStruct.addressData;
        }
        final ArrayList<String> list = new ArrayList<String>();
        if (b) {
            list.add("PREF");
        }
        while (true) {
            switch (n) {
                default:
                    Log.e("vCard", "Unknown StructuredPostal type: " + n);
                    break Label_0106;
                case 2:
                    list.add("WORK");
                    break Label_0106;
                case 1:
                    list.add("HOME");
                case 3:
                    this.mBuilder.append("ADR");
                    if (!list.isEmpty()) {
                        this.mBuilder.append(";");
                        this.appendTypeParameters(list);
                    }
                    if (appendCharset) {
                        this.mBuilder.append(";");
                        this.mBuilder.append(this.mVCardCharsetParameter);
                    }
                    if (reallyUseQuotedPrintable) {
                        this.mBuilder.append(";");
                        this.mBuilder.append("ENCODING=QUOTED-PRINTABLE");
                    }
                    this.mBuilder.append(":");
                    this.mBuilder.append(addressData);
                    this.mBuilder.append("\r\n");
                    break;
                case 0:
                    if (!TextUtils.isEmpty((CharSequence)s) && VCardUtils.containsOnlyAlphaDigitHyphen(s)) {
                        list.add("X-" + s);
                    }
                    continue;
            }
            break;
        }
    }
    
    public VCardBuilder appendPostals(final List<ContentValues> list) {
        if (list == null || list.isEmpty()) {
            if (this.mIsDoCoMo) {
                this.mBuilder.append("ADR");
                this.mBuilder.append(";");
                this.mBuilder.append("HOME");
                this.mBuilder.append(":");
                this.mBuilder.append("\r\n");
            }
        }
        else if (this.mIsDoCoMo) {
            this.appendPostalsForDoCoMo(list);
        }
        else {
            this.appendPostalsForGeneric(list);
        }
        return this;
    }
    
    public VCardBuilder appendRelation(final List<ContentValues> list) {
        if (this.mUsesAndroidProperty && list != null) {
            for (final ContentValues contentValues : list) {
                if (contentValues != null) {
                    this.appendAndroidSpecificProperty("vnd.android.cursor.item/relation", contentValues);
                }
            }
        }
        return this;
    }
    
    public VCardBuilder appendSipAddresses(final List<ContentValues> list) {
        int n;
        if (this.mIsV30OrV40) {
            n = 0;
        }
        else {
            if (!this.mUsesDefactProperty) {
                return this;
            }
            n = 1;
        }
        if (list != null) {
            final Iterator<ContentValues> iterator = list.iterator();
            while (iterator.hasNext()) {
                final String asString = iterator.next().getAsString("data1");
                if (!TextUtils.isEmpty((CharSequence)asString)) {
                    if (n != 0) {
                        String substring = asString;
                        if (asString.startsWith("sip:")) {
                            if (asString.length() == 4) {
                                continue;
                            }
                            substring = asString.substring(4);
                        }
                        this.appendLineWithCharsetAndQPDetection("X-SIP", substring);
                    }
                    else {
                        String string = asString;
                        if (!asString.startsWith("sip:")) {
                            string = "sip:" + asString;
                        }
                        String s;
                        if (VCardConfig.isVersion40(this.mVCardType)) {
                            s = "TEL";
                        }
                        else {
                            s = "IMPP";
                        }
                        this.appendLineWithCharsetAndQPDetection(s, string);
                    }
                }
            }
        }
        return this;
    }
    
    public void appendTelLine(final Integer n, final String s, final String s2, final boolean b) {
        this.mBuilder.append("TEL");
        this.mBuilder.append(";");
        int intValue;
        if (n == null) {
            intValue = 7;
        }
        else {
            intValue = n;
        }
        final ArrayList<String> list = new ArrayList<String>();
        int n2 = b ? 1 : 0;
        while (true) {
            switch (intValue) {
                default:
                    n2 = (b ? 1 : 0);
                    break Label_0148;
                case 20:
                    list.add("MSG");
                    n2 = (b ? 1 : 0);
                    break Label_0148;
                case 17:
                    list.addAll(Arrays.<String>asList("WORK", "CELL"));
                    n2 = (b ? 1 : 0);
                    break Label_0148;
                case 15:
                    list.add("TLX");
                    n2 = (b ? 1 : 0);
                    break Label_0148;
                case 13:
                    list.add("FAX");
                    n2 = (b ? 1 : 0);
                    break Label_0148;
                case 12:
                    n2 = 1;
                    break Label_0148;
                case 11:
                    list.add("ISDN");
                    n2 = (b ? 1 : 0);
                    break Label_0148;
                case 10:
                    list.add("WORK");
                    n2 = 1;
                    break Label_0148;
                case 9:
                    list.add("CAR");
                    n2 = (b ? 1 : 0);
                    break Label_0148;
                case 7:
                    list.add("VOICE");
                    n2 = (b ? 1 : 0);
                    break Label_0148;
                case 2:
                    list.add("CELL");
                    n2 = (b ? 1 : 0);
                    break Label_0148;
                case 4:
                    list.addAll(Arrays.<String>asList("WORK", "FAX"));
                    n2 = (b ? 1 : 0);
                    break Label_0148;
                case 5:
                    list.addAll(Arrays.<String>asList("HOME", "FAX"));
                    n2 = (b ? 1 : 0);
                    break Label_0148;
                case 3:
                    list.addAll(Arrays.<String>asList("WORK"));
                    n2 = (b ? 1 : 0);
                    break Label_0148;
                case 1:
                    list.addAll(Arrays.<String>asList("HOME"));
                    n2 = (b ? 1 : 0);
                case 8:
                case 14:
                case 16:
                case 19:
                    if (n2 != 0) {
                        list.add("PREF");
                    }
                    if (list.isEmpty()) {
                        this.appendUncommonPhoneType(this.mBuilder, intValue);
                    }
                    else {
                        this.appendTypeParameters(list);
                    }
                    this.mBuilder.append(":");
                    this.mBuilder.append(s2);
                    this.mBuilder.append("\r\n");
                case 6:
                    if (this.mIsDoCoMo) {
                        list.add("VOICE");
                        n2 = (b ? 1 : 0);
                        continue;
                    }
                    list.add("PAGER");
                    n2 = (b ? 1 : 0);
                    continue;
                case 18:
                    list.add("WORK");
                    if (this.mIsDoCoMo) {
                        list.add("VOICE");
                        n2 = (b ? 1 : 0);
                        continue;
                    }
                    list.add("PAGER");
                    n2 = (b ? 1 : 0);
                    continue;
                case 0: {
                    if (TextUtils.isEmpty((CharSequence)s)) {
                        list.add("VOICE");
                        n2 = (b ? 1 : 0);
                        continue;
                    }
                    if (VCardUtils.isMobilePhoneLabel(s)) {
                        list.add("CELL");
                        n2 = (b ? 1 : 0);
                        continue;
                    }
                    if (this.mIsV30OrV40) {
                        list.add(s);
                        n2 = (b ? 1 : 0);
                        continue;
                    }
                    final String upperCase = s.toUpperCase();
                    if (VCardUtils.isValidInV21ButUnknownToContactsPhoteType(upperCase)) {
                        list.add(upperCase);
                        n2 = (b ? 1 : 0);
                        continue;
                    }
                    n2 = (b ? 1 : 0);
                    if (VCardUtils.containsOnlyAlphaDigitHyphen(s)) {
                        list.add("X-" + s);
                        n2 = (b ? 1 : 0);
                    }
                    continue;
                }
            }
            break;
        }
    }
    
    public VCardBuilder appendWebsites(final List<ContentValues> list) {
        if (list != null) {
            final Iterator<ContentValues> iterator = list.iterator();
            while (iterator.hasNext()) {
                final String asString = iterator.next().getAsString("data1");
                String trim;
                if ((trim = asString) != null) {
                    trim = asString.trim();
                }
                if (!TextUtils.isEmpty((CharSequence)trim)) {
                    this.appendLineWithCharsetAndQPDetection("URL", trim);
                }
            }
        }
        return this;
    }
    
    public void clear() {
        this.mBuilder = new StringBuilder();
        this.mEndAppended = false;
        this.appendLine("BEGIN", "VCARD");
        if (VCardConfig.isVersion40(this.mVCardType)) {
            this.appendLine("VERSION", "4.0");
        }
        else if (VCardConfig.isVersion30(this.mVCardType)) {
            this.appendLine("VERSION", "3.0");
        }
        else {
            if (!VCardConfig.isVersion21(this.mVCardType)) {
                Log.w("vCard", "Unknown vCard version detected.");
            }
            this.appendLine("VERSION", "2.1");
        }
    }
    
    @Override
    public String toString() {
        if (!this.mEndAppended) {
            if (this.mIsDoCoMo) {
                this.appendLine("X-CLASS", "PUBLIC");
                this.appendLine("X-REDUCTION", "");
                this.appendLine("X-NO", "");
                this.appendLine("X-DCM-HMN-MODE", "");
            }
            this.appendLine("END", "VCARD");
            this.mEndAppended = true;
        }
        return this.mBuilder.toString();
    }
    
    private static class PostalStruct
    {
        final String addressData;
        final boolean appendCharset;
        final boolean reallyUseQuotedPrintable;
        
        public PostalStruct(final boolean reallyUseQuotedPrintable, final boolean appendCharset, final String addressData) {
            this.reallyUseQuotedPrintable = reallyUseQuotedPrintable;
            this.appendCharset = appendCharset;
            this.addressData = addressData;
        }
    }
}
