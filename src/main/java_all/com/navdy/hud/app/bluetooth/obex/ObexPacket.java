package com.navdy.hud.app.bluetooth.obex;

import java.io.IOException;
import java.io.InputStream;

public class ObexPacket
{
    public int mHeaderId;
    public int mLength;
    public byte[] mPayload;
    
    private ObexPacket(final int mHeaderId, final int mLength) {
        this.mPayload = null;
        this.mHeaderId = mHeaderId;
        this.mLength = mLength;
    }
    
    public static ObexPacket read(int read, final InputStream inputStream) throws IOException {
        final int n = (inputStream.read() << 8) + inputStream.read();
        final ObexPacket obexPacket = new ObexPacket(read, n);
        byte[] mPayload = null;
        if (n > 3) {
            final byte[] array = new byte[n - 3];
            read = inputStream.read(array);
            while (true) {
                mPayload = array;
                if (read == array.length) {
                    break;
                }
                read += inputStream.read(array, read, array.length - read);
            }
        }
        obexPacket.mPayload = mPayload;
        return obexPacket;
    }
    
    public static ObexPacket read(final InputStream inputStream) throws IOException {
        return read(inputStream.read(), inputStream);
    }
}
