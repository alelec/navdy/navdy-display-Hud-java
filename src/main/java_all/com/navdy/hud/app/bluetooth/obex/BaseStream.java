package com.navdy.hud.app.bluetooth.obex;

import java.io.IOException;

public interface BaseStream
{
    boolean continueOperation(final boolean p0, final boolean p1) throws IOException;
    
    void ensureNotDone() throws IOException;
    
    void ensureOpen() throws IOException;
    
    void streamClosed(final boolean p0) throws IOException;
}
