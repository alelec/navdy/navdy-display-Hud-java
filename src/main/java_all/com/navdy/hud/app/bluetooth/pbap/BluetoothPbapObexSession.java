package com.navdy.hud.app.bluetooth.pbap;

import com.navdy.hud.app.bluetooth.obex.HeaderSet;
import com.navdy.hud.app.bluetooth.obex.ClientSession;
import android.util.Log;
import com.navdy.hud.app.bluetooth.obex.ObexTransport;
import android.os.Handler;

final class BluetoothPbapObexSession
{
    static final int OBEX_SESSION_AUTHENTICATION_REQUEST = 105;
    static final int OBEX_SESSION_AUTHENTICATION_TIMEOUT = 106;
    static final int OBEX_SESSION_CONNECTED = 100;
    static final int OBEX_SESSION_DISCONNECTED = 102;
    static final int OBEX_SESSION_FAILED = 101;
    static final int OBEX_SESSION_REQUEST_COMPLETED = 103;
    static final int OBEX_SESSION_REQUEST_FAILED = 104;
    private static final byte[] PBAP_TARGET;
    private static final String TAG = "BTPbapObexSession";
    private BluetoothPbapObexAuthenticator mAuth;
    private ObexClientThread mObexClientThread;
    private Handler mSessionHandler;
    private final ObexTransport mTransport;
    
    static {
        PBAP_TARGET = new byte[] { 121, 97, 53, -16, -16, -59, 17, -40, 9, 102, 8, 0, 32, 12, -102, 102 };
    }
    
    public BluetoothPbapObexSession(final ObexTransport mTransport) {
        this.mAuth = null;
        this.mTransport = mTransport;
    }
    
    public void abort() {
        Log.d("BTPbapObexSession", "abort");
        if (this.mObexClientThread != null && this.mObexClientThread.mRequest != null) {
            new Thread() {
                @Override
                public void run() {
                    BluetoothPbapObexSession.this.mObexClientThread.mRequest.abort();
                }
            }.run();
        }
    }
    
    public boolean schedule(final BluetoothPbapRequest bluetoothPbapRequest) {
        Log.d("BTPbapObexSession", "schedule: " + bluetoothPbapRequest.getClass().getSimpleName());
        boolean schedule;
        if (this.mObexClientThread == null) {
            Log.e("BTPbapObexSession", "OBEX session not started");
            schedule = false;
        }
        else {
            schedule = this.mObexClientThread.schedule(bluetoothPbapRequest);
        }
        return schedule;
    }
    
    public boolean setAuthReply(final String reply) {
        Log.d("BTPbapObexSession", "setAuthReply key=" + reply);
        boolean b;
        if (this.mAuth == null) {
            b = false;
        }
        else {
            this.mAuth.setReply(reply);
            b = true;
        }
        return b;
    }
    
    public void start(final Handler mSessionHandler) {
        Log.d("BTPbapObexSession", "start");
        this.mSessionHandler = mSessionHandler;
        this.mAuth = new BluetoothPbapObexAuthenticator(this.mSessionHandler);
        (this.mObexClientThread = new ObexClientThread()).start();
    }
    
    public void stop() {
        Log.d("BTPbapObexSession", "stop");
        if (this.mObexClientThread == null) {
            return;
        }
        int n = 0;
        while (true) {
            try {
                Label_0065: {
                    if (!this.mObexClientThread.isAlive() || !this.mObexClientThread.isRunning()) {
                        break Label_0065;
                    }
                    n = 1;
                    Label_0059: {
                        if (n == 0) {
                            break Label_0059;
                        }
                        try {
                            this.mObexClientThread.interrupt();
                            try {
                                this.mObexClientThread.join();
                                this.mObexClientThread = null;
                                return;
                                n = 0;
                            }
                            catch (Throwable t) {}
                        }
                        catch (Throwable t2) {}
                    }
                }
            }
            catch (Throwable t3) {
                continue;
            }
            break;
        }
    }
    
    private class ObexClientThread extends Thread
    {
        private static final String TAG = "ObexClientThread";
        private ClientSession mClientSession;
        private BluetoothPbapRequest mRequest;
        private volatile boolean mRunning;
        
        public ObexClientThread() {
            this.mRunning = true;
            this.mClientSession = null;
            this.mRequest = null;
        }
        
        private boolean connect() {
            // 
            This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     1: istore_1       
            //     2: ldc             "ObexClientThread"
            //     4: ldc             "connect"
            //     6: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;)I
            //     9: pop            
            //    10: new             Lcom/navdy/hud/app/bluetooth/obex/ClientSession;
            //    13: astore_2       
            //    14: aload_2        
            //    15: aload_0        
            //    16: getfield        com/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread.this$0:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;
            //    19: invokestatic    com/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession.access$300:(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;)Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;
            //    22: invokespecial   com/navdy/hud/app/bluetooth/obex/ClientSession.<init>:(Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;)V
            //    25: aload_0        
            //    26: aload_2        
            //    27: putfield        com/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread.mClientSession:Lcom/navdy/hud/app/bluetooth/obex/ClientSession;
            //    30: aload_0        
            //    31: getfield        com/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread.mClientSession:Lcom/navdy/hud/app/bluetooth/obex/ClientSession;
            //    34: aload_0        
            //    35: getfield        com/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread.this$0:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;
            //    38: invokestatic    com/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession.access$400:(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;)Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexAuthenticator;
            //    41: invokevirtual   com/navdy/hud/app/bluetooth/obex/ClientSession.setAuthenticator:(Lcom/navdy/hud/app/bluetooth/obex/Authenticator;)V
            //    44: new             Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
            //    47: dup            
            //    48: invokespecial   com/navdy/hud/app/bluetooth/obex/HeaderSet.<init>:()V
            //    51: astore_2       
            //    52: aload_2        
            //    53: bipush          70
            //    55: invokestatic    com/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession.access$500:()[B
            //    58: invokevirtual   com/navdy/hud/app/bluetooth/obex/HeaderSet.setHeader:(ILjava/lang/Object;)V
            //    61: aload_0        
            //    62: getfield        com/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread.mClientSession:Lcom/navdy/hud/app/bluetooth/obex/ClientSession;
            //    65: aload_2        
            //    66: invokevirtual   com/navdy/hud/app/bluetooth/obex/ClientSession.connect:(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;)Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
            //    69: invokevirtual   com/navdy/hud/app/bluetooth/obex/HeaderSet.getResponseCode:()I
            //    72: sipush          160
            //    75: if_icmpeq       92
            //    78: aload_0        
            //    79: invokespecial   com/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread.disconnect:()V
            //    82: iload_1        
            //    83: ireturn        
            //    84: astore_2       
            //    85: goto            82
            //    88: astore_2       
            //    89: goto            82
            //    92: iconst_1       
            //    93: istore_1       
            //    94: goto            82
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                 
            //  -----  -----  -----  -----  ---------------------
            //  10     44     84     88     Ljava/lang/Throwable;
            //  61     82     88     92     Ljava/lang/Throwable;
            // 
            // The error that occurred was:
            // 
            // java.lang.IllegalStateException: Expression is linked from several locations: Label_0082:
            //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
            //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:556)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
            //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
            //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
            //     at java.lang.Thread.run(Unknown Source)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
        
        private void disconnect() {
            Log.d("ObexClientThread", "disconnect");
            if (this.mClientSession == null) {
                return;
            }
            try {
                this.mClientSession.disconnect(null);
                this.mClientSession.close();
            }
            catch (Throwable t) {}
        }
        
        public boolean isRunning() {
            return this.mRunning;
        }
        
        @Override
        public void run() {
            // 
            This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     1: invokespecial   java/lang/Thread.run:()V
            //     4: aload_0        
            //     5: invokespecial   com/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread.connect:()Z
            //     8: ifne            27
            //    11: aload_0        
            //    12: getfield        com/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread.this$0:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;
            //    15: invokestatic    com/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession.access$200:(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;)Landroid/os/Handler;
            //    18: bipush          101
            //    20: invokevirtual   android/os/Handler.obtainMessage:(I)Landroid/os/Message;
            //    23: invokevirtual   android/os/Message.sendToTarget:()V
            //    26: return         
            //    27: aload_0        
            //    28: getfield        com/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread.this$0:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;
            //    31: invokestatic    com/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession.access$200:(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;)Landroid/os/Handler;
            //    34: bipush          100
            //    36: invokevirtual   android/os/Handler.obtainMessage:(I)Landroid/os/Message;
            //    39: invokevirtual   android/os/Message.sendToTarget:()V
            //    42: aload_0        
            //    43: getfield        com/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread.mRunning:Z
            //    46: ifeq            136
            //    49: aload_0        
            //    50: dup            
            //    51: astore_1       
            //    52: monitorenter   
            //    53: aload_0        
            //    54: getfield        com/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread.mRequest:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;
            //    57: ifnonnull       64
            //    60: aload_0        
            //    61: invokevirtual   java/lang/Object.wait:()V
            //    64: aload_1        
            //    65: monitorexit    
            //    66: aload_0        
            //    67: getfield        com/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread.mRunning:Z
            //    70: ifeq            120
            //    73: aload_0        
            //    74: getfield        com/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread.mRequest:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;
            //    77: ifnull          120
            //    80: aload_0        
            //    81: getfield        com/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread.mRequest:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;
            //    84: aload_0        
            //    85: getfield        com/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread.mClientSession:Lcom/navdy/hud/app/bluetooth/obex/ClientSession;
            //    88: invokevirtual   com/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest.execute:(Lcom/navdy/hud/app/bluetooth/obex/ClientSession;)V
            //    91: aload_0        
            //    92: getfield        com/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread.mRequest:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;
            //    95: invokevirtual   com/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest.isSuccess:()Z
            //    98: ifeq            172
            //   101: aload_0        
            //   102: getfield        com/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread.this$0:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;
            //   105: invokestatic    com/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession.access$200:(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;)Landroid/os/Handler;
            //   108: bipush          103
            //   110: aload_0        
            //   111: getfield        com/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread.mRequest:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;
            //   114: invokevirtual   android/os/Handler.obtainMessage:(ILjava/lang/Object;)Landroid/os/Message;
            //   117: invokevirtual   android/os/Message.sendToTarget:()V
            //   120: aload_0        
            //   121: aconst_null    
            //   122: putfield        com/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread.mRequest:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;
            //   125: goto            42
            //   128: astore_2       
            //   129: aload_0        
            //   130: iconst_0       
            //   131: putfield        com/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread.mRunning:Z
            //   134: aload_1        
            //   135: monitorexit    
            //   136: aload_0        
            //   137: invokespecial   com/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread.disconnect:()V
            //   140: aload_0        
            //   141: getfield        com/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread.this$0:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;
            //   144: invokestatic    com/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession.access$200:(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;)Landroid/os/Handler;
            //   147: bipush          102
            //   149: invokevirtual   android/os/Handler.obtainMessage:(I)Landroid/os/Message;
            //   152: invokevirtual   android/os/Message.sendToTarget:()V
            //   155: goto            26
            //   158: astore_2       
            //   159: aload_1        
            //   160: monitorexit    
            //   161: aload_2        
            //   162: athrow         
            //   163: astore_2       
            //   164: aload_0        
            //   165: iconst_0       
            //   166: putfield        com/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread.mRunning:Z
            //   169: goto            91
            //   172: aload_0        
            //   173: getfield        com/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread.this$0:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;
            //   176: invokestatic    com/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession.access$200:(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;)Landroid/os/Handler;
            //   179: bipush          104
            //   181: aload_0        
            //   182: getfield        com/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread.mRequest:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;
            //   185: invokevirtual   android/os/Handler.obtainMessage:(ILjava/lang/Object;)Landroid/os/Message;
            //   188: invokevirtual   android/os/Message.sendToTarget:()V
            //   191: goto            120
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                            
            //  -----  -----  -----  -----  --------------------------------
            //  53     64     128    136    Ljava/lang/InterruptedException;
            //  53     64     158    163    Any
            //  64     66     158    163    Any
            //  80     91     163    172    Ljava/lang/Throwable;
            //  129    136    158    163    Any
            //  159    161    158    163    Any
            // 
            // The error that occurred was:
            // 
            // java.lang.IllegalStateException: Expression is linked from several locations: Label_0091:
            //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
            //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:556)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
            //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
            //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
            //     at java.lang.Thread.run(Unknown Source)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
        
        public boolean schedule(final BluetoothPbapRequest mRequest) {
            synchronized (this) {
                Log.d("ObexClientThread", "schedule: " + mRequest.getClass().getSimpleName());
                boolean b;
                if (this.mRequest != null) {
                    b = false;
                }
                else {
                    this.mRequest = mRequest;
                    this.notify();
                    b = true;
                }
                return b;
            }
        }
    }
}
