package com.navdy.hud.app.bluetooth.pbap.utils;

import java.util.Iterator;
import java.util.Map;
import java.nio.ByteBuffer;
import java.io.IOException;
import com.navdy.hud.app.bluetooth.obex.HeaderSet;
import java.util.HashMap;

public final class ObexAppParameters
{
    private final HashMap<Byte, byte[]> mParams;
    
    public ObexAppParameters() {
        this.mParams = new HashMap<Byte, byte[]>();
    }
    
    public ObexAppParameters(final byte[] array) {
        this.mParams = new HashMap<Byte, byte[]>();
        if (array != null) {
            int n3;
            byte b2;
            for (int n = 0; n < array.length && array.length - n >= 2; n = n3 + b2) {
                final int n2 = n + 1;
                final byte b = array[n];
                n3 = n2 + 1;
                b2 = array[n2];
                if (array.length - n3 - b2 < 0) {
                    break;
                }
                final byte[] array2 = new byte[b2];
                System.arraycopy(array, n3, array2, 0, b2);
                this.add(b, array2);
            }
        }
    }
    
    public static ObexAppParameters fromHeaderSet(final HeaderSet set) {
        try {
            return new ObexAppParameters((byte[])set.getHeader(76));
        }
        catch (IOException ex) {
            return null;
        }
    }
    
    public void add(final byte b, final byte b2) {
        this.mParams.put(b, ByteBuffer.allocate(1).put(b2).array());
    }
    
    public void add(final byte b, final int n) {
        this.mParams.put(b, ByteBuffer.allocate(4).putInt(n).array());
    }
    
    public void add(final byte b, final long n) {
        this.mParams.put(b, ByteBuffer.allocate(8).putLong(n).array());
    }
    
    public void add(final byte b, final String s) {
        this.mParams.put(b, s.getBytes());
    }
    
    public void add(final byte b, final short n) {
        this.mParams.put(b, ByteBuffer.allocate(2).putShort(n).array());
    }
    
    public void add(final byte b, final byte[] array) {
        this.mParams.put(b, array);
    }
    
    public void addToHeaderSet(final HeaderSet set) {
        if (this.mParams.size() > 0) {
            set.setHeader(76, this.getHeader());
        }
    }
    
    public boolean exists(final byte b) {
        return this.mParams.containsKey(b);
    }
    
    public byte getByte(final byte b) {
        final byte[] array = this.mParams.get(b);
        byte value;
        if (array == null || array.length < 1) {
            value = 0;
        }
        else {
            value = ByteBuffer.wrap(array).get();
        }
        return value;
    }
    
    public byte[] getByteArray(final byte b) {
        return this.mParams.get(b);
    }
    
    public byte[] getHeader() {
        int n = 0;
        final Iterator<Map.Entry<Byte, byte[]>> iterator = this.mParams.entrySet().iterator();
        while (iterator.hasNext()) {
            n += iterator.next().getValue().length + 2;
        }
        final byte[] array = new byte[n];
        int n2 = 0;
        for (final Map.Entry<Byte, byte[]> entry : this.mParams.entrySet()) {
            final int length = entry.getValue().length;
            final int n3 = n2 + 1;
            array[n2] = entry.getKey();
            final int n4 = n3 + 1;
            array[n3] = (byte)length;
            System.arraycopy(entry.getValue(), 0, array, n4, length);
            n2 = n4 + length;
        }
        return array;
    }
    
    public int getInt(final byte b) {
        final byte[] array = this.mParams.get(b);
        int int1;
        if (array == null || array.length < 4) {
            int1 = 0;
        }
        else {
            int1 = ByteBuffer.wrap(array).getInt();
        }
        return int1;
    }
    
    public short getShort(final byte b) {
        final byte[] array = this.mParams.get(b);
        short short1;
        if (array == null || array.length < 2) {
            short1 = 0;
        }
        else {
            short1 = ByteBuffer.wrap(array).getShort();
        }
        return short1;
    }
    
    public String getString(final byte b) {
        final byte[] array = this.mParams.get(b);
        String s;
        if (array == null) {
            s = null;
        }
        else {
            s = new String(array);
        }
        return s;
    }
    
    @Override
    public String toString() {
        return this.mParams.toString();
    }
}
