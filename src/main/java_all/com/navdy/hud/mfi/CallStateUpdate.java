package com.navdy.hud.mfi;

public class CallStateUpdate
{
    public String addressBookID;
    public String callUUID;
    public int conferenceGroup;
    public Direction direction;
    public DisconnectReason disconnectReason;
    public String displayName;
    public boolean isConferenced;
    public String label;
    public String remoteID;
    public IAPCommunicationsManager.Service service;
    public Status status;
    
    public CallStateUpdate() {
        this.status = Status.Disconnected;
        this.direction = Direction.Unknown;
        this.service = IAPCommunicationsManager.Service.Unknown;
        this.disconnectReason = DisconnectReason.Ended;
    }
    
    public enum Direction
    {
        Incoming, 
        Outgoing, 
        Unknown;
    }
    
    public enum DisconnectReason
    {
        Declined, 
        Ended, 
        Failed;
    }
    
    public enum Status
    {
        Active, 
        Connecting, 
        Disconnected, 
        Disconnecting, 
        Held, 
        Ringing, 
        Sending;
    }
}
