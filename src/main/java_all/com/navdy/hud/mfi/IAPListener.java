package com.navdy.hud.mfi;

public interface IAPListener
{
    void onCoprocessorStatusCheckFailed(final String p0, final String p1, final String p2);
    
    void onDeviceAuthenticationSuccess(final int p0);
}
