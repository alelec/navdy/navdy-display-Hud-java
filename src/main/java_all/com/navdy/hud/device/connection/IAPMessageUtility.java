package com.navdy.hud.device.connection;

import com.navdy.hud.mfi.IAPCommunicationsManager;
import com.navdy.service.library.events.callcontrol.TelephonyRequest;
import android.text.TextUtils;
import com.navdy.service.library.events.callcontrol.PhoneStatus;
import com.navdy.service.library.events.callcontrol.PhoneEvent;
import com.navdy.service.library.events.callcontrol.CallAction;
import com.navdy.hud.mfi.CallStateUpdate;
import com.navdy.service.library.events.audio.MusicTrackInfo;
import java.util.HashMap;
import com.navdy.service.library.events.audio.MusicShuffleMode;
import com.navdy.service.library.events.audio.MusicRepeatMode;
import com.navdy.service.library.events.audio.MusicPlaybackState;
import com.navdy.hud.mfi.NowPlayingUpdate;
import java.util.Map;

public class IAPMessageUtility
{
    private static final Map<NowPlayingUpdate.PlaybackStatus, MusicPlaybackState> PLAYBACK_STATES;
    private static final Map<NowPlayingUpdate.PlaybackRepeat, MusicRepeatMode> REPEAT_MODES;
    private static final Map<NowPlayingUpdate.PlaybackShuffle, MusicShuffleMode> SHUFFLE_MODES;
    
    static {
        PLAYBACK_STATES = new HashMap<NowPlayingUpdate.PlaybackStatus, MusicPlaybackState>() {
            {
                this.put(NowPlayingUpdate.PlaybackStatus.Stopped, MusicPlaybackState.PLAYBACK_STOPPED);
                this.put(NowPlayingUpdate.PlaybackStatus.Playing, MusicPlaybackState.PLAYBACK_PLAYING);
                this.put(NowPlayingUpdate.PlaybackStatus.Paused, MusicPlaybackState.PLAYBACK_PAUSED);
                this.put(NowPlayingUpdate.PlaybackStatus.SeekForward, MusicPlaybackState.PLAYBACK_FAST_FORWARDING);
                this.put(NowPlayingUpdate.PlaybackStatus.SeekBackward, MusicPlaybackState.PLAYBACK_REWINDING);
            }
        };
        SHUFFLE_MODES = new HashMap<NowPlayingUpdate.PlaybackShuffle, MusicShuffleMode>() {
            {
                this.put(NowPlayingUpdate.PlaybackShuffle.Off, MusicShuffleMode.MUSIC_SHUFFLE_MODE_OFF);
                this.put(NowPlayingUpdate.PlaybackShuffle.Songs, MusicShuffleMode.MUSIC_SHUFFLE_MODE_SONGS);
                this.put(NowPlayingUpdate.PlaybackShuffle.Albums, MusicShuffleMode.MUSIC_SHUFFLE_MODE_ALBUMS);
            }
        };
        REPEAT_MODES = new HashMap<NowPlayingUpdate.PlaybackRepeat, MusicRepeatMode>() {
            {
                this.put(NowPlayingUpdate.PlaybackRepeat.Off, MusicRepeatMode.MUSIC_REPEAT_MODE_OFF);
                this.put(NowPlayingUpdate.PlaybackRepeat.One, MusicRepeatMode.MUSIC_REPEAT_MODE_ONE);
                this.put(NowPlayingUpdate.PlaybackRepeat.All, MusicRepeatMode.MUSIC_REPEAT_MODE_ALL);
            }
        };
    }
    
    public static MusicTrackInfo getMusicTrackInfoForNowPlayingUpdate(final NowPlayingUpdate nowPlayingUpdate) {
        MusicTrackInfo build;
        if (nowPlayingUpdate == null) {
            build = null;
        }
        else {
            final MusicTrackInfo.Builder repeatMode = new MusicTrackInfo.Builder().playbackState(IAPMessageUtility.PLAYBACK_STATES.get(nowPlayingUpdate.mPlaybackStatus)).name(nowPlayingUpdate.mediaItemTitle).album(nowPlayingUpdate.mediaItemAlbumTitle).author(nowPlayingUpdate.mediaItemArtist).duration((int)nowPlayingUpdate.mediaItemPlaybackDurationInMilliseconds).currentPosition((int)nowPlayingUpdate.mPlaybackElapsedTimeMilliseconds).shuffleMode(IAPMessageUtility.SHUFFLE_MODES.get(nowPlayingUpdate.playbackShuffle)).repeatMode(IAPMessageUtility.REPEAT_MODES.get(nowPlayingUpdate.playbackRepeat));
            if (nowPlayingUpdate.mediaItemPersistentIdentifier != null) {
                repeatMode.trackId(nowPlayingUpdate.mediaItemPersistentIdentifier.toString());
            }
            repeatMode.isPreviousAllowed(true).isNextAllowed(true);
            build = repeatMode.build();
        }
        return build;
    }
    
    public static PhoneEvent getPhoneEventForCallStateUpdate(final CallStateUpdate callStateUpdate, final CallAction callAction) {
        PhoneEvent build = null;
        if (callStateUpdate != null) {
            final PhoneEvent.Builder builder = new PhoneEvent.Builder();
            build = build;
            switch (callStateUpdate.status) {
                case Disconnecting:
                    return build;
                case Disconnected:
                    if (callAction != null && callAction == CallAction.CALL_DIAL) {
                        builder.status(PhoneStatus.PHONE_DISCONNECTING);
                        break;
                    }
                    builder.status(PhoneStatus.PHONE_IDLE);
                    break;
                case Sending:
                    builder.status(PhoneStatus.PHONE_DIALING);
                    break;
                case Ringing:
                    builder.status(PhoneStatus.PHONE_RINGING);
                    break;
                case Connecting:
                    builder.status(PhoneStatus.PHONE_OFFHOOK);
                    break;
                case Active:
                    builder.status(PhoneStatus.PHONE_OFFHOOK);
                    break;
                case Held:
                    builder.status(PhoneStatus.PHONE_HELD);
                    break;
            }
            if (!TextUtils.isEmpty((CharSequence)callStateUpdate.displayName)) {
                builder.contact_name(callStateUpdate.displayName);
            }
            if (!TextUtils.isEmpty((CharSequence)callStateUpdate.remoteID)) {
                builder.number(callStateUpdate.remoteID);
            }
            if (!TextUtils.isEmpty((CharSequence)callStateUpdate.label)) {
                builder.label(callStateUpdate.label);
            }
            if (!TextUtils.isEmpty((CharSequence)callStateUpdate.callUUID)) {
                builder.callUUID(callStateUpdate.callUUID);
            }
            build = builder.build();
        }
        return build;
    }
    
    public static boolean performActionForTelephonyRequest(final TelephonyRequest telephonyRequest, final IAPCommunicationsManager iapCommunicationsManager) {
        boolean b = false;
        if (telephonyRequest != null) {
            switch (telephonyRequest.action) {
                default:
                    b = b;
                    return b;
                case CALL_ACCEPT:
                    if (!TextUtils.isEmpty((CharSequence)telephonyRequest.callUUID)) {
                        iapCommunicationsManager.acceptCall(telephonyRequest.callUUID);
                        break;
                    }
                    iapCommunicationsManager.acceptCall();
                    break;
                case CALL_END:
                case CALL_REJECT:
                    if (!TextUtils.isEmpty((CharSequence)telephonyRequest.callUUID)) {
                        iapCommunicationsManager.endCall(telephonyRequest.callUUID);
                        break;
                    }
                    iapCommunicationsManager.endCall();
                    break;
                case CALL_DIAL:
                    if (!TextUtils.isEmpty((CharSequence)telephonyRequest.number)) {
                        iapCommunicationsManager.initiateDestinationCall(telephonyRequest.number);
                        break;
                    }
                    break;
                case CALL_MUTE:
                    iapCommunicationsManager.mute();
                    break;
                case CALL_UNMUTE:
                    iapCommunicationsManager.unMute();
                    break;
            }
            b = true;
        }
        return b;
    }
}
