package com.navdy.service.library.device.discovery;

import java.util.List;
import java.util.Arrays;
import com.navdy.service.library.device.connection.ConnectionInfo;
import android.content.Context;
import com.navdy.service.library.util.Listenable;

public abstract class RemoteDeviceScanner extends Listenable<Listener>
{
    protected Context mContext;
    
    RemoteDeviceScanner(final Context mContext) {
        this.mContext = mContext;
    }
    
    protected void dispatchOnDiscovered(final ConnectionInfo connectionInfo) {
        this.dispatchOnDiscovered(Arrays.<ConnectionInfo>asList(connectionInfo));
    }
    
    protected void dispatchOnDiscovered(final List<ConnectionInfo> list) {
        this.dispatchToListeners((EventDispatcher)new ScanEventDispatcher() {
            public void dispatchEvent(final RemoteDeviceScanner remoteDeviceScanner, final RemoteDeviceScanner.Listener listener) {
                listener.onDiscovered(remoteDeviceScanner, list);
            }
        });
    }
    
    protected void dispatchOnLost(final List<ConnectionInfo> list) {
        this.dispatchToListeners((EventDispatcher)new ScanEventDispatcher() {
            public void dispatchEvent(final RemoteDeviceScanner remoteDeviceScanner, final RemoteDeviceScanner.Listener listener) {
                listener.onLost(remoteDeviceScanner, list);
            }
        });
    }
    
    protected void dispatchOnScanStarted() {
        this.dispatchToListeners((EventDispatcher)new ScanEventDispatcher() {
            public void dispatchEvent(final RemoteDeviceScanner remoteDeviceScanner, final RemoteDeviceScanner.Listener listener) {
                listener.onScanStarted(remoteDeviceScanner);
            }
        });
    }
    
    protected void dispatchOnScanStopped() {
        this.dispatchToListeners((EventDispatcher)new ScanEventDispatcher() {
            public void dispatchEvent(final RemoteDeviceScanner remoteDeviceScanner, final RemoteDeviceScanner.Listener listener) {
                listener.onScanStopped(remoteDeviceScanner);
            }
        });
    }
    
    public abstract boolean startScan();
    
    public abstract boolean stopScan();
    
    public interface Listener extends Listenable.Listener
    {
        void onDiscovered(final RemoteDeviceScanner p0, final List<ConnectionInfo> p1);
        
        void onLost(final RemoteDeviceScanner p0, final List<ConnectionInfo> p1);
        
        void onScanStarted(final RemoteDeviceScanner p0);
        
        void onScanStopped(final RemoteDeviceScanner p0);
    }
    
    protected interface ScanEventDispatcher extends EventDispatcher<RemoteDeviceScanner, RemoteDeviceScanner.Listener>
    {
    }
}
