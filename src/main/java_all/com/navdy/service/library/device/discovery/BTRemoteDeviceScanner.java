package com.navdy.service.library.device.discovery;

import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.device.connection.ConnectionInfo;
import com.navdy.service.library.device.connection.BTConnectionInfo;
import com.navdy.service.library.device.connection.ConnectionType;
import com.navdy.service.library.device.connection.ServiceAddress;
import com.navdy.service.library.device.connection.ConnectionService;
import com.navdy.service.library.device.NavdyDeviceId;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import java.util.HashSet;
import android.content.IntentFilter;
import android.content.Context;
import android.content.BroadcastReceiver;
import java.util.Set;
import android.bluetooth.BluetoothAdapter;
import com.navdy.service.library.log.Logger;

public class BTRemoteDeviceScanner extends RemoteDeviceScanner
{
    private static final Logger sLogger;
    private BluetoothAdapter btAdapter;
    private Set<String> devicesSeen;
    private Runnable handleStartScan;
    private Runnable handleStopScan;
    private final BroadcastReceiver mReceiver;
    private boolean scanning;
    private int taskQueue;
    
    static {
        sLogger = new Logger(BTRemoteDeviceScanner.class);
    }
    
    public BTRemoteDeviceScanner(final Context context, final int taskQueue) {
        super(context);
        this.scanning = false;
        this.handleStartScan = new Runnable() {
            @Override
            public void run() {
                if (!BTRemoteDeviceScanner.this.scanning) {
                    BTRemoteDeviceScanner.this.scanning = true;
                    final IntentFilter intentFilter = new IntentFilter();
                    intentFilter.addAction("android.bluetooth.device.action.FOUND");
                    intentFilter.addAction("android.bluetooth.device.action.UUID");
                    intentFilter.addAction("android.bluetooth.device.action.NAME_CHANGED");
                    intentFilter.addAction("android.bluetooth.adapter.action.DISCOVERY_FINISHED");
                    BTRemoteDeviceScanner.this.mContext.registerReceiver(BTRemoteDeviceScanner.this.mReceiver, intentFilter);
                    BTRemoteDeviceScanner.this.devicesSeen = (Set<String>)new HashSet();
                    if (BTRemoteDeviceScanner.this.btAdapter != null) {
                        if (BTRemoteDeviceScanner.this.btAdapter.isDiscovering()) {
                            BTRemoteDeviceScanner.this.btAdapter.cancelDiscovery();
                        }
                        BTRemoteDeviceScanner.this.btAdapter.startDiscovery();
                        BTRemoteDeviceScanner.this.dispatchOnScanStarted();
                    }
                }
            }
        };
        this.handleStopScan = new Runnable() {
            @Override
            public void run() {
                if (BTRemoteDeviceScanner.this.scanning) {
                    BTRemoteDeviceScanner.this.scanning = false;
                    if (BTRemoteDeviceScanner.this.btAdapter != null) {
                        BTRemoteDeviceScanner.this.btAdapter.cancelDiscovery();
                    }
                    BTRemoteDeviceScanner.this.mContext.unregisterReceiver(BTRemoteDeviceScanner.this.mReceiver);
                }
            }
        };
        this.mReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                final String action = intent.getAction();
                if ("android.bluetooth.device.action.FOUND".equals(action) || "android.bluetooth.device.action.NAME_CHANGED".equals(action)) {
                    final BluetoothDevice bluetoothDevice = (BluetoothDevice)intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                    final String stringExtra = intent.getStringExtra("android.bluetooth.device.extra.NAME");
                    final String address = bluetoothDevice.getAddress();
                    BTRemoteDeviceScanner.sLogger.d("Scanned: " + address + " - name = " + stringExtra);
                    if (stringExtra != null && BTDeviceBroadcaster.isDisplay(stringExtra) && !BTRemoteDeviceScanner.this.devicesSeen.contains(address)) {
                        BTRemoteDeviceScanner.this.devicesSeen.add(address);
                        BTRemoteDeviceScanner.this.dispatchOnDiscovered(new BTConnectionInfo(new NavdyDeviceId(NavdyDeviceId.Type.BT, address, stringExtra), new ServiceAddress(address, ConnectionService.NAVDY_PROTO_SERVICE_UUID.toString()), ConnectionType.BT_PROTOBUF));
                    }
                }
                else if ("android.bluetooth.adapter.action.DISCOVERY_FINISHED".equals(action)) {
                    BTRemoteDeviceScanner.this.dispatchOnScanStopped();
                }
            }
        };
        this.taskQueue = taskQueue;
        this.btAdapter = BluetoothAdapter.getDefaultAdapter();
    }
    
    @Override
    public boolean startScan() {
        TaskManager.getInstance().execute(this.handleStartScan, this.taskQueue);
        return true;
    }
    
    @Override
    public boolean stopScan() {
        TaskManager.getInstance().execute(this.handleStopScan, this.taskQueue);
        return true;
    }
}
