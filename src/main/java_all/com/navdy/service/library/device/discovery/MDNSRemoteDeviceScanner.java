package com.navdy.service.library.device.discovery;

import java.util.TimerTask;
import com.navdy.service.library.device.connection.ConnectionInfo;
import android.os.Looper;
import java.util.LinkedList;
import android.content.Context;
import java.util.Queue;
import android.net.nsd.NsdServiceInfo;
import android.net.nsd.NsdManager;
import java.util.ArrayList;
import android.net.nsd.NsdManager;
import android.os.Handler;
import android.net.nsd.NsdManager;
import java.util.Timer;
import com.navdy.service.library.log.Logger;

public class MDNSRemoteDeviceScanner extends RemoteDeviceScanner
{
    public static final Logger sLogger;
    protected Timer discoveryTimer;
    protected NsdManager$DiscoveryListener mDiscoveryListener;
    protected boolean mDiscoveryRunning;
    protected Handler mHandler;
    protected NsdManager mNsdManager;
    protected ArrayList<NsdManager$ResolveListener> mResolveListeners;
    protected NsdServiceInfo mResolvingService;
    protected final String mServiceType;
    protected Queue<NsdServiceInfo> mUnresolvedServices;
    
    static {
        sLogger = new Logger(MDNSRemoteDeviceScanner.class);
    }
    
    public MDNSRemoteDeviceScanner(final Context context, final String mServiceType) {
        super(context);
        this.mServiceType = mServiceType;
        this.mResolveListeners = new ArrayList<NsdManager$ResolveListener>();
        this.mUnresolvedServices = new LinkedList<NsdServiceInfo>();
        this.mHandler = new Handler(Looper.getMainLooper());
    }
    
    public NsdManager$ResolveListener getNewResolveListener() {
        return (NsdManager$ResolveListener)new NsdManager$ResolveListener() {
            public void onResolveFailed(final NsdServiceInfo nsdServiceInfo, final int n) {
                MDNSRemoteDeviceScanner.this.mResolveListeners.remove(this);
                MDNSRemoteDeviceScanner.sLogger.e("Resolve failed" + n);
                MDNSRemoteDeviceScanner.this.resolveComplete();
            }
            
            public void onServiceResolved(final NsdServiceInfo nsdServiceInfo) {
                MDNSRemoteDeviceScanner.this.mResolveListeners.remove(this);
                MDNSRemoteDeviceScanner.sLogger.e("Resolve Succeeded. " + nsdServiceInfo);
                try {
                    MDNSRemoteDeviceScanner.this.mHandler.post((Runnable)new Runnable() {
                        final /* synthetic */ ConnectionInfo val$connectionInfo = ConnectionInfo.fromServiceInfo(nsdServiceInfo);
                        
                        @Override
                        public void run() {
                            MDNSRemoteDeviceScanner.this.dispatchOnDiscovered(this.val$connectionInfo);
                        }
                    });
                    MDNSRemoteDeviceScanner.this.resolveComplete();
                }
                catch (IllegalArgumentException ex) {
                    MDNSRemoteDeviceScanner.sLogger.e("Unable to process serviceInfo");
                }
            }
        };
    }
    
    public void initNsd() {
        this.initializeDiscoveryListener();
    }
    
    public void initializeDiscoveryListener() {
        if (this.mNsdManager == null) {
            this.mNsdManager = (NsdManager)this.mContext.getSystemService("servicediscovery");
        }
        if (this.mDiscoveryListener == null) {
            this.mDiscoveryListener = (NsdManager$DiscoveryListener)new NsdManager$DiscoveryListener() {
                public void onDiscoveryStarted(final String s) {
                    MDNSRemoteDeviceScanner.this.mDiscoveryRunning = true;
                    MDNSRemoteDeviceScanner.this.discoveryTimer = null;
                    MDNSRemoteDeviceScanner.sLogger.d("Service discovery started");
                    MDNSRemoteDeviceScanner.this.dispatchOnScanStarted();
                }
                
                public void onDiscoveryStopped(final String s) {
                    MDNSRemoteDeviceScanner.this.mDiscoveryRunning = false;
                    MDNSRemoteDeviceScanner.sLogger.i("Discovery stopped: " + s);
                    MDNSRemoteDeviceScanner.this.dispatchOnScanStopped();
                }
                
                public void onServiceFound(final NsdServiceInfo nsdServiceInfo) {
                    MDNSRemoteDeviceScanner.sLogger.d("Service discovery success " + nsdServiceInfo);
                    if (ConnectionInfo.isValidNavdyServiceInfo(nsdServiceInfo)) {
                        MDNSRemoteDeviceScanner.this.resolveService(nsdServiceInfo);
                    }
                }
                
                public void onServiceLost(final NsdServiceInfo nsdServiceInfo) {
                    MDNSRemoteDeviceScanner.sLogger.e("service lost" + nsdServiceInfo);
                }
                
                public void onStartDiscoveryFailed(final String s, final int n) {
                    MDNSRemoteDeviceScanner.sLogger.e("Start Discovery failed: Error code:" + n);
                }
                
                public void onStopDiscoveryFailed(final String s, final int n) {
                    MDNSRemoteDeviceScanner.sLogger.e("Stop Discovery failed: Error code:" + n);
                }
            };
        }
    }
    
    protected void resolveComplete() {
        this.mResolvingService = null;
        if (this.mUnresolvedServices.size() > 0) {
            this.resolveService(this.mUnresolvedServices.remove());
        }
    }
    
    protected void resolveService(final NsdServiceInfo mResolvingService) {
        if (this.mResolvingService == null) {
            this.mResolvingService = mResolvingService;
            final NsdManager$ResolveListener newResolveListener = this.getNewResolveListener();
            this.mResolveListeners.add(newResolveListener);
            this.mNsdManager.resolveService(mResolvingService, newResolveListener);
        }
        else {
            this.mUnresolvedServices.add(mResolvingService);
        }
    }
    
    @Override
    public boolean startScan() {
        boolean b = true;
        MDNSRemoteDeviceScanner.sLogger.e("Starting scan: " + this.mServiceType);
        this.initNsd();
        if (this.mNsdManager == null) {
            MDNSRemoteDeviceScanner.sLogger.e("Can't scan: no NsdManager: " + this.mServiceType);
            b = false;
        }
        else if (this.discoveryTimer != null || this.mDiscoveryRunning) {
            MDNSRemoteDeviceScanner.sLogger.e("Can't start: already started " + this.mServiceType);
        }
        else {
            (this.discoveryTimer = new Timer()).schedule(new TimerTask() {
                @Override
                public void run() {
                    MDNSRemoteDeviceScanner.sLogger.e("Scan timer fired: " + MDNSRemoteDeviceScanner.this.mServiceType);
                    MDNSRemoteDeviceScanner.this.mNsdManager.discoverServices(MDNSRemoteDeviceScanner.this.mServiceType, 1, MDNSRemoteDeviceScanner.this.mDiscoveryListener);
                    MDNSRemoteDeviceScanner.sLogger.e("Scan started: " + MDNSRemoteDeviceScanner.this.mServiceType);
                }
            }, 2000L);
        }
        return b;
    }
    
    @Override
    public boolean stopScan() {
        boolean b = false;
        MDNSRemoteDeviceScanner.sLogger.e("Stopping scan: " + this.mServiceType);
        if (this.mNsdManager == null) {
            MDNSRemoteDeviceScanner.sLogger.e("Can't stop: already stopped: " + this.mServiceType);
        }
        else if (this.discoveryTimer != null) {
            this.discoveryTimer.cancel();
            this.discoveryTimer = null;
            b = true;
        }
        else {
            Label_0119: {
                if (this.mDiscoveryListener == null) {
                    break Label_0119;
                }
                while (true) {
                    try {
                        this.mNsdManager.stopServiceDiscovery(this.mDiscoveryListener);
                        this.mDiscoveryListener = null;
                        this.mDiscoveryRunning = false;
                        b = true;
                    }
                    catch (Exception ex) {
                        if (this.mDiscoveryRunning) {
                            MDNSRemoteDeviceScanner.sLogger.e("Problem stopping nsd service discovery", ex);
                        }
                        continue;
                    }
                    break;
                }
            }
        }
        return b;
    }
}
