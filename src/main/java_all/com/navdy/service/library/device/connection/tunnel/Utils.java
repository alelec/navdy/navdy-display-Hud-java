package com.navdy.service.library.device.connection.tunnel;

public class Utils
{
    static final char[] hexArray;
    
    static {
        hexArray = "0123456789ABCDEF".toCharArray();
    }
    
    public static String bytesToHex(final byte[] array) {
        return bytesToHex(array, false);
    }
    
    public static String bytesToHex(final byte[] array, final boolean b) {
        int n;
        if (b) {
            n = 3;
        }
        else {
            n = 2;
        }
        final char[] array2 = new char[array.length * n];
        for (int i = 0; i < array.length; ++i) {
            final int n2 = array[i] & 0xFF;
            array2[i * n] = Utils.hexArray[n2 >>> 4];
            array2[i * n + 1] = Utils.hexArray[n2 & 0xF];
            if (b) {
                array2[i * n + 2] = 32;
            }
        }
        return new String(array2);
    }
    
    public static String toASCII(final byte[] array) {
        final StringBuilder sb = new StringBuilder();
        for (int length = array.length, i = 0; i < length; ++i) {
            final int n = array[i] & 0xFF;
            char c;
            if (n >= 32 && n < 128) {
                c = (char)n;
            }
            else {
                c = '.';
            }
            sb.append(c);
        }
        return sb.toString();
    }
}
