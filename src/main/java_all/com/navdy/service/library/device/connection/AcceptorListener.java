package com.navdy.service.library.device.connection;

import com.navdy.service.library.network.SocketAdapter;
import java.io.Closeable;
import com.navdy.service.library.util.IOUtils;
import java.io.IOException;
import android.content.Context;
import com.navdy.service.library.network.SocketAcceptor;

public class AcceptorListener extends ConnectionListener
{
    private SocketAcceptor acceptor;
    private ConnectionType connectionType;
    
    public AcceptorListener(final Context context, final SocketAcceptor acceptor, final ConnectionType connectionType) {
        super(context, "Acceptor/" + acceptor.getClass().getSimpleName());
        this.acceptor = acceptor;
        this.connectionType = connectionType;
    }
    
    protected AcceptThread getNewAcceptThread() throws IOException {
        return new AcceptThread();
    }
    
    @Override
    public ConnectionType getType() {
        return this.connectionType;
    }
    
    @Override
    public String toString() {
        final StringBuilder append = new StringBuilder().append(this.getClass().getSimpleName());
        String string;
        if (this.acceptor != null) {
            string = "/" + this.acceptor.getClass().getSimpleName();
        }
        else {
            string = "";
        }
        return append.append(string).toString();
    }
    
    private class AcceptThread extends ConnectionListener.AcceptThread
    {
        private volatile boolean closing;
        
        public AcceptThread() throws IOException {
            this.setName("AcceptThread-" + AcceptorListener.this.acceptor.getClass().getSimpleName());
        }
        
        public void cancel() {
            AcceptorListener.this.logger.d("Socket cancel " + this);
            this.closing = true;
            IOUtils.closeStream(AcceptorListener.this.acceptor);
        }
        
        @Override
        public void run() {
            AcceptorListener.this.logger.d(this.getName() + " started, closing:" + this.closing);
            AcceptorListener.this.dispatchStarted();
            Closeable accept = null;
            while (true) {
                try {
                    final SocketAdapter socketAdapter = (SocketAdapter)(accept = AcceptorListener.this.acceptor.accept());
                    final ConnectionInfo remoteConnectionInfo = AcceptorListener.this.acceptor.getRemoteConnectionInfo(socketAdapter, AcceptorListener.this.connectionType);
                    accept = socketAdapter;
                    accept = socketAdapter;
                    final SocketConnection socketConnection = new SocketConnection(remoteConnectionInfo, socketAdapter);
                    accept = socketAdapter;
                    AcceptorListener.this.dispatchConnected(socketConnection);
                    this.cancel();
                    AcceptorListener.this.dispatchStopped();
                    AcceptorListener.this.logger.i("END " + this.getName());
                }
                catch (Throwable t) {
                    if (!this.closing) {
                        AcceptorListener.this.logger.e("Socket accept() failed", t);
                    }
                    IOUtils.closeStream(accept);
                    this.cancel();
                    continue;
                }
                finally {
                    this.cancel();
                }
                break;
            }
        }
    }
}
