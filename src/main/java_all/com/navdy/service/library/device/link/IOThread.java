package com.navdy.service.library.device.link;

import com.navdy.service.library.log.Logger;

public class IOThread extends Thread
{
    private static final int SHUTDOWN_TIMEOUT = 1000;
    protected volatile boolean closing;
    protected final Logger logger;
    
    public IOThread() {
        this.logger = new Logger(this.getClass());
        this.closing = false;
    }
    
    public void cancel() {
        this.closing = true;
        this.interrupt();
        while (true) {
            try {
                this.join(1000L);
                if (this.isAlive()) {
                    this.logger.w("Thread still alive after join");
                }
            }
            catch (InterruptedException ex) {
                this.logger.e("Interrupted");
                continue;
            }
            break;
        }
    }
    
    public boolean isClosing() {
        return this.closing;
    }
}
