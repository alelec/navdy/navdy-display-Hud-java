package com.navdy.service.library.events.callcontrol;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class CallStateUpdateRequest extends Message
{
    public static final Boolean DEFAULT_START;
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.BOOL)
    public final Boolean start;
    
    static {
        DEFAULT_START = false;
    }
    
    private CallStateUpdateRequest(final Builder builder) {
        this(builder.start);
        this.setBuilder((Message.Builder)builder);
    }
    
    public CallStateUpdateRequest(final Boolean start) {
        this.start = start;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof CallStateUpdateRequest && this.equals(this.start, ((CallStateUpdateRequest)o).start));
    }
    
    @Override
    public int hashCode() {
        int hashCode = this.hashCode;
        if (hashCode == 0) {
            if (this.start != null) {
                hashCode = this.start.hashCode();
            }
            else {
                hashCode = 0;
            }
            this.hashCode = hashCode;
        }
        return hashCode;
    }
    
    public static final class Builder extends Message.Builder<CallStateUpdateRequest>
    {
        public Boolean start;
        
        public Builder() {
        }
        
        public Builder(final CallStateUpdateRequest callStateUpdateRequest) {
            super(callStateUpdateRequest);
            if (callStateUpdateRequest != null) {
                this.start = callStateUpdateRequest.start;
            }
        }
        
        public CallStateUpdateRequest build() {
            ((Message.Builder)this).checkRequiredFields();
            return new CallStateUpdateRequest(this, null);
        }
        
        public Builder start(final Boolean start) {
            this.start = start;
            return this;
        }
    }
}
