package com.navdy.service.library.events.file;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class FileTransferResponse extends Message
{
    public static final String DEFAULT_CHECKSUM = "";
    public static final String DEFAULT_DESTINATIONFILENAME = "";
    public static final FileTransferError DEFAULT_ERROR;
    public static final FileType DEFAULT_FILETYPE;
    public static final Integer DEFAULT_MAXCHUNKSIZE;
    public static final Long DEFAULT_OFFSET;
    public static final Boolean DEFAULT_SUCCESS;
    public static final Boolean DEFAULT_SUPPORTSACKS;
    public static final Integer DEFAULT_TRANSFERID;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String checksum;
    @ProtoField(label = Label.REQUIRED, tag = 8, type = Datatype.STRING)
    public final String destinationFileName;
    @ProtoField(tag = 6, type = Datatype.ENUM)
    public final FileTransferError error;
    @ProtoField(label = Label.REQUIRED, tag = 7, type = Datatype.ENUM)
    public final FileType fileType;
    @ProtoField(tag = 5, type = Datatype.INT32)
    public final Integer maxChunkSize;
    @ProtoField(tag = 2, type = Datatype.INT64)
    public final Long offset;
    @ProtoField(label = Label.REQUIRED, tag = 4, type = Datatype.BOOL)
    public final Boolean success;
    @ProtoField(tag = 9, type = Datatype.BOOL)
    public final Boolean supportsAcks;
    @ProtoField(tag = 1, type = Datatype.INT32)
    public final Integer transferId;
    
    static {
        DEFAULT_TRANSFERID = 0;
        DEFAULT_OFFSET = 0L;
        DEFAULT_SUCCESS = false;
        DEFAULT_MAXCHUNKSIZE = 0;
        DEFAULT_ERROR = FileTransferError.FILE_TRANSFER_NO_ERROR;
        DEFAULT_FILETYPE = FileType.FILE_TYPE_OTA;
        DEFAULT_SUPPORTSACKS = false;
    }
    
    private FileTransferResponse(final Builder builder) {
        this(builder.transferId, builder.offset, builder.checksum, builder.success, builder.maxChunkSize, builder.error, builder.fileType, builder.destinationFileName, builder.supportsAcks);
        this.setBuilder((Message.Builder)builder);
    }
    
    public FileTransferResponse(final Integer transferId, final Long offset, final String checksum, final Boolean success, final Integer maxChunkSize, final FileTransferError error, final FileType fileType, final String destinationFileName, final Boolean supportsAcks) {
        this.transferId = transferId;
        this.offset = offset;
        this.checksum = checksum;
        this.success = success;
        this.maxChunkSize = maxChunkSize;
        this.error = error;
        this.fileType = fileType;
        this.destinationFileName = destinationFileName;
        this.supportsAcks = supportsAcks;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof FileTransferResponse)) {
                b = false;
            }
            else {
                final FileTransferResponse fileTransferResponse = (FileTransferResponse)o;
                if (!this.equals(this.transferId, fileTransferResponse.transferId) || !this.equals(this.offset, fileTransferResponse.offset) || !this.equals(this.checksum, fileTransferResponse.checksum) || !this.equals(this.success, fileTransferResponse.success) || !this.equals(this.maxChunkSize, fileTransferResponse.maxChunkSize) || !this.equals(this.error, fileTransferResponse.error) || !this.equals(this.fileType, fileTransferResponse.fileType) || !this.equals(this.destinationFileName, fileTransferResponse.destinationFileName) || !this.equals(this.supportsAcks, fileTransferResponse.supportsAcks)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.transferId != null) {
                hashCode3 = this.transferId.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.offset != null) {
                hashCode4 = this.offset.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.checksum != null) {
                hashCode5 = this.checksum.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.success != null) {
                hashCode6 = this.success.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            int hashCode7;
            if (this.maxChunkSize != null) {
                hashCode7 = this.maxChunkSize.hashCode();
            }
            else {
                hashCode7 = 0;
            }
            int hashCode8;
            if (this.error != null) {
                hashCode8 = this.error.hashCode();
            }
            else {
                hashCode8 = 0;
            }
            int hashCode9;
            if (this.fileType != null) {
                hashCode9 = this.fileType.hashCode();
            }
            else {
                hashCode9 = 0;
            }
            int hashCode10;
            if (this.destinationFileName != null) {
                hashCode10 = this.destinationFileName.hashCode();
            }
            else {
                hashCode10 = 0;
            }
            if (this.supportsAcks != null) {
                hashCode = this.supportsAcks.hashCode();
            }
            hashCode2 = (((((((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode7) * 37 + hashCode8) * 37 + hashCode9) * 37 + hashCode10) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<FileTransferResponse>
    {
        public String checksum;
        public String destinationFileName;
        public FileTransferError error;
        public FileType fileType;
        public Integer maxChunkSize;
        public Long offset;
        public Boolean success;
        public Boolean supportsAcks;
        public Integer transferId;
        
        public Builder() {
        }
        
        public Builder(final FileTransferResponse fileTransferResponse) {
            super(fileTransferResponse);
            if (fileTransferResponse != null) {
                this.transferId = fileTransferResponse.transferId;
                this.offset = fileTransferResponse.offset;
                this.checksum = fileTransferResponse.checksum;
                this.success = fileTransferResponse.success;
                this.maxChunkSize = fileTransferResponse.maxChunkSize;
                this.error = fileTransferResponse.error;
                this.fileType = fileTransferResponse.fileType;
                this.destinationFileName = fileTransferResponse.destinationFileName;
                this.supportsAcks = fileTransferResponse.supportsAcks;
            }
        }
        
        public FileTransferResponse build() {
            ((Message.Builder)this).checkRequiredFields();
            return new FileTransferResponse(this, null);
        }
        
        public Builder checksum(final String checksum) {
            this.checksum = checksum;
            return this;
        }
        
        public Builder destinationFileName(final String destinationFileName) {
            this.destinationFileName = destinationFileName;
            return this;
        }
        
        public Builder error(final FileTransferError error) {
            this.error = error;
            return this;
        }
        
        public Builder fileType(final FileType fileType) {
            this.fileType = fileType;
            return this;
        }
        
        public Builder maxChunkSize(final Integer maxChunkSize) {
            this.maxChunkSize = maxChunkSize;
            return this;
        }
        
        public Builder offset(final Long offset) {
            this.offset = offset;
            return this;
        }
        
        public Builder success(final Boolean success) {
            this.success = success;
            return this;
        }
        
        public Builder supportsAcks(final Boolean supportsAcks) {
            this.supportsAcks = supportsAcks;
            return this;
        }
        
        public Builder transferId(final Integer transferId) {
            this.transferId = transferId;
            return this;
        }
    }
}
