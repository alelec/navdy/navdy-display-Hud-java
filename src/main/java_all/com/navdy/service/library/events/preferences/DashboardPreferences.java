package com.navdy.service.library.events.preferences;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class DashboardPreferences extends Message
{
    public static final String DEFAULT_LEFTGAUGEID = "";
    public static final MiddleGauge DEFAULT_MIDDLEGAUGE;
    public static final String DEFAULT_RIGHTGAUGEID = "";
    public static final ScrollableSide DEFAULT_SCROLLABLESIDE;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String leftGaugeId;
    @ProtoField(tag = 1, type = Datatype.ENUM)
    public final MiddleGauge middleGauge;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String rightGaugeId;
    @ProtoField(tag = 2, type = Datatype.ENUM)
    public final ScrollableSide scrollableSide;
    
    static {
        DEFAULT_MIDDLEGAUGE = MiddleGauge.SPEEDOMETER;
        DEFAULT_SCROLLABLESIDE = ScrollableSide.LEFT;
    }
    
    private DashboardPreferences(final Builder builder) {
        this(builder.middleGauge, builder.scrollableSide, builder.rightGaugeId, builder.leftGaugeId);
        this.setBuilder((Message.Builder)builder);
    }
    
    public DashboardPreferences(final MiddleGauge middleGauge, final ScrollableSide scrollableSide, final String rightGaugeId, final String leftGaugeId) {
        this.middleGauge = middleGauge;
        this.scrollableSide = scrollableSide;
        this.rightGaugeId = rightGaugeId;
        this.leftGaugeId = leftGaugeId;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof DashboardPreferences)) {
                b = false;
            }
            else {
                final DashboardPreferences dashboardPreferences = (DashboardPreferences)o;
                if (!this.equals(this.middleGauge, dashboardPreferences.middleGauge) || !this.equals(this.scrollableSide, dashboardPreferences.scrollableSide) || !this.equals(this.rightGaugeId, dashboardPreferences.rightGaugeId) || !this.equals(this.leftGaugeId, dashboardPreferences.leftGaugeId)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.middleGauge != null) {
                hashCode3 = this.middleGauge.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.scrollableSide != null) {
                hashCode4 = this.scrollableSide.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.rightGaugeId != null) {
                hashCode5 = this.rightGaugeId.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            if (this.leftGaugeId != null) {
                hashCode = this.leftGaugeId.hashCode();
            }
            hashCode2 = ((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<DashboardPreferences>
    {
        public String leftGaugeId;
        public MiddleGauge middleGauge;
        public String rightGaugeId;
        public ScrollableSide scrollableSide;
        
        public Builder() {
        }
        
        public Builder(final DashboardPreferences dashboardPreferences) {
            super(dashboardPreferences);
            if (dashboardPreferences != null) {
                this.middleGauge = dashboardPreferences.middleGauge;
                this.scrollableSide = dashboardPreferences.scrollableSide;
                this.rightGaugeId = dashboardPreferences.rightGaugeId;
                this.leftGaugeId = dashboardPreferences.leftGaugeId;
            }
        }
        
        public DashboardPreferences build() {
            return new DashboardPreferences(this, null);
        }
        
        public Builder leftGaugeId(final String leftGaugeId) {
            this.leftGaugeId = leftGaugeId;
            return this;
        }
        
        public Builder middleGauge(final MiddleGauge middleGauge) {
            this.middleGauge = middleGauge;
            return this;
        }
        
        public Builder rightGaugeId(final String rightGaugeId) {
            this.rightGaugeId = rightGaugeId;
            return this;
        }
        
        public Builder scrollableSide(final ScrollableSide scrollableSide) {
            this.scrollableSide = scrollableSide;
            return this;
        }
    }
}
