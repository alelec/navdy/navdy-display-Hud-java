package com.navdy.service.library.events.glances;

import com.squareup.wire.ProtoEnum;

public enum EmailConstants implements ProtoEnum
{
    EMAIL_BODY(5), 
    EMAIL_FROM_EMAIL(0), 
    EMAIL_FROM_NAME(1), 
    EMAIL_SUBJECT(4), 
    EMAIL_TO_EMAIL(2), 
    EMAIL_TO_NAME(3);
    
    private final int value;
    
    private EmailConstants(final int value) {
        this.value = value;
    }
    
    @Override
    public int getValue() {
        return this.value;
    }
}
