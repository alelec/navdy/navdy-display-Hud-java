package com.navdy.service.library.events.callcontrol;

import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class PhoneBatteryStatus extends Message
{
    public static final Boolean DEFAULT_CHARGING;
    public static final Integer DEFAULT_LEVEL;
    public static final BatteryStatus DEFAULT_STATUS;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 3, type = Datatype.BOOL)
    public final Boolean charging;
    @ProtoField(tag = 2, type = Datatype.INT32)
    public final Integer level;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final BatteryStatus status;
    
    static {
        DEFAULT_STATUS = BatteryStatus.BATTERY_OK;
        DEFAULT_LEVEL = 0;
        DEFAULT_CHARGING = false;
    }
    
    public PhoneBatteryStatus(final BatteryStatus status, final Integer level, final Boolean charging) {
        this.status = status;
        this.level = level;
        this.charging = charging;
    }
    
    private PhoneBatteryStatus(final Builder builder) {
        this(builder.status, builder.level, builder.charging);
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof PhoneBatteryStatus)) {
                b = false;
            }
            else {
                final PhoneBatteryStatus phoneBatteryStatus = (PhoneBatteryStatus)o;
                if (!this.equals(this.status, phoneBatteryStatus.status) || !this.equals(this.level, phoneBatteryStatus.level) || !this.equals(this.charging, phoneBatteryStatus.charging)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.status != null) {
                hashCode3 = this.status.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.level != null) {
                hashCode4 = this.level.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            if (this.charging != null) {
                hashCode = this.charging.hashCode();
            }
            hashCode2 = (hashCode3 * 37 + hashCode4) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public enum BatteryStatus implements ProtoEnum
    {
        BATTERY_EXTREMELY_LOW(3), 
        BATTERY_LOW(2), 
        BATTERY_OK(1);
        
        private final int value;
        
        private BatteryStatus(final int value) {
            this.value = value;
        }
        
        @Override
        public int getValue() {
            return this.value;
        }
    }
    
    public static final class Builder extends Message.Builder<PhoneBatteryStatus>
    {
        public Boolean charging;
        public Integer level;
        public BatteryStatus status;
        
        public Builder() {
        }
        
        public Builder(final PhoneBatteryStatus phoneBatteryStatus) {
            super(phoneBatteryStatus);
            if (phoneBatteryStatus != null) {
                this.status = phoneBatteryStatus.status;
                this.level = phoneBatteryStatus.level;
                this.charging = phoneBatteryStatus.charging;
            }
        }
        
        public PhoneBatteryStatus build() {
            ((Message.Builder)this).checkRequiredFields();
            return new PhoneBatteryStatus(this, null);
        }
        
        public Builder charging(final Boolean charging) {
            this.charging = charging;
            return this;
        }
        
        public Builder level(final Integer level) {
            this.level = level;
            return this;
        }
        
        public Builder status(final BatteryStatus status) {
            this.status = status;
            return this;
        }
    }
}
