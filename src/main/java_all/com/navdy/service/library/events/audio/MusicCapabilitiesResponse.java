package com.navdy.service.library.events.audio;

import java.util.Collections;
import com.squareup.wire.ProtoField;
import java.util.List;
import com.squareup.wire.Message;

public final class MusicCapabilitiesResponse extends Message
{
    public static final List<MusicCapability> DEFAULT_CAPABILITIES;
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REPEATED, messageType = MusicCapability.class, tag = 1)
    public final List<MusicCapability> capabilities;
    
    static {
        DEFAULT_CAPABILITIES = Collections.<MusicCapability>emptyList();
    }
    
    private MusicCapabilitiesResponse(final Builder builder) {
        this(builder.capabilities);
        this.setBuilder((Message.Builder)builder);
    }
    
    public MusicCapabilitiesResponse(final List<MusicCapability> list) {
        this.capabilities = Message.<MusicCapability>immutableCopyOf(list);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof MusicCapabilitiesResponse && this.equals(this.capabilities, ((MusicCapabilitiesResponse)o).capabilities));
    }
    
    @Override
    public int hashCode() {
        int hashCode = this.hashCode;
        if (hashCode == 0) {
            if (this.capabilities != null) {
                hashCode = this.capabilities.hashCode();
            }
            else {
                hashCode = 1;
            }
            this.hashCode = hashCode;
        }
        return hashCode;
    }
    
    public static final class Builder extends Message.Builder<MusicCapabilitiesResponse>
    {
        public List<MusicCapability> capabilities;
        
        public Builder() {
        }
        
        public Builder(final MusicCapabilitiesResponse musicCapabilitiesResponse) {
            super(musicCapabilitiesResponse);
            if (musicCapabilitiesResponse != null) {
                this.capabilities = (List<MusicCapability>)Message.<Object>copyOf((List<Object>)musicCapabilitiesResponse.capabilities);
            }
        }
        
        public MusicCapabilitiesResponse build() {
            return new MusicCapabilitiesResponse(this, null);
        }
        
        public Builder capabilities(final List<MusicCapability> list) {
            this.capabilities = Message.Builder.<MusicCapability>checkForNulls(list);
            return this;
        }
    }
}
