package com.navdy.service.library.events.places;

import java.util.Collections;
import com.squareup.wire.ProtoField;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.destination.Destination;
import java.util.List;
import com.squareup.wire.Message;

public final class PlaceTypeSearchResponse extends Message
{
    public static final List<Destination> DEFAULT_DESTINATIONS;
    public static final String DEFAULT_REQUEST_ID = "";
    public static final RequestStatus DEFAULT_REQUEST_STATUS;
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REPEATED, messageType = Destination.class, tag = 3)
    public final List<Destination> destinations;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String request_id;
    @ProtoField(tag = 2, type = Datatype.ENUM)
    public final RequestStatus request_status;
    
    static {
        DEFAULT_REQUEST_STATUS = RequestStatus.REQUEST_SUCCESS;
        DEFAULT_DESTINATIONS = Collections.<Destination>emptyList();
    }
    
    private PlaceTypeSearchResponse(final Builder builder) {
        this(builder.request_id, builder.request_status, builder.destinations);
        this.setBuilder((Message.Builder)builder);
    }
    
    public PlaceTypeSearchResponse(final String request_id, final RequestStatus request_status, final List<Destination> list) {
        this.request_id = request_id;
        this.request_status = request_status;
        this.destinations = Message.<Destination>immutableCopyOf(list);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof PlaceTypeSearchResponse)) {
                b = false;
            }
            else {
                final PlaceTypeSearchResponse placeTypeSearchResponse = (PlaceTypeSearchResponse)o;
                if (!this.equals(this.request_id, placeTypeSearchResponse.request_id) || !this.equals(this.request_status, placeTypeSearchResponse.request_status) || !this.equals(this.destinations, placeTypeSearchResponse.destinations)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.request_id != null) {
                hashCode3 = this.request_id.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            if (this.request_status != null) {
                hashCode = this.request_status.hashCode();
            }
            int hashCode4;
            if (this.destinations != null) {
                hashCode4 = this.destinations.hashCode();
            }
            else {
                hashCode4 = 1;
            }
            hashCode2 = (hashCode3 * 37 + hashCode) * 37 + hashCode4;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<PlaceTypeSearchResponse>
    {
        public List<Destination> destinations;
        public String request_id;
        public RequestStatus request_status;
        
        public Builder() {
        }
        
        public Builder(final PlaceTypeSearchResponse placeTypeSearchResponse) {
            super(placeTypeSearchResponse);
            if (placeTypeSearchResponse != null) {
                this.request_id = placeTypeSearchResponse.request_id;
                this.request_status = placeTypeSearchResponse.request_status;
                this.destinations = (List<Destination>)Message.<Object>copyOf((List<Object>)placeTypeSearchResponse.destinations);
            }
        }
        
        public PlaceTypeSearchResponse build() {
            return new PlaceTypeSearchResponse(this, null);
        }
        
        public Builder destinations(final List<Destination> list) {
            this.destinations = Message.Builder.<Destination>checkForNulls(list);
            return this;
        }
        
        public Builder request_id(final String request_id) {
            this.request_id = request_id;
            return this;
        }
        
        public Builder request_status(final RequestStatus request_status) {
            this.request_status = request_status;
            return this;
        }
    }
}
