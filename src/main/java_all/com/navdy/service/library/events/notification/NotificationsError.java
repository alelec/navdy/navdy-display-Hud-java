package com.navdy.service.library.events.notification;

import com.squareup.wire.ProtoEnum;

public enum NotificationsError implements ProtoEnum
{
    NOTIFICATIONS_ERROR_AUTH_FAILED(2), 
    NOTIFICATIONS_ERROR_BOND_REMOVED(3), 
    NOTIFICATIONS_ERROR_UNKNOWN(1);
    
    private final int value;
    
    private NotificationsError(final int value) {
        this.value = value;
    }
    
    @Override
    public int getValue() {
        return this.value;
    }
}
