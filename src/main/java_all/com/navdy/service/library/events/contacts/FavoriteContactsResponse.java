package com.navdy.service.library.events.contacts;

import java.util.Collections;
import com.squareup.wire.ProtoField;
import com.navdy.service.library.events.RequestStatus;
import java.util.List;
import com.squareup.wire.Message;

public final class FavoriteContactsResponse extends Message
{
    public static final List<Contact> DEFAULT_CONTACTS;
    public static final RequestStatus DEFAULT_STATUS;
    public static final String DEFAULT_STATUSDETAIL = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REPEATED, messageType = Contact.class, tag = 3)
    public final List<Contact> contacts;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final RequestStatus status;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String statusDetail;
    
    static {
        DEFAULT_STATUS = RequestStatus.REQUEST_SUCCESS;
        DEFAULT_CONTACTS = Collections.<Contact>emptyList();
    }
    
    public FavoriteContactsResponse(final RequestStatus status, final String statusDetail, final List<Contact> list) {
        this.status = status;
        this.statusDetail = statusDetail;
        this.contacts = Message.<Contact>immutableCopyOf(list);
    }
    
    private FavoriteContactsResponse(final Builder builder) {
        this(builder.status, builder.statusDetail, builder.contacts);
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof FavoriteContactsResponse)) {
                b = false;
            }
            else {
                final FavoriteContactsResponse favoriteContactsResponse = (FavoriteContactsResponse)o;
                if (!this.equals(this.status, favoriteContactsResponse.status) || !this.equals(this.statusDetail, favoriteContactsResponse.statusDetail) || !this.equals(this.contacts, favoriteContactsResponse.contacts)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.status != null) {
                hashCode3 = this.status.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            if (this.statusDetail != null) {
                hashCode = this.statusDetail.hashCode();
            }
            int hashCode4;
            if (this.contacts != null) {
                hashCode4 = this.contacts.hashCode();
            }
            else {
                hashCode4 = 1;
            }
            hashCode2 = (hashCode3 * 37 + hashCode) * 37 + hashCode4;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<FavoriteContactsResponse>
    {
        public List<Contact> contacts;
        public RequestStatus status;
        public String statusDetail;
        
        public Builder() {
        }
        
        public Builder(final FavoriteContactsResponse favoriteContactsResponse) {
            super(favoriteContactsResponse);
            if (favoriteContactsResponse != null) {
                this.status = favoriteContactsResponse.status;
                this.statusDetail = favoriteContactsResponse.statusDetail;
                this.contacts = (List<Contact>)Message.<Object>copyOf((List<Object>)favoriteContactsResponse.contacts);
            }
        }
        
        public FavoriteContactsResponse build() {
            ((Message.Builder)this).checkRequiredFields();
            return new FavoriteContactsResponse(this, null);
        }
        
        public Builder contacts(final List<Contact> list) {
            this.contacts = Message.Builder.<Contact>checkForNulls(list);
            return this;
        }
        
        public Builder status(final RequestStatus status) {
            this.status = status;
            return this;
        }
        
        public Builder statusDetail(final String statusDetail) {
            this.statusDetail = statusDetail;
            return this;
        }
    }
}
