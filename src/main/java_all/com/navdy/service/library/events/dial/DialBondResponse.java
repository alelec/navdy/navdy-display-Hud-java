package com.navdy.service.library.events.dial;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class DialBondResponse extends Message
{
    public static final String DEFAULT_MACADDRESS = "";
    public static final String DEFAULT_NAME = "";
    public static final DialError DEFAULT_STATUS;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String macAddress;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String name;
    @ProtoField(tag = 1, type = Datatype.ENUM)
    public final DialError status;
    
    static {
        DEFAULT_STATUS = DialError.DIAL_ERROR;
    }
    
    private DialBondResponse(final Builder builder) {
        this(builder.status, builder.name, builder.macAddress);
        this.setBuilder((Message.Builder)builder);
    }
    
    public DialBondResponse(final DialError status, final String name, final String macAddress) {
        this.status = status;
        this.name = name;
        this.macAddress = macAddress;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof DialBondResponse)) {
                b = false;
            }
            else {
                final DialBondResponse dialBondResponse = (DialBondResponse)o;
                if (!this.equals(this.status, dialBondResponse.status) || !this.equals(this.name, dialBondResponse.name) || !this.equals(this.macAddress, dialBondResponse.macAddress)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.status != null) {
                hashCode3 = this.status.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.name != null) {
                hashCode4 = this.name.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            if (this.macAddress != null) {
                hashCode = this.macAddress.hashCode();
            }
            hashCode2 = (hashCode3 * 37 + hashCode4) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<DialBondResponse>
    {
        public String macAddress;
        public String name;
        public DialError status;
        
        public Builder() {
        }
        
        public Builder(final DialBondResponse dialBondResponse) {
            super(dialBondResponse);
            if (dialBondResponse != null) {
                this.status = dialBondResponse.status;
                this.name = dialBondResponse.name;
                this.macAddress = dialBondResponse.macAddress;
            }
        }
        
        public DialBondResponse build() {
            return new DialBondResponse(this, null);
        }
        
        public Builder macAddress(final String macAddress) {
            this.macAddress = macAddress;
            return this;
        }
        
        public Builder name(final String name) {
            this.name = name;
            return this;
        }
        
        public Builder status(final DialError status) {
            this.status = status;
            return this;
        }
    }
}
