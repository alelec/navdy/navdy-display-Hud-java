package com.navdy.service.library.events;

import com.squareup.wire.ProtoEnum;
import com.squareup.wire.Extension;
import com.squareup.wire.Message;
import com.squareup.wire.ProtoField;
import com.squareup.wire.ExtendableMessage;

public final class NavdyEvent extends ExtendableMessage<NavdyEvent>
{
    public static final MessageType DEFAULT_TYPE;
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.ENUM)
    public final MessageType type;
    
    static {
        DEFAULT_TYPE = MessageType.Coordinate;
    }
    
    private NavdyEvent(final Builder builder) {
        this(builder.type);
        this.setBuilder((ExtendableBuilder<NavdyEvent>)builder);
    }
    
    public NavdyEvent(final MessageType type) {
        this.type = type;
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = false;
        boolean equals;
        if (o == this) {
            equals = true;
        }
        else {
            equals = b;
            if (o instanceof NavdyEvent) {
                final NavdyEvent navdyEvent = (NavdyEvent)o;
                equals = b;
                if (this.extensionsEqual(navdyEvent)) {
                    equals = this.equals(this.type, navdyEvent.type);
                }
            }
        }
        return equals;
    }
    
    @Override
    public int hashCode() {
        int hashCode;
        if ((hashCode = this.hashCode) == 0) {
            final int extensionsHashCode = this.extensionsHashCode();
            int hashCode2;
            if (this.type != null) {
                hashCode2 = this.type.hashCode();
            }
            else {
                hashCode2 = 0;
            }
            hashCode = extensionsHashCode * 37 + hashCode2;
            this.hashCode = hashCode;
        }
        return hashCode;
    }
    
    public static final class Builder extends ExtendableBuilder<NavdyEvent>
    {
        public MessageType type;
        
        public Builder() {
        }
        
        public Builder(final NavdyEvent navdyEvent) {
            super(navdyEvent);
            if (navdyEvent != null) {
                this.type = navdyEvent.type;
            }
        }
        
        public NavdyEvent build() {
            ((Message.Builder)this).checkRequiredFields();
            return new NavdyEvent(this, null);
        }
        
        public <E> Builder setExtension(final Extension<NavdyEvent, E> extension, final E e) {
            super.<E>setExtension(extension, e);
            return this;
        }
        
        public Builder type(final MessageType type) {
            this.type = type;
            return this;
        }
    }
    
    public enum MessageType implements ProtoEnum
    {
        AccelerateShutdown(129), 
        AudioStatus(115), 
        AutoCompleteRequest(105), 
        AutoCompleteResponse(106), 
        CalendarEvent(112), 
        CalendarEventUpdates(113), 
        CallEvent(3), 
        CallStateUpdateRequest(104), 
        CancelSpeechRequest(109), 
        CannedMessagesRequest(135), 
        CannedMessagesUpdate(136), 
        ClearGlances(126), 
        ConnectionRequest(70), 
        ConnectionStateChange(22), 
        ConnectionStatus(71), 
        ContactRequest(62), 
        ContactResponse(63), 
        Coordinate(1), 
        DashboardPreferences(125), 
        DateTimeConfiguration(35), 
        DestinationSelectedRequest(130), 
        DestinationSelectedResponse(131), 
        DeviceInfo(27), 
        DialBondRequest(48), 
        DialBondResponse(49), 
        DialSimulationEvent(60), 
        DialStatusRequest(46), 
        DialStatusResponse(47), 
        DisconnectRequest(53), 
        DismissScreen(14), 
        DisplaySpeakerPreferencesRequest(86), 
        DisplaySpeakerPreferencesUpdate(87), 
        DriveRecordingsRequest(94), 
        DriveRecordingsResponse(95), 
        DriverProfilePreferencesRequest(50), 
        DriverProfilePreferencesUpdate(51), 
        FavoriteContactsRequest(64), 
        FavoriteContactsResponse(65), 
        FavoriteDestinationsRequest(66), 
        FavoriteDestinationsUpdate(67), 
        FileListRequest(4), 
        FileListResponse(5), 
        FileTransferData(38), 
        FileTransferRequest(37), 
        FileTransferResponse(36), 
        FileTransferStatus(39), 
        GestureEvent(16), 
        GetNavigationSessionState(55), 
        GlanceEvent(107), 
        InputPreferencesRequest(75), 
        InputPreferencesUpdate(76), 
        LaunchAppEvent(74), 
        LinkPropertiesChanged(116), 
        MediaRemoteKeyEvent(61), 
        MusicArtworkRequest(123), 
        MusicArtworkResponse(124), 
        MusicCapabilitiesRequest(121), 
        MusicCapabilitiesResponse(122), 
        MusicCollectionRequest(119), 
        MusicCollectionResponse(120), 
        MusicCollectionSourceUpdate(137), 
        MusicEvent(45), 
        MusicTrackInfo(44), 
        MusicTrackInfoRequest(43), 
        NavigationManeuverEvent(8), 
        NavigationPreferencesRequest(58), 
        NavigationPreferencesUpdate(59), 
        NavigationRouteCancelRequest(89), 
        NavigationRouteRequest(9), 
        NavigationRouteResponse(10), 
        NavigationRouteStatus(88), 
        NavigationSessionDeferRequest(103), 
        NavigationSessionRequest(11), 
        NavigationSessionResponse(12), 
        NavigationSessionRouteChange(100), 
        NavigationSessionStatusEvent(13), 
        NetworkLinkReady(110), 
        NetworkStateChange(101), 
        Notification(2), 
        NotificationEvent(26), 
        NotificationListRequest(32), 
        NotificationListResponse(33), 
        NotificationPreferencesRequest(90), 
        NotificationPreferencesUpdate(91), 
        NotificationsStateChange(41), 
        NotificationsStateRequest(42), 
        NowPlayingUpdateRequest(108), 
        ObdStatusRequest(80), 
        ObdStatusResponse(81), 
        PhoneBatteryStatus(77), 
        PhoneEvent(28), 
        PhoneStatusRequest(72), 
        PhoneStatusResponse(73), 
        PhotoRequest(30), 
        PhotoResponse(31), 
        PhotoUpdate(85), 
        PhotoUpdateQuery(132), 
        PhotoUpdateQueryResponse(133), 
        PhotoUpdatesRequest(84), 
        Ping(40), 
        PlaceTypeSearchRequest(127), 
        PlaceTypeSearchResponse(128), 
        PlacesSearchRequest(17), 
        PlacesSearchResponse(18), 
        PreviewFileRequest(6), 
        PreviewFileResponse(7), 
        ReadSettingsRequest(19), 
        ReadSettingsResponse(20), 
        RecommendedDestinationsRequest(68), 
        RecommendedDestinationsUpdate(69), 
        ResumeMusicRequest(134), 
        RouteManeuverRequest(56), 
        RouteManeuverResponse(57), 
        ShowCustomNotification(34), 
        ShowScreen(15), 
        SmsMessageRequest(78), 
        SmsMessageResponse(79), 
        SpeechRequest(23), 
        SpeechRequestStatus(114), 
        StartDrivePlaybackEvent(96), 
        StartDrivePlaybackResponse(99), 
        StartDriveRecordingEvent(92), 
        StartDriveRecordingResponse(98), 
        StopDrivePlaybackEvent(97), 
        StopDriveRecordingEvent(93), 
        StopDriveRecordingResponse(102), 
        SuggestedDestination(111), 
        TelephonyRequest(24), 
        TelephonyResponse(25), 
        TransmitLocation(52), 
        TripUpdate(82), 
        TripUpdateAck(83), 
        UpdateSettings(21), 
        VoiceAssistRequest(29), 
        VoiceAssistResponse(54), 
        VoiceSearchRequest(117), 
        VoiceSearchResponse(118);
        
        private final int value;
        
        private MessageType(final int value) {
            this.value = value;
        }
        
        @Override
        public int getValue() {
            return this.value;
        }
    }
}
