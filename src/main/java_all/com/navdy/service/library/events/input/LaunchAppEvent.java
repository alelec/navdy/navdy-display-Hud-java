package com.navdy.service.library.events.input;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class LaunchAppEvent extends Message
{
    public static final String DEFAULT_APPBUNDLEID = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String appBundleID;
    
    private LaunchAppEvent(final Builder builder) {
        this(builder.appBundleID);
        this.setBuilder((Message.Builder)builder);
    }
    
    public LaunchAppEvent(final String appBundleID) {
        this.appBundleID = appBundleID;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof LaunchAppEvent && this.equals(this.appBundleID, ((LaunchAppEvent)o).appBundleID));
    }
    
    @Override
    public int hashCode() {
        int hashCode = this.hashCode;
        if (hashCode == 0) {
            if (this.appBundleID != null) {
                hashCode = this.appBundleID.hashCode();
            }
            else {
                hashCode = 0;
            }
            this.hashCode = hashCode;
        }
        return hashCode;
    }
    
    public static final class Builder extends Message.Builder<LaunchAppEvent>
    {
        public String appBundleID;
        
        public Builder() {
        }
        
        public Builder(final LaunchAppEvent launchAppEvent) {
            super(launchAppEvent);
            if (launchAppEvent != null) {
                this.appBundleID = launchAppEvent.appBundleID;
            }
        }
        
        public Builder appBundleID(final String appBundleID) {
            this.appBundleID = appBundleID;
            return this;
        }
        
        public LaunchAppEvent build() {
            return new LaunchAppEvent(this, null);
        }
    }
}
