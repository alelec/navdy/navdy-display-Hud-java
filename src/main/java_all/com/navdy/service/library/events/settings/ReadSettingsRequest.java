package com.navdy.service.library.events.settings;

import java.util.Collections;
import com.squareup.wire.ProtoField;
import java.util.List;
import com.squareup.wire.Message;

public final class ReadSettingsRequest extends Message
{
    public static final List<String> DEFAULT_SETTINGS;
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REPEATED, tag = 1, type = Datatype.STRING)
    public final List<String> settings;
    
    static {
        DEFAULT_SETTINGS = Collections.<String>emptyList();
    }
    
    private ReadSettingsRequest(final Builder builder) {
        this(builder.settings);
        this.setBuilder((Message.Builder)builder);
    }
    
    public ReadSettingsRequest(final List<String> list) {
        this.settings = Message.<String>immutableCopyOf(list);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof ReadSettingsRequest && this.equals(this.settings, ((ReadSettingsRequest)o).settings));
    }
    
    @Override
    public int hashCode() {
        int hashCode = this.hashCode;
        if (hashCode == 0) {
            if (this.settings != null) {
                hashCode = this.settings.hashCode();
            }
            else {
                hashCode = 1;
            }
            this.hashCode = hashCode;
        }
        return hashCode;
    }
    
    public static final class Builder extends Message.Builder<ReadSettingsRequest>
    {
        public List<String> settings;
        
        public Builder() {
        }
        
        public Builder(final ReadSettingsRequest readSettingsRequest) {
            super(readSettingsRequest);
            if (readSettingsRequest != null) {
                this.settings = (List<String>)Message.<Object>copyOf((List<Object>)readSettingsRequest.settings);
            }
        }
        
        public ReadSettingsRequest build() {
            return new ReadSettingsRequest(this, null);
        }
        
        public Builder settings(final List<String> list) {
            this.settings = Message.Builder.<String>checkForNulls(list);
            return this;
        }
    }
}
