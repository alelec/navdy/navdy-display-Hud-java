package com.navdy.service.library.events;

import com.squareup.wire.ProtoEnum;

public enum LegacyCapability implements ProtoEnum
{
    CAPABILITY_COMPACT_UI(2), 
    CAPABILITY_LOCAL_MUSIC_BROWSER(4), 
    CAPABILITY_PLACE_TYPE_SEARCH(3), 
    CAPABILITY_VOICE_SEARCH(5), 
    CAPABILITY_VOICE_SEARCH_BETA(1);
    
    private final int value;
    
    private LegacyCapability(final int value) {
        this.value = value;
    }
    
    @Override
    public int getValue() {
        return this.value;
    }
}
