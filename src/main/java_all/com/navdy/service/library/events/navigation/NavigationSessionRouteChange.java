package com.navdy.service.library.events.navigation;

import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class NavigationSessionRouteChange extends Message
{
    public static final String DEFAULT_OLDROUTEID = "";
    public static final RerouteReason DEFAULT_REASON;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 2)
    public final NavigationRouteResult newRoute;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String oldRouteId;
    @ProtoField(tag = 3, type = Datatype.ENUM)
    public final RerouteReason reason;
    
    static {
        DEFAULT_REASON = RerouteReason.NAV_SESSION_OFF_REROUTE;
    }
    
    private NavigationSessionRouteChange(final Builder builder) {
        this(builder.oldRouteId, builder.newRoute, builder.reason);
        this.setBuilder((Message.Builder)builder);
    }
    
    public NavigationSessionRouteChange(final String oldRouteId, final NavigationRouteResult newRoute, final RerouteReason reason) {
        this.oldRouteId = oldRouteId;
        this.newRoute = newRoute;
        this.reason = reason;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof NavigationSessionRouteChange)) {
                b = false;
            }
            else {
                final NavigationSessionRouteChange navigationSessionRouteChange = (NavigationSessionRouteChange)o;
                if (!this.equals(this.oldRouteId, navigationSessionRouteChange.oldRouteId) || !this.equals(this.newRoute, navigationSessionRouteChange.newRoute) || !this.equals(this.reason, navigationSessionRouteChange.reason)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.oldRouteId != null) {
                hashCode3 = this.oldRouteId.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.newRoute != null) {
                hashCode4 = this.newRoute.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            if (this.reason != null) {
                hashCode = this.reason.hashCode();
            }
            hashCode2 = (hashCode3 * 37 + hashCode4) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<NavigationSessionRouteChange>
    {
        public NavigationRouteResult newRoute;
        public String oldRouteId;
        public RerouteReason reason;
        
        public Builder() {
        }
        
        public Builder(final NavigationSessionRouteChange navigationSessionRouteChange) {
            super(navigationSessionRouteChange);
            if (navigationSessionRouteChange != null) {
                this.oldRouteId = navigationSessionRouteChange.oldRouteId;
                this.newRoute = navigationSessionRouteChange.newRoute;
                this.reason = navigationSessionRouteChange.reason;
            }
        }
        
        public NavigationSessionRouteChange build() {
            return new NavigationSessionRouteChange(this, null);
        }
        
        public Builder newRoute(final NavigationRouteResult newRoute) {
            this.newRoute = newRoute;
            return this;
        }
        
        public Builder oldRouteId(final String oldRouteId) {
            this.oldRouteId = oldRouteId;
            return this;
        }
        
        public Builder reason(final RerouteReason reason) {
            this.reason = reason;
            return this;
        }
    }
    
    public enum RerouteReason implements ProtoEnum
    {
        NAV_SESSION_ARRIVAL_REROUTE(3), 
        NAV_SESSION_FUEL_REROUTE(4), 
        NAV_SESSION_OFF_REROUTE(1), 
        NAV_SESSION_ROUTE_PICKER(5), 
        NAV_SESSION_TRAFFIC_REROUTE(2);
        
        private final int value;
        
        private RerouteReason(final int value) {
            this.value = value;
        }
        
        @Override
        public int getValue() {
            return this.value;
        }
    }
}
