package com.navdy.service.library.events.preferences;

import com.squareup.wire.ProtoEnum;
import java.util.Collections;
import com.squareup.wire.ProtoField;
import java.util.List;
import com.squareup.wire.Message;

public final class DriverProfilePreferences extends Message
{
    public static final List<String> DEFAULT_ADDITIONALLOCALES;
    public static final Boolean DEFAULT_AUTO_ON_ENABLED;
    public static final String DEFAULT_CAR_MAKE = "";
    public static final String DEFAULT_CAR_MODEL = "";
    public static final String DEFAULT_CAR_YEAR = "";
    public static final String DEFAULT_DEVICE_NAME = "";
    public static final DialLongPressAction DEFAULT_DIAL_LONG_PRESS_ACTION;
    public static final DisplayFormat DEFAULT_DISPLAY_FORMAT;
    public static final String DEFAULT_DRIVER_EMAIL = "";
    public static final String DEFAULT_DRIVER_NAME = "";
    public static final FeatureMode DEFAULT_FEATURE_MODE;
    public static final Boolean DEFAULT_LIMIT_BANDWIDTH;
    public static final String DEFAULT_LOCALE = "en_US";
    public static final Long DEFAULT_OBDBLACKLISTLASTMODIFIED;
    public static final ObdScanSetting DEFAULT_OBDSCANSETTING;
    public static final String DEFAULT_PHOTO_CHECKSUM = "";
    public static final Boolean DEFAULT_PROFILE_IS_PUBLIC;
    public static final Long DEFAULT_SERIAL_NUMBER;
    public static final UnitSystem DEFAULT_UNIT_SYSTEM;
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REPEATED, tag = 19, type = Datatype.STRING)
    public final List<String> additionalLocales;
    @ProtoField(tag = 10, type = Datatype.BOOL)
    public final Boolean auto_on_enabled;
    @ProtoField(tag = 7, type = Datatype.STRING)
    public final String car_make;
    @ProtoField(tag = 8, type = Datatype.STRING)
    public final String car_model;
    @ProtoField(tag = 9, type = Datatype.STRING)
    public final String car_year;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String device_name;
    @ProtoField(tag = 18, type = Datatype.ENUM)
    public final DialLongPressAction dial_long_press_action;
    @ProtoField(tag = 11, type = Datatype.ENUM)
    public final DisplayFormat display_format;
    @ProtoField(tag = 6, type = Datatype.STRING)
    public final String driver_email;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String driver_name;
    @ProtoField(tag = 14, type = Datatype.ENUM)
    public final FeatureMode feature_mode;
    @ProtoField(tag = 16, type = Datatype.BOOL)
    public final Boolean limit_bandwidth;
    @ProtoField(tag = 12, type = Datatype.STRING)
    public final String locale;
    @ProtoField(tag = 17, type = Datatype.INT64)
    public final Long obdBlacklistLastModified;
    @ProtoField(tag = 15, type = Datatype.ENUM)
    public final ObdScanSetting obdScanSetting;
    @ProtoField(tag = 5, type = Datatype.STRING)
    public final String photo_checksum;
    @ProtoField(tag = 4, type = Datatype.BOOL)
    public final Boolean profile_is_public;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.INT64)
    public final Long serial_number;
    @ProtoField(tag = 13, type = Datatype.ENUM)
    public final UnitSystem unit_system;
    
    static {
        DEFAULT_SERIAL_NUMBER = 0L;
        DEFAULT_PROFILE_IS_PUBLIC = false;
        DEFAULT_AUTO_ON_ENABLED = true;
        DEFAULT_DISPLAY_FORMAT = DisplayFormat.DISPLAY_FORMAT_NORMAL;
        DEFAULT_UNIT_SYSTEM = UnitSystem.UNIT_SYSTEM_IMPERIAL;
        DEFAULT_FEATURE_MODE = FeatureMode.FEATURE_MODE_RELEASE;
        DEFAULT_OBDSCANSETTING = ObdScanSetting.SCAN_DEFAULT_ON;
        DEFAULT_LIMIT_BANDWIDTH = false;
        DEFAULT_OBDBLACKLISTLASTMODIFIED = 0L;
        DEFAULT_DIAL_LONG_PRESS_ACTION = DialLongPressAction.DIAL_LONG_PRESS_VOICE_ASSISTANT;
        DEFAULT_ADDITIONALLOCALES = Collections.<String>emptyList();
    }
    
    private DriverProfilePreferences(final Builder builder) {
        this(builder.serial_number, builder.driver_name, builder.device_name, builder.profile_is_public, builder.photo_checksum, builder.driver_email, builder.car_make, builder.car_model, builder.car_year, builder.auto_on_enabled, builder.display_format, builder.locale, builder.unit_system, builder.feature_mode, builder.obdScanSetting, builder.limit_bandwidth, builder.obdBlacklistLastModified, builder.dial_long_press_action, builder.additionalLocales);
        this.setBuilder((Message.Builder)builder);
    }
    
    public DriverProfilePreferences(final Long serial_number, final String driver_name, final String device_name, final Boolean profile_is_public, final String photo_checksum, final String driver_email, final String car_make, final String car_model, final String car_year, final Boolean auto_on_enabled, final DisplayFormat display_format, final String locale, final UnitSystem unit_system, final FeatureMode feature_mode, final ObdScanSetting obdScanSetting, final Boolean limit_bandwidth, final Long obdBlacklistLastModified, final DialLongPressAction dial_long_press_action, final List<String> list) {
        this.serial_number = serial_number;
        this.driver_name = driver_name;
        this.device_name = device_name;
        this.profile_is_public = profile_is_public;
        this.photo_checksum = photo_checksum;
        this.driver_email = driver_email;
        this.car_make = car_make;
        this.car_model = car_model;
        this.car_year = car_year;
        this.auto_on_enabled = auto_on_enabled;
        this.display_format = display_format;
        this.locale = locale;
        this.unit_system = unit_system;
        this.feature_mode = feature_mode;
        this.obdScanSetting = obdScanSetting;
        this.limit_bandwidth = limit_bandwidth;
        this.obdBlacklistLastModified = obdBlacklistLastModified;
        this.dial_long_press_action = dial_long_press_action;
        this.additionalLocales = Message.<String>immutableCopyOf(list);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof DriverProfilePreferences)) {
                b = false;
            }
            else {
                final DriverProfilePreferences driverProfilePreferences = (DriverProfilePreferences)o;
                if (!this.equals(this.serial_number, driverProfilePreferences.serial_number) || !this.equals(this.driver_name, driverProfilePreferences.driver_name) || !this.equals(this.device_name, driverProfilePreferences.device_name) || !this.equals(this.profile_is_public, driverProfilePreferences.profile_is_public) || !this.equals(this.photo_checksum, driverProfilePreferences.photo_checksum) || !this.equals(this.driver_email, driverProfilePreferences.driver_email) || !this.equals(this.car_make, driverProfilePreferences.car_make) || !this.equals(this.car_model, driverProfilePreferences.car_model) || !this.equals(this.car_year, driverProfilePreferences.car_year) || !this.equals(this.auto_on_enabled, driverProfilePreferences.auto_on_enabled) || !this.equals(this.display_format, driverProfilePreferences.display_format) || !this.equals(this.locale, driverProfilePreferences.locale) || !this.equals(this.unit_system, driverProfilePreferences.unit_system) || !this.equals(this.feature_mode, driverProfilePreferences.feature_mode) || !this.equals(this.obdScanSetting, driverProfilePreferences.obdScanSetting) || !this.equals(this.limit_bandwidth, driverProfilePreferences.limit_bandwidth) || !this.equals(this.obdBlacklistLastModified, driverProfilePreferences.obdBlacklistLastModified) || !this.equals(this.dial_long_press_action, driverProfilePreferences.dial_long_press_action) || !this.equals(this.additionalLocales, driverProfilePreferences.additionalLocales)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.serial_number != null) {
                hashCode3 = this.serial_number.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.driver_name != null) {
                hashCode4 = this.driver_name.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.device_name != null) {
                hashCode5 = this.device_name.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.profile_is_public != null) {
                hashCode6 = this.profile_is_public.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            int hashCode7;
            if (this.photo_checksum != null) {
                hashCode7 = this.photo_checksum.hashCode();
            }
            else {
                hashCode7 = 0;
            }
            int hashCode8;
            if (this.driver_email != null) {
                hashCode8 = this.driver_email.hashCode();
            }
            else {
                hashCode8 = 0;
            }
            int hashCode9;
            if (this.car_make != null) {
                hashCode9 = this.car_make.hashCode();
            }
            else {
                hashCode9 = 0;
            }
            int hashCode10;
            if (this.car_model != null) {
                hashCode10 = this.car_model.hashCode();
            }
            else {
                hashCode10 = 0;
            }
            int hashCode11;
            if (this.car_year != null) {
                hashCode11 = this.car_year.hashCode();
            }
            else {
                hashCode11 = 0;
            }
            int hashCode12;
            if (this.auto_on_enabled != null) {
                hashCode12 = this.auto_on_enabled.hashCode();
            }
            else {
                hashCode12 = 0;
            }
            int hashCode13;
            if (this.display_format != null) {
                hashCode13 = this.display_format.hashCode();
            }
            else {
                hashCode13 = 0;
            }
            int hashCode14;
            if (this.locale != null) {
                hashCode14 = this.locale.hashCode();
            }
            else {
                hashCode14 = 0;
            }
            int hashCode15;
            if (this.unit_system != null) {
                hashCode15 = this.unit_system.hashCode();
            }
            else {
                hashCode15 = 0;
            }
            int hashCode16;
            if (this.feature_mode != null) {
                hashCode16 = this.feature_mode.hashCode();
            }
            else {
                hashCode16 = 0;
            }
            int hashCode17;
            if (this.obdScanSetting != null) {
                hashCode17 = this.obdScanSetting.hashCode();
            }
            else {
                hashCode17 = 0;
            }
            int hashCode18;
            if (this.limit_bandwidth != null) {
                hashCode18 = this.limit_bandwidth.hashCode();
            }
            else {
                hashCode18 = 0;
            }
            int hashCode19;
            if (this.obdBlacklistLastModified != null) {
                hashCode19 = this.obdBlacklistLastModified.hashCode();
            }
            else {
                hashCode19 = 0;
            }
            if (this.dial_long_press_action != null) {
                hashCode = this.dial_long_press_action.hashCode();
            }
            int hashCode20;
            if (this.additionalLocales != null) {
                hashCode20 = this.additionalLocales.hashCode();
            }
            else {
                hashCode20 = 1;
            }
            hashCode2 = (((((((((((((((((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode7) * 37 + hashCode8) * 37 + hashCode9) * 37 + hashCode10) * 37 + hashCode11) * 37 + hashCode12) * 37 + hashCode13) * 37 + hashCode14) * 37 + hashCode15) * 37 + hashCode16) * 37 + hashCode17) * 37 + hashCode18) * 37 + hashCode19) * 37 + hashCode) * 37 + hashCode20;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<DriverProfilePreferences>
    {
        public List<String> additionalLocales;
        public Boolean auto_on_enabled;
        public String car_make;
        public String car_model;
        public String car_year;
        public String device_name;
        public DialLongPressAction dial_long_press_action;
        public DisplayFormat display_format;
        public String driver_email;
        public String driver_name;
        public FeatureMode feature_mode;
        public Boolean limit_bandwidth;
        public String locale;
        public Long obdBlacklistLastModified;
        public ObdScanSetting obdScanSetting;
        public String photo_checksum;
        public Boolean profile_is_public;
        public Long serial_number;
        public UnitSystem unit_system;
        
        public Builder() {
        }
        
        public Builder(final DriverProfilePreferences driverProfilePreferences) {
            super(driverProfilePreferences);
            if (driverProfilePreferences != null) {
                this.serial_number = driverProfilePreferences.serial_number;
                this.driver_name = driverProfilePreferences.driver_name;
                this.device_name = driverProfilePreferences.device_name;
                this.profile_is_public = driverProfilePreferences.profile_is_public;
                this.photo_checksum = driverProfilePreferences.photo_checksum;
                this.driver_email = driverProfilePreferences.driver_email;
                this.car_make = driverProfilePreferences.car_make;
                this.car_model = driverProfilePreferences.car_model;
                this.car_year = driverProfilePreferences.car_year;
                this.auto_on_enabled = driverProfilePreferences.auto_on_enabled;
                this.display_format = driverProfilePreferences.display_format;
                this.locale = driverProfilePreferences.locale;
                this.unit_system = driverProfilePreferences.unit_system;
                this.feature_mode = driverProfilePreferences.feature_mode;
                this.obdScanSetting = driverProfilePreferences.obdScanSetting;
                this.limit_bandwidth = driverProfilePreferences.limit_bandwidth;
                this.obdBlacklistLastModified = driverProfilePreferences.obdBlacklistLastModified;
                this.dial_long_press_action = driverProfilePreferences.dial_long_press_action;
                this.additionalLocales = (List<String>)Message.<Object>copyOf((List<Object>)driverProfilePreferences.additionalLocales);
            }
        }
        
        public Builder additionalLocales(final List<String> list) {
            this.additionalLocales = Message.Builder.<String>checkForNulls(list);
            return this;
        }
        
        public Builder auto_on_enabled(final Boolean auto_on_enabled) {
            this.auto_on_enabled = auto_on_enabled;
            return this;
        }
        
        public DriverProfilePreferences build() {
            ((Message.Builder)this).checkRequiredFields();
            return new DriverProfilePreferences(this, null);
        }
        
        public Builder car_make(final String car_make) {
            this.car_make = car_make;
            return this;
        }
        
        public Builder car_model(final String car_model) {
            this.car_model = car_model;
            return this;
        }
        
        public Builder car_year(final String car_year) {
            this.car_year = car_year;
            return this;
        }
        
        public Builder device_name(final String device_name) {
            this.device_name = device_name;
            return this;
        }
        
        public Builder dial_long_press_action(final DialLongPressAction dial_long_press_action) {
            this.dial_long_press_action = dial_long_press_action;
            return this;
        }
        
        public Builder display_format(final DisplayFormat display_format) {
            this.display_format = display_format;
            return this;
        }
        
        public Builder driver_email(final String driver_email) {
            this.driver_email = driver_email;
            return this;
        }
        
        public Builder driver_name(final String driver_name) {
            this.driver_name = driver_name;
            return this;
        }
        
        public Builder feature_mode(final FeatureMode feature_mode) {
            this.feature_mode = feature_mode;
            return this;
        }
        
        public Builder limit_bandwidth(final Boolean limit_bandwidth) {
            this.limit_bandwidth = limit_bandwidth;
            return this;
        }
        
        public Builder locale(final String locale) {
            this.locale = locale;
            return this;
        }
        
        public Builder obdBlacklistLastModified(final Long obdBlacklistLastModified) {
            this.obdBlacklistLastModified = obdBlacklistLastModified;
            return this;
        }
        
        public Builder obdScanSetting(final ObdScanSetting obdScanSetting) {
            this.obdScanSetting = obdScanSetting;
            return this;
        }
        
        public Builder photo_checksum(final String photo_checksum) {
            this.photo_checksum = photo_checksum;
            return this;
        }
        
        public Builder profile_is_public(final Boolean profile_is_public) {
            this.profile_is_public = profile_is_public;
            return this;
        }
        
        public Builder serial_number(final Long serial_number) {
            this.serial_number = serial_number;
            return this;
        }
        
        public Builder unit_system(final UnitSystem unit_system) {
            this.unit_system = unit_system;
            return this;
        }
    }
    
    public enum DialLongPressAction implements ProtoEnum
    {
        DIAL_LONG_PRESS_PLACE_SEARCH(1), 
        DIAL_LONG_PRESS_VOICE_ASSISTANT(0);
        
        private final int value;
        
        private DialLongPressAction(final int value) {
            this.value = value;
        }
        
        @Override
        public int getValue() {
            return this.value;
        }
    }
    
    public enum DisplayFormat implements ProtoEnum
    {
        DISPLAY_FORMAT_COMPACT(1), 
        DISPLAY_FORMAT_NORMAL(0);
        
        private final int value;
        
        private DisplayFormat(final int value) {
            this.value = value;
        }
        
        @Override
        public int getValue() {
            return this.value;
        }
    }
    
    public enum FeatureMode implements ProtoEnum
    {
        FEATURE_MODE_BETA(2), 
        FEATURE_MODE_EXPERIMENTAL(3), 
        FEATURE_MODE_RELEASE(1);
        
        private final int value;
        
        private FeatureMode(final int value) {
            this.value = value;
        }
        
        @Override
        public int getValue() {
            return this.value;
        }
    }
    
    public enum ObdScanSetting implements ProtoEnum
    {
        SCAN_DEFAULT_OFF(4), 
        SCAN_DEFAULT_ON(1), 
        SCAN_OFF(3), 
        SCAN_ON(2);
        
        private final int value;
        
        private ObdScanSetting(final int value) {
            this.value = value;
        }
        
        @Override
        public int getValue() {
            return this.value;
        }
    }
    
    public enum UnitSystem implements ProtoEnum
    {
        UNIT_SYSTEM_IMPERIAL(1), 
        UNIT_SYSTEM_METRIC(0);
        
        private final int value;
        
        private UnitSystem(final int value) {
            this.value = value;
        }
        
        @Override
        public int getValue() {
            return this.value;
        }
    }
}
