package com.navdy.service.library.events.audio;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class NowPlayingUpdateRequest extends Message
{
    public static final Boolean DEFAULT_START;
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.BOOL)
    public final Boolean start;
    
    static {
        DEFAULT_START = false;
    }
    
    private NowPlayingUpdateRequest(final Builder builder) {
        this(builder.start);
        this.setBuilder((Message.Builder)builder);
    }
    
    public NowPlayingUpdateRequest(final Boolean start) {
        this.start = start;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof NowPlayingUpdateRequest && this.equals(this.start, ((NowPlayingUpdateRequest)o).start));
    }
    
    @Override
    public int hashCode() {
        int hashCode = this.hashCode;
        if (hashCode == 0) {
            if (this.start != null) {
                hashCode = this.start.hashCode();
            }
            else {
                hashCode = 0;
            }
            this.hashCode = hashCode;
        }
        return hashCode;
    }
    
    public static final class Builder extends Message.Builder<NowPlayingUpdateRequest>
    {
        public Boolean start;
        
        public Builder() {
        }
        
        public Builder(final NowPlayingUpdateRequest nowPlayingUpdateRequest) {
            super(nowPlayingUpdateRequest);
            if (nowPlayingUpdateRequest != null) {
                this.start = nowPlayingUpdateRequest.start;
            }
        }
        
        public NowPlayingUpdateRequest build() {
            ((Message.Builder)this).checkRequiredFields();
            return new NowPlayingUpdateRequest(this, null);
        }
        
        public Builder start(final Boolean start) {
            this.start = start;
            return this;
        }
    }
}
