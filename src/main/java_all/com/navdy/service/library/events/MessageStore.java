package com.navdy.service.library.events;

import com.navdy.service.library.util.IOUtils;
import java.io.IOException;
import com.squareup.wire.Message;
import com.squareup.wire.Wire;
import java.io.File;
import com.navdy.service.library.log.Logger;

public class MessageStore
{
    private static final Logger sLogger;
    private File directory;
    private Wire wire;
    
    static {
        sLogger = new Logger(MessageStore.class);
    }
    
    public MessageStore(final File directory) {
        this.directory = directory;
        this.wire = new Wire((Class<?>[])new Class[] { Ext_NavdyEvent.class });
    }
    
    public static <T extends Message> T removeNulls(final T t) {
        return NavdyEventUtil.<T>applyDefaults(t);
    }
    
    public <T extends Message> T readMessage(final String s, final Class<T> clazz) throws IOException {
        return this.<T>readMessage(s, clazz, null);
    }
    
    public <T extends Message> T readMessage(final String p0, final Class<T> p1, final NavdyEventUtil.Initializer<T> p2) throws IOException {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: dup            
        //     4: aload_0        
        //     5: getfield        com/navdy/service/library/events/MessageStore.directory:Ljava/io/File;
        //     8: aload_1        
        //     9: invokespecial   java/io/File.<init>:(Ljava/io/File;Ljava/lang/String;)V
        //    12: astore          4
        //    14: aconst_null    
        //    15: astore          5
        //    17: aconst_null    
        //    18: astore          6
        //    20: aconst_null    
        //    21: astore          7
        //    23: aload           6
        //    25: astore          8
        //    27: aload           4
        //    29: invokevirtual   java/io/File.length:()J
        //    32: lconst_0       
        //    33: lcmp           
        //    34: ifne            63
        //    37: aload           6
        //    39: astore          8
        //    41: aload_2        
        //    42: aload_3        
        //    43: invokestatic    com/navdy/service/library/events/NavdyEventUtil.getDefault:(Ljava/lang/Class;Lcom/navdy/service/library/events/NavdyEventUtil$Initializer;)Lcom/squareup/wire/Message;
        //    46: astore          6
        //    48: aload           6
        //    50: astore_1       
        //    51: aload           7
        //    53: astore_2       
        //    54: aload_2        
        //    55: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //    58: aload_1        
        //    59: invokestatic    com/navdy/service/library/events/MessageStore.removeNulls:(Lcom/squareup/wire/Message;)Lcom/squareup/wire/Message;
        //    62: areturn        
        //    63: aload           6
        //    65: astore          8
        //    67: new             Ljava/io/FileInputStream;
        //    70: dup            
        //    71: aload           4
        //    73: invokespecial   java/io/FileInputStream.<init>:(Ljava/io/File;)V
        //    76: astore          7
        //    78: aload_0        
        //    79: getfield        com/navdy/service/library/events/MessageStore.wire:Lcom/squareup/wire/Wire;
        //    82: aload           7
        //    84: aload_2        
        //    85: invokevirtual   com/squareup/wire/Wire.parseFrom:(Ljava/io/InputStream;Ljava/lang/Class;)Lcom/squareup/wire/Message;
        //    88: astore          8
        //    90: aload           8
        //    92: astore_1       
        //    93: aload           7
        //    95: astore_2       
        //    96: goto            54
        //    99: astore          6
        //   101: aload           5
        //   103: astore          7
        //   105: aload           7
        //   107: astore          8
        //   109: aload_2        
        //   110: aload_3        
        //   111: invokestatic    com/navdy/service/library/events/NavdyEventUtil.getDefault:(Ljava/lang/Class;Lcom/navdy/service/library/events/NavdyEventUtil$Initializer;)Lcom/squareup/wire/Message;
        //   114: astore_2       
        //   115: aload           7
        //   117: astore          8
        //   119: getstatic       com/navdy/service/library/events/MessageStore.sLogger:Lcom/navdy/service/library/log/Logger;
        //   122: astore_3       
        //   123: aload           7
        //   125: astore          8
        //   127: new             Ljava/lang/StringBuilder;
        //   130: astore          5
        //   132: aload           7
        //   134: astore          8
        //   136: aload           5
        //   138: invokespecial   java/lang/StringBuilder.<init>:()V
        //   141: aload           7
        //   143: astore          8
        //   145: aload_3        
        //   146: aload           5
        //   148: ldc             "Failed to read message from "
        //   150: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   153: aload_1        
        //   154: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   157: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   160: aload           6
        //   162: invokevirtual   com/navdy/service/library/log/Logger.w:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   165: aload           7
        //   167: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   170: aload_2        
        //   171: astore_1       
        //   172: goto            58
        //   175: astore_1       
        //   176: aload           8
        //   178: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   181: aload_1        
        //   182: athrow         
        //   183: astore_1       
        //   184: aload           7
        //   186: astore          8
        //   188: goto            176
        //   191: astore          6
        //   193: goto            105
        //    Exceptions:
        //  throws java.io.IOException
        //    Signature:
        //  <T:Lcom/squareup/wire/Message;>(Ljava/lang/String;Ljava/lang/Class<TT;>;Lcom/navdy/service/library/events/NavdyEventUtil$Initializer<TT;>;)TT;
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  27     37     99     105    Ljava/lang/Exception;
        //  27     37     175    176    Any
        //  41     48     99     105    Ljava/lang/Exception;
        //  41     48     175    176    Any
        //  67     78     99     105    Ljava/lang/Exception;
        //  67     78     175    176    Any
        //  78     90     191    196    Ljava/lang/Exception;
        //  78     90     183    191    Any
        //  109    115    175    176    Any
        //  119    123    175    176    Any
        //  127    132    175    176    Any
        //  136    141    175    176    Any
        //  145    165    175    176    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.UnsupportedOperationException: The requested operation is not supported.
        //     at com.strobel.util.ContractUtils.unsupported(ContractUtils.java:27)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:276)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:271)
        //     at com.strobel.assembler.metadata.TypeReference.makeGenericType(TypeReference.java:150)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:187)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:25)
        //     at com.strobel.assembler.metadata.ParameterizedType.accept(ParameterizedType.java:103)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visit(TypeSubstitutionVisitor.java:39)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:173)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:25)
        //     at com.strobel.assembler.metadata.ParameterizedType.accept(ParameterizedType.java:103)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visit(TypeSubstitutionVisitor.java:39)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:173)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:25)
        //     at com.strobel.assembler.metadata.ParameterizedType.accept(ParameterizedType.java:103)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visit(TypeSubstitutionVisitor.java:39)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitTypes(TypeSubstitutionVisitor.java:331)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitMethod(TypeSubstitutionVisitor.java:273)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferCall(TypeAnalysis.java:2597)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1029)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:803)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:770)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:881)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:803)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:672)
        //     at com.strobel.decompiler.ast.TypeAnalysis.invalidateDependentExpressions(TypeAnalysis.java:759)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1011)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:803)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:770)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:881)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:803)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:672)
        //     at com.strobel.decompiler.ast.TypeAnalysis.invalidateDependentExpressions(TypeAnalysis.java:759)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1011)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:803)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:770)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:766)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferCall(TypeAnalysis.java:2505)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1029)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:803)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:770)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:881)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:803)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:672)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:655)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:365)
        //     at com.strobel.decompiler.ast.TypeAnalysis.run(TypeAnalysis.java:96)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:109)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public void writeMessage(final Message message, final String s) {
        final String string = s + ".new";
        final File file = new File(this.directory, string);
        try {
            final byte[] byteArray = message.toByteArray();
            final int copyFile = IOUtils.copyFile(file.getAbsolutePath(), byteArray);
            if (copyFile != byteArray.length) {
                MessageStore.sLogger.e("failed to write messages to " + string + "wrote: " + copyFile + " bytes, length:" + byteArray.length);
            }
            else if (!file.renameTo(new File(this.directory, s))) {
                MessageStore.sLogger.e("could not replace messages in " + s);
            }
        }
        catch (Exception ex) {
            MessageStore.sLogger.e("could not write new messages to " + s + " : ", ex);
        }
    }
}
