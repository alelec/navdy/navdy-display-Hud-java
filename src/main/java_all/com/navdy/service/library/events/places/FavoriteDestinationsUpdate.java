package com.navdy.service.library.events.places;

import java.util.Collections;
import com.squareup.wire.ProtoField;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.destination.Destination;
import java.util.List;
import com.squareup.wire.Message;

public final class FavoriteDestinationsUpdate extends Message
{
    public static final List<Destination> DEFAULT_DESTINATIONS;
    public static final Long DEFAULT_SERIAL_NUMBER;
    public static final RequestStatus DEFAULT_STATUS;
    public static final String DEFAULT_STATUSDETAIL = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REPEATED, messageType = Destination.class, tag = 4)
    public final List<Destination> destinations;
    @ProtoField(label = Label.REQUIRED, tag = 3, type = Datatype.INT64)
    public final Long serial_number;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final RequestStatus status;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String statusDetail;
    
    static {
        DEFAULT_STATUS = RequestStatus.REQUEST_SUCCESS;
        DEFAULT_SERIAL_NUMBER = 0L;
        DEFAULT_DESTINATIONS = Collections.<Destination>emptyList();
    }
    
    public FavoriteDestinationsUpdate(final RequestStatus status, final String statusDetail, final Long serial_number, final List<Destination> list) {
        this.status = status;
        this.statusDetail = statusDetail;
        this.serial_number = serial_number;
        this.destinations = Message.<Destination>immutableCopyOf(list);
    }
    
    private FavoriteDestinationsUpdate(final Builder builder) {
        this(builder.status, builder.statusDetail, builder.serial_number, builder.destinations);
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof FavoriteDestinationsUpdate)) {
                b = false;
            }
            else {
                final FavoriteDestinationsUpdate favoriteDestinationsUpdate = (FavoriteDestinationsUpdate)o;
                if (!this.equals(this.status, favoriteDestinationsUpdate.status) || !this.equals(this.statusDetail, favoriteDestinationsUpdate.statusDetail) || !this.equals(this.serial_number, favoriteDestinationsUpdate.serial_number) || !this.equals(this.destinations, favoriteDestinationsUpdate.destinations)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.status != null) {
                hashCode3 = this.status.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.statusDetail != null) {
                hashCode4 = this.statusDetail.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            if (this.serial_number != null) {
                hashCode = this.serial_number.hashCode();
            }
            int hashCode5;
            if (this.destinations != null) {
                hashCode5 = this.destinations.hashCode();
            }
            else {
                hashCode5 = 1;
            }
            hashCode2 = ((hashCode3 * 37 + hashCode4) * 37 + hashCode) * 37 + hashCode5;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<FavoriteDestinationsUpdate>
    {
        public List<Destination> destinations;
        public Long serial_number;
        public RequestStatus status;
        public String statusDetail;
        
        public Builder() {
        }
        
        public Builder(final FavoriteDestinationsUpdate favoriteDestinationsUpdate) {
            super(favoriteDestinationsUpdate);
            if (favoriteDestinationsUpdate != null) {
                this.status = favoriteDestinationsUpdate.status;
                this.statusDetail = favoriteDestinationsUpdate.statusDetail;
                this.serial_number = favoriteDestinationsUpdate.serial_number;
                this.destinations = (List<Destination>)Message.<Object>copyOf((List<Object>)favoriteDestinationsUpdate.destinations);
            }
        }
        
        public FavoriteDestinationsUpdate build() {
            ((Message.Builder)this).checkRequiredFields();
            return new FavoriteDestinationsUpdate(this, null);
        }
        
        public Builder destinations(final List<Destination> list) {
            this.destinations = Message.Builder.<Destination>checkForNulls(list);
            return this;
        }
        
        public Builder serial_number(final Long serial_number) {
            this.serial_number = serial_number;
            return this;
        }
        
        public Builder status(final RequestStatus status) {
            this.status = status;
            return this;
        }
        
        public Builder statusDetail(final String statusDetail) {
            this.statusDetail = statusDetail;
            return this;
        }
    }
}
