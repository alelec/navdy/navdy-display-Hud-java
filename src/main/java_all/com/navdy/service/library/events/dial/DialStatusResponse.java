package com.navdy.service.library.events.dial;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class DialStatusResponse extends Message
{
    public static final Integer DEFAULT_BATTERYLEVEL;
    public static final Boolean DEFAULT_ISCONNECTED;
    public static final Boolean DEFAULT_ISPAIRED;
    public static final String DEFAULT_MACADDRESS = "";
    public static final String DEFAULT_NAME = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 5, type = Datatype.INT32)
    public final Integer batteryLevel;
    @ProtoField(tag = 2, type = Datatype.BOOL)
    public final Boolean isConnected;
    @ProtoField(tag = 1, type = Datatype.BOOL)
    public final Boolean isPaired;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String macAddress;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String name;
    
    static {
        DEFAULT_ISPAIRED = false;
        DEFAULT_ISCONNECTED = false;
        DEFAULT_BATTERYLEVEL = 0;
    }
    
    private DialStatusResponse(final Builder builder) {
        this(builder.isPaired, builder.isConnected, builder.name, builder.macAddress, builder.batteryLevel);
        this.setBuilder((Message.Builder)builder);
    }
    
    public DialStatusResponse(final Boolean isPaired, final Boolean isConnected, final String name, final String macAddress, final Integer batteryLevel) {
        this.isPaired = isPaired;
        this.isConnected = isConnected;
        this.name = name;
        this.macAddress = macAddress;
        this.batteryLevel = batteryLevel;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof DialStatusResponse)) {
                b = false;
            }
            else {
                final DialStatusResponse dialStatusResponse = (DialStatusResponse)o;
                if (!this.equals(this.isPaired, dialStatusResponse.isPaired) || !this.equals(this.isConnected, dialStatusResponse.isConnected) || !this.equals(this.name, dialStatusResponse.name) || !this.equals(this.macAddress, dialStatusResponse.macAddress) || !this.equals(this.batteryLevel, dialStatusResponse.batteryLevel)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.isPaired != null) {
                hashCode3 = this.isPaired.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.isConnected != null) {
                hashCode4 = this.isConnected.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.name != null) {
                hashCode5 = this.name.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.macAddress != null) {
                hashCode6 = this.macAddress.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            if (this.batteryLevel != null) {
                hashCode = this.batteryLevel.hashCode();
            }
            hashCode2 = (((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<DialStatusResponse>
    {
        public Integer batteryLevel;
        public Boolean isConnected;
        public Boolean isPaired;
        public String macAddress;
        public String name;
        
        public Builder() {
        }
        
        public Builder(final DialStatusResponse dialStatusResponse) {
            super(dialStatusResponse);
            if (dialStatusResponse != null) {
                this.isPaired = dialStatusResponse.isPaired;
                this.isConnected = dialStatusResponse.isConnected;
                this.name = dialStatusResponse.name;
                this.macAddress = dialStatusResponse.macAddress;
                this.batteryLevel = dialStatusResponse.batteryLevel;
            }
        }
        
        public Builder batteryLevel(final Integer batteryLevel) {
            this.batteryLevel = batteryLevel;
            return this;
        }
        
        public DialStatusResponse build() {
            return new DialStatusResponse(this, null);
        }
        
        public Builder isConnected(final Boolean isConnected) {
            this.isConnected = isConnected;
            return this;
        }
        
        public Builder isPaired(final Boolean isPaired) {
            this.isPaired = isPaired;
            return this;
        }
        
        public Builder macAddress(final String macAddress) {
            this.macAddress = macAddress;
            return this;
        }
        
        public Builder name(final String name) {
            this.name = name;
            return this;
        }
    }
}
