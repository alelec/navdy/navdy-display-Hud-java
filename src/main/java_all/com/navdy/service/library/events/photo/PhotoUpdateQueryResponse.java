package com.navdy.service.library.events.photo;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class PhotoUpdateQueryResponse extends Message
{
    public static final String DEFAULT_IDENTIFIER = "";
    public static final Boolean DEFAULT_UPDATEREQUIRED;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String identifier;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.BOOL)
    public final Boolean updateRequired;
    
    static {
        DEFAULT_UPDATEREQUIRED = false;
    }
    
    private PhotoUpdateQueryResponse(final Builder builder) {
        this(builder.identifier, builder.updateRequired);
        this.setBuilder((Message.Builder)builder);
    }
    
    public PhotoUpdateQueryResponse(final String identifier, final Boolean updateRequired) {
        this.identifier = identifier;
        this.updateRequired = updateRequired;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof PhotoUpdateQueryResponse)) {
                b = false;
            }
            else {
                final PhotoUpdateQueryResponse photoUpdateQueryResponse = (PhotoUpdateQueryResponse)o;
                if (!this.equals(this.identifier, photoUpdateQueryResponse.identifier) || !this.equals(this.updateRequired, photoUpdateQueryResponse.updateRequired)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.identifier != null) {
                hashCode3 = this.identifier.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            if (this.updateRequired != null) {
                hashCode = this.updateRequired.hashCode();
            }
            hashCode2 = hashCode3 * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<PhotoUpdateQueryResponse>
    {
        public String identifier;
        public Boolean updateRequired;
        
        public Builder() {
        }
        
        public Builder(final PhotoUpdateQueryResponse photoUpdateQueryResponse) {
            super(photoUpdateQueryResponse);
            if (photoUpdateQueryResponse != null) {
                this.identifier = photoUpdateQueryResponse.identifier;
                this.updateRequired = photoUpdateQueryResponse.updateRequired;
            }
        }
        
        public PhotoUpdateQueryResponse build() {
            ((Message.Builder)this).checkRequiredFields();
            return new PhotoUpdateQueryResponse(this, null);
        }
        
        public Builder identifier(final String identifier) {
            this.identifier = identifier;
            return this;
        }
        
        public Builder updateRequired(final Boolean updateRequired) {
            this.updateRequired = updateRequired;
            return this;
        }
    }
}
