package com.navdy.service.library.events.dial;

import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class DialBondRequest extends Message
{
    public static final DialAction DEFAULT_ACTION;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 1, type = Datatype.ENUM)
    public final DialAction action;
    
    static {
        DEFAULT_ACTION = DialAction.DIAL_BOND;
    }
    
    private DialBondRequest(final Builder builder) {
        this(builder.action);
        this.setBuilder((Message.Builder)builder);
    }
    
    public DialBondRequest(final DialAction action) {
        this.action = action;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof DialBondRequest && this.equals(this.action, ((DialBondRequest)o).action));
    }
    
    @Override
    public int hashCode() {
        int hashCode = this.hashCode;
        if (hashCode == 0) {
            if (this.action != null) {
                hashCode = this.action.hashCode();
            }
            else {
                hashCode = 0;
            }
            this.hashCode = hashCode;
        }
        return hashCode;
    }
    
    public static final class Builder extends Message.Builder<DialBondRequest>
    {
        public DialAction action;
        
        public Builder() {
        }
        
        public Builder(final DialBondRequest dialBondRequest) {
            super(dialBondRequest);
            if (dialBondRequest != null) {
                this.action = dialBondRequest.action;
            }
        }
        
        public Builder action(final DialAction action) {
            this.action = action;
            return this;
        }
        
        public DialBondRequest build() {
            return new DialBondRequest(this, null);
        }
    }
    
    public enum DialAction implements ProtoEnum
    {
        DIAL_BOND(1), 
        DIAL_CLEAR_BOND(2), 
        DIAL_REBOND(3);
        
        private final int value;
        
        private DialAction(final int value) {
            this.value = value;
        }
        
        @Override
        public int getValue() {
            return this.value;
        }
    }
}
