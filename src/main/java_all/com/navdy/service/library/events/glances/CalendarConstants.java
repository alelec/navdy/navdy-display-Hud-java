package com.navdy.service.library.events.glances;

import com.squareup.wire.ProtoEnum;

public enum CalendarConstants implements ProtoEnum
{
    CALENDAR_LOCATION(3), 
    CALENDAR_TIME(1), 
    CALENDAR_TIME_STR(2), 
    CALENDAR_TITLE(0);
    
    private final int value;
    
    private CalendarConstants(final int value) {
        this.value = value;
    }
    
    @Override
    public int getValue() {
        return this.value;
    }
}
