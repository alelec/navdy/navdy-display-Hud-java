package com.navdy.service.library.events.places;

import java.util.Collections;
import com.squareup.wire.ProtoField;
import com.navdy.service.library.events.RequestStatus;
import java.util.List;
import com.squareup.wire.Message;

public final class PlacesSearchResponse extends Message
{
    public static final String DEFAULT_REQUESTID = "";
    public static final List<PlacesSearchResult> DEFAULT_RESULTS;
    public static final String DEFAULT_SEARCHQUERY = "";
    public static final RequestStatus DEFAULT_STATUS;
    public static final String DEFAULT_STATUSDETAIL = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 5, type = Datatype.STRING)
    public final String requestId;
    @ProtoField(label = Label.REPEATED, messageType = PlacesSearchResult.class, tag = 4)
    public final List<PlacesSearchResult> results;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String searchQuery;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.ENUM)
    public final RequestStatus status;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String statusDetail;
    
    static {
        DEFAULT_STATUS = RequestStatus.REQUEST_SUCCESS;
        DEFAULT_RESULTS = Collections.<PlacesSearchResult>emptyList();
    }
    
    private PlacesSearchResponse(final Builder builder) {
        this(builder.searchQuery, builder.status, builder.statusDetail, builder.results, builder.requestId);
        this.setBuilder((Message.Builder)builder);
    }
    
    public PlacesSearchResponse(final String searchQuery, final RequestStatus status, final String statusDetail, final List<PlacesSearchResult> list, final String requestId) {
        this.searchQuery = searchQuery;
        this.status = status;
        this.statusDetail = statusDetail;
        this.results = Message.<PlacesSearchResult>immutableCopyOf(list);
        this.requestId = requestId;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof PlacesSearchResponse)) {
                b = false;
            }
            else {
                final PlacesSearchResponse placesSearchResponse = (PlacesSearchResponse)o;
                if (!this.equals(this.searchQuery, placesSearchResponse.searchQuery) || !this.equals(this.status, placesSearchResponse.status) || !this.equals(this.statusDetail, placesSearchResponse.statusDetail) || !this.equals(this.results, placesSearchResponse.results) || !this.equals(this.requestId, placesSearchResponse.requestId)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.searchQuery != null) {
                hashCode3 = this.searchQuery.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.status != null) {
                hashCode4 = this.status.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.statusDetail != null) {
                hashCode5 = this.statusDetail.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.results != null) {
                hashCode6 = this.results.hashCode();
            }
            else {
                hashCode6 = 1;
            }
            if (this.requestId != null) {
                hashCode = this.requestId.hashCode();
            }
            hashCode2 = (((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<PlacesSearchResponse>
    {
        public String requestId;
        public List<PlacesSearchResult> results;
        public String searchQuery;
        public RequestStatus status;
        public String statusDetail;
        
        public Builder() {
        }
        
        public Builder(final PlacesSearchResponse placesSearchResponse) {
            super(placesSearchResponse);
            if (placesSearchResponse != null) {
                this.searchQuery = placesSearchResponse.searchQuery;
                this.status = placesSearchResponse.status;
                this.statusDetail = placesSearchResponse.statusDetail;
                this.results = (List<PlacesSearchResult>)Message.<Object>copyOf((List<Object>)placesSearchResponse.results);
                this.requestId = placesSearchResponse.requestId;
            }
        }
        
        public PlacesSearchResponse build() {
            ((Message.Builder)this).checkRequiredFields();
            return new PlacesSearchResponse(this, null);
        }
        
        public Builder requestId(final String requestId) {
            this.requestId = requestId;
            return this;
        }
        
        public Builder results(final List<PlacesSearchResult> list) {
            this.results = Message.Builder.<PlacesSearchResult>checkForNulls(list);
            return this;
        }
        
        public Builder searchQuery(final String searchQuery) {
            this.searchQuery = searchQuery;
            return this;
        }
        
        public Builder status(final RequestStatus status) {
            this.status = status;
            return this;
        }
        
        public Builder statusDetail(final String statusDetail) {
            this.statusDetail = statusDetail;
            return this;
        }
    }
}
