package com.navdy.service.library.events;

import com.squareup.wire.WireType;

public class WireUtil
{
    private static final NavdyEvent.MessageType[] sMessageTypes;
    
    static {
        sMessageTypes = NavdyEvent.MessageType.values();
    }
    
    public static NavdyEvent.MessageType getEventType(final byte[] array) {
        final int eventType = parseEventType(array);
        NavdyEvent.MessageType messageType;
        if (eventType > 0 && eventType <= WireUtil.sMessageTypes.length) {
            messageType = WireUtil.sMessageTypes[eventType - 1];
        }
        else {
            messageType = null;
        }
        return messageType;
    }
    
    public static int getEventTypeIndex(final byte[] array) {
        return parseEventType(array);
    }
    
    public static int parseEventType(final byte[] array) {
        final byte b = array[0];
        final byte b2 = (byte)(b & 0x7);
        if (WireType.VARINT.value() != b2) {
            throw new RuntimeException("expecting varint:" + b2);
        }
        final int n = b >> 3;
        if (n != 2) {
            throw new RuntimeException("unexpected tag:" + n);
        }
        return readVarint32(array);
    }
    
    public static int readVarint32(final byte[] array) {
        int n = array[1];
        if (n < 0) {
            final int n2 = n & 0x7F;
            int n3 = 1 + 1;
            final byte b = array[n3];
            if (b >= 0) {
                n = (n2 | b << 7);
            }
            else {
                final int n4 = n2 | (b & 0x7F) << 7;
                ++n3;
                final byte b2 = array[n3];
                if (b2 >= 0) {
                    n = (n4 | b2 << 14);
                }
                else {
                    final int n5 = n4 | (b2 & 0x7F) << 14;
                    ++n3;
                    final byte b3 = array[n3];
                    if (b3 >= 0) {
                        n = (n5 | b3 << 21);
                    }
                    else {
                        ++n3;
                        final byte b4 = array[n3];
                        final int n6 = n = (n5 | (b3 & 0x7F) << 21 | b4 << 28);
                        if (b4 < 0) {
                            final boolean b5 = false;
                            int n7 = n3;
                            int n8;
                            for (int i = b5 ? 1 : 0; i < 5; ++i, n7 = n8) {
                                n8 = n7 + 1;
                                n = n6;
                                if (array[n8] >= 0) {
                                    return n;
                                }
                            }
                            throw new RuntimeException("marlformed varint");
                        }
                    }
                }
            }
        }
        return n;
    }
}
