package com.navdy.service.library.events.navigation;

import java.util.Collections;
import com.squareup.wire.ProtoField;
import com.navdy.service.library.events.RequestStatus;
import java.util.List;
import com.squareup.wire.Message;

public final class RouteManeuverResponse extends Message
{
    public static final List<RouteManeuver> DEFAULT_MANEUVERS;
    public static final RequestStatus DEFAULT_STATUS;
    public static final String DEFAULT_STATUSDETAIL = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REPEATED, messageType = RouteManeuver.class, tag = 3)
    public final List<RouteManeuver> maneuvers;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final RequestStatus status;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String statusDetail;
    
    static {
        DEFAULT_STATUS = RequestStatus.REQUEST_SUCCESS;
        DEFAULT_MANEUVERS = Collections.<RouteManeuver>emptyList();
    }
    
    public RouteManeuverResponse(final RequestStatus status, final String statusDetail, final List<RouteManeuver> list) {
        this.status = status;
        this.statusDetail = statusDetail;
        this.maneuvers = Message.<RouteManeuver>immutableCopyOf(list);
    }
    
    private RouteManeuverResponse(final Builder builder) {
        this(builder.status, builder.statusDetail, builder.maneuvers);
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof RouteManeuverResponse)) {
                b = false;
            }
            else {
                final RouteManeuverResponse routeManeuverResponse = (RouteManeuverResponse)o;
                if (!this.equals(this.status, routeManeuverResponse.status) || !this.equals(this.statusDetail, routeManeuverResponse.statusDetail) || !this.equals(this.maneuvers, routeManeuverResponse.maneuvers)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.status != null) {
                hashCode3 = this.status.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            if (this.statusDetail != null) {
                hashCode = this.statusDetail.hashCode();
            }
            int hashCode4;
            if (this.maneuvers != null) {
                hashCode4 = this.maneuvers.hashCode();
            }
            else {
                hashCode4 = 1;
            }
            hashCode2 = (hashCode3 * 37 + hashCode) * 37 + hashCode4;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<RouteManeuverResponse>
    {
        public List<RouteManeuver> maneuvers;
        public RequestStatus status;
        public String statusDetail;
        
        public Builder() {
        }
        
        public Builder(final RouteManeuverResponse routeManeuverResponse) {
            super(routeManeuverResponse);
            if (routeManeuverResponse != null) {
                this.status = routeManeuverResponse.status;
                this.statusDetail = routeManeuverResponse.statusDetail;
                this.maneuvers = (List<RouteManeuver>)Message.<Object>copyOf((List<Object>)routeManeuverResponse.maneuvers);
            }
        }
        
        public RouteManeuverResponse build() {
            ((Message.Builder)this).checkRequiredFields();
            return new RouteManeuverResponse(this, null);
        }
        
        public Builder maneuvers(final List<RouteManeuver> list) {
            this.maneuvers = Message.Builder.<RouteManeuver>checkForNulls(list);
            return this;
        }
        
        public Builder status(final RequestStatus status) {
            this.status = status;
            return this;
        }
        
        public Builder statusDetail(final String statusDetail) {
            this.statusDetail = statusDetail;
            return this;
        }
    }
}
