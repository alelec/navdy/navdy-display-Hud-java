package com.navdy.service.library.events.audio;

import com.squareup.wire.ProtoEnum;

public enum MusicDataSource implements ProtoEnum
{
    MUSIC_SOURCE_ANDROID_INTENT(2), 
    MUSIC_SOURCE_MEDIA_CONTROLLER(3), 
    MUSIC_SOURCE_NONE(1), 
    MUSIC_SOURCE_OS_NOTIFICATION(4), 
    MUSIC_SOURCE_PANDORA_API(6), 
    MUSIC_SOURCE_SPOTIFY_INTENT(5);
    
    private final int value;
    
    private MusicDataSource(final int value) {
        this.value = value;
    }
    
    @Override
    public int getValue() {
        return this.value;
    }
}
