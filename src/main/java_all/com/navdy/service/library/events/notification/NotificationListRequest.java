package com.navdy.service.library.events.notification;

import com.squareup.wire.Message;

public final class NotificationListRequest extends Message
{
    private static final long serialVersionUID = 0L;
    
    public NotificationListRequest() {
    }
    
    private NotificationListRequest(final Builder builder) {
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof NotificationListRequest;
    }
    
    @Override
    public int hashCode() {
        return 0;
    }
    
    public static final class Builder extends Message.Builder<NotificationListRequest>
    {
        public Builder() {
        }
        
        public Builder(final NotificationListRequest notificationListRequest) {
            super(notificationListRequest);
        }
        
        public NotificationListRequest build() {
            return new NotificationListRequest(this, null);
        }
    }
}
