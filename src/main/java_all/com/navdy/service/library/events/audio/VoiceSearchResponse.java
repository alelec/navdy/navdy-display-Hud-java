package com.navdy.service.library.events.audio;

import com.squareup.wire.ProtoEnum;
import java.util.Collections;
import com.squareup.wire.ProtoField;
import com.navdy.service.library.events.destination.Destination;
import java.util.List;
import com.squareup.wire.Message;

public final class VoiceSearchResponse extends Message
{
    public static final Integer DEFAULT_CONFIDENCELEVEL;
    public static final VoiceSearchError DEFAULT_ERROR;
    public static final Boolean DEFAULT_LISTENINGOVERBLUETOOTH;
    public static final String DEFAULT_LOCALE = "";
    public static final String DEFAULT_PREFIX = "";
    public static final String DEFAULT_RECOGNIZEDWORDS = "";
    public static final List<Destination> DEFAULT_RESULTS;
    public static final VoiceSearchState DEFAULT_STATE;
    public static final Integer DEFAULT_VOLUMELEVEL;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 6, type = Datatype.INT32)
    public final Integer confidenceLevel;
    @ProtoField(tag = 5, type = Datatype.ENUM)
    public final VoiceSearchError error;
    @ProtoField(tag = 4, type = Datatype.BOOL)
    public final Boolean listeningOverBluetooth;
    @ProtoField(tag = 9, type = Datatype.STRING)
    public final String locale;
    @ProtoField(tag = 8, type = Datatype.STRING)
    public final String prefix;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String recognizedWords;
    @ProtoField(label = Label.REPEATED, messageType = Destination.class, tag = 7)
    public final List<Destination> results;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final VoiceSearchState state;
    @ProtoField(tag = 3, type = Datatype.INT32)
    public final Integer volumeLevel;
    
    static {
        DEFAULT_STATE = VoiceSearchState.VOICE_SEARCH_ERROR;
        DEFAULT_VOLUMELEVEL = 0;
        DEFAULT_LISTENINGOVERBLUETOOTH = false;
        DEFAULT_ERROR = VoiceSearchError.NOT_AVAILABLE;
        DEFAULT_CONFIDENCELEVEL = 0;
        DEFAULT_RESULTS = Collections.<Destination>emptyList();
    }
    
    private VoiceSearchResponse(final Builder builder) {
        this(builder.state, builder.recognizedWords, builder.volumeLevel, builder.listeningOverBluetooth, builder.error, builder.confidenceLevel, builder.results, builder.prefix, builder.locale);
        this.setBuilder((Message.Builder)builder);
    }
    
    public VoiceSearchResponse(final VoiceSearchState state, final String recognizedWords, final Integer volumeLevel, final Boolean listeningOverBluetooth, final VoiceSearchError error, final Integer confidenceLevel, final List<Destination> list, final String prefix, final String locale) {
        this.state = state;
        this.recognizedWords = recognizedWords;
        this.volumeLevel = volumeLevel;
        this.listeningOverBluetooth = listeningOverBluetooth;
        this.error = error;
        this.confidenceLevel = confidenceLevel;
        this.results = Message.<Destination>immutableCopyOf(list);
        this.prefix = prefix;
        this.locale = locale;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof VoiceSearchResponse)) {
                b = false;
            }
            else {
                final VoiceSearchResponse voiceSearchResponse = (VoiceSearchResponse)o;
                if (!this.equals(this.state, voiceSearchResponse.state) || !this.equals(this.recognizedWords, voiceSearchResponse.recognizedWords) || !this.equals(this.volumeLevel, voiceSearchResponse.volumeLevel) || !this.equals(this.listeningOverBluetooth, voiceSearchResponse.listeningOverBluetooth) || !this.equals(this.error, voiceSearchResponse.error) || !this.equals(this.confidenceLevel, voiceSearchResponse.confidenceLevel) || !this.equals(this.results, voiceSearchResponse.results) || !this.equals(this.prefix, voiceSearchResponse.prefix) || !this.equals(this.locale, voiceSearchResponse.locale)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.state != null) {
                hashCode3 = this.state.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.recognizedWords != null) {
                hashCode4 = this.recognizedWords.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.volumeLevel != null) {
                hashCode5 = this.volumeLevel.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.listeningOverBluetooth != null) {
                hashCode6 = this.listeningOverBluetooth.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            int hashCode7;
            if (this.error != null) {
                hashCode7 = this.error.hashCode();
            }
            else {
                hashCode7 = 0;
            }
            int hashCode8;
            if (this.confidenceLevel != null) {
                hashCode8 = this.confidenceLevel.hashCode();
            }
            else {
                hashCode8 = 0;
            }
            int hashCode9;
            if (this.results != null) {
                hashCode9 = this.results.hashCode();
            }
            else {
                hashCode9 = 1;
            }
            int hashCode10;
            if (this.prefix != null) {
                hashCode10 = this.prefix.hashCode();
            }
            else {
                hashCode10 = 0;
            }
            if (this.locale != null) {
                hashCode = this.locale.hashCode();
            }
            hashCode2 = (((((((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode7) * 37 + hashCode8) * 37 + hashCode9) * 37 + hashCode10) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<VoiceSearchResponse>
    {
        public Integer confidenceLevel;
        public VoiceSearchError error;
        public Boolean listeningOverBluetooth;
        public String locale;
        public String prefix;
        public String recognizedWords;
        public List<Destination> results;
        public VoiceSearchState state;
        public Integer volumeLevel;
        
        public Builder() {
        }
        
        public Builder(final VoiceSearchResponse voiceSearchResponse) {
            super(voiceSearchResponse);
            if (voiceSearchResponse != null) {
                this.state = voiceSearchResponse.state;
                this.recognizedWords = voiceSearchResponse.recognizedWords;
                this.volumeLevel = voiceSearchResponse.volumeLevel;
                this.listeningOverBluetooth = voiceSearchResponse.listeningOverBluetooth;
                this.error = voiceSearchResponse.error;
                this.confidenceLevel = voiceSearchResponse.confidenceLevel;
                this.results = (List<Destination>)Message.<Object>copyOf((List<Object>)voiceSearchResponse.results);
                this.prefix = voiceSearchResponse.prefix;
                this.locale = voiceSearchResponse.locale;
            }
        }
        
        public VoiceSearchResponse build() {
            ((Message.Builder)this).checkRequiredFields();
            return new VoiceSearchResponse(this, null);
        }
        
        public Builder confidenceLevel(final Integer confidenceLevel) {
            this.confidenceLevel = confidenceLevel;
            return this;
        }
        
        public Builder error(final VoiceSearchError error) {
            this.error = error;
            return this;
        }
        
        public Builder listeningOverBluetooth(final Boolean listeningOverBluetooth) {
            this.listeningOverBluetooth = listeningOverBluetooth;
            return this;
        }
        
        public Builder locale(final String locale) {
            this.locale = locale;
            return this;
        }
        
        public Builder prefix(final String prefix) {
            this.prefix = prefix;
            return this;
        }
        
        public Builder recognizedWords(final String recognizedWords) {
            this.recognizedWords = recognizedWords;
            return this;
        }
        
        public Builder results(final List<Destination> list) {
            this.results = Message.Builder.<Destination>checkForNulls(list);
            return this;
        }
        
        public Builder state(final VoiceSearchState state) {
            this.state = state;
            return this;
        }
        
        public Builder volumeLevel(final Integer volumeLevel) {
            this.volumeLevel = volumeLevel;
            return this;
        }
    }
    
    public enum VoiceSearchError implements ProtoEnum
    {
        AMBIENT_NOISE(6), 
        FAILED_TO_START(3), 
        NEED_PERMISSION(1), 
        NOT_AVAILABLE(0), 
        NO_RESULTS_FOUND(5), 
        NO_WORDS_RECOGNIZED(4), 
        OFFLINE(2), 
        VOICE_SEARCH_ERROR_CARPLAY_BLACKLIST(7);
        
        private final int value;
        
        private VoiceSearchError(final int value) {
            this.value = value;
        }
        
        @Override
        public int getValue() {
            return this.value;
        }
    }
    
    public enum VoiceSearchState implements ProtoEnum
    {
        VOICE_SEARCH_ERROR(0), 
        VOICE_SEARCH_LISTENING(2), 
        VOICE_SEARCH_SEARCHING(3), 
        VOICE_SEARCH_STARTING(1), 
        VOICE_SEARCH_SUCCESS(4);
        
        private final int value;
        
        private VoiceSearchState(final int value) {
            this.value = value;
        }
        
        @Override
        public int getValue() {
            return this.value;
        }
    }
}
