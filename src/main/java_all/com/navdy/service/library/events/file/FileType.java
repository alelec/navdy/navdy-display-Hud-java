package com.navdy.service.library.events.file;

import com.squareup.wire.ProtoEnum;

public enum FileType implements ProtoEnum
{
    FILE_TYPE_LOGS(2), 
    FILE_TYPE_OTA(1), 
    FILE_TYPE_PERF_TEST(3);
    
    private final int value;
    
    private FileType(final int value) {
        this.value = value;
    }
    
    @Override
    public int getValue() {
        return this.value;
    }
}
