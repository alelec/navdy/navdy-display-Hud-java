package com.navdy.service.library.events.notification;

import java.util.Collections;
import com.squareup.wire.ProtoField;
import okio.ByteString;
import java.util.List;
import com.squareup.wire.Message;

public final class NotificationEvent extends Message
{
    public static final List<NotificationAction> DEFAULT_ACTIONS;
    public static final String DEFAULT_APPID = "";
    public static final String DEFAULT_APPNAME = "";
    public static final Boolean DEFAULT_CANNOTREPLYBACK;
    public static final NotificationCategory DEFAULT_CATEGORY;
    public static final String DEFAULT_ICONRESOURCENAME = "";
    public static final Integer DEFAULT_ID;
    public static final String DEFAULT_IMAGERESOURCENAME = "";
    public static final String DEFAULT_MESSAGE = "";
    public static final ByteString DEFAULT_PHOTO;
    public static final String DEFAULT_SOURCEIDENTIFIER = "";
    public static final String DEFAULT_SUBTITLE = "";
    public static final String DEFAULT_TITLE = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REPEATED, messageType = NotificationAction.class, tag = 7)
    public final List<NotificationAction> actions;
    @ProtoField(tag = 6, type = Datatype.STRING)
    public final String appId;
    @ProtoField(tag = 13, type = Datatype.STRING)
    public final String appName;
    @ProtoField(tag = 12, type = Datatype.BOOL)
    public final Boolean cannotReplyBack;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.ENUM)
    public final NotificationCategory category;
    @ProtoField(tag = 9, type = Datatype.STRING)
    public final String iconResourceName;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.INT32)
    public final Integer id;
    @ProtoField(tag = 10, type = Datatype.STRING)
    public final String imageResourceName;
    @ProtoField(tag = 5, type = Datatype.STRING)
    public final String message;
    @ProtoField(tag = 8, type = Datatype.BYTES)
    public final ByteString photo;
    @ProtoField(tag = 11, type = Datatype.STRING)
    public final String sourceIdentifier;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String subtitle;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String title;
    
    static {
        DEFAULT_ID = 0;
        DEFAULT_CATEGORY = NotificationCategory.CATEGORY_OTHER;
        DEFAULT_ACTIONS = Collections.<NotificationAction>emptyList();
        DEFAULT_PHOTO = ByteString.EMPTY;
        DEFAULT_CANNOTREPLYBACK = false;
    }
    
    private NotificationEvent(final Builder builder) {
        this(builder.id, builder.category, builder.title, builder.subtitle, builder.message, builder.appId, builder.actions, builder.photo, builder.iconResourceName, builder.imageResourceName, builder.sourceIdentifier, builder.cannotReplyBack, builder.appName);
        this.setBuilder((Message.Builder)builder);
    }
    
    public NotificationEvent(final Integer id, final NotificationCategory category, final String title, final String subtitle, final String message, final String appId, final List<NotificationAction> list, final ByteString photo, final String iconResourceName, final String imageResourceName, final String sourceIdentifier, final Boolean cannotReplyBack, final String appName) {
        this.id = id;
        this.category = category;
        this.title = title;
        this.subtitle = subtitle;
        this.message = message;
        this.appId = appId;
        this.actions = Message.<NotificationAction>immutableCopyOf(list);
        this.photo = photo;
        this.iconResourceName = iconResourceName;
        this.imageResourceName = imageResourceName;
        this.sourceIdentifier = sourceIdentifier;
        this.cannotReplyBack = cannotReplyBack;
        this.appName = appName;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof NotificationEvent)) {
                b = false;
            }
            else {
                final NotificationEvent notificationEvent = (NotificationEvent)o;
                if (!this.equals(this.id, notificationEvent.id) || !this.equals(this.category, notificationEvent.category) || !this.equals(this.title, notificationEvent.title) || !this.equals(this.subtitle, notificationEvent.subtitle) || !this.equals(this.message, notificationEvent.message) || !this.equals(this.appId, notificationEvent.appId) || !this.equals(this.actions, notificationEvent.actions) || !this.equals(this.photo, notificationEvent.photo) || !this.equals(this.iconResourceName, notificationEvent.iconResourceName) || !this.equals(this.imageResourceName, notificationEvent.imageResourceName) || !this.equals(this.sourceIdentifier, notificationEvent.sourceIdentifier) || !this.equals(this.cannotReplyBack, notificationEvent.cannotReplyBack) || !this.equals(this.appName, notificationEvent.appName)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.id != null) {
                hashCode3 = this.id.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.category != null) {
                hashCode4 = this.category.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.title != null) {
                hashCode5 = this.title.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.subtitle != null) {
                hashCode6 = this.subtitle.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            int hashCode7;
            if (this.message != null) {
                hashCode7 = this.message.hashCode();
            }
            else {
                hashCode7 = 0;
            }
            int hashCode8;
            if (this.appId != null) {
                hashCode8 = this.appId.hashCode();
            }
            else {
                hashCode8 = 0;
            }
            int hashCode9;
            if (this.actions != null) {
                hashCode9 = this.actions.hashCode();
            }
            else {
                hashCode9 = 1;
            }
            int hashCode10;
            if (this.photo != null) {
                hashCode10 = this.photo.hashCode();
            }
            else {
                hashCode10 = 0;
            }
            int hashCode11;
            if (this.iconResourceName != null) {
                hashCode11 = this.iconResourceName.hashCode();
            }
            else {
                hashCode11 = 0;
            }
            int hashCode12;
            if (this.imageResourceName != null) {
                hashCode12 = this.imageResourceName.hashCode();
            }
            else {
                hashCode12 = 0;
            }
            int hashCode13;
            if (this.sourceIdentifier != null) {
                hashCode13 = this.sourceIdentifier.hashCode();
            }
            else {
                hashCode13 = 0;
            }
            int hashCode14;
            if (this.cannotReplyBack != null) {
                hashCode14 = this.cannotReplyBack.hashCode();
            }
            else {
                hashCode14 = 0;
            }
            if (this.appName != null) {
                hashCode = this.appName.hashCode();
            }
            hashCode2 = (((((((((((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode7) * 37 + hashCode8) * 37 + hashCode9) * 37 + hashCode10) * 37 + hashCode11) * 37 + hashCode12) * 37 + hashCode13) * 37 + hashCode14) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<NotificationEvent>
    {
        public List<NotificationAction> actions;
        public String appId;
        public String appName;
        public Boolean cannotReplyBack;
        public NotificationCategory category;
        public String iconResourceName;
        public Integer id;
        public String imageResourceName;
        public String message;
        public ByteString photo;
        public String sourceIdentifier;
        public String subtitle;
        public String title;
        
        public Builder() {
        }
        
        public Builder(final NotificationEvent notificationEvent) {
            super(notificationEvent);
            if (notificationEvent != null) {
                this.id = notificationEvent.id;
                this.category = notificationEvent.category;
                this.title = notificationEvent.title;
                this.subtitle = notificationEvent.subtitle;
                this.message = notificationEvent.message;
                this.appId = notificationEvent.appId;
                this.actions = (List<NotificationAction>)Message.<Object>copyOf((List<Object>)notificationEvent.actions);
                this.photo = notificationEvent.photo;
                this.iconResourceName = notificationEvent.iconResourceName;
                this.imageResourceName = notificationEvent.imageResourceName;
                this.sourceIdentifier = notificationEvent.sourceIdentifier;
                this.cannotReplyBack = notificationEvent.cannotReplyBack;
                this.appName = notificationEvent.appName;
            }
        }
        
        public Builder actions(final List<NotificationAction> list) {
            this.actions = Message.Builder.<NotificationAction>checkForNulls(list);
            return this;
        }
        
        public Builder appId(final String appId) {
            this.appId = appId;
            return this;
        }
        
        public Builder appName(final String appName) {
            this.appName = appName;
            return this;
        }
        
        public NotificationEvent build() {
            ((Message.Builder)this).checkRequiredFields();
            return new NotificationEvent(this, null);
        }
        
        public Builder cannotReplyBack(final Boolean cannotReplyBack) {
            this.cannotReplyBack = cannotReplyBack;
            return this;
        }
        
        public Builder category(final NotificationCategory category) {
            this.category = category;
            return this;
        }
        
        public Builder iconResourceName(final String iconResourceName) {
            this.iconResourceName = iconResourceName;
            return this;
        }
        
        public Builder id(final Integer id) {
            this.id = id;
            return this;
        }
        
        public Builder imageResourceName(final String imageResourceName) {
            this.imageResourceName = imageResourceName;
            return this;
        }
        
        public Builder message(final String message) {
            this.message = message;
            return this;
        }
        
        public Builder photo(final ByteString photo) {
            this.photo = photo;
            return this;
        }
        
        public Builder sourceIdentifier(final String sourceIdentifier) {
            this.sourceIdentifier = sourceIdentifier;
            return this;
        }
        
        public Builder subtitle(final String subtitle) {
            this.subtitle = subtitle;
            return this;
        }
        
        public Builder title(final String title) {
            this.title = title;
            return this;
        }
    }
}
