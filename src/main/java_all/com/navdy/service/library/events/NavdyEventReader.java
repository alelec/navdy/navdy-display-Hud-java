package com.navdy.service.library.events;

import java.io.IOException;
import java.io.InputStream;
import com.squareup.wire.Wire;
import java.io.DataInputStream;
import com.navdy.service.library.log.Logger;

public class NavdyEventReader
{
    private static final int FRAME_LENGTH = 5;
    private static final Logger sLogger;
    protected DataInputStream mInputStream;
    protected Wire mWire;
    
    static {
        sLogger = new Logger(NavdyEventReader.class);
    }
    
    public NavdyEventReader(final InputStream inputStream) {
        this.mInputStream = new DataInputStream(inputStream);
        this.mWire = new Wire((Class<?>[])new Class[] { Ext_NavdyEvent.class });
    }
    
    private byte[] readBytes(final int n) throws IOException {
        final byte[] array = new byte[n];
        this.mInputStream.readFully(array);
        return array;
    }
    
    private int readSize() throws IOException {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: iconst_5       
        //     2: invokespecial   com/navdy/service/library/events/NavdyEventReader.readBytes:(I)[B
        //     5: astore_1       
        //     6: aconst_null    
        //     7: astore_2       
        //     8: aload_0        
        //     9: getfield        com/navdy/service/library/events/NavdyEventReader.mWire:Lcom/squareup/wire/Wire;
        //    12: aload_1        
        //    13: ldc             Lcom/navdy/service/library/events/Frame;.class
        //    15: invokevirtual   com/squareup/wire/Wire.parseFrom:([BLjava/lang/Class;)Lcom/squareup/wire/Message;
        //    18: checkcast       Lcom/navdy/service/library/events/Frame;
        //    21: astore_1       
        //    22: aload_1        
        //    23: astore_2       
        //    24: aload_2        
        //    25: ifnonnull       51
        //    28: iconst_0       
        //    29: istore_3       
        //    30: iload_3        
        //    31: ireturn        
        //    32: astore_2       
        //    33: iconst_0       
        //    34: istore_3       
        //    35: goto            30
        //    38: astore_1       
        //    39: getstatic       com/navdy/service/library/events/NavdyEventReader.sLogger:Lcom/navdy/service/library/log/Logger;
        //    42: ldc             "Failed to parse frame"
        //    44: aload_1        
        //    45: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //    48: goto            24
        //    51: aload_2        
        //    52: getfield        com/navdy/service/library/events/Frame.size:Ljava/lang/Integer;
        //    55: invokevirtual   java/lang/Integer.intValue:()I
        //    58: istore_3       
        //    59: goto            30
        //    Exceptions:
        //  throws java.io.IOException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  0      6      32     38     Ljava/lang/Exception;
        //  8      22     38     51     Ljava/lang/Throwable;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0024:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public byte[] readBytes() throws IOException {
        final int size = this.readSize();
        byte[] bytes;
        if (size <= 0) {
            bytes = null;
        }
        else {
            bytes = this.readBytes(size);
        }
        return bytes;
    }
}
