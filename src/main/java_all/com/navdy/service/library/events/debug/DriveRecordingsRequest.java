package com.navdy.service.library.events.debug;

import com.squareup.wire.Message;

public final class DriveRecordingsRequest extends Message
{
    private static final long serialVersionUID = 0L;
    
    public DriveRecordingsRequest() {
    }
    
    private DriveRecordingsRequest(final Builder builder) {
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof DriveRecordingsRequest;
    }
    
    @Override
    public int hashCode() {
        return 0;
    }
    
    public static final class Builder extends Message.Builder<DriveRecordingsRequest>
    {
        public Builder() {
        }
        
        public Builder(final DriveRecordingsRequest driveRecordingsRequest) {
            super(driveRecordingsRequest);
        }
        
        public DriveRecordingsRequest build() {
            return new DriveRecordingsRequest(this, null);
        }
    }
}
