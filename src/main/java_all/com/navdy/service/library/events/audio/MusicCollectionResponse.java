package com.navdy.service.library.events.audio;

import java.util.Collections;
import com.squareup.wire.ProtoField;
import java.util.List;
import com.squareup.wire.Message;

public final class MusicCollectionResponse extends Message
{
    public static final List<MusicCharacterMap> DEFAULT_CHARACTERMAP;
    public static final List<MusicCollectionInfo> DEFAULT_MUSICCOLLECTIONS;
    public static final List<MusicTrackInfo> DEFAULT_MUSICTRACKS;
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REPEATED, messageType = MusicCharacterMap.class, tag = 4)
    public final List<MusicCharacterMap> characterMap;
    @ProtoField(tag = 1)
    public final MusicCollectionInfo collectionInfo;
    @ProtoField(label = Label.REPEATED, messageType = MusicCollectionInfo.class, tag = 2)
    public final List<MusicCollectionInfo> musicCollections;
    @ProtoField(label = Label.REPEATED, messageType = MusicTrackInfo.class, tag = 3)
    public final List<MusicTrackInfo> musicTracks;
    
    static {
        DEFAULT_MUSICCOLLECTIONS = Collections.<MusicCollectionInfo>emptyList();
        DEFAULT_MUSICTRACKS = Collections.<MusicTrackInfo>emptyList();
        DEFAULT_CHARACTERMAP = Collections.<MusicCharacterMap>emptyList();
    }
    
    public MusicCollectionResponse(final MusicCollectionInfo collectionInfo, final List<MusicCollectionInfo> list, final List<MusicTrackInfo> list2, final List<MusicCharacterMap> list3) {
        this.collectionInfo = collectionInfo;
        this.musicCollections = Message.<MusicCollectionInfo>immutableCopyOf(list);
        this.musicTracks = Message.<MusicTrackInfo>immutableCopyOf(list2);
        this.characterMap = Message.<MusicCharacterMap>immutableCopyOf(list3);
    }
    
    private MusicCollectionResponse(final Builder builder) {
        this(builder.collectionInfo, builder.musicCollections, builder.musicTracks, builder.characterMap);
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof MusicCollectionResponse)) {
                b = false;
            }
            else {
                final MusicCollectionResponse musicCollectionResponse = (MusicCollectionResponse)o;
                if (!this.equals(this.collectionInfo, musicCollectionResponse.collectionInfo) || !this.equals(this.musicCollections, musicCollectionResponse.musicCollections) || !this.equals(this.musicTracks, musicCollectionResponse.musicTracks) || !this.equals(this.characterMap, musicCollectionResponse.characterMap)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 1;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.collectionInfo != null) {
                hashCode3 = this.collectionInfo.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.musicCollections != null) {
                hashCode4 = this.musicCollections.hashCode();
            }
            else {
                hashCode4 = 1;
            }
            int hashCode5;
            if (this.musicTracks != null) {
                hashCode5 = this.musicTracks.hashCode();
            }
            else {
                hashCode5 = 1;
            }
            if (this.characterMap != null) {
                hashCode = this.characterMap.hashCode();
            }
            hashCode2 = ((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<MusicCollectionResponse>
    {
        public List<MusicCharacterMap> characterMap;
        public MusicCollectionInfo collectionInfo;
        public List<MusicCollectionInfo> musicCollections;
        public List<MusicTrackInfo> musicTracks;
        
        public Builder() {
        }
        
        public Builder(final MusicCollectionResponse musicCollectionResponse) {
            super(musicCollectionResponse);
            if (musicCollectionResponse != null) {
                this.collectionInfo = musicCollectionResponse.collectionInfo;
                this.musicCollections = (List<MusicCollectionInfo>)Message.<Object>copyOf((List<Object>)musicCollectionResponse.musicCollections);
                this.musicTracks = (List<MusicTrackInfo>)Message.<Object>copyOf((List<Object>)musicCollectionResponse.musicTracks);
                this.characterMap = (List<MusicCharacterMap>)Message.<Object>copyOf((List<Object>)musicCollectionResponse.characterMap);
            }
        }
        
        public MusicCollectionResponse build() {
            return new MusicCollectionResponse(this, null);
        }
        
        public Builder characterMap(final List<MusicCharacterMap> list) {
            this.characterMap = Message.Builder.<MusicCharacterMap>checkForNulls(list);
            return this;
        }
        
        public Builder collectionInfo(final MusicCollectionInfo collectionInfo) {
            this.collectionInfo = collectionInfo;
            return this;
        }
        
        public Builder musicCollections(final List<MusicCollectionInfo> list) {
            this.musicCollections = Message.Builder.<MusicCollectionInfo>checkForNulls(list);
            return this;
        }
        
        public Builder musicTracks(final List<MusicTrackInfo> list) {
            this.musicTracks = Message.Builder.<MusicTrackInfo>checkForNulls(list);
            return this;
        }
    }
}
