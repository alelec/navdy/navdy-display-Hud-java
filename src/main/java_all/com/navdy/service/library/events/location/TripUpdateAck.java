package com.navdy.service.library.events.location;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class TripUpdateAck extends Message
{
    public static final Integer DEFAULT_SEQUENCE_NUMBER;
    public static final Long DEFAULT_TRIP_NUMBER;
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.INT32)
    public final Integer sequence_number;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.INT64)
    public final Long trip_number;
    
    static {
        DEFAULT_TRIP_NUMBER = 0L;
        DEFAULT_SEQUENCE_NUMBER = 0;
    }
    
    private TripUpdateAck(final Builder builder) {
        this(builder.trip_number, builder.sequence_number);
        this.setBuilder((Message.Builder)builder);
    }
    
    public TripUpdateAck(final Long trip_number, final Integer sequence_number) {
        this.trip_number = trip_number;
        this.sequence_number = sequence_number;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof TripUpdateAck)) {
                b = false;
            }
            else {
                final TripUpdateAck tripUpdateAck = (TripUpdateAck)o;
                if (!this.equals(this.trip_number, tripUpdateAck.trip_number) || !this.equals(this.sequence_number, tripUpdateAck.sequence_number)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.trip_number != null) {
                hashCode3 = this.trip_number.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            if (this.sequence_number != null) {
                hashCode = this.sequence_number.hashCode();
            }
            hashCode2 = hashCode3 * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<TripUpdateAck>
    {
        public Integer sequence_number;
        public Long trip_number;
        
        public Builder() {
        }
        
        public Builder(final TripUpdateAck tripUpdateAck) {
            super(tripUpdateAck);
            if (tripUpdateAck != null) {
                this.trip_number = tripUpdateAck.trip_number;
                this.sequence_number = tripUpdateAck.sequence_number;
            }
        }
        
        public TripUpdateAck build() {
            ((Message.Builder)this).checkRequiredFields();
            return new TripUpdateAck(this, null);
        }
        
        public Builder sequence_number(final Integer sequence_number) {
            this.sequence_number = sequence_number;
            return this;
        }
        
        public Builder trip_number(final Long trip_number) {
            this.trip_number = trip_number;
            return this;
        }
    }
}
