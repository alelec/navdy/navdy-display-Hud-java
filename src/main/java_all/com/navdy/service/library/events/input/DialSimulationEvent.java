package com.navdy.service.library.events.input;

import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class DialSimulationEvent extends Message
{
    public static final DialInputAction DEFAULT_DIALACTION;
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final DialInputAction dialAction;
    
    static {
        DEFAULT_DIALACTION = DialInputAction.DIAL_CLICK;
    }
    
    private DialSimulationEvent(final Builder builder) {
        this(builder.dialAction);
        this.setBuilder((Message.Builder)builder);
    }
    
    public DialSimulationEvent(final DialInputAction dialAction) {
        this.dialAction = dialAction;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof DialSimulationEvent && this.equals(this.dialAction, ((DialSimulationEvent)o).dialAction));
    }
    
    @Override
    public int hashCode() {
        int hashCode = this.hashCode;
        if (hashCode == 0) {
            if (this.dialAction != null) {
                hashCode = this.dialAction.hashCode();
            }
            else {
                hashCode = 0;
            }
            this.hashCode = hashCode;
        }
        return hashCode;
    }
    
    public static final class Builder extends Message.Builder<DialSimulationEvent>
    {
        public DialInputAction dialAction;
        
        public Builder() {
        }
        
        public Builder(final DialSimulationEvent dialSimulationEvent) {
            super(dialSimulationEvent);
            if (dialSimulationEvent != null) {
                this.dialAction = dialSimulationEvent.dialAction;
            }
        }
        
        public DialSimulationEvent build() {
            ((Message.Builder)this).checkRequiredFields();
            return new DialSimulationEvent(this, null);
        }
        
        public Builder dialAction(final DialInputAction dialAction) {
            this.dialAction = dialAction;
            return this;
        }
    }
    
    public enum DialInputAction implements ProtoEnum
    {
        DIAL_CLICK(1), 
        DIAL_LEFT_TURN(3), 
        DIAL_LONG_CLICK(2), 
        DIAL_RIGHT_TURN(4);
        
        private final int value;
        
        private DialInputAction(final int value) {
            this.value = value;
        }
        
        @Override
        public int getValue() {
            return this.value;
        }
    }
}
