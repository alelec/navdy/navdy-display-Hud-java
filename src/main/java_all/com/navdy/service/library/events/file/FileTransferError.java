package com.navdy.service.library.events.file;

import com.squareup.wire.ProtoEnum;

public enum FileTransferError implements ProtoEnum
{
    FILE_TRANSFER_ABORTED(3), 
    FILE_TRANSFER_CHECKSUM_ERROR(8), 
    FILE_TRANSFER_HOST_BUSY(9), 
    FILE_TRANSFER_ILLEGAL_CHUNK(5), 
    FILE_TRANSFER_INSUFFICIENT_SPACE(2), 
    FILE_TRANSFER_IO_ERROR(4), 
    FILE_TRANSFER_NOT_INITIATED(6), 
    FILE_TRANSFER_NO_ERROR(1), 
    FILE_TRANSFER_PERMISSION_DENIED(7);
    
    private final int value;
    
    private FileTransferError(final int value) {
        this.value = value;
    }
    
    @Override
    public int getValue() {
        return this.value;
    }
}
