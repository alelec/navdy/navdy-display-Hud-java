package com.navdy.service.library.util;

import java.util.ArrayList;
import android.net.NetworkInfo;
import android.net.ConnectivityManager;
import java.util.List;
import java.util.Iterator;
import android.text.TextUtils;
import android.app.ActivityManager;
import android.app.ActivityManager;
import android.content.Context;
import java.io.Closeable;
import java.util.StringTokenizer;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.StringReader;
import java.io.InputStream;
import com.navdy.service.library.log.Logger;

public class SystemUtils
{
    private static final String COMMA = ",";
    private static final String PERCENTAGE = "%";
    private static final String SPACE = " ";
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(SystemUtils.class);
    }
    
    private static int extractNumberFromPercent(String trim) {
        int int1;
        final int n = int1 = 0;
        if (trim == null) {
            return int1;
        }
        try {
            final String trim2 = trim.trim();
            final int index = trim2.indexOf("%");
            trim = trim2;
            if (index != -1) {
                trim = trim2.substring(0, index).trim();
            }
            int1 = Integer.parseInt(trim);
            return int1;
        }
        catch (Throwable t) {
            SystemUtils.sLogger.e(t);
            int1 = n;
            return int1;
        }
    }
    
    public static CpuInfo getCpuUsage() {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore_0       
        //     2: aconst_null    
        //     3: astore_1       
        //     4: aload_1        
        //     5: astore_2       
        //     6: aload_0        
        //     7: astore_3       
        //     8: invokestatic    java/lang/Runtime.getRuntime:()Ljava/lang/Runtime;
        //    11: ldc             "top -m 5 -t -n 1"
        //    13: invokevirtual   java/lang/Runtime.exec:(Ljava/lang/String;)Ljava/lang/Process;
        //    16: astore          4
        //    18: aload_1        
        //    19: astore_2       
        //    20: aload_0        
        //    21: astore_3       
        //    22: aload           4
        //    24: invokevirtual   java/lang/Process.waitFor:()I
        //    27: pop            
        //    28: aload_1        
        //    29: astore_2       
        //    30: aload_0        
        //    31: astore_3       
        //    32: aload           4
        //    34: invokevirtual   java/lang/Process.getInputStream:()Ljava/io/InputStream;
        //    37: astore          4
        //    39: aload           4
        //    41: astore_2       
        //    42: aload           4
        //    44: astore_3       
        //    45: aload           4
        //    47: ldc             "UTF-8"
        //    49: invokestatic    com/navdy/service/library/util/IOUtils.convertInputStreamToString:(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;
        //    52: astore_1       
        //    53: aload           4
        //    55: astore_2       
        //    56: aload           4
        //    58: astore_3       
        //    59: new             Ljava/io/BufferedReader;
        //    62: astore          5
        //    64: aload           4
        //    66: astore_2       
        //    67: aload           4
        //    69: astore_3       
        //    70: new             Ljava/io/StringReader;
        //    73: astore_0       
        //    74: aload           4
        //    76: astore_2       
        //    77: aload           4
        //    79: astore_3       
        //    80: aload_0        
        //    81: aload_1        
        //    82: invokespecial   java/io/StringReader.<init>:(Ljava/lang/String;)V
        //    85: aload           4
        //    87: astore_2       
        //    88: aload           4
        //    90: astore_3       
        //    91: aload           5
        //    93: aload_0        
        //    94: invokespecial   java/io/BufferedReader.<init>:(Ljava/io/Reader;)V
        //    97: iconst_1       
        //    98: istore          6
        //   100: aconst_null    
        //   101: astore_1       
        //   102: aconst_null    
        //   103: astore          7
        //   105: aload           4
        //   107: astore_2       
        //   108: aload           4
        //   110: astore_3       
        //   111: new             Ljava/util/ArrayList;
        //   114: astore          8
        //   116: aload           4
        //   118: astore_2       
        //   119: aload           4
        //   121: astore_3       
        //   122: aload           8
        //   124: invokespecial   java/util/ArrayList.<init>:()V
        //   127: aload           4
        //   129: astore_2       
        //   130: aload           4
        //   132: astore_3       
        //   133: aload           5
        //   135: invokevirtual   java/io/BufferedReader.readLine:()Ljava/lang/String;
        //   138: astore          9
        //   140: aload           9
        //   142: ifnull          700
        //   145: aload           4
        //   147: astore_2       
        //   148: aload           4
        //   150: astore_3       
        //   151: aload           9
        //   153: invokevirtual   java/lang/String.trim:()Ljava/lang/String;
        //   156: invokevirtual   java/lang/String.length:()I
        //   159: ifeq            127
        //   162: iload           6
        //   164: iconst_1       
        //   165: if_icmpne       309
        //   168: aload           4
        //   170: astore_2       
        //   171: aload           4
        //   173: astore_3       
        //   174: new             Ljava/util/StringTokenizer;
        //   177: astore          10
        //   179: aload           4
        //   181: astore_2       
        //   182: aload           4
        //   184: astore_3       
        //   185: aload           10
        //   187: aload           9
        //   189: ldc             ","
        //   191: invokespecial   java/util/StringTokenizer.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   194: aload           7
        //   196: astore          11
        //   198: aload_1        
        //   199: astore_0       
        //   200: aload           4
        //   202: astore_2       
        //   203: aload           4
        //   205: astore_3       
        //   206: aload           10
        //   208: invokevirtual   java/util/StringTokenizer.hasMoreElements:()Z
        //   211: ifeq            297
        //   214: aload           4
        //   216: astore_2       
        //   217: aload           4
        //   219: astore_3       
        //   220: aload           10
        //   222: invokevirtual   java/util/StringTokenizer.nextElement:()Ljava/lang/Object;
        //   225: checkcast       Ljava/lang/String;
        //   228: invokevirtual   java/lang/String.trim:()Ljava/lang/String;
        //   231: astore_0       
        //   232: aload           4
        //   234: astore_2       
        //   235: aload           4
        //   237: astore_3       
        //   238: aload_0        
        //   239: ldc             " "
        //   241: invokevirtual   java/lang/String.indexOf:(Ljava/lang/String;)I
        //   244: istore          12
        //   246: iload           12
        //   248: iflt            194
        //   251: aload_1        
        //   252: ifnonnull       276
        //   255: aload           4
        //   257: astore_2       
        //   258: aload           4
        //   260: astore_3       
        //   261: aload_0        
        //   262: iload           12
        //   264: iconst_1       
        //   265: iadd           
        //   266: invokevirtual   java/lang/String.substring:(I)Ljava/lang/String;
        //   269: invokevirtual   java/lang/String.trim:()Ljava/lang/String;
        //   272: astore_1       
        //   273: goto            194
        //   276: aload           4
        //   278: astore_2       
        //   279: aload           4
        //   281: astore_3       
        //   282: aload_0        
        //   283: iload           12
        //   285: iconst_1       
        //   286: iadd           
        //   287: invokevirtual   java/lang/String.substring:(I)Ljava/lang/String;
        //   290: invokevirtual   java/lang/String.trim:()Ljava/lang/String;
        //   293: astore          11
        //   295: aload_1        
        //   296: astore_0       
        //   297: iinc            6, 1
        //   300: aload           11
        //   302: astore          7
        //   304: aload_0        
        //   305: astore_1       
        //   306: goto            127
        //   309: aload           7
        //   311: astore          11
        //   313: aload_1        
        //   314: astore_0       
        //   315: iload           6
        //   317: iconst_4       
        //   318: if_icmplt       297
        //   321: aconst_null    
        //   322: astore          11
        //   324: aconst_null    
        //   325: astore          10
        //   327: aconst_null    
        //   328: astore          13
        //   330: aload           4
        //   332: astore_2       
        //   333: aload           4
        //   335: astore_3       
        //   336: new             Ljava/util/StringTokenizer;
        //   339: astore          14
        //   341: aload           4
        //   343: astore_2       
        //   344: aload           4
        //   346: astore_3       
        //   347: aload           14
        //   349: aload           9
        //   351: ldc             " "
        //   353: invokespecial   java/util/StringTokenizer.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   356: iconst_1       
        //   357: istore          15
        //   359: iconst_0       
        //   360: istore          16
        //   362: iconst_0       
        //   363: istore          12
        //   365: aload           4
        //   367: astore_2       
        //   368: aload           4
        //   370: astore_3       
        //   371: aload           14
        //   373: invokevirtual   java/util/StringTokenizer.hasMoreElements:()Z
        //   376: ifeq            626
        //   379: aload           4
        //   381: astore_2       
        //   382: aload           4
        //   384: astore_3       
        //   385: aload           14
        //   387: invokevirtual   java/util/StringTokenizer.nextElement:()Ljava/lang/Object;
        //   390: checkcast       Ljava/lang/String;
        //   393: astore_0       
        //   394: iload           16
        //   396: istore          17
        //   398: iload           12
        //   400: istore          18
        //   402: aload           13
        //   404: astore_3       
        //   405: aload           10
        //   407: astore_2       
        //   408: aload           11
        //   410: astore          9
        //   412: iload           15
        //   414: tableswitch {
        //                2: 514
        //                3: 540
        //                4: 490
        //                5: 566
        //                6: 490
        //                7: 490
        //                8: 490
        //                9: 490
        //               10: 490
        //               11: 586
        //               12: 606
        //          default: 472
        //        }
        //   472: aload           11
        //   474: astore          9
        //   476: aload           10
        //   478: astore_2       
        //   479: aload           13
        //   481: astore_3       
        //   482: iload           12
        //   484: istore          18
        //   486: iload           16
        //   488: istore          17
        //   490: iinc            15, 1
        //   493: iload           17
        //   495: istore          16
        //   497: iload           18
        //   499: istore          12
        //   501: aload_3        
        //   502: astore          13
        //   504: aload_2        
        //   505: astore          10
        //   507: aload           9
        //   509: astore          11
        //   511: goto            365
        //   514: aload           4
        //   516: astore_3       
        //   517: aload_0        
        //   518: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //   521: istore          17
        //   523: iload           12
        //   525: istore          18
        //   527: aload           13
        //   529: astore_3       
        //   530: aload           10
        //   532: astore_2       
        //   533: aload           11
        //   535: astore          9
        //   537: goto            490
        //   540: aload           4
        //   542: astore_3       
        //   543: aload_0        
        //   544: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //   547: istore          18
        //   549: iload           16
        //   551: istore          17
        //   553: aload           13
        //   555: astore_3       
        //   556: aload           10
        //   558: astore_2       
        //   559: aload           11
        //   561: astore          9
        //   563: goto            490
        //   566: iload           16
        //   568: istore          17
        //   570: iload           12
        //   572: istore          18
        //   574: aload           13
        //   576: astore_3       
        //   577: aload           10
        //   579: astore_2       
        //   580: aload_0        
        //   581: astore          9
        //   583: goto            490
        //   586: iload           16
        //   588: istore          17
        //   590: iload           12
        //   592: istore          18
        //   594: aload           13
        //   596: astore_3       
        //   597: aload_0        
        //   598: astore_2       
        //   599: aload           11
        //   601: astore          9
        //   603: goto            490
        //   606: iload           16
        //   608: istore          17
        //   610: iload           12
        //   612: istore          18
        //   614: aload_0        
        //   615: astore_3       
        //   616: aload           10
        //   618: astore_2       
        //   619: aload           11
        //   621: astore          9
        //   623: goto            490
        //   626: aload           4
        //   628: astore_2       
        //   629: aload           4
        //   631: astore_3       
        //   632: aload           11
        //   634: invokestatic    com/navdy/service/library/util/SystemUtils.extractNumberFromPercent:(Ljava/lang/String;)I
        //   637: istore          15
        //   639: aload           4
        //   641: astore_2       
        //   642: aload           4
        //   644: astore_3       
        //   645: new             Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;
        //   648: astore_0       
        //   649: aload           4
        //   651: astore_2       
        //   652: aload           4
        //   654: astore_3       
        //   655: aload_0        
        //   656: iload           16
        //   658: iload           12
        //   660: aload           13
        //   662: aload           10
        //   664: iload           15
        //   666: invokespecial   com/navdy/service/library/util/SystemUtils$ProcessCpuInfo.<init>:(IILjava/lang/String;Ljava/lang/String;I)V
        //   669: aload           4
        //   671: astore_2       
        //   672: aload           4
        //   674: astore_3       
        //   675: aload           8
        //   677: aload_0        
        //   678: invokevirtual   java/util/ArrayList.add:(Ljava/lang/Object;)Z
        //   681: pop            
        //   682: aload           7
        //   684: astore          11
        //   686: aload_1        
        //   687: astore_0       
        //   688: goto            297
        //   691: astore_1       
        //   692: aload_2        
        //   693: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   696: aconst_null    
        //   697: astore_1       
        //   698: aload_1        
        //   699: areturn        
        //   700: aload           4
        //   702: astore_2       
        //   703: aload           4
        //   705: astore_3       
        //   706: new             Lcom/navdy/service/library/util/SystemUtils$CpuInfo;
        //   709: dup            
        //   710: aload_1        
        //   711: invokestatic    com/navdy/service/library/util/SystemUtils.extractNumberFromPercent:(Ljava/lang/String;)I
        //   714: aload           7
        //   716: invokestatic    com/navdy/service/library/util/SystemUtils.extractNumberFromPercent:(Ljava/lang/String;)I
        //   719: aload           8
        //   721: invokespecial   com/navdy/service/library/util/SystemUtils$CpuInfo.<init>:(IILjava/util/ArrayList;)V
        //   724: astore_1       
        //   725: aload           4
        //   727: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   730: goto            698
        //   733: astore_1       
        //   734: aload_3        
        //   735: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   738: aload_1        
        //   739: athrow         
        //   740: astore_0       
        //   741: iload           16
        //   743: istore          17
        //   745: iload           12
        //   747: istore          18
        //   749: aload           13
        //   751: astore_3       
        //   752: aload           10
        //   754: astore_2       
        //   755: aload           11
        //   757: astore          9
        //   759: goto            490
        //   762: astore_0       
        //   763: iload           16
        //   765: istore          17
        //   767: iload           12
        //   769: istore          18
        //   771: aload           13
        //   773: astore_3       
        //   774: aload           10
        //   776: astore_2       
        //   777: aload           11
        //   779: astore          9
        //   781: goto            490
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  8      18     691    698    Ljava/lang/Throwable;
        //  8      18     733    740    Any
        //  22     28     691    698    Ljava/lang/Throwable;
        //  22     28     733    740    Any
        //  32     39     691    698    Ljava/lang/Throwable;
        //  32     39     733    740    Any
        //  45     53     691    698    Ljava/lang/Throwable;
        //  45     53     733    740    Any
        //  59     64     691    698    Ljava/lang/Throwable;
        //  59     64     733    740    Any
        //  70     74     691    698    Ljava/lang/Throwable;
        //  70     74     733    740    Any
        //  80     85     691    698    Ljava/lang/Throwable;
        //  80     85     733    740    Any
        //  91     97     691    698    Ljava/lang/Throwable;
        //  91     97     733    740    Any
        //  111    116    691    698    Ljava/lang/Throwable;
        //  111    116    733    740    Any
        //  122    127    691    698    Ljava/lang/Throwable;
        //  122    127    733    740    Any
        //  133    140    691    698    Ljava/lang/Throwable;
        //  133    140    733    740    Any
        //  151    162    691    698    Ljava/lang/Throwable;
        //  151    162    733    740    Any
        //  174    179    691    698    Ljava/lang/Throwable;
        //  174    179    733    740    Any
        //  185    194    691    698    Ljava/lang/Throwable;
        //  185    194    733    740    Any
        //  206    214    691    698    Ljava/lang/Throwable;
        //  206    214    733    740    Any
        //  220    232    691    698    Ljava/lang/Throwable;
        //  220    232    733    740    Any
        //  238    246    691    698    Ljava/lang/Throwable;
        //  238    246    733    740    Any
        //  261    273    691    698    Ljava/lang/Throwable;
        //  261    273    733    740    Any
        //  282    295    691    698    Ljava/lang/Throwable;
        //  282    295    733    740    Any
        //  336    341    691    698    Ljava/lang/Throwable;
        //  336    341    733    740    Any
        //  347    356    691    698    Ljava/lang/Throwable;
        //  347    356    733    740    Any
        //  371    379    691    698    Ljava/lang/Throwable;
        //  371    379    733    740    Any
        //  385    394    691    698    Ljava/lang/Throwable;
        //  385    394    733    740    Any
        //  517    523    740    762    Ljava/lang/Throwable;
        //  517    523    733    740    Any
        //  543    549    762    784    Ljava/lang/Throwable;
        //  543    549    733    740    Any
        //  632    639    691    698    Ljava/lang/Throwable;
        //  632    639    733    740    Any
        //  645    649    691    698    Ljava/lang/Throwable;
        //  645    649    733    740    Any
        //  655    669    691    698    Ljava/lang/Throwable;
        //  655    669    733    740    Any
        //  675    682    691    698    Ljava/lang/Throwable;
        //  675    682    733    740    Any
        //  706    725    691    698    Ljava/lang/Throwable;
        //  706    725    733    740    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index: 407, Size: 407
        //     at java.util.ArrayList.rangeCheck(Unknown Source)
        //     at java.util.ArrayList.get(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static int getNativeProcessId(final String s) {
        final Closeable closeable = null;
        Closeable inputStream;
        final Closeable closeable2 = inputStream = null;
        Closeable closeable3 = closeable;
        try {
            final Process exec = Runtime.getRuntime().exec("ps");
            inputStream = closeable2;
            closeable3 = closeable;
            exec.waitFor();
            inputStream = closeable2;
            closeable3 = closeable;
            final Closeable closeable4 = closeable3 = (inputStream = exec.getInputStream());
            final String convertInputStreamToString = IOUtils.convertInputStreamToString((InputStream)closeable4, "UTF-8");
            inputStream = closeable4;
            closeable3 = closeable4;
            inputStream = closeable4;
            closeable3 = closeable4;
            inputStream = closeable4;
            closeable3 = closeable4;
            final StringReader stringReader = new StringReader(convertInputStreamToString);
            inputStream = closeable4;
            closeable3 = closeable4;
            final BufferedReader bufferedReader = new BufferedReader(stringReader);
            String line;
            do {
                inputStream = closeable4;
                closeable3 = closeable4;
                line = bufferedReader.readLine();
                if (line == null) {
                    IOUtils.closeStream(closeable4);
                    return -1;
                }
                inputStream = closeable4;
                closeable3 = closeable4;
            } while (!line.contains(s));
            inputStream = closeable4;
            closeable3 = closeable4;
            inputStream = closeable4;
            closeable3 = closeable4;
            final StringTokenizer stringTokenizer = new StringTokenizer(line);
            inputStream = closeable4;
            closeable3 = closeable4;
            stringTokenizer.nextElement();
            inputStream = closeable4;
            closeable3 = closeable4;
            return Integer.parseInt(((String)stringTokenizer.nextElement()).trim());
        }
        catch (Throwable t) {
            IOUtils.closeStream(inputStream);
            return -1;
        }
        finally {
            IOUtils.closeStream(closeable3);
        }
    }
    
    public static int getProcessId(final Context context, final String s) {
        for (final ActivityManager$RunningAppProcessInfo activityManager$RunningAppProcessInfo : ((ActivityManager)context.getSystemService("activity")).getRunningAppProcesses()) {
            if (TextUtils.equals((CharSequence)activityManager$RunningAppProcessInfo.processName, (CharSequence)s)) {
                return activityManager$RunningAppProcessInfo.pid;
            }
        }
        return -1;
    }
    
    public static String getProcessName(final Context context, final int n) {
        final List runningAppProcesses = ((ActivityManager)context.getSystemService("activity")).getRunningAppProcesses();
        if (runningAppProcesses != null) {
            for (final ActivityManager$RunningAppProcessInfo activityManager$RunningAppProcessInfo : runningAppProcesses) {
                if (activityManager$RunningAppProcessInfo != null && activityManager$RunningAppProcessInfo.pid == n) {
                    return activityManager$RunningAppProcessInfo.processName;
                }
            }
            return "";
        }
        return "";
        return "";
    }
    
    public static boolean isConnectedToCellNetwork(final Context context) {
        final boolean b = false;
        final NetworkInfo networkInfo = ((ConnectivityManager)context.getSystemService("connectivity")).getNetworkInfo(0);
        boolean b2 = b;
        if (networkInfo != null) {
            b2 = b;
            if (networkInfo.isConnected()) {
                b2 = true;
            }
        }
        return b2;
    }
    
    public static boolean isConnectedToNetwork(final Context context) {
        final NetworkInfo activeNetworkInfo = ((ConnectivityManager)context.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }
    
    public static boolean isConnectedToWifi(final Context context) {
        boolean b = true;
        final NetworkInfo networkInfo = ((ConnectivityManager)context.getSystemService("connectivity")).getNetworkInfo(1);
        if (networkInfo == null || !networkInfo.isConnected()) {
            b = false;
        }
        return b;
    }
    
    public static class CpuInfo
    {
        private ArrayList<ProcessCpuInfo> plist;
        private int system;
        private int usr;
        
        public CpuInfo(final int usr, final int system, final ArrayList<ProcessCpuInfo> plist) {
            this.usr = usr;
            this.system = system;
            this.plist = plist;
        }
        
        public int getCpuSystem() {
            return this.system;
        }
        
        public int getCpuUser() {
            return this.usr;
        }
        
        public ArrayList<ProcessCpuInfo> getList() {
            return this.plist;
        }
    }
    
    public static class ProcessCpuInfo
    {
        private int cpu;
        private String name;
        private int pid;
        private String thread;
        private int tid;
        
        public ProcessCpuInfo(final int pid, final int tid, final String name, final String thread, final int cpu) {
            this.pid = pid;
            this.tid = tid;
            this.name = name;
            this.thread = thread;
            this.cpu = cpu;
        }
        
        public int getCpu() {
            return this.cpu;
        }
        
        public int getPid() {
            return this.pid;
        }
        
        public String getProcessName() {
            return this.name;
        }
        
        public String getThreadName() {
            return this.thread;
        }
        
        public int getTid() {
            return this.tid;
        }
    }
}
