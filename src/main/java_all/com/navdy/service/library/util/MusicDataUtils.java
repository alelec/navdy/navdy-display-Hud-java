package com.navdy.service.library.util;

import com.navdy.service.library.events.audio.MusicArtworkResponse;
import com.navdy.service.library.events.audio.MusicTrackInfo;

public final class MusicDataUtils
{
    public static final String ALTERNATE_SEPARATOR = "_";
    private static final String SEPARATOR = " // ";
    
    public static String photoIdentifierFromTrackInfo(final MusicTrackInfo musicTrackInfo) {
        String s;
        if (musicTrackInfo == null) {
            s = null;
        }
        else if (musicTrackInfo.index == null) {
            s = songIdentifierFromTrackInfo(musicTrackInfo);
        }
        else {
            s = musicTrackInfo.index.toString();
        }
        return s;
    }
    
    public static String songIdentifierFromArtworkResponse(final MusicArtworkResponse musicArtworkResponse) {
        String replaceAll;
        if (musicArtworkResponse == null) {
            replaceAll = null;
        }
        else {
            replaceAll = (String.valueOf(musicArtworkResponse.name) + " // " + String.valueOf(musicArtworkResponse.album) + " // " + String.valueOf(musicArtworkResponse.author)).replaceAll("[^A-Za-z0-9_]", "_");
        }
        return replaceAll;
    }
    
    public static String songIdentifierFromTrackInfo(final MusicTrackInfo musicTrackInfo) {
        return songIdentifierFromTrackInfo(musicTrackInfo, " // ");
    }
    
    public static String songIdentifierFromTrackInfo(final MusicTrackInfo musicTrackInfo, final String s) {
        String replaceAll;
        if (musicTrackInfo == null) {
            replaceAll = null;
        }
        else {
            replaceAll = (String.valueOf(musicTrackInfo.name) + s + String.valueOf(musicTrackInfo.album) + s + String.valueOf(musicTrackInfo.author)).replaceAll("[^A-Za-z0-9_]", "_");
        }
        return replaceAll;
    }
}
