package com.navdy.service.library.file;

import java.security.NoSuchAlgorithmException;
import com.navdy.service.library.events.file.FileTransferResponse;
import okio.ByteString;
import java.io.EOFException;
import java.io.Closeable;
import com.navdy.service.library.util.IOUtils;
import com.navdy.service.library.events.file.FileTransferError;
import com.navdy.service.library.events.file.FileTransferStatus;
import com.navdy.service.library.events.file.FileTransferData;
import android.content.Context;
import com.navdy.service.library.events.file.FileType;
import java.io.FileInputStream;
import java.io.File;
import java.security.MessageDigest;
import com.navdy.service.library.log.Logger;

public class FileTransferSession
{
    public static final int CHUNK_SIZE = 131072;
    public static final int PULL_CHUNK_SIZE = 16384;
    private static final Logger sLogger;
    private long mAlreadyTransferred;
    MessageDigest mDataCheckSum;
    String mDestinationFolder;
    int mExpectedChunk;
    File mFile;
    String mFileName;
    private FileInputStream mFileReader;
    long mFileSize;
    private FileType mFileType;
    private boolean mIsTestTransfer;
    private long mLastActivity;
    private boolean mPullRequest;
    private byte[] mReadBuffer;
    long mTotalBytesTransferred;
    int mTransferId;
    private boolean mWaitForAcknowledgements;
    long negotiatedOffset;
    
    static {
        sLogger = new Logger(FileTransferSession.class);
    }
    
    public FileTransferSession(final int mTransferId) {
        this.mTransferId = -1;
        this.negotiatedOffset = -1L;
        this.mExpectedChunk = 0;
        this.mLastActivity = 0L;
        this.mPullRequest = false;
        this.mIsTestTransfer = false;
        this.mTransferId = mTransferId;
    }
    
    private FileTransferStatus verifyTestDataChunk(final Context context, final FileTransferData fileTransferData, final FileTransferStatus.Builder builder) {
        synchronized (this) {
            FileTransferStatus fileTransferStatus;
            if (fileTransferData.chunkIndex != this.mExpectedChunk) {
                FileTransferSession.sLogger.e("Illegal chunk,  Expected :" + this.mExpectedChunk + ", received:" + fileTransferData.chunkIndex);
                fileTransferStatus = builder.error(FileTransferError.FILE_TRANSFER_ILLEGAL_CHUNK).build();
            }
            else {
                if (fileTransferData.dataBytes != null && fileTransferData.dataBytes.size() > 0) {
                    this.mTotalBytesTransferred += fileTransferData.dataBytes.size();
                    this.mDataCheckSum.update(fileTransferData.dataBytes.toByteArray());
                }
                builder.totalBytesTransferred(this.mTotalBytesTransferred);
                ++this.mExpectedChunk;
                if (fileTransferData.lastChunk) {
                    if (this.mTotalBytesTransferred != this.mFileSize) {
                        FileTransferSession.sLogger.e("Not the last chunk, data missing. Did not receive full file");
                        fileTransferStatus = builder.error(FileTransferError.FILE_TRANSFER_ILLEGAL_CHUNK).build();
                        return fileTransferStatus;
                    }
                    final String bytesToHexString = IOUtils.bytesToHexString(this.mDataCheckSum.digest());
                    if (!bytesToHexString.equals(fileTransferData.fileCheckSum)) {
                        FileTransferSession.sLogger.e(String.format("Received last chunk but the checksum did not match. expected = \"%s\", actual = \"%s\"", fileTransferData.fileCheckSum, bytesToHexString));
                        fileTransferStatus = builder.error(FileTransferError.FILE_TRANSFER_CHECKSUM_ERROR).build();
                        return fileTransferStatus;
                    }
                    FileTransferSession.sLogger.d("Received last chunk, Transfer complete, final data size " + this.mFileSize);
                    builder.transferComplete(true);
                }
                fileTransferStatus = builder.success(true).build();
            }
            return fileTransferStatus;
        }
    }
    
    public FileTransferStatus appendChunk(final Context p0, final FileTransferData p1) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: dup            
        //     2: astore_3       
        //     3: monitorenter   
        //     4: aload_0        
        //     5: invokestatic    java/lang/System.currentTimeMillis:()J
        //     8: putfield        com/navdy/service/library/file/FileTransferSession.mLastActivity:J
        //    11: aconst_null    
        //    12: astore          4
        //    14: aconst_null    
        //    15: astore          5
        //    17: aconst_null    
        //    18: astore          6
        //    20: new             Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;
        //    23: astore          7
        //    25: aload           7
        //    27: invokespecial   com/navdy/service/library/events/file/FileTransferStatus$Builder.<init>:()V
        //    30: aload           7
        //    32: aload_0        
        //    33: getfield        com/navdy/service/library/file/FileTransferSession.mTransferId:I
        //    36: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
        //    39: invokevirtual   com/navdy/service/library/events/file/FileTransferStatus$Builder.transferId:(Ljava/lang/Integer;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;
        //    42: lconst_0       
        //    43: invokestatic    java/lang/Long.valueOf:(J)Ljava/lang/Long;
        //    46: invokevirtual   com/navdy/service/library/events/file/FileTransferStatus$Builder.totalBytesTransferred:(Ljava/lang/Long;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;
        //    49: iconst_0       
        //    50: invokestatic    java/lang/Boolean.valueOf:(Z)Ljava/lang/Boolean;
        //    53: invokevirtual   com/navdy/service/library/events/file/FileTransferStatus$Builder.transferComplete:(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;
        //    56: iconst_0       
        //    57: invokestatic    java/lang/Boolean.valueOf:(Z)Ljava/lang/Boolean;
        //    60: invokevirtual   com/navdy/service/library/events/file/FileTransferStatus$Builder.success:(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;
        //    63: aload_2        
        //    64: getfield        com/navdy/service/library/events/file/FileTransferData.chunkIndex:Ljava/lang/Integer;
        //    67: invokevirtual   com/navdy/service/library/events/file/FileTransferStatus$Builder.chunkIndex:(Ljava/lang/Integer;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;
        //    70: astore          8
        //    72: aload_0        
        //    73: getfield        com/navdy/service/library/file/FileTransferSession.mIsTestTransfer:Z
        //    76: ifeq            92
        //    79: aload_0        
        //    80: aload_1        
        //    81: aload_2        
        //    82: aload           8
        //    84: invokespecial   com/navdy/service/library/file/FileTransferSession.verifyTestDataChunk:(Landroid/content/Context;Lcom/navdy/service/library/events/file/FileTransferData;Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;)Lcom/navdy/service/library/events/file/FileTransferStatus;
        //    87: astore_1       
        //    88: aload_3        
        //    89: monitorexit    
        //    90: aload_1        
        //    91: areturn        
        //    92: new             Ljava/io/File;
        //    95: astore          9
        //    97: aload           9
        //    99: aload_0        
        //   100: getfield        com/navdy/service/library/file/FileTransferSession.mDestinationFolder:Ljava/lang/String;
        //   103: aload_0        
        //   104: getfield        com/navdy/service/library/file/FileTransferSession.mFileName:Ljava/lang/String;
        //   107: invokespecial   java/io/File.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   110: aload           9
        //   112: invokevirtual   java/io/File.exists:()Z
        //   115: ifne            141
        //   118: getstatic       com/navdy/service/library/file/FileTransferSession.sLogger:Lcom/navdy/service/library/log/Logger;
        //   121: ldc             "Session not initiated "
        //   123: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;)V
        //   126: aload           8
        //   128: getstatic       com/navdy/service/library/events/file/FileTransferError.FILE_TRANSFER_NOT_INITIATED:Lcom/navdy/service/library/events/file/FileTransferError;
        //   131: invokevirtual   com/navdy/service/library/events/file/FileTransferStatus$Builder.error:(Lcom/navdy/service/library/events/file/FileTransferError;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;
        //   134: invokevirtual   com/navdy/service/library/events/file/FileTransferStatus$Builder.build:()Lcom/navdy/service/library/events/file/FileTransferStatus;
        //   137: astore_1       
        //   138: goto            88
        //   141: aload_2        
        //   142: getfield        com/navdy/service/library/events/file/FileTransferData.chunkIndex:Ljava/lang/Integer;
        //   145: invokevirtual   java/lang/Integer.intValue:()I
        //   148: aload_0        
        //   149: getfield        com/navdy/service/library/file/FileTransferSession.mExpectedChunk:I
        //   152: if_icmpeq       217
        //   155: getstatic       com/navdy/service/library/file/FileTransferSession.sLogger:Lcom/navdy/service/library/log/Logger;
        //   158: astore_1       
        //   159: new             Ljava/lang/StringBuilder;
        //   162: astore          7
        //   164: aload           7
        //   166: invokespecial   java/lang/StringBuilder.<init>:()V
        //   169: aload_1        
        //   170: aload           7
        //   172: ldc             "Illegal chunk,  Expected :"
        //   174: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   177: aload_0        
        //   178: getfield        com/navdy/service/library/file/FileTransferSession.mExpectedChunk:I
        //   181: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   184: ldc             ", received:"
        //   186: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   189: aload_2        
        //   190: getfield        com/navdy/service/library/events/file/FileTransferData.chunkIndex:Ljava/lang/Integer;
        //   193: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   196: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   199: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;)V
        //   202: aload           8
        //   204: getstatic       com/navdy/service/library/events/file/FileTransferError.FILE_TRANSFER_ILLEGAL_CHUNK:Lcom/navdy/service/library/events/file/FileTransferError;
        //   207: invokevirtual   com/navdy/service/library/events/file/FileTransferStatus$Builder.error:(Lcom/navdy/service/library/events/file/FileTransferError;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;
        //   210: invokevirtual   com/navdy/service/library/events/file/FileTransferStatus$Builder.build:()Lcom/navdy/service/library/events/file/FileTransferStatus;
        //   213: astore_1       
        //   214: goto            88
        //   217: aload           4
        //   219: astore          10
        //   221: aload           5
        //   223: astore_1       
        //   224: getstatic       com/navdy/service/library/file/FileTransferSession.sLogger:Lcom/navdy/service/library/log/Logger;
        //   227: ldc             "Starting to write chunk %d into file %s from folder %s"
        //   229: iconst_3       
        //   230: anewarray       Ljava/lang/Object;
        //   233: dup            
        //   234: iconst_0       
        //   235: aload_2        
        //   236: getfield        com/navdy/service/library/events/file/FileTransferData.chunkIndex:Ljava/lang/Integer;
        //   239: aastore        
        //   240: dup            
        //   241: iconst_1       
        //   242: aload           9
        //   244: invokevirtual   java/io/File.getName:()Ljava/lang/String;
        //   247: aastore        
        //   248: dup            
        //   249: iconst_2       
        //   250: aload           9
        //   252: invokevirtual   java/io/File.getAbsolutePath:()Ljava/lang/String;
        //   255: aastore        
        //   256: invokestatic    java/lang/String.format:(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
        //   259: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   262: aload           6
        //   264: astore          7
        //   266: aload           4
        //   268: astore          10
        //   270: aload           5
        //   272: astore_1       
        //   273: aload_2        
        //   274: getfield        com/navdy/service/library/events/file/FileTransferData.dataBytes:Lokio/ByteString;
        //   277: ifnull          340
        //   280: aload           6
        //   282: astore          7
        //   284: aload           4
        //   286: astore          10
        //   288: aload           5
        //   290: astore_1       
        //   291: aload_2        
        //   292: getfield        com/navdy/service/library/events/file/FileTransferData.dataBytes:Lokio/ByteString;
        //   295: invokevirtual   okio/ByteString.size:()I
        //   298: ifle            340
        //   301: aload           4
        //   303: astore          10
        //   305: aload           5
        //   307: astore_1       
        //   308: new             Ljava/io/FileOutputStream;
        //   311: astore          7
        //   313: aload           4
        //   315: astore          10
        //   317: aload           5
        //   319: astore_1       
        //   320: aload           7
        //   322: aload           9
        //   324: iconst_1       
        //   325: invokespecial   java/io/FileOutputStream.<init>:(Ljava/io/File;Z)V
        //   328: aload           7
        //   330: aload_2        
        //   331: getfield        com/navdy/service/library/events/file/FileTransferData.dataBytes:Lokio/ByteString;
        //   334: invokevirtual   okio/ByteString.toByteArray:()[B
        //   337: invokevirtual   java/io/FileOutputStream.write:([B)V
        //   340: aload           7
        //   342: astore          10
        //   344: aload           7
        //   346: astore_1       
        //   347: aload           8
        //   349: aload           9
        //   351: invokevirtual   java/io/File.length:()J
        //   354: invokestatic    java/lang/Long.valueOf:(J)Ljava/lang/Long;
        //   357: invokevirtual   com/navdy/service/library/events/file/FileTransferStatus$Builder.totalBytesTransferred:(Ljava/lang/Long;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;
        //   360: pop            
        //   361: aload           7
        //   363: astore          10
        //   365: aload           7
        //   367: astore_1       
        //   368: aload_0        
        //   369: aload_0        
        //   370: getfield        com/navdy/service/library/file/FileTransferSession.mExpectedChunk:I
        //   373: iconst_1       
        //   374: iadd           
        //   375: putfield        com/navdy/service/library/file/FileTransferSession.mExpectedChunk:I
        //   378: aload           7
        //   380: astore          10
        //   382: aload           7
        //   384: astore_1       
        //   385: aload_2        
        //   386: getfield        com/navdy/service/library/events/file/FileTransferData.lastChunk:Ljava/lang/Boolean;
        //   389: invokevirtual   java/lang/Boolean.booleanValue:()Z
        //   392: ifeq            584
        //   395: aload           7
        //   397: astore          10
        //   399: aload           7
        //   401: astore_1       
        //   402: aload           9
        //   404: invokevirtual   java/io/File.length:()J
        //   407: aload_0        
        //   408: getfield        com/navdy/service/library/file/FileTransferSession.mFileSize:J
        //   411: lcmp           
        //   412: ifeq            469
        //   415: aload           7
        //   417: astore          10
        //   419: aload           7
        //   421: astore_1       
        //   422: getstatic       com/navdy/service/library/file/FileTransferSession.sLogger:Lcom/navdy/service/library/log/Logger;
        //   425: ldc             "Not the last chunk, data missing. Did not receive full file"
        //   427: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;)V
        //   430: aload           7
        //   432: astore          10
        //   434: aload           7
        //   436: astore_1       
        //   437: aload           8
        //   439: getstatic       com/navdy/service/library/events/file/FileTransferError.FILE_TRANSFER_ILLEGAL_CHUNK:Lcom/navdy/service/library/events/file/FileTransferError;
        //   442: invokevirtual   com/navdy/service/library/events/file/FileTransferStatus$Builder.error:(Lcom/navdy/service/library/events/file/FileTransferError;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;
        //   445: invokevirtual   com/navdy/service/library/events/file/FileTransferStatus$Builder.build:()Lcom/navdy/service/library/events/file/FileTransferStatus;
        //   448: astore_2       
        //   449: aload_2        
        //   450: astore_1       
        //   451: aload           7
        //   453: invokestatic    com/navdy/service/library/util/IOUtils.fileSync:(Ljava/io/FileOutputStream;)V
        //   456: aload           7
        //   458: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   461: goto            88
        //   464: astore_1       
        //   465: aload_3        
        //   466: monitorexit    
        //   467: aload_1        
        //   468: athrow         
        //   469: aload           7
        //   471: astore          10
        //   473: aload           7
        //   475: astore_1       
        //   476: aload           9
        //   478: invokestatic    com/navdy/service/library/util/IOUtils.hashForFile:(Ljava/io/File;)Ljava/lang/String;
        //   481: astore          4
        //   483: aload           7
        //   485: astore          10
        //   487: aload           7
        //   489: astore_1       
        //   490: aload           4
        //   492: aload_2        
        //   493: getfield        com/navdy/service/library/events/file/FileTransferData.fileCheckSum:Ljava/lang/String;
        //   496: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   499: ifeq            610
        //   502: aload           7
        //   504: astore          10
        //   506: aload           7
        //   508: astore_1       
        //   509: getstatic       com/navdy/service/library/file/FileTransferSession.sLogger:Lcom/navdy/service/library/log/Logger;
        //   512: astore_2       
        //   513: aload           7
        //   515: astore          10
        //   517: aload           7
        //   519: astore_1       
        //   520: new             Ljava/lang/StringBuilder;
        //   523: astore          4
        //   525: aload           7
        //   527: astore          10
        //   529: aload           7
        //   531: astore_1       
        //   532: aload           4
        //   534: invokespecial   java/lang/StringBuilder.<init>:()V
        //   537: aload           7
        //   539: astore          10
        //   541: aload           7
        //   543: astore_1       
        //   544: aload_2        
        //   545: aload           4
        //   547: ldc_w           "Written last chunk to the file , Transfer complete, final file size "
        //   550: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   553: aload           9
        //   555: invokevirtual   java/io/File.length:()J
        //   558: invokevirtual   java/lang/StringBuilder.append:(J)Ljava/lang/StringBuilder;
        //   561: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   564: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //   567: aload           7
        //   569: astore          10
        //   571: aload           7
        //   573: astore_1       
        //   574: aload           8
        //   576: iconst_1       
        //   577: invokestatic    java/lang/Boolean.valueOf:(Z)Ljava/lang/Boolean;
        //   580: invokevirtual   com/navdy/service/library/events/file/FileTransferStatus$Builder.transferComplete:(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;
        //   583: pop            
        //   584: aload           7
        //   586: invokestatic    com/navdy/service/library/util/IOUtils.fileSync:(Ljava/io/FileOutputStream;)V
        //   589: aload           7
        //   591: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   594: aload           8
        //   596: iconst_1       
        //   597: invokestatic    java/lang/Boolean.valueOf:(Z)Ljava/lang/Boolean;
        //   600: invokevirtual   com/navdy/service/library/events/file/FileTransferStatus$Builder.success:(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;
        //   603: invokevirtual   com/navdy/service/library/events/file/FileTransferStatus$Builder.build:()Lcom/navdy/service/library/events/file/FileTransferStatus;
        //   606: astore_1       
        //   607: goto            88
        //   610: aload           7
        //   612: astore          10
        //   614: aload           7
        //   616: astore_1       
        //   617: getstatic       com/navdy/service/library/file/FileTransferSession.sLogger:Lcom/navdy/service/library/log/Logger;
        //   620: ldc             "Received last chunk but the checksum did not match. expected = \"%s\", actual = \"%s\""
        //   622: iconst_2       
        //   623: anewarray       Ljava/lang/Object;
        //   626: dup            
        //   627: iconst_0       
        //   628: aload_2        
        //   629: getfield        com/navdy/service/library/events/file/FileTransferData.fileCheckSum:Ljava/lang/String;
        //   632: aastore        
        //   633: dup            
        //   634: iconst_1       
        //   635: aload           4
        //   637: aastore        
        //   638: invokestatic    java/lang/String.format:(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
        //   641: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;)V
        //   644: aload           7
        //   646: astore          10
        //   648: aload           7
        //   650: astore_1       
        //   651: aload           8
        //   653: getstatic       com/navdy/service/library/events/file/FileTransferError.FILE_TRANSFER_CHECKSUM_ERROR:Lcom/navdy/service/library/events/file/FileTransferError;
        //   656: invokevirtual   com/navdy/service/library/events/file/FileTransferStatus$Builder.error:(Lcom/navdy/service/library/events/file/FileTransferError;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;
        //   659: invokevirtual   com/navdy/service/library/events/file/FileTransferStatus$Builder.build:()Lcom/navdy/service/library/events/file/FileTransferStatus;
        //   662: astore_2       
        //   663: aload_2        
        //   664: astore_1       
        //   665: aload           7
        //   667: invokestatic    com/navdy/service/library/util/IOUtils.fileSync:(Ljava/io/FileOutputStream;)V
        //   670: aload           7
        //   672: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   675: goto            88
        //   678: astore_2       
        //   679: aload           10
        //   681: astore_1       
        //   682: getstatic       com/navdy/service/library/file/FileTransferSession.sLogger:Lcom/navdy/service/library/log/Logger;
        //   685: ldc_w           "Error writing the chunk to the file"
        //   688: aload_2        
        //   689: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   692: aload           10
        //   694: astore_1       
        //   695: aload           8
        //   697: getstatic       com/navdy/service/library/events/file/FileTransferError.FILE_TRANSFER_IO_ERROR:Lcom/navdy/service/library/events/file/FileTransferError;
        //   700: invokevirtual   com/navdy/service/library/events/file/FileTransferStatus$Builder.error:(Lcom/navdy/service/library/events/file/FileTransferError;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;
        //   703: invokevirtual   com/navdy/service/library/events/file/FileTransferStatus$Builder.build:()Lcom/navdy/service/library/events/file/FileTransferStatus;
        //   706: astore_2       
        //   707: aload_2        
        //   708: astore_1       
        //   709: aload           10
        //   711: invokestatic    com/navdy/service/library/util/IOUtils.fileSync:(Ljava/io/FileOutputStream;)V
        //   714: aload           10
        //   716: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   719: goto            88
        //   722: astore          7
        //   724: aload_1        
        //   725: astore_2       
        //   726: aload_2        
        //   727: invokestatic    com/navdy/service/library/util/IOUtils.fileSync:(Ljava/io/FileOutputStream;)V
        //   730: aload_2        
        //   731: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   734: aload           7
        //   736: athrow         
        //   737: astore_1       
        //   738: aload           7
        //   740: astore_2       
        //   741: aload_1        
        //   742: astore          7
        //   744: goto            726
        //   747: astore_2       
        //   748: aload           7
        //   750: astore          10
        //   752: goto            679
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  4      11     464    469    Any
        //  20     88     464    469    Any
        //  92     138    464    469    Any
        //  141    214    464    469    Any
        //  224    262    678    679    Ljava/io/IOException;
        //  224    262    722    726    Any
        //  273    280    678    679    Ljava/io/IOException;
        //  273    280    722    726    Any
        //  291    301    678    679    Ljava/io/IOException;
        //  291    301    722    726    Any
        //  308    313    678    679    Ljava/io/IOException;
        //  308    313    722    726    Any
        //  320    328    678    679    Ljava/io/IOException;
        //  320    328    722    726    Any
        //  328    340    747    755    Ljava/io/IOException;
        //  328    340    737    747    Any
        //  347    361    678    679    Ljava/io/IOException;
        //  347    361    722    726    Any
        //  368    378    678    679    Ljava/io/IOException;
        //  368    378    722    726    Any
        //  385    395    678    679    Ljava/io/IOException;
        //  385    395    722    726    Any
        //  402    415    678    679    Ljava/io/IOException;
        //  402    415    722    726    Any
        //  422    430    678    679    Ljava/io/IOException;
        //  422    430    722    726    Any
        //  437    449    678    679    Ljava/io/IOException;
        //  437    449    722    726    Any
        //  451    461    464    469    Any
        //  476    483    678    679    Ljava/io/IOException;
        //  476    483    722    726    Any
        //  490    502    678    679    Ljava/io/IOException;
        //  490    502    722    726    Any
        //  509    513    678    679    Ljava/io/IOException;
        //  509    513    722    726    Any
        //  520    525    678    679    Ljava/io/IOException;
        //  520    525    722    726    Any
        //  532    537    678    679    Ljava/io/IOException;
        //  532    537    722    726    Any
        //  544    567    678    679    Ljava/io/IOException;
        //  544    567    722    726    Any
        //  574    584    678    679    Ljava/io/IOException;
        //  574    584    722    726    Any
        //  584    607    464    469    Any
        //  617    644    678    679    Ljava/io/IOException;
        //  617    644    722    726    Any
        //  651    663    678    679    Ljava/io/IOException;
        //  651    663    722    726    Any
        //  665    675    464    469    Any
        //  682    692    722    726    Any
        //  695    707    722    726    Any
        //  709    719    464    469    Any
        //  726    737    464    469    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index: 371, Size: 371
        //     at java.util.ArrayList.rangeCheck(Unknown Source)
        //     at java.util.ArrayList.get(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public void endSession(final Context context, final boolean b) {
        synchronized (this) {
            IOUtils.closeStream(this.mFileReader);
            if (this.mPullRequest && b) {
                IOUtils.deleteFile(context, this.mFile.getAbsolutePath());
            }
            this.mReadBuffer = null;
        }
    }
    
    public FileType getFileType() {
        return this.mFileType;
    }
    
    public long getLastActivity() {
        synchronized (this) {
            return this.mLastActivity;
        }
    }
    
    public FileTransferData getNextChunk() throws Throwable {
        // monitorenter(this)
        Label_0076: {
            try {
                final int n = this.mExpectedChunk++;
                final long n2 = this.mFileSize - this.mAlreadyTransferred;
                if (n2 < 0L) {
                    throw new EOFException("End of file, no more chunk");
                }
                break Label_0076;
            }
            catch (Throwable t) {
                try {
                    FileTransferSession.sLogger.e("Exception while reading the next chunk ", t);
                    IOUtils.closeStream(this.mFileReader);
                    return null;
                    int n3 = 16384;
                    boolean b = false;
                    // iftrue(Label_0100:, n2 >= (long)16384)
                    Block_6: {
                        break Block_6;
                        Label_0125: {
                            this.mAlreadyTransferred += this.mFileReader.read(this.mReadBuffer);
                        }
                        final int n;
                        build = new FileTransferData.Builder().transferId(this.mTransferId).chunkIndex(n).dataBytes(ByteString.of(this.mReadBuffer)).lastChunk(b).build();
                        return build;
                    }
                    final long n2;
                    n3 = (int)n2;
                    b = true;
                    Label_0100: {
                        this.mReadBuffer = new byte[n3];
                    }
                }
                // iftrue(Label_0125:, this.mReadBuffer != null && this.mReadBuffer.length == n3)
                finally {
                }
                // monitorexit(this)
            }
        }
    }
    
    public boolean handleFileTransferStatus(final FileTransferStatus fileTransferStatus) {
        // monitorenter(this)
        Label_0063: {
            if (fileTransferStatus == null) {
                break Label_0063;
            }
            try {
                boolean b;
                if (fileTransferStatus.success && fileTransferStatus.chunkIndex == this.mExpectedChunk - 1 && fileTransferStatus.totalBytesTransferred == this.mAlreadyTransferred) {
                    b = true;
                }
                else {
                    b = false;
                }
                return b;
            }
            finally {
            }
            // monitorexit(this)
        }
    }
    
    public FileTransferResponse initFileTransfer(final Context p0, final FileType p1, final String p2, final String p3, final long p4, final long p5, final String p6, final boolean p7) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: dup            
        //     2: astore          11
        //     4: monitorenter   
        //     5: aload_0        
        //     6: dup            
        //     7: astore          11
        //     9: monitorenter   
        //    10: aload_0        
        //    11: invokestatic    java/lang/System.currentTimeMillis:()J
        //    14: putfield        com/navdy/service/library/file/FileTransferSession.mLastActivity:J
        //    17: aload           11
        //    19: monitorexit    
        //    20: aload_0        
        //    21: aload_3        
        //    22: putfield        com/navdy/service/library/file/FileTransferSession.mDestinationFolder:Ljava/lang/String;
        //    25: aload_0        
        //    26: aload           4
        //    28: putfield        com/navdy/service/library/file/FileTransferSession.mFileName:Ljava/lang/String;
        //    31: aload_0        
        //    32: lload           5
        //    34: putfield        com/navdy/service/library/file/FileTransferSession.mFileSize:J
        //    37: aload_0        
        //    38: aload_2        
        //    39: putfield        com/navdy/service/library/file/FileTransferSession.mFileType:Lcom/navdy/service/library/events/file/FileType;
        //    42: aload_0        
        //    43: dup            
        //    44: astore          11
        //    46: monitorenter   
        //    47: aload_0        
        //    48: iconst_0       
        //    49: putfield        com/navdy/service/library/file/FileTransferSession.mExpectedChunk:I
        //    52: aload           11
        //    54: monitorexit    
        //    55: new             Ljava/io/File;
        //    58: astore_1       
        //    59: aload_1        
        //    60: aload_3        
        //    61: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //    64: aconst_null    
        //    65: astore          12
        //    67: new             Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;
        //    70: astore          13
        //    72: aload           13
        //    74: invokespecial   com/navdy/service/library/events/file/FileTransferResponse$Builder.<init>:()V
        //    77: aload           13
        //    79: aload_2        
        //    80: invokevirtual   com/navdy/service/library/events/file/FileTransferResponse$Builder.fileType:(Lcom/navdy/service/library/events/file/FileType;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;
        //    83: aload           4
        //    85: invokevirtual   com/navdy/service/library/events/file/FileTransferResponse$Builder.destinationFileName:(Ljava/lang/String;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;
        //    88: pop            
        //    89: aload           13
        //    91: aload_0        
        //    92: getfield        com/navdy/service/library/file/FileTransferSession.mTransferId:I
        //    95: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
        //    98: invokevirtual   com/navdy/service/library/events/file/FileTransferResponse$Builder.transferId:(Ljava/lang/Integer;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;
        //   101: pop            
        //   102: new             Ljava/io/File;
        //   105: astore          14
        //   107: aload           14
        //   109: aload_1        
        //   110: aload           4
        //   112: invokespecial   java/io/File.<init>:(Ljava/io/File;Ljava/lang/String;)V
        //   115: aload           14
        //   117: invokevirtual   java/io/File.exists:()Z
        //   120: ifeq            746
        //   123: aload           14
        //   125: invokevirtual   java/io/File.canWrite:()Z
        //   128: ifne            173
        //   131: aload           13
        //   133: getstatic       com/navdy/service/library/events/file/FileTransferError.FILE_TRANSFER_PERMISSION_DENIED:Lcom/navdy/service/library/events/file/FileTransferError;
        //   136: invokevirtual   com/navdy/service/library/events/file/FileTransferResponse$Builder.error:(Lcom/navdy/service/library/events/file/FileTransferError;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;
        //   139: iconst_0       
        //   140: invokestatic    java/lang/Boolean.valueOf:(Z)Ljava/lang/Boolean;
        //   143: invokevirtual   com/navdy/service/library/events/file/FileTransferResponse$Builder.success:(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;
        //   146: invokevirtual   com/navdy/service/library/events/file/FileTransferResponse$Builder.build:()Lcom/navdy/service/library/events/file/FileTransferResponse;
        //   149: astore_1       
        //   150: aload           11
        //   152: monitorexit    
        //   153: aload_1        
        //   154: areturn        
        //   155: astore_1       
        //   156: aload           11
        //   158: monitorexit    
        //   159: aload_1        
        //   160: athrow         
        //   161: astore_1       
        //   162: aload           11
        //   164: monitorexit    
        //   165: aload_1        
        //   166: athrow         
        //   167: astore_1       
        //   168: aload           11
        //   170: monitorexit    
        //   171: aload_1        
        //   172: athrow         
        //   173: aload           14
        //   175: invokevirtual   java/io/File.length:()J
        //   178: lstore          15
        //   180: getstatic       com/navdy/service/library/file/FileTransferSession.sLogger:Lcom/navdy/service/library/log/Logger;
        //   183: astore_2       
        //   184: new             Ljava/lang/StringBuilder;
        //   187: astore_1       
        //   188: aload_1        
        //   189: invokespecial   java/lang/StringBuilder.<init>:()V
        //   192: aload_2        
        //   193: aload_1        
        //   194: ldc_w           "Requested offset :"
        //   197: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   200: lload           7
        //   202: invokevirtual   java/lang/StringBuilder.append:(J)Ljava/lang/StringBuilder;
        //   205: ldc_w           " , Already existing file size:"
        //   208: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   211: lload           15
        //   213: invokevirtual   java/lang/StringBuilder.append:(J)Ljava/lang/StringBuilder;
        //   216: ldc_w           ", override :"
        //   219: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   222: iload           10
        //   224: invokevirtual   java/lang/StringBuilder.append:(Z)Ljava/lang/StringBuilder;
        //   227: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   230: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //   233: lload           7
        //   235: lload           15
        //   237: lcmp           
        //   238: ifgt            584
        //   241: iload           10
        //   243: ifeq            577
        //   246: lload           7
        //   248: lstore          17
        //   250: lload           17
        //   252: lstore          19
        //   254: lload           17
        //   256: lconst_0       
        //   257: lcmp           
        //   258: ifle            292
        //   261: lload           17
        //   263: lstore          19
        //   265: aload           14
        //   267: lload           7
        //   269: invokestatic    com/navdy/service/library/util/IOUtils.hashForFile:(Ljava/io/File;J)Ljava/lang/String;
        //   272: aload           9
        //   274: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   277: ifne            292
        //   280: getstatic       com/navdy/service/library/file/FileTransferSession.sLogger:Lcom/navdy/service/library/log/Logger;
        //   283: ldc_w           "Checksum mismatch, local file does not match remote file"
        //   286: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;)V
        //   289: lconst_0       
        //   290: lstore          19
        //   292: getstatic       com/navdy/service/library/file/FileTransferSession.sLogger:Lcom/navdy/service/library/log/Logger;
        //   295: astore_1       
        //   296: new             Ljava/lang/StringBuilder;
        //   299: astore_2       
        //   300: aload_2        
        //   301: invokespecial   java/lang/StringBuilder.<init>:()V
        //   304: aload_1        
        //   305: aload_2        
        //   306: ldc_w           "New proposed offset :"
        //   309: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   312: lload           19
        //   314: invokevirtual   java/lang/StringBuilder.append:(J)Ljava/lang/StringBuilder;
        //   317: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   320: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //   323: lload           19
        //   325: lstore          17
        //   327: lload           15
        //   329: lload           19
        //   331: lsub           
        //   332: lconst_0       
        //   333: lcmp           
        //   334: ifle            466
        //   337: aconst_null    
        //   338: astore          21
        //   340: aconst_null    
        //   341: astore          22
        //   343: aconst_null    
        //   344: astore          23
        //   346: aconst_null    
        //   347: astore          24
        //   349: aconst_null    
        //   350: astore          25
        //   352: aconst_null    
        //   353: astore          26
        //   355: aload           23
        //   357: astore_1       
        //   358: aload           21
        //   360: astore_2       
        //   361: new             Ljava/io/FileOutputStream;
        //   364: astore          4
        //   366: aload           23
        //   368: astore_1       
        //   369: aload           21
        //   371: astore_2       
        //   372: aload           4
        //   374: aload           14
        //   376: iconst_1       
        //   377: invokespecial   java/io/FileOutputStream.<init>:(Ljava/io/File;Z)V
        //   380: aload           24
        //   382: astore_1       
        //   383: aload           25
        //   385: astore_2       
        //   386: aload           4
        //   388: invokevirtual   java/io/FileOutputStream.getChannel:()Ljava/nio/channels/FileChannel;
        //   391: astore          26
        //   393: aload           26
        //   395: astore_1       
        //   396: aload           26
        //   398: astore_2       
        //   399: aload           26
        //   401: lload           19
        //   403: invokevirtual   java/nio/channels/FileChannel.truncate:(J)Ljava/nio/channels/FileChannel;
        //   406: pop            
        //   407: aload           4
        //   409: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   412: aload           26
        //   414: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   417: getstatic       com/navdy/service/library/file/FileTransferSession.sLogger:Lcom/navdy/service/library/log/Logger;
        //   420: astore_2       
        //   421: new             Ljava/lang/StringBuilder;
        //   424: astore_1       
        //   425: aload_1        
        //   426: invokespecial   java/lang/StringBuilder.<init>:()V
        //   429: aload_2        
        //   430: aload_1        
        //   431: ldc_w           "Truncated the file , previous size :"
        //   434: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   437: lload           15
        //   439: invokevirtual   java/lang/StringBuilder.append:(J)Ljava/lang/StringBuilder;
        //   442: ldc_w           ", new size :"
        //   445: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   448: aload           14
        //   450: invokevirtual   java/io/File.length:()J
        //   453: invokevirtual   java/lang/StringBuilder.append:(J)Ljava/lang/StringBuilder;
        //   456: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   459: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;)V
        //   462: lload           19
        //   464: lstore          17
        //   466: aload_0        
        //   467: lload           17
        //   469: putfield        com/navdy/service/library/file/FileTransferSession.negotiatedOffset:J
        //   472: getstatic       com/navdy/service/library/file/FileTransferSession.sLogger:Lcom/navdy/service/library/log/Logger;
        //   475: astore_2       
        //   476: new             Ljava/lang/StringBuilder;
        //   479: astore_1       
        //   480: aload_1        
        //   481: invokespecial   java/lang/StringBuilder.<init>:()V
        //   484: aload_2        
        //   485: aload_1        
        //   486: ldc_w           "Final offset proposed "
        //   489: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   492: aload_0        
        //   493: getfield        com/navdy/service/library/file/FileTransferSession.negotiatedOffset:J
        //   496: invokevirtual   java/lang/StringBuilder.append:(J)Ljava/lang/StringBuilder;
        //   499: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   502: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //   505: lload           5
        //   507: aload_0        
        //   508: getfield        com/navdy/service/library/file/FileTransferSession.negotiatedOffset:J
        //   511: lsub           
        //   512: lstore          5
        //   514: aload_3        
        //   515: invokestatic    com/navdy/service/library/util/IOUtils.getFreeSpace:(Ljava/lang/String;)J
        //   518: lload           5
        //   520: lcmp           
        //   521: ifgt            797
        //   524: getstatic       com/navdy/service/library/file/FileTransferSession.sLogger:Lcom/navdy/service/library/log/Logger;
        //   527: astore_1       
        //   528: new             Ljava/lang/StringBuilder;
        //   531: astore_2       
        //   532: aload_2        
        //   533: invokespecial   java/lang/StringBuilder.<init>:()V
        //   536: aload_1        
        //   537: aload_2        
        //   538: ldc_w           "Not enough space to write the file, space required :"
        //   541: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   544: lload           5
        //   546: invokevirtual   java/lang/StringBuilder.append:(J)Ljava/lang/StringBuilder;
        //   549: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   552: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //   555: aload           13
        //   557: getstatic       com/navdy/service/library/events/file/FileTransferError.FILE_TRANSFER_IO_ERROR:Lcom/navdy/service/library/events/file/FileTransferError;
        //   560: invokevirtual   com/navdy/service/library/events/file/FileTransferResponse$Builder.error:(Lcom/navdy/service/library/events/file/FileTransferError;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;
        //   563: iconst_0       
        //   564: invokestatic    java/lang/Boolean.valueOf:(Z)Ljava/lang/Boolean;
        //   567: invokevirtual   com/navdy/service/library/events/file/FileTransferResponse$Builder.success:(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;
        //   570: invokevirtual   com/navdy/service/library/events/file/FileTransferResponse$Builder.build:()Lcom/navdy/service/library/events/file/FileTransferResponse;
        //   573: astore_1       
        //   574: goto            150
        //   577: lload           15
        //   579: lstore          17
        //   581: goto            250
        //   584: lload           15
        //   586: lstore          19
        //   588: goto            292
        //   591: astore          9
        //   593: aload           22
        //   595: astore          4
        //   597: aload           26
        //   599: astore_3       
        //   600: aload_3        
        //   601: astore_1       
        //   602: aload           4
        //   604: astore_2       
        //   605: getstatic       com/navdy/service/library/file/FileTransferSession.sLogger:Lcom/navdy/service/library/log/Logger;
        //   608: astore          26
        //   610: aload_3        
        //   611: astore_1       
        //   612: aload           4
        //   614: astore_2       
        //   615: new             Ljava/lang/StringBuilder;
        //   618: astore          12
        //   620: aload_3        
        //   621: astore_1       
        //   622: aload           4
        //   624: astore_2       
        //   625: aload           12
        //   627: invokespecial   java/lang/StringBuilder.<init>:()V
        //   630: aload_3        
        //   631: astore_1       
        //   632: aload           4
        //   634: astore_2       
        //   635: aload           26
        //   637: aload           12
        //   639: ldc_w           "Exception truncating the file to offset "
        //   642: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   645: lload           19
        //   647: invokevirtual   java/lang/StringBuilder.append:(J)Ljava/lang/StringBuilder;
        //   650: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   653: aload           9
        //   655: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   658: aload_3        
        //   659: astore_1       
        //   660: aload           4
        //   662: astore_2       
        //   663: aload           13
        //   665: getstatic       com/navdy/service/library/events/file/FileTransferError.FILE_TRANSFER_IO_ERROR:Lcom/navdy/service/library/events/file/FileTransferError;
        //   668: invokevirtual   com/navdy/service/library/events/file/FileTransferResponse$Builder.error:(Lcom/navdy/service/library/events/file/FileTransferError;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;
        //   671: iconst_0       
        //   672: invokestatic    java/lang/Boolean.valueOf:(Z)Ljava/lang/Boolean;
        //   675: invokevirtual   com/navdy/service/library/events/file/FileTransferResponse$Builder.success:(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;
        //   678: invokevirtual   com/navdy/service/library/events/file/FileTransferResponse$Builder.build:()Lcom/navdy/service/library/events/file/FileTransferResponse;
        //   681: astore          9
        //   683: aload           9
        //   685: astore_1       
        //   686: aload           4
        //   688: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   691: aload_3        
        //   692: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   695: goto            150
        //   698: astore_1       
        //   699: getstatic       com/navdy/service/library/file/FileTransferSession.sLogger:Lcom/navdy/service/library/log/Logger;
        //   702: ldc_w           "Error processing FileTransferRequest "
        //   705: aload_1        
        //   706: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   709: aload           13
        //   711: getstatic       com/navdy/service/library/events/file/FileTransferError.FILE_TRANSFER_IO_ERROR:Lcom/navdy/service/library/events/file/FileTransferError;
        //   714: invokevirtual   com/navdy/service/library/events/file/FileTransferResponse$Builder.error:(Lcom/navdy/service/library/events/file/FileTransferError;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;
        //   717: iconst_0       
        //   718: invokestatic    java/lang/Boolean.valueOf:(Z)Ljava/lang/Boolean;
        //   721: invokevirtual   com/navdy/service/library/events/file/FileTransferResponse$Builder.success:(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;
        //   724: invokevirtual   com/navdy/service/library/events/file/FileTransferResponse$Builder.build:()Lcom/navdy/service/library/events/file/FileTransferResponse;
        //   727: astore_1       
        //   728: goto            150
        //   731: astore_3       
        //   732: aload_2        
        //   733: astore          4
        //   735: aload           4
        //   737: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   740: aload_1        
        //   741: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   744: aload_3        
        //   745: athrow         
        //   746: getstatic       com/navdy/service/library/file/FileTransferSession.sLogger:Lcom/navdy/service/library/log/Logger;
        //   749: ldc_w           "File does not exists, creating new file"
        //   752: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //   755: lconst_0       
        //   756: lstore          17
        //   758: aload           14
        //   760: invokevirtual   java/io/File.createNewFile:()Z
        //   763: ifne            466
        //   766: getstatic       com/navdy/service/library/file/FileTransferSession.sLogger:Lcom/navdy/service/library/log/Logger;
        //   769: ldc_w           "Permission denied"
        //   772: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //   775: aload           13
        //   777: getstatic       com/navdy/service/library/events/file/FileTransferError.FILE_TRANSFER_PERMISSION_DENIED:Lcom/navdy/service/library/events/file/FileTransferError;
        //   780: invokevirtual   com/navdy/service/library/events/file/FileTransferResponse$Builder.error:(Lcom/navdy/service/library/events/file/FileTransferError;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;
        //   783: iconst_0       
        //   784: invokestatic    java/lang/Boolean.valueOf:(Z)Ljava/lang/Boolean;
        //   787: invokevirtual   com/navdy/service/library/events/file/FileTransferResponse$Builder.success:(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;
        //   790: invokevirtual   com/navdy/service/library/events/file/FileTransferResponse$Builder.build:()Lcom/navdy/service/library/events/file/FileTransferResponse;
        //   793: astore_1       
        //   794: goto            150
        //   797: aload_0        
        //   798: getfield        com/navdy/service/library/file/FileTransferSession.negotiatedOffset:J
        //   801: lstore          5
        //   803: lload           5
        //   805: lload           7
        //   807: lcmp           
        //   808: ifne            874
        //   811: aload           9
        //   813: astore_1       
        //   814: aload           13
        //   816: ldc             131072
        //   818: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
        //   821: invokevirtual   com/navdy/service/library/events/file/FileTransferResponse$Builder.maxChunkSize:(Ljava/lang/Integer;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;
        //   824: aload_0        
        //   825: getfield        com/navdy/service/library/file/FileTransferSession.negotiatedOffset:J
        //   828: invokestatic    java/lang/Long.valueOf:(J)Ljava/lang/Long;
        //   831: invokevirtual   com/navdy/service/library/events/file/FileTransferResponse$Builder.offset:(Ljava/lang/Long;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;
        //   834: pop            
        //   835: getstatic       com/navdy/service/library/file/FileTransferSession.sLogger:Lcom/navdy/service/library/log/Logger;
        //   838: ldc_w           "FileTransferRequest successful"
        //   841: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //   844: aload           13
        //   846: iconst_1       
        //   847: invokestatic    java/lang/Boolean.valueOf:(Z)Ljava/lang/Boolean;
        //   850: invokevirtual   com/navdy/service/library/events/file/FileTransferResponse$Builder.success:(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;
        //   853: pop            
        //   854: aload_1        
        //   855: ifnull          865
        //   858: aload           13
        //   860: aload_1        
        //   861: invokevirtual   com/navdy/service/library/events/file/FileTransferResponse$Builder.checksum:(Ljava/lang/String;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;
        //   864: pop            
        //   865: aload           13
        //   867: invokevirtual   com/navdy/service/library/events/file/FileTransferResponse$Builder.build:()Lcom/navdy/service/library/events/file/FileTransferResponse;
        //   870: astore_1       
        //   871: goto            150
        //   874: aload           12
        //   876: astore_1       
        //   877: aload_0        
        //   878: getfield        com/navdy/service/library/file/FileTransferSession.negotiatedOffset:J
        //   881: lconst_0       
        //   882: lcmp           
        //   883: ifle            814
        //   886: aload           14
        //   888: aload_0        
        //   889: getfield        com/navdy/service/library/file/FileTransferSession.negotiatedOffset:J
        //   892: invokestatic    com/navdy/service/library/util/IOUtils.hashForFile:(Ljava/io/File;J)Ljava/lang/String;
        //   895: astore_1       
        //   896: goto            814
        //   899: astore_2       
        //   900: aload_2        
        //   901: astore_3       
        //   902: goto            735
        //   905: astore          9
        //   907: aload_2        
        //   908: astore_3       
        //   909: goto            600
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  5      10     161    167    Any
        //  10     20     155    161    Any
        //  20     47     161    167    Any
        //  47     55     167    173    Any
        //  55     64     161    167    Any
        //  67     102    161    167    Any
        //  102    150    698    731    Ljava/lang/Exception;
        //  102    150    161    167    Any
        //  156    159    155    161    Any
        //  159    161    161    167    Any
        //  168    171    167    173    Any
        //  171    173    161    167    Any
        //  173    233    698    731    Ljava/lang/Exception;
        //  173    233    161    167    Any
        //  265    289    698    731    Ljava/lang/Exception;
        //  265    289    161    167    Any
        //  292    323    698    731    Ljava/lang/Exception;
        //  292    323    161    167    Any
        //  361    366    591    600    Ljava/lang/Throwable;
        //  361    366    731    735    Any
        //  372    380    591    600    Ljava/lang/Throwable;
        //  372    380    731    735    Any
        //  386    393    905    912    Ljava/lang/Throwable;
        //  386    393    899    905    Any
        //  399    407    905    912    Ljava/lang/Throwable;
        //  399    407    899    905    Any
        //  407    462    698    731    Ljava/lang/Exception;
        //  407    462    161    167    Any
        //  466    574    698    731    Ljava/lang/Exception;
        //  466    574    161    167    Any
        //  605    610    731    735    Any
        //  615    620    731    735    Any
        //  625    630    731    735    Any
        //  635    658    731    735    Any
        //  663    683    731    735    Any
        //  686    695    698    731    Ljava/lang/Exception;
        //  686    695    161    167    Any
        //  699    728    161    167    Any
        //  735    746    698    731    Ljava/lang/Exception;
        //  735    746    161    167    Any
        //  746    755    698    731    Ljava/lang/Exception;
        //  746    755    161    167    Any
        //  758    794    698    731    Ljava/lang/Exception;
        //  758    794    161    167    Any
        //  797    803    698    731    Ljava/lang/Exception;
        //  797    803    161    167    Any
        //  814    854    161    167    Any
        //  858    865    161    167    Any
        //  865    871    161    167    Any
        //  877    896    698    731    Ljava/lang/Exception;
        //  877    896    161    167    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index: 448, Size: 448
        //     at java.util.ArrayList.rangeCheck(Unknown Source)
        //     at java.util.ArrayList.get(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public FileTransferResponse initPull(final Context context, final FileType mFileType, final String mDestinationFolder, final String mFileName, final boolean mWaitForAcknowledgements) {
        synchronized (this) {
            while (true) {
                final FileTransferResponse.Builder builder = new FileTransferResponse.Builder();
                this.mWaitForAcknowledgements = mWaitForAcknowledgements;
                this.mFileType = mFileType;
                builder.supportsAcks(mWaitForAcknowledgements);
                builder.destinationFileName(mFileName).fileType(mFileType);
                try {
                    synchronized (this) {
                        this.mLastActivity = System.currentTimeMillis();
                        // monitorexit(this)
                        this.mDestinationFolder = mDestinationFolder;
                        this.mFileName = mFileName;
                        this.mFile = new File(mDestinationFolder, this.mFileName);
                        this.mPullRequest = true;
                        if (this.mFile.exists() && this.mFile.canRead() && this.mFile.length() > 0L) {
                            this.mFileSize = this.mFile.length();
                            this.mFileReader = new FileInputStream(this.mFile);
                            return builder.success(true).maxChunkSize(16384).transferId(this.mTransferId).offset(this.mFileSize).build();
                        }
                    }
                }
                catch (Throwable t) {
                    builder.success(false).error(FileTransferError.FILE_TRANSFER_IO_ERROR).build();
                    return;
                }
                return builder.success(false).error(FileTransferError.FILE_TRANSFER_IO_ERROR).build();
            }
        }
    }
    
    public FileTransferResponse initTestData(Context context, final FileType mFileType, final String mFileName, final long mFileSize) {
        synchronized (this) {
            this.mLastActivity = System.currentTimeMillis();
            this.mDestinationFolder = "";
            this.mFileName = mFileName;
            this.mFileSize = mFileSize;
            this.mFileType = mFileType;
            this.mExpectedChunk = 0;
            this.mTotalBytesTransferred = 0L;
            this.negotiatedOffset = 0L;
            this.mIsTestTransfer = true;
            context = (Context)new FileTransferResponse.Builder();
            ((FileTransferResponse.Builder)context).fileType(mFileType).destinationFileName(mFileName);
            ((FileTransferResponse.Builder)context).maxChunkSize(131072).offset(this.negotiatedOffset);
            ((FileTransferResponse.Builder)context).transferId(this.mTransferId);
            try {
                this.mDataCheckSum = MessageDigest.getInstance("MD5");
                FileTransferSession.sLogger.d("FileTransferRequest successful");
                ((FileTransferResponse.Builder)context).success(true);
                context = (Context)((FileTransferResponse.Builder)context).build();
                return (FileTransferResponse)context;
            }
            catch (NoSuchAlgorithmException ex) {
                FileTransferSession.sLogger.e("unable to create MD5 message digest");
                context = (Context)((FileTransferResponse.Builder)context).error(FileTransferError.FILE_TRANSFER_CHECKSUM_ERROR).success(false).build();
            }
        }
    }
    
    public boolean isFlowControlEnabled() {
        return this.mWaitForAcknowledgements;
    }
}
