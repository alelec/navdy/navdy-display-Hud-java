package com.navdy.service.library.file;

import com.navdy.service.library.events.file.FileTransferResponse;
import com.navdy.service.library.events.file.FileTransferRequest;
import com.navdy.service.library.events.file.FileTransferStatus;
import com.navdy.service.library.events.file.FileTransferData;
import com.navdy.service.library.events.file.FileType;

public interface IFileTransferManager
{
    FileType getFileType(final int p0);
    
    FileTransferData getNextChunk(final int p0) throws Throwable;
    
    FileTransferStatus handleFileTransferData(final FileTransferData p0);
    
    FileTransferResponse handleFileTransferRequest(final FileTransferRequest p0);
    
    boolean handleFileTransferStatus(final FileTransferStatus p0);
    
    void stop();
}
