package com.navdy.service.library.network;

import com.navdy.service.library.device.connection.ConnectionInfo;
import com.navdy.service.library.device.connection.ConnectionType;
import java.io.IOException;
import java.io.Closeable;

public interface SocketAcceptor extends Closeable
{
    SocketAdapter accept() throws IOException;
    
    void close() throws IOException;
    
    ConnectionInfo getRemoteConnectionInfo(final SocketAdapter p0, final ConnectionType p1);
}
