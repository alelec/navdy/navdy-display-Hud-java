package com.navdy.service.library.network;

import java.io.IOException;

public interface SocketFactory
{
    SocketAdapter build() throws IOException;
}
